<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

function getGatunekRoutes($lang = null) {
    $con = \Cake\Datasource\ConnectionManager::get('default');
    $gatunki = $con->execute('SELECT * FROM gatunek WHERE mask IS NOT NULL AND mask!=""')->fetchAll('assoc');
    $allRoutes = [];
    if (!empty($gatunki)) {
        foreach ($gatunki as $gatunek) {
            $allRoutes[$gatunek['mask']] = [
                'controller' => 'Towar',
                'action' => 'index',
                'type'=>$gatunek['id']
            ];
        }
    }
    return $allRoutes;
}

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

//Router::addUrlFilter(function ($params, $request) {
//	if (isset($request->params['language']) && !isset($params['language'])) {
//		$params['language'] = $request->params['language'];
//	} elseif (!isset($params['language'])) {
//		$params['language'] = 'pl'; // set your default language here
//	}
//	return $params;
//});
 
//Router::scope('/:language', function ($routes) {
//	// All URLs with the language go here
//    
//            $routes->prefix('admin', function ($routes) {
//                        $routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'InflectedRoute']);
//                        $routes->connect('/', ['controller' => 'Index', 'action' => 'login']);
//                        $routes->connect('/login', ['controller' => 'Index', 'action' => 'login']);
//                        $routes->fallbacks(DashedRoute::class);
//                    });
//    $routes->connect('/', ['controller' => 'Home', 'action' => 'index']);
//    $routes->connect('/rodo/*', ['controller' => 'Landingpage', 'action' => 'rodo']);
//    $routes->connect('/towar/lista/*', ['controller' => 'Towar', 'action' => 'index']);
//    $routes->connect('/kontakt', ['controller' => 'Pages', 'action' => 'view',10,'kontakt']);
//    $routes->fallbacks(DashedRoute::class);
//});
Router::scope('/', function (RouteBuilder $routes) {
            $routes->prefix('api', function ($routes) {
                        $routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'InflectedRoute']);
    $routes->extensions(['xml','json']);
                        $routes->fallbacks(DashedRoute::class);
                    });
            $routes->prefix('admin', function ($routes) {
                        $routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'InflectedRoute']);
                        $routes->connect('/', ['controller' => 'Index', 'action' => 'login']);
                        $routes->connect('/login', ['controller' => 'Index', 'action' => 'login']);
                        $routes->fallbacks(DashedRoute::class);
                    });
    /**
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */
                    
    $routes->connect('/', ['controller' => 'Home', 'action' => 'index']);
    $routes->connect('/kontakt', ['controller' => 'Pages', 'action' => 'view',1,'kontakt']);
$routesMask = getGatunekRoutes();
    if (!empty($routesMask)) {
        foreach ($routesMask as $mask => $routParam) {
            $routes->connect(((strpos($mask, '/')!==false && strpos($mask, '/')==0)?'':'/').$mask, $routParam);
        }
    }
    $routes->connect('/towar/lista/*', ['controller' => 'Towar', 'action' => 'index']);
    $routes->extensions(['xml','pdf']);

    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);`
     *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);`
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks(DashedRoute::class);
});
/**
 * Load all plugin routes. See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();
