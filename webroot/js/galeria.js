$(document).on('click', '.view-thumbs-control.slide-next a', function () {
//    $('.view-thumbs-inner').scrollLeft($('.view-thumbs-inner').scrollLeft()+95);
    thumbSlide($('.view-thumbs-inner'), ($($('.view-thumbs-inner > .view-thumb')[0]).width()*2));
});
$(document).on('click', '.view-thumbs-control.slide-prev a', function () {
//    $('.view-thumbs-inner').scrollLeft($('.view-thumbs-inner').scrollLeft()-95);
    thumbSlide($('.view-thumbs-inner'), -($($('.view-thumbs-inner > .view-thumb')[0]).width()*2));
});
$(document).on('click', '.pion .view-thumbs-control.slide-next a', function () {
//    $('.view-thumbs-inner').scrollLeft($('.view-thumbs-inner').scrollLeft()+95);
    thumbSlidePion($('.pion .view-thumbs-inner'), ($($('.pion .view-thumbs-inner > .view-thumb')[0]).height()*2));
});
$(document).on('click', '.pion .view-thumbs-control.slide-prev a', function () {
//    $('.view-thumbs-inner').scrollLeft($('.view-thumbs-inner').scrollLeft()-95);
    thumbSlidePion($('.pion .view-thumbs-inner'), -($($('.pion .view-thumbs-inner > .view-thumb')[0]).height()*2));
});
$(document).on('click', '.view-thumb', function () {
    $('.view-thumb').removeClass('active');
    $(this).addClass('active');
    var w = false;
    if ($('.view-image img').attr('w') !== undefined && $('.view-image img').attr('w') === 'true') {
        w = true;
    }
    $('.view-image').addClass('reload');
    $('.view-image img').attr('src', $(this).find('img').attr('src').replace('thumb', 'thumb_3') + (w ? '/1' : '')).attr('alt', $(this).find('img').attr('alt'));
    $('.view-image').attr('href', $(this).find('img').attr('src-orgin'));
    $('.view-image img').on('load', function () {
        $('.view-image').removeClass('reload');
    });
});
$(document).ready(function () {
    if ($('.view-thumbs-inner > .view-thumb').length > 3) {
        $('.view-thumbs-inner')
                .mousewheel(function (event, delta) {
                    if (delta < 0) {
                        thumbSlide($('.view-thumbs-inner'), ($($('.view-thumbs-inner > .view-thumb')[0]).width() * 2) * (delta * -1));
                    } else {
                        thumbSlide($('.view-thumbs-inner'), -($($('.view-thumbs-inner > .view-thumb')[0]).width() * 2) * delta);
                    }
                    return false; // prevent default
                });
    }
});
function thumbSlide(item, move) {
    item.stop();
    var befScrl = item.scrollLeft();
    item.animate({
        scrollLeft: befScrl + move
    }, 300);
}
function thumbSlidePion(item, move) {
    item.stop();
    var befScrl = item.scrollTop();
    item.animate({
        scrollTop: befScrl + move
    }, 300);
}
var tmpPrevSrc = '';
$(document).on('click', '.view-image', function () {
    var imgSrc = $(this).attr('href');
    $('body').addClass('image-preview');
    $('#image-preview').show();
    $('#image-preview img').attr('src', imgSrc);
    loadImagePreview(imgSrc);

//    $(this).addClass('preview');
//    tmpPrevSrc=$(this).find('img').attr('src');
//    $(this).find('img').attr('src',tmpPrevSrc.replace('thumb_3_',''));

});
function loadImagePreview(imgSrc) {
    if ($('#image-preview img')[0].complete) {
        setPrevPos();
    } else {
        var image = new Image();
        image.src = imgSrc;
        image.onload = function () {
            setPrevPos();
        };
    }
}
function setPrevPos() {
    var iHeight = ($('#image-preview img').height() + 30);
    if (iHeight < $(window).height()) {
        $('#image-preview img').css('margin-top', (($(window).height() - iHeight) / 2));
    } else {
        $('#image-preview img').css('margin-top', 0);
    }
    $('#image-preview img').addClass('loaded');
    $('.image-preview-nav').addClass('loaded');
    $('.image-preview-nav').css('width',$('#image-preview img').width()+30);
    $('.image-preview-nav').css('height',$('#image-preview img').height()+30);
    $('.image-preview-nav').css('left',$('#image-preview img').offset().left);
    $('.image-preview-nav').css('margin-top',$('#image-preview img').css('margin-top'));
    if($('.view-thumb').length>1){
        $('.image-preview-nav [data-target=prev],.image-preview-nav [data-target=next]').removeClass('hidden');
    }else{
        $('.image-preview-nav [data-target=prev],.image-preview-nav [data-target=next]').addClass('hidden');
    }
}
$(document).on('click', '#image-preview', function () {
    closePreview();
});
function previewNav(type) {
    var act = $('.view-thumb.active');
    if (type !== undefined && type === 'prev') {
        var next = act.prev('.view-thumb');
    } else if(type==='close'){
        closePreview();
        return false;
    }
    else {
        var next = act.next('.view-thumb');
    }
     if (!(next !== undefined && next.length > 0)) {
        if (type !== undefined && type === 'prev') {
            next=$($('.view-thumb')[$('.view-thumb').length-1])
        }else{
            next=$($('.view-thumb')[0]);
        }
    }
    if (next !== undefined && next.length > 0) {
        $('.view-thumb').removeClass('active');
        next.addClass('active');
//        next.trigger('click');
        var imgSrc = next.find('img').attr('src-orgin');
        $('#image-preview img').removeClass('loaded');
        $('.image-preview-nav').removeClass('loaded');
        $('#image-preview img').attr('src', imgSrc);
        loadImagePreview(imgSrc);
    }else{
        closePreview();
    }
}
$(document).on('click', '.image-preview-nav > a', function () {
    previewNav($(this).attr('data-target'));
    return false;
});
function closePreview(){
    $('body').removeClass('image-preview');
    $('#image-preview img').css('margin-top', 0);
    $('#image-preview img').removeClass('loaded');
    $('.image-preview-nav').removeClass('loaded');
        $('.view-thumb').removeClass('active');
    $('.view-thumb img[src-orgin="'+$('.view-image').attr('href')+'"]').parents('.view-thumb').addClass('active');
    $('#image-preview').hide();
}
$(window).resize(function(){
    if($('body').hasClass('image-preview')){
        setPrevPos();
    }
});
$(document).on('change','.view-ilosc .item-ilosc-input',function(){
    var ilosc = parseFloat($(this).val());
    if(ilosc<1 || isNaN(ilosc)){
        ilosc=1;
    }
    var total=parseFloat($(this).attr('item-price')) * ilosc;
    $('.view-total').html(currency(total));
});

var myElement = document.getElementById('galeria-image-big');
var hammertime = new Hammer(myElement, {});
hammertime.on('swipe', function(ev) {
        if(ev.type==='swipe'){
            if(ev.direction===2){
                if($('.view-thumb.active').next('.view-thumb').length>0){
                    $('.view-thumb.active').next('.view-thumb').trigger('click');
                }
            }else if(ev.direction===4){
                if($('.view-thumb.active').prev('.view-thumb').length>0){
                    $('.view-thumb.active').prev('.view-thumb').trigger('click');
                }
            }
        }
});