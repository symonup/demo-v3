String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};
var curentDietaItems;
var blockedDays;
var allEvents = new Object();
var priceRangeSlider;
var messages = new Object();
function empty(item) {
    if (item === undefined || item === '' || item === null) {
        return true;
    } else {
        return false;
    }
}
function _error(msg, title) {

    if (typeof (PNotify) === 'undefined') {
        return;
    }
    if (title === undefined || title === '') {
        title = 'Błąd!'
    }
    new PNotify({
        title: title,
        type: "error",
        text: msg,
        styling: 'bootstrap3'
    });

}
function _success(msg, title) {
    if (typeof (PNotify) === 'undefined') {
        return;
    }
    if (title === undefined || title === '') {
        title = 'Sukces!'
    }
    new PNotify({
        title: title,
        type: "success",
        text: msg,
        styling: 'bootstrap3'
    });

}
function bodyPadding() {
    if (!$('body').hasClass('mobile')) {
        $('body').css('padding-bottom', $('footer').height());
    } else {
        $('body').css('padding-bottom', 0);
    }
}
$(window).resize(function () {
    checkWindowWidth();
    bodyPadding();
    mainBanerResize();
});
function checkWindowWidth() {
    if ($('body').hasClass('devDesktop')) {
        if ($(window).width() >= 965 && $('body').hasClass('mobile')) {
            $.ajax({
                url: '/home/set-desktop/1',
                async: false,
                success: function () {
                    document.location.reload();
                }
            });
        } else {
            if ($(window).width() < 965 && !$('body').hasClass('mobile')) {
                $.ajax({
                    url: '/home/set-desktop/0',
                    async: false,
                    success: function () {
                        document.location.reload();
                    }
                });
            }
        }
    }

}
$(document).ready(function () {
    checkWindowWidth();
    moment.locale('pl');
    if ($('[multi-select=true]').length > 0) {
        $.each($('[multi-select=true]'), function (i, v) {
            createMultiselect(v);
        });
    }
    if (Object.keys(alertMessages).length > 0) {
        $.each(alertMessages, function (msgType, messages) {
            if (msgType === 'success') {
                $.each(messages, function (i, msg) {
                    _success(msg);
                });
            } else {
                $.each(messages, function (i, msg) {
                    _error(msg);
                });
            }
        });
    }
    bodyPadding();
    $('[data-toggle="tooltip"]').tooltip({
        container: 'body'
    });
    $('[data-toggle="popover"]').popover({
        container: 'body'
    });
    priceRangeSlider = $("#price-range").ionRangeSlider({
        type: "double",
        min: 0,
        max: $("#price-range").attr('data-max'),
        step: $("#price-range").attr('data-step'),
        postfix: $("#price-range").attr('data-postfix'),
        prefix: $("#price-range").attr('data-prefix'),
        hide_min_max: true,
        hide_from_to: true,
        onStart: function (data) {
            $('.price-from').val(data.from);
            $('.price-to').val(data.to);
        },
        onChange: function (data) {
            $('.price-from').val(data.from);
            $('.price-to').val(data.to);
        },
        onUpdate: function (data) {
        }
    });
    var values = [3, 7, 12, 16, 18];
    var values_p = ['3+', '7+', '12+', '16+', '18+'];
    $("#pegi-range").ionRangeSlider({
        type: "double",
        min: 3,
        max: 18,
        step: 1,
        postfix: false,
        prefix: false,
        hide_min_max: true,
        hide_from_to: false,
        values: values,
        prettify: function (n) {
            var ind = values.indexOf(n);
            return values_p[ind];
        },
        onStart: function (data) {
            $('.pegi-from').val(data.from);
            $('.pegi-to').val(data.to);
        },
        onChange: function (data) {
            $('.pegi-from').val(data.from);
            $('.pegi-to').val(data.to);
        },
        onUpdate: function (data) {
        }
    });
    if ($('.filter-category').length > 0) {
        $('.filter-category li.curent').parents('li:not(.filter-category-container)').addClass('open');
    }
    if ($('.register-form').length > 0) {
        validateRegister();
    }
    if ($('#wysylka-form').length > 0) {
        validateWysylka();
    }
    if ($('#faktura-form').length > 0) {
        validateFaktura();
    }
    if ($('.mobile-category .dropdown-item.active').length > 0) {
        $($('.mobile-category .dropdown-item.active').parents('.dropdown-menu')[0]).addClass('show');
        $($('.mobile-category .dropdown-item.active').parents('.mobile-category')[0]).addClass('show');
        $($('.mobile-category .dropdown-item.active').parents('.mobile-category')[0]).find('a[dropdown-toggle]').attr('aria-expanded', 'true');
    }
    mainBanerResize();
    timecounters();
    lazyImages();
    checkReload();
    if ($('#get-inpost-map').length > 0) {
        inpostSelect();
    }
    if ($('.form-auto-submit').length > 0) {
        $('.form-auto-submit').submit();
    }

    if ($('#cookieBar').length > 0) {
        $('#cookieBar').addClass('open');
    }
    checkCssMap();
});
function checkReload() {
    if ($('[page-reload-all]').length > 0) {
        var sec = parseFloat($($('[page-reload-all]')[0]).attr('page-reload-all')) * 1000;
        setTimeout(function () {
            document.location.reload();
        }, sec);
    }
}
$(document).on('click', '.towar-lista-filter > li', function () {
    if ($(this).hasClass('open')) {
        $(this).removeClass('open');
    } else {
        $('.towar-lista-filter > li').removeClass('open');
        $(this).addClass('open');
    }
});
function timecounters() {
//    return false;
    if ($('.time-counter').length > 0) {
        $.each($('.time-counter'), function (i, v) {
            starttime('#' + $(v).attr('id'), $(v).attr('count-to'), $(v).attr('label-d'), $(v).attr('label-h'), $(v).attr('label-m'), $(v).attr('label-s'), $(v).attr('hide-labels'));
        });
    }
}
$(document).on('change', '.filter-category input[type=checkbox]', function () {
    var elem = this;
    var firstParent = $(this).parents('li')[0];
    var allParent = $(firstParent).parents('li');
    var allChildren = $(firstParent).find('li');

    if (allChildren.length > 0) {
        $.each(allChildren, function (i, v) {
            $.each($(v).find('input[type=checkbox]'), function (ci, cv) {
                cv.checked = $(elem).is(':checked');
            });
        });
    }
    if (allParent.length > 0) {
        $.each(allParent, function (i, v) {
            if ($(elem).is(':checked')) {
                $(v).find('input[type=checkbox]')[0].checked = true;
            } else {
                if ($(v).find('input[type=checkbox]').is(':checked') && !($(v).find('input[type=checkbox]:checked').length > 1)) {
                    $(v).find('input[type=checkbox]')[0].checked = false;
                }
            }
        });
    }

});
var filterTimeOutSubmit;
$(document).on('change', '#filterForm :input:not(.finder):not(.goToPage):not([name="sort"])', function () {
    if (!$('body').hasClass('mobile') || !$('#filterShowXs').is(':visible')) {
        clearTimeout(filterTimeOutSubmit);
        filterTimeOutSubmit = setTimeout(filterSubmit, 1500);
    }
});
function filterSubmit() {
    $('body .reloadPage').css('padding-top', $(document).scrollTop() + ($(window).height() / 2) - 50);
    $('body').addClass('reload');
    if ($('.price-from').val() == '0' && $('.price-to').val() == $('#price-range').attr('data-max')) {
        $('#price-range').attr('disabled', 'disabled');
    }
    $('#filterForm').submit();

}
$(window).scroll(function () {
    if ($('body.reload .reloadPage').length > 0) {
        $('body .reloadPage').css('padding-top', $(document).scrollTop() + ($(window).height() / 2) - 50);
    }
    if ($('body').hasClass('devMobile')) {
        $('*').tooltip('hide');
    }
});
$(document).on('keyup', '#filterForm input.price-from,#filterForm input.price-to', function () {
    var value = $(this).val().replace(',', '.');
    $(this).val(value);
    if (value === '' || parseFloat(value) < 0) {
        value = 0;
    }
    if ($(this).hasClass('price-to')) {
        $(priceRangeSlider).data("ionRangeSlider").update({to: value});
    } else {
        $(priceRangeSlider).data("ionRangeSlider").update({from: value});
    }
});
$(document).on('click', '#price-range-inputs > div', function () {
    $(this).find('input').focus().select();
});
function round(val, prec) {
    val = parseFloat(val);
    if (isNaN(val)) {
        return val;
    }
    if (prec === undefined) {
        return Math.round(val);
    }
    var tmpPrec = '1';
    for (var i = 0; i < parseInt(prec); i++) {
        tmpPrec = tmpPrec + '0';
    }
    prec = parseFloat(tmpPrec);
    return (Math.round((val * prec)) / prec);
}
function currency(value) {
    if (value === undefined) {
        value = 0;
    }
    value = parseFloat(value);
    if (isNaN(value)) {
        value = 0;
    }

    value = round(value, 2).toString();
    var $tmp = value.split('.');
    var $thousend = '';
    var $after = '';
    var $before = '';
    var $space = '';
    var $symbol = currencyOptions['symbol'];
    if (currencyOptions['options']['space']) {
        $space = ' ';
    }
    if (currencyOptions['options']['after']) {
        $after = $space + $symbol;
    } else {
        $before = $symbol + $space;
    }
    if (($tmp[0] / 1000) >= 1) {
        $thousend = $tmp[0] / 1000;
        var $tmpTh = $thousend.toString().split('.');
        if ($tmpTh.length > 1) {
            var $add0 = '';
            if ($tmpTh[1].length < 3) {
                for ($i = $tmpTh[1].length; $i < 3; $i++) {
                    $add0 += '0';
                }
            }
        } else {
            var $add0 = '000';
        }
        $thousend = $tmpTh[0] + currencyOptions['options']['thousend'] + (($tmpTh[1] !== undefined) ? $tmpTh[1] : '') + $add0;
        var $decimal = '';
        if ($tmp[1] !== undefined) {
            if ($tmp[1].length == 1) {
                $decimal = $tmp[1] + '0';
            } else if ($tmp[1].length == 0) {
                $decimal = $tmp[1] + '00';
            } else {
                $decimal = $tmp[1];
            }
        } else {
            $decimal = '00';
        }
        return $before + $thousend + currencyOptions['options']['decimal'] + $decimal + $after;
    } else {
        if ($tmp[1] !== undefined) {
            if ($tmp[1].length == 1) {
                $decimal = $tmp[1] + '0';
            } else if ($tmp[1].length == 0) {
                $decimal = $tmp[1] + '00';
            } else {
                $decimal = $tmp[1];
            }
        } else {
            $decimal = '00';
        }
        return $before + $tmp[0] + currencyOptions['options']['decimal'] + $decimal + $after;
    }
}
Object.size = function (obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key))
            size++;
    }
    return size;
};

$(document).on('click', '.search > i', function () {
    var fraza = $('.search-input').val();
    var href = document.location.href;
    if (fraza !== '') {
        if (document.location.search !== '') {
            href.replace(document.location.search, '');
        }
        fraza = basePath + '?search=' + fraza;
        document.location = fraza;
    }
    return false;
});
$(document).on('keypress', '.search-input', function (e) {
    var event = e || window.event;
    var charCode = event.which || event.keyCode;
    if (charCode == '13') {
//        $('.search > button').trigger('click');
//        return false;
    }
});
$(document).on('keyup', 'form.search .search-input', function (e) {
    var charCode = e.which || e.keyCode;
    if (charCode == 27) {
        $('#rq-results').remove();
    } else {
        searchKeyUp($(this).parents('form'));
    }
});
$(document).on('change', 'form.search select', function (e) {
    searchKeyUp($(this).parents('form'));
});
var xhrSearch;
function searchKeyUp(form) {
//    if (!$('body').hasClass('mobile')) {
    if (xhrSearch !== undefined) {
        xhrSearch.abort();
    }
    if (form.find('.search-input').val() !== '') {
        var url = form.attr('ajax-action');
        var data = form.serialize();
        xhrSearch = $.ajax({
            type: 'post',
            url: url,
            data: data,
            success: function (data, textStatus, jqXHR) {
                if (form.find('#rq-results').length > 0) {
                    form.find('#rq-results').remove();
                }
                $(data).appendTo(form);
            }
        });
    } else {
        form.find('#rq-results').remove();
    }
//    }
}
$(document).on('click', '*', function (e) {
    if ($('#rq-results').length > 0) {
        if (!$(e.target).hasClass('.search') && !($(e.target).parents('form.search').length > 0)) {
            $('#rq-results').remove();
        }
    }
    if ($('.towar-lista-filter > li.open').length > 0) {
        if (!$(e.target).hasClass('.towar-lista-filter') && !($(e.target).parents('.towar-lista-filter').length > 0)) {
            $('.towar-lista-filter > li').removeClass('open');
        }
    }
    if ($('.lws-widget-container').length > 0) {
        if (!$(e.target).hasClass('.lws-widget-container') && !($(e.target).parents('.lws-widget-container').length > 0)) {
            $('.open-widget-lws,.widget-lws').removeClass('active');
        }
    }
});
$(document).on('click', '.confirm-link', function () {
    var href = $(this).attr('href');
    var yesBtn = (($(this).attr('confirm-btn-yes') !== undefined) ? $(this).attr('confirm-btn-yes') : 'Tak');
    var noBtn = (($(this).attr('confirm-btn-no') !== undefined) ? $(this).attr('confirm-btn-no') : 'Nie');
    var cfMessage = $(this).attr('confirm-message');
    swal({
        title: '',
        text: cfMessage,
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: yesBtn,
        cancelButtonText: noBtn,
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-outline-success',
        buttonsStyling: false
    }).then(function () {
        document.location = href;
    }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if (dismiss === 'cancel') {

        }
    });
    return false;
});
$(document).on('click', '.cart-list-show-detalis', function () {
    var parentTr = $($(this).parents('tr')[0]);
    if (parentTr.hasClass('open')) {
        parentTr.removeClass('open')
    } else {
        parentTr.addClass('open')
    }
});

$(document).on('keyup', '.finder', function () {
    var searchTerm = $(this).val();
    var _target = $(this).attr('data-target');
    if (searchTerm === '')
    {
        $(_target + ' li').removeClass('hidden');
    } else
    {
        var searchSplit = searchTerm.replace(/ /g, "'):containsi('");
        $.extend($.expr[':'], {'containsi': function (elem, i, match, array) {
                return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
            }
        });

        $(_target + ' li').not(":containsi('" + searchSplit + "')").each(function (e) {
            $(this).addClass('hidden');
        });

        $(_target + " li:containsi('" + searchSplit + "')").each(function (e) {
            $(this).removeClass('hidden');
        });
    }
});

$(document).on('keypress', 'input[data-type=float]', function (e) {
    var key = e.which || e.keyCode;
    if ((key >= 48 && key <= 57) || key === 44 || key === 46 || key === 8 || key === 37 || key === 38 || key === 39 || key === 40) {
        // 0-9 comma and dot only
    } else {
        return false;
    }
});
$(document).on('keydown', 'input[data-type=number]', function (e) {
    var key = e.which || e.keyCode;
    console.log(key);
    if ((key >= 96 && key <= 105) || (key >= 48 && key <= 57) || key === 13 || key === 46 || key === 8 || key === 37 || key === 38 || key === 39 || key === 40) {
        // 0-9 
    } else {
        return false;
    }
});
$(document).on('keypress', 'input[data-type=phone]', function (e) {
    var key = e.which || e.keyCode;
    if ((key >= 48 && key <= 57) || key === 32 || key === 45 || key === 40 || key === 41 || key === 8 || key === 46 || key === 37 || key === 38 || key === 39 || key === 43) {
        // 0-9 comma and dot only
    } else {
        return false;
    }
});
$(document).on('keyup change', 'input[data-type=float]', function () {
    $(this).val($(this).val().replace(',', '.'));
});
function goToTab(target) {
    if ($(target).length === 1) {
        $(target).trigger('click');
        $('body,html').animate({
            scrollTop: $(target).offset().top
        }, 300);
    }
    return false;
}
$(document).on('click', '.filter-more-btn', function () {
    var parent = $(this).parents('ul')[0];
    if ($(parent).hasClass('more-open')) {
        $(parent).removeClass('more-open');
        $(this).html($(this).attr('more-txt'));
    } else {
        $(parent).addClass('more-open');
        $(this).html($(this).attr('less-txt'));
    }
});
$(document).on('click', '.favorite', function () {
    var target = '';
    var add = false;
    $(this).tooltip('hide');
    $(this).blur();
    document.activeElement = null;
    if ($(this).hasClass('active')) {
        target = $(this).attr('remove-target');
        $(this).removeClass('active');
        $('.favorite[item-id=' + $(this).attr('item-id') + ']').removeClass('active');
        if ($('body').hasClass('towar_index_ulubione')) {
            $($(this).parents('.item-list')[0]).fadeOut();
        }
    } else {
        add = true;
        target = $(this).attr('add-target');
        $(this).addClass('active');
        $('.favorite[item-id=' + $(this).attr('item-id') + ']').addClass('active');
    }
    if (target !== '') {
        $.ajax({
            type: 'get',
            dataType: 'json',
            url: target,
            success: function (data, textStatus, jqXHR) {
                if ($('body').hasClass('towar_index_ulubione') && add) {
                    document.location.reload();
                }
            }
        });
    }
    return false;
});
$(document).on('click', '.btn.disabled', function () {
    return false;
});
function getTranslate(message, sync) {
    var translateMessage = message;
    if (sync == undefined) {
        sync = false;
    }
    if (messages[message] !== undefined && messages[message] !== message) {
        return messages[message];
    }
    $.ajax({
        type: 'get',
        async: sync,
        data: 'message=' + message,
        url: basePath + 'home/get-translate',
        success: function (data, textStatus, jqXHR) {
            translateMessage = data;
        }
    });
    messages[message] = translateMessage;
    return translateMessage;
}
function validateRegister() {
    $('.register-form').validate({
        rules: {
            imie: {required: true},
            nazwisko: {required: true},
            password: {required: true},
            repeat_password: {required: true, equalTo: "#register-password"},
            email: {
                required: true
            },
            telefon: {
                required: true,
//                    phonePL: true
            },
            nip: {
                nip: true
            }
        },
        messages: {
//            telefon:{
//                    phonePL: getTranslate('validate.PodajPoprawnyNrTel',false)
//                },
            nip: {
                nip: getTranslate('validate.PodajPoprawnyNrNIP', false)
            }
        },
        showErrors: function (errorMap, errorList) {
            $.each(errorList, function (i, v) {
                $($(v.element).parents('.form-group')[0]).addClass('error');
            });
            this.defaultShowErrors();
        },
        unhighlight: function (element, errorClass, validClass) {
            $($(element).parents('.form-group')[0]).removeClass('error');
        }
    });
}
function validateWysylka() {
    $('#wysylka-form').validate({
        rules: {
            imie: {required: true},
            nazwisko: {required: true},
            ulica: {required: true},
            nr_domu: {required: true},
            miasto: {required: true},
            kraj: {required: true},
            email: {
                required: true
            },
            telefon: {
                required: true,
//                    phonePL: true
            },
            kod: {required: true, postalcodePL: true}
        },
        messages: {
//            telefon:
//                    {
//                    phonePL: getTranslate('validate.PodajPoprawnyNrTel',false)
//                },
            kod: {
                postalcodePL: getTranslate('validate.PodajPoprawnyKodPocztowy', false)
            }
        },
        showErrors: function (errorMap, errorList) {
            $.each(errorList, function (i, v) {
                $($(v.element).parents('.form-group')[0]).addClass('error');
            });
            this.defaultShowErrors();
        },
        unhighlight: function (element, errorClass, validClass) {
            $($(element).parents('.form-group')[0]).removeClass('error');
        }
    });
}
function validateFaktura() {
    $('#faktura-form').validate({
        rules: {
            imie: {required: true},
            nazwisko: {required: true},
            nip: {nip: true},
            ulica: {required: true},
            nr_domu: {required: true},
            miasto: {required: true},
            kraj: {required: true},
            email: {
                required: true
            },
            telefon: {
                required: true,
//                    phonePL: true
            },
            kod: {required: true, postalcodePL: true}
        },
        messages: {
            kod: {
                postalcodePL: getTranslate('validate.PodajPoprawnyKodPocztowy', false)
            },
            nip: {
                nip: getTranslate('validate.PodajPoprawnyNrNIP', false)
            }
        },
        showErrors: function (errorMap, errorList) {
            $.each(errorList, function (i, v) {
                $($(v.element).parents('.form-group')[0]).addClass('error');
            });
            this.defaultShowErrors();
        },
        unhighlight: function (element, errorClass, validClass) {
            $($(element).parents('.form-group')[0]).removeClass('error');
        }
    });
}
function getPrices() {
    $.ajax({
        type: 'GET',
        url: '/uzytkownik/get-prices',
        async: true
    });
}
$(document).on('click', '#catShowAllXs', function () {
    if ($('ul.filter-category').hasClass('showAll')) {
        $('ul.filter-category').removeClass('showAll');
    } else {
        $('ul.filter-category').addClass('showAll');
    }
});
$(document).on('click', '#filterShowXs', function () {
    if ($(this).hasClass('opened')) {
        $('form#filterForm').removeClass('open');
        $(this).removeClass('opened');
    } else {
        $('form#filterForm').addClass('open');
        $(this).addClass('opened');
    }
    $(this).blur();
});
$(document).on('click', '#pagesLeftMenuOpen', function () {
    if ($('.pages-menu').hasClass('open')) {
        $('.pages-menu').removeClass('open');
    } else {
        $('.pages-menu').addClass('open');
    }
});
$(document).on('click', '#userMenuShowBtn', function () {
    if ($('.user-nav').hasClass('open')) {
        $('.user-nav').removeClass('open');
    } else {
        $('.user-nav').addClass('open');
    }
});
function mainBanerResize() {
//    return false;
    $.each($('.banner-container'), function (i, v) {
        var baseW = parseInt($($(v).find('.carousel-item')[0]).attr('def-width'));
        var actW = $(v).width();
        var actProc = ((actW * 100) / baseW);
        var baseH = parseInt($($(v).find('.carousel-item')[0]).attr('def-height'));
        var actH = ((baseH * actProc) / 100);
        $(v).find('.carousel-item').height(actH);
        $(v).addClass('loaded');
    });
}
function starttime(item, endTime, labelD, labelH, labelM, labelS, hideLabels)
{
    if (hideLabels === undefined || hideLabels === 'false' || hideLabels === false) {
        hideLabels = false;
    } else {
        hideLabels = true;
    }

//     return false;
//  var endtime = '2018-11-23 12:00:00';
    var arr = endTime.split(/[- :]/);
    var dH = ((arr[3] !== undefined) ? arr[3] : '00');
    var dM = ((arr[4] !== undefined) ? arr[4] : '00');
    var dS = ((arr[5] !== undefined) ? arr[5] : '00');
    var date = new Date(arr[0], arr[1] - 1, arr[2], dH, dM, dS);
    var now = new Date();
    var t = date.getTime() - now.getTime();

    var seconds = Math.floor((t / 1000) % 60);
    var minutes = Math.floor((t / 1000 / 60) % 60);
    var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
    var days = Math.floor(t / (1000 * 60 * 60 * 24));
    var htmlDays = '';
    if (days > 4 && ($(item).attr('allow-days') !== undefined && $(item).attr('allow-days') === 'true')) {
        for (var di = 0; di < days.toString().length; di++) {
            htmlDays = htmlDays + '<span>' + days.toString()[di] + '</span>';
        }
    } else if (days > 0) {
        var dayHours = days * 24;
        hours = hours + dayHours;
    }
    var htmlHours = '<span>0</span><span>0</span>';
    var htmlMinutes = '<span>0</span><span>0</span>';
    var htmlSeconds = '<span>0</span><span>0</span>';
    if (hours > 0) {
        if (hours.toString().length > 1) {
            if (hours.toString().length > 2) {
                htmlHours = '<span>' + hours.toString()[0] + '</span><span>' + hours.toString()[1] + '</span><span>' + hours.toString()[2] + '</span>';
            } else {
                htmlHours = '<span>' + hours.toString()[0] + '</span><span>' + hours.toString()[1] + '</span>';
            }
        } else {
            htmlHours = '<span>0</span><span>' + hours + '</span>';
        }
    }
    if (minutes > 0) {
        if (minutes.toString().length > 1) {
            htmlMinutes = '<span>' + minutes.toString()[0] + '</span><span>' + minutes.toString()[1] + '</span>';

        } else {
            htmlMinutes = '<span>0</span><span>' + minutes + '</span>';
        }
    }
    if (seconds > 0) {
        if (seconds.toString().length > 1) {
            htmlSeconds = '<span>' + seconds.toString()[0] + '</span><span>' + seconds.toString()[1] + '</span>';

        } else {
            htmlSeconds = '<span>0</span><span>' + seconds + '</span>';
        }
    }
//  document.getElementById('d').innerHTML=days;
    if (htmlDays !== '') {
        $(item).find('.days').html(htmlDays + ((!hideLabels && labelD !== undefined && labelD !== '') ? '<div class="time-label">' + labelD + '</div>' : ''));
    }
    $(item).find('.hours').html(htmlHours + ((!hideLabels && labelH !== undefined && labelH !== '') ? '<div class="time-label">' + labelH + '</div>' : ''));
    $(item).find('.minutes').html(htmlMinutes + (!hideLabels && (labelM !== undefined && labelM !== '') ? '<div class="time-label">' + labelM + '</div>' : ''));
    $(item).find('.seconds').html(htmlSeconds + ((!hideLabels && labelS !== undefined && labelS !== '') ? '<div class="time-label">' + labelS + '</div>' : ''));
    if (hours > 0 || minutes > 0 || seconds > 0) {
        var q = setTimeout(function () {
            starttime(item, endTime, labelD, labelH, labelM, labelS, hideLabels);
        }, 500);
    } else {
        if ($(item).attr('page-reload') !== undefined && $(item).attr('page-reload') !== '') {
            if ($(item).attr('page-reload') !== 'self') {
                document.location = $(item).attr('page-reload');
            } else {
                document.location.reload();
            }
        }
    }
}
$(document).on('click', '.getPupUpLink', function () {
    var href = $(this).attr('href');
    var modalTarget = $(this).attr('modal-target');
    if (href !== undefined && href !== '') {
        $.ajax({
            type: 'get',
            url: href,
            success: function (data, textStatus, jqXHR) {
                $(data).appendTo($('body'));
                if (modalTarget !== undefined && modalTarget !== '') {
                    $(modalTarget).modal('show');
                }
            }
        });
    }
    return false;
});
$(document).on('hidden.bs.modal', '.modalRemoveOnHide', function () {
    $(this).remove();
});
function lazyImages() {
    if ($('img[lazy-image]').length > 0) {
        $.each($('img[lazy-image]'), function (i, image) {
            var loadImage = new Image();
            loadImage.onload = function () {
                image.src = this.src;
            }
            loadImage.src = $(image).attr('lazy-image');
//            $(image).attr('src',$(image).attr('lazy-image'));
        });
    }
}
$(document).on('click', '.btn-open-register', function () {
    $('.register-form').addClass('open');
    $('.register-action').addClass('hide');
});
//$(document).on('click','.get-inpost-map',function(){
//    var href=$(this).attr('href');
//    $.ajax({
//        'type':'get',
//        url:href,
//        success: function (data, textStatus, jqXHR) {
//            $(data).appendTo($('body'));
//            $('#inpost-modal').modal('show');
//        }
//    });
//    return false;
//});
//$(document).on('shown.bs.modal','#inpost-modal',function(){
//    inpostSelect();
//});
function inpostSelect() {
    window.easyPackAsyncInit = function () {
        easyPack.init({});
        $(document).on('click', '#get-inpost-map', function () {
            easyPack.modalMap(function (point) {
                this.close();   // Close modal with map, must be called from inside modalMap() callback.
                $('#paczkomat').val(point.name);
                $('#paczkomat-info').val(point.address.line1 + '<br/>' + point.address.line2);
                $('#paczkomat').trigger('change');
                $('#paczkomat-info').trigger('change');
                $('#paczkomat-name').html(point.address.line1 + '<br/>' + point.address.line2);
            }, {width: ($('body').hasClass('mobile') ? ($(window).width() - 40) : 600), height: ($('body').hasClass('mobile') ? ($(window).height() - 60) : 400), useGeolocation: true});
            $($('#widget-modal').parents('div')[0]).addClass('inpostMapBackground');

        });
//        var map = easyPack.mapWidget('easypack-map', function(point) {
//            point.name;
//            point.address_details.building_number;
//            point.address_details.flat_number;
//            point.address_details.city;
//            point.address_details.post_code;
//            point.address_details.province;
//            point.address_details.street;
//            point.address.line1;
//            point.address.line2;
//            $('#paczkomat-id').val(point.name);
//            $('#inpost-modal').modal('hide');
//        });
    };
}
$(document).on('click', 'label.error', function () {
    $(this).remove();
});
$(document).on('keyup', 'input.error,select.error,textarea.error', function () {
    $(this).siblings('label.error').remove();
});
$(document).on('click', 'footer .footer-head', function () {
    if ($('body').hasClass('mobile') && $('body').hasClass('devDesktop')) {
        if ($(this).next('ul.footer-links').hasClass('open')) {
            $(this).removeClass('open');
            $(this).next('ul.footer-links').removeClass('open');
        } else {
            $(this).addClass('open');
            $(this).next('ul.footer-links').addClass('open');
        }

    }
});
$(document).on('touchstart', 'footer .footer-head', function () {
    if ($('body').hasClass('mobile') && $('body').hasClass('devMobile')) {
        if ($(this).next('ul.footer-links').hasClass('open')) {
            $(this).removeClass('open');
            $(this).next('ul.footer-links').removeClass('open');
        } else {
            $(this).addClass('open');
            $(this).next('ul.footer-links').addClass('open');
        }

    }
});
$(document).on('change', '#view-zlotowki-uzyj', function () {
    var url = $(this).attr('data-target');
    if (url !== undefined && url !== '' && $(this).is(':checked')) {
        setActiveBonus(url);
    }
});
$(document).on('click', '#view-zlotowki-uzyj-btn', function () {
    var url = $(this).attr('data-target');
    if (url !== undefined && url !== '') {
        setActiveBonus(url);
    }
});
function setActiveBonus(url) {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: url,
        success: function (data, textStatus, jqXHR) {
            if (data['status'] !== undefined && data['status'] === 'success') {
                if (data['message'] !== undefined && data['message'] !== '') {
                    _success(data['message']);
                }
            } else {
                if (data['message'] !== undefined && data['message'] !== '') {
                    _error(data['message']);
                }
            }
        },
        error: function (data, textStatus, message) {
            if (data.status === 403) {
                _error(getTranslate('errors.wymaganeLogowanie'));
            } else {
                _error(message);
            }
        }
    })
}
$(document).on('click', '.confirmCookie', function () {
    $('#cookieBar').removeClass('open');
    setTimeout(function () {
        $('#cookieBar').remove();
    }, 300);
    var target = $(this).attr('data-target');
    $.ajax({
        type: 'get',
        url: target
    });
});
$(document).on('click', '.home-cat-action', function () {
    if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $('.homeCategory').removeClass('active');
    } else {
        $(this).addClass('active');
        $('.homeCategory').addClass('active');
    }
});
$(document).on('click', 'a[link-type="type_popup"]', function () {
    var url = $(this).attr('href');
    $.ajax({
        type: 'get',
        url: url,
        success: function (data, textStatus, jqXHR) {
            $(data).appendTo('body');
            $('#strona-modal .modal-body').css('max-height', $(window).height() - 130);
            $('#strona-modal').modal('show');

        }
    });
    return false;
});
function backToTop() {
    $('html,body').animate({
        scrollTop: 0
    }, 300);
}
function checkCssMap() {

}

function createMultiselect(item) {
    var SelectAllText = 'Zaznacz wszystko';
    var AllSelectedText = 'Wybrano wszystko';
    var CountSelectedText = 'Wybrano %count% z %total%';
    var NoMatchesFoundText = 'Brak wyników';
    if (!empty($(item).attr('select-all-text'))) {
        SelectAllText = $(item).attr('select-all-text');
    }
    if (!empty($(item).attr('all-selected-text'))) {
        AllSelectedText = $(item).attr('all-selected-text');
    }
    if (!empty($(item).attr('count-selected-text'))) {
        CountSelectedText = $(item).attr('count-selected-text');
    }
    if (!empty($(item).attr('no-matches-found-text'))) {
        NoMatchesFoundText = $(item).attr('no-matches-found-text');
    }
    $(item).multipleSelect({
        width: '100%',
        filter: !empty($(item).attr('data-filter')),
        minimumCountSelected: (!empty($(item).attr('max-count')) ? $(item).attr('max-count') : 3),
        onClose: function () {
            if (!empty($(item).attr('on-change-action'))) {
                var fn = window[$(item).attr('on-change-action')];
                if (typeof fn === "function")
                {
                    fn(item);
                }
            }
        },
        onOpen: function () {
            multiCategoriesSelected = $(item).val();
        },
        formatSelectAll: function () {
            return SelectAllText;
        },
        formatAllSelected: function () {
            return AllSelectedText;
        },
        formatCountSelected: function (count, total) {
            return CountSelectedText.replaceAll('%count%', count).replaceAll('%total%', total);
        },
        formatNoMatchesFound: function () {
            return NoMatchesFoundText;
        },

    });
}
var registerXhr;
$(document).on('submit', '#modal-register form', function () {
    var form = this;
    var btn = $(form).find('button[type=submit]');
    if (btn.hasClass('reload')) {
        return false;
    }
    var url = $(form).attr('action');
    var data = $(form).serialize();
    btn.addClass('reload');
    if (registerXhr !== undefined) {
        registerXhr.abort();
    }
    registerXhr = $.ajax({
        type: 'POST',
        dataType: 'json',
        url: url,
        data: data,
        success: function (data, textStatus, jqXHR) {
            formErrorRemove($(form));
            if (!empty(data['status']) && data['status'] === 'success') {
                if (!empty(data['message'])) {
                    _success(data['message']);
                }
                if (!empty(data['redirect'])) {
                    document.location = data['redirect'];
                }
            } else {
                if (!empty(data['message'])) {
                    _error(data['message']);
                }
                if (!empty(data['errors'])) {
                    $.each(data['errors'], function (filedName, errors) {
                        var input = $(form).find('[name="' + filedName + '"]');
                        $.each(errors, function (errType, message) {
                            inputError(input, message);
                        });
                    });
                    console.log($(form).find(':input.error'));
                    if ($(form).find(':input.error').length > 0) {
                        $($(form).find(':input.error')[0]).focus();
                    }
                }
            }
            btn.removeClass('reload');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            _error(textStatus);
            btn.removeClass('reload');
        }
    });
    return false;
});
function inputError(input, message) {
    if (input.hasClass('error')) {
        inputErrorRemove(input);
    }
    input.addClass('error');
    if (input.parents('.form-group').length > 0) {
        $(input.parents('.form-group')[0]).addClass('error');
    }
    $('<label class="error">' + message + '</label>').insertAfter(input);
}
function inputErrorRemove(input) {
    if (input.parents('.form-group.error').length > 0) {
        $(input.parents('.form-group.error')[0]).removeClass('error');
    }
    input.siblings('label.error').remove();
    input.removeClass('error');
}
function formErrorRemove(form) {
    form.find('label.error').remove();
    form.find('.error').removeClass('error');
}
$(document).on('change', '.form-group.error input,.form-group.error select,.form-group.error textarea,input.error,select.error,textarea.error', function () {
    inputErrorRemove($(this));
});
$(document).on('change', '.pagination .goToPage', function () {
    var baseUrl = $(this).attr('data-url');
    var hasQuery = baseUrl.indexOf('?');
    paginGoTo($(this).val(),this, baseUrl, hasQuery);
    return false;
});
$(document).on('keypress', '.pagination .goToPage', function (e) {
    var charCode = e.which || e.keyCode;
    if (charCode == 13) {
        var baseUrl = $(this).attr('data-url');
        var hasQuery = baseUrl.indexOf('?');
        paginGoTo($(this).val(),this, baseUrl, hasQuery);
        return false;
    }
});
function paginGoTo(page, input, baseUrl, hasQuery) {
    if (!empty(baseUrl)) {
        if (!empty($(input).attr('max'))) {
            var max = parseInt($(input).attr('max'));
            var val = parseInt(page);
            if (val > max) {
                page = max;
            }
        }
        if (!empty($(input).attr('min'))) {
            var min = parseInt($(input).attr('min'));
            var val = parseInt(page);
            if (val < min) {
                page = min;
            }
        }
        if (isNaN(page)) {
            page = 0;
        }
        if (page > 0) {
            if (hasQuery >= 0) {
                baseUrl = baseUrl + '&page=' + page;
            } else {
                baseUrl = baseUrl + '?page=' + page;
            }
        }
        document.location = baseUrl;
    }
}