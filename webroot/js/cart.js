var spinChangeTimeOut;
var kosztyKraje;
var kosztyKrajeDef;
var krajDef;
$(document).ready(function () {

    iloscSpiner();
    checkCartFvChecked();
    checkWysylkaKraj();
    if ($('.sumary-total-to-pay > #cart-total-to-pay,#cart-total-to-pay-top').length > 0) {
        sumTotal();
    }
    if ($('#cart-form').length > 0) {
        cartValidation();
    }
    if ($('input[other-req]').length > 0) {
        $('input[other-req]').trigger('change');
    }
    if ($('#towar-view-kolor').length > 0) {
        $('#towar-view-kolor').trigger('change');
    }
    checkWysylkaChecked();
});
function iloscSpiner() {
    $(".item-ilosc-input").spinner({
        icons: {down: "custom-down-icon", up: "custom-up-icon"},
        min: 0,
        change: function (event, ui) {
            $(event.currentTarget).trigger('change');
        },
        stop: function (event, ui) {
            spinChangeTimeOut = setTimeout(function () {
                $(event.target).trigger('change');
            }, 500);
        },
        start: function () {
            if (spinChangeTimeOut !== undefined) {
                clearTimeout(spinChangeTimeOut);
            }
        }

    });
}
function checkWysylkaChecked() {
    if ($('.rodzajePlatnosciContainer ul.dostawa li > input:checked').length > 0) {
        $('.rodzajePlatnosciContainer ul.dostawa li > input:checked').trigger('change');
    }
}
$(document).on('change', '.version-ordered-box', function () {
    var val = parseInt($(this).val());
    if (isNaN(val)) {
        val = 0;
    }
    if (val < 0) {
        val = 0;
        $(this).val(val);
    }
    var itemPrice = parseFloat($(this).attr('item-price'));
    var inBox = parseInt($(this).attr('in-box'));
    var parent = $($(this).parents('tr')[0]);
    parent.find('.variant-total-price').html(currency((val * (itemPrice * inBox))));
});
$(document).on('keyup', '.version-ordered-box', function () {
    $(this).trigger('change');
});
$(document).on('click', '.add-to-cart', function () {
    var btn = $(this);
    var asGratis = $(this).attr('as-gratis');
    if(asGratis!==undefined && asGratis==='true'){
        asGratis=true;
    }else{
        asGratis=false;
    }
    if (btn.hasClass('reload')) {
        return false;
    }
    var zestaw = $(this).attr('zestaw-id');
    if (zestaw !== undefined && zestaw !== '') {
        addZestaw(zestaw, btn);
        return false;
    }
    var variants = $($(this).parents('.add-to-cart-container')[0]).find('.item-ilosc-input');
    var variantSelected = new Object();
    if (!variants.length > 0) {
        variantSelected[btn.attr('item-id')] = 1;
    } else {
        $.each(variants, function (i, v) {
            if ($(v).val() === '' && !($(v).attr('version-id') !== undefined && $(v).attr('version-id') !== '')) {
                variantSelected[$(v).attr('item-id')] = 1;
            } else if ($(v).val() !== '0') {
                if ($(v).val() === '') {
                    $(v).val(1);
                }
                variantSelected[$(v).attr('item-id') + (($(v).attr('version-id') !== undefined && $(v).attr('version-id') !== '') ? '_' + $(v).attr('version-id') : '')] = parseInt($(v).val());
            }
        });
    }
    if (Object.size(variantSelected) > 0) {
        console.log('addToCart', variantSelected);
        addToCart(btn.attr('item-id'), variantSelected, btn,asGratis);
    } else {
        _error(btn.attr('error-message'));
    }
    return false;
});
$(document).on('click', '.add-to-cart-bonus', function () {
    var cardId = $(this).attr('card-id');
    var btn = $(this);
    if (cardId !== undefined && cardId !== '') {
        var urlAction = '';
        if ($(this).attr('add-action') !== undefined && $(this).attr('add-action') !== '') {
            urlAction = $(btn).attr('add-action');
        } else {
            urlAction = basePath + 'cart/card-add/' + cardId;
        }
        if (btn !== undefined) {
            btn.addClass('reload');
        }
        addToCartXhr = $.ajax({
            type: 'post',
            dataType: 'json',
            url: urlAction,
            data: 'cardId=' + cardId,
            success: function (data, textStatus, jqXHR) {
                if (btn !== undefined) {
                    btn.removeClass('reload');
                }
                if (data['status'] !== undefined && data['status'] === 'success') {
                    addToCartSuccess(data);
                } else {
                    if (data['message'] !== undefined && data['message'] !== '') {
                        _error(data['message']);
                    }
                }
            }
        });
    }
    return false;
});
$(document).on('click', '.cart-form *:not(.add-to-cart)', function () {
    return false;
});
var addToCartXhr;
function setItemVersion(id, name) {
    $('.item-ilosc-input').attr('version-id', id);
    $('.version-label').html(name);
    if ($('.view-thumb[version-id=' + id + ']').length > 0) {
        $('.view-thumb[version-id=' + id + ']').trigger('click');
    }
    return false;
}
function addToCartSuccess(data) {
    $('.cartInfo').html(data['cartItems']['cart-info']);
    if(!empty(data['cartItems']['to_free_delivery'])){
        $('#nav-free-delivery').html(data['cartItems']['to_free_delivery']);
    }
    if (data['html'] !== undefined) {
        if (data['returnTr'] !== undefined && data['returnTr'] !== false) {
            var addItemKey = $(data['html']).attr('item-key');
            if ($('.koszyk-table tbody tr[item-key="' + addItemKey + '"]').length > 0) {
                var iloscInput = $('.koszyk-table tbody tr[item-key="' + addItemKey + '"]').find('.item-ilosc-input');
                iloscInput.val($(data['html']).find('.item-ilosc-input').val());
                iloscInput.attr('aria-valuenow', iloscInput.val());
            } else {
                $(data['html']).appendTo($('.koszyk-table tbody'));
            }
            cartAction(data);
            $('html,body').animate({
                scrollTop: $('.koszyk-table tbody tr[item-key="' + addItemKey + '"]').offset().top
            }, 300);
        } else {
            $(data['html']).appendTo('body');
            $('#cart-modal').modal('show');
        }
    }
    if (!$('body').hasClass('mobile') && data['cartItems']['preview'] !== undefined && data['cartItems']['preview'] !== '') {
        if ($('.goToCart #cartPreview').length > 0) {
            $('.goToCart #cartPreview').remove();
        }
        $(data['cartItems']['preview']).appendTo($('.goToCart'));
    }
    iloscSpiner();
}
function addToCart(itemId, variants, btn,asGratis) {
    if (itemId === undefined) {
        return false;
    }
    if (asGratis === undefined) {
        asGratis = false;
    }
    var urlAction = '';
    if (btn !== undefined && $(btn).attr('add-action') !== undefined && $(btn).attr('add-action') !== '') {
        urlAction = $(btn).attr('add-action');
    } else {
        urlAction = basePath + 'cart/add/' + itemId;
    }
    if (variants === undefined) {
        variants = new Object();
    }
    if(asGratis){
        variants['gratis']=true;
    }
    if (addToCartXhr !== undefined) {
        addToCartXhr.abort();
        $('.add-to-cart').removeClass('reload');
    }
    if (btn !== undefined) {
        btn.addClass('reload');
    }
    addToCartXhr = $.ajax({
        type: 'post',
        dataType: 'json',
        url: urlAction,
        data: variants,
        success: function (data, textStatus, jqXHR) {
            if (btn !== undefined) {
                btn.removeClass('reload');
            }
            if (data['status'] !== undefined && data['status'] === 'success') {
                addToCartSuccess(data);
            } else {
                if (data['message'] !== undefined && data['message'] !== '') {
                    _error(data['message']);
                }
            }
        }
    });
}
function addZestaw(id, btn)
{
    var data = new Object();
    data['zetaw_id'] = id;
    data['towary'] = new Array();
    var zestawIlosc = 1;
    if($('.zestaw-ilosc[zestaw-id="'+id+'"]').length > 0){
        zestawIlosc=parseInt($('.zestaw-ilosc[zestaw-id="'+id+'"]').val());
        if(isNaN(zestawIlosc) || zestawIlosc < 1){
            zestawIlosc=1;
        }
    }
    data['ilosc']=zestawIlosc;
    var zestaw_items = $('#zestaw_' + id + ' input[type=hidden]');
    $.each(zestaw_items, function (index, value) {
        data['towary'][index] = $(value).val();
    });
    $.ajax({
        type: 'post',
        url: basePath + 'cart/add-zestaw/' + id,
        data: data,
        dataType: 'json',
        success: function (data) {
            if (btn !== undefined) {
                btn.removeClass('reload');
            }
            if (data['status'] !== undefined && data['status'] === 'success') {
                $('.cartInfo').html(data['cartItems']['cart-info']);
    if(!empty(data['cartItems']['to_free_delivery'])){
        $('#nav-free-delivery').html(data['cartItems']['to_free_delivery']);
    }
                if (data['html'] !== undefined) {
                    $(data['html']).appendTo('body');
                    $('#cart-modal').modal('show');
                }
                if (data['cartItems']['preview'] !== undefined && data['cartItems']['preview'] !== '') {
                    if ($('.goToCart #cartPreview').length > 0) {
                        $('.goToCart #cartPreview').remove();
                    }
                    $(data['cartItems']['preview']).appendTo($('.goToCart'));
                }
                iloscSpiner();
                if (data['error-message'] !== undefined && data['error-message'] !== '') {
                    _error(data['error-message']);
                }
            } else {
                if (data['message'] !== undefined && data['message'] !== '') {
                    _error(data['message']);
                }
            }
        }
    });
}
$(document).on('hidden.bs.modal', '#cart-modal', function () {
//    $('.item-ilosc-input').val('');
    $('#cart-modal').remove();
});
$(document).on('click', '.remove-from-cart', function () {

    var itemTr = $($(this).parents('tr')[0]);
    var itemKey = $(this).attr('item-key');
    var zestawKey = $(this).attr('zestaw-key');
    var cfMessage = $(this).attr('confirm-message');
    var yesBtn = (($(this).attr('confirm-btn-yes') !== undefined) ? $(this).attr('confirm-btn-yes') : 'Tak');
    var noBtn = (($(this).attr('confirm-btn-no') !== undefined) ? $(this).attr('confirm-btn-no') : 'Nie');
    var _data = '';
    if (zestawKey !== undefined && zestawKey !== '') {
        _data = 'z=' + zestawKey;
    } else {
        _data = 'i=' + itemKey;
    }
    if (cfMessage !== undefined && cfMessage !== '') {
        swal({
            title: '',
            text: cfMessage,
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: yesBtn,
            cancelButtonText: noBtn,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function () {
            $.ajax({
                type: 'get',
                data: _data,
                url: basePath + 'cart/remove',
                dataType: 'json',
                success: cartAction
            });
        }, function (dismiss) {
            // dismiss can be 'cancel', 'overlay',
            // 'close', and 'timer'
            if (dismiss === 'cancel') {

            }
        });
    } else {
        $.ajax({
            type: 'get',
            data: _data,
            url: basePath + 'cart/remove',
            dataType: 'json',
            success: cartAction
        });
    }
    return false;
});
$(document).on('click', '.remove-all-from-cart', function () {

    var cfMessage = $(this).attr('confirm-message');
    var yesBtn = (($(this).attr('confirm-btn-yes') !== undefined) ? $(this).attr('confirm-btn-yes') : 'Tak');
    var noBtn = (($(this).attr('confirm-btn-no') !== undefined) ? $(this).attr('confirm-btn-no') : 'Nie');
    var _data = '';
    swal({
        title: '',
        text: cfMessage,
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: yesBtn,
        cancelButtonText: noBtn,
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {
        $.ajax({
            type: 'get',
            data: _data,
            url: basePath + 'cart/remove-all',
            dataType: 'json',
            success: cartAction
        });
    }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if (dismiss === 'cancel') {

        }
    });
    return false;
});
$(document).on('change', '.cart-ordered-box', function () {
    var itemKey = $(this).attr('item-key');
    var zestawKey = $(this).attr('zestaw-key');
    var val = parseInt($(this).val());
    if (isNaN(val)) {
        val = 0;
    }
    if (val < 0) {
        val = 0;
    }
    var _data = '';
    if (zestawKey !== undefined && zestawKey !== '') {
        _data = 'z=' + zestawKey + '&v=' + val;
    } else {
        _data = 'i=' + itemKey + '&v=' + val;
    }
    $.ajax({
        type: 'get',
        data: _data,
        url: basePath + 'cart/update',
        dataType: 'json',
        success: cartAction
    });
});
function cartAction(data) {
    if (data['cartItems'] !== undefined && data['cartItems'] !== null) {
        $('#cartItemsCount').html(data['cartItems']['total_items']);
        $('#cartItemsValue').html(data['cartItems']['total_value']);
//        $('#cartItemsValue').html(currency(data['cartItems']['total_value']));
        $('#cartTotalNetto').html(currency(data['cartItems']['total_netto']));
        $('#cartTotalBrutto').html(currency(data['cartItems']['total_z_rabatem']));
        $('#totalBruttonIn').val(data['cartItems']['total_z_rabatem']);
        if (data['cartItems']['rabat'] !== undefined && data['cartItems']['rabat'] > 0) {
            $('#cartTotalNettoRabat').html(currency(data['cartItems']['total_netto_rabat']));
            $('#cartTotalBruttoRabat').html(currency(data['cartItems']['total_z_rabatem']));
            $('#cartDiscountValue').html(data['cartItems']['rabat']);
            $('#cartRabatRow').removeClass('hidden');
            $('#cart-total-to-pay').html(currency(data['cartItems']['total_z_rabatem']));
            $('#cart-total-to-pay-top').html(currency(data['cartItems']['total_z_rabatem']));
        } else {
            $('#cartTotalNettoRabat').html(currency(data['cartItems']['total_netto']));
            $('#cartTotalBruttoRabat').html(currency(data['cartItems']['total_z_rabatem']));
            $('#cartDiscountValue').html(0);
            $('#cartRabatRow').addClass('hidden');
            $('#cart-total-to-pay').html(currency(data['cartItems']['total_z_rabatem']));
            $('#cart-total-to-pay-top').html(currency(data['cartItems']['total_z_rabatem']));
        }
        if (data['cartItems']['items'] !== undefined) {
            $.each(data['cartItems']['items'], function (key, item) {
                if ($('tr[item-key="' + key + '"]').length > 0 && key!=='gratis') {
                    $('tr[item-key="' + key + '"] .discountPrice').html(currency(item['cena_z_rabatem']));
                    $('tr[item-key="' + key + '"] .discountValue').html(item['item_rabat']);
                    if (item['cena_razem_z_rabatem'] !== undefined) {
                        $('tr[item-key="' + key + '"] .cartItemBrutto').html(currency(item['cena_razem_z_rabatem']));
                    } else {
                        $('tr[item-key="' + key + '"] .cartItemBrutto').html(currency(item['total_brutto']));
                    }
                }
            });
        }
        $('.cartInfo').html(data['cartItems']['cart-info']);
    if(!empty(data['cartItems']['to_free_delivery'])){
        $('#nav-free-delivery').html(data['cartItems']['to_free_delivery']);
    }
        if (data['cartItems']['platnosci_html'] !== undefined && $('#orderFormContainer').length > 0) {
            $('#orderFormContainer').html(data['cartItems']['platnosci_html']);
            checkWysylkaKraj();
            checkWysylkaChecked();
        }
        sumTotal();
        iloscSpiner();
    } else {
        
        $('#cartItemsCount').html('0');
        $('#cartItemsValue').html('0');
        document.location.reload();
    }
    if (data['error-message'] !== undefined && data['error-message'] !== '') {
        _error(data['error-message']);
    }
    if (data['action'] !== undefined) {
        switch (data['action']) {
            case 'remove':
                {
                    if (data['zestawKey'] !== undefined) {
                        $('tr[zestaw-key=' + data['zestawKey'] + ']').fadeOut('fast', function () {
                            $('tr[zestaw-key=' + data['zestawKey'] + ']').remove();
                        });
                    } else {
                        $('tr[item-key=' + data['itemKey'] + ']').fadeOut('fast', function () {
                            $('tr[item-key=' + data['itemKey'] + ']').remove();
                        });
                    }
                    if(data['cartItems']['remove_gratis']!==undefined && data['cartItems']['remove_gratis']===true){
                        $('tr[item-key=gratis]').fadeOut('fast', function () {
                            $('tr[item-key=gratis]').remove();
                        });
                    }
                }
                break;
            case 'update':
                {
                    if (data['zestawKey'] !== undefined) {
                        if (data['maxIlosc'] !== undefined && data['maxIlosc'] > 0) {
                            $('tr[zestaw-key=' + data['zestawKey'] + '] .item-ilosc-input').val(data['maxIlosc']);
                        }
                    }
                    if(data['cartItems']['remove_gratis']!==undefined && data['cartItems']['remove_gratis']===true){
                        $('tr[item-key=gratis]').fadeOut('fast', function () {
                            $('tr[item-key=gratis]').remove();
                        });
                    }
//                $('tr[item-key='+data['itemKey']+'] .cartItemNetto').html(currency(data['cartItems']['items'][data['itemKey']]['cena_razem_netto']));
//                $('tr[item-key='+data['itemKey']+'] .cartItemBrutto').html(currency(data['cartItems']['items'][data['itemKey']]['cena_razem_brutto']));
                }
                break;
            case 'updateError':
                {
                    $('.item-ilosc-input[item-key="' + data['itemKey'] + '"]').val(data['cartItems']['items'][data['itemKey']]['ilosc']);
                    $('.item-ilosc-input[item-key="' + data['itemKey'] + '"]').attr('aria-valuenow', data['cartItems']['items'][data['itemKey']]['ilosc']);
                    if (data['message'] !== undefined && data['message'] !== '') {
                        _error(data['message']);
                    }
                }
                break;
        }
    }
}
$(document).on('change', '#adres-wysylki', function () {
    $('.address-label').addClass('hidden');
    $('.address-label[address-id=' + $(this).val() + ']').removeClass('hidden');
});
$(document).on('change', '#faktura,input[name=dane_do_fv]', function () {
    checkCartFvChecked();
});
function checkCartFvChecked() {
    if ($('#faktura').is(':checked')) {
        $('#chceFvCheck').addClass('active');
        var checkDane = $('input[name=dane_do_fv]:checked');
        if (checkDane.val() === 'inne') {
            $('.fv-other').addClass('active');
            $('.fv-other input[tmp-req=true], .fv-other select[tmp-req=true]').attr('required', 'required');
            $('.fv-other input, .fv-other select').removeAttr('disabled');
        } else {
            $('.fv-other').removeClass('active');
            $('.fv-other input, .fv-other select').removeAttr('required');
            $('.fv-other input, .fv-other select').attr('disabled', 'disabled');
        }
    } else {
        $('#chceFvCheck').removeClass('active');
        $('.fv-other').removeClass('active');
        $('.fv-other input, .fv-other select').removeAttr('required');
        $('.fv-other input, .fv-other select').attr('disabled', 'disabled');
    }
}
var setPlatnoscXhr;
$(document).on('change', '.check-wysylka > input', function () {
    var checkItem = $('input[name=wysylka]:checked').parents('.check-wysylka')[0];
    $('.check-wysylka').removeClass('checked');
    $(checkItem).addClass('checked');
    $('.check-platnosc').removeClass('checked');
    $('.check-platnosc[platnosc-id=' + $(checkItem).attr('platnosc-id') + ']').addClass('checked');
    var dane = new Object();
    dane['rodzaj'] = $(checkItem).attr('platnosc-id');
    dane['wysylka'] = $(checkItem).attr('wysylka-id');
    sumTotal();
    if (setPlatnoscXhr !== undefined) {
        setPlatnoscXhr.abort();
    }
    setPlatnoscXhr = $.ajax({
        type: 'POST',
        data: dane,
        dataType: 'json',
        url: basePath + 'cart/set-platnosc',
        success: function (data, textStatus, jqXHR) {

        }
    });
});
$(document).on('click', '.platnosc-item', function () {
    var checkItem = $(this).find('.check-platnosc');
    var lastChecked = $('.check-wysylka.checked');
    $('.check-platnosc').removeClass('checked');
    checkItem.addClass('checked');
    if (lastChecked.length > 0 && $('.check-wysylka[platnosc-id=' + checkItem.attr('platnosc-id') + '][wysylka-id=' + lastChecked.attr('wysylka-id') + ']').length > 0) {
        $('.check-wysylka[platnosc-id=' + checkItem.attr('platnosc-id') + '][wysylka-id=' + lastChecked.attr('wysylka-id') + ']').find('input').trigger('click');
    } else {
        $($('.check-wysylka[platnosc-id=' + checkItem.attr('platnosc-id') + ']')[0]).find('input').trigger('click');
    }
});

$(document).on('change', '.rodzajePlatnosciContainer ul li > input', function () {
    var price = 0;
    if ($(this).parents('ul').hasClass('error'))
        $(this).parents('ul').removeClass('error');
    $.each($('.rodzajePlatnosciContainer ul li > input[koszt-dodatkowy=1]'), function (index, value) {
        var koszt = ((kosztyDodatkowe !== undefined && kosztyDodatkowe[$('.rodzajePlatnosciContainer ul li > input[name=wysylka]:checked').val()] !== undefined) ? kosztyDodatkowe[$('.rodzajePlatnosciContainer ul li > input[name=wysylka]:checked').val()] : 0);
        $(value).attr('price', koszt);
        $(value).siblings('label').find('span').html(currency(koszt));
    });
    $.each($('.rodzajePlatnosciContainer ul li > input:checked'), function (index, value) {
        if ($(value).attr('darmowa-wysylka') !== undefined && (parseFloat($(value).attr('darmowa-wysylka')) > 0) && (parseFloat($('#wartosc_koszyka').attr('value')) >= parseFloat($(value).attr('darmowa-wysylka'))))
        {
            price = parseFloat($(value).attr('price'));
        } else {
            if ($(value).attr('def-price') !== undefined)
                price = price + parseFloat($(value).attr('def-price'));
            else
                price = price + parseFloat($(value).attr('price'));
        }
    });
    $('#dostawa_platnosc').attr('value', price);
    $('#dostawa_platnosc').html(currency((price)));
    var wartosc_koszyka = parseFloat($('#wartosc_koszyka').attr('value'));
    if ($('.koszyk-promocja-item[typ=rabat]').length > 0) {
        $.each($('.koszyk-promocja-item[typ=rabat]'), function (pi, pv) {
            var pWartosc = parseFloat($(pv).attr('value'));
            var pRabat = parseFloat($(pv).attr('rabat'));
            if (!isNaN(pWartosc) && !isNaN(pRabat) && pWartosc > 0 && pRabat > 0) {
                var pRValue = round(pWartosc * (pRabat / 100), 2);
                wartosc_koszyka = wartosc_koszyka - pRValue;
            }
        });
    }
    $('#wartosc_koszyka').html(currency(wartosc_koszyka));
    var razem = ((price + wartosc_koszyka));

    $('#prowizja_platnosc').attr('value', 0);
    $('#prowizja_platnosc').html(currency(0));
    $('#prowizja_platnosc_div').hide();

    $('#wartosc_razem').html(currency((razem)));
    var item = '';
    if ($(this).attr('name') == 'wysylka')
    {
        if ($(this).attr('paczkomat') !== undefined && $(this).attr('paczkomat') === 'true') {
            $('#paczkomat-container').addClass('open');
            $('[paczkomat-not-required=true]').removeAttr('required');
        } else {
            $('#paczkomat-container').removeClass('open');
            $('[paczkomat-not-required=true]').attr('required', 'required');
        }
        $('#punkty-odbioru-container .punkt-container').removeClass('open');
        if ($('#punkt-container-' + $(this).val()).length > 0) {
            $('#punkty-odbioru-container').addClass('open');
            $('#punkt-container-' + $(this).val() + ' input[type=radio]').removeAttr('disabled');
            $('#punkt-container-' + $(this).val()).addClass('open');
            $('.show-on-odbior').removeClass('hidden');
            $('.hide-on-odbior,[hide-on-odbior=true]').addClass('hidden');
            $('[disable-on-odbior=true]').attr('disabled', 'disabled');
        } else {
            $('#punkty-odbioru-container').removeClass('open');
            $('#punkty-odbioru-container .punkt-container input[type=radio]').attr('disabled', 'disabled');
            if ($(this).attr('e-wysylka') !== undefined && $(this).attr('e-wysylka') === 'true') {
                $('.hide-on-odbior,[hide-on-odbior=true]').addClass('hidden');
                $('[disable-on-odbior=true]').attr('disabled', 'disabled');
            } else {
                $('[disable-on-odbior=true]').removeAttr('disabled');
                $('.hide-on-odbior,[hide-on-odbior=true]').removeClass('hidden');
            }
            $('.show-on-odbior').addClass('hidden');
        }
        item = 'wysylka_id';
        $('ul.platnosc > li').addClass('disabled');
        $('ul.platnosc > li.' + $(this).attr('id')).removeClass('disabled');

        if ($('ul.platnosc > li.disabled input:checked').length > 0) {
            $('ul.platnosc > li:not(.disabled) input[type=radio]')[0].checked = true;
            $('ul.platnosc > li:not(.disabled) input:checked').trigger('change');
            return true;
        }
    }
    if ($(this).attr('name') == 'platnosc')
    {
        item = 'rodzaj_platnosci_id';
//        $('ul.dostawa > li').addClass('disabled');
//        $('ul.dostawa > li.' + $(this).attr('id')).removeClass('disabled');
        if ($('ul.dostawa > li.disabled input:checked').length > 0) {
            $('ul.dostawa > li:not(.disabled) input[type=radio]')[0].checked = true;
            $('ul.dostawa > li:not(.disabled) input:checked').trigger('change');
            return true;
        }
    }
    sumTotal();
    if (item !== '') {
        if (xhrPlatnoscWysylka !== undefined) {
            xhrPlatnoscWysylka.abort();
        }
        xhrPlatnoscWysylka = $.ajax({type: 'post', data: {'rodzaj': $('ul.platnosc > li input:checked').val(), 'wysylka': $('ul.dostawa > li input:checked').val()}, url: '/cart/set-platnosc/'});
    }
});
var xhrPlatnoscWysylka;

function sumTotal() {
    if ($('.rodzajePlatnosciContainer ul li > input:checked').length > 1) {
        var wysylkaVal = parseFloat($('.rodzajePlatnosciContainer ul.dostawa li > input:checked').attr('price'));
        var totalBrutto = parseFloat($('input#totalBruttonIn').val());
        var platnoscVal = parseFloat($('.rodzajePlatnosciContainer ul.platnosc li > input:checked').attr('price'));
        var sum = totalBrutto;
        if ($('#promocja-kod-value').length > 0) {
            var rabat = parseFloat($('#promocja-kod-value').val());
            var rabatTyp = $('#promocja-kod-typ').val();
            if (rabat > 0) {
                if (rabatTyp === 'kwota') {
                    sum = sum - rabat;
                } else {
                    sum = sum - (sum * (rabat / 100));
                }
            }
        }
        sum = sum + wysylkaVal + +platnoscVal;
        if ($('#bonusowe-zlotowki').is(':checked')) {
            var bonus = parseFloat($('#bonusowe-zlotowki').val());
            sum = sum - bonus;
            if (sum < 0) {
                sum = 0;
            }
        }
        $('.sumary-total-to-pay > #cart-total-to-pay').html(currency(sum));
        $('#cart-total-to-pay-top').html(currency(sum));
//        $('#cart-submit').removeClass('disabled');
        $('.rodzajePlatnosciContainer').removeClass('error');
    } else {
        var totalBrutto = parseFloat($('input#totalBruttonIn').val());
        var sum = totalBrutto;
        if ($('#promocja-kod-value').length > 0) {
            var rabat = parseFloat($('#promocja-kod-value').val());
            var rabatTyp = $('#promocja-kod-typ').val();
            if (rabat > 0) {
                if (rabatTyp === 'kwota') {
                    sum = sum - rabat;
                } else {
                    sum = sum - (sum * (rabat / 100));
                }
            }
        }
        if ($('#bonusowe-zlotowki').is(':checked')) {
            var bonus = parseFloat($('#bonusowe-zlotowki').val());
            sum = sum - bonus;
            if (sum < 0) {
                sum = 0;
            }
        }
        $('.sumary-total-to-pay > #cart-total-to-pay').html(currency(sum));
        $('#cart-total-to-pay-top').html(currency(sum));
//        $('#cart-submit').addClass('disabled');
    }
}
$(document).on('change', '#bonusowe-zlotowki', function () {
    sumTotal();
});
function cartValidation() {
    $('#cart-form').validate({
        rules: {
            email: {
                required: true
            },
            telefon: {
                required: true,
                phonePL: true
            },
            wysylka_nip: {
                nip: true
            },
            wysylka_kod: {
                postalcodePL: (($('#wysylka-kod').attr('required') !== undefined) ? true : false)
            },
            faktura_nip: {
                nip: true
            },
            faktura_kod: {
                postalcodePL: true
            },
            nip: {
                nip: true
            }
        },
        messages: {
            telefon: {
                phonePL: getTranslate('validate.PodajPoprawnyNrTel', false)
            },
            wysylka_nip: {
                nip: getTranslate('validate.PodajPoprawnyNrNIP', false)
            },
            wysylka_kod: {
                postalcodePL: getTranslate('validate.PodajPoprawnyKodPocztowy', false)
            },
            faktura_nip: {
                nip: getTranslate('validate.PodajPoprawnyNrNIP', false)
            },
            faktura_kod: {
                postalcodePL: getTranslate('validate.PodajPoprawnyKodPocztowy', false)
            },
            nip: {
                nip: getTranslate('validate.PodajPoprawnyNrNIP', false)
            }
        },
        showErrors: function (errorMap, errorList) {
            $.each(errorList, function (i, v) {
                $($(v.element).parents('.form-group')[0]).addClass('error');
            });
            this.defaultShowErrors();
        },
        unhighlight: function (element, errorClass, validClass) {
            $($(element).parents('.form-group')[0]).removeClass('error');
        }
    });
}
$(document).on('submit', '#cart-form', function () {
    if (!($('.rodzajePlatnosciContainer ul li > input:checked').length > 1)) {
        $('.rodzajePlatnosciContainer').addClass('error');
        _error(getTranslate('koszyk.WybierzRodzajDostawy'));
        $('html,body').animate({
            scrollTop: $('.rodzajePlatnosciContainer').offset().top - 50
        }, 300);
        return false;
    }
    if ($('#punkt-container-' + $('.rodzajePlatnosciContainer ul li > input[name="wysylka"]:checked').val()).length > 0 && !($('#punkt-container-' + $('.rodzajePlatnosciContainer ul li > input[name="wysylka"]:checked').val() + ' input[type=radio][name="punkt_odbioru_id"]:checked').length > 0)) {
        $('#punkt-container-' + $('.rodzajePlatnosciContainer ul li > input[name="wysylka"]:checked').val()).addClass('error');
        _error(getTranslate('koszyk.WybierzPunktOdbioru'));
        $('html,body').animate({
            scrollTop: $('#punkt-container-' + $('.rodzajePlatnosciContainer ul li > input[name="wysylka"]:checked').val()).offset().top - 50
        }, 300);
        return false;
    } else if ($('#punkt-container-' + $('.rodzajePlatnosciContainer ul li > input[name="wysylka"]:checked').val()).length > 0 && $('#punkt-container-' + $('.rodzajePlatnosciContainer ul li > input[name="wysylka"]:checked').val() + ' input[type=radio][name="punkt_odbioru_id"]:checked').length > 0) {
        var pktOdbioruVal = $('#punkt-container-' + $('.rodzajePlatnosciContainer ul li > input[name="wysylka"]:checked').val() + ' input[type=radio][name="punkt_odbioru_id"]:checked').val();
        $('<input type="hidden" name="punkty_odbioru_id" value="' + pktOdbioruVal + '"/>').appendTo($(this));
    }
    $('body').addClass('reloadPage');
});
$(document).on('click', '#cart-submit-top', function () {
    if (!($('.rodzajePlatnosciContainer ul li > input:checked').length > 1)) {
        $('.rodzajePlatnosciContainer').addClass('error');
        _error(getTranslate('koszyk.WybierzRodzajDostawy'));
        $('html,body').animate({
            scrollTop: $('.rodzajePlatnosciContainer').offset().top - 50
        }, 300);
        return false;
    }
    if (!$('.order-form-container').hasClass('open')) {
        $('.cart-container').addClass('hide');
        $('.order-form-container').animate({
            height: 1371
        }, 300, function () {
            $('.order-form-container').addClass('open');
            $('.order-form-container').removeAttr('style');
            $('html,body').animate({
                scrollTop: $('.order-form-container').offset().top
            }, 300);
        });
    } else {
        $('html,body').animate({
            scrollTop: $('.order-form-container').offset().top
        }, 300);
    }
});
$(document).on('click', '#cart-back', function () {

    $('.cart-container').removeClass('hide');
    $('.order-form-container').animate({
        height: 0
    }, 300, function () {
        $('.order-form-container').removeClass('open');
        $('.order-form-container').removeAttr('style');
        $('html,body').animate({
            scrollTop: $('.cart-container').offset().top
        }, 300);
    });

});
$(document).on('click', '#submit-order-a', function () {
    $('html,body').css('overflow', 'hidden');
    $('.submitInProgres').addClass('active');
});
var kodXhr;
$(document).on('click', '.kod-rabatowy-btn', function () {
    var dane = new Object();
    dane['kod'] = $('.kod-rabatowy').val();
    if (dane['kod'] !== '') {
        if (kodXhr !== undefined) {
            kodXhr.abort();
        }
        kodXhr = $.ajax({
            type: 'POST',
            data: dane,
            dataType: 'json',
            url: basePath + 'cart/check-code',
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                if (data['status'] !== undefined && data['status'] === 'success') {
                    $('.rabatKodVal').remove();
                    if (data['valueHtml'] !== undefined && data['valueHtml'] !== '') {
                        $(data['valueHtml']).appendTo($('#produkty-sum'));
                    }
                    if (data['message'] !== undefined && data['message'] !== '') {
                        _success(data['message']);
                    }
                    sumTotal();
                } else {
                    if (data['message'] !== undefined && data['message'] !== '') {
                        _error(data['message']);
                        $('.kod-rabatowy').addClass('error');
                    }
                }
            }
        });
    } else {
        $('.kod-rabatowy').addClass('error');
    }
});
$(document).on('keyup', 'input.error', function () {
    $(this).removeClass('error');
});
$(document).on('change keyup', 'input[other-req]', function () {
    var other = $(this).attr('other-req').split(',');
    if ($(this).val() !== '') {
        $(this).attr('required', 'required');
        $.each(other, function (i, v) {
            $(v).attr('required', 'required').blur();
        });
    } else {
        $(this).removeAttr('required');
        $.each(other, function (i, v) {
            if ($(v).attr('required') !== undefined) {
                $(v).trigger('change');
            }
        });
        $(this).blur();
    }
});
$(document).on('change', 'form[on-change-action] input,form[on-change-action] select,form[on-change-action] textarea', function () {
    var action = $(this).parents('form').attr('on-change-action');
    var field = $(this).attr('name');
    var value = $(this).val();
    console.log(field, value);
    $.ajax({
        type: 'POST',
        data: field + '=' + value,
        url: action
    });
});
$(document).on('change', 'input[type=radio][name="punkt_odbioru_id"]', function () {
    $(this).parents('.punkt-container').removeClass('error');
});
$(document).on('submit', '#form-karta-kod', function () {
    var data = $(this).serialize();
    var url = $(this).attr('action');
    var form = this;
    $.ajax({
        type: 'POST',
        data: data,
        dataType: 'json',
        url: url,
        success: function (data, textStatus, jqXHR) {
            if (data['status'] !== undefined && data['status'] === 'success') {
                $(form).find('input[type=text]').val('');
                if (data['message'] !== undefined && data['message'] !== '') {
                    _success(data['message']);
                }
                if (data['bonusy'] !== undefined) {
                    $('#bonus_value').html(data['bonusy']);
                }
                if (!($('#bonus_value').length > 0)) {
                    document.location.reload();
                }
            } else {
                if (data['message'] !== undefined && data['message'] !== '') {
                    _error(data['message']);
                }
            }
        },
        error: function (data, textStatus, message) {
            if (data.status === 403) {
                _error(getTranslate('errors.wymaganeLogowanie'));
            } else {
                _error(message);
            }
        }
    });
    return false;
});
$(document).on('change', 'select[name="wysylka_kraj"]', function () {
    checkWysylkaKraj();
    sumTotal();
});
function checkWysylkaKraj() {
    if ($('#cart-form select[name="wysylka_kraj"]').val() !== krajDef) {
        var koszt = kosztyKrajeDef;
        $('.hide-for-kraj').addClass('hidden');
        $('[disable-for-kraj=true]').attr('disabled', 'disabled');
        if (!($('input[type=radio][name=platnosc]:visible:checked').length > 0)) {
            $('input[type=radio][name=platnosc]:visible')[0].checked = true;
        }
        if (!($('input[type=radio][name=wysylka]:visible:checked').length > 0)) {
            $('input[type=radio][name=wysylka]:visible')[0].checked = true;
        }
        var ws = $('input[type=radio][name=wysylka]:checked').val();
        if (kosztyKraje !== undefined && kosztyKraje[$('#cart-form select[name="wysylka_kraj"]').val()] !== undefined && kosztyKraje[$('#cart-form select[name="wysylka_kraj"]').val()][ws] !== undefined) {
            koszt = kosztyKraje[$('#cart-form select[name="wysylka_kraj"]').val()][ws];
        }
        $('<input type="hidden" id="koszt-for-kraj" value="' + koszt + '"/>').appendTo($('#cart-form'));
        $.each($('input[type=radio][name=wysylka]:not([e-wysylka=true]):visible'), function (i, v) {
            koszt = kosztyKrajeDef;
            if (kosztyKraje !== undefined && kosztyKraje[$('#cart-form select[name="wysylka_kraj"]').val()] !== undefined && kosztyKraje[$('#cart-form select[name="wysylka_kraj"]').val()][$(v).val()] !== undefined) {
                koszt = kosztyKraje[$('#cart-form select[name="wysylka_kraj"]').val()][$(v).val()];
            }
            var price = parseFloat($(v).attr('def-price'));
            price = price + koszt;
            $(v).attr('price', price);
            $(v).next('label').find('.platnosc-cena-label').html(currency(price));
        });
        $('input[type=radio][name=wysylka]:checked').trigger('change');
    } else {
        $('#koszt-for-kraj').remove();
        $('.hide-for-kraj').removeClass('hidden');
        $('[disable-for-kraj=true]').removeAttr('disabled');
        $.each($('input[type=radio][name=wysylka]:visible'), function (i, v) {
            var price = parseFloat($(v).attr('def-price'));
            $(v).attr('price', price);
            $(v).next('label').find('.platnosc-cena-label').html(currency(price));
        });
    }
}