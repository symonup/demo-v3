$(document).ready(function () {
    if ($('.searchAttr').length > 0) {
        $.each($('.searchAttr input[type=checkbox]:checked'), function (i, v) {
            var mainParent = $($(v).parents('li')[($(v).parents('li').length - 1)]);
            if (!mainParent.hasClass('open')) {
                mainParent.addClass('open');
            }
        });
    }
});
$(document).on('click', '.rozwin-grupe', function () {
    if ($(this).hasClass('open')) {
        $(this).removeClass('open');
        $('tr.group[group_id=' + $(this).attr('group') + ']').removeClass('open');
    } else {
        $(this).addClass('open');
        $('tr.group[group_id=' + $(this).attr('group') + ']').addClass('open');
    }
});
$(document).on('click', '.attrSubAdd', function () {
    var inputItem = $('.subAttrValDef').clone();
    inputItem.removeClass('subAttrValDef').removeAttr('disabled');
    var div = $('<div>');
    inputItem.appendTo(div);
    div.appendTo($('#wartosci_podrzedne'));
});
$(document).on('change', '.attr-input', function () {
    if (!$(this).is(':checked')) {
        if ($('.sub-attr-input[attr-id=' + $(this).val() + ']').length > 0) {
            $.each($('.sub-attr-input[attr-id=' + $(this).val() + ']'), function (i, v) {
                v.checked = false;
            });
        }
    }
});
$(document).on('change', '.sub-attr-input', function () {
    if ($(this).is(':checked')) {
        $('.attr-input[attr-id=' + $(this).attr('attr-id') + ']')[0].checked = true;
    }
});
$(document).on('click', '.attr-rozwin-liste', function () {
    if ($($(this).parents('li')[0]).hasClass('open')) {
        $($(this).parents('li')[0]).removeClass('open');
    } else {
        $($(this).parents('li')[0]).addClass('open');
    }
});
$(document).on('keyup','.searchAttr input[type=text]',function(){
    var checked=false;
    if($(this).val()!==''){
        checked=true;
    }
    var checkbox=$($(this).parents('li')[0]).find('input[type=checkbox]');
    if(checked && !checkbox.is(':checked')){
        checkbox[0].checked=true;
        checkbox.trigger('change');
    }
    if(!checked && checkbox.is(':checked')){
        checkbox[0].checked=false;
        checkbox.trigger('change');
    }
});