var xhrSearch;
$(document).ready(function() {
//    $('ul.similar_product_sort').sortable({
//        group: 'similar_product_sort',
//        pullPlaceholder: false,
//        scroll: true,
//        scrollSensitivity: 10,
//        axis: "y",
//        // animation on drop
//        onDrop: function($item, container, _super) {
//            var $clonedItem = $('<li/>').css({height: 0});
//            $item.before($clonedItem);
//            $clonedItem.animate({'height': $item.height()});
//            $item.animate($clonedItem.position(), function() {
//                $clonedItem.detach();
//                _super($item, container);
//            });
//
//            $item.addClass('selected');
//            if ($($item[0].parentElement).hasClass('similar_search_results'))
//                $item.find('input').attr('disabled', 'disabled');
//            else if ($($item[0].parentElement).hasClass('akcesoria_search_results'))
//                $item.find('input').attr('disabled', 'disabled');
//            else if ($($item[0].parentElement).hasClass('artykul_search_results'))
//                $item.find('input').attr('disabled', 'disabled');
//            else if ($($item[0].parentElement).hasClass('polecane_search_results'))
//                $item.find('input').attr('disabled', 'disabled');
//            else
//                $item.find('input').removeAttr('disabled');
//        },
//        // set $item relative to cursor position
//        onDragStart: function($item, container, _super) {
//            var offset = $item.offset(),
//                    pointer = container.rootGroup.pointer;
//
//            adjustment = {
//                left: pointer.left - offset.left,
//                top: pointer.top - offset.top
//            };
//
//            _super($item, container);
//        },
//        onDrag: function($item, position, _super) {
//            $item.css({
//                left: position.left - adjustment.left,
//                top: position.top - adjustment.top
//            });
//        }
//    });
});
$(document).on('keyup', '#similarSearch', function() {
    if (xhrSearch != undefined) {
        xhrSearch.abort();
    }
    $('.searching').html('<span aria-hidden="true" class="glyphicon glyphicon-refresh animate"></span> searching');
    var adres = document.location.pathname.split('/');
   
    var ajaxAdr = $(this).attr('data-target');
    var towarId=((editTowarId!==undefined)?'&towar_id='+editTowarId:'');
    ajaxAdr = ajaxAdr + $(this).val();
    xhrSearch = $.ajax({
        type: 'get',
        data: 'field=podobne'+towarId,
        url: ajaxAdr,
        dataType:'json',
        success: function(msg) {
            $('.similar_search_results').html(msg['content']);
            $('.searching').html('');
        }
    });
});
$(document).on('keyup', '#akcesoriaSearch', function() {
    if (xhrSearch != undefined) {
        xhrSearch.abort();
    }
    $('.akcesoria_searching').html('<span aria-hidden="true" class="glyphicon glyphicon-refresh animate"></span> searching');
    var adres = document.location.pathname.split('/');
    var ajaxAdr = $(this).attr('data-target');
    var towarId=((editTowarId!==undefined)?'&towar_id='+editTowarId:'');
    ajaxAdr = ajaxAdr + $(this).val();
    xhrSearch = $.ajax({
        type: 'get',
        data: 'field=akcesoria'+towarId,
        url: ajaxAdr,
        dataType:'json',
        success: function(msg) {
            $('.akcesoria_search_results').html(msg['content']);
            $('.akcesoria_searching').html('');
        }
    });
});
$(document).on('keyup', '#towarArtykulSearch', function() {
    if (xhrSearch != undefined) {
        xhrSearch.abort();
    }
    $('.artykul_searching').html('<span aria-hidden="true" class="glyphicon glyphicon-refresh animate"></span> searching');
    var adres = document.location.pathname.split('/');
    var ajaxAdr = $(this).attr('data-target');
    ajaxAdr = ajaxAdr + $(this).val();
    xhrSearch = $.ajax({
        type: 'get',
        data: 'field=artykul',
        url: ajaxAdr,
        dataType:'json',
        success: function(msg) {
            $('.artykul_search_results').html(msg['content']);
            $('.artykul_searching').html('');
        }
    });
});

$(document).on('keyup', '#polecaneSearch', function() {
    if (xhrSearch != undefined) {
        xhrSearch.abort();
    }
    $('.polecane_searching').html('<span aria-hidden="true" class="glyphicon glyphicon-refresh animate"></span> searching');
    var adres = document.location.pathname.split('/');
    
    var ajaxAdr = $(this).attr('data-target');
    var towarId=((editTowarId!==undefined)?'&towar_id='+editTowarId:'');
    ajaxAdr = ajaxAdr + $(this).val();
    xhrSearch = $.ajax({
        type: 'get',
        data: 'field=polecane'+towarId,
        url: ajaxAdr,
        dataType:'json',
        success: function(msg) {
            $('.polecane_search_results').html(msg['content']);
            $('.polecane_searching').html('');
        }
    });
});
$(document).on('keyup', '#artykulTowarSearch', function() {
    if (xhrSearch != undefined) {
        xhrSearch.abort();
    }
    $('.polecane_searching').html('<span aria-hidden="true" class="glyphicon glyphicon-refresh animate"></span> searching');
    
    var ajaxAdr = $(this).attr('data-target');
    ajaxAdr = ajaxAdr + $(this).val();
    xhrSearch = $.ajax({
        type: 'get',
        data: 'field=towar',
        url: ajaxAdr,
        dataType:'json',
        success: function(msg) {
            $('.polecane_search_results').html(msg['content']);
            $('.polecane_searching').html('');
        }
    });
});

$(document).on('keyup', '[wymiana-search="true"]', function() {
    if (xhrSearch != undefined) {
        xhrSearch.abort();
    }
    $('.wymiana_searching').html('<span aria-hidden="true" class="glyphicon glyphicon-refresh animate"></span> searching');
    var adres = document.location.pathname.split('/');
    var ajaxAdr = [adres[0], adres[1], adres[2]];
    var wymiana_id = $(this).parents('div.wymiana_content').attr('id').replace('wymiana_', '');
    ajaxAdr = basePath+'admin/towar/find_similar/' + $(this).val();
    xhrSearch = $.ajax({
        type: 'get',
        data: 'field=zestaw_towar_wymiana&wymiana=1&towar_id=' + wymiana_id,
        dataType:'json',
        url: ajaxAdr,
        success: function(msg) {
            $('.similar_search_results_' + wymiana_id).html(msg['content']);
            $('.wymiana_searching').html('');
        }
    });
});
$(document).on('click', '.similar_product_sort > li', function() {
    if ($(this).hasClass('selected'))
        $(this).removeClass('selected');
    else
        $(this).addClass('selected');
});
$(document).on('click', 'div.similar_action_content > *', function() {
    var action = $(this).attr('data-action');
    var dataContent = $(this).attr('data-content');
    var dataContentResult = $(this).attr('data-content-result');
    if (action === 'move-to-similar')
    {
        var selected = $('.' + dataContentResult + ' > li.selected');
        if (selected.length > 0)
        {
            $.each(selected, function(index, value) {
                $(value).removeClass('selected');
                var input = $(value).find('input');
                input.removeAttr('disabled');
                $(value).appendTo($('.' + dataContent));
            });
        }
        else
            alert('Select items to move.');
    }
    else if (action === 'move-to-result')
    {
        var selected = $('.' + dataContent + ' > li.selected');
        if (selected.length > 0)
        {
            $.each(selected, function(index, value) {
                $(value).removeClass('selected');
                var input = $(value).find('input');
                input.attr('disabled', 'disabled');
                $(value).appendTo($('.' + dataContentResult));
            });
        }
        else
            alert('Select items to move.');
    }
    else if (action === 'move-all-to-similar')
    {
        var selected = $('.' + dataContentResult + ' > li');
        if (selected.length > 0)
        {
            $.each(selected, function(index, value) {
                $(value).removeClass('selected');
                var input = $(value).find('input');
                input.removeAttr('disabled');
                $(value).appendTo($('.' + dataContent));
            });
        }
    }
    else if (action === 'move-all-to-result')
    {
        var selected = $('.' + dataContent + ' > li');
        if (selected.length > 0)
        {
            $.each(selected, function(index, value) {
                $(value).removeClass('selected');
                var input = $(value).find('input');
                input.attr('disabled', 'disabled');
                $(value).appendTo($('.' + dataContentResult));
            });
        }
    }
    else if (action === 'delete-similar')
    {
        var selected = $('.' + dataContent + ' > li.selected');
        if (selected.length > 0)
        {
            $.each(selected, function(index, value) {
                $(value).remove();
            });
        }
        else
            alert('Select items to delete.');
    } else if (action === 'full-size')
    {
        if ($('.' + dataContent).attr('style') == 'max-height: 100%;')
            $('.' + dataContent).removeAttr('style');
        else
            $('.' + dataContent).css('max-height', '100%');
    }
});
$(document).on('dblclick', '.similar_product_sort.similar_search_results > li,.similar_product_sort.akcesoria_search_results > li,.similar_product_sort.polecane_search_results > li,.similar_product_sort.artykul_search_results > li', function() {
    var contClass = '';
    if ($(this).parents('.similar_product_sort').hasClass('similar_search_results'))
        contClass = 'similar_products';
    else if ($(this).parents('.similar_product_sort').hasClass('akcesoria_search_results'))
        contClass = 'akcesoria_products';
    else if ($(this).parents('.similar_product_sort').hasClass('polecane_search_results'))
        contClass = 'polecane_products';
    else if ($(this).parents('.similar_product_sort').hasClass('artykul_search_results'))
        contClass = 'artykul_products';
    if (contClass !== '')
    {
        var selected = $(this);
        if (selected.length > 0)
        {
            $.each(selected, function(index, value) {
                $(value).removeClass('selected');
                $(value).find('input').removeAttr('disabled');
                $(value).find('select').removeAttr('disabled');
                $(value).appendTo($('.' + contClass));
            });
        }
    }
});
$(document).on('dblclick', '.similar_product_sort.similar_products > li,.similar_product_sort.akcesoria_products > li,.similar_product_sort.polecane_products > li,.similar_product_sort.artykul_products > li', function() {
    var contClass = '';
    if ($(this).parents('.similar_product_sort').hasClass('similar_products'))
        contClass = 'similar_search_results';
    else if ($(this).parents('.similar_product_sort').hasClass('akcesoria_products'))
        contClass = 'akcesoria_search_results';
    else if ($(this).parents('.similar_product_sort').hasClass('polecane_products'))
        contClass = 'polecane_search_results';
    else if ($(this).parents('.similar_product_sort').hasClass('artykul_products'))
        contClass = 'artykul_search_results';
    var selected = $(this);
    if (selected.length > 0)
    {
        $.each(selected, function(index, value) {
            $(value).removeClass('selected');
            $(value).find('input').attr('disabled', 'disabled');
            $(value).find('select').attr('disabled', 'disabled');
            $(value).appendTo($('.' + contClass));
        });
    }
});