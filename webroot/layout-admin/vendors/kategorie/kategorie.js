$(document).ready(function(){
    if($('.multiple-checkbox-list:not(.openAll) input[type=checkbox]').length>0){
        $('.multiple-checkbox-list ul').addClass('hidden');
        $('.multiple-checkbox-list .caret').addClass('closed').removeClass('open');
    }
    if($('.multiple-checkbox-list:not(.openAll) input[type=checkbox]:checked').length>0){
        $.each($('.multiple-checkbox-list input[type=checkbox]:checked'),function(i,v){
            $.each($(v).parents('li'),function(pi,pv){
                $($(pv).find('ul')[0]).removeClass('hidden');
                $($(pv).find('.caret')[0]).removeClass('closed').addClass('open');
            });
                $(v).trigger('change');
        });
    }
});
$(document).on('click','.caret',function(){
    if($(this).hasClass('open')){
        $($(this).parents('li')[0]).find('ul').addClass('hidden');
        $($(this).parents('li')[0]).find('ul .caret').addClass('closed').removeClass('open');
        $(this).addClass('closed').removeClass('open');
    } else {
        $($($(this).parents('li')[0]).find('ul')[0]).removeClass('hidden');
        $(this).removeClass('closed').addClass('open');
    }
});
$(document).on('change','.multiple-checkbox-list input[type=checkbox]',function(){
    if($(this).is(':checked')){
        var caret=$($(this).parents('li')[0]).find('.caret');
        if(caret.length>0 && $(caret[0]).hasClass('closed')){
            $(caret[0]).trigger('click');
        }
        $.each($(this).parents('li'),function(i,v){
            if(i>0){
                $(v).addClass('child-selected');
            }
            else{
                $(v).addClass('selected');
            }
        });
    } else {
        $($(this).parents('li')[0]).removeClass('selected');
        $.each($(this).parents('li'),function(i,v){
                if(!$(v).find('input[type=checkbox]:checked').length>0){
                    $(v).removeClass('child-selected');
                }
        });
    }
});
$(document).on('click','.multiple-checkbox-list-container .showAll',function(){
    var item=$($(this).attr('data-target'));
    item.find('ul').removeClass('hidden');
    item.find('.caret').removeClass('closed').addClass('open');
});
$(document).on('click','.multiple-checkbox-list-container .hideAll',function(){
    var item=$($(this).attr('data-target'));
    item.find('ul').addClass('hidden');
    item.find('.caret').addClass('closed').removeClass('open');
});
$(document).on('click','.multiple-checkbox-list-container .selectAll',function(){
    var item=$($(this).attr('data-target'));
    $.each(item.find('input[type=checkbox]'),function(i,v){
        if(!$($(v).parents('li')[0]).hasClass('hidden')){
            v.checked=true;
            $(v).trigger('change');
    }
    });
});
$(document).on('click','.multiple-checkbox-list-container .unselectAll',function(){
    var item=$($(this).attr('data-target'));
    $.each(item.find('input[type=checkbox]'),function(i,v){
        if(!$($(v).parents('li')[0]).hasClass('hidden')){
           v.checked=false;
            $(v).trigger('change');
    }
    });
});
$(document).on('keyup','.multi-select-finder',function(){
    var searchTerm = $(this).val();
    var _target = $(this).attr('data-target');
    if (searchTerm === '')
        $(_target + ' li').removeClass('hidden');
    else
    {
        var searchSplit = searchTerm.replace(/ /g, "'):containsi('");
        $.extend($.expr[':'], {'containsi': function (elem, i, match, array) {
                return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
            }
        });

        $(_target + ' li').not(":containsi('" + searchSplit + "')").each(function (e) {
            $(this).addClass('hidden');
        });

        $(_target + " li:containsi('" + searchSplit + "')").each(function (e) {
            $(this).removeClass('hidden');
        });
    }
});