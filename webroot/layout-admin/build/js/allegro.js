var forAuction = false;
var imagesUploaded;
var imagesDescriptionActive = null;
$(document).ready(function () {
    if ($('[new-auction=true]').length > 0 && $('#category-id').length > 0 && $('#category-id').val() !== '') {
        forAuction = true;
        getCategoryDetalis();
    }
    if ($('.description-item .content textarea').length > 0) {
        allegroSummernoteItem($('.description-items > .description-item .content textarea'));
    }

});
$(document).on('click', '.setAllegroCategoryBtn', function () {
    var url = $(this).attr('data-target');
    if (url !== undefined && url !== '') {
        if ($(this).attr('new-auction') !== undefined && $(this).attr('new-auction') === 'true') {
            forAuction = true;
        }
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: url,
            success: function (data, textStatus, jqXHR) {
                if (data['status'] !== undefined && data['status'] === 'success') {
                    $(data['html']).appendTo($('body'));
                    $('#allegro-kategorie-modal').modal('show');
                }
            }
        });
    } else {
        _error('No url data');
        return false;
    }
});
$(document).on('change', '.allegro-category-select', function () {
    var selected = $(this).find('option:selected');
    if (selected.attr('href') !== undefined && selected.attr('href') !== '') {
        $(this).nextAll('.allegro-category-select').remove();
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: selected.attr('href'),
            data: 'key=' + $('.allegro-category-select').length,
            success: function (data, textStatus, jqXHR) {
                if (data['status'] !== undefined && data['status'] === 'success') {
                    $(data['html']).appendTo($('#allegro-kategorie-options-container'));
                }
            }
        });
    }
});
$(document).on('click', '#allegro-confirm-select-cat', function () {
    var selectedCat = $('#allegro-kategorie-options-container .allegro-category-select:last-child').val();
    var selectedPath = '';
    var selectedPathIds = ''
    $.each($('#allegro-kategorie-options-container .allegro-category-select'), function (i, v) {
        selectedPath = selectedPath + ((selectedPath !== '') ? ' / ' : '') + $(v).find('option:selected').text();
        selectedPathIds = selectedPathIds + ((selectedPathIds !== '') ? ',' : '') + $(v).val();
    });
    if ($('input[allegro-cat-id=true]').length > 0) {
        $('input[allegro-cat-id=true]').val(selectedCat);
    }
    if ($('input[allegro-cat-path=true]').length > 0) {
        $('input[allegro-cat-path=true]').val(selectedPath);
    }
    if ($('input[allegro-cat-path-id=true]').length > 0) {
        $('input[allegro-cat-path-id=true]').val(selectedPathIds);
    }
    if ($('.allegro-cat-path-html').length > 0) {
        $('.allegro-cat-path-html').html(selectedPath);
    }
    $('#allegro-kategorie-modal').modal('hide');
    if (forAuction) {
        getCategoryDetalis();
    }
});
$(document).on('hidden.bs.modal', '#allegro-kategorie-modal', function () {
    $('#allegro-kategorie-modal').remove();
});
function getCategoryDetalis() {
    var url = $('#category-id').attr('data-target') + '/' + $('#category-id').val() + '/1';
    if ($('#offer-id').length > 0) {
        url = url + '?itemId=' + $('#offer-id').html();
    }
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: url,
        success: function (data, textStatus, jqXHR) {
            if (data['status'] !== undefined && data['status'] === 'success') {
                $('#category-params').html(data['html']);
            } else {
                if (data['message'] !== undefined && data['message'] !== '') {
                    _error(data['message']);
                }
            }
        },
        error: function (data, textStatus, message) {
            _error(message);
        }
    });
}
$(document).on('click', '.image-item .remove', function () {
    $($(this).parents('.image-item')[0]).remove();
});
$(document).on('click', '#send-images', function () {
    var allImages = $('.image-item input.image-to-send');
    var url = $(this).attr('data-target');
    if (allImages.length > 0) {
        var data = new Object();
        $.each(allImages, function (i, v) {
            data[i] = $(v).val();
        });
        $('#send-images').addClass('reload');
        $.ajax({
            type: 'POST',
            data: data,
            url: url,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if (data['status'] !== undefined && data['status'] === 'success') {
                    if (data['images'] !== undefined) {
                        imagesUploaded = new Object();
                        $.each(data['images'], function (i, v) {
                            imagesUploaded[i] = v.location;
                            $('.image-to-send[image-id=' + i + ']').val(v.location);
                        });
                    }
                    if (data['modal'] !== undefined) {
                        $(data['modal']).appendTo('body');
                    }
                    $('#send-images').remove();
                    $('.image-item .remove').remove();
                    if (data['message'] !== undefined && data['message'] !== '') {
                        _success(data['message']);
                    }
                } else {
                    if (data['message'] !== undefined && data['message'] !== '') {
                        _error(data['message']);
                    }
                }
            },
            error: function (data, textStatus, message) {
                _error(message);
            }
        });
    } else {
        _error('Brak zdjęć do wysłania');
    }
});
var allegroText = new Object();
$(document).on('click', '.description-item-type', function () {
    if (!$(this).hasClass('active')) {
        var type = $(this).attr('data-type');
        var container = $($(this).parents('.description-item')[0]);
        container.find('.description-item-type').removeClass('active');
        $(this).addClass('active');
        var key = container.attr('key');
        if (key === undefined || key === '') {
            key = $('.description-items > .description-item').length;
            container.attr('key', key);
        }
        container.attr('type', type);
        var content = $('#description-items-default > [data-type="' + type + '"]').html();
        allegroText[key] = ((container.find('textarea').length > 0) ? container.find('textarea').val() : ((allegroText[key] !== undefined) ? allegroText[key] : ''));
        container.find('.content').html(content);
        container.find('textarea').val(allegroText[key]);
        if (container.find('textarea').length > 0) {
            allegroSummernoteItem(container.find('textarea'));
        }
    }
});
$(document).on('click', '#add-description-line', function () {
    var itemDefault = $('#default-description-item').clone();
    var key = (parseInt($(this).attr('key')) + 1);
    $(this).attr('key', key);
    itemDefault.find('.description-item').attr('key', key);
    $(itemDefault.html()).appendTo($('.description-items'));
    var addedItem = $('.description-items .description-item:last-child textarea');
    if (addedItem.length > 0) {
        allegroSummernoteItem(addedItem);
    }
});
$(document).on('click', '.description-items > .description-item .header .move', function () {
    var target = $(this).attr('data-target');
    var container = $($(this).parents('.description-item')[0]);
    if (target === 'up') {
        if (container.prev('.description-item').length > 0) {
            container.insertBefore(container.prev('.description-item'));
        }
    } else {
        if (container.next('.description-item').length > 0) {
            container.insertAfter(container.next('.description-item'));
        }
    }
});
$(document).on('click', '.description-items > .description-item .header .remove', function () {
    var container = $($(this).parents('.description-item')[0]);
    var yesBtn = (($(this).attr('confirm-btn-yes') !== undefined) ? $(this).attr('confirm-btn-yes') : 'Tak');
    var noBtn = (($(this).attr('confirm-btn-no') !== undefined) ? $(this).attr('confirm-btn-no') : 'Nie');
    var cfMessage = $(this).attr('confirm-message');
    swal({
        title: '',
        text: cfMessage,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: yesBtn,
        cancelButtonText: noBtn,
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {
        container.remove();
    }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if (dismiss === 'cancel') {

        }
    });
});
$(document).on('click', '.description-items > .description-item .header .copy', function () {
    var container = $($(this).parents('.description-item')[0]);
    var newItem = container.clone();
    var key = (parseInt($('#add-description-line').attr('key')) + 1);
    $('#add-description-line').attr('key', key);
    newItem.find('[key]').attr('key', key);
    newItem.insertAfter(container);
});
$(document).on('click', '.description-items > .description-item .image-desceription', function () {
    imagesDescriptionActive = $(this);
    $('#allegro-images-modal').modal('show');
});
$(document).on('click', '.select-allegro-image', function () {
    if (imagesDescriptionActive !== null && imagesDescriptionActive !== undefined) {
        imagesDescriptionActive.html($(this).html());
        $('#allegro-images-modal').modal('hide');
    }
});
$(document).on('hidden.bs.modal', '#allegro-images-modal', function () {
    imagesDescriptionActive = null;
});
$(document).on('submit', '#form-new-auction', function () {
    if ($('#send-images').length > 0) {
        _error($('#send-images').attr('error-message'));
        $('#send-images').focus();
        return false;
    }
    var ilosc = $('#stock-available').val();
    if (ilosc === '' || isNaN(parseInt(ilosc)) || parseInt(ilosc) < 1) {
        _error($('#stock-available').attr('error-message'));
        $('#stock-available').focus();
        return false;
    }
    var dane = $(this).serialize();
    var url = $(this).attr('action');
    var btn = $(this).find('[type=submit]');
    btn.addClass('reload');
    dane = dane + '&' + $.param({description: setDescriptionContent()});
    $.ajax({
        type: 'POST',
        data: dane,
        url: url,
        dataType: 'json',
        success: function (data, textStatus, jqXHR) {
            btn.removeClass('reload');
            if (data['status'] !== undefined && data['status'] === 'success') {
                if (data['errors'] !== undefined && data['errors'] !== '' && data['errors'] !== null) {
                    _error(data['errors']);
                } else {
                    if (data['message'] !== undefined && data['message'] !== '') {
                        _success(data['message']);
                    }
                    if (data['results'] !== undefined && data['results']['reloadUrl'] !== undefined && data['results']['reloadUrl'] !== '') {
                        document.location = data['results']['reloadUrl'];
                    }
                }

            } else {
                if (data['errors'] !== undefined && data['errors'] !== '') {
                    _error(data['errors']);
                }
                if (data['message'] !== undefined && data['message'] !== '') {
                    _error(data['message']);
                }
            }
        },
        error: function (data, textStatus, message) {
            btn.removeClass('reload');
            _error(message);
        }
    });
    return false;
});
function allegroSummernoteItem(item) {
    item.summernote({
        height: 200,
        tabsize: 2,
        styleTags: ['p', 'h1', 'h2'],
        toolbar: [
            ['style', ['style', 'bold']],
            ['para', ['ul', 'ol']],
            ['misc', ['undo', 'redo', 'codeview', 'fullscreen']]
        ],
        onCreateLink: function (link) {
            return link;
        },
         callbacks: {
            onPaste: function (e) {
                if (document.queryCommandSupported("insertText")) {
                    var text = $(e.currentTarget).html();
                    var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');

                    setTimeout(function () {
                        document.execCommand('insertText', false, bufferText);
                    }, 10);
                    e.preventDefault();
                } else { //IE
                    var text = window.clipboardData.getData("text")
                    if (trap) {
                        trap = false;
                    } else {
                        trap = true;
                        setTimeout(function () {
                            document.execCommand('paste', false, text);
                        }, 10);
                        e.preventDefault();
                    }
                }

            }
        }
    });
}
function setDescriptionContent() {
    var descriptionContent = {sections: new Object()};
    $.each($('.description-items > .description-item'), function (i, v) {
        var type = $(v).attr('type');
        switch (type) {
            case 'text' :
                {
                    var text = $(v).find('textarea').val().replaceAll('<br>','');
                    text = text.replaceAll('<p></p>','');
                    descriptionContent['sections'][i] = {items: [{type: 'TEXT', content: text}]};
                }
                break;
            case 'image' :
                {
                    descriptionContent['sections'][i] = {items: [{type: 'IMAGE', url: $(v).find('.image-desceription input').val()}]};
                }
                break;
            case 'text-image' :
                {
                    var text = $(v).find('textarea').val().replaceAll('<br>','');
                    text = text.replaceAll('<p></p>','');
                    descriptionContent['sections'][i] = {items: [{type: 'TEXT', content: text}, {type: 'IMAGE', url: $(v).find('.image-desceription input').val()}]};
                }
                break;
            case 'image-text' :
                {
                    var text = $(v).find('textarea').val().replaceAll('<br>','');
                    text = text.replaceAll('<p></p>','');
                    descriptionContent['sections'][i] = {items: [{type: 'IMAGE', url: $(v).find('.image-desceription input').val()}, {type: 'TEXT', content: text}]};
                }
                break;
            case 'image-image' :
                {
                    descriptionContent['sections'][i] = {items: [{type: 'IMAGE', url: $(v).find('.image-desceription input').val()}, {type: 'IMAGE', url: $(v).find('.image-desceription input').val()}]};
                }
                break;
        }
    });
    return descriptionContent;
}
$(document).on('change', '.multiple-item', function () {
    if ($(this).parents('div.select.required').length > 0) {
        var group = $(this).attr('multiple-key');
        if ($(this).is(':checked')) {
            if (group !== undefined && group !== '') {
                $('.multiple-item[multiple-key="' + group + '"]').removeAttr('required');
            }
        } else {
            if (!($('.multiple-item[multiple-key="' + group + '"]:checked').length) > 0) {
                $('.multiple-item[multiple-key="' + group + '"]').attr('required', 'required');
            }
        }
    }
});
$(document).on('change', '#sellingmode-format', function () {
    $('.sellingModeDuration').addClass('hidden');
    $('.sellingModeDuration select').attr('disabled', 'disabled');
    $('.sellingModeDuration[type="' + $(this).val() + '"]').removeClass('hidden');
    $('.sellingModeDuration[type="' + $(this).val() + '"] select').removeAttr('disabled');
});
$(document).on('click', '.allegro-powiaz', function () {
    $('#allegro-powiaz-modal form').attr('action', $(this).attr('href'));
    $('#allegro-powiaz-modal').modal('show');
    return false;
});
$(document).on('submit', '#allegro-powiaz-modal form', function () {
    var action = $(this).attr('action');
    var dane = $(this).serialize();
    var btn = $(this).find('[type=submit]');
    btn.addClass('reload');
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: action,
        data:dane,
        success: function (data, textStatus, jqXHR) {
            btn.removeClass('reload');
            if (data['status'] !== undefined && data['status'] === 'success') {
                $('#allegro-powiaz-modal').modal('hide');
                if (data['message'] !== undefined && data['message'] !== '') {
                    _success(data['message']);
                }
                $('.allegro-powiaz[item-id='+data['item_id']+']').remove();
            } else {
                if (data['message'] !== undefined && data['message'] !== '') {
                    _error(data['message']);
                }
            }
        },
        error: function (data, textStatus, message) {
            btn.removeClass('reload');
            _error(message);
        }
    });
    return false;
});