String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};
var mimeType;
var croppChanged = false;
var blockUnload = false;
var codeMirrorsCss = new Object();
var codeMirrorsJs = new Object();
var codeMirrorsHtml = new Object();
var dataTablesArr = new Object();
function empty(item){
    if(typeof (item) == 'undefined' || item===undefined || item===false || item===''){
        return true;
    }
    return false;
}
function _error(msg, title) {

    if (typeof (PNotify) === 'undefined') {
        return;
    }
    if (title === undefined || title === '') {
        title = 'Error!'
    }
    new PNotify({
        title: title,
        type: "error",
        text: msg,
        styling: 'bootstrap3'
    });

}
function _success(msg, title) {

    if (typeof (PNotify) === 'undefined') {
        return;
    }
    if (title === undefined || title === '') {
        title = 'Success!'
    }
    new PNotify({
        title: title,
        type: "success",
        text: msg,
        styling: 'bootstrap3'
    });

}
$(document).ready(function () {
    if($('[change-action]').length>0){
        $('[change-action]').trigger('change');
    }
    if ($('#tips-slider').length > 0) {
        $('#tips-slider').carousel({
            interval: 20000
        });
    }
    if (Object.keys(alertMessages).length > 0) {
        $.each(alertMessages, function (msgType, messages) {
            if (msgType === 'success') {
                $.each(messages, function (i, msg) {
                    _success(msg);
                });
            } else {
                $.each(messages, function (i, msg) {
                    _error(msg);
                });
            }
        });
    }
    if ($('.data-table').length > 0) {
        $.each($('.data-table'), function (i, v) {
            dataTablesArr[i] = $(v).dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Polish.json"
                },
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: ''
                    }
                },
                "pageLength": (($($('.data-table')[0]).attr('on-page') !== undefined) ? $($('.data-table')[0]).attr('on-page') : 50),
                "autoWidth": false,
                dom: "<'row'<'col-sm-6'f><'col-sm-6'l>>" +
                        "<'row'<'col-sm-12'tr>>" +
                        "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                "order": [[(($(v).attr('def-sort') !== undefined) ? parseInt($(v).attr('def-sort')) : 0), (($(v).attr('def-sort-direction') !== undefined) ? $(v).attr('def-sort-direction') : "asc")]]

            });
        });
    }
    $('[data-toggle=popover]').popover();
    $('[data-toggle=popover-info]').popover({
        template: '<div class="popover popover-help" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
    });

    $.datetimepicker.setLocale('pl');
    $('.datepicker').datepicker({
        dateFormat: "yy-mm-dd"
    });
    $('.daypicker').datepicker({
        dateFormat: "dd-mm"
    });
    $('[datepicker=date]').datetimepicker({
        format: 'Y-m-d',
        timepicker: false
    });
    $('[datepicker=mm-dd]').datepicker({
        dateFormat: "mm-dd"
    });
    $('[datepicker=dd]').datepicker({
        dateFormat: "dd"
    });
    $('[datepicker=time]').datetimepicker({
        datepicker: false,
        format: 'H:i'
    });
    $('[datepicker=datetime]').datetimepicker({
        format: 'Y-m-d H:i',
        step: 30,
        defaultTime: '12:00',
        lang: 'pl'
    });
    checkAlerts();
    $('select[select2=true]').select2();
    $('textarea[summernote=true]').summernote({
        height: 200,
        tabsize: 2,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['insert', ['hr', 'table', 'link', 'video', 'picture', 'elfinder']],
            ['misc', ['undo', 'redo', 'codeview', 'fullscreen']]
        ],
        onCreateLink: function (link) {
            return link;
        }
    });
    $('tbody.sorted-kolejnosc').sortable({
        containerSelector: 'tbody',
        itemSelector: 'tr',
        handle: ".sort-item-point",
        start: function (event, ui) {
        },
        stop: function (event, ui) {
            updateSortData($(event.target));
        }
    });
    if ($('#wykres_zamowien').length > 0) {
        wykresZamowien();
    }
//    $('[color-picker=true]').colorpicker({
//        format: 'hex6'
//    }).on('colorpickerChange colorpickerCreate', function (e) {
//        $(e.colorpicker.element[0]).css('background-color', e.color.toString());
//    });
    if ($('#div-order-1').length === 1 && $('#div-order-2').length === 1) {
        $('#div-order-1').insertBefore($('#div-order-2'));
    }
    if ($('textarea[code-mirror-css=true]').length > 0) {
        $.each($('textarea[code-mirror-css=true]'), function (index, elem) {
            codeMirrorsCss[index] = CodeMirror.fromTextArea(elem, {
                mode: "css",
                lineNumbers: true
            });
        });
    }
    if ($('textarea[code-mirror-html=true]').length > 0) {
        $.each($('textarea[code-mirror-html=true]'), function (index, elem) {
            codeMirrorsHtml[index] = CodeMirror.fromTextArea(elem, {
                mode: "xml",
                htmlMode: true,
                lineNumbers: true
            });
        });
    }
    if ($('textarea[code-mirror-js=true]').length > 0) {
        $.each($('textarea[code-mirror-js=true]'), function (index, elem) {
            codeMirrorsJs[index] = CodeMirror.fromTextArea(elem, {
                mode: "javascript",
                lineNumbers: true
            });
        });
    }
    if ($('#hot-deal-edit-aktywna').length > 0) {
        checkHotDealInputs();
    }
    $('[tagsinput=true]').tagsinput();
    if ($('.select2-remote').length > 0) {
        $.each($('.select2-remote'), function (i, v) {
            $(v).select2({
                placeholder: 'Wyszukaj produkt po nazwie lub kodzie',
                ajax: {
                    url: $(v).attr('data-target'),
                    dataType: 'json',
                    processResults: function (data) {
                        // Tranforms the top-level key of the response object from 'items' to 'results'
                        return {
                            results: data.items
                        };
                    }
                }
            });
        });
    }
    if ($('.crop-autoload').length > 0) {
        cropImages('.crop-autoload');
    }
    croppChanged = false;
});
function checkHotDealInputs() {
    if ($('#hot-deal-edit-aktywna').is(':checked')) {
        $('#hot-deal input').removeAttr('disabled');
    } else {
        $('#hot-deal input:not([type=checkbox])').attr('disabled', 'disabled');
    }
}
$(document).on('change', '#hot-deal-edit-aktywna', function () {
    checkHotDealInputs();
});
function img(node, target) {
    domtoimage.toPng(node)
            .then(function (dataUrl) {
                var img = new Image();
                img.src = dataUrl;
                document.body.appendChild(img);
                $(img).appendTo(target);
            })
            .catch(function (error) {
                console.error('oops, something went wrong!', error);
            });
}
var activeSummernote;
function elfinderDialog(e) {
    activeSummernote = $(e.prevObject).find('textarea[summernote]');
    var fm = $('<div/>').dialogelfinder({
        url: basePath + 'layout-admin/vendors/elFinder-2.1.28/php/connector.minimal.php', // change with the url of your connector
        lang: 'pl',
        width: 840,
        height: 450,
        destroyOnClose: true,
        getFileCallback: function (files, fm) {
            if (activeSummernote.length > 0) {
                activeSummernote.summernote('insertImage', files.url);
            }
        },
        commandsOptions: {
            getfile: {
                oncomplete: 'close',
                folders: false
            }
        }
    }).dialogelfinder('instance');
}

var alertsTimeOut;
function checkAlerts() {
    if (alertsTimeOut !== undefined) {
        clearTimeout(alertsTimeOut);
    }
    $.ajax({
        type: 'get',
        dataType: 'json',
        url: basePath + 'admin/index/check-alerts',
        success: function (msg) {
            if (msg['alerts'] !== undefined && msg['html'] !== undefined) {
                $('#alerts-menu').html(msg['html']);
                if (msg['alerts'] > 0) {
                    $('#alerts-count').html(msg['alerts']);
                    $('#alerts-count-menu').html(msg['alerts']);
                } else {
                    $('#alerts-count').html('');
                    $('#alerts-count-menu').html('0');
                }
            }
            alertsTimeOut = setTimeout(checkAlerts, 60000);
        }
    });
}
$(document).on('click', '.confirm-link', function () {
    var href = $(this).attr('href');
    var yesBtn = (($(this).attr('confirm-btn-yes') !== undefined) ? $(this).attr('confirm-btn-yes') : 'Tak');
    var noBtn = (($(this).attr('confirm-btn-no') !== undefined) ? $(this).attr('confirm-btn-no') : 'Nie');
    var cfMessage = $(this).attr('confirm-message');
    swal({
        title: '',
        text: cfMessage,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: yesBtn,
        cancelButtonText: noBtn,
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {
        document.location = href;
    }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if (dismiss === 'cancel') {

        }
    });
    return false;
});
function updateConfig(_data) {
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: basePath + 'admin/konfiguracja/ajax_update',
        data: _data,
        success: function (msg) {
            if (msg['status'] === 'success') {
                if (msg['action'] !== undefined) {
                    switch (msg['action']) {
                        case 'getDostawyKalendarz':
                            {
                                $('.dostawy-calendar').fullCalendar('refetchEvents');
                            }
                            break;
                        case 'blockDays':
                            {
                                $('.blockDayContainer').html(msg['items']);
                                $('.dostawy-calendar').fullCalendar('refetchEvents');
                            }
                            break;
                        case 'removeBlockDay' :
                            {

                            }
                            break;
                    }
                }
            }
        }
    });
}
$(document).on('click', '.file-add > button', function () {
    $(this).parents('.file-add').find('input').trigger('click');
});
$(document).on('change', '.file-add input[type=file]', function () {
    var input = this;
    var fileType = $(input).attr('file-type');
    var autoSubmit = $(input).attr('auto-submit');
    if(empty(autoSubmit) || autoSubmit!=='true'){
        autoSubmit = false;
    }else{
        autoSubmit = true;
    }
    if(empty(fileType)){
       fileType='image'; 
    }
    if(fileType==='image'){
    $(input).parents('.file-add').find('.upload-file-preview img').cropper('destroy');
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            mimeType = dataURLtoMimeType(reader.result);
            $(input).parents('.file-add').find('.upload-file-preview img').attr('src', e.target.result);
            croppChanged = true;
            cropImages();
        }
        reader.readAsDataURL(input.files[0]);
    }
    }
    if(autoSubmit){
        $(input).parents('form').submit();
    }
    
});
$(document).on('click', '.ajaxLink', function () {
    var elem = this;
    var _url = $(elem).attr('href');
    if (_url !== undefined && _url !== '') {
        $.ajax({
            type: 'get',
            dataType: 'json',
            url: _url,
            success: function (msg) {
                if (msg['status'] === 'success') {
                    if (msg['link'] !== undefined) {
                        $($(elem).parents()[0]).html(msg['link']);
                    }
                    if (msg['action'] !== undefined && msg['action'] !== '') {
                        switch (msg['action']) {
                            case 'reload':
                                document.location.reload();
                                break;

                            default:

                                break;
                        }
                    }
                } else {
                    if (msg['message'] !== undefined) {
                        _error(msg['message']);
                    }
                    if (msg['errors'] !== undefined) {
                    }
                }
            }
        });
    }
    return false;
});
$(document).on('click', '.add-towar-zdjecie', function () {
    $('#add-towar-zdjecie').trigger('click');
});
$(document).on('change', '#add-towar-zdjecie', function () {
    $('#add-towar-zdjecie-form').submit();
});
$(document).on('click', '.towar-zdjecie-alt button', function () {
    var input = $($(this).parents('.towar-zdjecie-alt')[0]).find('input');
    var btn = $(this);
    var _data = new Object();
    _data['id'] = input.attr('item-id');
    _data['alt'] = input.val();
    setTowarZdjecieField(_data, btn);
});
function setTowarZdjecieField(_data, btn) {
    if (btn !== undefined) {
        btn.addClass('reload');
    }
    $.ajax({
        type: 'post',
        dataType: 'json',
        data: _data,
        url: basePath + 'admin/towar-zdjecie/set-field',
        success: function (msg) {
            if (btn !== undefined) {
                btn.removeClass('reload');
            }
            if (msg['status'] !== undefined && msg['status'] === 'success' && msg['message'] !== undefined && msg['message'] !== '') {
                _success(msg['message']);
            }
            if (msg['status'] !== undefined && msg['status'] === 'error' && msg['message'] !== undefined && msg['message'] !== '') {
                _error(msg['message']);
            }
        }
    });
}
function updateSortData($sortItem)
{
    var target = $sortItem.attr('sort-target');
    var items = $sortItem.find('[sort-item-id]');
    var sortData = new Object();
    var kolejnosc = items.length;
    $.each(items, function (i, v) {
        var itemId = $(v).attr('sort-item-id');
        if (sortData[itemId] === undefined) {
            sortData[itemId] = new Object();
        }
        sortData[itemId]['id'] = itemId;
        sortData[itemId]['kolejnosc'] = kolejnosc;
        kolejnosc--;
    });
    if (target !== undefined && target !== '') {
        $.ajax({
            type: 'post',
            dataType: 'json',
            data: sortData,
            url: target,
            success: function (msg) {

            }
        });
    }
}
$(document).on('change', '.zdjecie-wariant select', function () {
    var _data = new Object();
    _data['id'] = $(this).attr('item-id');
    _data['wariant_id'] = $(this).val();
    setTowarZdjecieField(_data);
});
$(document).on('click', '.distAdd', function () {
    var div = $('.rabatDistDef').clone();
    var lp = 0;
    if ($('#wartosci_rabat_dist .rabatDistItem').length > 0) {
        lp = (parseInt($('#wartosci_rabat_dist .rabatDistItem:last-child').attr('lp')) + 1);
    } else {
        lp = 0;
    }
    div.removeClass('rabatDistDef');
    div.find('input').removeAttr('disabled');
    div.attr('lp', lp);
    $.each(div.find('input'), function (i, v) {
        $(v).attr('name', $(v).attr('name').replace('%lp%', lp));
    });
    div.appendTo($('#wartosci_rabat_dist'));
});
$(document).on('click', '.removeDistValue', function () {
    $(this).parents('.rabatDistItem').remove();
});
$(document).on('click', '.user-price-submit', function () {
    $($(this).parents('tr')[0]).find('form.user-price-form').submit();
});
$(document).on('keyup', '.user-price', function () {
    $(this).addClass('changed');
});
$(document).on('submit', 'form.user-price-form', function () {
    var btn = $($(this).parents('tr')[0]).find('.user-price-submit');
    var _data = $(this).serialize();
    var inputs = $(this).find('.user-price');
    btn.addClass('reload');
    $.ajax({
        type: 'post',
        data: _data,
        url: basePath + 'admin/uzytkownik/save-towar-cena',
        dataType: 'json',
        success: function (data, textStatus, jqXHR) {
            btn.removeClass('reload');
            if (data['status'] !== undefined && data['status'] === 'error' && data['message'] !== undefined && data['message'] !== '') {
                _error(data['message']);
            }
            if (data['deleteIds'] !== undefined) {
                console.log(data['deleteIds'].length);
                $.each(data['deleteIds'], function (i, v) {
                    $('input[cena-id=' + v + ']').attr('cena-id', '').val('');
                });
            }
            if (data['successIds'] !== undefined) {
                console.log(data['successIds'].length);
                $.each(data['successIds'], function (i, v) {
                    $('input[cena-key=' + i + ']').attr('cena-id', v).val(v);
                });
            }
            inputs.removeClass('changed').removeClass('if-save');
        }
    });
    return false;
});
window.addEventListener("beforeunload", function (e) {
    var stop = $('.user-price.changed').length;
    if (stop > 0 || blockUnload) {
        $('.user-price.changed').addClass('if-save');
        var confirmationMessage = "\o/";

        e.returnValue = confirmationMessage;     // Gecko, Trident, Chrome 34+
        return confirmationMessage;              // Gecko, WebKit, Chrome <34
    }
});
$(document).on('submit', 'form', function () {
    blockUnload = false;
});
$(document).on('submit', '.ajaxForm', function () {
    var submitBtn = $(this).find('[type=submit]');
    var _data = $(this).serialize();
    var action = $(this).attr('action');
    submitBtn.addClass('reload');
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: action,
        data: _data,
        success: function (data, textStatus, jqXHR) {
            submitBtn.removeClass('reload');
            if (data['status'] !== undefined && data['status'] === 'success') {

                if (data['message'] !== undefined && data['message'] !== '') {
                    _success(data['message']);
                }
                if (data['values'] !== undefined) {
                    $.each(data['values'], function (field, value) {
                        if ($('#' + field + ':input').length > 0) {
                            $('#' + field + ':input').val(value);
                        } else {
                            if ($('#' + field).length > 0) {
                                $('#' + field).html(value);
                            }
                        }
                    });
                }
            } else {
                if (data['message'] !== undefined && data['message'] !== '') {
                    _error(data['message']);
                }
            }
        },
        error: function (data, textStatus, errorMsg) {
            submitBtn.removeClass('reload');
            _error(errorMsg);
        }
    });
    return false;
});
function wykresZamowien() {
    var chart_zamowienia_settings = {
        series: {
            lines: {
                show: false,
                fill: true
            },
            splines: {
                show: true,
                tension: 0.4,
                lineWidth: 1,
                fill: 0.4
            },
            points: {
                radius: 0,
                show: true
            },
            shadowSize: 2
        },
        grid: {
            verticalLines: true,
            hoverable: true,
            clickable: true,
            tickColor: "#d5d5d5",
            borderWidth: 1,
            color: '#fff'
        },
        colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
        xaxis: {
            tickColor: "rgba(51, 51, 51, 0.06)",
            mode: "time",
            tickSize: [1, "day"],
            //tickLength: 10,
            axisLabel: "Date",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Verdana, Arial',
            axisLabelPadding: 10
        },
        yaxis: {
            ticks: 8,
            tickColor: "rgba(51, 51, 51, 0.06)",
            min: 0,
            max: statystykiMax
        },
        tooltip: false
    };
    if (statystykiArray !== undefined) {
        var arr_data1 = [];
        $.each(statystykiArray, function (i, v) {
            arr_data1.push([gd(v['y'], v['m'], v['d']), v['count']]);
        });
        if ($("#wykres_zamowien").length) {

            $.plot($("#wykres_zamowien"), [arr_data1], chart_zamowienia_settings);
        }
    } else {
        return false;
    }
}
function getZamowieniaStat(start, end) {
    $('#statFrom').val(start.format('Y-MM-DD'));
    $('#statTo').val(end.format('Y-MM-DD'));
    $('#indexStatForm').submit();
}
$(document).on('change', '#orderStatusForm #status', function () {
    if ($(this).val() == 'przyjete') {
        $('#orderStatusForm .inputTerminWysylki input').attr('required', 'required');
        $('#orderStatusForm .inputTerminWysylki').show();
    } else {
        $('#orderStatusForm .inputTerminWysylki').hide();
        $('#orderStatusForm .inputTerminWysylki input').removeAttr('required');
    }
});
$(document).on('change', '#orderStatusForm #status', function () {
    if ($(this).val() == 'wyslane') {
//        $('#orderStatusForm .inputListPrzewozowy input').attr('required', 'required');
        $('#orderStatusForm .inputListPrzewozowy input,#orderStatusForm .inputListPrzewozowy select').removeAttr('disabled');
        $('#orderStatusForm .inputListPrzewozowy').show();
    } else {
        $('#orderStatusForm .inputListPrzewozowy').hide();
        $('#orderStatusForm .inputListPrzewozowy input,#orderStatusForm .inputListPrzewozowy select').attr('disabled', 'disabled');
        $('#orderStatusForm .inputListPrzewozowy input,#orderStatusForm .inputListPrzewozowy select').removeAttr('required');
    }
});
$(document).on('change', '.input-ilosc', function () {
    var parent = $($(this).parents('tr')[0]);
    var ilosc = parseInt($(this).val());
    if (isNaN(ilosc) || ilosc < 0) {
        ilosc = 0;
        $(this).val(0);
    }
    if (ilosc === 0) {
        parent.find('input[type=checkbox]')[0].checked = false;
        parent.find('.cena_razem_netto').html(0);
        parent.find('.cena_razem_brutto').html(0);
    } else {
        var cena_netto = parseFloat($(this).attr('cena-netto'));
        var cena_brutto = parseFloat($(this).attr('cena-brutto'));
        var ilosc_w_kartonie = parseInt($(this).attr('ilosc-w-kartonie'));
        var szt = ilosc * ilosc_w_kartonie;
        var cena_razem_netto = round(szt * cena_netto, 2);
        var cena_razem_brutto = round(szt * cena_brutto, 2);
        parent.find('.cena_razem_netto').html(cena_razem_netto);
        parent.find('.cena_razem_brutto').html(cena_razem_brutto);
        parent.find('input[type=checkbox]')[0].checked = true;
    }
    addZamPrzelicz();
});
$(document).on('change', '.add_towar_item_checkbox', function () {
    var parent = $($(this).parents('tr')[0]);
    var input = parent.find('.input-ilosc');
    if ($(this).is(':checked')) {
        if (input.val() === '' || input.val() === '0') {
            input.val(1);
        }
    } else {
        input.val(0);
    }
    input.trigger('change');
});
function addZamPrzelicz() {
    $('#addToNewZamItems tr').removeClass('selected');
    var checkedItems = $('#addToNewZamItems input[type=checkbox]:checked');
    var sumNetto = 0;
    var sumBrutto = 0;
    $.each(checkedItems, function (i, v) {
        var parent = $($(v).parents('tr')[0]);
        var cn = parseFloat(parent.find('.cena_razem_netto').html());
        var cb = parseFloat(parent.find('.cena_razem_brutto').html());
        if (!isNaN(cn) && !isNaN(cb)) {
            parent.addClass('selected');
            sumNetto = sumNetto + cn;
            sumBrutto = sumBrutto + cb;
        }
    });
    $('.zamowienie_wartosc_razem_netto').html(round(sumNetto, 2));
    $('.zamowienie_wartosc_razem').html(round(sumBrutto, 2));
}
function round(val, prec) {
    val = parseFloat(val);
    if (isNaN(val)) {
        return val;
    }
    if (prec === undefined) {
        return Math.round(val);
    }
    var tmpPrec = '1';
    for (var i = 0; i < parseInt(prec); i++) {
        tmpPrec = tmpPrec + '0';
    }
    prec = parseFloat(tmpPrec);
    return (Math.round((val * prec)) / prec);
}

$(document).on('keyup', '.tableFinder', function () {
    var searchTerm = $(this).val();
    var _target = $(this).attr('data-target');
    if (searchTerm === '')
    {
        $(_target + ' tr').removeClass('hidden');
    } else
    {
        var searchSplit = searchTerm.replace(/ /g, "'):containsi('");
        $.extend($.expr[':'], {'containsi': function (elem, i, match, array) {
                return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
            }
        });

        $(_target + ' tr').not(":containsi('" + searchSplit + "')").each(function (e) {
            $(this).addClass('hidden');
        });

        $(_target + " tr:containsi('" + searchSplit + "')").each(function (e) {
            $(this).removeClass('hidden');
        });
    }
});
$(document).on('change', '.pokaz-wybrane', function () {
    $('.tableFinder').val('');
    $('#addToNewZamItems tr').removeClass('hidden');
    if ($(this).is(':checked')) {
        $('#addToNewZamItems tr:not(.selected)').addClass('hidden');
    }
});
$(document).on('submit', '#addZamowienieForm', function () {
    $('#addToNewZamItems input').attr('disabled', 'disabled');
    $('#addToNewZamItems tr.selected input').removeAttr('disabled');
});
$(document).on('keypress', 'input[data-type=float]', function (e) {
    var key = e.which || e.keyCode;
    if ((key >= 48 && key <= 57) || key === 44 || key === 46 || key === 8 || key === 37 || key === 38 || key === 39 || key === 40 || key === 13) {
        // 0-9 comma and dot only
    } else {
        return false;
    }
});
$(document).on('keyup change', 'input[data-type=float]', function () {
    $(this).val($(this).val().replace(',', '.'));
});
var saveGolaby = false;
$(document).on('click', '.userCenaSaveAll', function () {
    var changed = $('.user-price.changed').parents('tr');
    $(this).addClass('reload');
    if (changed.length > 0) {
        saveGolaby = true;
        $.each(changed, function (ti, tr) {
            $(tr).find('.user-price-submit').trigger('click');
        });
    }
    $(this).removeClass('reload');
});
$(document).on('keypress', 'input[data-type=phone]', function (e) {
    var key = e.which || e.keyCode;
    if ((key >= 48 && key <= 57) || key === 32 || key === 45 || key === 40 || key === 41 || key === 8 || key === 46 || key === 37 || key === 38 || key === 39) {
        // 0-9 comma and dot only
    } else {
        return false;
    }
});
$(document).on('shown.bs.tab', 'a.towar_edit_tab', function (e) {
//  e.target // newly activated tab
//  e.relatedTarget // previous active tab
    if ($(e.target).attr('hide-save') !== undefined && $(e.target).attr('hide-save') === 'true') {
        $('.x_footer').addClass('hidden');
    } else {
        $('.x_footer').removeClass('hidden');
    }
});

$(document).on('click', '.show_detalis', function () {
    if ($('.koszyk-item-detalis#item_' + $(this).attr('item')).is(':visible')) {
        $('.koszyk-item-detalis#item_' + $(this).attr('item')).removeClass('open');
    } else {
        $('.koszyk-item-detalis#item_' + $(this).attr('item')).addClass('open');
    }
});
$(document).on('click', '.showAllDetalis', function () {
    $('.koszyk-item-detalis').addClass('open');
});
$(document).on('click', '.hideAllDetalis', function () {
    $('.koszyk-item-detalis').removeClass('open');
});
$(document).on('change', '.blockDayInput', function () {
    if ($(this).val() !== '') {
        $('.saveBlockDay').addClass('show');
    } else {
        $('.saveBlockDay').removeClass('show');
    }
});
$(document).on('click', '.saveBlockDay', function () {
    var data = new Object();
    var blockDays = $('#allBlockedDays').val();
    if (blockDays !== '') {
        if (blockDays.indexOf($('.blockDayInput').val()) >= 0) {
            var isDate = $(".block-day-item[date='" + $('.blockDayInput').val() + "']");
            isDate.addClass('added');
            setTimeout(function () {
                isDate.removeClass('added');
            }, 100);
            $('.blockDayInput').val('').trigger('change');
            return false;
        }
        blockDays = blockDays + '|' + $('.blockDayInput').val();
    } else {
        blockDays = $('.blockDayInput').val();
    }
    $('#allBlockedDays').val(blockDays);
    data['typ'] = 'dostawa';
    data['nazwa'] = 'block_days';
    data['wartosc'] = blockDays;
    updateConfig(data);
    $('.blockDayInput').val('').trigger('change');
});
$(document).on('click', '.removeBlockDay', function () {
    if ($(this).parents('.block-day-item').attr('date') !== undefined && $(this).parents('.block-day-item').attr('date') !== '') {
        var cfMessage = $(this).attr('confirm-message');
        var date = $(this).parents('.block-day-item').attr('date');
        var data = new Object();
        var blockDays = $('#allBlockedDays').val();

        if (blockDays !== '') {
            if (blockDays.indexOf(date) >= 0) {
                swal({
                    title: '',
                    text: cfMessage,
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Tak',
                    cancelButtonText: 'Nie',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function () {
                    blockDays = blockDays.replace(date + '|', '');
                    blockDays = blockDays.replace('|' + date, '');
                    blockDays = blockDays.replace(date, '');
                    var isDate = $(".block-day-item[date='" + date + "']");
                    isDate.addClass('removed');
                    setTimeout(function () {
                        isDate.remove();
                    }, 100);
                    $('#allBlockedDays').val(blockDays);
                    data['typ'] = 'dostawa';
                    data['nazwa'] = 'block_days';
                    data['wartosc'] = blockDays;
                    updateConfig(data);
                }, function (dismiss) {
                    // dismiss can be 'cancel', 'overlay',
                    // 'close', and 'timer'
                    if (dismiss === 'cancel') {

                    }
                });
            } else {
                return false;
            }
        } else {
            return false;
        }
    } else {
        return false;
    }
});
//$(document).on('change', '[activate-cena-specjalna=true]', function () {
//    if ($(this).is(':checked')) {
//        $(this).parents('.tab-ceny').find('[def-disabled=true]').removeAttr('disabled').attr('required', 'reqiured');
//    } else {
//        $(this).parents('.tab-ceny').find('[def-disabled=true]').attr('disabled', 'disabled').removeAttr('required');
//    }
//});
$(document).on('change', '.on-change-submit', function () {
    $(this).parents('form').submit();
});
$(document).on('submit', '.ajax-multi-form-submit', function () {
    var form = this;
    var data = $(form).serialize();
    var url = $(form).attr('action');
    var type = $(form).attr('method');
    var dataType = 'json';
    if ($(form).attr('data-type') !== undefined && $(form).attr('data-type') !== '') {
        dataType = $(form).attr('data-type');
    }
    $(form).addClass('reload');
    $.ajax({
        type: type,
        dataType: dataType,
        url: url,
        data: data,
        success: function (data, textStatus, jqXHR) {
            $(form).removeClass('reload');
            if (data['status'] !== undefined && data['status'] === 'success') {
                if (data['message'] !== undefined && data['message'] !== '') {
                    _success(data['message'])
                }
            } else {
                if (data['message'] !== undefined && data['message'] !== '') {
                    _error(data['message'])
                }
            }
        },
        error: function (data, textStatus, message) {
            $(form).removeClass('reload');
            _error(message);
        }
    });
    return false;
});

$(document).on('click', '.ajaxLinkPopUp', function () {
    var elem = this;
    var _url = $(elem).attr('href');
    var modalTarget = $(elem).attr('modal-target');
    if (_url !== undefined && _url !== '') {
        $.ajax({
            type: 'get',
            url: _url,
            success: function (msg) {
                $(msg).appendTo($('body'));
                if (modalTarget !== undefined && modalTarget !== '') {
                    $(modalTarget).modal('show');
                    $('textarea[summernote=true]').summernote({
                        height: 200,
                        tabsize: 2,
                        toolbar: [
                            ['style', ['bold', 'italic', 'underline', 'clear']],
                            ['font', ['strikethrough', 'superscript', 'subscript']],
                            ['fontsize', ['fontsize']],
                            ['color', ['color']],
                            ['para', ['ul', 'ol', 'paragraph']],
                            ['height', ['height']],
                            ['insert', ['hr', 'table', 'link', 'video', 'elfinder']],
                            ['misc', ['undo', 'redo', 'codeview', 'fullscreen']]
                        ],
                        onCreateLink: function (link) {
                            return link;
                        }
                    });
                }
            },
            error: function (data, textStatus, errorMsg) {
                _error(errorMsg);
            }
        });
    }
    return false;
});
$(document).on('hidden.bs.modal', '.modalRemoveOnClose', function () {
    $(this).remove();
});
$(document).on('click', '.szablon-odpowiedz', function () {
    var target = $(this).attr('data-target');
    var tresc = '';
    if ($('#dostepny').length > 0 && $('#dostepny').val() === '0') {
        tresc = $('#szablon-zapytanie-nie').val();
    } else {
        if ($('#szablon-zapytanie-tak').length > 0) {
            tresc = $('#szablon-zapytanie-tak').val();
        } else if ($('#szablon-zapytanie').length > 0) {
            tresc = $('#szablon-zapytanie').val();
        }
    }
    $(target).summernote('code', tresc);
});
$(document).on('submit', '.ajaxFormModal', function () {
    var submitBtn = $(this).find('[type=submit]');
    var _data = $(this).serialize();
    var action = $(this).attr('action');
    var modalTarget = $(this).attr('modal-target');
    submitBtn.addClass('reload');
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: action,
        data: _data,
        success: function (data, textStatus, jqXHR) {
            submitBtn.removeClass('reload');
            if (data['status'] !== undefined && data['status'] === 'success') {

                if (data['message'] !== undefined && data['message'] !== '') {
                    _success(data['message']);
                }
                if (data['action'] !== undefined && data['action'] !== '') {
                    var fn = window[data['action']];
                    if (typeof fn === "function")
                    {
                        fn(data);
                    }
                }
                if (modalTarget !== undefined && modalTarget !== '') {
                    $(modalTarget).modal('hide');
                }
            } else {
                if (data['message'] !== undefined && data['message'] !== '') {
                    _error(data['message']);
                }
            }
        },
        error: function (data, textStatus, errorMsg) {
            submitBtn.removeClass('reload');
            _error(errorMsg);
        }
    });
    return false;
});
function powiadomienieSetWysylkaDate(dane) {
    if (dane !== undefined) {
        if (dane['dostepnosc-item-id'] !== undefined) {
            if (dane['wyslane'] !== undefined) {
                $('td.wyslane[dostepnosc-item-id="' + dane['dostepnosc-item-id'] + '"]').html(dane['wyslane']);
                $('[data-toggle=popover]').popover();
            }
            if (dane['countPremiery'] !== undefined) {
                $('#labelCountPremiery').html(dane['countPremiery']);
            }
            if (dane['countDostepnosc'] !== undefined) {
                $('#labelCountDostepnosc').html(dane['countDostepnosc']);
            }
            checkAlerts();
        }
    }
}
function zapytanieSetWysylkaDate(dane) {
    if (dane !== undefined) {
        if (dane['zapytanie-item-id'] !== undefined) {
            if (dane['wyslane'] !== undefined) {
                $('td.wyslane[zapytanie-item-id="' + dane['zapytanie-item-id'] + '"]').html(dane['wyslane']);
                $('[data-toggle=popover]').popover();
            }
            if (dane['countNaZamowienie'] !== undefined) {
                $('#labelCountNaZamowienie').html(dane['countNaZamowienie']);
            }
            checkAlerts();
        }
    }
}
$(document).on('click', '.radio-unselect', function () {
    var target = $(this).attr('target');
    if (target !== undefined && target !== '') {
        $(target)[0].checked = false;
        $(target).removeAttr('checked');
        $(target).trigger('change');
    }
});
$(document).on('change', 'input[check-all]', function () {
    var check = $(this).attr('check-all');
    if ($(this).is(':checked')) {
        if ($('input[check-item="' + check + '"]:not(:checked)').length > 0) {
            $.each($('input[check-item="' + check + '"]:not(:checked)'), function (i, v) {
                v.checked = true;
            });
        }
    } else {
        if ($('input[check-item="' + check + '"]:checked').length > 0) {
            $.each($('input[check-item="' + check + '"]:checked'), function (i, v) {
                v.checked = false;
            });
        }
    }
});
$(document).on('change', 'input[check-item]', function () {
    if (!$(this).is(':checked')) {
        var check = $(this).attr('check-item');
        if ($('input[check-all="' + check + '"]').length > 0) {
            $.each($('input[check-all="' + check + '"]'), function (i, v) {
                v.checked = false;
            });
        }
    }
});
$(document).on('click', '.add-new-map', function () {
    var katId = $(this).attr('kat-id');
    var defKey = $(this).attr('key');
    var addKey = $(this).attr('add-key');
    var hurtownia = $(this).attr('hurtownia');
    var defContent = $('#map-default').html();
    defContent = defContent.replaceAll('_hurtownia_', hurtownia);
    defContent = defContent.replaceAll('_key_', defKey + addKey);
    defContent = defContent.replaceAll('_katid_', katId);
    $(defContent).appendTo($('.map-container[kat-id="' + katId + '"][hurtownia="' + hurtownia + '"]'));
    addKey = (parseInt(addKey) + 1);
    $(this).attr('add-key', addKey);
    blockUnload = true;
});
$(document).on('change', '.map-item input[type=text]', function () {
    blockUnload = true;
});
$(document).on('click', '.map-item .remove', function () {
    var yesBtn = (($(this).attr('confirm-btn-yes') !== undefined) ? $(this).attr('confirm-btn-yes') : 'Tak');
    var noBtn = (($(this).attr('confirm-btn-no') !== undefined) ? $(this).attr('confirm-btn-no') : 'Nie');
    var key = $(this).attr('row-key');
    var cfMessage = $(this).attr('confirm-message');
    var item = this;
    var valItem = $(item).parents('.map-item').find('input[type=text]').val();
    if (valItem !== undefined && valItem !== '') {
        swal({
            title: '',
            text: cfMessage,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: yesBtn,
            cancelButtonText: noBtn,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function () {
            $(item).parents('.map-item').remove();
            blockUnload = true;
        }, function (dismiss) {
            // dismiss can be 'cancel', 'overlay',
            // 'close', and 'timer'
            if (dismiss === 'cancel') {

            }
        });
    } else {
        $(item).parents('.map-item').remove();
    }
});

var cropImg = [];
function cropImages(subClass) {
    if (subClass === undefined) {
        subClass = '';
    }
    var autoload = false;
    if (subClass !== '') {
        autoload = true;
    }
    if ($('.crop-img' + subClass).length > 0) {
        $.each($('.crop-img' + subClass), function (i, v) {
            cropImg[i] = $(v).find('img');
            var imgWidth = parseInt($(v).attr('def-width'));
            var imgHeight = parseInt($(v).attr('def-height'));
            var containerW = $(v).width();
            var containerH = (containerW * imgHeight) / imgWidth;
            console.log(imgWidth, imgHeight);
            cropImg[i].cropper({
                aspectRatio: imgWidth / imgHeight,
                scalable: false,
                cropBoxResizable: true,
                autoCropArea: 1,
                autoCrop: true,
                crop: function (event) {
                    if (!autoload) {
                        croppChanged = true;
                    }
                    autoload = false;
                    console.log(event.detail.x);
                    console.log(event.detail.y);
                    console.log(event.detail.width);
                    console.log(event.detail.height);
                    console.log(event.detail.rotate);
                    console.log(event.detail.scaleX);
                    console.log(event.detail.scaleY);
                }
            });
            if ($(v).find('.cropp-options').length > 0) {
                $(v).find('.cropp-options').remove();
            }
            $('<div class="cropp-options"><span data-toggle="tooltip" title="Powiększ" class="zoom zoom-in" zoom="in" index="' + i + '"><i class="fa fa-search-plus"></i></span><span data-toggle="tooltip" title="Pomiejsz" class="zoom zoom-out" zoom="out" index="' + i + '"><i class="fa fa-search-minus"></i></span><span data-toggle="tooltip" title="Resetuj" class="reset" index="' + i + '" onclick="cropReset(' + i + ');"><i class="fa fa-refresh"></i></span></div>').appendTo($(v));
            $('.cropp-options [data-toggle="tooltip"]').tooltip();
        });
    }
    return true;
}
function getCrop() {
    if (cropImg.length > 0) {
        $.each(cropImg, function (i, v) {
            console.log(mimeType);
            var imgMaxW = parseInt($(v).parents('form').attr('img-max-w'));
            var imgMaxH = parseInt($(v).parents('form').attr('img-max-h'));
            var cropConfig = {};
            if (isNaN(imgMaxW)) {
                imgMaxW = 0;
            }
            if (isNaN(imgMaxH)) {
                imgMaxH = 0;
            }
            if (imgMaxW !== 0 || imgMaxH !== 0) {
                if (imgMaxH > imgMaxW) {
                    cropConfig = {maxHeight: imgMaxH};
                } else if (imgMaxW > 0) {
                    cropConfig = {maxWidth: imgMaxW};
                }
            }
            if (mimeType === undefined) {
                mimeType = dataURLtoMimeType($(v).cropper('getCroppedCanvas', cropConfig).toDataURL());
            }
            var cropper = $(v).cropper('getCroppedCanvas', cropConfig).toDataURL(mimeType);
            var iName = $(v).parents('.file-add').find('input[type="file"]').attr('name') + '_crop';
            if (croppChanged) {
                $(v).parents('form').find('input[name="mime"]').remove();
                $(v).parents('form').find('input[name="' + iName + '"]').remove();
                $('<input type="hidden" name="mime" value="' + mimeType + '"/>').appendTo($(v).parents('form'));
                $('<input type="hidden" name="' + iName + '" value="' + cropper + '"/>').appendTo($(v).parents('form'));
            }
        });
    } else {
        return false;
    }
}
$(document).on('submit', '.crop-form', function () {
    getCrop();
});

function dataURLtoMimeType(dataURL) {
    var BASE64_MARKER = ';base64,';
    var data;

    if (dataURL.indexOf(BASE64_MARKER) == -1) {
        var parts = dataURL.split(',');
        var contentType = parts[0].split(':')[1];
        data = decodeURIComponent(parts[1]);
    } else {
        var parts = dataURL.split(BASE64_MARKER);
        var contentType = parts[0].split(':')[1];
        var raw = window.atob(parts[1]);
        var rawLength = raw.length;

        data = new Uint8Array(rawLength);

        for (var i = 0; i < rawLength; ++i) {
            data[i] = raw.charCodeAt(i);
        }
    }

    var arr = data.subarray(0, 4);
    var header = "";
    for (var i = 0; i < arr.length; i++) {
        header += arr[i].toString(16);
    }
    switch (header) {
        case "89504e47":
            mimeType = "image/png";
            break;
        case "47494638":
            mimeType = "image/gif";
            break;
        case "ffd8ffe0":
        case "ffd8ffe1":
        case "ffd8ffe2":
            mimeType = "image/jpeg";
            break;
        default:
            mimeType = ""; // Or you can use the blob.type as fallback
            break;
    }

    return mimeType;
}
function zoom(ci, z) {
    if (cropImg[ci] !== undefined) {
        cropImg[ci].cropper('zoom', z);
    }
}
function cropReset(ci) {
    if (cropImg[ci] !== undefined) {
        cropImg[ci].cropper('reset');
    }
}
$(document).on('click', '.cropp-options .zoom', function () {
    var index = $(this).attr('index');
    var zoomType = $(this).attr('zoom');
    if (zoomType == 'in') {
        zoom(index, 0.1);
    } else {
        zoom(index, -0.1);
    }
});

$(document).on('change', '#options-by-type', function () {
    $('.options-item-type').removeClass('active');
    $('.options-item-type :input').attr('disabled', true);
    $('.options-item-type[data-type=' + $(this).val() + ']').addClass('active');
    $('.options-item-type[data-type=' + $(this).val() + '] :input').removeAttr('disabled');
});
function callBackUpdate(data) {
    console.log(data);
    if (data['status'] !== undefined && data['status'] === 'success') {
        $('#labelCountCallback').html(data['callBackCount']);
        $('tr[call-id="' + data['callId'] + '"] .call_status').html(data['call_status']);
        $('tr[call-id="' + data['callId'] + '"] .call_admin').html(data['call_admin']);
        $('tr[call-id="' + data['callId'] + '"] .call_data').html(data['call_data']);
        $('tr[call-id="' + data['callId'] + '"] .call_notatki').html(data['call_notatki']);
        checkAlerts();
    }
}
$(document).on('change','[change-action]',function(){
    var actionTarget = $(this).attr('change-action');
    var actionValue = $(this).val();
    $(actionTarget).removeClass('active');
    $(actionTarget+' :input').attr('disabled',true);
    $('.show-on-'+actionValue).addClass('active');
    $('.show-on-'+actionValue+' :input').removeAttr('disabled');
});