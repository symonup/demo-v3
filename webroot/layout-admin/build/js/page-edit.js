$(document).ready(function () {
    var htmlControls='<div class="item-controls">'
            +'<button type="button" data-toggle="tooltip" data-placment="top" title="Edytuj blok" class="btn btn-xs btn-info item-edit"><i class="fa fa-pencil"></i></button>'
            +'<button type="button" data-toggle="tooltip" data-placment="top" title="Usuń blok" class="btn btn-xs btn-danger item-delete"><i class="fa fa-trash"></i></button>'
            +'<span data-toggle="tooltip" data-placment="top" title="Przesuń blok" class="btn btn-xs btn-default item-move"><i class="fa fa-arrows-alt"></i></span>'
            +'</div>';
    if($('.page-edit-content').length>0){
    var elements = $('.page-edit-content')[0].childNodes;
    
    $.each(elements, function (index, elem) {
        if (!$(elem).hasClass('page-element')) {
            var tmp = elem;
            if (elem.nodeName === '#text') {
                $('<div class="page-element html-item">HTML'+htmlControls+'<div class="item-content"><textarea>' + $(elem).text() + '</textarea></div></div>').insertBefore($(elem));
                $(elem).remove();
            }else{
                var newElem=$('<div class="page-element html-item">HTML'+htmlControls+'<div class="item-content"><textarea></textarea></div></div>');
                newElem.insertBefore($(elem));
                newElem.find('textarea').val($(elem)[0].outerHTML);
                $(elem).remove();
            }
        }
    });
    }
        afterOptions();
});
$(document).on('click','.page-element.html-item > .item-controls > .item-edit',function(){
    var parent=$($(this).parents('.page-element')[0]);
    if(!parent.hasClass('edit')){
        parent.addClass('edit');
    }else{
        parent.removeClass('edit');
    }
});
$(document).on('click','.add-new-page-blok',function(){
    $.ajax({
        type: 'GET',
        url: "/admin/strona/get-blok-list",
        success: function (data, textStatus, jqXHR) {
            $(data).appendTo('body');
            $('#blok-list-modal').modal('show');
        }
    });
});
$(document).on('hide.bs.modal','#blok-list-modal',function(){
    $('#blok-list-modal').remove();
});
$(document).on('click','.select-blok-item',function(){
    $('.select-blok-item').removeClass('selected');
    $(this).addClass('selected');
});
$(document).on('click','#select-blok-item',function(){
    var selectedItem=$('.select-blok-item.selected');
    if(selectedItem.length>0){
        $.each(selectedItem,function(i,v){
            $($(v).html()).appendTo($('.page-edit-content'));
        });
        afterOptions();
            $('#blok-list-modal').modal('hide');
    }else{
        $('.select-blok-item > div').css('border-color','#ff0000');
        setTimeout(function(){
            $('.select-blok-item > div').removeAttr('style');
        },1000);
        _error('Wybierz blok do dodania!');
    }
});
$(document).on('click','.item-delete',function(){
    var parent=$($(this).parents('.page-element')[0]);
    swal({
        title: '',
        text: 'Czy chcesz usunąć blok?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Tak',
        cancelButtonText: 'Nie',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {
        parent.fadeOut('fast',function(){
            parent.remove();
        });
    }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if (dismiss === 'cancel') {

        }
    });
});
$(document).on('submit','form.pageForm',function(){
    $('#tresc').val(getPageHtml());
});
$(document).on('click','.page-preview',function(){
    $('form#previewPage #prevwiew-tresc').val(getPageHtml());
    $('form#previewPage').submit();
    return false;
});
$(document).on('click','.item-controls > *',function () {
   $(this).tooltip('hide'); 
});
function getPageHtml(){
    var pageElements=$('.page-edit-content > .page-element');
    var htmlContent='';
    $.each(pageElements,function(i,element){
        if($(element).hasClass('html-item')){
            var tmpHtml=$(element).find('textarea').summernote('code');
            if($(tmpHtml).hasClass('html-content')){
            htmlContent+=tmpHtml;
            }
            else{
                htmlContent+='<div class="html-content">'+tmpHtml+'</div>';
            }
        }else{
            htmlContent+=$(element).attr('item-id');
        }
    });
    return htmlContent;
}
function afterOptions(){
    $('[data-toggle=tooltip').tooltip();
    $('.page-edit-content textarea:not(.note-codable)').summernote({
        height: 200,
        tabsize: 2,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['insert', ['hr', 'table', 'link', 'video', 'elfinder']],
            ['misc', ['undo', 'redo', 'codeview', 'fullscreen']]
        ],
        onCreateLink: function (link) {
            return link;
        }
    });
    $('.page-edit-content').sortable({
        containerSelector: 'div.page-edit-content',
        itemSelector: '.page-element',
        handle: ".item-move",
        start: function (event, ui) {
            $('.item-move').tooltip('hide');
        }
    });
}