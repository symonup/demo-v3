$(document).ready(function(){
    $.each($('.wymiana_value input[type=radio]'),function(index,value){
       if($(value).is(':checked')){
           $(value).trigger('change');
       } 
    });
    $('div.elementy_zestawu_sortable').sortable({
        containerSelector: 'div.elementy_zestawu_sortable',
        itemSelector: 'div.zestaw_element',
        placeholderClass: 'item-sort',
        placeholder: '<div class="item-sort glyphicon glyphicon-share-alt"></div>',
        handle:'.zestaw-order-handle',
        onDrop: function($item, container, _super) {
            var $clonedItem = $('<div/>').css({height: 'auto'});
            $item.before($clonedItem);
            $clonedItem.animate({'height': $item.height()});

            $item.animate($clonedItem.position(), function() {
                $clonedItem.detach();
                _super($item, container);
            });
            updateSortData();
        },
        // set $item relative to cursor position
        onDragStart: function($item, container, _super) {
            var offset = $item.offset(),
                    pointer = container.rootGroup.pointer;

            adjustment = {
                left: pointer.left - offset.left,
                top: pointer.top - offset.top
            };

            _super($item, container);
        },
        onDrag: function($item, position) {
            $item.css({
                left: position.left - adjustment.left,
                top: position.top - adjustment.top
            });
        }
    });
});
$(document).on('keyup', '#findElement', function() {
    if (xhrSearch != undefined) {
        xhrSearch.abort();
    }
    if($(this).val()===''){
            $('#findResult').html('');
            $('.searching').html('');
    }else{
    $('.searching').html('<span aria-hidden="true" class="glyphicon glyphicon-refresh animate"></span> searching');
    var adres = document.location.pathname.split('/');
    var ajaxAdr = [adres[0], adres[1], adres[2]];
    ajaxAdr = ajaxAdr.join('/') + '/find_element/' + $(this).val();
    xhrSearch = $.ajax({
        type: 'get',
        url: ajaxAdr,
        success: function(msg) {
            $('#findResult').html(msg);
            $('.searching').html('');
        }
    });
    }
});
$(document).on('click', '.find_element button', function() {
    var towar_id = $(this).attr('towar_id');
    var adres = document.location.pathname.split('/');
    var ajaxAdr = [adres[0], adres[1], adres[2]];
    ajaxAdr = ajaxAdr.join('/') + '/add_element/' + towar_id;
    $.ajax({
        type: 'get',
        url: ajaxAdr,
        success: function(msg) {
            $(msg).appendTo($('#elementy_zestawu'));
        }
    });
});
$(document).on('change', '.wymiana_value input[type=radio]', function() {
    var towar_id = $(this).parents('div.wymiana_value').attr('towar_id');
    $('#wymiana_' + towar_id + ' > *').hide();
    if ($(this).val() == 1)
    {
        if ($('#wymiana_' + towar_id + ' > .zestaw_wymiana_kategorie select').length > 0)
            $('#wymiana_' + towar_id + ' > .zestaw_wymiana_kategorie select').attr('disabled', 'disabled');
        if ($('#wymiana_' + towar_id + ' > .zestaw_wymiana_lista .similar_products input').length > 0)
            $('#wymiana_' + towar_id + ' > .zestaw_wymiana_lista .similar_products input').removeAttr('disabled');
        if ($('#wymiana_' + towar_id + ' > .zestaw_wymiana_lista').length > 0)
            $('#wymiana_' + towar_id + ' > .zestaw_wymiana_lista').show();
    }
    else
    if ($(this).val() == 2)
    {
        if ($('#wymiana_' + towar_id + ' > .zestaw_wymiana_kategorie select').length > 0)
            $('#wymiana_' + towar_id + ' > .zestaw_wymiana_kategorie select').removeAttr('disabled');
        if ($('#wymiana_' + towar_id + ' > .zestaw_wymiana_lista .similar_products input').length > 0)
            $('#wymiana_' + towar_id + ' > .zestaw_wymiana_lista .similar_products input').attr('disabled', 'disabled');
        if ($('#wymiana_' + towar_id + ' > .zestaw_wymiana_kategorie').length > 0)
            $('#wymiana_' + towar_id + ' > .zestaw_wymiana_kategorie').show();
    }
    else
    {
        if ($('#wymiana_' + towar_id + ' > .zestaw_wymiana_kategorie select').length > 0)
            $('#wymiana_' + towar_id + ' > .zestaw_wymiana_kategorie select').attr('disabled', 'disabled');
        if ($('#wymiana_' + towar_id + ' > .zestaw_wymiana_lista .similar_products input').length > 0)
            $('#wymiana_' + towar_id + ' > .zestaw_wymiana_lista .similar_products input').attr('disabled', 'disabled');
    }
});
$(document).on('click','.remove_zestaw_towar',function(){
    if(confirm($(this).attr('data-confirm')))
        {
            $(this).parents('div.zestaw_element').remove();
        }
});
$(document).on('change','#selectZestawItem',function(){
    document.location=$(this).attr('data-target')+$(this).val();
});