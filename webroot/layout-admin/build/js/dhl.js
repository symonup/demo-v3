$(document).ready(function(){
    if($('#dhl-rodzaj-paczki').length>0){
        checkDhlType();
    }
    if($('#pickupdate').length>0){
        checkPickUpDate();
    }
});
var xhrZamDhl;
$(document).on('click', 'a#dhl_book', function() {
    var checkbox = $('div.zamowienie.index table input[type="checkbox"]:checked:not(#select_all_checkbox), [book_info=true]');
    var checkCount = $('div.zamowienie.index table input[type="checkbox"]:checked:not(#select_all_checkbox)').length;
    var action = $(this).attr('data-action');
    if (checkCount >= 1) {
        if (xhrZamDhl != undefined) {
            xhrZamDhl.abort();
        }
        xhrZamDhl = $.ajax({
            type: "POST",
            dataType: 'json',
            url: action,
            data: checkbox.serialize(),
            success: function(msg) {
                if(msg['status']!==undefined && msg['status']==='success'){
                    document.location.reload();
                }else{
                    if(msg['message']!==undefined && msg['message']!==''){
                        alert(msg['message']);
                    }
                }
            }
        });
        return false;
    }
    else
        alert('zaznacz minimum 1 zamówienie');
});
$(document).on('change','#dhl-rodzaj-paczki',function(){
    checkDhlType();
});
function checkDhlType(){
    var rodzaj = $('#dhl-rodzaj-paczki').val();
    if(rodzaj!=='ENVELOPE'){
        $('.dhl-rodzaj-paczki-required').attr('required','required');
    }else{
        $('.dhl-rodzaj-paczki-required').removeAttr('required','required');
    }
}
$(document).on('change','#pickupdate',function(){
    checkPickUpDate();
});
function checkPickUpDate(){
    var date = $('#pickupdate').val();
    if(date!==''){
        $('div.zamowienie.index table input[type="checkbox"]:not(#select_all_checkbox)').attr('disabled','disabled');
        $('div.zamowienie.index table input[type="checkbox"][data-nadania="'+date+'"]').removeAttr('disabled');
    }else{
        $('div.zamowienie.index table input[type="checkbox"]').removeAttr('disabled');
    }
    
}