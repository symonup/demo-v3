$(document).on('click','#addLayoutSchemaColorInputs',function(){
    var key=0;
    if($('#layoutSchemaFormContainer > div[item-key]').length>0){
        key = parseInt($('#layoutSchemaFormContainer > div[item-key]:last-child').attr('item-key'))+1;
        if(isNaN(key)){
            key=$('#layoutSchemaFormContainer > div[item-key]').length+1;
        }
    }
    $($('#clearLayoutSchemaColorForm').html().replaceAll('%key%',key)).appendTo($('#layoutSchemaFormContainer'));
});
$(document).on('click','.removeLayoutSchemaColorInputs',function(){
    var item = this;
    if($('#layoutSchemaFormContainer > div[item-key]').length>1){
    var yesBtn = (($(item).attr('confirm-btn-yes') !== undefined) ? $(item).attr('confirm-btn-yes') : 'Tak');
    var noBtn = (($(item).attr('confirm-btn-no') !== undefined) ? $(item).attr('confirm-btn-no') : 'Nie');
    var cfMessage = $(item).attr('confirm-message');
    swal({
        title: '',
        text: cfMessage,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: yesBtn,
        cancelButtonText: noBtn,
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {
        $('#layoutSchemaFormContainer > div[item-key='+$(item).attr('item-key')+']').remove();
    }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if (dismiss === 'cancel') {

        }
    });
    }
});
