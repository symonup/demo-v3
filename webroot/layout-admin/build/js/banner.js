
$(document).ready(function () {
    if($('.slide-is-linked').length>0){
        $('.slide-is-linked').trigger('change');
    }
    
    if ($('#slide-type').length > 0) {
        $('#slide-type').trigger('change');
    }
    if ($('#slide-is-linked').length > 0) {
        $('#slide-is-linked').trigger('change');
    }
    checkBannerPreview();
    $('#slide-elements-container .panel-collapse').removeClass('in');
});
$(window).resize(function () {
    checkBannerPreview();
});
function checkBannerPreview() {
    if ($('#banner-preview').length > 0) {
        var winHeight = $(window).height();
        var prevHeight = $('#banner-preview').height();
        var bodyPadding = prevHeight;
        if (prevHeight > (winHeight / 2)) {
            bodyPadding = (winHeight / 2);
            $('#banner-preview').css('max-height', bodyPadding).css('overflow', 'auto');
        } else {
            $('#banner-preview').removeAttr('style');
        }
        $('body').css('padding-bottom', bodyPadding);
    }
}
$(document).on('change', '#slide-type', function () {
    $('.banner-slide-type:not([data-type=link])').removeClass('active');
    $('.banner-slide-type[data-type=' + $(this).val() + ']').addClass('active');
});
$(document).on('change', '#slide-is-linked', function () {
    if ($(this).is(':checked')) {
        $('.banner-slide-type[data-type=link]').addClass('active');
    } else {
        $('.banner-slide-type[data-type=link]').removeClass('active');
    }
});

$(document).on('click', '#add-next-slide-element', function () {
    var newSlideElement = $('#slide-element-container-slideelementnumber').clone();
    var next = $('#slide-elements-container > .panel').length;
    if (next > 0) {
        next = parseInt($('#slide-elements-container > .panel:last-child').attr('pos')) + 1;
    }
    newSlideElement.attr('id', newSlideElement.attr('id').replace('slideelementnumber', next)).attr('pos', next);
    var newSlideElementHtml = newSlideElement.html();
    newSlideElementHtml = newSlideElementHtml.replaceAll('slideelementnumberplus1', (next + 1));
    newSlideElementHtml = newSlideElementHtml.replaceAll('slideelementnumber', next);
    newSlideElement.html(newSlideElementHtml);
    newSlideElement.appendTo($('#slide-elements-container'));
    if ($('#slide-element-container-' + next).find('textarea[code-mirror-css]').length > 0) {
        $.each($('#slide-element-container-' + next).find('textarea[code-mirror-css]'), function (index, mirror) {
            var nextMirr = index;
            if (Object.keys(codeMirrorsCss).length > 0) {
                nextMirr = Object.keys(codeMirrorsCss).length;
            }
            codeMirrorsCss[nextMirr] = CodeMirror.fromTextArea(mirror, {
                mode: "css",
                lineNumbers: true
            });
        });
    }
    if ($('#slide-element-container-' + next).find('textarea[code-mirror-html]').length > 0) {
        $.each($('#slide-element-container-' + next).find('textarea[code-mirror-html]'), function (index, mirror) {
            var nextMirr = index;
            if (Object.keys(codeMirrorsCss).length > 0) {
                nextMirr = Object.keys(codeMirrorsCss).length;
            }
            codeMirrorsHtml[nextMirr] = CodeMirror.fromTextArea(mirror, {
                mode: "xml",
                htmlMode: true,
                lineNumbers: true
            });
        });
    }
    if ($('#slide-element-container-' + next).find('textarea[code-mirror-js]').length > 0) {
        $.each($('#slide-element-container-' + next).find('textarea[code-mirror-js]'), function (index, mirror) {
            var nextMirr = index;
            if (Object.keys(codeMirrorsJs).length > 0) {
                nextMirr = Object.keys(codeMirrorsJs).length;
            }
            codeMirrorsJs[nextMirr] = CodeMirror.fromTextArea(mirror, {
                mode: "javascript",
                lineNumbers: true
            });
        });
    }
});
$(document).on('click', '.remove-slide-element', function () {
    if (confirm($(this).attr('confirm'))) {
        $('#newLink_' + $(this).parents('.panel-slide-element').attr('pos')).remove();
        $($(this).parents('div.panel-slide-element').find('[slide-element-cord-left]').attr('slide-element-cord-left')).remove();
        $(this).parents('div.panel-slide-element').remove();
    }
});
$(document).on('click', '.file-add > button', function () {
    $($(this).parents('.file-add')[0]).find('input[type=file]').trigger('click');
});
$(document).on('change', '.file-add > input[type=file]', function () {
    var input = this;
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $($(input).parents('.file-add')[0]).find('.upload-file-preview > img').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
});
String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};
$(document).on('shown.bs.collapse', '.panel-collapse', function () {
    if (ctLink !== undefined) {
        ctLink = $($(this).parents('.panel-slide-element')[0]).attr('pos');
    }
});
$(document).on('hide.bs.collapse', '.panel-collapse', function () {
    ctLink = '';
});
function showCoords(c)
{
    $('#crW').html(c.w);
    $('#crH').html(c.h);
    $('#crT').html(c.y);
    $('#crL').html(c.x);
    var left = (c.x * 100) / imgWidth;
    var top = (c.y * 100) / imgHeight;
    var width = c.w;//(c.w * 100) / imgWidth;
    var height = c.h;//(c.h * 100) / imgHeight;
    $('#banner-slides-elements-' + ctLink + '-pos-left').val(left);
    $('#banner-slides-elements-' + ctLink + '-pos-top').val(top);
    $('#banner-slides-elements-' + ctLink + '-width').val(width);
    $('#banner-slides-elements-' + ctLink + '-height').val(height);
    if ($('#banner-slides-elements-' + ctLink + '-text').length > 0)
    {
        if ($('#newLink_' + ctLink) && $('#newLink_' + ctLink).length > 0)
        {
            var newLink = $('#newLink_' + ctLink);
            newLink.attr('style', 'position:absolute; width:' + c.w + 'px;height:' + c.h + 'px; top:' + c.y + 'px; left:' + c.x + 'px;');

        } else {
            var newLink = $('<div class="linkArea" id="newLink_' + ctLink + '" style="position:absolute; width:' + c.w + 'px;height:' + c.h + 'px; top:' + c.y + 'px; left:' + c.x + 'px;"><span ' + (($('#banner-slides-elements-' + ctLink + '-css').val() != '') ? 'style="' + $('#banner-slides-elements-' + ctLink + '-css').val() + '"' : '') + '>' + (($('#banner-slides-elements-' + ctLink + '-text').val() != '') ? $('#banner-slides-elements-' + ctLink + '-text').val() : 'Link ' + ctLink) + '</span></div>');
            newLink.appendTo($('.jcrop-holder'));
        }
    }
}
$(document).on('shown.bs.collapse', '.panel-slide-element', function () {
    editElem($(this).attr('pos'));
});
function editElem(id){
    ctLink=id;
    var left=parseFloat($('#banner-slides-elements-' + ctLink + '-pos-left').val());
    var top=parseFloat($('#banner-slides-elements-' + ctLink + '-pos-top').val());
    var proc=(($('.jcrop-holder').width()*100)/imgWidth)/100;
    left = ($('.jcrop-holder').width()*left)/100;
    top = ($('.jcrop-holder').height()*top)/100;
    var w=parseFloat($('#banner-slides-elements-' + ctLink + '-width').val());
    var h=parseFloat($('#banner-slides-elements-' + ctLink + '-height').val());
}
$(document).on('hide.bs.collapse', '.panel-slide-element', function () {
    ctLink=parseInt($('.panel-slide-element:last-child').attr('pos'))+1;
});
$(document).on('change','.slide-is-linked',function(){
    if($(this).is(':checked')){
        $(this).parents('.panel-slide-element').find('.banner-slide-element-type[data-type=link]').show();
    }else{
        $(this).parents('.panel-slide-element').find('.banner-slide-element-type[data-type=link]').hide();
    }
    
});
$(document).on('keyup', '.panel-slide-element', function() {
    $.each(codeMirrorsHtml,function(i,v){
        v.save();
    });
    $.each(codeMirrorsCss,function(i,v){
        v.save();
    });
                if ($('#newLink_' + $(this).attr('pos')).length > 0)
                {
                        $('#newLink_' + $(this).attr('pos') + ' > div').attr('style', $('#banner-slides-elements-'+$(this).attr('pos')+'-css').val().replaceAll("\n\r",''));
                    $('#newLink_' + $(this).attr('pos') + ' > div').html($('#banner-slides-elements-'+$(this).attr('pos')+'-text').val().replaceAll("\n\r",''));
                }
            });