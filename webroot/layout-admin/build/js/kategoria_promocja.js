var xhrSearch;
$(document).on('keyup', '#findElement', function() {
    if (xhrSearch != undefined) {
        xhrSearch.abort();
    }
    if($(this).val()===''){
            $('#findResult').html('');
            $('.searching').html('');
    }else{
    $('.searching').html('<span aria-hidden="true" class="glyphicon glyphicon-refresh animate"></span> searching');
    var ajaxAdr = $(this).attr('data-url');
    ajaxAdr = ajaxAdr+'/' + $(this).val();
    xhrSearch = $.ajax({
        type: 'get',
        url: ajaxAdr,
        success: function(msg) {
            $('#findResult').html(msg);
            $('.searching').html('');
        }
    });
    }
});

$(document).on('click', '.find_element button', function() {
    var ajaxAdr = $(this).attr('data-url');
    $.ajax({
        type: 'get',
        url: ajaxAdr,
        success: function(msg) {
            $('#promotion_item').html(msg);
        }
    });
});