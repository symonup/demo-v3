$(document).on('mouseover', '#new_stars > span', function () {
    $('#new_stars > span').removeClass('active');
    var ocena = parseInt($(this).attr('ocena'));
    for (var i = 1; i <= ocena; i++)
    {
        $('#new_stars > span[ocena=' + i + ']').addClass('active');
    }
});
$(document).on('mouseout', '#new_stars > span', function () {
    $('#new_stars > span').removeClass('active');
    var clicked = $('#new_stars > span.clicked');
    if ($(clicked).length > 0)
    {
        var ocena = parseInt($(clicked).attr('ocena'));
        for (var i = 1; i <= ocena; i++)
        {
            $('#new_stars > span[ocena=' + i + ']').addClass('active');
        }
    }
});
$(document).on('click', '#new_stars > span', function () {
    $('#new_stars > span').removeClass('clicked');
    $(this).addClass('clicked');
    $('input#ocena').val($(this).attr('ocena'));
});
$(document).on('click', '.opinion_paginate > a', function () {
    var old_opinion_page = opinion_page;
    if ($(this).hasClass('new'))
    {
        if (opinion_page > 1)
            opinion_page--;
        else
            opinion_page = opiniaPageCount;
    } else
        opinion_page++;
    if (opinion_page > opiniaPageCount)
        opinion_page = 1;
    $('#home_opinion .reload').addClass('open');
    $('#home_opinion').animate({
        opacity: 0.25
    }, 50);
    $.ajax({
        type: 'get',
        url: '/index/get_opinion/' + opinion_page,
        success: function (msg)
        {
            $('#home_opinion').html(msg + '<div class="reload"><span aria-hidden="true" class="glyphicon glyphicon-refresh animate"> </span></div>');
            $('#home_opinion').animate({
                opacity: 1
            }, 500);
//$('#home_opinion').css('opacity',1);
        },
        statusCode: {
            404: function () {
                opinion_page = old_opinion_page;
                $('#home_opinion .reload').removeClass('open');
                $('#home_opinion').animate({
                    opacity: 1
                }, 500);
            }}
    });
});