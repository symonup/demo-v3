var carousel = document.querySelector('.cl-carousel');
var cells = carousel.querySelectorAll('.carousel__cell');
var cellCount; // cellCount set from cells-range input value
var selectedIndex = 0;
var selectedIndexReal = 0;
var cellWidth = carousel.offsetWidth;
var cellHeight = carousel.offsetHeight;
var isHorizontal = true;
var rotateFn = isHorizontal ? 'rotateY' : 'rotateX';
var radius, theta;
// console.log( cellWidth, cellHeight );

function rotateCarousel() {
  var angle = theta * selectedIndex * -1;
  carousel.style.transform = 'translateZ(' + -radius + 'px) ' + 
    rotateFn + '(' + angle + 'deg)';
    changeCarousel(true);
}

var prevButton = document.querySelector('.previous-button');
prevButton.addEventListener( 'click', function() {
  selectedIndex--;
  selectedIndexReal--;
  cellCount = cells.length;
  if(selectedIndexReal<0){
      selectedIndexReal=(cellCount-1);
  }
  rotateCarousel();
});

var nextButton = document.querySelector('.next-button');
nextButton.addEventListener( 'click', function() {
  selectedIndex++;
  selectedIndexReal++;
  cellCount = cells.length;
  if(selectedIndexReal>(cellCount-1)){
      selectedIndexReal=0;
  }
  rotateCarousel();
});


function rotation3d(){
    //+' rotate3d(0, 1, 0, 10deg)'
    $('.cl-carousel .carousel__cell')[selectedIndex].style.transform;
}

function changeCarousel(stopRotate) {
    if(stopRotate===undefined){
        stopRotate=false;
    }
  cellCount = cells.length;
  theta = 360 / cellCount;
  var cellSize = isHorizontal ? cellWidth : cellHeight;
  radius = Math.round( ( cellSize / 2) / Math.tan( Math.PI / cellCount ) );
  var prevIndex1=(selectedIndexReal-1);
  var prevIndex2=(selectedIndexReal-2);
  var nextIndex1=(selectedIndexReal+1);
  var nextIndex2=(selectedIndexReal+2);
  if(prevIndex1 < 0){
      prevIndex1 = (cellCount-1);
      prevIndex2 = (cellCount-2);
  }
  else if(prevIndex2<0){
      prevIndex2 = (cellCount-1);
  }
  if(nextIndex1 > (cellCount-1)){
      nextIndex1=0;
      nextIndex2=1;
  }else if(nextIndex2 > (cellCount-1)){
      nextIndex2=0;
  }
  for ( var i=0; i < cells.length; i++ ) {
    var cell = cells[i];
    if ( i < cellCount ) {
      // visible cell
      cell.style.opacity = 1;
      var cellAngle = theta * i;
      var cssRotation =rotateFn + '(' + cellAngle + 'deg) translateZ(' + radius + 'px)';
      if(i==prevIndex1 || i==prevIndex2){
          cssRotation = cssRotation + ' rotate3d(0, 1, 0, 10deg)';
      }
      if(i==nextIndex1 || i==nextIndex2){
          cssRotation = cssRotation + ' rotate3d(0, 1, 0, -10deg)';
      }
      cell.style.transform = cssRotation;
    } else {
      // hidden cell
      cell.style.opacity = 0;
      cell.style.transform = 'none';
    }
  }
if(!stopRotate){
  rotateCarousel();
  }
}

var orientationRadios = document.querySelectorAll('input[name="orientation"]');
( function() {
  for ( var i=0; i < orientationRadios.length; i++ ) {
    var radio = orientationRadios[i];
    radio.addEventListener( 'change', onOrientationChange );
  }
})();

function onOrientationChange() {
  var checkedRadio = document.querySelector('input[name="orientation"]:checked');
  isHorizontal = true;//checkedRadio.value == 'horizontal';
  rotateFn = isHorizontal ? 'rotateY' : 'rotateX';
  changeCarousel();
}

// set initials
onOrientationChange();
