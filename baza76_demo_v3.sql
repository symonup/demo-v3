-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: 22276.m.tld.pl
-- Czas generowania: 23 Kwi 2020, 10:56
-- Wersja serwera: 5.7.28-31-log
-- Wersja PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `baza76_demo_v3`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `administrator`
--

CREATE TABLE `administrator` (
  `id` int(11) NOT NULL,
  `login` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `role_id` int(11) NOT NULL,
  `imie` varchar(100) DEFAULT NULL,
  `nazwisko` varchar(100) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `administrator`
--

INSERT INTO `administrator` (`id`, `login`, `password`, `role_id`, `imie`, `nazwisko`, `email`) VALUES
(1, 'admin', '$2y$10$mDWu5QzWHpJnu08q3EO1t.36c7WfChJzGzK4HQxdvroLtL2iecnLi', 1, 'Jan', 'Kowalski', 'test@projekt-samatix.pl');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `allegro_aukcja`
--

CREATE TABLE `allegro_aukcja` (
  `id` int(11) NOT NULL,
  `allegro_konto_id` int(11) DEFAULT NULL,
  `towar_id` int(11) DEFAULT NULL,
  `szablon` int(11) DEFAULT NULL,
  `item_id` varchar(100) DEFAULT NULL,
  `item_info` text,
  `item_is_allegro_standard` int(1) DEFAULT NULL,
  `sprzedane` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `category_id` varchar(100) DEFAULT NULL,
  `validation` text,
  `iloscToUpdate` int(11) DEFAULT '0',
  `priceToUpdate` float DEFAULT '0',
  `iloscQueryId` varchar(128) DEFAULT NULL,
  `iloscQueryStatus` varchar(200) DEFAULT NULL,
  `cenaQueryId` varchar(128) DEFAULT NULL,
  `cenaQueryStatus` varchar(200) DEFAULT NULL,
  `statusQueryId` varchar(128) DEFAULT NULL,
  `statusQueryStatus` varchar(200) DEFAULT NULL,
  `renew` tinyint(4) DEFAULT '0',
  `iloscQueryStatusLog` text,
  `removed` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `allegro_aukcja_przesylka`
--

CREATE TABLE `allegro_aukcja_przesylka` (
  `id` int(11) NOT NULL,
  `allegro_aukcja_id` int(11) NOT NULL,
  `fid` int(11) NOT NULL,
  `pierwsza_sztuka` float(6,2) NOT NULL,
  `kolejna_sztuka` float(6,2) DEFAULT NULL,
  `ilosc_w_paczce` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `allegro_aukcja_wariant`
--

CREATE TABLE `allegro_aukcja_wariant` (
  `id` int(11) NOT NULL,
  `allegro_aukcja_id` int(11) NOT NULL,
  `fid` int(11) NOT NULL,
  `mask` bigint(20) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `allegro_konto`
--

CREATE TABLE `allegro_konto` (
  `id` int(11) NOT NULL,
  `userId` varchar(30) DEFAULT NULL,
  `restClientId` varchar(128) NOT NULL,
  `restClientSecret` varchar(128) DEFAULT NULL,
  `restApiKey` varchar(128) DEFAULT NULL,
  `restRedirectUri` varchar(128) NOT NULL,
  `soapApiKey` varchar(128) NOT NULL,
  `token` text,
  `defaultCategory` int(11) DEFAULT NULL,
  `defaultCategoryPath` varchar(255) DEFAULT NULL,
  `defaultCategoryPathId` varchar(255) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `isDefault` tinyint(1) DEFAULT '0',
  `token_expire` datetime DEFAULT NULL,
  `devClientId` varchar(128) DEFAULT NULL,
  `devClientSecret` varchar(128) DEFAULT NULL,
  `device_code` varchar(128) DEFAULT NULL,
  `user_code` varchar(128) DEFAULT NULL,
  `api_interval` int(11) DEFAULT NULL,
  `kraj` varchar(10) DEFAULT NULL,
  `wojewodztwo` varchar(50) DEFAULT NULL,
  `miasto` varchar(100) DEFAULT NULL,
  `kod_pocztowy` varchar(10) DEFAULT NULL,
  `automaticRenew` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `allegro_pola_kategoria`
--

CREATE TABLE `allegro_pola_kategoria` (
  `id` int(11) NOT NULL,
  `allegro_aukcja_id` int(11) NOT NULL,
  `fid` int(11) NOT NULL,
  `value` text NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `allegro_szablon`
--

CREATE TABLE `allegro_szablon` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(100) NOT NULL,
  `html` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `allegro_zamowienie`
--

CREATE TABLE `allegro_zamowienie` (
  `id` int(11) NOT NULL,
  `allegro_konto_id` int(11) DEFAULT NULL,
  `order_id` varchar(100) DEFAULT NULL,
  `kupujacy_id` varchar(100) DEFAULT NULL,
  `kupujacy_email` varchar(100) DEFAULT NULL,
  `kupujacy_login` varchar(100) DEFAULT NULL,
  `kupujacy_guest` tinyint(1) DEFAULT '0',
  `kupujacy_telefon` varchar(20) DEFAULT NULL,
  `payment_id` varchar(100) DEFAULT NULL,
  `payment_type` varchar(50) DEFAULT NULL,
  `payment_provider` varchar(50) DEFAULT NULL,
  `payment_finished_at` datetime DEFAULT NULL,
  `payment_wplata_kwota` float DEFAULT NULL,
  `payment_wplata_waluta` varchar(10) DEFAULT NULL,
  `allegro_status` varchar(50) DEFAULT NULL,
  `allegro_wysylka_id` varchar(100) DEFAULT NULL,
  `allegro_wysylka_nazwa` varchar(100) DEFAULT NULL,
  `allegro_wysylka_koszt` float DEFAULT NULL,
  `allegro_wysylka_waluta` varchar(10) DEFAULT NULL,
  `allegro_wysylka_smart` tinyint(1) DEFAULT '0',
  `faktura` tinyint(1) DEFAULT '0',
  `wartosc_razem` float DEFAULT NULL,
  `waluta` varchar(10) DEFAULT NULL,
  `wysylka_imie` varchar(100) DEFAULT NULL,
  `wysylka_nazwisko` varchar(100) DEFAULT NULL,
  `wysylka_ulica` varchar(100) DEFAULT NULL,
  `wysylka_miasto` varchar(100) DEFAULT NULL,
  `wysylka_kod` varchar(10) DEFAULT NULL,
  `wysylka_kraj` varchar(10) DEFAULT NULL,
  `wysylka_telefon` varchar(20) DEFAULT NULL,
  `paczkomat_id` varchar(50) DEFAULT NULL,
  `paczkomat_nazwa` varchar(100) DEFAULT NULL,
  `paczkomat_opis` varchar(100) DEFAULT NULL,
  `paczkomat_adres` varchar(100) DEFAULT NULL,
  `faktura_ulica` varchar(100) DEFAULT NULL,
  `faktura_miasto` varchar(100) DEFAULT NULL,
  `faktura_kod` varchar(10) DEFAULT NULL,
  `faktura_kraj` varchar(10) DEFAULT NULL,
  `faktura_firma` varchar(150) DEFAULT NULL,
  `faktura_nip` varchar(20) DEFAULT NULL,
  `data_zakupu` datetime DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `uwagi` text,
  `notatki` text,
  `nr_listu_przewozowego` varchar(100) DEFAULT NULL,
  `kurier_typ` varchar(20) DEFAULT NULL,
  `get_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `allegro_zamowienie_towar`
--

CREATE TABLE `allegro_zamowienie_towar` (
  `id` int(11) NOT NULL,
  `towar_id` int(11) DEFAULT NULL,
  `allegro_zamowienie_id` int(11) NOT NULL,
  `item_id` varchar(100) DEFAULT NULL,
  `nazwa` varchar(150) DEFAULT NULL,
  `cena_podstawowa_za_sztuke` float DEFAULT NULL,
  `waluta` varchar(10) DEFAULT NULL,
  `cena_za_sztuke` float DEFAULT NULL,
  `ilosc` int(11) DEFAULT NULL,
  `data_zakupu` datetime DEFAULT NULL,
  `buy_id` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `atrybut`
--

CREATE TABLE `atrybut` (
  `id` int(11) NOT NULL,
  `atrybut_typ_id` int(11) NOT NULL,
  `kategoria_id` int(11) DEFAULT NULL,
  `nazwa` varchar(100) NOT NULL,
  `pole` int(11) NOT NULL DEFAULT '0',
  `kolejnosc` int(11) DEFAULT '0',
  `pod_tytul` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `atrybut`
--

INSERT INTO `atrybut` (`id`, `atrybut_typ_id`, `kategoria_id`, `nazwa`, `pole`, `kolejnosc`, `pod_tytul`) VALUES
(1, 1, NULL, 'Szerokość', 1, 0, ''),
(2, 1, NULL, 'Wysokość', 1, 0, ''),
(3, 1, NULL, 'Długość', 0, 0, ''),
(4, 1, NULL, 'Grubość', 0, 0, ''),
(5, 2, NULL, '60 cali', 0, 0, ''),
(6, 2, NULL, '72 cale', 0, 0, ''),
(7, 2, NULL, '120 cali', 0, 0, ''),
(8, 3, NULL, '640 x 480', 0, 0, ''),
(9, 3, NULL, '1024 x 786', 0, 0, ''),
(10, 3, NULL, '2K', 0, 0, ''),
(11, 3, NULL, '4K', 0, 0, '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `atrybut_i18n`
--

CREATE TABLE `atrybut_i18n` (
  `id` int(11) NOT NULL,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `atrybut_i18n`
--

INSERT INTO `atrybut_i18n` (`id`, `locale`, `model`, `foreign_key`, `field`, `content`) VALUES
(1, 'pl_PL', 'Atrybut', 1, 'nazwa', 'Szerokość'),
(2, 'pl_PL', 'Atrybut', 1, 'pod_tytul', ''),
(3, 'pl_PL', 'Atrybut', 2, 'nazwa', 'Wysokość'),
(4, 'pl_PL', 'Atrybut', 2, 'pod_tytul', ''),
(5, 'pl_PL', 'Atrybut', 3, 'nazwa', 'Długość'),
(6, 'pl_PL', 'Atrybut', 3, 'pod_tytul', ''),
(7, 'pl_PL', 'Atrybut', 4, 'nazwa', 'Grubość'),
(8, 'pl_PL', 'Atrybut', 4, 'pod_tytul', ''),
(9, 'pl_PL', 'Atrybut', 5, 'nazwa', '60 cali'),
(10, 'pl_PL', 'Atrybut', 5, 'pod_tytul', ''),
(11, 'pl_PL', 'Atrybut', 6, 'nazwa', '72 cale'),
(12, 'pl_PL', 'Atrybut', 6, 'pod_tytul', ''),
(13, 'pl_PL', 'Atrybut', 7, 'nazwa', '120 cali'),
(14, 'pl_PL', 'Atrybut', 7, 'pod_tytul', ''),
(15, 'pl_PL', 'Atrybut', 8, 'nazwa', '640 x 480'),
(16, 'pl_PL', 'Atrybut', 8, 'pod_tytul', ''),
(17, 'pl_PL', 'Atrybut', 9, 'nazwa', '1024 x 786'),
(18, 'pl_PL', 'Atrybut', 9, 'pod_tytul', ''),
(19, 'pl_PL', 'Atrybut', 10, 'nazwa', '2K'),
(20, 'pl_PL', 'Atrybut', 10, 'pod_tytul', ''),
(21, 'pl_PL', 'Atrybut', 11, 'nazwa', '4K'),
(22, 'pl_PL', 'Atrybut', 11, 'pod_tytul', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `atrybut_podrzedne`
--

CREATE TABLE `atrybut_podrzedne` (
  `id` int(11) NOT NULL,
  `atrybut_id` int(11) NOT NULL,
  `nazwa` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `atrybut_typ`
--

CREATE TABLE `atrybut_typ` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(100) NOT NULL,
  `kolejnosc` int(11) DEFAULT '0',
  `atrybut_typ_parent_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `atrybut_typ`
--

INSERT INTO `atrybut_typ` (`id`, `nazwa`, `kolejnosc`, `atrybut_typ_parent_id`) VALUES
(1, 'Rozmiar', 3, NULL),
(2, 'Przekątna', 2, NULL),
(3, 'Rozdzielczość', 1, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `atrybut_typ_i18n`
--

CREATE TABLE `atrybut_typ_i18n` (
  `id` int(11) NOT NULL,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `atrybut_typ_i18n`
--

INSERT INTO `atrybut_typ_i18n` (`id`, `locale`, `model`, `foreign_key`, `field`, `content`) VALUES
(1, 'pl_PL', 'AtrybutTyp', 1, 'nazwa', 'Rozmiar'),
(2, 'pl_PL', 'AtrybutTyp', 2, 'nazwa', 'Przekątna'),
(3, 'pl_PL', 'AtrybutTyp', 3, 'nazwa', 'Rozdzielczość');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `atrybut_typ_parent`
--

CREATE TABLE `atrybut_typ_parent` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(50) NOT NULL,
  `pod_tytul` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `atrybut_typ_parent_i18n`
--

CREATE TABLE `atrybut_typ_parent_i18n` (
  `id` int(11) NOT NULL,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(150) NOT NULL,
  `type` varchar(20) DEFAULT NULL,
  `css` text,
  `glowny` tinyint(1) DEFAULT '0',
  `ukryty` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `banner`
--

INSERT INTO `banner` (`id`, `nazwa`, `type`, `css`, `glowny`, `ukryty`) VALUES
(8, 'Slider główny', 'carusel', '', 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `banner_slides`
--

CREATE TABLE `banner_slides` (
  `id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `type` varchar(20) DEFAULT 'file',
  `file` varchar(128) DEFAULT NULL,
  `is_linked` tinyint(1) DEFAULT '0',
  `link` varchar(255) DEFAULT NULL,
  `title` varchar(150) DEFAULT NULL,
  `target` varchar(6) DEFAULT '_self',
  `height` int(11) DEFAULT NULL,
  `css` text,
  `width` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `banner_slides`
--

INSERT INTO `banner_slides` (`id`, `banner_id`, `type`, `file`, `is_linked`, `link`, `title`, `target`, `height`, `css`, `width`) VALUES
(29, 8, 'file', '5de77ca1385e94.40637033.png', 1, '#link-slide', 'Nazwa slajdu 1', '_self', 400, '', 1086),
(30, 8, 'file', '5de77cb1d78993.09969813.png', 0, '', 'Slajd 2', '_self', 400, '', 1086),
(31, 8, 'file', '5de77ca9a69842.33464838.png', 0, '', 'Jeszcze jeden slide', '_self', 400, '', 1086);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `banner_slides_elements`
--

CREATE TABLE `banner_slides_elements` (
  `id` int(11) NOT NULL,
  `banner_slides_id` int(11) NOT NULL,
  `type` varchar(20) NOT NULL DEFAULT 'html',
  `file` varchar(128) DEFAULT NULL,
  `text` text,
  `title` varchar(160) DEFAULT NULL,
  `tooltip` tinyint(1) DEFAULT '0',
  `pos_left` varchar(15) DEFAULT 'auto',
  `pos_top` varchar(15) DEFAULT 'auto',
  `position` varchar(15) DEFAULT 'absolute',
  `css` text,
  `link` varchar(255) DEFAULT NULL,
  `target` varchar(6) DEFAULT '_self',
  `is_linked` tinyint(4) DEFAULT '0',
  `_width` int(11) DEFAULT NULL,
  `_height` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `tytul` varchar(255) NOT NULL,
  `tresc` text NOT NULL,
  `opis` text NOT NULL,
  `zdjecie` varchar(128) DEFAULT NULL,
  `data` datetime NOT NULL,
  `ukryty` tinyint(1) DEFAULT '0',
  `blog_komentarz_count` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `blog`
--

INSERT INTO `blog` (`id`, `tytul`, `tresc`, `opis`, `zdjecie`, `data`, `ukryty`, `blog_komentarz_count`) VALUES
(1, 'Tytuł tego artykułu, o dowolnej treści dodanej przez admina i autora', '<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eget neque vitae turpis egestas finibus eget cursus dui. Aliquam vitae laoreet diam, id fringilla dolor. Pellentesque rhoncus aliquet nisi eget bibendum. Aliquam lacinia pretium nulla non viverra. Nunc facilisis facilisis sem, sit amet suscipit mi ultrices pretium. Aenean porta dignissim est, non tincidunt massa suscipit et. Etiam volutpat quam eu enim dictum, et vehicula mi porta. Quisque hendrerit sapien elit, et ultricies neque tempor ac. Vestibulum pellentesque ac ex id dapibus. Mauris nisl massa, dictum eu congue non, aliquet at odio. Curabitur sed risus in sapien sollicitudin vulputate sit amet luctus nisl. Duis aliquet diam nec justo dapibus bibendum id tincidunt sem. Phasellus elementum pellentesque dolor, non interdum sapien blandit sed. Pellentesque pretium a neque ac vehicula. Maecenas eget mi leo. Proin et viverra ipsum.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Maecenas consectetur urna a augue auctor fermentum. Cras volutpat, nunc ac rutrum tristique, lacus ligula vestibulum nisl, ac venenatis leo est quis velit. Sed imperdiet elementum ultricies. Pellentesque mattis velit ut metus vestibulum rutrum. Donec vel egestas nisl. Proin in pulvinar lectus. Quisque ac vestibulum nisi, vitae vulputate libero. Ut aliquet convallis mi vel bibendum. Nulla facilisi. Fusce facilisis, tortor nec fermentum interdum, ipsum lorem rhoncus nibh, id tincidunt nisl leo ullamcorper odio. Curabitur a mattis orci. Donec vestibulum, odio a posuere eleifend, nisi ex porttitor orci, a vehicula mi diam luctus tortor. Nam urna dolor, euismod at leo et, lobortis rhoncus urna. Aenean hendrerit aliquam nisl et sagittis. Nullam sed odio tortor. Mauris efficitur nisl in bibendum malesuada.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Sed ex sem, ultricies fringilla lectus volutpat, blandit cursus leo. Proin convallis pellentesque dictum. Suspendisse ut tortor ipsum. Donec sed efficitur urna, sit amet sodales lectus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis id nibh vitae erat consequat sagittis. Praesent nisl orci, tempor non nibh eu, varius fermentum risus. Nam pharetra sollicitudin neque. Mauris rhoncus finibus magna sed placerat.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eget neque vitae turpis egestas finibus eget cursus dui. Aliquam vitae laoreet diam, id fringilla dolor. Pellentesque rhoncus aliquet nisi eget bibendum. Aliquam lacinia pretium nulla non viverra. Nunc facilisis facilisis sem, sit amet suscipit mi ultrices pretium. Aenean porta dignissim est, non tincidunt massa suscipit et. Etiam volutpat quam eu enim dictum, et vehicula mi porta. Quisque hendrerit sapien elit, et ultricies neque tempor ac. Vestibulum pellentesque ac ex id dapibus. Mauris nisl massa, dictum eu congue non, aliquet at odio.', '5de8ede8040b5.png', '2019-12-05 12:04:55', 0, 320),
(2, 'Tytuł tego artykułu, o dowolnej treści dodanej przez admina i autora', '<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eget neque vitae turpis egestas finibus eget cursus dui. Aliquam vitae laoreet diam, id fringilla dolor. Pellentesque rhoncus aliquet nisi eget bibendum. Aliquam lacinia pretium nulla non viverra. Nunc facilisis facilisis sem, sit amet suscipit mi ultrices pretium. Aenean porta dignissim est, non tincidunt massa suscipit et. Etiam volutpat quam eu enim dictum, et vehicula mi porta. Quisque hendrerit sapien elit, et ultricies neque tempor ac. Vestibulum pellentesque ac ex id dapibus. Mauris nisl massa, dictum eu congue non, aliquet at odio. Curabitur sed risus in sapien sollicitudin vulputate sit amet luctus nisl. Duis aliquet diam nec justo dapibus bibendum id tincidunt sem. Phasellus elementum pellentesque dolor, non interdum sapien blandit sed. Pellentesque pretium a neque ac vehicula. Maecenas eget mi leo. Proin et viverra ipsum.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Maecenas consectetur urna a augue auctor fermentum. Cras volutpat, nunc ac rutrum tristique, lacus ligula vestibulum nisl, ac venenatis leo est quis velit. Sed imperdiet elementum ultricies. Pellentesque mattis velit ut metus vestibulum rutrum. Donec vel egestas nisl. Proin in pulvinar lectus. Quisque ac vestibulum nisi, vitae vulputate libero. Ut aliquet convallis mi vel bibendum. Nulla facilisi. Fusce facilisis, tortor nec fermentum interdum, ipsum lorem rhoncus nibh, id tincidunt nisl leo ullamcorper odio. Curabitur a mattis orci. Donec vestibulum, odio a posuere eleifend, nisi ex porttitor orci, a vehicula mi diam luctus tortor. Nam urna dolor, euismod at leo et, lobortis rhoncus urna. Aenean hendrerit aliquam nisl et sagittis. Nullam sed odio tortor. Mauris efficitur nisl in bibendum malesuada.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Sed ex sem, ultricies fringilla lectus volutpat, blandit cursus leo. Proin convallis pellentesque dictum. Suspendisse ut tortor ipsum. Donec sed efficitur urna, sit amet sodales lectus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis id nibh vitae erat consequat sagittis. Praesent nisl orci, tempor non nibh eu, varius fermentum risus. Nam pharetra sollicitudin neque. Mauris rhoncus finibus magna sed placerat.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eget neque vitae turpis egestas finibus eget cursus dui. Aliquam vitae laoreet diam, id fringilla dolor. Pellentesque rhoncus aliquet nisi eget bibendum. Aliquam lacinia pretium nulla non viverra. Nunc facilisis facilisis sem, sit amet suscipit mi ultrices pretium. Aenean porta dignissim est, non tincidunt massa suscipit et. Etiam volutpat quam eu enim dictum, et vehicula mi porta. Quisque hendrerit sapien elit, et ultricies neque tempor ac. Vestibulum pellentesque ac ex id dapibus. Mauris nisl massa, dictum eu congue non, aliquet at odio. Curabitur sed risus in sapien sollicitudin vulputate sit amet luctus nisl. Duis aliquet diam nec justo dapibus bibendum id tincidunt sem. Phasellus elementum pellentesque dolor, non interdum sapien blandit sed. Pellentesque pretium a neque ac vehicula. Maecenas eget mi leo. Proin et viverra ipsum.', '5de8ee514bd3b.png', '2019-12-05 12:06:51', 0, 60),
(3, 'Tytuł tego artykułu, o dowolnej treści dodanej przez admina i autora', '<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eget neque vitae turpis egestas finibus eget cursus dui. Aliquam vitae laoreet diam, id fringilla dolor. Pellentesque rhoncus aliquet nisi eget bibendum. Aliquam lacinia pretium nulla non viverra. Nunc facilisis facilisis sem, sit amet suscipit mi ultrices pretium. Aenean porta dignissim est, non tincidunt massa suscipit et. Etiam volutpat quam eu enim dictum, et vehicula mi porta. Quisque hendrerit sapien elit, et ultricies neque tempor ac. Vestibulum pellentesque ac ex id dapibus. Mauris nisl massa, dictum eu congue non, aliquet at odio. Curabitur sed risus in sapien sollicitudin vulputate sit amet luctus nisl. Duis aliquet diam nec justo dapibus bibendum id tincidunt sem. Phasellus elementum pellentesque dolor, non interdum sapien blandit sed. Pellentesque pretium a neque ac vehicula. Maecenas eget mi leo. Proin et viverra ipsum.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Maecenas consectetur urna a augue auctor fermentum. Cras volutpat, nunc ac rutrum tristique, lacus ligula vestibulum nisl, ac venenatis leo est quis velit. Sed imperdiet elementum ultricies. Pellentesque mattis velit ut metus vestibulum rutrum. Donec vel egestas nisl. Proin in pulvinar lectus. Quisque ac vestibulum nisi, vitae vulputate libero. Ut aliquet convallis mi vel bibendum. Nulla facilisi. Fusce facilisis, tortor nec fermentum interdum, ipsum lorem rhoncus nibh, id tincidunt nisl leo ullamcorper odio. Curabitur a mattis orci. Donec vestibulum, odio a posuere eleifend, nisi ex porttitor orci, a vehicula mi diam luctus tortor. Nam urna dolor, euismod at leo et, lobortis rhoncus urna. Aenean hendrerit aliquam nisl et sagittis. Nullam sed odio tortor. Mauris efficitur nisl in bibendum malesuada.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Sed ex sem, ultricies fringilla lectus volutpat, blandit cursus leo. Proin convallis pellentesque dictum. Suspendisse ut tortor ipsum. Donec sed efficitur urna, sit amet sodales lectus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis id nibh vitae erat consequat sagittis. Praesent nisl orci, tempor non nibh eu, varius fermentum risus. Nam pharetra sollicitudin neque. Mauris rhoncus finibus magna sed placerat.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eget neque vitae turpis egestas finibus eget cursus dui. Aliquam vitae laoreet diam, id fringilla dolor. Pellentesque rhoncus aliquet nisi eget bibendum. Aliquam lacinia pretium nulla non viverra. Nunc facilisis facilisis sem, sit amet suscipit mi ultrices pretium. Aenean porta dignissim est, non tincidunt massa suscipit et. Etiam volutpat quam eu enim dictum, et vehicula mi porta. Quisque hendrerit sapien elit, et ultricies neque tempor ac. Vestibulum pellentesque ac ex id dapibus. Mauris nisl massa, dictum eu congue non, aliquet at odio. Curabitur sed risus in sapien sollicitudin vulputate sit amet luctus nisl. Duis aliquet diam nec justo dapibus bibendum id tincidunt sem. Phasellus elementum pellentesque dolor, non interdum sapien blandit sed. Pellentesque pretium a neque ac vehicula. Maecenas eget mi leo. Proin et viverra ipsum.', '5de8ef136ffc4.png', '2019-12-05 12:07:17', 0, 1985),
(4, 'Tytuł tego artykułu, o dowolnej treści dodanej przez admina i autora', '<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eget neque vitae turpis egestas finibus eget cursus dui. Aliquam vitae laoreet diam, id fringilla dolor. Pellentesque rhoncus aliquet nisi eget bibendum. Aliquam lacinia pretium nulla non viverra. Nunc facilisis facilisis sem, sit amet suscipit mi ultrices pretium. Aenean porta dignissim est, non tincidunt massa suscipit et. Etiam volutpat quam eu enim dictum, et vehicula mi porta. Quisque hendrerit sapien elit, et ultricies neque tempor ac. Vestibulum pellentesque ac ex id dapibus. Mauris nisl massa, dictum eu congue non, aliquet at odio. Curabitur sed risus in sapien sollicitudin vulputate sit amet luctus nisl. Duis aliquet diam nec justo dapibus bibendum id tincidunt sem. Phasellus elementum pellentesque dolor, non interdum sapien blandit sed. Pellentesque pretium a neque ac vehicula. Maecenas eget mi leo. Proin et viverra ipsum.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Maecenas consectetur urna a augue auctor fermentum. Cras volutpat, nunc ac rutrum tristique, lacus ligula vestibulum nisl, ac venenatis leo est quis velit. Sed imperdiet elementum ultricies. Pellentesque mattis velit ut metus vestibulum rutrum. Donec vel egestas nisl. Proin in pulvinar lectus. Quisque ac vestibulum nisi, vitae vulputate libero. Ut aliquet convallis mi vel bibendum. Nulla facilisi. Fusce facilisis, tortor nec fermentum interdum, ipsum lorem rhoncus nibh, id tincidunt nisl leo ullamcorper odio. Curabitur a mattis orci. Donec vestibulum, odio a posuere eleifend, nisi ex porttitor orci, a vehicula mi diam luctus tortor. Nam urna dolor, euismod at leo et, lobortis rhoncus urna. Aenean hendrerit aliquam nisl et sagittis. Nullam sed odio tortor. Mauris efficitur nisl in bibendum malesuada.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Sed ex sem, ultricies fringilla lectus volutpat, blandit cursus leo. Proin convallis pellentesque dictum. Suspendisse ut tortor ipsum. Donec sed efficitur urna, sit amet sodales lectus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis id nibh vitae erat consequat sagittis. Praesent nisl orci, tempor non nibh eu, varius fermentum risus. Nam pharetra sollicitudin neque. Mauris rhoncus finibus magna sed placerat.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eget neque vitae turpis egestas finibus eget cursus dui. Aliquam vitae laoreet diam, id fringilla dolor. Pellentesque rhoncus aliquet nisi eget bibendum. Aliquam lacinia pretium nulla non viverra. Nunc facilisis facilisis sem, sit amet suscipit mi ultrices pretium. Aenean porta dignissim est, non tincidunt massa suscipit et. Etiam volutpat quam eu enim dictum, et vehicula mi porta. Quisque hendrerit sapien elit, et ultricies neque tempor ac. Vestibulum pellentesque ac ex id dapibus. Mauris nisl massa, dictum eu congue non, aliquet at odio. Curabitur sed risus in sapien sollicitudin vulputate sit amet luctus nisl. Duis aliquet diam nec justo dapibus bibendum id tincidunt sem. Phasellus elementum pellentesque dolor, non interdum sapien blandit sed. Pellentesque pretium a neque ac vehicula. Maecenas eget mi leo. Proin et viverra ipsum.', '5de8ee8d8d5db.png', '2019-12-05 12:07:49', 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `blok`
--

CREATE TABLE `blok` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(50) NOT NULL,
  `ukryty` tinyint(1) NOT NULL DEFAULT '0',
  `tresc` longtext NOT NULL,
  `locale` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `blok`
--

INSERT INTO `blok` (`id`, `nazwa`, `ukryty`, `tresc`, `locale`) VALUES
(4, 'Kontakt', 0, '<div class=\"row\">\r\n    <div class=\"col-12 fs-24 fw-bold\">Kontakt</div>\r\n</div>\r\n<div class=\"row mt-30\">\r\n    <div class=\"col-12 fs-18 fw-bold\">Atkin</div>\r\n</div>\r\n<div class=\"row justify-content-center align-items-center\">\r\n    <div class=\"md-flex text-left after-line md-after-line\">\r\n        <p class=\"fs-16 text-left d-inline-block mb-0\">\r\n            ul. Ulica 93<br>\r\n            <span class=\"no-warp\">00-000 Miasto</span></p>\r\n    </div>\r\n    <div class=\"md-flex text-center md-after-line\">\r\n        <p class=\"fs-16 text-left d-inline-block mb-0\">\r\n            Nip: 111 222 33 44<br>\r\n            Regon: 444 555 666</p>\r\n    </div>\r\n    <div class=\"md-flex after-line md-after-line text-center\">\r\n        <p class=\"fs-16 mb-0\">\r\n            <a href=\"tel:123 456 789\">123 456 789</a>\r\n        </p>\r\n    </div>\r\n    <div class=\"md-flex text-center md-after-line\">\r\n        <p class=\"fs-16 mb-0\">\r\n            <a href=\"mailto: biuro@atkin.pl\"> biuro@atkin.pl</a>\r\n        </p>\r\n    </div>\r\n    <div class=\"md-flex text-right\">\r\n        <p class=\"fs-16 mb-0\">\r\n            <a href=\"#\" target=\"_blank\"><i class=\"fab fa-facebook-f f-color-facebook fs-24 m-r-5 v-align-m\"></i> Nasza strona na Facebooku <i class=\"v-align-tb fa fa-angle-right f-color-1\"></i></a>\r\n        </p>\r\n    </div>\r\n</div>\r\n<div class=\"row\">\r\n    <div class=\"col-12\">\r\n        <div class=\"page-small-head\"><span>Napisz do nas</span></div>\r\n    </div>\r\n</div>\r\n<div class=\"row\">\r\n    <div class=\"col-12\">\r\n        [%-_kontakt-form_-%]\r\n    </div>\r\n</div>', 'pl_PL');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `blok_i18n`
--

CREATE TABLE `blok_i18n` (
  `id` int(11) NOT NULL,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bonus`
--

CREATE TABLE `bonus` (
  `id` int(11) NOT NULL,
  `wartosc` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `bonus`
--

INSERT INTO `bonus` (`id`, `wartosc`) VALUES
(1, 1),
(2, 2),
(3, 5),
(4, 7),
(5, 11);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `call_back`
--

CREATE TABLE `call_back` (
  `id` int(11) NOT NULL,
  `telefon` varchar(150) NOT NULL,
  `data` datetime NOT NULL,
  `status` tinyint(2) DEFAULT '0',
  `notatki` text,
  `data_status` datetime DEFAULT NULL,
  `administrator_id` int(11) DEFAULT NULL,
  `administrator_dane` varchar(150) DEFAULT NULL,
  `zgoda` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `call_back`
--

INSERT INTO `call_back` (`id`, `telefon`, `data`, `status`, `notatki`, `data_status`, `administrator_id`, `administrator_dane`, `zgoda`) VALUES
(1, '600300900', '2020-01-28 10:45:41', 3, 'testowe notatki do callback', '2020-01-28 11:52:34', 1, 'admin', 0),
(2, '3256556551', '2020-01-28 11:23:01', 2, 'Klient zainteresowany kupnem czegoś tam, złoży zamówienie przez sklep\r\ntest', '2020-01-28 11:50:46', 1, 'admin', 1),
(3, '234234', '2020-01-28 11:23:28', 1, 'bz status\r\ntest\r\nedit', '2020-01-28 11:49:58', 1, 'admin', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `certyfikat`
--

CREATE TABLE `certyfikat` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(100) DEFAULT NULL,
  `skrot` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `certyfikat`
--

INSERT INTO `certyfikat` (`id`, `nazwa`, `skrot`) VALUES
(1, '', 'CE'),
(2, '', 'EN71'),
(3, '', 'Safe Toys'),
(4, '', 'TUV'),
(5, 'Certyfikat Instytutu Matki i Dziecka oraz Certyfikat Państwowego Zakładu Higieny', 'CIMiD - CPZH'),
(6, '', 'WE'),
(7, '', 'Fair Trade'),
(8, '', 'GOTS'),
(9, '', 'PEFC'),
(10, '', 'FSC');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cron`
--

CREATE TABLE `cron` (
  `id` int(11) NOT NULL,
  `typ` varchar(100) NOT NULL,
  `skip` int(11) DEFAULT '0',
  `take` int(11) DEFAULT '1',
  `modifiedTime` date DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0',
  `blokada` tinyint(4) DEFAULT '0',
  `token` varchar(128) DEFAULT NULL,
  `last_run` datetime DEFAULT NULL,
  `page` int(11) DEFAULT '0',
  `max_time` int(11) DEFAULT NULL COMMENT 'Maksymalny czas wykonywania w sekundach',
  `nextModifiedTime` date DEFAULT NULL,
  `log` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `cron`
--

INSERT INTO `cron` (`id`, `typ`, `skip`, `take`, `modifiedTime`, `status`, `blokada`, `token`, `last_run`, `page`, `max_time`, `nextModifiedTime`, `log`) VALUES
(1, 'priceMessage', 0, 1, NULL, 0, 0, '5c4751f667b72', '2019-01-22 18:25:10', 0, NULL, NULL, 0),
(2, 'newsletter', 0, 1, NULL, 0, 0, NULL, NULL, 0, NULL, NULL, 0),
(3, 'integracjaIkonka', 0, 1, NULL, 0, 0, '5d3af0d65db8b', '2019-07-26 14:23:50', 0, NULL, NULL, 0),
(4, 'downloadImages', 0, 30, NULL, 0, 0, '5d3ae98b37af8', '2019-07-26 13:52:43', 0, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `faktura`
--

CREATE TABLE `faktura` (
  `id` int(11) NOT NULL,
  `zamowienie_id` int(11) DEFAULT NULL,
  `typ` varchar(30) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `status_fiskalny` tinyint(4) DEFAULT NULL,
  `id_dokumentu` varchar(50) DEFAULT NULL,
  `numer_dostawcy` varchar(50) DEFAULT NULL,
  `rozszerzenie_numeru` varchar(30) DEFAULT NULL,
  `numer_dokumentu` varchar(50) DEFAULT NULL,
  `numer_do_korekty` varchar(50) DEFAULT NULL,
  `data_wystawienia_do_korekty` date DEFAULT NULL,
  `numer_zamowienia` varchar(50) DEFAULT NULL,
  `magazyn_docelowy_mm` varchar(50) DEFAULT NULL,
  `kod_kontrahenta` varchar(50) DEFAULT NULL,
  `nazwa_skrocona` varchar(100) DEFAULT NULL,
  `nazwa_pelna` varchar(250) DEFAULT NULL,
  `miasto` varchar(100) DEFAULT NULL,
  `kod_pocztowy` varchar(20) DEFAULT NULL,
  `adres` varchar(100) DEFAULT NULL,
  `nip` varchar(20) DEFAULT NULL,
  `kategoria_dokumentu` varchar(50) DEFAULT NULL,
  `podtytul_kategorii` varchar(50) DEFAULT NULL,
  `miejsce_wystawienia` varchar(50) DEFAULT NULL,
  `data_wystawienia` date DEFAULT NULL,
  `data_sprzedazy` date DEFAULT NULL,
  `data_otrzymania` date DEFAULT NULL,
  `ilosc_pozycji` int(11) DEFAULT NULL,
  `wg_cen_netto` tinyint(1) DEFAULT '0',
  `aktywna_cena` varchar(30) DEFAULT NULL,
  `wartosc_netto` float DEFAULT NULL,
  `wartosc_vat` float DEFAULT NULL,
  `wartosc_brutto` float DEFAULT NULL,
  `koszt` float DEFAULT NULL,
  `rabat_nazwa` varchar(50) DEFAULT NULL,
  `rabat_procent` float DEFAULT NULL,
  `forma_platnosci` varchar(100) DEFAULT NULL,
  `termin_platnosci` date DEFAULT NULL,
  `kwota_zaplacona_przy_odbiorze` float DEFAULT NULL,
  `wartosc_do_zaplaty` float DEFAULT NULL,
  `zaokraglenie_do_zaplaty` tinyint(4) DEFAULT '0',
  `zaokraglenie_vat` tinyint(4) DEFAULT '0',
  `automatyczne_przeliczanie` tinyint(1) DEFAULT '0',
  `statusy_specjalne` int(11) DEFAULT NULL,
  `wystawiajacy` varchar(100) DEFAULT NULL,
  `odbiorca` varchar(100) DEFAULT NULL,
  `podstawa_wydania` varchar(100) DEFAULT NULL,
  `wartosc_opakowan` float DEFAULT NULL,
  `wartosc_zwroconych_opakowan` float DEFAULT NULL,
  `waluta` varchar(10) DEFAULT NULL,
  `kurs_waluty` float DEFAULT NULL,
  `uwagi` varchar(500) DEFAULT NULL,
  `komentarz` varchar(500) DEFAULT NULL,
  `podtytul` varchar(100) DEFAULT NULL,
  `empty_data` varchar(100) DEFAULT NULL,
  `import_dokumentu` tinyint(1) DEFAULT '0',
  `eksportowy` tinyint(1) DEFAULT '0',
  `rodzaj_transakcji` tinyint(4) DEFAULT NULL,
  `platnosc_karta_nazwa` varchar(100) DEFAULT NULL,
  `platnosc_karta_kwota` float DEFAULT NULL,
  `platnosc_kredyt_nazwa` varchar(100) DEFAULT NULL,
  `platnosc_kredyt_kwota` float DEFAULT NULL,
  `panstwo` varchar(30) DEFAULT NULL,
  `prefix_ue` varchar(10) DEFAULT NULL,
  `czy_ue` tinyint(1) DEFAULT '0',
  `token` varchar(128) DEFAULT NULL,
  `sklep_nazwa` varchar(150) DEFAULT NULL,
  `sklep_miasto` varchar(100) DEFAULT NULL,
  `sklep_kod_pocztowy` varchar(10) DEFAULT NULL,
  `sklep_adres` varchar(100) DEFAULT NULL,
  `sklep_nip` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `faktura`
--

INSERT INTO `faktura` (`id`, `zamowienie_id`, `typ`, `status`, `status_fiskalny`, `id_dokumentu`, `numer_dostawcy`, `rozszerzenie_numeru`, `numer_dokumentu`, `numer_do_korekty`, `data_wystawienia_do_korekty`, `numer_zamowienia`, `magazyn_docelowy_mm`, `kod_kontrahenta`, `nazwa_skrocona`, `nazwa_pelna`, `miasto`, `kod_pocztowy`, `adres`, `nip`, `kategoria_dokumentu`, `podtytul_kategorii`, `miejsce_wystawienia`, `data_wystawienia`, `data_sprzedazy`, `data_otrzymania`, `ilosc_pozycji`, `wg_cen_netto`, `aktywna_cena`, `wartosc_netto`, `wartosc_vat`, `wartosc_brutto`, `koszt`, `rabat_nazwa`, `rabat_procent`, `forma_platnosci`, `termin_platnosci`, `kwota_zaplacona_przy_odbiorze`, `wartosc_do_zaplaty`, `zaokraglenie_do_zaplaty`, `zaokraglenie_vat`, `automatyczne_przeliczanie`, `statusy_specjalne`, `wystawiajacy`, `odbiorca`, `podstawa_wydania`, `wartosc_opakowan`, `wartosc_zwroconych_opakowan`, `waluta`, `kurs_waluty`, `uwagi`, `komentarz`, `podtytul`, `empty_data`, `import_dokumentu`, `eksportowy`, `rodzaj_transakcji`, `platnosc_karta_nazwa`, `platnosc_karta_kwota`, `platnosc_kredyt_nazwa`, `platnosc_kredyt_kwota`, `panstwo`, `prefix_ue`, `czy_ue`, `token`, `sklep_nazwa`, `sklep_miasto`, `sklep_kod_pocztowy`, `sklep_adres`, `sklep_nip`) VALUES
(1, 5, 'FS', 1, 0, '5', '', 'SF', '5/SF/MAG/2020', '', NULL, '', '', 'z5', 'JanLand S.C.', 'JanLand S.C.', 'Łuków', '25-800', 'Gromska 22', '6511568458', 'Sprzedaż', 'Sprzedaż dla klienta', 'Wrocław', '2020-04-01', '2020-04-01', NULL, 2, 0, 'Detaliczna', 98.89, 20.11, 119, 0, '', 0, '', '2020-04-01', 119, 119, 0, 0, 1, 0, ';Szef', '', '', 0, 0, 'PLN', 1, '', '', 'Zamówienie nr: FE6KSR', '', 0, 0, 0, '', 0, '', 0, 'Polska', 'PL', 0, 'e627fdf9863fc9661685313c8c02a95e8d9bc002', NULL, NULL, NULL, NULL, NULL),
(2, 2, 'FS', 1, 0, '7', '', 'SF', '7/SF/MAG/2020', '', NULL, '', '', 'u7', 'Jan Kowalski', 'Jan Kowalski', 'London', '78-111', 'test, 11', '', 'Sprzedaż', 'Sprzedaż dla klienta', 'Wrocław', '2020-02-07', '2020-02-07', NULL, 10, 0, 'Detaliczna', 55.56, 4.44, 60, 0, '', 0, '', '2020-02-07', 60, 60, 0, 0, 1, 0, ';Szef', '', '', 0, 0, 'PLN', 1, '', '', 'Zamówienie nr: P8ICXR', '', 0, 0, 0, '', 0, '', 0, 'Polska', 'PL', 0, '67f40618fbd40b188a06d1e0746013f1f60885dd', NULL, NULL, NULL, NULL, NULL),
(3, 3, 'FS', 1, 0, '6', '', 'SF', '6/SF/MAG/2020', '', NULL, '', '', 'u7', 'Jan Kowalski', 'Jan Kowalski', 'London', '78-111', 'test, 11', '', 'Sprzedaż', 'Sprzedaż dla klienta', 'Wrocław', '2020-02-07', '2020-02-07', NULL, 4, 0, 'Detaliczna', 55.56, 4.44, 60, 0, '', 0, '', '2020-02-07', 60, 60, 0, 0, 1, 0, ';Szef', '', '', 0, 0, 'PLN', 1, '', '', 'Zamówienie nr: CYH0NP', '', 0, 0, 0, '', 0, '', 0, 'Polska', 'PL', 0, '11692be2adc2ae1b46fe82cc9540dba68c0a19ed', NULL, NULL, NULL, NULL, NULL),
(4, 5, 'FS', 1, 0, '8', '', 'SF', '8/SF/MAG/2020', '', NULL, '', '', 'z5', 'JanLand S.C.', 'JanLand S.C.', 'Łuków', '25-800', 'Gromska 22', '6511568458', 'Sprzedaż', 'Sprzedaż dla klienta', 'Wrocław', '2020-04-01', '2020-04-01', NULL, 2, 0, 'Detaliczna', 98.89, 20.11, 119, 0, '', 0, '', '2020-04-01', 0, 119, 0, 0, 1, 0, ';Szef', '', '', 0, 0, 'PLN', 1, '', '', 'Zamówienie nr: FE6KSR', '', 0, 0, 0, '', 0, '', 0, 'Polska', 'PL', 0, '7704da909db4c851f706a04f53a7e23395ce9ce2', 'Firma przykładowa systemu InsERT GT', 'Wrocław', '54-445', 'Bławatkowa 25/3', '111-111-11-11'),
(5, 5, 'FS', 1, 0, '9', '', 'SF', '9/SF/MAG/2020', '', NULL, '', '', 'z5', 'JanLand S.C.', 'JanLand S.C.', 'Łuków', '25-800', 'Gromska 22', '6511568458', 'Sprzedaż', 'Sprzedaż dla klienta', 'Wrocław', '2020-04-01', '2020-04-01', NULL, 2, 0, 'Detaliczna', 98.89, 20.11, 119, 0, '', 0, '', '2020-04-01', 119, 119, 0, 0, 1, 0, ';Szef', '', '', 0, 0, 'PLN', 1, '', '', 'Zamówienie nr: FE6KSR', '', 0, 0, 0, 'Karta płatnicza', 119, '', 0, 'Polska', 'PL', 0, '2db8adfe84c51afdd05762ed1ca05569242fe7db', 'Firma przykładowa systemu InsERT GT', 'Wrocław', '54-445', 'Bławatkowa 25/3', '111-111-11-11');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `faktura_pozycja`
--

CREATE TABLE `faktura_pozycja` (
  `id` int(11) NOT NULL,
  `faktura_id` int(11) NOT NULL,
  `lp` int(11) DEFAULT NULL,
  `nazwa` varchar(255) DEFAULT NULL,
  `typ_towaru` tinyint(4) DEFAULT NULL,
  `symbol` varchar(100) DEFAULT NULL,
  `rabat_procentowy` tinyint(1) DEFAULT '0',
  `rabat_od_ceny` tinyint(1) DEFAULT '0',
  `rabat_pozycja` tinyint(1) DEFAULT '0',
  `rabat_blokada` tinyint(1) DEFAULT '0',
  `rabat_wartosc` float DEFAULT NULL,
  `rabat_procent` float DEFAULT NULL,
  `jednostka` varchar(20) DEFAULT NULL,
  `ilosc_jednostka` float DEFAULT NULL,
  `ilosc_magazyn` float DEFAULT NULL,
  `cena_magazyn` float DEFAULT NULL,
  `cena_netto` float DEFAULT NULL,
  `cena_brutto` float DEFAULT NULL,
  `stawka_vat` float DEFAULT NULL,
  `wartosc_netto` float DEFAULT NULL,
  `wartosc_vat` float DEFAULT NULL,
  `wartosc_brutto` float DEFAULT NULL,
  `koszt` float DEFAULT NULL,
  `opis_uslugi` varchar(500) DEFAULT NULL,
  `nazwa_uslugi` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `faktura_pozycja`
--

INSERT INTO `faktura_pozycja` (`id`, `faktura_id`, `lp`, `nazwa`, `typ_towaru`, `symbol`, `rabat_procentowy`, `rabat_od_ceny`, `rabat_pozycja`, `rabat_blokada`, `rabat_wartosc`, `rabat_procent`, `jednostka`, `ilosc_jednostka`, `ilosc_magazyn`, `cena_magazyn`, `cena_netto`, `cena_brutto`, `stawka_vat`, `wartosc_netto`, `wartosc_vat`, `wartosc_brutto`, `koszt`, `opis_uslugi`, `nazwa_uslugi`) VALUES
(1, 1, 1, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'AST2665', 1, 0, 0, 1, 0, 0, 'szt.', 1, 1, 0, 81.3, 100, 23, 81.3, 18.7, 100, 0, '', ''),
(2, 1, 2, 'Wysyłka 48h', 2, 'KURIER_48', 1, 0, 0, 1, 0, 0, 'szt.', 1, 1, 0, 17.59, 19, 8, 17.59, 1.41, 19, 0, '', ''),
(3, 2, 1, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'T_8', 1, 0, 0, 1, 0, 0, 'szt.', 0, 0, 130, 105.69, 130, 23, 0, 0, 0, 0, '', ''),
(4, 2, 2, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'T_2', 1, 0, 0, 1, 0, 0, 'szt.', 0, 0, 130, 105.69, 130, 23, 0, 0, 0, 0, '', ''),
(5, 2, 3, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'T_3', 1, 0, 0, 1, 0, 0, 'szt.', 0, 0, 130, 105.69, 130, 23, 0, 0, 0, 0, '', ''),
(6, 2, 4, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'T_4', 1, 0, 0, 1, 0, 0, 'szt.', 0, 0, 130, 105.69, 130, 23, 0, 0, 0, 0, '', ''),
(7, 2, 5, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'T_5', 1, 0, 0, 1, 0, 0, 'szt.', 0, 0, 130, 105.69, 130, 23, 0, 0, 0, 0, '', ''),
(8, 2, 6, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'T_6', 1, 0, 0, 1, 0, 0, 'szt.', 0, 0, 130, 105.69, 130, 23, 0, 0, 0, 0, '', ''),
(9, 2, 7, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'T_7', 1, 0, 0, 1, 0, 0, 'szt.', 0, 0, 130, 105.69, 130, 23, 0, 0, 0, 0, '', ''),
(10, 2, 8, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'T_9', 1, 0, 0, 1, 0, 0, 'szt.', 0, 0, 130, 105.69, 130, 23, 0, 0, 0, 0, '', ''),
(11, 2, 9, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'AST2665', 1, 0, 0, 1, 0, 0, 'szt.', 0, 0, 130, 97.56, 120, 23, 0, 0, 0, 0, '', ''),
(12, 2, 10, 'Wysyłka 48h', 2, 'KURIER_48', 1, 0, 0, 0, 0, 0, 'szt.', 1, 1, 0, 55.56, 60, 8, 55.56, 4.44, 60, 0, '', ''),
(13, 3, 1, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'T_3', 1, 0, 0, 1, 0, 0, 'szt.', 0, 0, 130, 105.69, 130, 23, 0, 0, 0, 0, '', ''),
(14, 3, 2, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'T_9', 1, 0, 0, 1, 0, 0, 'szt.', 0, 0, 130, 105.69, 130, 23, 0, 0, 0, 0, '', ''),
(15, 3, 3, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'AST2665', 1, 0, 0, 1, 0, 0, 'szt.', 0, 0, 130, 81.3, 100, 23, 0, 0, 0, 0, '', ''),
(16, 3, 4, 'Wysyłka 48h', 2, 'KURIER_48', 1, 0, 0, 1, 0, 0, 'szt.', 1, 1, 0, 55.56, 60, 8, 55.56, 4.44, 60, 0, '', ''),
(17, 1, 1, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'AST2665', 1, 0, 0, 1, 0, 0, 'szt.', 1, 1, 0, 81.3, 100, 23, 81.3, 18.7, 100, 0, '', ''),
(18, 1, 2, 'Wysyłka 48h', 2, 'KURIER_48', 1, 0, 0, 1, 0, 0, 'szt.', 1, 1, 0, 17.59, 19, 8, 17.59, 1.41, 19, 0, '', ''),
(19, 2, 1, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'T_8', 1, 0, 0, 1, 0, 0, 'szt.', 0, 0, 130, 105.69, 130, 23, 0, 0, 0, 0, '', ''),
(20, 2, 2, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'T_2', 1, 0, 0, 1, 0, 0, 'szt.', 0, 0, 130, 105.69, 130, 23, 0, 0, 0, 0, '', ''),
(21, 2, 3, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'T_3', 1, 0, 0, 1, 0, 0, 'szt.', 0, 0, 130, 105.69, 130, 23, 0, 0, 0, 0, '', ''),
(22, 2, 4, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'T_4', 1, 0, 0, 1, 0, 0, 'szt.', 0, 0, 130, 105.69, 130, 23, 0, 0, 0, 0, '', ''),
(23, 2, 5, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'T_5', 1, 0, 0, 1, 0, 0, 'szt.', 0, 0, 130, 105.69, 130, 23, 0, 0, 0, 0, '', ''),
(24, 2, 6, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'T_6', 1, 0, 0, 1, 0, 0, 'szt.', 0, 0, 130, 105.69, 130, 23, 0, 0, 0, 0, '', ''),
(25, 2, 7, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'T_7', 1, 0, 0, 1, 0, 0, 'szt.', 0, 0, 130, 105.69, 130, 23, 0, 0, 0, 0, '', ''),
(26, 2, 8, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'T_9', 1, 0, 0, 1, 0, 0, 'szt.', 0, 0, 130, 105.69, 130, 23, 0, 0, 0, 0, '', ''),
(27, 2, 9, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'AST2665', 1, 0, 0, 1, 0, 0, 'szt.', 0, 0, 130, 97.56, 120, 23, 0, 0, 0, 0, '', ''),
(28, 2, 10, 'Wysyłka 48h', 2, 'KURIER_48', 1, 0, 0, 0, 0, 0, 'szt.', 1, 1, 0, 55.56, 60, 8, 55.56, 4.44, 60, 0, '', ''),
(29, 3, 1, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'T_3', 1, 0, 0, 1, 0, 0, 'szt.', 0, 0, 130, 105.69, 130, 23, 0, 0, 0, 0, '', ''),
(30, 3, 2, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'T_9', 1, 0, 0, 1, 0, 0, 'szt.', 0, 0, 130, 105.69, 130, 23, 0, 0, 0, 0, '', ''),
(31, 3, 3, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'AST2665', 1, 0, 0, 1, 0, 0, 'szt.', 0, 0, 130, 81.3, 100, 23, 0, 0, 0, 0, '', ''),
(32, 3, 4, 'Wysyłka 48h', 2, 'KURIER_48', 1, 0, 0, 1, 0, 0, 'szt.', 1, 1, 0, 55.56, 60, 8, 55.56, 4.44, 60, 0, '', ''),
(33, 4, 1, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'AST2665', 1, 0, 0, 1, 0, 0, 'szt.', 1, 1, 0, 81.3, 100, 23, 81.3, 18.7, 100, 0, '', ''),
(34, 4, 2, 'Wysyłka 48h', 2, 'KURIER_48', 1, 0, 0, 1, 0, 0, 'szt.', 1, 1, 0, 17.59, 19, 8, 17.59, 1.41, 19, 0, '', ''),
(35, 5, 1, 'Tytuł tego przedmiotu o długości jaką przedstawia', 1, 'AST2665', 1, 0, 0, 0, 0, 0, 'szt.', 1, 1, 0, 81.3, 100, 23, 81.3, 18.7, 100, 0, '', ''),
(36, 5, 2, 'Wysyłka 48h', 2, 'KURIER_48', 1, 0, 0, 1, 0, 0, 'szt.', 1, 1, 0, 17.59, 19, 8, 17.59, 1.41, 19, 0, '', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `footer_link`
--

CREATE TABLE `footer_link` (
  `id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `label` varchar(80) NOT NULL,
  `title` varchar(150) DEFAULT NULL,
  `target` varchar(10) DEFAULT '_blank',
  `kolejnosc` int(11) DEFAULT '0',
  `grupa` int(11) DEFAULT NULL,
  `strona_id` int(11) DEFAULT NULL,
  `nofollow` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `footer_link`
--

INSERT INTO `footer_link` (`id`, `url`, `label`, `title`, `target`, `kolejnosc`, `grupa`, `strona_id`, `nofollow`) VALUES
(1, '#', 'Status zamówienia', 'Status zamówienia', '_self', 6, 1, NULL, 0),
(2, '#', 'Wysyłka i dostawa', 'Wysyłka i dostawa', '_self', 8, 1, NULL, 0),
(3, '#', 'Zwroty', 'Zwroty', '_self', 7, 1, NULL, 0),
(4, '#', 'Opcje płatności', '#', '_self', 9, 1, NULL, 0),
(5, '/kontakt', 'Kontakt', 'Kontakt', '_self', 2, 2, NULL, 0),
(6, '#', 'Status zamówienia', 'Status zamówienia', '_self', 5, 2, NULL, 0),
(7, '#', 'Status zamówienia', 'Status zamówienia', '_self', 3, 2, NULL, 0),
(11, 'O nas', 'O nas', 'O nas', '_self', 4, 1, 2, 0),
(13, 'Dostawa', 'Dostawa', 'Dostawa', '_self', 1, 1, 6, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gatunek`
--

CREATE TABLE `gatunek` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(80) NOT NULL,
  `zdjecie` varchar(128) DEFAULT NULL,
  `kolejnosc` int(11) DEFAULT '0',
  `ukryty` tinyint(1) DEFAULT '0',
  `glowna` tinyint(1) DEFAULT '0',
  `mask` varchar(128) DEFAULT NULL,
  `nazwa_2` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gatunek_i18n`
--

CREATE TABLE `gatunek_i18n` (
  `id` int(11) NOT NULL,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `google_kategorie`
--

CREATE TABLE `google_kategorie` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `google_kategorie`
--

INSERT INTO `google_kategorie` (`id`, `nazwa`) VALUES
(1, 'Zwierzęta i artykuły dla zwierząt'),
(2, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt'),
(3, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla ptaków'),
(4, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla kotów'),
(5, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla psów'),
(6, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla rybek'),
(7, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla gadów i płazów'),
(8, 'Sztuka i rozrywka'),
(9, 'Dom i ogród / Ozdoby / Dzieła sztuki'),
(11, 'Dom i ogród / Ozdoby / Dzieła sztuki / Rzeźby i posągi'),
(16, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby'),
(18, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do mierzenia i oznaczania / Pędzle malarskie'),
(19, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Tekstylia / Płótna artystyczne / Kanwy i tablice malarskie'),
(24, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Rękodzieło z papieru'),
(32, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Ozdoby / Koraliki'),
(33, 'Sztuka i rozrywka / Hobby i sztuki piękne / Przybory do żonglowania'),
(35, 'Sztuka i rozrywka / Hobby i sztuki piękne / Magia i sztuczki'),
(36, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Kleje i magnesy dla rękodzieła / Magnesy ozdobne'),
(37, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Modele redukcyjne'),
(39, 'Aparaty, kamery i przyrządy optyczne / Fotografia'),
(40, 'Dom i ogród / Artykuły gospodarstwa domowego / Przechowywanie i utrzymanie porządku / Przechowywanie zdjęć / Albumy na zdjęcia'),
(41, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Ciemnia'),
(42, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Oświetlenie i studio'),
(44, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Garncarstwo i rzeźbiarstwo'),
(47, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Tekstylia / Tkaniny'),
(49, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Włókna do rękodzieła / Nici'),
(53, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Wręczanie prezentów / Bony podarunkowe'),
(54, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne'),
(55, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych'),
(56, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Wzmacniacze do instrumentów muzycznych'),
(57, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów dętych blaszanych'),
(59, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do perkusji / Pałki i miotełki perkusyjne'),
(60, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do keyboardów'),
(61, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych'),
(62, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte'),
(63, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty dęte blaszane'),
(65, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty dęte blaszane / Eufonia'),
(67, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty dęte blaszane / Rogi'),
(70, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty dęte blaszane / Puzony'),
(72, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty dęte blaszane / Tuby'),
(74, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Elektroniczne instrumenty muzyczne / Keyboardy'),
(75, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne'),
(76, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Pianina i fortepiany'),
(77, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty strunowe'),
(78, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty strunowe / Kontrabasy'),
(79, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty strunowe / Wiolonczele'),
(80, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty strunowe / Gitary'),
(84, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty strunowe / Harfy'),
(85, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty strunowe / Altówki'),
(86, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty strunowe / Skrzypce'),
(87, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty dęte drewniane'),
(88, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty dęte drewniane / Klarnety'),
(89, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty dęte drewniane / Flety'),
(90, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty dęte drewniane / Flety proste'),
(91, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty dęte drewniane / Saksofony'),
(93, 'Artykuły biurowe / Akcesoria do książek / Zakładki'),
(94, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Wręczanie prezentów / Pakowanie prezentów'),
(95, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Wręczanie prezentów / Kartki okolicznościowe'),
(96, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia'),
(97, 'Artykuły religijne i dewocjonalia / Artykuły religijne'),
(100, 'Torby, walizki i akcesoria podróżne / Plecaki'),
(101, 'Torby, walizki i akcesoria podróżne / Aktówki'),
(103, 'Torby, walizki i akcesoria podróżne / Torby sportowe'),
(104, 'Torby, walizki i akcesoria podróżne / Saszetki na biodra'),
(105, 'Torby, walizki i akcesoria podróżne / Torby na ubrania'),
(106, 'Torby, walizki i akcesoria podróżne / Listonoszki'),
(107, 'Torby, walizki i akcesoria podróżne / Walizki'),
(108, 'Torby, walizki i akcesoria podróżne / Kosmetyczki'),
(110, 'Torby, walizki i akcesoria podróżne / Akcesoria podróżne'),
(111, 'Biznes i przemysł'),
(112, 'Biznes i przemysł / Rolnictwo'),
(113, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Nawozy'),
(114, 'Biznes i przemysł / Budownictwo'),
(115, 'Sprzęt / Materiały budowlane'),
(119, 'Sprzęt / Materiały budowlane / Drzwi'),
(120, 'Sprzęt / Materiały budowlane / Szkło'),
(121, 'Sprzęt / Materiały budowlane / Pokrycia dachowe / Rynny'),
(122, 'Sprzęt / Materiały budowlane / Izolacje'),
(123, 'Sprzęt / Materiały budowlane / Pokrycia dachowe'),
(124, 'Sprzęt / Materiały budowlane / Okna'),
(125, 'Sprzęt / Materiały budowlane / Drewno'),
(126, 'Sprzęt / Narzędzia / Wózki ręczne'),
(127, 'Sprzęt / Materiały elektryczne'),
(128, 'Sprzęt / Ogrodzenia i barierki'),
(130, 'Sprzęt / Narzędzia / Drabiny i rusztowania'),
(131, 'Biznes i przemysł / Transport materiałów / Narzędzia do wciągania i podnoszenia'),
(133, 'Sprzęt / Hydraulika'),
(134, 'Biznes i przemysł / Budownictwo / Geodezja'),
(135, 'Biznes i przemysł / Usługi gastronomiczne'),
(136, 'Żywność, napoje i tytoń / Żywność / Kosze prezentowe'),
(137, 'Biznes i przemysł / Usługi gastronomiczne / Automaty sprzedające'),
(138, 'Biznes i przemysł / Handel detaliczny'),
(139, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Urządzenia wejściowe / Skanery kodów kreskowych'),
(141, 'Aparaty, kamery i przyrządy optyczne'),
(142, 'Aparaty, kamery i przyrządy optyczne / Aparaty i kamery'),
(143, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria i części do aparatów'),
(146, 'Elektronika / Akcesoria elektroniczne / Adaptery / Adaptery kart pamięci'),
(147, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria do obiektywów / Filtry na obiektywy'),
(148, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria i części do aparatów / Lampy błyskowe'),
(149, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Obiektywy do aparatów i kamer'),
(150, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Statywy trójnożne i jednonożne'),
(152, 'Aparaty, kamery i przyrządy optyczne / Aparaty i kamery / Aparaty cyfrowe'),
(153, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria i części do aparatów / Filmy do aparatów i kamer'),
(154, 'Aparaty, kamery i przyrządy optyczne / Aparaty i kamery / Aparaty i kamery analogowe'),
(155, 'Aparaty, kamery i przyrządy optyczne / Aparaty i kamery / Kamery wideo'),
(156, 'Aparaty, kamery i przyrządy optyczne / Przyrządy optyczne'),
(157, 'Aparaty, kamery i przyrządy optyczne / Przyrządy optyczne / Lornetki'),
(158, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Sprzęt laboratoryjny / Mikroskopy'),
(160, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria optyczne'),
(161, 'Aparaty, kamery i przyrządy optyczne / Przyrządy optyczne / Dalmierze'),
(165, 'Aparaty, kamery i przyrządy optyczne / Przyrządy optyczne / Lunety teleskopowe / Teleskopy'),
(166, 'Ubrania i akcesoria'),
(167, 'Ubrania i akcesoria / Akcesoria do ubrań'),
(168, 'Ubrania i akcesoria / Akcesoria do ubrań / Bandany i chusty na głowę'),
(169, 'Ubrania i akcesoria / Akcesoria do ubrań / Paski'),
(170, 'Ubrania i akcesoria / Akcesoria do ubrań / Rękawiczki i mitenki'),
(171, 'Ubrania i akcesoria / Akcesoria do ubrań / Akcesoria do włosów'),
(173, 'Ubrania i akcesoria / Akcesoria do ubrań / Czapki'),
(175, 'Ubrania i akcesoria / Akcesoria do torebek i portfeli / Breloczki'),
(176, 'Ubrania i akcesoria / Akcesoria do ubrań / Krawaty i muszki'),
(177, 'Ubrania i akcesoria / Akcesoria do ubrań / Szaliki i szale'),
(178, 'Ubrania i akcesoria / Akcesoria do ubrań / Okulary przeciwsłoneczne'),
(179, 'Ubrania i akcesoria / Akcesoria do ubrań / Szelki'),
(180, 'Ubrania i akcesoria / Akcesoria do ubrań / Spinki do krawatów'),
(181, 'Ubrania i akcesoria / Akcesoria do ubrań / Akcesoria do włosów / Peruki'),
(182, 'Ubrania i akcesoria / Ubrania / Ubrania dla dzieci i niemowląt'),
(184, 'Ubrania i akcesoria / Przebrania i akcesoria'),
(187, 'Ubrania i akcesoria / Buty'),
(188, 'Ubrania i akcesoria / Biżuteria'),
(189, 'Ubrania i akcesoria / Biżuteria / Biżuteria na kostki'),
(190, 'Ubrania i akcesoria / Biżuteria / Biżuteria do ciała'),
(191, 'Ubrania i akcesoria / Biżuteria / Bransoletki'),
(192, 'Ubrania i akcesoria / Biżuteria / Zawieszki i wisiorki'),
(193, 'Ubrania i akcesoria / Akcesoria do ubrań / Spinki do mankietów'),
(194, 'Ubrania i akcesoria / Biżuteria / Kolczyki'),
(196, 'Ubrania i akcesoria / Biżuteria / Naszyjniki'),
(197, 'Ubrania i akcesoria / Biżuteria / Broszki i przypinki'),
(198, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Ozdoby / Kamienie luzem'),
(200, 'Ubrania i akcesoria / Biżuteria / Pierścionki'),
(201, 'Ubrania i akcesoria / Biżuteria / Zegarki'),
(203, 'Ubrania i akcesoria / Ubrania / Odzież wierzchnia'),
(204, 'Ubrania i akcesoria / Ubrania / Spodnie'),
(206, 'Ubrania i akcesoria / Ubrania / Uniformy / Mundurki szkolne'),
(207, 'Ubrania i akcesoria / Ubrania / Szorty'),
(208, 'Ubrania i akcesoria / Ubrania / Piżamy i ubrania na co dzień'),
(209, 'Ubrania i akcesoria / Ubrania / Bielizna i skarpety / Skarpety'),
(211, 'Ubrania i akcesoria / Ubrania / Stroje kąpielowe'),
(212, 'Ubrania i akcesoria / Ubrania / Podkoszulki i topy'),
(213, 'Ubrania i akcesoria / Ubrania / Bielizna i skarpety'),
(214, 'Ubrania i akcesoria / Ubrania / Bielizna i skarpety / Biustonosze'),
(215, 'Ubrania i akcesoria / Ubrania / Bielizna i skarpety / Wyroby pończosznicze'),
(216, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie'),
(217, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Monety i waluty kolekcjonerskie'),
(218, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Skały i skamieniałości'),
(219, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Znaczki pocztowe'),
(220, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Broń kolekcjonerska'),
(221, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Broń kolekcjonerska / Miecze kolekcjonerskie'),
(222, 'Elektronika'),
(223, 'Elektronika / Audio'),
(224, 'Elektronika / Audio / Elementy audio / Wzmacniacze audio'),
(225, 'Elektronika / Audio / Odtwarzacze i nagrywarki audio / Boomboksy'),
(226, 'Elektronika / Audio / Odtwarzacze i nagrywarki audio / Odtwarzacze i nagrywarki CD'),
(230, 'Elektronika / Audio / Odtwarzacze i nagrywarki audio / Systemy karaoke'),
(232, 'Elektronika / Audio / Akcesoria audio / Akcesoria do odtwarzaczy MP3'),
(233, 'Elektronika / Audio / Odtwarzacze i nagrywarki audio / Odtwarzacze MP3'),
(234, 'Elektronika / Audio / Elementy audio / Mikrofony'),
(235, 'Elektronika / Audio / Odtwarzacze i nagrywarki audio / Odtwarzacze i nagrywarki MiniDisc'),
(236, 'Elektronika / Audio / Elementy audio / Miksery audio'),
(239, 'Elektronika / Audio / Akcesoria audio / Akcesoria do radia satelitarnego'),
(241, 'Elektronika / Audio / Elementy audio / Odbiorniki audio i wideo'),
(242, 'Elektronika / Audio / Odtwarzacze i nagrywarki audio'),
(243, 'Elektronika / Audio / Odtwarzacze i nagrywarki audio / Odtwarzacze i nagrywarki kaset'),
(244, 'Elektronika / Audio / Odtwarzacze i nagrywarki audio / Dyktafony'),
(245, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Elektroniczne instrumenty muzyczne / Samplery audio'),
(246, 'Elektronika / Audio / Elementy audio / Procesory sygnałowe'),
(247, 'Elektronika / Audio / Elementy audio / Procesory sygnałowe / Procesory efektów'),
(248, 'Elektronika / Audio / Elementy audio / Procesory sygnałowe / Equalizery'),
(249, 'Elektronika / Audio / Elementy audio / Głośniki'),
(251, 'Elektronika / Audio / Odtwarzacze i nagrywarki audio / Systemy stereo'),
(252, 'Elektronika / Audio / Odtwarzacze i nagrywarki audio / Systemy kina domowego'),
(256, 'Elektronika / Audio / Odtwarzacze i nagrywarki audio / Gramofony i adaptery'),
(258, 'Elektronika / Akcesoria elektroniczne / Adaptery'),
(259, 'Elektronika / Akcesoria elektroniczne / Kable'),
(262, 'Elektronika / Telekomunikacja'),
(263, 'Elektronika / Telekomunikacja / Akcesoria do komunikacji radiowej'),
(264, 'Elektronika / Telekomunikacja / Telefonia / Akcesoria do telefonów komórkowych'),
(265, 'Elektronika / Telekomunikacja / Telefonia / Akcesoria do telefonów'),
(266, 'Elektronika / Telekomunikacja / Automatyczne sekretarki'),
(267, 'Elektronika / Telekomunikacja / Telefonia / Telefony komórkowe'),
(268, 'Elektronika / Telekomunikacja / Pagery'),
(269, 'Elektronika / Telekomunikacja / Telefonia / Akcesoria do telefonów / Karty telefoniczne'),
(270, 'Elektronika / Telekomunikacja / Telefonia'),
(271, 'Elektronika / Telekomunikacja / Telefonia / Telefony przewodowe'),
(272, 'Elektronika / Telekomunikacja / Telefonia / Telefony bezprzewodowe'),
(273, 'Elektronika / Telekomunikacja / Urządzenia radiowe / Radia dwukierunkowe'),
(274, 'Elektronika / Telekomunikacja / Konferencje wideo'),
(275, 'Elektronika / Akcesoria elektroniczne / Zasilanie'),
(276, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Akumulatory'),
(278, 'Elektronika / Komputery'),
(279, 'Elektronika / Akcesoria elektroniczne / Akcesoria komputerowe'),
(280, 'Elektronika / Akcesoria elektroniczne / Akcesoria komputerowe / Akcesoria do palmtopów'),
(283, 'Elektronika / Wideo / Akcesoria do wideo / Akcesoria do monitorów komputerowych'),
(284, 'Elektronika / Drukowanie, kopiowanie, skanowanie i faksowanie / Akcesoria do skanerów'),
(285, 'Elektronika / Akcesoria elektroniczne / Elementy komputera'),
(286, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Karty i adaptery I/O / Karty i adaptery audio'),
(287, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Karty i adaptery I/O'),
(288, 'Elektronika / Akcesoria elektroniczne / Nośniki pamięci'),
(289, 'Elektronika / Płyty drukowane i komponenty / Obwody drukowane / Obwody drukowane do komputerów / Płyty główne'),
(290, 'Elektronika / Sieci / Karty i adaptery sieciowe'),
(291, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Zasilacze komputerowe'),
(292, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Procesory komputerowe'),
(293, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Szafy rackowe'),
(294, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Komputerowe zestawy startowe'),
(295, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Części do systemów chłodzenia komputerów'),
(296, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Obudowy komputerowe'),
(297, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Karty i adaptery I/O / Karty i adaptery wideo'),
(298, 'Elektronika / Komputery / Palmtopy'),
(300, 'Elektronika / Akcesoria elektroniczne / Akcesoria komputerowe / Stacje dokujące do laptopów'),
(301, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Urządzenia wejściowe / Kontrolery do gier'),
(302, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Urządzenia wejściowe / Tablety graficzne'),
(303, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Urządzenia wejściowe / Klawiatury komputerowe'),
(304, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Urządzenia wejściowe / Myszki i trackballe'),
(305, 'Elektronika / Wideo / Monitory komputerowe'),
(306, 'Elektronika / Drukowanie, kopiowanie, skanowanie i faksowanie / Skanery'),
(308, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Urządzenia wejściowe / Gładziki'),
(311, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Huby USB i FireWire'),
(312, 'Aparaty, kamery i przyrządy optyczne / Aparaty i kamery / Kamery internetowe'),
(313, 'Oprogramowanie / Programy komputerowe'),
(315, 'Oprogramowanie / Programy komputerowe / Kompilatory i narzędzia programistyczne'),
(317, 'Oprogramowanie / Programy komputerowe / Programy edukacyjne'),
(318, 'Oprogramowanie / Programy komputerowe / Programy na palmtopy i urządzenia PDA'),
(319, 'Oprogramowanie / Programy komputerowe / Programy do multimediów i projektowania'),
(321, 'Oprogramowanie / Programy komputerowe / Systemy operacyjne'),
(325, 'Elektronika / Komputery / Komputery stacjonarne'),
(328, 'Elektronika / Komputery / Laptopy'),
(331, 'Elektronika / Komputery / Serwery komputerowe'),
(333, 'Artykuły biurowe / Sprzęt biurowy / Kalkulatory'),
(337, 'Artykuły biurowe / Sprzęt biurowy / Słowniki i translatory elektroniczne'),
(338, 'Sprzęt / Narzędzia / Latarki'),
(339, 'Elektronika / GPS-y'),
(340, 'Elektronika / Elektronika morska'),
(341, 'Elektronika / Akcesoria elektroniczne / Piloty'),
(342, 'Elektronika / Sieci'),
(343, 'Elektronika / Sieci / Modemy'),
(345, 'Elektronika / Drukowanie, kopiowanie, skanowanie i faksowanie'),
(356, 'Elektronika / Drukowanie, kopiowanie, skanowanie i faksowanie / Akcesoria do kopiarek, drukarek i faksów / Materiały eksploatacyjne do drukarek / Tonery i kartridże atramentowe'),
(359, 'Dom i ogród / Bezpieczeństwo domu i biura'),
(360, 'Elektronika / Telekomunikacja / Interkomy'),
(361, 'Biznes i przemysł / Organy ścigania / Wykrywacze metalu'),
(362, 'Aparaty, kamery i przyrządy optyczne / Aparaty i kamery / Kamery przemysłowe'),
(363, 'Dom i ogród / Bezpieczeństwo domu i biura / Światła bezpieczeństwa'),
(364, 'Dom i ogród / Bezpieczeństwo domu i biura / Monitory i rejestratory bezpieczeństwa'),
(365, 'Dom i ogród / Bezpieczeństwo domu i biura / Czujniki systemu bezpieczeństwa'),
(367, 'Elektronika / Akcesoria elektroniczne / Czyste nośniki'),
(376, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Pamięci komputerowe / Duplikatory dysków / Duplikatory CD/DVD'),
(377, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Pamięci komputerowe / Napędy optyczne'),
(380, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Pamięci komputerowe / Dyski twarde'),
(381, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Pamięci komputerowe / Akcesoria do dysków twardych / Futerały na dyski twarde'),
(384, 'Elektronika / Akcesoria elektroniczne / Nośniki pamięci / Pamięci flash'),
(385, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Pamięci komputerowe / Napędy taśmowe'),
(386, 'Elektronika / Wideo'),
(387, 'Elektronika / Wideo / Odtwarzacze i nagrywarki wideo'),
(388, 'Elektronika / Wideo / Odtwarzacze i nagrywarki wideo / Odtwarzacze DVD i Blu-ray'),
(389, 'Elektronika / Wideo / Odtwarzacze i nagrywarki wideo / Nagrywarki DVD'),
(390, 'Elektronika / Wideo / Odtwarzacze i nagrywarki wideo / Rejestratory cyfrowe DVR'),
(391, 'Elektronika / Wideo / Odtwarzacze i nagrywarki wideo / Odtwarzacze wideo'),
(393, 'Elektronika / Wideo / Akcesoria do wideo / Akcesoria do projektorów'),
(394, 'Elektronika / Wideo / Akcesoria do wideo / Akcesoria do projektorów / Wymienne lampy do projektorów'),
(395, 'Elektronika / Wideo / Akcesoria do wideo / Akcesoria do projektorów / Ekrany projekcyjne'),
(396, 'Elektronika / Wideo / Projektory'),
(397, 'Elektronika / Wideo / Projektory / Projektory multimedialne'),
(398, 'Elektronika / Wideo / Projektory / Rzutniki pisma'),
(399, 'Elektronika / Wideo / Projektory / Rzutniki przezroczy'),
(401, 'Elektronika / Wideo / Telewizja satelitarna i kablowa / Odbiorniki telewizji satelitarnej'),
(403, 'Elektronika / Wideo / Akcesoria do wideo / Akcesoria i części do telewizorów'),
(404, 'Elektronika / Wideo / Telewizory'),
(408, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Efekty specjalne'),
(409, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Efekty specjalne / Maszyny do mgły'),
(410, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Efekty specjalne / Oświetlenie specjalne'),
(412, 'Żywność, napoje i tytoń'),
(413, 'Żywność, napoje i tytoń / Napoje'),
(414, 'Żywność, napoje i tytoń / Napoje / Napoje alkoholowe / Piwa'),
(415, 'Żywność, napoje i tytoń / Napoje / Gorąca czekolada'),
(417, 'Żywność, napoje i tytoń / Napoje / Napoje alkoholowe / Likiery i trunki'),
(418, 'Żywność, napoje i tytoń / Napoje / Mleko'),
(420, 'Żywność, napoje i tytoń / Napoje / Wody'),
(421, 'Żywność, napoje i tytoń / Napoje / Napoje alkoholowe / Wina'),
(422, 'Żywność, napoje i tytoń / Żywność'),
(423, 'Żywność, napoje i tytoń / Żywność / Przekąski'),
(424, 'Żywność, napoje i tytoń / Żywność / Wypieki / Chleby i bułki'),
(427, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy'),
(428, 'Żywność, napoje i tytoń / Żywność / Nabiał'),
(429, 'Żywność, napoje i tytoń / Żywność / Nabiał / Sery'),
(430, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa'),
(431, 'Żywność, napoje i tytoń / Żywność / Ziarna, ryż i płatki'),
(432, 'Żywność, napoje i tytoń / Żywność / Mięso, owoce morza i jajka'),
(433, 'Żywność, napoje i tytoń / Żywność / Orzechy i ziarna'),
(434, 'Żywność, napoje i tytoń / Żywność / Makarony i kluski'),
(435, 'Żywność, napoje i tytoń / Wyroby tytoniowe'),
(436, 'Meble'),
(438, 'Meble / Krzesła i fotele / Worki do siedzenia'),
(441, 'Meble / Ławki'),
(442, 'Meble / Wózki'),
(443, 'Meble / Krzesła i fotele'),
(447, 'Meble / Szafy i przechowywanie / Bufety i kredensy'),
(448, 'Meble / Szafy i przechowywanie / Kredensy na porcelanę'),
(450, 'Meble / Futony'),
(451, 'Meble / Łóżka i akcesoria / Zagłówki i oparcia na stopy'),
(453, 'Meble / Wózki / Wózki kuchenne'),
(456, 'Meble / Krzesła i fotele / Szezlongi'),
(457, 'Meble / Meble RTV'),
(458, 'Meble / Otomany'),
(460, 'Meble / Sofy'),
(462, 'Meble / Stoły / Stoliki nocne'),
(463, 'Meble / Szafy i przechowywanie / Szafy na dokumenty'),
(464, 'Meble / Półki i regały'),
(465, 'Meble / Półki i regały / Regały i półki stojące'),
(469, 'Zdrowie i uroda'),
(471, 'Zdrowie i uroda / Higiena osobista / Masaż i relaks / Masażery'),
(472, 'Zdrowie i uroda / Higiena osobista / Solaria'),
(473, 'Zdrowie i uroda / Higiena osobista / Kosmetyki'),
(474, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria i kosmetyki do kąpieli'),
(475, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Zestawy do kąpieli'),
(476, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do makijażu / Lusterka kosmetyczne'),
(477, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż'),
(478, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja paznokci'),
(479, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Perfumy i wody kolońskie'),
(481, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja skóry / Kosmetyki do cery trądzikowej'),
(482, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja skóry / Odżywki i balsamy do ust'),
(484, 'Zdrowie i uroda / Higiena osobista / Dezodoranty i antyperspiranty'),
(485, 'Zdrowie i uroda / Higiena osobista / Środki do higieny intymnej dla kobiet'),
(486, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja włosów'),
(487, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja włosów / Narzędzia do stylizacji włosów / Grzebienie i szczotki'),
(488, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja włosów / Narzędzia do stylizacji włosów / Produkty nadające skręt włosom'),
(489, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja włosów / Narzędzia do stylizacji włosów / Lokówki do włosów'),
(490, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja włosów / Narzędzia do stylizacji włosów / Suszarki do włosów'),
(491, 'Zdrowie i uroda / Opieka zdrowotna'),
(493, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja pleców'),
(494, 'Zdrowie i uroda / Opieka zdrowotna / Monitory funkcji życiowych'),
(495, 'Zdrowie i uroda / Opieka zdrowotna / Monitory funkcji życiowych / Ciśnieniomierze'),
(496, 'Zdrowie i uroda / Opieka zdrowotna / Monitory funkcji życiowych / Urządzenia do pomiaru tkanki tłuszczowej'),
(497, 'Zdrowie i uroda / Opieka zdrowotna / Monitory funkcji życiowych / Urządzenia do pomiaru cholesterolu'),
(500, 'Zdrowie i uroda / Opieka zdrowotna / Monitory funkcji życiowych / Wagi osobowe'),
(501, 'Zdrowie i uroda / Opieka zdrowotna / Monitory funkcji życiowych / Termometry medyczne'),
(506, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja uszu'),
(508, 'Zdrowie i uroda / Opieka zdrowotna / Pierwsza pomoc'),
(509, 'Zdrowie i uroda / Opieka zdrowotna / Pierwsza pomoc / Środki opatrunkowe i bandaże'),
(510, 'Zdrowie i uroda / Opieka zdrowotna / Pierwsza pomoc / Zestawy pierwszej pomocy'),
(511, 'Biznes i przemysł / Branża medyczna / Medyczne materiały eksploatacyjne / Rękawiczki jednorazowe'),
(512, 'Dom i ogród / Artykuły gospodarstwa domowego / Dezynsekcja / Środki odstraszające / Domowe odstraszacze insektów'),
(513, 'Biznes i przemysł / Środki ochrony osobistej / Maski ochronne / Maski medyczne'),
(515, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja stóp'),
(516, 'Zdrowie i uroda / Opieka zdrowotna / Pierwsza pomoc / Leczenie ciepłem i zimnem'),
(517, 'Zdrowie i uroda / Opieka zdrowotna / Środki higieniczne dla osób cierpiących na nietrzymanie moczu'),
(518, 'Zdrowie i uroda / Opieka zdrowotna / Lekarstwa'),
(519, 'Zdrowie i uroda / Opieka zdrowotna / Artykuły dla osób mających trudności z poruszaniem się'),
(520, 'Zdrowie i uroda / Opieka zdrowotna / Artykuły dla osób mających trudności z poruszaniem się / Sprzęt ułatwiający poruszanie się'),
(521, 'Zdrowie i uroda / Opieka zdrowotna / Artykuły dla osób mających trudności z poruszaniem się / Akcesoria do sprzętu ułatwiającego poruszanie się'),
(523, 'Zdrowie i uroda / Opieka zdrowotna / Usztywniacze ortopedyczne i stabilizatory'),
(524, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja oczu / Okulary'),
(525, 'Zdrowie i uroda / Opieka zdrowotna / Zdrowy tryb życia i dieta / Witaminy i suplementy diety'),
(526, 'Zdrowie i uroda / Higiena osobista / Higiena jamy ustnej'),
(527, 'Zdrowie i uroda / Higiena osobista / Higiena jamy ustnej / Szczoteczki do zębów'),
(528, 'Zdrowie i uroda / Higiena osobista / Golenie i strzyżenie'),
(529, 'Zdrowie i uroda / Higiena osobista / Golenie i strzyżenie / Płyny po goleniu'),
(531, 'Zdrowie i uroda / Higiena osobista / Golenie i strzyżenie / Akcesoria do golarek elektrycznych'),
(532, 'Zdrowie i uroda / Higiena osobista / Golenie i strzyżenie / Golarki elektryczne'),
(533, 'Zdrowie i uroda / Higiena osobista / Golenie i strzyżenie / Trymery do włosów'),
(534, 'Zdrowie i uroda / Higiena osobista / Golenie i strzyżenie / Maszynki i ostrza do golenia'),
(536, 'Dom i ogród'),
(537, 'Dzieci i niemowlęta'),
(538, 'Dzieci i niemowlęta / Transport niemowląt / Nosidełka dla niemowląt'),
(539, 'Dzieci i niemowlęta / Zabawki dla niemowląt / Kojce'),
(540, 'Dzieci i niemowlęta / Bezpieczeństwo niemowląt'),
(541, 'Dzieci i niemowlęta / Bezpieczeństwo niemowląt / Nianie elektroniczne'),
(542, 'Dzieci i niemowlęta / Bezpieczeństwo niemowląt / Bramki ochronne dla dzieci i zwierząt'),
(543, 'Dzieci i niemowlęta / Bezpieczeństwo niemowląt / Blokady i zabezpieczenia przed dziećmi'),
(544, 'Dzieci i niemowlęta / Bezpieczeństwo niemowląt / Barierki do łóżek dziecięcych'),
(547, 'Dzieci i niemowlęta / Transport niemowląt / Foteliki samochodowe dla dzieci i niemowląt'),
(548, 'Dzieci i niemowlęta / Przewijanie'),
(549, 'Torby, walizki i akcesoria podróżne / Torby na akcesoria dziecięce'),
(550, 'Dzieci i niemowlęta / Przewijanie / Kubły na pieluchy'),
(551, 'Dzieci i niemowlęta / Przewijanie / Pieluchy'),
(552, 'Dzieci i niemowlęta / Trening czystości / Nocniki'),
(553, 'Dzieci i niemowlęta / Przewijanie / Chusteczki dla niemowląt'),
(554, 'Meble / Meble dla dzieci i niemowląt'),
(555, 'Dzieci i niemowlęta / Zabawki dla niemowląt / Bujaczki i leżaki bujane dla niemowląt'),
(558, 'Meble / Meble dla dzieci i niemowląt / Przewijaki'),
(559, 'Meble / Meble dla dzieci i niemowląt / Krzesełka do karmienia i siedziska na krzesła'),
(560, 'Dzieci i niemowlęta / Zabawki dla niemowląt / Szelki do skakania i huśtawki'),
(561, 'Dzieci i niemowlęta / Karmienie'),
(562, 'Dzieci i niemowlęta / Karmienie / Jedzenie dla dzieci i niemowląt'),
(563, 'Dzieci i niemowlęta / Karmienie / Jedzenie dla dzieci i niemowląt / Mleko w proszku dla niemowląt'),
(564, 'Dzieci i niemowlęta / Karmienie / Butelki dla niemowląt'),
(565, 'Dzieci i niemowlęta / Karmienie / Laktatory'),
(566, 'Dzieci i niemowlęta / Zdrowie niemowląt / Smoczki i gryzaki'),
(567, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja skóry'),
(568, 'Dzieci i niemowlęta / Transport niemowląt / Wózki dla niemowląt'),
(569, 'Dom i ogród / Bielizna stołowa i pościelowa / Artykuły pościelowe'),
(572, 'Dom i ogród / Ozdoby / Tabliczki adresowe'),
(573, 'Dom i ogród / Ozdoby / Koszyki'),
(574, 'Dom i ogród / Akcesoria łazienkowe'),
(575, 'Dom i ogród / Akcesoria łazienkowe / Półki na wannę'),
(576, 'Dom i ogród / Bielizna stołowa i pościelowa / Ręczniki / Ręczniki kąpielowe i myjki'),
(577, 'Dom i ogród / Akcesoria łazienkowe / Maty i dywaniki łazienkowe'),
(578, 'Dom i ogród / Akcesoria łazienkowe / Zawieszki do zasłon prysznicowych'),
(579, 'Dom i ogród / Akcesoria łazienkowe / Pojemniki na chusteczki'),
(580, 'Dom i ogród / Akcesoria łazienkowe / Zasłony prysznicowe'),
(581, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Części do prysznica / Głowice prysznicowe'),
(582, 'Dom i ogród / Akcesoria łazienkowe / Mydelniczki'),
(583, 'Dom i ogród / Akcesoria łazienkowe / Szczotki toaletowe z uchwytami'),
(584, 'Dom i ogród / Akcesoria łazienkowe / Uchwyty na papier toaletowy'),
(585, 'Dom i ogród / Akcesoria łazienkowe / Uchwyty na szczoteczki do zębów'),
(586, 'Dom i ogród / Akcesoria łazienkowe / Uchwyty i wieszaki na ręczniki'),
(587, 'Dom i ogród / Ozdoby / Podpórki na książki'),
(588, 'Dom i ogród / Ozdoby / Zapachy do domu / Świece'),
(592, 'Dom i ogród / Ozdoby / Zapachy do domu'),
(594, 'Dom i ogród / Oświetlenie'),
(595, 'Dom i ogród / Ozdoby / Lustra'),
(596, 'Dom i ogród / Ozdoby / Ozdoby sezonowe i świąteczne'),
(597, 'Dom i ogród / Ozdoby / Ramy do obrazów'),
(598, 'Dom i ogród / Ozdoby / Dywaniki podłogowe'),
(599, 'Dom i ogród / Ozdoby / Narzuty'),
(600, 'Dom i ogród / Akcesoria dla palaczy'),
(601, 'Dom i ogród / Bielizna stołowa i pościelowa / Bielizna stołowa'),
(602, 'Dom i ogród / Ozdoby / Wazony'),
(603, 'Dom i ogród / Ozdoby / Dekoracje okienne'),
(604, 'Dom i ogród / Sprzęt AGD'),
(605, 'Dom i ogród / Sprzęt AGD / Ogrzewanie, wentylacja i klimatyzacja / Klimatyzatory'),
(606, 'Dom i ogród / Sprzęt AGD / Ogrzewanie, wentylacja i klimatyzacja / Oczyszczacze powietrza'),
(607, 'Dom i ogród / Sprzęt AGD / Ogrzewanie, wentylacja i klimatyzacja / Osuszacze'),
(608, 'Dom i ogród / Sprzęt AGD / Ogrzewanie, wentylacja i klimatyzacja / Wentylatory'),
(609, 'Dom i ogród / Sprzęt AGD / Systemy napędowe do bram garażowych'),
(610, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Usuwanie odpadów'),
(611, 'Dom i ogród / Sprzęt AGD / Ogrzewanie, wentylacja i klimatyzacja / Piecyki przenośne'),
(613, 'Dom i ogród / Sprzęt AGD / Ogrzewanie, wentylacja i klimatyzacja / Nawilżacze'),
(615, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Urządzenia do tekstyliów / Maszyny do szycia'),
(616, 'Dom i ogród / Sprzęt AGD / Myjki parowe do dywanów'),
(618, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do odkurzaczy'),
(619, 'Dom i ogród / Sprzęt AGD / Odkurzacze'),
(621, 'Dom i ogród / Sprzęt AGD / Podgrzewacze wody'),
(623, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania'),
(624, 'Dom i ogród / Artykuły gospodarstwa domowego / Produkty papierowe do gospodarstwa domowego / Chusteczki higieniczne'),
(625, 'Dom i ogród / Akcesoria do kominków i pieców opalanych węglem / Drewno opałowe i paliwo'),
(627, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły do pralni'),
(628, 'Dom i ogród / Artykuły gospodarstwa domowego / Środki i narzędzia do pielęgnacji obuwia'),
(629, 'Dom i ogród / Artykuły gospodarstwa domowego / Produkty papierowe do gospodarstwa domowego / Papier toaletowy'),
(630, 'Dom i ogród / Artykuły gospodarstwa domowego'),
(631, 'Dom i ogród / Artykuły gospodarstwa domowego / Przechowywanie i utrzymanie porządku / Przechowywanie ubrań / Wieszaki'),
(632, 'Sprzęt'),
(633, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły do pralni / Deski do prasowania'),
(634, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły do pralni / Kosze do prania'),
(635, 'Sprzęt / Narzędzia / Drabiny i rusztowania / Podesty'),
(636, 'Dom i ogród / Artykuły gospodarstwa domowego / Przechowywanie i utrzymanie porządku'),
(637, 'Dom i ogród / Artykuły gospodarstwa domowego / Przechowywanie śmieci / Kosze na śmieci'),
(638, 'Dom i ogród / Kuchnia i jadalnia'),
(639, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Fartuszki'),
(640, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Formy do pieczenia'),
(641, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Formy do pieczenia / Blachy do pieczenia'),
(642, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Formy do pieczenia / Formy do chleba'),
(643, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Formy do pieczenia / Formy do ciasta'),
(644, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Formy do pieczenia / Foremki do muffinów lub babeczek'),
(645, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Formy do pieczenia / Formy do tarty i kiszu'),
(646, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Formy do pieczenia / Kamienie do pizzy'),
(647, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Formy do pieczenia / Kokile'),
(648, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Formy do pieczenia / Brytfanny'),
(649, 'Dom i ogród / Kuchnia i jadalnia / Naczynia barowe'),
(650, 'Dom i ogród / Kuchnia i jadalnia / Naczynia barowe / Karafki'),
(651, 'Dom i ogród / Kuchnia i jadalnia / Naczynia barowe / Shakery i przybory do drinków'),
(652, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Dzbanki do kawy i herbaty'),
(653, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Durszlaki i sitka'),
(654, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Garnki'),
(655, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Garnki / Formy do zapiekanek'),
(656, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Garnki / Bemary'),
(657, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Garnki / Kociołki'),
(658, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Garnki / Patelnie grillowe'),
(659, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Garnki / Czajniki na gaz'),
(660, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Garnki / Szybkowary'),
(661, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Garnki / Rondle'),
(662, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Garnki / Patelnie'),
(663, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Garnki / Garnki do zup'),
(664, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Garnki / Woki'),
(665, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Noże kuchenne'),
(666, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Deski do krojenia'),
(667, 'Dom i ogród / Kuchnia i jadalnia / Przechowywanie żywności / Pojemniki do przechowywania żywności'),
(668, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne'),
(669, 'Dom i ogród / Kuchnia i jadalnia / Transportery do żywności i napojów / Pudełka i torebki na drugie śniadanie'),
(670, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Rękawice kuchenne'),
(671, 'Dom i ogród / Kuchnia i jadalnia / Transportery do żywności i napojów / Kosze piknikowe'),
(672, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe'),
(673, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Zastawy obiadowe'),
(674, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Naczynia do napojów'),
(675, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Sztućce'),
(676, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Solniczki i pieprzniczki'),
(677, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Trójnogi'),
(679, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Płyty grzewcze'),
(680, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Zmywarki'),
(681, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Zamrażarki'),
(683, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Piekarniki'),
(684, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Okapy kuchenne'),
(685, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Kuchenki'),
(686, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Lodówki'),
(687, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Wędzarnie'),
(688, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Zgniatarki do śmieci'),
(689, 'Dom i ogród / Trawnik i ogród'),
(690, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Kompostowanie / Kompost'),
(691, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Ochrona roślin'),
(693, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Szklarnie'),
(694, 'Dom i ogród / Trawnik i ogród / Silnikowe narzędzia ogrodowe / Kosiarki'),
(695, 'Dom i ogród / Akcesoria do kominków i pieców opalanych węglem / Stojaki i pojemniki na drewno'),
(696, 'Dom i ogród / Ozdoby'),
(697, 'Dom i ogród / Ozdoby / Baseniki kąpielowe dla ptaków'),
(698, 'Dom i ogród / Ozdoby / Karmniki / Karmniki dla ptaków'),
(699, 'Dom i ogród / Ozdoby / Domki dla ptaków i zwierząt / Budki dla ptaków'),
(700, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Architektura ogrodowa / Mostki ogrodowe'),
(701, 'Dom i ogród / Ozdoby / Flagi i rękawy powietrzne'),
(702, 'Dom i ogród / Ozdoby / Fontanny i stawy'),
(703, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Architektura ogrodowa / Pergole, kratki, altanki'),
(704, 'Dom i ogród / Ozdoby / Kamienie ogrodowe'),
(706, 'Dom i ogród / Ozdoby / Skrzynki pocztowe'),
(708, 'Dom i ogród / Ozdoby / Tablice pamiątkowe'),
(709, 'Dom i ogród / Ozdoby / Łańcuchy deszczowe'),
(710, 'Dom i ogród / Ozdoby / Deszczomierze'),
(711, 'Dom i ogród / Ozdoby / Zegary słoneczne'),
(712, 'Dom i ogród / Ozdoby / Wiatrowskazy i ozdoby dachowe'),
(714, 'Dom i ogród / Ozdoby / Dzwonki wietrzne'),
(716, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Architektura ogrodowa / Zadaszenia i altany'),
(717, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Hamaki'),
(718, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Huśtawki ogrodowe'),
(719, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Parasole i pawilony ogrodowe'),
(720, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Architektura ogrodowa / Składziki, garaże i wiaty'),
(721, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Doniczki i osłonki'),
(726, 'Artykuły dla dorosłych / Broń / Rozpylacze obronne'),
(727, 'Artykuły dla dorosłych / Broń / Paralizatory'),
(728, 'Dom i ogród / Artykuły gospodarstwa domowego / Dezynsekcja'),
(729, 'Dom i ogród / Baseny i spa'),
(730, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne'),
(732, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Maszynki do chleba'),
(733, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Otwieracze do puszek'),
(734, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do ekspresów do kawy i espresso / Młynki do kawy'),
(736, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Ekspresy do kawy i espresso'),
(737, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Parowary i garnki do gotowania na parze / Wolnowary'),
(738, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Frytownice'),
(739, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Parowary i garnki do gotowania na parze / Maszynki do jajek'),
(741, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Noże elektryczne'),
(743, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Suszarki do żywności'),
(744, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Rozdrabniacze i młynki do żywności'),
(747, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Kuchenki jednopalnikowe'),
(748, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Maszynki do lodów'),
(749, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Kruszarki do lodu'),
(750, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Wyciskarki soków'),
(751, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Czajniki elektryczne'),
(752, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Ostrzałki do noży'),
(753, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Mikrofalówki'),
(755, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Maszynki do makaronu'),
(756, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Maszynki do popcornu'),
(757, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Parowary i garnki do gotowania na parze / Maszynki do ryżu'),
(759, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Tostery i grille / Maszynki do kanapek'),
(760, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Parowary i garnki do gotowania na parze / Parowary'),
(761, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Tostery i grille / Opiekacze'),
(762, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Tostery i grille / Tostery'),
(763, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Pakowarki próżniowe'),
(764, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Tostery i grille / Waflownice'),
(765, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Filtry do wody'),
(766, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Maszynki do jogurtu'),
(772, 'Artykuły dla dorosłych'),
(773, 'Artykuły dla dorosłych / Artykuły erotyczne'),
(774, 'Artykuły dla dorosłych / Artykuły erotyczne / Odzież erotyczna'),
(775, 'Zdrowie i uroda / Opieka zdrowotna / Prezerwatywy'),
(776, 'Artykuły dla dorosłych / Artykuły erotyczne / Filmy erotyczne'),
(777, 'Zdrowie i uroda / Higiena osobista / Żele intymne'),
(778, 'Artykuły dla dorosłych / Artykuły erotyczne / Zabawki erotyczne'),
(779, 'Artykuły dla dorosłych / Artykuły erotyczne / Książki erotyczne'),
(780, 'Artykuły dla dorosłych / Broń'),
(781, 'Artykuły dla dorosłych / Broń / Konserwacja i akcesoria do broni palnej / Amunicja'),
(782, 'Artykuły dla dorosłych / Broń / Broń palna'),
(783, 'Media'),
(784, 'Media / Książki'),
(839, 'Media / Filmy'),
(855, 'Media / Nagrania muzyczne i dźwiękowe'),
(886, 'Media / Gazety i czasopisma'),
(887, 'Media / Nuty'),
(888, 'Pojazdy i części'),
(891, 'Pojazdy i części / Akcesoria i części do pojazdów / Wyposażenie elektroniczne pojazdów silnikowych / Wzmacniacze samochodowe'),
(894, 'Pojazdy i części / Akcesoria i części do pojazdów / Wyposażenie elektroniczne pojazdów silnikowych / Korektory i zwrotnice'),
(895, 'Pojazdy i części / Akcesoria i części do pojazdów / Wyposażenie elektroniczne pojazdów silnikowych / Głośniki samochodowe'),
(899, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych'),
(908, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Elementy układu wydechowego'),
(911, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Koła do pojazdów silnikowych / Opony'),
(912, 'Elektronika / Antyradary'),
(913, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów'),
(916, 'Pojazdy i części / Pojazdy / Pojazdy silnikowe / Samochody osobowe, ciężarówki i furgonetki'),
(919, 'Pojazdy i części / Pojazdy / Pojazdy silnikowe / Motocykle i skutery'),
(920, 'Pojazdy i części / Pojazdy / Pojazdy silnikowe / Pojazdy kempingowe'),
(922, 'Artykuły biurowe'),
(923, 'Artykuły biurowe / Sortowanie i utrzymanie porządku'),
(925, 'Artykuły biurowe / Sortowanie i utrzymanie porządku / Pudła na dokumenty'),
(926, 'Artykuły biurowe / Sortowanie i utrzymanie porządku / Organizery na płyty'),
(927, 'Artykuły biurowe / Sortowanie i utrzymanie porządku / Kalendarze, organizery i terminarze'),
(928, 'Artykuły biurowe / Sortowanie i utrzymanie porządku / Kasety na pieniądze'),
(930, 'Artykuły biurowe / Sortowanie i utrzymanie porządku / Skoroszyty i teczki zawieszane'),
(932, 'Artykuły biurowe / Artykuły różne'),
(934, 'Artykuły biurowe / Artykuły różne / Taśmy samoprzylepne'),
(935, 'Artykuły biurowe / Akcesoria biurowe / Podkładki do pisania'),
(936, 'Artykuły biurowe / Artykuły różne / Klipsy i spinacze do papieru'),
(938, 'Artykuły biurowe / Artykuły różne / Gumki do mazania'),
(939, 'Artykuły biurowe / Sortowanie i utrzymanie porządku / Organizery na biurko'),
(941, 'Artykuły biurowe / Akcesoria biurowe / Szkła powiększające'),
(943, 'Artykuły biurowe / Akcesoria biurowe / Temperówki'),
(944, 'Artykuły biurowe / Artykuły różne / Gumki recepturki'),
(947, 'Artykuły biurowe / Akcesoria biurowe / Zszywacze'),
(948, 'Artykuły biurowe / Artykuły różne / Zszywki'),
(949, 'Artykuły biurowe / Artykuły różne / Pinezki z łbem plastikowym'),
(950, 'Artykuły biurowe / Sprzęt biurowy'),
(952, 'Artykuły biurowe / Sprzęt biurowy / Drukarki do etykiet'),
(953, 'Artykuły biurowe / Sprzęt biurowy / Niszczarki'),
(954, 'Artykuły biurowe / Sprzęt biurowy / Transkrybery i systemy dyktowania'),
(955, 'Artykuły biurowe / Sprzęt biurowy / Maszyny do pisania'),
(956, 'Artykuły biurowe / Artykuły różne / Artykuły papiernicze'),
(957, 'Artykuły biurowe / Artykuły różne / Artykuły papiernicze / Wizytówki'),
(958, 'Artykuły biurowe / Artykuły różne / Artykuły papiernicze / Koperty'),
(959, 'Artykuły biurowe / Artykuły różne / Artykuły papiernicze / Karty katalogowe'),
(960, 'Artykuły biurowe / Artykuły różne / Etykiety i przekładki indeksujące'),
(961, 'Artykuły biurowe / Artykuły różne / Artykuły papiernicze / Bruliony i notesy'),
(962, 'Artykuły biurowe / Artykuły różne / Artykuły papiernicze / Papier ksero'),
(963, 'Artykuły biurowe / Sprzęt do prezentacji / Foliogramy'),
(964, 'Artykuły biurowe / Sprzęt do prezentacji'),
(965, 'Artykuły biurowe / Sprzęt do prezentacji / Tablice kredowe'),
(966, 'Artykuły biurowe / Sprzęt do prezentacji / Tablice do prezentacji'),
(967, 'Artykuły biurowe / Sprzęt do prezentacji / Flipcharty'),
(968, 'Artykuły biurowe / Sprzęt do prezentacji / Sztalugi'),
(969, 'Artykuły biurowe / Sprzęt do prezentacji / Wskaźniki laserowe'),
(970, 'Artykuły biurowe / Sprzęt do prezentacji / Pulpity'),
(971, 'Artykuły biurowe / Sprzęt do prezentacji / Tablice suchościeralne'),
(973, 'Artykuły biurowe / Akcesoria pakunkowe / Pudełka wysyłkowe'),
(974, 'Artykuły biurowe / Akcesoria pakunkowe / Materiały pakunkowe'),
(975, 'Artykuły biurowe / Akcesoria pakunkowe / Taśmy pakunkowe'),
(976, 'Biznes i przemysł / Informacja wizualna'),
(977, 'Artykuły biurowe / Akcesoria biurowe / Przybory piśmiennicze i rysunkowe'),
(978, 'Artykuły biurowe / Akcesoria biurowe / Przybory piśmiennicze i rysunkowe / Kreda'),
(979, 'Artykuły biurowe / Akcesoria biurowe / Przybory piśmiennicze i rysunkowe / Kredki'),
(980, 'Artykuły biurowe / Akcesoria biurowe / Przybory piśmiennicze i rysunkowe / Markery i zakreślacze'),
(981, 'Artykuły biurowe / Akcesoria biurowe / Przybory piśmiennicze i rysunkowe / Pióra, długopisy i ołówki / Ołówki / Ołówki do pisania'),
(982, 'Artykuły biurowe / Akcesoria biurowe / Przybory piśmiennicze i rysunkowe / Pióra, długopisy i ołówki / Pióra i długopisy'),
(984, 'Dom i ogród / Rośliny / Kwiaty'),
(985, 'Dom i ogród / Rośliny'),
(988, 'Sprzęt sportowy'),
(989, 'Sprzęt sportowy / Sport / Taniec'),
(990, 'Sprzęt sportowy / Fitness'),
(992, 'Sprzęt sportowy / Fitness / Trening kardio / Sprzęt kardio / Orbitreki'),
(993, 'Sprzęt sportowy / Fitness / Piłki do ćwiczeń'),
(994, 'Sprzęt sportowy / Fitness / Trening kardio / Sprzęt kardio / Rowery treningowe'),
(995, 'Sprzęt sportowy / Fitness / Trening kardio / Sprzęt kardio / Przyrządy do wiosłowania'),
(996, 'Sprzęt sportowy / Fitness / Trening kardio / Sprzęt kardio / Steppery'),
(997, 'Sprzęt sportowy / Fitness / Trening kardio / Sprzęt kardio / Bieżnie'),
(999, 'Sprzęt sportowy / Fitness / Joga i pilates'),
(1000, 'Sprzęt sportowy / Sport / Gimnastyka');
INSERT INTO `google_kategorie` (`id`, `nazwa`) VALUES
(1001, 'Sprzęt sportowy / Gry towarzyskie'),
(1002, 'Sprzęt sportowy / Gry towarzyskie / Air hockey'),
(1003, 'Sprzęt sportowy / Gry towarzyskie / Bilard'),
(1004, 'Sprzęt sportowy / Gry towarzyskie / Gra w kręgle'),
(1005, 'Sprzęt sportowy / Gry towarzyskie / Gra w rzutki'),
(1006, 'Sprzęt sportowy / Sport / Szermierka'),
(1007, 'Sprzęt sportowy / Gry towarzyskie / Piłkarzyki'),
(1008, 'Sprzęt sportowy / Gry towarzyskie / Tenis stołowy'),
(1009, 'Sprzęt sportowy / Gry towarzyskie / Shuffleboard stołowy'),
(1011, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu'),
(1013, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach'),
(1014, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Meble kempingowe'),
(1015, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Kuchenki przenośne'),
(1016, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Turystyczne akcesoria kuchenne'),
(1017, 'Dom i ogród / Kuchnia i jadalnia / Transportery do żywności i napojów / Pojemniki termoizolacyjne'),
(1019, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Oświetlenie kempingowe'),
(1020, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Śpiwory'),
(1021, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Maty kempingowe'),
(1022, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Namioty'),
(1023, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Przenośne filtry i oczyszczacze do wody'),
(1025, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze'),
(1026, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Rowery'),
(1027, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Zapięcia rowerowe'),
(1028, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Bagażniki rowerowe'),
(1029, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Odzież i akcesoria dla rowerzystów / Kaski rowerowe'),
(1030, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Rowery jednokołowe'),
(1031, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo'),
(1033, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Łucznictwo'),
(1034, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Myślistwo / Osłony myśliwskie'),
(1037, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Żyłki i przypony'),
(1041, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Torby i pojemniki na przynętę'),
(1043, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Golf'),
(1044, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Golf / Torby golfowe'),
(1045, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Golf / Piłki golfowe'),
(1046, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Golf / Kije golfowe'),
(1047, 'Sprzęt sportowy / Sport / Piłka ręczna'),
(1049, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Paintball i ASG / Paintball'),
(1051, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Sportowe artykuły kolekcjonerskie / Akcesoria dla fanów sportu / Akcesoria dla fanów wyścigów samochodowych'),
(1057, 'Sprzęt sportowy / Sport / Łyżwiarstwo figurowe i hokej / Łyżwy'),
(1058, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na wrotkach i łyżworolkach / Łyżworolki'),
(1059, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na deskorolce / Deskorolki'),
(1060, 'Sprzęt sportowy / Sport / Lekkoatletyka'),
(1062, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Gry na świeżym powietrzu / Badminton'),
(1065, 'Sprzęt sportowy / Sport / Tenis'),
(1068, 'Sprzęt sportowy / Sport / Zapasy'),
(1070, 'Sprzęt sportowy / Sport / Baseball i softball'),
(1074, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Sportowe artykuły kolekcjonerskie / Akcesoria dla fanów sportu / Akcesoria dla fanów baseballu i softballu'),
(1076, 'Sprzęt sportowy / Sport / Baseball i softball / Rękawice do baseballa i softballa'),
(1077, 'Sprzęt sportowy / Sport / Baseball i softball / Maszyny do wyrzucania piłek'),
(1078, 'Sprzęt sportowy / Sport / Baseball i softball / Odzież ochronna do baseballa i softballa'),
(1081, 'Sprzęt sportowy / Sport / Koszykówka'),
(1082, 'Sprzęt sportowy / Sport / Koszykówka / Kosze do gry w koszykówkę'),
(1083, 'Sprzęt sportowy / Sport / Koszykówka / Piłki do koszykówki'),
(1084, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Sportowe artykuły kolekcjonerskie / Akcesoria dla fanów sportu / Akcesoria dla fanów koszykówki'),
(1087, 'Sprzęt sportowy / Sport / Krykiet'),
(1089, 'Sprzęt sportowy / Sport / Hokej na trawie i lacrosse / Piłki do hokeja na trawie'),
(1092, 'Sprzęt sportowy / Sport / Hokej na trawie i lacrosse / Kije do hokeja na trawie'),
(1093, 'Sprzęt sportowy / Sport / Futbol amerykański'),
(1094, 'Sprzęt sportowy / Sport / Futbol amerykański / Piłki do futbolu amerykańskiego'),
(1095, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Sportowe artykuły kolekcjonerskie / Akcesoria dla fanów sportu / Akcesoria dla fanów futbolu amerykańskiego'),
(1097, 'Sprzęt sportowy / Sport / Futbol amerykański / Ochraniacze do futbolu amerykańskiego'),
(1098, 'Sprzęt sportowy / Sport / Futbol amerykański / Ochraniacze do futbolu amerykańskiego / Kaski do futbolu amerykańskiego'),
(1105, 'Sprzęt sportowy / Sport / Łyżwiarstwo figurowe i hokej / Ochraniacze hokejowe'),
(1110, 'Sprzęt sportowy / Sport / Rugby'),
(1111, 'Sprzęt sportowy / Sport / Piłka nożna'),
(1112, 'Sprzęt sportowy / Sport / Piłka nożna / Piłki do gry w piłkę nożną'),
(1113, 'Sprzęt sportowy / Sport / Piłka nożna / Bramki do piłki nożnej'),
(1114, 'Sprzęt sportowy / Sport / Piłka nożna / Odzież ochronna do piłki nożnej / Piłkarskie ochraniacze goleni'),
(1115, 'Sprzęt sportowy / Sport / Siatkówka'),
(1116, 'Sprzęt sportowy / Sport / Siatkówka / Piłki siatkowe'),
(1117, 'Sprzęt sportowy / Sport / Siatkówka / Siatki do siatkówki'),
(1120, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Pływanie łodzią i rafting'),
(1122, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Konserwacja i pielęgnacja jednostek pływających'),
(1124, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Pływanie łodzią i rafting / Kanadyjki'),
(1125, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Silniki do jednostek pływających'),
(1127, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Pływanie łodzią i rafting / Kajaki'),
(1128, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Odzież żeglarska i do uprawiania sportów wodnych / Kamizelki ratunkowe'),
(1129, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Pływanie łodzią i rafting / Wiosła'),
(1130, 'Pojazdy i części / Pojazdy / Jednostki pływające / Skutery wodne'),
(1132, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Części do łodzi żaglowych'),
(1133, 'Pojazdy i części / Akcesoria i części do pojazdów / Przewóz i przechowywanie bagażu / Przyczepy / Przyczepy na łodzie'),
(1135, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Nurkowanie i snorkeling'),
(1136, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Nurkowanie i snorkeling / Kamizelki ratowniczo-wypornościowe'),
(1137, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Nurkowanie i snorkeling / Komputery nurkowe'),
(1138, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Odzież żeglarska i do uprawiania sportów wodnych / Suche skafandry'),
(1139, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Nurkowanie i snorkeling / Płetwy'),
(1140, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Nurkowanie i snorkeling / Maski do nurkowania'),
(1141, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Nurkowanie i snorkeling / Automaty oddechowe'),
(1142, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Nurkowanie i snorkeling / Fajki do nurkowania'),
(1143, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Surfing'),
(1144, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Pływanie'),
(1145, 'Sprzęt sportowy / Sport / Piłka wodna'),
(1146, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Sporty wodne holowane / Jazda na nartach wodnych'),
(1147, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Odzież żeglarska i do uprawiania sportów wodnych / Stroje piankowe'),
(1148, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Windsurfing'),
(1157, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Jazda na nartach i snowboardzie / Kijki narciarskie'),
(1158, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Jazda na nartach i snowboardzie / Narty / Narty zjazdowe'),
(1161, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Jazda na nartach i snowboardzie / Kaski narciarskie i snowboardowe'),
(1162, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Jazda na nartach i snowboardzie / Wiązania snowboardowe'),
(1163, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Jazda na nartach i snowboardzie / Buty snowboardowe'),
(1164, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Jazda na nartach i snowboardzie / Deski snowboardowe'),
(1166, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Chodzenie z rakietami śnieżnymi'),
(1167, 'Sprzęt / Narzędzia'),
(1169, 'Sprzęt / Narzędzia / Kowadła'),
(1171, 'Sprzęt / Narzędzia / Siekiery'),
(1174, 'Sprzęt / Narzędzia / Narzędzia snycerskie'),
(1179, 'Sprzęt / Narzędzia / Łomy stalowe'),
(1180, 'Sprzęt / Narzędzia / Przecinaki'),
(1181, 'Sprzęt / Narzędzia / Przecinaki / Szczypce tnące przegubowe'),
(1182, 'Sprzęt / Narzędzia / Przecinaki / Przecinaki do szkła'),
(1184, 'Sprzęt / Narzędzia / Gwintowniki i narzynki'),
(1185, 'Sprzęt / Narzędzia / Uchwyty'),
(1186, 'Sprzęt / Narzędzia / Wbijanie'),
(1187, 'Sprzęt / Narzędzia / Heble'),
(1188, 'Sprzęt / Narzędzia / Piaszczarki'),
(1191, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Poziomice'),
(1193, 'Sprzęt / Narzędzia / Narzędzia murarskie'),
(1194, 'Sprzęt / Narzędzia / Wyciągacze gwoździ'),
(1195, 'Sprzęt / Narzędzia / Śrubokręty nasadkowe'),
(1196, 'Sprzęt / Narzędzia / Narzędzia do chwytania'),
(1198, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Kątomierze'),
(1201, 'Sprzęt / Narzędzia / Koziołki do cięcia drewna'),
(1202, 'Sprzęt / Narzędzia / Szpachle i skrobaki'),
(1203, 'Sprzęt / Narzędzia / Śrubokręty'),
(1205, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Kątowniki'),
(1206, 'Sprzęt / Narzędzia / Gwoździarki i zszywacze'),
(1207, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Wykrywacze profili'),
(1215, 'Sprzęt / Narzędzia / Narzędzia do uszczelniania'),
(1216, 'Sprzęt / Narzędzia / Wiertarki / Wiertarki pionowe'),
(1217, 'Sprzęt / Narzędzia / Wiertarki'),
(1218, 'Sprzęt / Materiały elektryczne / Agregaty prądotwórcze'),
(1219, 'Sprzęt / Narzędzia / Młynki'),
(1220, 'Sprzęt / Narzędzia / Opalarki'),
(1221, 'Sprzęt / Narzędzia / Wkrętarki udarowe'),
(1223, 'Dom i ogród / Trawnik i ogród / Silnikowe narzędzia ogrodowe / Przycinarki do chwastów'),
(1225, 'Sprzęt / Narzędzia / Polerki'),
(1226, 'Dom i ogród / Trawnik i ogród / Silnikowe narzędzia ogrodowe / Myjki ciśnieniowe'),
(1235, 'Sprzęt / Narzędzia / Piły'),
(1236, 'Sprzęt / Narzędzia / Lutownice'),
(1238, 'Sprzęt / Narzędzia / Narzędzia do spawania'),
(1239, 'Gry i zabawki'),
(1241, 'Dzieci i niemowlęta / Zabawki dla niemowląt / Chodziki i chodziki z zabawkami dla niemowląt'),
(1242, 'Dzieci i niemowlęta / Zabawki dla niemowląt / Karuzele do łóżeczka'),
(1243, 'Dzieci i niemowlęta / Zabawki dla niemowląt / Maty do zabawy'),
(1244, 'Dzieci i niemowlęta / Zabawki dla niemowląt / Grzechotki'),
(1246, 'Gry i zabawki / Gry / Gry planszowe'),
(1247, 'Gry i zabawki / Gry / Gry karciane'),
(1249, 'Gry i zabawki / Zabawki ogrodowe'),
(1251, 'Gry i zabawki / Zabawki ogrodowe / Domki do zabawy'),
(1253, 'Gry i zabawki / Zabawki'),
(1254, 'Gry i zabawki / Zabawki / Zabawki do budowania'),
(1255, 'Gry i zabawki / Zabawki / Lalki, zestawy i figurki'),
(1257, 'Gry i zabawki / Zabawki / Lalki, zestawy i figurki / Lalki'),
(1258, 'Gry i zabawki / Zabawki / Lalki, zestawy i figurki / Pacynki i marionetki'),
(1259, 'Gry i zabawki / Zabawki / Lalki, zestawy i figurki / Pluszaki'),
(1261, 'Gry i zabawki / Zabawki / Zabawki latające'),
(1262, 'Gry i zabawki / Zabawki / Zabawki edukacyjne'),
(1264, 'Gry i zabawki / Zabawki / Zabawki muzyczne'),
(1266, 'Gry i zabawki / Zabawki / Zabawki sportowe'),
(1267, 'Pojazdy i części / Pojazdy / Pojazdy silnikowe'),
(1268, 'Gry i zabawki / Zabawki / Zabawki na plażę i do piasku'),
(1270, 'Elektronika / Akcesoria do gier wideo'),
(1279, 'Oprogramowanie / Gry wideo'),
(1294, 'Elektronika / Konsole do gier wideo'),
(1300, 'Sprzęt / Narzędzia / Narzędzia malarskie / Pędzle do malowania'),
(1301, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Pamięci komputerowe / Napędy dyskietek'),
(1302, 'Dom i ogród / Trawnik i ogród / Nawadnianie / Akcesoria do zraszaczy / Sterowniki zraszaczy'),
(1305, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki'),
(1306, 'Dom i ogród / Bezpieczeństwo powodziowe, ogniowe i gazowe / Wykrywacze wycieków'),
(1312, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Pieczęcie'),
(1318, 'Sprzęt / Akcesoria do sprzętu / Cewki'),
(1319, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Części do odpływów / Syfony'),
(1334, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do mikrofalówek'),
(1337, 'Elektronika / Elementy / Modulatory'),
(1340, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Broń kolekcjonerska / Stojaki i gabloty na miecze'),
(1348, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Zasilacze UPS'),
(1350, 'Elektronika / Sieci / Mostki i routery'),
(1352, 'Sprzęt / Ogrodzenia i barierki / Sztachety'),
(1354, 'Sprzęt / Hydraulika / Dystrybucja i filtrowanie wody / Dystrybutory wody / Chłodziarki do wody'),
(1356, 'Sprzęt / Materiały budowlane / Wyposażenie do drzwi / Gałki i klamki do drzwi'),
(1360, 'Zdrowie i uroda / Higiena osobista / Higiena jamy ustnej / Pasty do zębów'),
(1361, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Materiały malarskie / Farby'),
(1367, 'Sprzęt / Narzędzia / Wiertarki / Przenośniki śrubowe'),
(1368, 'Elektronika / Wideo / Sprzęt do edycji i produkcji filmów'),
(1371, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Zaproszenia'),
(1374, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Stacje pogodowe'),
(1375, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Akcesoria do zasilaczy UPS'),
(1380, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja oczu'),
(1387, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Musztardy'),
(1388, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Ekspresy do kawy i espresso / Przelewowe ekspresy do kawy'),
(1390, 'Sprzęt / Hydraulika / Dystrybucja i filtrowanie wody / Destylatory wody'),
(1391, 'Sprzęt / Narzędzia / Gratowniki'),
(1395, 'Meble / Stoły / Stoliki / Stoliki kawowe'),
(1407, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Części do odpływów / Pręty do udrażniania rur i odpływów'),
(1413, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Linijki bez podziałki'),
(1420, 'Elektronika / Audio / Akcesoria audio'),
(1425, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Akcesoria do toalet i bidetów / Spłuczki toaletowe i korki do spłuczek'),
(1434, 'Dom i ogród / Bezpieczeństwo powodziowe, ogniowe i gazowe / Gaśnice'),
(1436, 'Dom i ogród / Oświetlenie / Oświetlenie awaryjne'),
(1439, 'Sprzęt / Narzędzia / Klucze narzędziowe'),
(1442, 'Zdrowie i uroda / Higiena osobista / Masaż i relaks / Fotele do masażu'),
(1444, 'Dom i ogród / Kuchnia i jadalnia / Transportery do żywności i napojów / Piersiówki'),
(1445, 'Żywność, napoje i tytoń / Żywność / Przekąski / Krakersy'),
(1450, 'Sprzęt / Narzędzia / Narzędzia murarskie / Strugi'),
(1454, 'Sprzęt / Narzędzia / Przecinaki / Ręczne przecinaki do metalu'),
(1458, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Płyty do mocowania'),
(1459, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Kółka miernicze'),
(1463, 'Meble / Krzesła i fotele / Stołki barowe'),
(1465, 'Sprzęt / Narzędzia / Wiertarki / Dłutownice'),
(1469, 'Sprzęt / Narzędzia / Przewodowe narzędzia ręczne'),
(1470, 'Biznes i przemysł / Produkcja przemysłowa'),
(1475, 'Biznes i przemysł / Hotelarstwo'),
(1479, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Chemikalia / Środki do czyszczenia kominów'),
(1480, 'Elektronika / Akcesoria elektroniczne / Kable / Kable sieciowe'),
(1483, 'Ubrania i akcesoria / Akcesoria do ubrań / Akcesoria do włosów / Gumki do włosów'),
(1484, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Pieczęcie na koperty'),
(1487, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Karty i adaptery I/O / Karty i adaptery telewizyjne'),
(1489, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Akcesoria do baterii'),
(1491, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Rząd koński / Popręgi'),
(1492, 'Sprzęt / Akcesoria do sprzętu / Łańcuchy, druty i sznury / Łańcuchy'),
(1496, 'Dom i ogród / Kuchnia i jadalnia / Przechowywanie żywności / Zawijanie żywności / Folia'),
(1505, 'Elektronika / Akcesoria do gier wideo / Akcesoria do domowych konsol do gier'),
(1508, 'Sprzęt / Akcesoria do sprzętu / Akcesoria mocujące do sprzętu / Kołki rozporowe'),
(1513, 'Artykuły biurowe / Artykuły różne / Artykuły papiernicze / Papier pakunkowy'),
(1514, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Części do odpływów / Kratki odpływowe'),
(1516, 'Ubrania i akcesoria / Ubrania / Garnitury / Garnitury ze spódnicami'),
(1519, 'Sprzęt / Wentylacja / Sterowniki HVAC'),
(1529, 'Żywność, napoje i tytoń / Żywność / Przyprawy i wzmacniacze smaku / Zioła i przyprawy'),
(1530, 'Dom i ogród / Akcesoria do kominków i pieców opalanych węglem / Narzędzia do kominków'),
(1533, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Średnicomierze do drewna'),
(1534, 'Żywność, napoje i tytoń / Żywność / Przekąski / Popcorn'),
(1539, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Wykrywacze prętów zbrojeniowych'),
(1540, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do wiercenia / Wiertła'),
(1541, 'Dom i ogród / Trawnik i ogród / Silnikowe narzędzia ogrodowe / Odśnieżarki'),
(1544, 'Elektronika / Elementy / Rozgałęźniki'),
(1546, 'Dom i ogród / Oświetlenie / Reflektory'),
(1548, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Oświetlenie i studio / Światłomierze'),
(1549, 'Meble / Stoły / Stoliki / Stoliki niskie'),
(1550, 'Elektronika / Elektronika morska / Sonary do lokalizacji ryb'),
(1552, 'Elektronika / Elektronika morska / Radary morskie'),
(1555, 'Sprzęt / Narzędzia / Narzędzia murarskie / Linie pomocnicze'),
(1556, 'Biznes i przemysł / Organy ścigania'),
(1557, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Ekspresy do kawy i espresso / Zaparzacze do kawy'),
(1562, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Urządzenia wejściowe / Przełączniki KVM'),
(1563, 'Sprzęt / Narzędzia / Przepychacze'),
(1564, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Akumulatory / Akumulatory do laptopów'),
(1568, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Majonezy'),
(1573, 'Żywność, napoje i tytoń / Żywność / Wypieki / Bajgle'),
(1578, 'Ubrania i akcesoria / Ubrania / Bielizna i skarpety / Bielizna modelująca'),
(1580, 'Ubrania i akcesoria / Ubrania / Garnitury / Smokingi'),
(1581, 'Ubrania i akcesoria / Ubrania / Spódnice'),
(1584, 'Sprzęt / Narzędzia / Maszyny do nacinania gwintu'),
(1594, 'Ubrania i akcesoria / Ubrania / Garnitury'),
(1599, 'Dom i ogród / Bielizna stołowa i pościelowa / Artykuły pościelowe / Maty do leżenia'),
(1602, 'Meble / Stoły / Stoliki / Ławy'),
(1603, 'Sprzęt / Narzędzia / Zmieniacze żarówek'),
(1604, 'Ubrania i akcesoria / Ubrania'),
(1611, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Oświetlenie i studio / Tła'),
(1622, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Ciemnia / Chemikalia fotograficzne'),
(1623, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Pamięci komputerowe / Akcesoria do dysków twardych'),
(1624, 'Biznes i przemysł / Badania naukowe i laboratoryjne'),
(1625, 'Artykuły biurowe / Sprzęt biurowy / Laminatory'),
(1626, 'Dom i ogród / Sprzęt AGD / Ogrzewanie, wentylacja i klimatyzacja'),
(1627, 'Artykuły biurowe / Sprzęt do prezentacji / Tablice do prezentacji / Tablice montażowe'),
(1632, 'Sprzęt / Narzędzia / Klucze'),
(1634, 'Elektronika / Wideo / Multipleksery wideo'),
(1636, 'Sprzęt / Hydraulika / Armatury / Wanny'),
(1639, 'Dom i ogród / Bezpieczeństwo powodziowe, ogniowe i gazowe / Szafki i stelaże na gaśnice'),
(1640, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Mierniki i sterowniki przepływu'),
(1644, 'Sprzęt / Narzędzia / Ostrzałki'),
(1645, 'Biznes i przemysł / Branża medyczna / Fartuchy chirurgiczne '),
(1647, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Ekspresy do kawy i espresso / Kawiarki'),
(1648, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Materiały malarskie / Bejce'),
(1653, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do mieszania kolorów / Noże malarskie'),
(1659, 'Dom i ogród / Artykuły gospodarstwa domowego / Środki i narzędzia do pielęgnacji obuwia / Pasty i woski do butów'),
(1662, 'Ubrania i akcesoria / Akcesoria do ubrań / Akcesoria do włosów / Opaski do włosów'),
(1663, 'Sprzęt / Narzędzia / Tokarki'),
(1665, 'Elektronika / Akcesoria elektroniczne / Nośniki pamięci / Pamięć podręczna'),
(1667, 'Sprzęt / Narzędzia / Zginacze do rur i prętów'),
(1668, 'Sprzęt / Narzędzia / Narzędzia murarskie / Narzędzia do układania cegieł'),
(1671, 'Żywność, napoje i tytoń / Napoje / Napoje alkoholowe / Likiery i trunki / Gin'),
(1673, 'Sprzęt / Hydraulika / Armatury'),
(1675, 'Ubrania i akcesoria / Ubrania / Bielizna i skarpety / Dodatki do bielizny / Podwiązki'),
(1679, 'Dom i ogród / Bezpieczeństwo powodziowe, ogniowe i gazowe'),
(1680, 'Zdrowie i uroda / Opieka zdrowotna / Testy medyczne / Testy ciążowe'),
(1683, 'Elektronika / Drukowanie, kopiowanie, skanowanie i faksowanie / Akcesoria do kopiarek, drukarek i faksów / Pamięci do drukarek'),
(1684, 'Dom i ogród / Rośliny / Drzewa'),
(1687, 'Sprzęt / Hydraulika / Armatury / Zlewy i umywalki'),
(1694, 'Sprzęt / Hydraulika / Mocowania hydrauliczne / Kołnierze hydrauliczne'),
(1695, 'Aparaty, kamery i przyrządy optyczne / Przyrządy optyczne / Lunety teleskopowe / Celowniki optyczne'),
(1698, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Wagi'),
(1699, 'Sprzęt / Narzędzia / Narzędzia malarskie / Gąbki malarskie'),
(1700, 'Dom i ogród / Sprzęt AGD / Ogrzewanie, wentylacja i klimatyzacja / Wentylatory / Wentylatory sufitowe'),
(1702, 'Żywność, napoje i tytoń / Żywność / Dipy i smarowidła / Salsa'),
(1708, 'Artykuły biurowe / Sprzęt biurowy / Frankownice'),
(1709, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do podgrzewaczy wody / Przewody wentylacyjne do podgrzewaczy wody'),
(1718, 'Elektronika / Akcesoria elektroniczne / Anteny'),
(1719, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do mieszania kolorów / Palety'),
(1722, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Akumulatory / Akumulatory do aparatów'),
(1723, 'Sprzęt / Hydraulika / Akcesoria do studni'),
(1732, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Przyrządy pomiarowe'),
(1733, 'Elektronika / Akcesoria elektroniczne / Nośniki pamięci / RAM'),
(1735, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Chemikalia / Neutralizatory kwasów'),
(1738, 'Gry i zabawki / Zabawki ogrodowe / Trampoliny'),
(1739, 'Sprzęt / Akcesoria do sprzętu / Akcesoria mocujące do sprzętu / Nakrętki i śruby'),
(1744, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do podgrzewaczy wody / Elementy podgrzewaczy wody'),
(1745, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Akumulatory / Baterie do telefonów komórkowych'),
(1746, 'Sprzęt / Hydraulika / Armatury / Toalety i bidety / Pisuary'),
(1749, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Chemikalia / Środki do czyszczenia rur'),
(1753, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Smary'),
(1754, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Rząd koński / Lejce'),
(1755, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Suszone owoce'),
(1763, 'Elektronika / Akcesoria elektroniczne / Kable / Kable systemowe i zasilające'),
(1767, 'Elektronika / Akcesoria elektroniczne / Nośniki pamięci / Pamięć wideo'),
(1771, 'Sprzęt / Akcesoria do sprzętu / Zawiasy'),
(1772, 'Ubrania i akcesoria / Ubrania / Bielizna i skarpety / Bielizna damska'),
(1774, 'Sprzęt / Narzędzia / Narzędzia malarskie / Wałki malarskie'),
(1779, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Części do prysznica / Drzwi i kabiny prysznicowe'),
(1783, 'Artykuły dla dorosłych / Broń / Konserwacja i akcesoria do broni palnej / Kabury'),
(1785, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Sondy i lokalizatory'),
(1786, 'Ubrania i akcesoria / Akcesoria do ubrań / Kominiarki'),
(1787, 'Sprzęt / Narzędzia / Urządzenia do naprawy kranów'),
(1788, 'Sprzęt / Ogrodzenia i barierki / Bramy'),
(1794, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Piaski i gleby'),
(1795, 'Biznes i przemysł / Ciężki sprzęt'),
(1799, 'Sprzęt / Narzędzia / Narzędzia murarskie / Obrzynarki'),
(1801, 'Elektronika / Elementy'),
(1802, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Alarmy i zamki samochodowe / Alarmy samochodowe'),
(1803, 'Artykuły biurowe / Obróbka papieru / Przyciski do papieru'),
(1806, 'Artykuły dla dorosłych / Broń / Konserwacja i akcesoria do broni palnej / Rękojeści broni palnej'),
(1807, 'Ubrania i akcesoria / Ubrania / Bielizna i skarpety / Kalesony'),
(1809, 'Sprzęt / Narzędzia / Drabiny i rusztowania / Platformy robocze'),
(1810, 'Sprzęt / Hydraulika / Mocowania hydrauliczne'),
(1813, 'Biznes i przemysł / Finanse i ubezpieczenia'),
(1816, 'Sprzęt / Akcesoria do sprzętu / Filtry i ekrany'),
(1817, 'Dom i ogród / Kuchnia i jadalnia / Naczynia barowe / Nalewaki do piwa'),
(1819, 'Sprzęt / Narzędzia / Rozwiertaki'),
(1821, 'Sprzęt / Hydraulika / Dystrybucja i filtrowanie wody / Dystrybutory wody / Fontanny z wodą pitną'),
(1822, 'Artykuły dla dorosłych / Broń / Konserwacja i akcesoria do broni palnej / Szyny montażowe'),
(1824, 'Sprzęt / Narzędzia / Przecinaki / Przecinaki do prętów zbrojeniowych'),
(1827, 'Biznes i przemysł / Leśnictwo i pozyskiwanie drewna'),
(1831, 'Ubrania i akcesoria / Ubrania / Odzież wierzchnia / Kamizelki'),
(1832, 'Sprzęt / Narzędzia / Narzędzia znakujące'),
(1835, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do podgrzewaczy wody / Podstawki do podgrzewaczy wody'),
(1836, 'Artykuły biurowe / Obróbka papieru / Utrząsarki do papieru'),
(1837, 'Biznes i przemysł / Handel detaliczny / Reklamówki papierowe i plastikowe'),
(1841, 'Sprzęt / Narzędzia / Narzędzia do frezowania pionowego'),
(1850, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Rozdzielacze'),
(1855, 'Żywność, napoje i tytoń / Żywność / Nabiał / Serki ziarniste'),
(1856, 'Ubrania i akcesoria / Akcesoria do butów / Sznurowadła'),
(1862, 'Sprzęt / Narzędzia / Szczotki do rur'),
(1863, 'Gry i zabawki / Zabawki ogrodowe / Drążki pogo'),
(1865, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Akcesoria do toalet i bidetów / Deski sedesowe i bidetowe'),
(1866, 'Sprzęt / Narzędzia / Drabiny i rusztowania / Rusztowania'),
(1867, 'Elektronika / Akcesoria elektroniczne / Kable / Kable audio-wideo'),
(1868, 'Żywność, napoje i tytoń / Napoje / Kawy'),
(1869, 'Sprzęt / Materiały elektryczne / Gniazdka ścienne'),
(1870, 'Sprzęt / Zamki i klucze / Systemy kart wejściowych'),
(1871, 'Dom i ogród / Bezpieczeństwo powodziowe, ogniowe i gazowe / Alarmy przeciwpożarowe'),
(1874, 'Dom i ogród / Artykuły gospodarstwa domowego / Środki i narzędzia do pielęgnacji obuwia / Szczotki do butów'),
(1876, 'Żywność, napoje i tytoń / Żywność / Wypieki'),
(1880, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Akumulatory / Akumulatory do telefonów bezprzewodowych'),
(1887, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Kartki z odpowiedzią'),
(1893, 'Ubrania i akcesoria / Akcesoria do ubrań / Opaski na rękę'),
(1895, 'Żywność, napoje i tytoń / Żywność / Wypieki / Muffiny'),
(1897, 'Sprzęt / Wentylacja / Sterowniki HVAC / Termostaty'),
(1898, 'Biznes i przemysł / Branża medyczna / Fartuchy lekarskie'),
(1901, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja włosów / Kosmetyki do stylizacji włosów'),
(1906, 'Biznes i przemysł / Organy ścigania / Kajdanki'),
(1910, 'Sprzęt / Zbiorniki'),
(1919, 'Sprzęt / Ogrodzenia i barierki / Słupy ogrodzeniowe'),
(1921, 'Sprzęt / Hydraulika / Armatury / Toalety i bidety / Toalety'),
(1922, 'Ubrania i akcesoria / Akcesoria do ubrań / Nakrycia głowy / Przybranie głowy'),
(1923, 'Sprzęt / Narzędzia / Strugarki poprzeczne'),
(1924, 'Elektronika / Telekomunikacja / Telefonia / Telefony satelitarne'),
(1926, 'Żywność, napoje i tytoń / Napoje / Napoje alkoholowe / Likiery i trunki / Whiskey'),
(1927, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Ozdoby / Metki'),
(1928, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Urządzenia wejściowe'),
(1932, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Części do odpływów / Środki do udrażniania rur i odpływów'),
(1933, 'Ubrania i akcesoria / Akcesoria do butów'),
(1934, 'Dom i ogród / Bezpieczeństwo powodziowe, ogniowe i gazowe / Tryskacze'),
(1935, 'Sprzęt / Materiały elektryczne / Przełączniki elektryczne / Włączniki światła'),
(1948, 'Ubrania i akcesoria / Akcesoria do ubrań / Akcesoria do włosów / Szpilki do włosów i kokówki'),
(1949, 'Artykuły religijne i dewocjonalia / Artykuły religijne / Nakrycia głowy i zasłony twarzy'),
(1952, 'Sprzęt / Hydraulika / Dystrybucja i filtrowanie wody / Zmiękczacze wody'),
(1954, 'Żywność, napoje i tytoń / Żywność / Nabiał / Jogurty'),
(1958, 'Sprzęt / Narzędzia / Kombinerki'),
(1962, 'Dom i ogród / Akcesoria łazienkowe / Drążki prysznicowe'),
(1963, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Akcesoria do umywalek'),
(1967, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Narzędzia ogrodnicze / Spryskiwacze ogrodowe'),
(1969, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Dressingi do sałatek'),
(1974, 'Sprzęt / Zamki i klucze'),
(1977, 'Elektronika / Elementy / Konektory'),
(1979, 'Sprzęt / Akcesoria do sprzętu / Materiały ochronne'),
(1985, 'Dom i ogród / Bielizna stołowa i pościelowa / Artykuły pościelowe / Koce'),
(1991, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Wykrywacze gazu'),
(1993, 'Elektronika / Akcesoria elektroniczne / Akcesoria komputerowe / Podkładki pod myszki'),
(1994, 'Sprzęt / Narzędzia / Wiertarki / Wiertarki pneumatyczne'),
(1995, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Luty i topniki'),
(1996, 'Artykuły biurowe / Wózki biurowe / Szafki na sprzęt RTV'),
(2002, 'Meble / Krzesła i fotele / Fotele bujane'),
(2006, 'Sprzęt / Materiały elektryczne / Skrzynki montażowe i uchwyty'),
(2007, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Oświetlenie i studio / Stojaki i uchwyty studyjne'),
(2008, 'Sprzęt / Materiały budowlane / Pokrycia dachowe / Kołnierze dachowe'),
(2014, 'Artykuły biurowe / Obróbka papieru'),
(2015, 'Sprzęt / Narzędzia / Kompresory'),
(2018, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Ketchupy'),
(2020, 'Ubrania i akcesoria / Akcesoria do ubrań / Nakrycia głowy'),
(2021, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Linijki'),
(2027, 'Elektronika / Wideo / Akcesoria do wideo'),
(2028, 'Sprzęt / Materiały elektryczne / Gniazda keystone'),
(2030, 'Sprzęt / Materiały budowlane / Klapy'),
(2032, 'Sprzęt / Hydraulika / Armatury / Baterie'),
(2034, 'Dom i ogród / Akcesoria łazienkowe / Haczyki łazienkowe'),
(2044, 'Dom i ogród / Akcesoria do kominków i pieców opalanych węglem / Miechy'),
(2045, 'Meble / Meble biurowe / Krzesła i fotele biurowe'),
(2047, 'Biznes i przemysł / Środki ochrony osobistej'),
(2053, 'Sprzęt / Narzędzia / Urządzenia do czyszczenia rur'),
(2055, 'Sprzęt / Hydraulika / Dystrybucja i filtrowanie wody / Filtry narurowe'),
(2058, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Materiały malarskie / Farby do gruntowania'),
(2060, 'Dom i ogród / Sprzęt AGD / Ogrzewanie, wentylacja i klimatyzacja / Kaloryfery'),
(2062, 'Sprzęt / Hydraulika / Armatury / Toalety i bidety'),
(2063, 'Sprzęt / Hydraulika / Dystrybucja i filtrowanie wody / Akcesoria do filtrowania wody / Wkłady do filtrów wody'),
(2065, 'Oprogramowanie / Towary i waluty cyfrowe / Zdjęcia i filmy stockowe'),
(2068, 'Sprzęt / Hydraulika / Mocowania hydrauliczne / Dysze'),
(2070, 'Elektronika / Akcesoria do gier wideo / Akcesoria do przenośnych konsol do gier'),
(2072, 'Biznes i przemysł / Ciężki sprzęt / Rębaki'),
(2073, 'Żywność, napoje i tytoń / Napoje / Herbaty i napary'),
(2074, 'Zdrowie i uroda / Higiena osobista / Masaż i relaks / Stoły do masażu'),
(2077, 'Sprzęt / Narzędzia / Narzędzia malarskie'),
(2080, 'Sprzęt / Narzędzia / Przecinaki / Przecinaki do rur'),
(2081, 'Meble / Szafy i przechowywanie / Toaletki / Toaletki do łazienki'),
(2082, 'Elektronika / Akcesoria elektroniczne'),
(2088, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Części do prysznica / Baterie prysznicowe'),
(2092, 'Oprogramowanie'),
(2093, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Termopary'),
(2096, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych'),
(2104, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Winietki'),
(2106, 'Elektronika / Telekomunikacja / Urządzenia radiowe / Radia CB'),
(2107, 'Żywność, napoje i tytoń / Napoje / Napoje alkoholowe / Likiery i trunki / Wódki'),
(2110, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Ochraniacze motocyklowe / Kaski motocyklowe'),
(2121, 'Elektronika / Sieci / Regeneratory sygnału i transceivery'),
(2125, 'Dzieci i niemowlęta / Karmienie / Śliniaczki'),
(2126, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Oleje spożywcze'),
(2130, 'Elektronika / Akcesoria elektroniczne / Nośniki pamięci / ROM'),
(2139, 'Artykuły biurowe / Sortowanie i utrzymanie porządku / Oprawa dokumentów / Akcesoria do segregatorów / Koszulki'),
(2140, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Octy'),
(2145, 'Elektronika / Wideo / Akcesoria do wideo / Przewijacze'),
(2147, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Narzędzia ogrodnicze / Łopaty i szpadle'),
(2154, 'Elektronika / Audio / Systemy PA'),
(2155, 'Biznes i przemysł / Film i telewizja'),
(2160, 'Ubrania i akcesoria / Ubrania / Bielizna i skarpety / Dodatki do bielizny / Pasy do podwiązek'),
(2161, 'Dom i ogród / Bezpieczeństwo domu i biura / Czujniki ruchu'),
(2165, 'Elektronika / Audio / Elementy audio'),
(2169, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Naczynia do napojów / Kubki'),
(2170, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Części do odpływów / Hydraulika ścieków'),
(2171, 'Sprzęt / Hydraulika / Dystrybucja i filtrowanie wody / Akcesoria do filtrowania wody'),
(2174, 'Sprzęt / Akcesoria do narzędzi / Ostrza do narzędzi'),
(2175, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do podgrzewaczy wody / Bojlery'),
(2178, 'Elektronika / Elektronika morska / Chartplottery i GPS-y morskie'),
(2181, 'Sprzęt / Narzędzia / Narzędzia murarskie / Kielnie murarskie'),
(2182, 'Elektronika / Elementy / Konwertery'),
(2187, 'Biznes i przemysł / Górnictwo głębinowe i odkrywkowe'),
(2188, 'Żywność, napoje i tytoń / Żywność / Dipy i smarowidła / Dżemy i galaretki'),
(2192, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Suwmiarki'),
(2194, 'Żywność, napoje i tytoń / Żywność / Wypieki / Ciasta'),
(2195, 'Sprzęt / Akcesoria do sprzętu / Akcesoria mocujące do sprzętu / Uszczelki'),
(2198, 'Sprzęt / Narzędzia / Noże narzędziowe'),
(2202, 'Sprzęt / Akcesoria do narzędzi / Ostrza do narzędzi / Ostrza do pił'),
(2203, 'Sprzęt / Hydraulika / Zestawy do naprawy hydrauliki'),
(2204, 'Dom i ogród / Trawnik i ogród / Silnikowe narzędzia ogrodowe / Glebogryzarki i kultywatory silnikowe'),
(2205, 'Elektronika / Elementy / Konwertery / Konwertery skanujące'),
(2206, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Części do prysznica'),
(2207, 'Artykuły biurowe / Obróbka papieru / Falcerki'),
(2208, 'Sprzęt / Narzędzia / Wbijanie / Młotki'),
(2210, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Rząd koński / Siodła'),
(2212, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Taśmy'),
(2214, 'Artykuły dla dorosłych / Broń / Konserwacja i akcesoria do broni palnej'),
(2216, 'Sprzęt / Hydraulika / Rury hydrauliczne'),
(2218, 'Dom i ogród / Trawnik i ogród / Silnikowe narzędzia ogrodowe / Krawędziarki do trawników'),
(2220, 'Żywność, napoje i tytoń / Napoje / Napoje alkoholowe / Likiery i trunki / Tequila'),
(2221, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do podgrzewaczy wody / Stojaki na podgrzewaczy wody'),
(2222, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Akumulatory / Akumulatory do kamer wideo'),
(2227, 'Biznes i przemysł / Środki ochrony osobistej / Okulary ochronne'),
(2229, 'Żywność, napoje i tytoń / Żywność / Wypieki / Ciastka'),
(2230, 'Sprzęt / Akcesoria do sprzętu / Akcesoria mocujące do sprzętu / Ośki dwuczęściowe'),
(2234, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Ciemnia / Sprzęt do wywoływania i obróbki zdjęć'),
(2238, 'Sprzęt / Wentylacja / Sterowniki HVAC / Panele sterowania'),
(2242, 'Meble / Meble biurowe / Stoły do pracy / Stoły kreślarskie'),
(2243, 'Sprzęt / Hydraulika / Kontrolery poziomu wody'),
(2246, 'Zdrowie i uroda / Opieka zdrowotna / Monitory funkcji życiowych / Glukometry'),
(2247, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Ekspresy do kawy i espresso / Perkolatory'),
(2248, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Elementy ozdobne do pojazdów / Tablice rejestracyjne'),
(2249, 'Dom i ogród / Oświetlenie / Oprawy oświetleniowe / Żyrandole'),
(2250, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Miotełki do kurzu'),
(2251, 'Sprzęt / Akcesoria do sprzętu / Akcesoria mocujące do sprzętu / Śruby'),
(2257, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Części do odpływów / Wkłady do rur i odpływów'),
(2263, 'Artykuły biurowe / Sprzęt do prezentacji / Tablice do prezentacji / Tablice piankowe'),
(2271, 'Ubrania i akcesoria / Ubrania / Sukienki'),
(2273, 'Sprzęt / Hydraulika / Dystrybucja i filtrowanie wody'),
(2274, 'Sprzęt / Materiały elektryczne / Końcówki kablowe'),
(2277, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Chemikalia'),
(2282, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Materiały murarskie / Cementy'),
(2290, 'Pojazdy i części / Akcesoria i części do pojazdów / Przewóz i przechowywanie bagażu / Organizatory samochodowe'),
(2292, 'Ubrania i akcesoria / Ubrania / Uniformy / Fartuchy laboratoryjne'),
(2301, 'Dom i ogród / Artykuły gospodarstwa domowego / Środki i narzędzia do pielęgnacji obuwia / Torby na buty'),
(2302, 'Ubrania i akcesoria / Ubrania / Piżamy i ubrania na co dzień / Szlafroki'),
(2305, 'Sprzęt / Narzędzia / Narzędzia murarskie / Mieszacze do cementu'),
(2306, 'Ubrania i akcesoria / Ubrania / Uniformy'),
(2310, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do podgrzewaczy wody / Anody'),
(2313, 'Dom i ogród / Trawnik i ogród / Nawadnianie / Węże ogrodowe'),
(2314, 'Dom i ogród / Bielizna stołowa i pościelowa / Artykuły pościelowe / Prześcieradła'),
(2330, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Prowadnice do noży'),
(2334, 'Dom i ogród / Ozdoby / Tapety'),
(2337, 'Sprzęt / Narzędzia / Narzędzia murarskie / Pilniki grubozębne'),
(2343, 'Sprzęt / Hydraulika / Dystrybucja i filtrowanie wody / Dystrybutory wody'),
(2344, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Organizery kuchenne / Pudełka na przyprawy'),
(2345, 'Sprzęt / Materiały elektryczne / Kable i przewody elektryczne'),
(2349, 'Biznes i przemysł / Środki ochrony osobistej / Maski ochronne / Maski strażackie'),
(2353, 'Elektronika / Telekomunikacja / Telefonia / Akcesoria do telefonów komórkowych / Futerały na telefony komórkowe'),
(2358, 'Elektronika / Sieci / Mostki i routery / Bramki i routery VoIP'),
(2359, 'Sprzęt / Hydraulika / Mocowania hydrauliczne / Złączki do rur'),
(2363, 'Dom i ogród / Kuchnia i jadalnia / Naczynia barowe / Podkładki pod piwo'),
(2364, 'Żywność, napoje i tytoń / Napoje / Napoje alkoholowe / Likiery i trunki / Brandy'),
(2365, 'Dom i ogród / Akcesoria do kominków i pieców opalanych węglem / Kratki kominkowe'),
(2367, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do klimatyzatorów'),
(2370, 'Dom i ogród / Oświetlenie / Oświetlenie obrazów'),
(2371, 'Dom i ogród / Artykuły gospodarstwa domowego / Środki i narzędzia do pielęgnacji obuwia / Suszarki do butów'),
(2372, 'Elektronika / Audio / Akcesoria audio / Akcesoria do gramofonów'),
(2374, 'Dom i ogród / Artykuły gospodarstwa domowego / Worki na śmieci'),
(2376, 'Sprzęt / Hydraulika / Armatury / Toalety i bidety / Bidety'),
(2377, 'Sprzęt / Materiały elektryczne / Płyty ścienne'),
(2380, 'Sprzęt / Akcesoria do narzędzi / Zszywki przemysłowe'),
(2387, 'Zdrowie i uroda / Higiena osobista / Środki do higieny intymnej dla kobiet / Podpaski i wkładki higieniczne'),
(2389, 'Biznes i przemysł / Środki ochrony osobistej / Kamizelki kuloodporne'),
(2392, 'Żywność, napoje i tytoń / Żywność / Przekąski / Czipsy'),
(2394, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria i części do aparatów / Lampy do kamer wideo'),
(2396, 'Ubrania i akcesoria / Ubrania / Uniformy / Odzież gastronomiczna / Kurtki szefa kuchni'),
(2401, 'Artykuły biurowe / Sprzęt do prezentacji / Tablice do prezentacji / Tablice informacyjne'),
(2406, 'Sprzęt / Hydraulika / Dystrybucja i filtrowanie wody / Akcesoria do filtrowania wody / Obudowy do filtrów wody'),
(2408, 'Sprzęt / Akcesoria do sprzętu / Akcesoria mocujące do sprzętu / Gwoździe'),
(2410, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Akcesoria do umywalek / Nogi do umywalek'),
(2411, 'Sprzęt / Narzędzia / Przecinaki / Noże introligatorskie'),
(2413, 'Sprzęt / Materiały elektryczne / Automatyka domowa'),
(2414, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Pamięci komputerowe'),
(2416, 'Sprzęt / Narzędzia / Drabiny i rusztowania / Drabinowe schody jezdne'),
(2418, 'Dom i ogród / Akcesoria łazienkowe / Wiszące szafki łazienkowe'),
(2422, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Ekspresy do kawy i espresso / Ekspresy do espresso'),
(2423, 'Żywność, napoje i tytoń / Żywność / Zupy i buliony'),
(2425, 'Dom i ogród / Oświetlenie / Żarówki'),
(2427, 'Ubrania i akcesoria / Akcesoria do butów / Ostrogi'),
(2431, 'Dom i ogród / Artykuły gospodarstwa domowego / Środki i narzędzia do pielęgnacji obuwia / Prawidła do butów'),
(2432, 'Żywność, napoje i tytoń / Żywność / Przekąski / Ciasta ryżowe'),
(2435, 'Artykuły biurowe / Tabliczki z nazwiskiem'),
(2441, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja włosów / Szampony i odżywki'),
(2443, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Paintball i ASG / ASG'),
(2446, 'Dom i ogród / Artykuły gospodarstwa domowego / Przechowywanie i utrzymanie porządku / Haczyki i stojaki do przechowywania'),
(2447, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do latarek'),
(2455, 'Elektronika / Sieci / Huby i switche'),
(2456, 'Sprzęt / Narzędzia / Wibratory przemysłowe'),
(2463, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Akcesoria do wanien / Kurki do baterii nawannowych'),
(2465, 'Sprzęt / Narzędzia / Narzędzia malarskie / Pistolety natryskowe'),
(2466, 'Sprzęt / Hydraulika / Mocowania hydrauliczne / Zawory hydrauliczne'),
(2471, 'Elektronika / Telekomunikacja / Urządzenia radiowe'),
(2473, 'Biznes i przemysł / Środki ochrony osobistej / Maski ochronne / Maski gazowe'),
(2474, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Materiały malarskie / Spoiwa do farb'),
(2475, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Oświetlenie i studio / Kontrolery oświetlenia'),
(2477, 'Ubrania i akcesoria / Akcesoria do ubrań / Akcesoria do włosów / Grzebienie ozdobne'),
(2478, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Akcesoria do toalet i bidetów / Uszczelnienia do muszli'),
(2479, 'Elektronika / Sieci / Koncentratory i multipleksery'),
(2481, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Taśmy miernicze'),
(2485, 'Sprzęt / Akcesoria do sprzętu / Przechowywanie i porządkowanie narzędzi / Pasy na narzędzia'),
(2486, 'Sprzęt / Narzędzia / Narzędzia malarskie / Aerografy'),
(2490, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Oświetlenie i studio / Kontrolery oświetlenia / Filtry oświetleniowe i żelowe'),
(2494, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Osłony i pokrowce na pojazdy / Dachy sztywne (typu hardtop)'),
(2495, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Elementy ozdobne do pojazdów'),
(2496, 'Biznes i przemysł / Branża medyczna'),
(2497, 'Gry i zabawki / Zabawki / Lalki, zestawy i figurki / Akcesoria do domków dla lalek'),
(2499, 'Gry i zabawki / Zabawki / Lalki, zestawy i figurki / Domki dla lalek'),
(2503, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria i kosmetyki do kąpieli / Mydła w kostce'),
(2505, 'Gry i zabawki / Zabawki / Pojazdy do zabawy'),
(2507, 'Dom i ogród / Kuchnia i jadalnia / Transportery do żywności i napojów / Torby termoizolacyjne'),
(2508, 'Zdrowie i uroda / Higiena osobista / Golenie i strzyżenie / Sztyfty do tamowania krwawienia'),
(2511, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do pielęgnacji twarzy / Pumeksy'),
(2512, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Pływanie / Płetwy pływackie / Płetwy treningowe'),
(2513, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Płyny eksploatacyjne / Płyny do układu wspomagania kierownicy'),
(2515, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Ręczne instrumenty perkusyjne / Tamburyny'),
(2516, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Ciemnia / Sprzęt do wywoływania i obróbki zdjęć / Sprzęt i środki do retuszu'),
(2517, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Płyny eksploatacyjne / Dodatki do płynów chłodniczych'),
(2518, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Zestawy perkusyjne'),
(2520, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Ciemnia / Sprzęt do powiększania'),
(2521, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja oczu / Akcesoria do okularów'),
(2522, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria i kosmetyki do kąpieli / Dodatki do kąpieli'),
(2524, 'Dom i ogród / Oświetlenie / Oprawy oświetleniowe / Sufitowe oprawy oświetleniowe'),
(2526, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja skóry / Produkty do mycia twarzy'),
(2527, 'Zdrowie i uroda / Higiena osobista / Higiena jamy ustnej / Protezy'),
(2528, 'Pojazdy i części / Pojazdy / Pojazdy silnikowe / Pojazdy terenowe i quady / Gokarty i pojazdy typu buggy'),
(2530, 'Dom i ogród / Artykuły gospodarstwa domowego / Produkty papierowe do gospodarstwa domowego'),
(2531, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Banery'),
(2532, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Rękodzieło z papieru / Papiery kolorowe'),
(2534, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Części i akcesoria do szyb samochodowych'),
(2535, 'Dom i ogród / Sprzęt AGD / Ogrzewanie, wentylacja i klimatyzacja / Wentylatory / Wentylatory biurkowe i stojące'),
(2540, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do maszynek do makaronu'),
(2541, 'Dom i ogród / Bielizna stołowa i pościelowa / Artykuły pościelowe / Poszwy'),
(2543, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Ciemnia / Sprzęt do powiększania / Zegary ciemniowe'),
(2546, 'Gry i zabawki / Zabawki / Zabawki zdalnie sterowane'),
(2547, 'Dom i ogród / Bielizna stołowa i pościelowa / Bielizna stołowa / Maty stołowe'),
(2548, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do makijażu'),
(2549, 'Dom i ogród / Sprzęt AGD / Urządzenia do prania / Pralki'),
(2550, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Układy smarowania'),
(2552, 'Zdrowie i uroda / Opieka zdrowotna / Testy medyczne / Testy narkotykowe'),
(2556, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Koła do pojazdów silnikowych / Części do kół'),
(2559, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Wręczanie prezentów'),
(2562, 'Ubrania i akcesoria / Ubrania / Bielizna i skarpety / Bielizna'),
(2563, 'Ubrania i akcesoria / Ubrania / Bielizna i skarpety / Dodatki do bielizny'),
(2564, 'Zdrowie i uroda / Higiena osobista / Środki do higieny intymnej dla kobiet / Tampony'),
(2570, 'Sprzęt / Hydraulika / Węże i przewody hydrauliczne'),
(2571, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż / Makijaż twarzy');
INSERT INTO `google_kategorie` (`id`, `nazwa`) VALUES
(2572, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Mieszanki do pieczenia'),
(2579, 'Sztuka i rozrywka / Hobby i sztuki piękne / Warzenie piwa i pędzenie wina / Produkcja wina'),
(2580, 'Ubrania i akcesoria / Ubrania / Piżamy i ubrania na co dzień / Piżamy'),
(2582, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Elementy ozdobne do pojazdów / Akcesoria do deski rozdzielczej'),
(2587, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Balony'),
(2588, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Elementy ozdobne do pojazdów / Piłki na anteny'),
(2589, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż / Makijaż ust / Konturówki do ust'),
(2590, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Czyszczenie i mycie pojazdów / Szampony samochodowe'),
(2591, 'Artykuły biurowe / Artykuły różne / Korektory'),
(2592, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja skóry / Balsamy i kremy do ciała'),
(2596, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja uszu / Stopery do uszu'),
(2600, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Ciemnia / Lampy ciemniowe'),
(2605, 'Żywność, napoje i tytoń / Napoje / Napoje alkoholowe / Likiery i trunki / Rum'),
(2608, 'Dom i ogród / Oświetlenie / Przewody i węże świetlne'),
(2610, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Ściągaczki do szyb i okien'),
(2611, 'Sprzęt / Hydraulika / Mocowania hydrauliczne / Regulatory hydrauliczne'),
(2612, 'Dom i ogród / Sprzęt AGD / Urządzenia do prania / Suszarki'),
(2613, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Architektura ogrodowa'),
(2614, 'Sprzęt sportowy / Fitness / Trening kardio / Skakanki'),
(2618, 'Gry i zabawki / Łamigłówki / Puzzle'),
(2619, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne'),
(2620, 'Zdrowie i uroda / Higiena osobista / Higiena jamy ustnej / Nici dentystyczne'),
(2621, 'Dom i ogród / Ozdoby / Dekoracje okienne / Lambrekiny i gzymsy'),
(2623, 'Artykuły biurowe / Akcesoria biurowe / Przybory piśmiennicze i rysunkowe / Węgiel rysunkowy'),
(2625, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Ciemnia / Sprzęt do wywoływania i obróbki zdjęć / Statywy do reprodukcji'),
(2626, 'Dom i ogród / Kuchnia i jadalnia / Przechowywanie żywności'),
(2627, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria do obiektywów / Osłony na obiektywy'),
(2628, 'Żywność, napoje i tytoń / Napoje / Napoje gazowane'),
(2629, 'Sprzęt / Narzędzia / Wiertarki / Wiertarki przenośne'),
(2631, 'Dom i ogród / Artykuły gospodarstwa domowego / Dezynsekcja / Pułapki na szkodniki'),
(2633, 'Zdrowie i uroda / Opieka zdrowotna / Monitory funkcji życiowych / Alkomaty'),
(2634, 'Sprzęt / Hydraulika / Mocowania hydrauliczne / Zaciski na rury'),
(2635, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Płyny eksploatacyjne / Zimowe płyny do spryskiwaczy'),
(2636, 'Artykuły biurowe / Akcesoria pakunkowe'),
(2639, 'Dom i ogród / Piece opalane drewnem'),
(2641, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Części układu przeniesienia napędu'),
(2642, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Lusterka samochodowe'),
(2643, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Czyszczenie i mycie pojazdów / Woski, politury i ochrona lakieru'),
(2644, 'Dom i ogród / Kuchnia i jadalnia / Przechowywanie żywności / Słoje na ciastka'),
(2645, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż / Makijaż ust'),
(2647, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Narzędzia do naprawy pojazdów / Zestawy do naprawy szyby przedniej'),
(2649, 'Dom i ogród / Sprzęt AGD / Ogrzewanie, wentylacja i klimatyzacja / Ogrzewacze ogrodowe'),
(2650, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Ciemnia / Sprzęt do wywoływania i obróbki zdjęć / Koreksy i szpule'),
(2652, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Elementy ozdobne do pojazdów / Chorągiewki'),
(2656, 'Zdrowie i uroda / Higiena osobista / Pęsety'),
(2658, 'Artykuły biurowe / Artykuły różne / Artykuły papiernicze / Kołonotatniki'),
(2660, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia'),
(2665, 'Sprzęt / Materiały budowlane / Wyposażenie do drzwi / Odboje drzwiowe'),
(2667, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Elementy ozdobne do pojazdów / Naklejki na zderzaki'),
(2668, 'Ubrania i akcesoria / Torebki, portfele i etui / Portfele i spinki na banknoty'),
(2669, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Włókna do rękodzieła / Przędza'),
(2671, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do mierzenia i oznaczania / Szablony i wycinanki'),
(2672, 'Dom i ogród / Baseny i spa / Akcesoria do basenów i spa / Zabawki na basen'),
(2674, 'Artykuły biurowe / Sprzęt do prezentacji / Tablice do prezentacji / Tablice na plakaty'),
(2675, 'Dom i ogród / Ozdoby / Wycieraczki'),
(2677, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły do pralni / Suszarki rozkładane'),
(2681, 'Zdrowie i uroda / Higiena osobista / Golenie i strzyżenie / Pędzle do golenia'),
(2683, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja paznokci / Lakiery do paznokci'),
(2684, 'Meble / Meble zewnętrzne / Stoły ogrodowe'),
(2686, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż / Makijaż oka / Kredki do brwi'),
(2688, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Płyny eksploatacyjne / Oleje przekładniowe'),
(2689, 'Artykuły biurowe / Artykuły różne / Artykuły papiernicze / Karteczki samoprzylepne'),
(2690, 'Dom i ogród / Oświetlenie / Żarówki / Świetlówki'),
(2691, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Akcesoria do toalet i bidetów'),
(2693, 'Meble / Stoły / Stoły do pokera i gier'),
(2694, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Naczynia do napojów / Kieliszki do shotów'),
(2696, 'Meble / Łóżka i akcesoria / Materace'),
(2698, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Ciemnia / Sprzęt do powiększania / Powiększalniki fotograficzne'),
(2699, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Alarmy i zamki samochodowe / Systemy dostępu zdalnego do pojazdów'),
(2700, 'Dom i ogród / Bielizna stołowa i pościelowa / Artykuły pościelowe / Poduszki'),
(2704, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Czyszczenie i mycie pojazdów / Środki do czyszczenia tapicerki samochodowej'),
(2706, 'Dom i ogród / Sprzęt AGD / Urządzenia do prania'),
(2710, 'Sprzęt / Hydraulika / Mocowania hydrauliczne / Przejściówki i łączniki hydrauliczne'),
(2712, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Naczynia do napojów / Kieliszki'),
(2713, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Mopy'),
(2719, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Płyny eksploatacyjne / Dodatki czyszczące do paliwa'),
(2720, 'Meble / Łóżka i akcesoria / Podstawy pod materace'),
(2722, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Elementy ozdobne do pojazdów / Naklejki na karoserię'),
(2723, 'Biznes i przemysł / Środki ochrony osobistej / Kaski'),
(2724, 'Gry i zabawki / Zabawki / Zabawki do jeżdżenia / Pojazdy do popychania i z pedałami'),
(2726, 'Sprzęt / Narzędzia / Przecinaki / Przecinaki do kafelków i gontów'),
(2727, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Układy paliwowe'),
(2728, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Ciemnia / Sprzęt do wywoływania i obróbki zdjęć / Kuwety, kuwety do płukania, kuwety do suszenia'),
(2729, 'Sprzęt / Materiały budowlane / Blaty'),
(2733, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja oczu / Szkła do okularów'),
(2734, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do paznokci / Pilniczki'),
(2735, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Płyny eksploatacyjne / Smary samochodowe'),
(2739, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do paznokci / Kopytka do skórek'),
(2740, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja skóry / Samoopalacze, kosmetyki brązujące i podkreślające opaleniznę'),
(2741, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Rękodzieło z papieru / Papiery welinowe'),
(2742, 'Dom i ogród / Artykuły gospodarstwa domowego / Produkty papierowe do gospodarstwa domowego / Ręczniki papierowe'),
(2743, 'Gry i zabawki / Zabawki ogrodowe / Piaskownice'),
(2745, 'Ubrania i akcesoria / Ubrania / Bielizna i skarpety / Podkoszulki'),
(2747, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria i kosmetyki do kąpieli / Żele pod prysznic'),
(2750, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Alarmy i zamki samochodowe / Blokady kierownicy'),
(2751, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do podgrzewaczy wody'),
(2753, 'Gry i zabawki / Zabawki / Zabawki do jeżdżenia / Pojazdy elektryczne'),
(2754, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły do pralni / Detergenty do prania'),
(2755, 'Dom i ogród / Baseny i spa / Akcesoria do basenów i spa / Odpływy basenowe'),
(2756, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Rząd koński / Uprzęże jeździeckie'),
(2757, 'Sprzęt / Hydraulika / Armatury / Zlewy i umywalki / Zlewy kuchenne i użytkowe'),
(2761, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż / Makijaż oka / Sztuczne rzęsy'),
(2763, 'Dom i ogród / Ozdoby / Fontanny i stawy / Stawy'),
(2764, 'Dzieci i niemowlęta / Transport niemowląt'),
(2765, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż / Makijaż twarzy / Podkłady i korektory'),
(2766, 'Sprzęt / Wentylacja / Wentylatory i przewody wentylacyjne'),
(2768, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Alarmy i zamki samochodowe'),
(2769, 'Zdrowie i uroda / Higiena osobista / Higiena jamy ustnej / Stymulatory do masażu dziąseł'),
(2770, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Płyny eksploatacyjne / Dodatki do paliwa'),
(2775, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Mąki'),
(2778, 'Gry i zabawki / Zabawki / Akcesoria do zabawek zdalnie sterowanych'),
(2779, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż / Makijaż oka'),
(2780, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do makijażu / Zalotki'),
(2781, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Konfetti'),
(2783, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Trąbki i piszczałki na przyjęcia'),
(2784, 'Dom i ogród / Ozdoby / Akcesoria do zapachów do domu / Świeczniki'),
(2786, 'Meble / Materace do futonów'),
(2788, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Płyny eksploatacyjne'),
(2789, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Elementy ozdobne do pojazdów / Samochodowe odświeżacze powietrza'),
(2792, 'Sprzęt / Narzędzia / Szczotki do komina'),
(2794, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły do pralni / Zmiękczacze i osuszacze'),
(2795, 'Sprzęt / Materiały budowlane / Wyposażenie do drzwi / Kołatki do drzwi'),
(2796, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Gąbki i zmywaki do szorowania'),
(2797, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Werble'),
(2799, 'Gry i zabawki / Zabawki / Zabawki do jeżdżenia'),
(2801, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja stóp / Wkładki do butów'),
(2802, 'Dom i ogród / Rośliny / Nasiona'),
(2803, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Proszek do pieczenia'),
(2804, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Ciemnia / Papier fotograficzny'),
(2805, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Układy chłodzenia'),
(2807, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż / Makijaż oka / Eyelinery'),
(2808, 'Biznes i przemysł / Środki ochrony osobistej / Odzież ochronna do pracy z materiałami niebezpiecznymi'),
(2809, 'Dom i ogród / Oświetlenie / Oprawy oświetleniowe / Lampy meblowe'),
(2810, 'Dom i ogród / Baseny i spa / Baseny pływackie'),
(2814, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja włosów / Produkty do koloryzacji włosów'),
(2815, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Ciemnia / Sprzęt do powiększania / Analizatory ciemniowe'),
(2816, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Wręczanie prezentów / Pakowanie prezentów / Papiery do pakowania'),
(2817, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Akcesoria do toalet i bidetów / Dźwignie do spłuczek'),
(2820, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Części silnikowe'),
(2826, 'Sprzęt / Materiały budowlane / Podłogi i dywany'),
(2828, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do paznokci / Cążki do paznokci'),
(2829, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria do obiektywów / Pokrywki na obiektywy'),
(2832, 'Dom i ogród / Baseny i spa / Akcesoria do basenów i spa'),
(2833, 'Pojazdy i części / Akcesoria i części do pojazdów / Wyposażenie elektroniczne pojazdów silnikowych / Subwoofery'),
(2834, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż / Makijaż oka / Tusze do rzęs'),
(2835, 'Sprzęt / Narzędzia / Zaciski i imadła narzędziowe'),
(2836, 'Pojazdy i części / Akcesoria i części do pojazdów / Przewóz i przechowywanie bagażu / Bagażniki samochodowe / Bagażniki do przewozu rowerów'),
(2837, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na wrotkach i łyżworolkach / Wrotki'),
(2839, 'Dom i ogród / Ozdoby / Wiatraczki'),
(2843, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Formy do pieczenia / Formy do pizzy'),
(2844, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja skóry / Ochrona przeciwsłoneczna'),
(2846, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Czyszczenie i mycie pojazdów / Środki do czyszczenia szyb'),
(2847, 'Dzieci i niemowlęta / Zabawki dla niemowląt'),
(2849, 'Dom i ogród / Sprzęt AGD / Urządzenia do prania / Urządzenia piorące kombo'),
(2851, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Części do odpływów / Korki i sitka do odpływów'),
(2856, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Perkusje elektroniczne'),
(2857, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Miotły'),
(2858, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż / Makijaż ust / Błyszczyki do ust'),
(2860, 'Dom i ogród / Baseny i spa / Akcesoria do basenów i spa / Zabawki dmuchane i leżaki do basenów'),
(2862, 'Dom i ogród / Akcesoria do kominków i pieców opalanych węglem'),
(2865, 'Dom i ogród / Artykuły gospodarstwa domowego / Dezynsekcja / Środki odstraszające'),
(2867, 'Gry i zabawki / Zabawki ogrodowe / Zjeżdżalnie'),
(2869, 'Dom i ogród / Artykuły gospodarstwa domowego / Dezynsekcja / Pestycydy'),
(2875, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria i kosmetyki do kąpieli / Gąbki i myjki do ciała'),
(2876, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria i kosmetyki do kąpieli / Szczotki do kąpieli'),
(2878, 'Sprzęt / Akcesoria do sprzętu'),
(2879, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Samochodowe elementy bezpieczeństwa'),
(2881, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Płyny eksploatacyjne / Środki do odtłuszczania silnika'),
(2882, 'Dom i ogród / Ozdoby / Dekoracje okienne / Zasłony i draperie'),
(2883, 'Artykuły biurowe / Akcesoria biurowe / Dzwonki recepcyjne'),
(2885, 'Dom i ogród / Ozdoby / Dekoracje okienne / Rolety i żaluzje'),
(2886, 'Sprzęt / Hydraulika / Armatury / Zlewy i umywalki / Umywalki łazienkowe'),
(2887, 'Żywność, napoje i tytoń / Napoje / Soki'),
(2890, 'Zdrowie i uroda / Opieka zdrowotna / Zdrowy tryb życia i dieta'),
(2894, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Czyszczenie i mycie pojazdów / Szczotki do mycia samochodów'),
(2895, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Czyszczenie i mycie pojazdów'),
(2899, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Wręczanie prezentów / Kwiaty cięte'),
(2901, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych'),
(2904, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż / Makijaż oka / Cienie do powiek'),
(2905, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Drożdże'),
(2907, 'Biznes i przemysł / Branża medyczna / Medyczne materiały eksploatacyjne'),
(2909, 'Sprzęt / Hydraulika / Mocowania hydrauliczne / Nasadki i zaślepki na rury'),
(2911, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria do obiektywów'),
(2915, 'Zdrowie i uroda / Higiena osobista'),
(2916, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Płyny eksploatacyjne / Oleje hydrauliczne do sprzęgła'),
(2917, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Bębny basowe'),
(2918, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe'),
(2919, 'Meble / Krzesła i fotele / Fotele masujące'),
(2920, 'Dom i ogród / Kuchnia i jadalnia / Transportery do żywności i napojów'),
(2921, 'Dom i ogród / Ozdoby / Fontanny i stawy / Akcesoria do fontann i stawów'),
(2922, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja oczu / Krople do oczu'),
(2923, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja oczu / Szkła kontaktowe'),
(2926, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Oświetlenie i studio / Oświetlenie studyjne'),
(2927, 'Dom i ogród / Bielizna stołowa i pościelowa / Artykuły pościelowe / Powłoczki i ozdobne poszewki na poduszki'),
(2928, 'Biznes i przemysł / Branża medyczna / Odzież chirurgiczna'),
(2932, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Koła do pojazdów silnikowych / Koła i felgi'),
(2933, 'Żywność, napoje i tytoń / Napoje / Napoje alkoholowe / Likiery i trunki / Likiery'),
(2934, 'Zdrowie i uroda / Higiena osobista / Patyczki kosmetyczne'),
(2935, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Części zawieszenia'),
(2939, 'Dom i ogród / Baseny i spa / Akcesoria do basenów i spa / Trampoliny basenowe'),
(2943, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Płyny eksploatacyjne / Płyny do spryskiwaczy'),
(2944, 'Dom i ogród / Oświetlenie / Żarówki / Żarówki tradycyjne'),
(2946, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja paznokci / Zmywacze do paznokci'),
(2947, 'Dom i ogród / Oświetlenie / Żarówki / Świetlówki kompaktowe'),
(2948, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Organizery kuchenne'),
(2949, 'Dzieci i niemowlęta / Przewijanie / Środki na wysypkę pieluszkową'),
(2951, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Naczynia do napojów / Tumblery'),
(2953, 'Gry i zabawki / Zabawki / Zabawki nakręcane'),
(2954, 'Zdrowie i uroda / Opieka zdrowotna / Pierwsza pomoc / Środki antyseptyczne i czyszczące'),
(2956, 'Dom i ogród / Akcesoria oświetleniowe'),
(2958, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do pielęgnacji twarzy'),
(2962, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo'),
(2963, 'Ubrania i akcesoria / Ubrania / Bielizna i skarpety / Halki i reformy'),
(2966, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Pływanie / Deski do pływania'),
(2967, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Rękodzieło z papieru / Papiery do origami'),
(2969, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Ciemnia / Sprzęt do powiększania / Ramki do powiększalnika'),
(2971, 'Zdrowie i uroda / Higiena osobista / Golenie i strzyżenie / Kremy do golenia'),
(2972, 'Sprzęt / Materiały budowlane / Wyposażenie do drzwi / Dzwonki i gongi do drzwi'),
(2974, 'Dom i ogród / Bielizna stołowa i pościelowa / Artykuły pościelowe / Falbany do łóżek'),
(2975, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do paznokci'),
(2976, 'Dom i ogród / Kuchnia i jadalnia / Naczynia barowe / Korkociągi'),
(2977, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Układy hamulcowe'),
(2978, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Ogniwa paliwowe'),
(2980, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż / Makijaż twarzy / Pudry do twarzy'),
(2981, 'Dom i ogród / Baseny i spa / Akcesoria do basenów i spa / Filtry do basenów i spa'),
(2982, 'Dom i ogród / Baseny i spa / Spa'),
(2984, 'Zdrowie i uroda / Opieka zdrowotna / Zdrowy tryb życia i dieta / Batony energetyczne'),
(2985, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Grille na zewnątrz'),
(2986, 'Artykuły biurowe / Akcesoria biurowe'),
(2987, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria i części do aparatów / Uchwyty na lampę błyskową'),
(2988, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Mulcz'),
(2989, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Koła do pojazdów silnikowych / Akcesoria do opon'),
(2991, 'Dom i ogród / Bielizna stołowa i pościelowa / Artykuły pościelowe / Ochraniacze na materace / Maty na materace'),
(2992, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja stóp / Leczenie haluksów'),
(2994, 'Dom i ogród / Baseny i spa / Akcesoria do basenów i spa / Osłony do basenów'),
(2996, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Akcesoria do wanien'),
(2997, 'Dom i ogród / Baseny i spa / Akcesoria do basenów i spa / Odkurzacze i szczotki basenowe'),
(2999, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Ciemnia / Sprzęt do wywoływania i obróbki zdjęć / Zlewy do ciemni'),
(3001, 'Sprzęt sportowy / Sport / Hokej na trawie i lacrosse / Bramki do hokeja na trawie'),
(3002, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Chemikalia laboratoryjne'),
(3005, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Tom-tomy'),
(3006, 'Dom i ogród / Oświetlenie / Oprawy oświetleniowe'),
(3009, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja paznokci / Kremy i olejki do skórek'),
(3011, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja oczu / Pielęgnacja szkieł kontaktowych'),
(3013, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja włosów / Nożyczki do włosów'),
(3014, 'Sztuka i rozrywka / Hobby i sztuki piękne / Warzenie piwa i pędzenie wina / Ziarna i słody do warzenia piwa'),
(3015, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Hi-haty'),
(3017, 'Dom i ogród / Baseny i spa / Akcesoria do basenów i spa / Środki czystości i chemikalia do basenów'),
(3018, 'Pojazdy i części / Pojazdy / Pojazdy silnikowe / Pojazdy terenowe i quady / Quady i pojazdy wielozadaniowe'),
(3019, 'Zdrowie i uroda / Higiena osobista / Higiena jamy ustnej / Wybielacze zębów'),
(3020, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Koła do pojazdów silnikowych'),
(3021, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż / Makijaż ust / Pomadki do ust'),
(3022, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja stóp / Usuwanie nagniotków i modzeli'),
(3024, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Alarmy i zamki samochodowe / Immobilisery'),
(3025, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do makijażu / Pędzle do makijażu'),
(3026, 'Artykuły biurowe / Akcesoria biurowe / Przybory piśmiennicze i rysunkowe / Pióra, długopisy i ołówki / Ołówki / Ołówki do szkicowania'),
(3029, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Ciemnia / Sprzęt do powiększania / Lupy'),
(3031, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Materiały murarskie / Cegły, kamienie, beton'),
(3032, 'Ubrania i akcesoria / Torebki, portfele i etui / Torebki'),
(3035, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria do statywów trójnożnych i jednonożnych / Uchwyty i mocowania do statywów trójnożnych'),
(3037, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do paznokci / Nożyczki do skórek'),
(3040, 'Zdrowie i uroda / Higiena osobista / Higiena jamy ustnej / Płyny do płukania jamy ustnej'),
(3043, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Cymbały'),
(3044, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Płyny eksploatacyjne / Oleje silnikowe'),
(3049, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja stóp / Odświeżacze do stóp'),
(3051, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Płyny eksploatacyjne / Płyny hamulcowe'),
(3053, 'Sprzęt / Akcesoria do sprzętu / Łańcuchy, druty i sznury / Liny'),
(3055, 'Elektronika / Audio / Akcesoria audio / Akcesoria do odtwarzaczy MP3 / Futerały na odtwarzacze MP3'),
(3056, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Oświetlenie i studio / Kontrolery oświetlenia / Rozpraszacze światła'),
(3057, 'Sprzęt sportowy / Sport / Zapasy / Ochraniacze zapaśnicze'),
(3059, 'Sprzęt sportowy / Gry towarzyskie / Bilard / Trójkąty do ustawiania bil'),
(3060, 'Sprzęt sportowy / Sport / Futbol amerykański / Ochraniacze do futbolu amerykańskiego / Akcesoria do kasków do futbolu amerykańskiego'),
(3061, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Organizery kuchenne / Uchwyty na ręczniki papierowe'),
(3062, 'Artykuły biurowe / Sortowanie i utrzymanie porządku / Pojemniki na przybory piśmiennicze i rysunkowe'),
(3063, 'Sprzęt sportowy / Sport / Futbol amerykański / Ochraniacze do futbolu amerykańskiego / Akcesoria do kasków do futbolu amerykańskiego / Osłony oczu do kasków do futbolu amerykańskiego'),
(3064, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Chodzenie z rakietami śnieżnymi / Rakiety śnieżne'),
(3066, 'Ubrania i akcesoria / Ubrania / Odzież wierzchnia / Stroje przeciwdeszczowe'),
(3067, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na deskorolce / Stroje ochronne dla deskorolkarzy'),
(3070, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów jednokołowych'),
(3071, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Narzędzia ogrodnicze / Widły ogrodowe'),
(3072, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Organizery kuchenne / Serwetniki'),
(3073, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Chodzenie z rakietami śnieżnymi / Wiązania do rakiet śnieżnych'),
(3074, 'Gry i zabawki / Zabawki / Zabawki biurowe'),
(3077, 'Sprzęt sportowy / Sport / Rugby / Ochraniacze do rugby / Kaski do rugby'),
(3079, 'Gry i zabawki / Zabawki / Zabawki artystyczne i malarskie / Tablice rysunkowe'),
(3080, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły do pralni / Rolki od ubrań'),
(3082, 'Dom i ogród / Sprzęt AGD / Ogrzewanie, wentylacja i klimatyzacja / Piece i bojlery'),
(3083, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do mierzenia i oznaczania / Cyrkle'),
(3084, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Sprzęt jeździecki / Rękawice jeździeckie'),
(3086, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Skrobaczki kuchenne / Skrobaczki do misek'),
(3087, 'Pojazdy i części / Pojazdy / Jednostki pływające / Łodzie żaglowe'),
(3088, 'Gry i zabawki / Zabawki / Zabawki edukacyjne / Farmy mrówek'),
(3089, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Meble kempingowe / Łóżka polowe'),
(3090, 'Sprzęt sportowy / Sport / Futbol amerykański / Ochraniacze do futbolu amerykańskiego / Akcesoria do kasków do futbolu amerykańskiego / Maski do futbolu amerykańskiego'),
(3091, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Termometry spożywcze'),
(3092, 'Artykuły dla dorosłych / Broń / Nunczaka'),
(3093, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Paintball i ASG / ASG / Broń ASG'),
(3094, 'Elektronika / Płyty drukowane i komponenty / Półprzewodniki / Tranzystory'),
(3095, 'Pojazdy i części / Pojazdy / Jednostki pływające / Łodzie motorowe'),
(3096, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Żywa przynęta'),
(3097, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Części silników do jednostek pływających / Wirniki do jednostek pływających'),
(3101, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Sporty wodne holowane / Kneeboarding / Deski do kneeboardingu'),
(3103, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Herbicydy'),
(3105, 'Sprzęt sportowy / Sport / Tenis / Kosze i wózki na piłki tenisowe'),
(3106, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Golf / Kołeczki pod piłki golfowe'),
(3107, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Gry na świeżym powietrzu / Badminton / Siatki do badmintona'),
(3109, 'Sprzęt sportowy / Gry towarzyskie / Gra w rzutki / Części lotek / Shafty do lotek'),
(3110, 'Dom i ogród / Kuchnia i jadalnia / Przechowywanie żywności / Zawijanie żywności'),
(3111, 'Zdrowie i uroda / Opieka zdrowotna / Akcesoria do monitorów funkcji życiowych / Akcesoria do glukometrów / Nakłuwacze'),
(3113, 'Sprzęt sportowy / Sport / Tenis / Piłki tenisowe'),
(3116, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Paintball i ASG / ASG / Akcesoria do ASG'),
(3117, 'Elektronika / Sprzęt do gier zręcznościowych / Automaty do gry'),
(3118, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Odzież i akcesoria dla rowerzystów / Wpinane buty rowerowe'),
(3119, 'Sprzęt sportowy / Sport / Racquetball i squash / Gogle do gry w squasha i raquetballa'),
(3120, 'Dom i ogród / Trawnik i ogród / Silnikowe narzędzia ogrodowe / Przycinarki do żywopłotu'),
(3121, 'Elektronika / Płyty drukowane i komponenty / Elementy pasywne obwodów / Elementy indukcyjne'),
(3123, 'Sprzęt sportowy / Sport / Gimnastyka / Odskocznie'),
(3124, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do pakowarek próżniowych / Worki do pakowarek próżniowych'),
(3125, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Strzelanie do rzutek'),
(3126, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Gry na świeżym powietrzu / Piłka na uwięzi'),
(3127, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na deskorolce / Poręcze do jazdy na deskorolce'),
(3128, 'Ubrania i akcesoria / Ubrania / Ubrania sportowe / Stroje rowerowe / Spodenki kolarskie'),
(3129, 'Gry i zabawki / Zabawki / Zabawa przez imitację / Zabawki do odgrywania ról i zawodów'),
(3130, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Akcesoria do akumulatorów / Ładowarki do akumulatorów do aparatów'),
(3132, 'Sprzęt sportowy / Gry towarzyskie / Tenis stołowy / Akcesoria do robotów treningowych do tenisa stołowego'),
(3135, 'Sprzęt sportowy / Gry towarzyskie / Bilard / Bile do gry w bilarda'),
(3136, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Myślistwo'),
(3139, 'Sprzęt sportowy / Gry towarzyskie / Bilard / Stoły bilardowe'),
(3140, 'Elektronika / Sprzęt do gier zręcznościowych / Flippery'),
(3141, 'Sprzęt sportowy / Sport / Piłka nożna / Rękawice bramkarskie'),
(3143, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Części silników do jednostek pływających / Alternatory do jednostek pływających'),
(3144, 'Dom i ogród / Ozdoby / Ozdoby sezonowe i świąteczne / Ozdoby świąteczne'),
(3148, 'Sprzęt sportowy / Gry towarzyskie / Shuffleboard stołowy / Stoły do gry w shuffleboard'),
(3149, 'Sprzęt sportowy / Sport / Lekkoatletyka / Płotki'),
(3151, 'Żywność, napoje i tytoń / Wyroby tytoniowe / Papierosy'),
(3152, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Paintball i ASG / Paintball / Akcesoria do markerów paintballowych / Hoppery'),
(3156, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Skrobaki do żywności'),
(3160, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Listwy antyprzepięciowe'),
(3163, 'Gry i zabawki / Zabawki / Zabawki do budowania / Tory do kulek'),
(3164, 'Sprzęt sportowy / Fitness / Podnoszenie ciężarów / Wolne ciężary'),
(3166, 'Gry i zabawki / Zabawki / Lalki, zestawy i figurki / Zestawy zabawek'),
(3170, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Akcesoria strzeleckie / Tarcze strzeleckie'),
(3172, 'Gry i zabawki / Zabawki / Zabawki do budowania / Klocki piankowe'),
(3173, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Narzędzia ogrodnicze'),
(3175, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Łyżki do nabierania'),
(3176, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych / Akcesoria do gitar / Klucze do gitar'),
(3177, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Organizery kuchenne / Kuchenne organizery nablatowe i dystrybutory do napojów'),
(3178, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych / Akcesoria do gitar / Struny do gitar'),
(3181, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Krajalnice do mięs'),
(3182, 'Sprzęt sportowy / Sport / Gimnastyka / Konie gimnastyczne'),
(3183, 'Sprzęt sportowy / Gry towarzyskie / Bilard / Części i akcesoria do stołów bilardowych'),
(3185, 'Dom i ogród / Akcesoria oświetleniowe / Klosze lamp'),
(3187, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Paintball i ASG / Paintball / Akcesoria do markerów paintballowych'),
(3188, 'Ubrania i akcesoria / Ubrania / Ubrania sportowe / Stroje rowerowe / Szorty rowerowe'),
(3189, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Dokowanie i kotwiczenie / Kotwice'),
(3190, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Gry na świeżym powietrzu / Shuffleboard / Krążki do gry w shuffleboard'),
(3191, 'Ubrania i akcesoria / Ubrania / Uniformy / Stroje sportowe / Stroje do baseballu'),
(3192, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na deskorolce / Części do deskorolek / Trucki do deskorolek'),
(3195, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Sporty wodne holowane'),
(3196, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Płaskie łopatki'),
(3199, 'Gry i zabawki / Zabawki / Zabawki sportowe / Zabawki fitness'),
(3201, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wspinaczka / Torby na liny'),
(3202, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Łyżki do nabierania / Łyżki do lodów'),
(3204, 'Sprzęt sportowy / Sport / Hokej na trawie i lacrosse / Sprzęt treningowy do lacrosse'),
(3206, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Krajalnice kuchenne'),
(3207, 'Gry i zabawki / Zabawki / Baseny kulkowe'),
(3208, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria i kosmetyki do kąpieli / Mydła w płynie do rąk'),
(3210, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do strugarek poprzecznych / Frezy do kół zębatych'),
(3211, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wspinaczka / Woreczki na magnezję'),
(3212, 'Gry i zabawki / Zabawki / Zabawki ruchowe / Płyny do dmuchania baniek'),
(3213, 'Elektronika / Akcesoria do GPS-ów / Uchwyty do GPS-ów'),
(3214, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów'),
(3215, 'Gry i zabawki / Zabawki / Zabawki sportowe / Zabawki fitness / Obręcze hula-hoop'),
(3216, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Koła rowerowe'),
(3217, 'Sprzęt sportowy / Fitness / Podnoszenie ciężarów / Akcesoria do atlasów i ławek treningowych'),
(3218, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wspinaczka / Uprzęże wspinaczkowe'),
(3219, 'Sprzęt sportowy / Gry towarzyskie / Gra w kręgle / Kule do kręgli'),
(3220, 'Elektronika / Płyty drukowane i komponenty / Elementy pasywne obwodów / Kondensatory'),
(3221, 'Dom i ogród / Ozdoby / Kalkomanie na ściany i okna'),
(3222, 'Sprzęt sportowy / Gry towarzyskie / Bilard / Akcesoria do kijów bilardowych'),
(3224, 'Sprzęt / Narzędzia / Piły / Przenośne piły tarczowe'),
(3226, 'Gry i zabawki / Zabawki / Zabawki sportowe / Golf dla dzieci'),
(3227, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Gry na świeżym powietrzu / Dysk golf / Kosze do dysk golfa'),
(3229, 'Gry i zabawki / Zabawki / Zabawa przez imitację'),
(3230, 'Biznes i przemysł / Branża medyczna / Sprzęt medyczny / Automatyczne defibrylatory zewnętrzne'),
(3232, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Części układu wydechowego do jednostek pływających / Kolektory do jednostek pływających'),
(3234, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Paintball i ASG / Paintball / Markery paintballowe'),
(3235, 'Sprzęt sportowy / Sport / Boks i sztuki walki / Ochraniacze do boksu i sztuk walki / Rękawice bokserskie'),
(3237, 'Zwierzęta i artykuły dla zwierząt / Żywe zwierzęta'),
(3238, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla rybek / Akwaria'),
(3239, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do ekspresów do kawy i espresso / Dzbanki do kawy'),
(3240, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do szlifowania / Papier ścierny i gąbki ścierne'),
(3241, 'Sprzęt sportowy / Sport / Łyżwiarstwo figurowe i hokej / Części i akcesoria do łyżew / Napinacze do sznurowadeł'),
(3242, 'Elektronika / Audio / Sprzęt sceniczny / Transmitery bezprzewodowe'),
(3243, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Liczniki rowerowe'),
(3244, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Paintball i ASG / Paintball / Akcesoria do markerów paintballowych / Butle z powietrzem do gry w paintballa'),
(3245, 'Sprzęt sportowy / Gry towarzyskie / Air hockey / Stoły do gry w air hockey'),
(3246, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Odzież i akcesoria dla rowerzystów / Rękawiczki rowerowe'),
(3247, 'Sprzęt sportowy / Sport / Futbol amerykański / Ochraniacze do futbolu amerykańskiego / Akcesoria do kasków do futbolu amerykańskiego / Pasek podbródkowy'),
(3248, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Tłuczki do mięsa'),
(3250, 'Sprzęt sportowy / Gry towarzyskie / Gra w rzutki / Części lotek / Groty do lotek'),
(3252, 'Gry i zabawki / Zabawki / Zabawki muzyczne / Instrumenty dla dzieci'),
(3253, 'Ubrania i akcesoria / Ubrania / Uniformy / Stroje sportowe / Stroje sędziego'),
(3256, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Skrobaczki kuchenne'),
(3257, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Produkty do pielęgnacji koni'),
(3258, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Łyżki do nabierania / Łyżki do melonów'),
(3260, 'Sprzęt sportowy / Gry towarzyskie / Gra w kręgle / Ochraniacze nadgarstków do gry w kręgle'),
(3261, 'Sprzęt sportowy / Sport / Szermierka / Ochraniacze do szermierki'),
(3262, 'Dom i ogród / Ozdoby / Globusy'),
(3263, 'Gry i zabawki / Zabawki / Zabawki latające / Spadochrony zabawkowe'),
(3265, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Sprzęt jeździecki / Palcaty i szpicruty'),
(3266, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wspinaczka / Ochraniacze wspinaczkowe'),
(3268, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Stojaki na naczynia i suszarki do naczyń'),
(3269, 'Sprzęt sportowy / Sport / Taniec / Poręcze baletowe'),
(3270, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Stroiki elektroniczne'),
(3271, 'Sprzęt sportowy / Fitness / Podnoszenie ciężarów / Akcesoria do wolnych ciężarów / Gryfy'),
(3272, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Odzież i akcesoria dla rowerzystów / Pokrowce na buty rowerowe'),
(3273, 'Biznes i przemysł / Handel detaliczny / Obrót środkami pieniężnymi / Rozmieniarki pieniędzy'),
(3276, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na deskorolce'),
(3277, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Części silników do jednostek pływających / Tłoki do jednostek pływających i ich części'),
(3280, 'Sprzęt / Akcesoria do sprzętu / Przechowywanie i porządkowanie narzędzi / Szafy narzędziowe'),
(3281, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do lutownic'),
(3282, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Sporty wodne holowane / Wakeboarding'),
(3283, 'Oprogramowanie / Programy komputerowe / Mapy i programy do nawigacji GPS'),
(3284, 'Żywność, napoje i tytoń / Żywność / Przekąski / Suszone mięso'),
(3285, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Windsurfing / Części do desek windsurfingowych / Maszty windsufringowe'),
(3287, 'Gry i zabawki / Zabawki / Zabawki do budowania / Klocki z zaczepami'),
(3288, 'Gry i zabawki / Zabawki / Zabawa przez imitację / Zabawki do trawnika i ogrodu'),
(3289, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Sporty wodne holowane / Jazda na nartach wodnych / Deski do hydrofoilingu'),
(3291, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Łucznictwo / Części i akcesoria do strzał'),
(3292, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Siodełka'),
(3293, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Schładzacze do wody'),
(3294, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Tłuczki do ziemniaków'),
(3295, 'Sprzęt sportowy / Sport / Tenis / Akcesoria do rakiet tenisowych / Pierścienie wzmacniające do rakiet tenisowych'),
(3296, 'Gry i zabawki / Zabawki / Pojazdy do zabawy / Ciężarówki i maszyny budowlane'),
(3297, 'Sprzęt sportowy / Sport / Boks i sztuki walki / Trening bokserski i sztuk walki / Worki treningowe'),
(3298, 'Gry i zabawki / Zabawki / Zabawa przez imitację / Kuchnie i jedzenie do zabawy'),
(3300, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do frezarek pionowych / Stoły do frezarek pionowych'),
(3301, 'Gry i zabawki / Zabawki / Zabawki optyczne / Kalejdoskopy'),
(3304, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Pływanie / Rękawice do pływania'),
(3305, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Strzelanie do rzutek / Wyrzutnie rzutek'),
(3306, 'Elektronika / Audio / Akcesoria audio / Akcesoria do mikrofonów'),
(3307, 'Dom i ogród / Artykuły gospodarstwa domowego / Podkładki pod dywany'),
(3308, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Części układu sterowania do jednostek pływających / Linki sterownicze do jednostek pływających'),
(3309, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Części układu wydechowego do jednostek pływających / Tłumiki i części tłumików do jednostek pływających'),
(3311, 'Dom i ogród / Trawnik i ogród / Silnikowe narzędzia ogrodowe / Kosiarki / Kosiarki z napędem'),
(3314, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wspinaczka / Odzież i akcesoria do wspinaczki / Kaski wspinaczkowe'),
(3315, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Dokowanie i kotwiczenie'),
(3318, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Oświetlenie samochodowe'),
(3319, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Maszynki do waty cukrowej'),
(3320, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Surfing / Deski surfingowe'),
(3321, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Części silników do jednostek pływających / Elementy sterowania silnikiem do jednostek pływających'),
(3322, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wspinaczka / Ochraniacze dla uprawiających wspinaczkę'),
(3324, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do keyboardów / Pedały forte'),
(3326, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Narzędzia do naprawy pojazdów / Kable rozruchowe'),
(3327, 'Sprzęt sportowy / Gry towarzyskie / Gra w rzutki / Części lotek'),
(3328, 'Elektronika / Akcesoria elektroniczne / Zarządzanie kablami'),
(3329, 'Dom i ogród / Oświetlenie / Żarówki / Żarówki diodowe'),
(3330, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Naczynia do serwowania żywności / Dzbanki i karafki'),
(3331, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Jazda na nartach i snowboardzie / Narty / Narty biegowe'),
(3332, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Łucznictwo / Łuki i kusze / Łuki bloczkowe'),
(3334, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo'),
(3336, 'Sprzęt sportowy / Sport / Hokej na trawie i lacrosse / Części kijów do lacrosse'),
(3337, 'Dom i ogród / Kuchnia i jadalnia / Przechowywanie żywności / Pojemniki i torby na chleb'),
(3339, 'Sprzęt sportowy / Sport / Krykiet / Ochraniacze do krykieta / Rękawice do krykieta'),
(3340, 'Dom i ogród / Trawnik i ogród / Silnikowe narzędzia ogrodowe / Dmuchawy do liści'),
(3341, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Koszyki rowerowe'),
(3343, 'Sprzęt sportowy / Sport / Futbol amerykański / Ochraniacze do futbolu amerykańskiego / Akcesoria do kasków do futbolu amerykańskiego / Wyściółki do kasków do futbolu amerykańskiego'),
(3345, 'Sprzęt sportowy / Gry towarzyskie / Tenis stołowy / Stoły do tenisa stołowego'),
(3347, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Pipety do polewania mięsa'),
(3348, 'Dom i ogród / Akcesoria do sprzętu AGD'),
(3350, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Sporty wodne holowane / Jazda na nartach wodnych / Narty wodne'),
(3352, 'Sprzęt sportowy / Sport / Tenis / Akcesoria do rakiet tenisowych / Tłumiki drgań do rakiet tenisowych'),
(3353, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Sporty wodne holowane / Wakeboarding / Deski do wakeboardingu'),
(3354, 'Sprzęt sportowy / Sport / Cheerleading'),
(3355, 'Dom i ogród / Artykuły gospodarstwa domowego / Termometry domowe'),
(3356, 'Elektronika / Sprzęt do gier zręcznościowych'),
(3358, 'Meble / Krzesła i fotele / Składane krzesła i stołki'),
(3359, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Przybory wędkarskie / Haczyki wędkarskie'),
(3360, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Pływanie / Okulary i maski do pływania'),
(3361, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Zabawy związane z piciem alkoholu / Piwny ping-pong'),
(3362, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Dokowanie i kotwiczenie / Liny do kotwic'),
(3363, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wspinaczka / Przyrządy asekuracyjne'),
(3364, 'Zdrowie i uroda / Opieka zdrowotna / Artykuły dla osób mających trudności z poruszaniem się / Sprzęt ułatwiający poruszanie się / Wózki inwalidzkie'),
(3366, 'Sprzęt sportowy / Sport / Szermierka / Ochraniacze do szermierki / Rękawice szermiercze'),
(3367, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla kotów / Karma dla kotów'),
(3368, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Lusterka rowerowe'),
(3369, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wspinaczka / Taśmy wspinaczkowe'),
(3370, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Sporty wodne holowane / Kneeboarding'),
(3371, 'Gry i zabawki / Zabawki / Zabawki sportowe / Piłka nożna dla dzieci'),
(3373, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Naczynia do serwowania żywności / Cukiernice i dzbanki na śmietankę'),
(3374, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych / Elementy konserwujące i czyszczące do instrumentów strunowych / Kalafonia do smyczków'),
(3375, 'Sprzęt sportowy / Gry towarzyskie / Tenis stołowy / Rakietki do tenisa stołowego');
INSERT INTO `google_kategorie` (`id`, `nazwa`) VALUES
(3376, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Odzież żeglarska i do uprawiania sportów wodnych / Odzież ochronna do pływania'),
(3378, 'Gry i zabawki / Zabawki / Zabawki latające / Szybowce'),
(3379, 'Ubrania i akcesoria / Ubrania / Uniformy / Stroje sportowe / Stroje do softballu'),
(3381, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Szczypce do rozłupywania'),
(3382, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do grilla na zewnątrz / Ruszty do grilla na zewnątrz'),
(3385, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Wyciskarki do czosnku'),
(3387, 'Elektronika / Akcesoria elektroniczne / Nośniki pamięci / Pamięci flash / Karty pamięci flash'),
(3388, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Narzędzia ogrodnicze / Grabie'),
(3389, 'Sprzęt sportowy / Sport / Lekkoatletyka / Oszczepy'),
(3390, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Gry na świeżym powietrzu / Pickleball'),
(3391, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających'),
(3392, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych / Akcesoria do gitar / Środki nawilżające do gitar'),
(3395, 'Pojazdy i części / Pojazdy / Statki powietrzne'),
(3400, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Układy paliwowe do jednostek pływających'),
(3402, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do nawilżaczy / Filtry do nawilżaczy'),
(3403, 'Sprzęt sportowy / Sport / Tenis / Akcesoria do rakiet tenisowych / Owijki rakiet tenisowych'),
(3405, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Gry na świeżym powietrzu / Gry na trawie'),
(3406, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Pływanie łodzią i rafting / Łodzie wiosłowe'),
(3407, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja włosów / Narzędzia do stylizacji włosów / Prostownice do włosów'),
(3408, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Paintball i ASG / Paintball / Granaty paintballowe'),
(3410, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do oczyszczaczy powietrza'),
(3411, 'Sprzęt sportowy / Sport / Boks i sztuki walki / Części ringów bokserskich'),
(3412, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych / Akcesoria do gitar / Przetworniki do gitar elektrycznych'),
(3413, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Windsurfing / Żagle windsurfingowe'),
(3414, 'Ubrania i akcesoria / Ubrania / Uniformy / Mundury służb bezpieczeństwa'),
(3415, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Układy paliwowe do jednostek pływających / Przewody paliwowe do jednostek pływających'),
(3416, 'Elektronika / Płyty drukowane i komponenty / Obwody drukowane / Płytki rozwojowe'),
(3418, 'Sprzęt sportowy / Sport / Hokej na trawie i lacrosse / Części kijów do lacrosse / Kosze kijów do lacrosse'),
(3419, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Skrobaczki kuchenne / Skrobaczki do ciast'),
(3421, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Noże do pizzy'),
(3422, 'Elektronika / Akcesoria elektroniczne / Akcesoria do nośników pamięci'),
(3423, 'Sprzęt sportowy / Sport / Hokej na trawie i lacrosse / Części kijów do lacrosse / Rękojeści kijów do lacrosse'),
(3424, 'Elektronika / Płyty drukowane i komponenty / Elementy pasywne obwodów / Rezystory'),
(3425, 'Elektronika / Sieci / Serwery druku'),
(3426, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Rząd koński / Uzdy'),
(3427, 'Dom i ogród / Kuchnia i jadalnia / Naczynia barowe / Shakery i przybory do drinków / Otwieracze do butelek'),
(3428, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Przechowywanie rowerów'),
(3430, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Pędzle spożywcze'),
(3435, 'Dom i ogród / Kuchnia i jadalnia / Transportery do żywności i napojów / Manierki'),
(3436, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Osłony i pokrowce na pojazdy'),
(3437, 'Artykuły dla dorosłych / Broń / Baty'),
(3438, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Paintball i ASG / Paintball / Kulki paintballowe'),
(3439, 'Ubrania i akcesoria / Ubrania / Uniformy / Stroje sportowe / Stroje do koszykówki'),
(3440, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Zabawy związane z piciem alkoholu / Piwny ping-pong / Stoły do piwnego ping-ponga'),
(3441, 'Gry i zabawki / Zabawki / Zabawki do jeżdżenia / Bujaki i zabawki sprężynowe'),
(3442, 'Sprzęt sportowy / Sport / Futbol amerykański / Rękawice do futbolu amerykańskiego'),
(3443, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do zmywarek'),
(3444, 'Gry i zabawki / Zabawki / Pojazdy do zabawy / Samoloty do zabawy'),
(3445, 'Sprzęt sportowy / Sport / Lekkoatletyka / Poprzeczki do skoku wzwyż'),
(3446, 'Żywność, napoje i tytoń / Żywność / Przekąski / Precle'),
(3450, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do ekspresów do kawy i espresso / Filtry do kawy'),
(3452, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Dokowanie i kotwiczenie / Łańcuchy do kotwic'),
(3454, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wspinaczka / Urządzenia zaciskowe'),
(3455, 'Ubrania i akcesoria / Ubrania / Ubrania sportowe / Stroje rowerowe / Koszulki kolarskie'),
(3456, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do prania'),
(3457, 'Artykuły biurowe / Artykuły różne / Artykuły papiernicze / Papiery listowe'),
(3459, 'Dzieci i niemowlęta / Zabawki dla niemowląt / Zabawki do ciągnięcia i pchania'),
(3460, 'Gry i zabawki / Zabawki / Zabawki latające / Latawce'),
(3461, 'Elektronika / Akcesoria elektroniczne / Kable / Kable KVM'),
(3463, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Części silników do jednostek pływających / Gaźniki i części gaźników do jednostek pływających'),
(3465, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do perkusji'),
(3466, 'Gry i zabawki / Zabawki / Zabawki ruchowe / Bączki'),
(3467, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Wałki do ciasta'),
(3469, 'Sprzęt sportowy / Gry towarzyskie / Bilard / Oświetlenie stołów bilardowych'),
(3470, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do strugarek poprzecznych'),
(3472, 'Pojazdy i części / Akcesoria i części do pojazdów / Przewóz i przechowywanie bagażu / Bagażniki samochodowe'),
(3473, 'Dom i ogród / Ozdoby / Pozytywki'),
(3474, 'Gry i zabawki / Zabawki / Pojazdy do zabawy / Wyścigówki i tory'),
(3475, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Miarki i łyżki do odmierzania'),
(3476, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Pływanie łodzią i rafting / Pontony do raftingu'),
(3477, 'Biznes i przemysł / Branża medyczna / Sprzęt medyczny'),
(3478, 'Sprzęt sportowy / Sport / Lekkoatletyka / Dyski'),
(3479, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Organizery kuchenne / Regały na napoje'),
(3480, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Dokowanie i kotwiczenie / Kabestany'),
(3484, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Gry na świeżym powietrzu / Dysk golf'),
(3487, 'Sprzęt sportowy / Sport / Rugby / Rękawice do rugby'),
(3488, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na deskorolce / Stroje ochronne dla deskorolkarzy / Ochraniacze dla deskorolkarzy'),
(3489, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do wypieku chleba'),
(3491, 'Dom i ogród / Trawnik i ogród / Nawadnianie / Akcesoria do zraszaczy / Zawory zraszaczy'),
(3492, 'Sprzęt sportowy / Sport / Futbol amerykański / Słupki do futbolu amerykańskiego'),
(3494, 'Sprzęt / Narzędzia / Piły / Pilarki szablowe'),
(3495, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Narzędzia kempingowe / Noże myśliwskie i survivalowe'),
(3497, 'Sprzęt sportowy / Sport / Futbol amerykański / Ochraniacze do futbolu amerykańskiego / Ochraniacze szyi do futbolu amerykańskiego'),
(3498, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Zastawy obiadowe / Miski'),
(3500, 'Gry i zabawki / Zabawki / Zabawki edukacyjne / Zabawki naukowe'),
(3501, 'Sprzęt / Narzędzia / Świdry ziemne'),
(3502, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych / Akcesoria do gitar'),
(3505, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Łucznictwo / Łuki i kusze / Kusze'),
(3506, 'Gry i zabawki / Zabawki / Pojazdy do zabawy / Helikoptery'),
(3507, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Części silników do jednostek pływających / Blokady napędu do jednostek pływających'),
(3508, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Chemiczne ogrzewacze dłoni'),
(3509, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż / Sztuczne tatuaże'),
(3510, 'Sprzęt sportowy / Sport / Futbol amerykański / Ochraniacze do futbolu amerykańskiego / Spodenki do futbolu amerykańskiego'),
(3512, 'Zdrowie i uroda / Opieka zdrowotna / Artykuły dla osób mających trudności z poruszaniem się / Sprzęt ułatwiający poruszanie się / Skutery inwalidzkie'),
(3515, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Sportowe artykuły kolekcjonerskie / Akcesoria dla fanów sportu'),
(3516, 'Sprzęt / Narzędzia / Piły / Piły do metali'),
(3517, 'Sprzęt / Narzędzia / Piły / Ukośnice'),
(3518, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wspinaczka / Chwyty wspinaczkowe'),
(3521, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Obieraczki i wydrążacze'),
(3522, 'Dom i ogród / Akcesoria oświetleniowe / Wyłączniki czasowe oświetlenia'),
(3523, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do pakowarek próżniowych'),
(3524, 'Sprzęt sportowy / Gry towarzyskie / Piłkarzyki / Części stołów do gry w piłkarzyki'),
(3525, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Surfing / Stateczniki do desek surfingowych'),
(3526, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Spieniacze do mleka'),
(3528, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Strzelanie do rzutek / Rzutki strzeleckie'),
(3530, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla psów / Karma dla psów'),
(3531, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Rowery trójkołowe'),
(3532, 'Gry i zabawki / Zabawki / Zabawki zdalnie sterowane / Jednostki pływające zdalnie sterowane'),
(3533, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Łucznictwo / Strzały'),
(3534, 'Gry i zabawki / Zabawki / Zabawki ruchowe / Kulki'),
(3535, 'Sprzęt sportowy / Gry towarzyskie / Gra w kręgle / Rękawice do gry w kręgle'),
(3536, 'Sprzęt sportowy / Sport / Hokej na trawie i lacrosse / Piłki do lacrosse'),
(3538, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Moskitiery'),
(3539, 'Elektronika / Komputery / Palmtopy / Czytniki e-booków'),
(3540, 'Pojazdy i części / Pojazdy / Jednostki pływające'),
(3541, 'Elektronika / Akcesoria elektroniczne / Kable / Kable telefoniczne'),
(3542, 'Sprzęt sportowy / Fitness / Podnoszenie ciężarów / Atlasy treningowe i stelaże na ciężary'),
(3543, 'Sprzęt sportowy / Sport / Krykiet / Ochraniacze do krykieta / Kaski do krykieta'),
(3544, 'Sprzęt sportowy / Sport / Baseball i softball / Bazy baseballowe i softballowe'),
(3546, 'Sprzęt sportowy / Gry towarzyskie / Tenis stołowy / Roboty treningowe do tenisa stołowego'),
(3547, 'Sprzęt sportowy / Gry towarzyskie / Bilard / Części i akcesoria do stołów bilardowych / Sukno bilardowe'),
(3548, 'Sprzęt sportowy / Gry towarzyskie / Air hockey / Części stołu do gry w air hockey'),
(3549, 'Pojazdy i części / Pojazdy / Pojazdy silnikowe / Skutery śnieżne'),
(3550, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Jazda na nartach i snowboardzie / Gogle narciarskie i snowboardowe'),
(3551, 'Gry i zabawki / Zabawki / Pojazdy do zabawy / Samochody'),
(3552, 'Gry i zabawki / Zabawki / Zabawki sportowe / Koszykówka dla dzieci'),
(3553, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Zastawy obiadowe / Talerze'),
(3554, 'Gry i zabawki / Zabawki / Zabawki zdalnie sterowane / Helikoptery zdalnie sterowane'),
(3556, 'Gry i zabawki / Zabawki ogrodowe / Zabawki do wody / Stoliki wodne'),
(3558, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Torby i kufry rowerowe'),
(3559, 'Sprzęt sportowy / Gry towarzyskie / Gra w rzutki / Tarcze do darta'),
(3561, 'Dom i ogród / Artykuły gospodarstwa domowego / Przechowywanie i utrzymanie porządku / Torby do przechowywania'),
(3562, 'Gry i zabawki / Zabawki / Zabawki optyczne'),
(3565, 'Sprzęt sportowy / Sport / Tenis / Opakowania na piłki tenisowe'),
(3566, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Części silników do jednostek pływających / Mocowania silnika do jednostek pływających'),
(3568, 'Dom i ogród / Trawnik i ogród / Nawadnianie'),
(3572, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Dokowanie i kotwiczenie / Knagi'),
(3573, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do klimatyzatorów / Filtry do klimatyzatorów'),
(3574, 'Sprzęt sportowy / Gry towarzyskie / Bilard / Części i akcesoria do stołów bilardowych / Łuzy bilardowe'),
(3575, 'Sprzęt sportowy / Sport / Piłka wodna / Czepki do piłki wodnej'),
(3576, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Sportowe artykuły kolekcjonerskie / Akcesoria dla fanów sportu / Akcesoria dla fanów piłki nożnej'),
(3577, 'Sztuka i rozrywka / Hobby i sztuki piękne / Warzenie piwa i pędzenie wina'),
(3578, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Golf / Chorągiewki golfowe'),
(3579, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Surfing / Pokrowce na deski surfingowe'),
(3580, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Urządzenia wejściowe / Czytniki kart pamięci'),
(3582, 'Sprzęt / Narzędzia / Piły / Piły taśmowe'),
(3583, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Myślistwo / Wabiki na zwierzynę / Wabiki myśliwskie wizualne'),
(3584, 'Gry i zabawki / Zabawki / Lalki, zestawy i figurki / Akcesoria do lalek i figurek akcji'),
(3586, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Szczypce do rozłupywania / Szczypce do rozłupywania homarów i krabów'),
(3588, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do keyboardów / Stojaki na keyboardy'),
(3589, 'Gry i zabawki / Zabawki / Pojazdy do zabawy / Statki kosmiczne'),
(3590, 'Gry i zabawki / Zabawki / Pojazdy do zabawy / Motocykle do zabawy'),
(3591, 'Dom i ogród / Kuchnia i jadalnia / Przechowywanie żywności / Torebki do przechowywania żywności'),
(3594, 'Sprzęt / Narzędzia / Piły / Piły ręczne'),
(3595, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Pływanie / Ósemki do pływania'),
(3596, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Pływanie / Maszyny do pływania'),
(3597, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Trzepaczki'),
(3598, 'Ubrania i akcesoria / Ubrania / Uniformy / Stroje sportowe'),
(3599, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Autografy'),
(3601, 'Gry i zabawki / Zabawki / Zabawki zdalnie sterowane / Samochody i ciężarówki zdalnie sterowane'),
(3602, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Barometry'),
(3603, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Przybory wędkarskie / Przynęty'),
(3606, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Części silników do jednostek pływających'),
(3609, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do lutownic / Groty do lutownic'),
(3610, 'Dom i ogród / Trawnik i ogród / Silnikowe narzędzia ogrodowe / Piły łańcuchowe'),
(3614, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Podbieraki'),
(3615, 'Sprzęt sportowy / Sport / Łyżwiarstwo figurowe i hokej / Ochraniacze hokejowe / Spodnie hokejowe'),
(3616, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Narzędzia ogrodnicze / Taczki'),
(3617, 'Gry i zabawki / Zabawki / Zabawki do budowania / Klocki drewniane'),
(3618, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe'),
(3619, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Części układu wydechowego do jednostek pływających'),
(3620, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Chochle'),
(3621, 'Sprzęt sportowy / Sport / Futbol amerykański / Ochraniacze do futbolu amerykańskiego / Naramienniki do futbolu amerykańskiego'),
(3622, 'Sprzęt sportowy / Sport / Szermierka / Broń do szermierki'),
(3623, 'Sprzęt sportowy / Sport / Łyżwiarstwo figurowe i hokej / Części i akcesoria do łyżew / Ostrzałki do łyżew'),
(3624, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Windsurfing / Części do desek windsurfingowych'),
(3625, 'Gry i zabawki / Zabawki / Zabawki-roboty'),
(3626, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na deskorolce / Rampy'),
(3627, 'Gry i zabawki / Zabawki / Broń zabawkowa'),
(3629, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do lutownic / Stojaki lutownicze'),
(3631, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Kółka boczne do roweru'),
(3632, 'Elektronika / Płyty drukowane i komponenty / Półprzewodniki / Diody'),
(3633, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Skrobaczki kuchenne / Skrobaczki do grilla'),
(3634, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów trójkołowych'),
(3635, 'Elektronika / Płyty drukowane i komponenty / Elementy pasywne obwodów'),
(3636, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Sporty wodne holowane / Liny do holowania łodzi'),
(3637, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na deskorolce / Części do deskorolek / Koła do deskorolek'),
(3638, 'Sprzęt sportowy / Sport / Tenis / Akcesoria do rakiet tenisowych / Torby na rakiety tenisowe'),
(3639, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Ramy'),
(3640, 'Sprzęt sportowy / Fitness / Joga i pilates / Maty do jogi i pilatesu'),
(3641, 'Sprzęt sportowy / Gry towarzyskie / Piłkarzyki / Piłki do gry w piłkarzyki'),
(3642, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Golf / Części i akcesoria do kijów golfowych'),
(3644, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Narzędzia ogrodnicze / Łopatki ogrodowe'),
(3646, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych / Akcesoria do gitar / Stojaki na gitary'),
(3648, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Układy paliwowe do jednostek pływających / Zbiorniki paliwa do jednostek pływających i ich części'),
(3649, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Surfing / Smycze dla surferów'),
(3650, 'Sprzęt / Akcesoria do narzędzi'),
(3651, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Przybory wędkarskie / Ciężarki wędkarskie'),
(3652, 'Sprzęt sportowy / Sport / Boks i sztuki walki / Ringi bokserskie'),
(3654, 'Sprzęt sportowy / Fitness / Podnoszenie ciężarów / Pasy kulturystyczne'),
(3655, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Dokowanie i kotwiczenie / Haki do łodzi'),
(3656, 'Sprzęt sportowy / Sport / Futbol amerykański / Podstawki do wykopywania piłki'),
(3658, 'Sprzęt sportowy / Sport / Tenis / Akcesoria do rakiet tenisowych'),
(3659, 'Gry i zabawki / Zabawki / Zabawa przez imitację / Imitacje urządzeń elektronicznych'),
(3661, 'Dzieci i niemowlęta / Zabawki dla niemowląt / Zabawki z alfabetem'),
(3663, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Części układu sterowania do jednostek pływających / Kierownice do jednostek pływających'),
(3665, 'Gry i zabawki / Zabawki / Zabawki sportowe / Kręgle dla dzieci'),
(3666, 'Artykuły dla dorosłych / Broń / Miecze'),
(3667, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do oczyszczaczy powietrza / Filtry do oczyszczaczy powietrza'),
(3668, 'Sprzęt sportowy / Sport / Baseball i softball / Odzież ochronna do baseballa i softballa / Kaski dla pałkarzy'),
(3669, 'Sprzęt sportowy / Gry towarzyskie / Gra w kręgle / Kręgle'),
(3670, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na deskorolce / Części do deskorolek'),
(3671, 'Sprzęt sportowy / Sport / Baseball i softball / Piłki do gry w softball'),
(3672, 'Elektronika / Akcesoria elektroniczne / Akcesoria do nośników pamięci / Futerały na nośniki pamięci'),
(3673, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do frezarek pionowych / Frezy do frezarek pionowych'),
(3674, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Myślistwo / Pułapki na zwierzynę'),
(3675, 'Gry i zabawki / Zabawki / Zabawki sportowe / Bumerangi'),
(3676, 'Elektronika / Sprzęt do gier zręcznościowych / Akcesoria do automatów do gier zręcznościowych'),
(3677, 'Gry i zabawki / Zabawki / Zabawki zdalnie sterowane / Samoloty zdalnie sterowane'),
(3678, 'Sprzęt sportowy / Sport / Piłka wodna / Bramki do piłki wodnej'),
(3679, 'Sprzęt sportowy / Sport / Baseball i softball / Kije do gry w softball'),
(3680, 'Gry i zabawki / Zabawki / Zabawa przez imitację / Banki i pieniądze do zabawy'),
(3681, 'Elektronika / Sprzęt do gier zręcznościowych / Automaty do skee-ball'),
(3682, 'Żywność, napoje i tytoń / Wyroby tytoniowe / Cygara'),
(3683, 'Ubrania i akcesoria / Ubrania / Uniformy / Stroje sportowe / Stroje cheerleaderek'),
(3684, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do grilla na zewnątrz'),
(3685, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Szczypce do rozłupywania / Dziadki do orzechów'),
(3686, 'Dom i ogród / Ozdoby / Zapachy do domu / Kadzidełka'),
(3688, 'Zdrowie i uroda / Opieka zdrowotna / Akcesoria do monitorów funkcji życiowych / Akcesoria do glukometrów'),
(3689, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Gry na świeżym powietrzu / Shuffleboard / Kije do gry w shuffleboard'),
(3690, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Paintball i ASG / Paintball / Akcesoria do markerów paintballowych / Lufy do markerów paintballowych'),
(3691, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria i kosmetyki do kąpieli / Mydła dezynfekujące do rąk'),
(3692, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Garncarstwo i rzeźbiarstwo / Glina i ciasto do modelowania'),
(3694, 'Artykuły dla dorosłych / Broń / Gwiazdki do rzucania'),
(3695, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Meble kempingowe / Materace dmuchane'),
(3696, 'Dom i ogród / Ozdoby / Zegary / Zegary stojące i wahadłowe'),
(3697, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Szablony i formy / Wzory do szycia'),
(3698, 'Sprzęt sportowy / Gry towarzyskie / Gra w kręgle / Pokrowce na kule do kręgli'),
(3699, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Sztućce / Pałeczki'),
(3702, 'Elektronika / Płyty drukowane i komponenty'),
(3703, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Naczynia do serwowania żywności / Sosjerki'),
(3706, 'Sprzęt / Narzędzia / Piły / Piły stołowe'),
(3707, 'Sprzęt sportowy / Sport / Szermierka / Ochraniacze do szermierki / Maski do szermierki'),
(3708, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Łyżki do nabierania / Szufelki do lodu'),
(3712, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Pamięci komputerowe / Dyski USB Flash'),
(3713, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Minutniki kuchenne'),
(3714, 'Sprzęt sportowy / Sport / Racquetball i squash / Rakiety do raquetballa'),
(3715, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Łucznictwo / Łuki i kusze / Łuki strzeleckie'),
(3717, 'Sprzęt sportowy / Sport / Boks i sztuki walki / Pasy do sztuk walki'),
(3718, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Dokowanie i kotwiczenie / Drabiny do łodzi'),
(3719, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Błotniki rowerowe'),
(3720, 'Sprzęt sportowy / Gry towarzyskie / Bilard / Akcesoria do kijów bilardowych / Stojaki na kije bilardowe'),
(3722, 'Dom i ogród / Artykuły gospodarstwa domowego / Przechowywanie i utrzymanie porządku / Przechowywanie ubrań / Pudełka na ładowarki'),
(3723, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Dozowniki spożywcze'),
(3724, 'Ubrania i akcesoria / Ubrania / Uniformy / Stroje sportowe / Stroje do krykieta'),
(3725, 'Sprzęt / Narzędzia / Piły / Wyrzynarki'),
(3727, 'Elektronika / Audio / Sprzęt sceniczny'),
(3729, 'Ubrania i akcesoria / Ubrania / Ubrania sportowe / Stroje rowerowe / Getry rowerowe'),
(3730, 'Dom i ogród / Trawnik i ogród / Silnikowe narzędzia ogrodowe / Kosiarki / Kosiarki pchane'),
(3731, 'Gry i zabawki / Zabawki / Zabawki artystyczne i malarskie'),
(3733, 'Gry i zabawki / Zabawki / Zabawki ruchowe / Piłki do skakania'),
(3735, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Zabawy związane z piciem alkoholu'),
(3738, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Kijki trekkingowe'),
(3740, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Części układu hamulcowego roweru'),
(3741, 'Żywność, napoje i tytoń / Wyroby tytoniowe / Tytoń sypki'),
(3742, 'Elektronika / Sieci / Sprzęt do bezpieczeństwa sieciowego i firewalle'),
(3743, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Części silników do jednostek pływających / Części układu zapłonowego do jednostek pływających'),
(3744, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do frezarek pionowych'),
(3745, 'Sprzęt / Narzędzia / Pilniki narzędziowe'),
(3746, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wspinaczka / Karabinki'),
(3747, 'Sprzęt sportowy / Sport / Baseball i softball / Rękawice dla pałkarzy'),
(3748, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Myślistwo / Ambony'),
(3749, 'Gry i zabawki / Gry / Zestawy bingo'),
(3750, 'Dom i ogród / Kuchnia i jadalnia / Przechowywanie żywności / Zawijanie żywności / Folia spożywcza'),
(3751, 'Gry i zabawki / Zabawki / Zabawa przez imitację / Narzędzia do zabawy'),
(3754, 'Sprzęt sportowy / Gry towarzyskie / Bilard / Części i akcesoria do stołów bilardowych / Szczotki do stołów bilardowych'),
(3755, 'Sprzęt sportowy / Gry towarzyskie / Bilard / Rękawice do gry w bilarda'),
(3756, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Myślistwo / Wabiki na zwierzynę / Wabiki myśliwskie dźwiękowe'),
(3757, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Łucznictwo / Kołczany'),
(3761, 'Sprzęt sportowy / Sport / Rugby / Piłki do rugby'),
(3762, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Surfing / Nakładki na deski surfingowe'),
(3764, 'Elektronika / Akcesoria elektroniczne / Zarządzanie kablami / Spinacze do kabli'),
(3766, 'Sprzęt sportowy / Gry towarzyskie / Gra w rzutki / Części lotek / Piórka do lotek'),
(3768, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Łopaty do pieca'),
(3769, 'Elektronika / Komputery / Palmtopy / Urządzenia PDA'),
(3770, 'Sprzęt sportowy / Sport / Lekkoatletyka / Kule do pchania'),
(3772, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Golf / Artykuły treningowe do golfa'),
(3773, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Łucznictwo / Ochraniacze na ramię'),
(3774, 'Sprzęt sportowy / Sport / Gimnastyka / Kółka gimnastyczne'),
(3775, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych / Akcesoria do gitar / Przetworniki do gitar akustycznych'),
(3776, 'Gry i zabawki / Zabawki / Zabawki sportowe / Baseball dla dzieci'),
(3777, 'Zdrowie i uroda / Opieka zdrowotna / Pudełka na tabletki'),
(3778, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Sakwy i torby rowerowe'),
(3779, 'Sprzęt sportowy / Sport / Gimnastyka / Woltyżerka'),
(3780, 'Dom i ogród / Trawnik i ogród / Nawadnianie / Akcesoria do zraszaczy'),
(3781, 'Elektronika / Akcesoria do GPS-ów / Futerały na GPS-y'),
(3782, 'Gry i zabawki / Zabawki / Zabawki optyczne / Pryzmaty'),
(3783, 'Sprzęt sportowy / Sport / Baseball i softball / Piłki baseballowe'),
(3785, 'Sprzęt sportowy / Sport / Hokej na trawie i lacrosse / Części kijów do lacrosse / Naciągi i linki do rakiet do lacrosse'),
(3787, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Gry na świeżym powietrzu / Shuffleboard'),
(3788, 'Sprzęt sportowy / Gry towarzyskie / Tenis stołowy / Siatki i słupki do tenisa stołowego'),
(3789, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Paralotniarstwo i spadochroniarstwo'),
(3790, 'Sprzęt sportowy / Sport / Baseball i softball / Kije baseballowe'),
(3791, 'Sprzęt sportowy / Sport / Łyżwiarstwo figurowe i hokej / Części i akcesoria do łyżew'),
(3792, 'Gry i zabawki / Zabawki / Pojazdy do zabawy / Łodzie'),
(3793, 'Gry i zabawki / Gry'),
(3794, 'Sprzęt sportowy / Sport / Piłka wodna / Piłki do piłki wodnej'),
(3797, 'Sprzęt / Materiały elektryczne / Osłony i izolacje kabli / Koszulki termokurczliwe'),
(3798, 'Dom i ogród / Trawnik i ogród / Silnikowe narzędzia ogrodowe'),
(3800, 'Dom i ogród / Kuchnia i jadalnia / Transportery do żywności i napojów / Termosy'),
(3801, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Surfing / Wosk do desek surfingowych'),
(3802, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Naczynia do serwowania żywności / Półmiski'),
(3803, 'Biznes i przemysł / Handel detaliczny / Manekiny'),
(3805, 'Gry i zabawki / Zabawki / Zabawki do budowania / Zestawy do budowania'),
(3806, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Części silników do jednostek pływających / Śruby napędowe do jednostek pływających'),
(3807, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Pływanie / Czepki pływackie'),
(3808, 'Sprzęt sportowy / Sport / Gimnastyka / Ochraniacze do gimnastyki'),
(3809, 'Dom i ogród / Kuchnia i jadalnia / Transportery do żywności i napojów / Butelki na wodę'),
(3810, 'Sprzęt sportowy / Fitness / Joga i pilates / Sprzęt do pilatesu'),
(3811, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Przyczepki rowerowe'),
(3812, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Lakiery samochodowe'),
(3815, 'Sprzęt sportowy / Sport / Krykiet / Kije do krykieta'),
(3817, 'Sprzęt sportowy / Sport / Hokej na trawie i lacrosse / Kije do lacrosse'),
(3819, 'Dom i ogród / Bezpieczeństwo domu i biura / Sejfy'),
(3821, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Sprzęt jeździecki / Kaski jeździeckie'),
(3825, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wspinaczka / Liny wspinaczkowe'),
(3827, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Pompki rowerowe'),
(3828, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Narzędzia ogrodnicze / Roztrząsacze'),
(3829, 'Sprzęt sportowy / Sport / Koszykówka / Części i akcesoria do koszy / Siatki do koszykówki'),
(3831, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Organizery kuchenne / Wkłady na sztućce'),
(3833, 'Artykuły dla dorosłych / Broń / Kastety'),
(3835, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Kratki pod gorące naczynia'),
(3838, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do ekspresów do kawy i espresso / Portafiltry'),
(3839, 'Sprzęt sportowy / Gry towarzyskie / Gra w rzutki / Rzutki'),
(3840, 'Dom i ogród / Ozdoby / Zegary / Zegary naścienne'),
(3841, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Narzędzia ogrodnicze / Sekatory ogrodnicze'),
(3844, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Sztućce / Noże stołowe'),
(3845, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Organizery kuchenne / Wieszaki na garnki'),
(3846, 'Dom i ogród / Artykuły gospodarstwa domowego / Produkty papierowe do gospodarstwa domowego / Serwetki papierowe'),
(3847, 'Sprzęt sportowy / Gry towarzyskie / Piłkarzyki / Stoły do gry w piłkarzyki'),
(3848, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do lodówek'),
(3849, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wspinaczka / Ekspresy'),
(3850, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Foremki do ciastek'),
(3852, 'Ubrania i akcesoria / Ubrania / Uniformy / Stroje sportowe / Stroje do wrestlingu'),
(3855, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do grilla na zewnątrz / Pokrywy do grilla na zewnątrz'),
(3858, 'Sprzęt sportowy / Fitness / Podnoszenie ciężarów / Rękawice i uchwyty do podnoszenia ciężarów'),
(3859, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Przybory wędkarskie / Przynęty pływające'),
(3860, 'Dzieci i niemowlęta / Zabawki dla niemowląt / Zabawki do sortowania i układania'),
(3862, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do nawilżaczy'),
(3864, 'Sprzęt sportowy / Sport / Lekkoatletyka / Zeskoki do skoku wzwyż'),
(3865, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Sportowe artykuły kolekcjonerskie'),
(3866, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Konserwacja i pielęgnacja jednostek pływających / Środki do czyszczenia jednostek pływających'),
(3867, 'Gry i zabawki / Łamigłówki'),
(3868, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Trenażery rowerowe'),
(3869, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na deskorolce / Części do deskorolek / Decki do deskorolek'),
(3870, 'Sprzęt sportowy / Sport / Krykiet / Piłki do krykieta'),
(3871, 'Artykuły biurowe / Artykuły różne / Artykuły papiernicze / Pocztówki'),
(3873, 'Dom i ogród / Bezpieczeństwo domu i biura / Systemy alarmów domowych'),
(3874, 'Gry i zabawki / Zabawki / Zabawki ruchowe / Zabawki do dmuchania baniek'),
(3877, 'Sprzęt sportowy / Sport / Sprzęt ogólnosportowy / Megafony sportowe'),
(3878, 'Sprzęt sportowy / Sport / Lekkoatletyka / Pałeczki sztafetowe'),
(3879, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Dzwonki i klaksony rowerowe'),
(3880, 'Sprzęt sportowy / Sport / Lekkoatletyka / Młoty do rzucania'),
(3881, 'Sprzęt sportowy / Sport / Rugby / Słupki do rugby'),
(3882, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych / Akcesoria do gitar / Futerały i pokrowce na gitary'),
(3883, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Łucznictwo / Tarcze łucznicze'),
(3885, 'Sztuka i rozrywka / Hobby i sztuki piękne / Modelarstwo / Modele rakiet'),
(3888, 'Ubrania i akcesoria / Ubrania / Uniformy / Stroje sportowe / Stroje do futbolu amerykańskiego'),
(3889, 'Elektronika / Płyty drukowane i komponenty / Prototypowanie obwodów'),
(3890, 'Dom i ogród / Ozdoby / Zegary'),
(3892, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Układy paliwowe do jednostek pływających / Pompy paliwa do jednostek pływających'),
(3893, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Reklamy retro'),
(3894, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Windsurfing / Deski windsurfingowe'),
(3895, 'Elektronika / Akcesoria do GPS-ów'),
(3898, 'Dom i ogród / Ozdoby / Zapachy do domu / Odświeżacze powietrza'),
(3899, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Dokowanie i kotwiczenie / Trapy'),
(3900, 'Sprzęt sportowy / Gry towarzyskie / Tenis stołowy / Akcesoria do rakietek do tenisa stołowego'),
(3905, 'Zdrowie i uroda / Opieka zdrowotna / Akcesoria do monitorów funkcji życiowych / Akcesoria do glukometrów / Paski do glukometrów'),
(3906, 'Sprzęt sportowy / Sport / Tenis / Rakiety tenisowe'),
(3907, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Gry na świeżym powietrzu / Badminton / Lotki'),
(3908, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Windsurfing / Części do desek windsurfingowych / Stateczniki windsurfingowe'),
(3909, 'Gry i zabawki / Zabawki / Zabawki sportowe / Zośki'),
(3910, 'Sprzęt sportowy / Gry towarzyskie / Bilard / Kije bilardowe i podpórki'),
(3911, 'Gry i zabawki / Zabawki / Zabawki do kąpieli'),
(3912, 'Elektronika / Audio / Akcesoria audio / Statywy do mikrofonów'),
(3913, 'Ubrania i akcesoria / Akcesoria do ubrań / Sprzączki do pasków'),
(3914, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Osuszacze do sałaty'),
(3916, 'Żywność, napoje i tytoń / Wyroby tytoniowe / Tytoń do żucia'),
(3919, 'Sprzęt / Akcesoria do sprzętu / Przechowywanie i porządkowanie narzędzi / Stoły robocze'),
(3922, 'Sprzęt sportowy / Sport / Tenis / Akcesoria do rakiet tenisowych / Naciągi rakiet tenisowych'),
(3923, 'Artykuły religijne i dewocjonalia / Artykuły religijne / Różańce'),
(3924, 'Artykuły dla dorosłych / Broń / Pałki do sztuk walki'),
(3925, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Paintball i ASG / ASG / Kulki ASG'),
(3928, 'Gry i zabawki / Zabawki / Zabawki edukacyjne / Zestawy do zbierania owadów'),
(3929, 'Gry i zabawki / Zabawki / Zabawki ruchowe / Jojo'),
(3931, 'Pojazdy i części / Pojazdy / Pojazdy silnikowe / Wózki golfowe'),
(3932, 'Sprzęt / Narzędzia / Motyki i kilofy'),
(3937, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Narzędzia kempingowe'),
(3938, 'Sprzęt sportowy / Fitness / Piłki lekarskie'),
(3939, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Sztućce / Łyżki'),
(3941, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Naczynia do serwowania żywności / Wazy'),
(3943, 'Gry i zabawki / Zabawki / Zabawki sportowe / Hokej dla dzieci'),
(3944, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do wiercenia'),
(3945, 'Elektronika / Audio / Elementy audio / Procesory sygnałowe / Przedwzmacniacze mikrofonowe'),
(3946, 'Elektronika / Sprzęt do gier zręcznościowych / Akcesoria do flipperów'),
(3948, 'Gry i zabawki / Zabawki ogrodowe / Szczudła sprężynowe'),
(3949, 'Elektronika / Płyty drukowane i komponenty / Półprzewodniki / Mikrokontrolery'),
(3950, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Gry na świeżym powietrzu / Badminton / Rakiety do badmintona'),
(3951, 'Ubrania i akcesoria / Ubrania / Ubrania sportowe / Spodnie piłkarskie'),
(3953, 'Sprzęt sportowy / Sport / Cheerleading / Pompony dla cheerleaderek'),
(3954, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do frytownic'),
(3955, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Konserwacja i pielęgnacja jednostek pływających / Politury do jednostek pływających'),
(3956, 'Dom i ogród / Kuchnia i jadalnia / Przechowywanie żywności / Zawijanie żywności / Papier śniadaniowy'),
(3957, 'Sprzęt sportowy / Gry towarzyskie / Gra w rzutki / Podkładki pod tarcze do darta'),
(3958, 'Ubrania i akcesoria / Ubrania / Uniformy / Stroje sportowe / Stroje do hokeja'),
(3961, 'Sprzęt sportowy / Sport / Tenis / Siatki tenisowe'),
(3964, 'Sprzęt sportowy / Gry towarzyskie / Tenis stołowy / Piłki do tenisa stołowego'),
(3965, 'Żywność, napoje i tytoń / Żywność / Dipy i smarowidła / Masła orzechowe'),
(3966, 'Gry i zabawki / Zabawki / Zabawki latające / Rakiety powietrzne i wodne'),
(3968, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Układy paliwowe do jednostek pływających / Wskaźniki poziomu paliwa do jednostek pływających'),
(3970, 'Sprzęt sportowy / Sport / Hokej na trawie i lacrosse / Bramki do lacrosse'),
(3971, 'Sprzęt sportowy / Sport / Sprzęt ogólnosportowy / Magnezja'),
(3973, 'Sprzęt sportowy / Sport / Piłka nożna / Chorągiewki narożne na boisko'),
(3974, 'Sprzęt / Akcesoria do sprzętu / Przechowywanie i porządkowanie narzędzi'),
(3977, 'Pojazdy i części / Akcesoria i części do pojazdów / Akcesoria i części do samolotów'),
(3980, 'Sprzęt / Akcesoria do sprzętu / Przechowywanie i porządkowanie narzędzi / Skrzynki narzędziowe'),
(3982, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Odzież i akcesoria dla rowerzystów'),
(3983, 'Sprzęt sportowy / Sport / Rugby / Sprzęt treningowy do rugby'),
(3985, 'Sprzęt sportowy / Sport / Tenis / Maszyny wyrzucające piłki'),
(3987, 'Sprzęt sportowy / Sport / Lekkoatletyka / Zeskoki do skoku o tyczce'),
(3988, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do ekspresów do kawy i espresso'),
(3991, 'Elektronika / Płyty drukowane i komponenty / Półprzewodniki'),
(3992, 'Dom i ogród / Baseny i spa / Sauny'),
(3993, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Gry na świeżym powietrzu / Dysk golf / Torby do dysk golfa'),
(3994, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Piniaty'),
(3995, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Części układu sterowania do jednostek pływających'),
(3996, 'Sprzęt sportowy / Gry towarzyskie / Shuffleboard stołowy / Proszki do gry w shuffleboard stołowy'),
(3997, 'Sprzęt sportowy / Sport / Lekkoatletyka / Pistolety startowe'),
(3998, 'Sprzęt sportowy / Sport / Futbol amerykański / Sprzęt treningowy do futbolu amerykańskiego'),
(3999, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Moździerze i tłuczki'),
(4000, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Narzędzia ogrodnicze / Kultywatory'),
(4002, 'Sprzęt sportowy / Sport / Racquetball i squash / Rakiety do squasha'),
(4003, 'Ubrania i akcesoria / Ubrania / Uniformy / Stroje sportowe / Stroje do sztuk walki'),
(4004, 'Gry i zabawki / Zabawki / Zabawa przez imitację / Zabawki do sprzątania'),
(4005, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Szczypce kuchenne'),
(4006, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Sportowe artykuły kolekcjonerskie / Akcesoria dla fanów sportu / Akcesoria dla fanów hokeja'),
(4008, 'Sprzęt sportowy / Sport / Boks i sztuki walki / Ochraniacze do boksu i sztuk walki'),
(4009, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Naczynia do serwowania żywności / Tacki'),
(4010, 'Elektronika / Płyty drukowane i komponenty / Prototypowanie obwodów / Płytki prototypowe'),
(4011, 'Gry i zabawki / Łamigłówki / Łamigłówki mechaniczne'),
(4015, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Sztućce / Widelce'),
(4016, 'Elektronika / Akcesoria elektroniczne / Zarządzanie kablami / Tryktraki do kabli'),
(4018, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Rząd koński / Wędzidła'),
(4019, 'Sprzęt sportowy / Sport / Łyżwiarstwo figurowe i hokej / Części i akcesoria do łyżew / Ochraniacze płozy łyżwy'),
(4020, 'Sprzęt sportowy / Sport / Lekkoatletyka / Tyczki'),
(4021, 'Sprzęt sportowy / Gry towarzyskie / Shuffleboard stołowy / Krążki do gry w shuffleboard stołowy'),
(4022, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Wysokościomierze'),
(4023, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Paralotniarstwo i spadochroniarstwo / Spadochrony'),
(4024, 'Aparaty, kamery i przyrządy optyczne / Aparaty i kamery / Aparaty jednorazowe'),
(4026, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Naczynia do serwowania żywności'),
(4027, 'Pojazdy i części / Akcesoria i części do pojazdów / Przewóz i przechowywanie bagażu / Przyczepy'),
(4031, 'Sprzęt / Akcesoria do sprzętu / Przechowywanie i porządkowanie narzędzi / Pochwy na narzędzia'),
(4032, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do mierzenia i oznaczania / Pieczątki ozdobne'),
(4035, 'Elektronika / Audio / Elementy audio / Transmitery audio / Transmitery FM'),
(4036, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Akcesoria laboratoryjne / Kolby laboratoryjne'),
(4037, 'Pojazdy i części / Akcesoria i części do pojazdów / Przewóz i przechowywanie bagażu / Przyczepy / Przyczepy do przewożenia koni'),
(4040, 'Aparaty, kamery i przyrządy optyczne / Przyrządy optyczne / Lunety teleskopowe'),
(4043, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Golf / Części i akcesoria do kijów golfowych / Osłony na główki kijów golfowych'),
(4044, 'Pojazdy i części / Akcesoria i części do pojazdów / Przewóz i przechowywanie bagażu / Przyczepy / Przyczepy towarowe'),
(4049, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria i kosmetyki do kąpieli / Czepki pod prysznic'),
(4050, 'Sprzęt sportowy / Sport / Koszykówka / Części i akcesoria do koszy / Słupy koszy'),
(4054, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Ozdoby / Naklejki dekoracyjne'),
(4055, 'Biznes i przemysł / Handel detaliczny / Obrót środkami pieniężnymi / Papierowe rolki na monety i banderole na monety'),
(4056, 'Zdrowie i uroda / Higiena osobista / Środki ułatwiające zasypianie / Generatory białego szumu'),
(4057, 'Ubrania i akcesoria / Akcesoria do ubrań / Akcesoria do włosów / Włosy do przedłużania'),
(4060, 'Artykuły dla dorosłych / Artykuły erotyczne / Czasopisma erotyczne'),
(4061, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Akcesoria laboratoryjne / Cylindry miarowe'),
(4063, 'Meble / Szafy i przechowywanie / Szafy i garderoby'),
(4070, 'Biznes i przemysł / Informacja wizualna / Znaki elektroniczne / Neony'),
(4072, 'Biznes i przemysł / Piercing i tatuaże / Sprzęt do tatuażu / Igły do tatuażu'),
(4073, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Pistolety do kleju'),
(4074, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Mierniki pH'),
(4075, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Akcesoria laboratoryjne / Pipety'),
(4076, 'Zdrowie i uroda / Higiena osobista / Środki ułatwiające zasypianie'),
(4077, 'Dom i ogród / Bielizna stołowa i pościelowa / Ręczniki'),
(4080, 'Meble / Stoły / Stoły składane'),
(4081, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Poziomice / Poziomice klasyczne'),
(4082, 'Dom i ogród / Akcesoria dla palaczy / Popielniczki'),
(4085, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Kompostowanie'),
(4086, 'Artykuły biurowe / Sortowanie i utrzymanie porządku / Oprawa dokumentów / Akcesoria do segregatorów'),
(4089, 'Sprzęt sportowy / Sport / Koszykówka / Części i akcesoria do koszy / Tablice do koszykówki'),
(4091, 'Żywność, napoje i tytoń / Wyroby tytoniowe / E-papierosy i waporyzery'),
(4093, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Sportowe artykuły kolekcjonerskie / Pamiątki sportowe z autografem / Pamiątki piłkarskie z autografem'),
(4095, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Narzędzia kempingowe / Noże i narzędzia wielofunkcyjne'),
(4096, 'Biznes i przemysł / Usługi gastronomiczne / Koszyki gastronomiczne'),
(4102, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Części do laptopów / Zapasowe ekrany do laptopów'),
(4105, 'Meble / Meble zewnętrzne / Siedzenia ogrodowe / Leżaki'),
(4106, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do makijażu / Gąbeczki do makijażu'),
(4111, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych / Akcesoria do gitar / Kostki do gitar'),
(4116, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Sprzęt laboratoryjny / Autoklawy'),
(4117, 'Artykuły biurowe / Artykuły różne / Etykiety i przekładki indeksujące / Zawieszki towarowe'),
(4121, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do makijażu / Bibułki matujące'),
(4122, 'Biznes i przemysł / Piercing i tatuaże / Sprzęt do piercingu / Igły do piercingu'),
(4124, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Sportowe artykuły kolekcjonerskie / Pamiątki sportowe z autografem / Pamiątki z autografem związane z futbolem amerykańskim'),
(4126, 'Dom i ogród / Bielizna stołowa i pościelowa / Ręczniki / Ręczniki plażowe'),
(4127, 'Biznes i przemysł / Handel detaliczny / Metkownice'),
(4131, 'Biznes i przemysł / Informacja wizualna / Znaki elektroniczne / Znaki LED'),
(4132, 'Sprzęt / Narzędzia / Narzędzia murarskie / Zacieraczka do betonu'),
(4133, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Sprzęt laboratoryjny / Laboratoryjne płyty grzewcze'),
(4136, 'Aparaty, kamery i przyrządy optyczne / Przyrządy optyczne / Lunety teleskopowe / Lunety obserwacyjne'),
(4137, 'Artykuły biurowe / Artykuły różne / Etykiety i przekładki indeksujące / Ramki do etykiet');
INSERT INTO `google_kategorie` (`id`, `nazwa`) VALUES
(4140, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Akcesoria laboratoryjne / Tryskawki'),
(4142, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Stojaki muzyczne'),
(4143, 'Dom i ogród / Bielizna stołowa i pościelowa / Bielizna stołowa / Obrusy'),
(4144, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Sportowe artykuły kolekcjonerskie / Pamiątki sportowe z autografem / Pamiątki hokejowe z autografem'),
(4145, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Koszyki na bidony'),
(4148, 'Meble / Szafy i przechowywanie / Toaletki'),
(4149, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Sportowe artykuły kolekcjonerskie / Pamiątki sportowe z autografem / Pamiątki baseballowe i softballowe z autografem'),
(4151, 'Biznes i przemysł / Handel detaliczny / Obrót środkami pieniężnymi / Liczarki do pieniędzy'),
(4154, 'Artykuły biurowe / Artykuły różne / Etykiety i przekładki indeksujące / Przekładki indeksujące'),
(4155, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Akcesoria laboratoryjne / Statywy do probówek'),
(4160, 'Biznes i przemysł / Handel detaliczny / Gabloty sklepowe'),
(4161, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Maszynki do lodu'),
(4163, 'Meble / Przepierzenia'),
(4164, 'Aparaty, kamery i przyrządy optyczne / Przyrządy optyczne / Lunety'),
(4167, 'Gry i zabawki / Zabawki / Zabawki sportowe / Latające talerze'),
(4171, 'Dom i ogród / Bielizna stołowa i pościelowa'),
(4174, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Ozdobne spinki i zamknięcia / Zamki błyskawiczne'),
(4175, 'Sztuka i rozrywka / Hobby i sztuki piękne / Modelarstwo / Zestawy do modelarstwa redukcyjnego'),
(4177, 'Gry i zabawki / Zabawki / Zabawki ruchowe / Zabawki na sprężynie'),
(4179, 'Ubrania i akcesoria / Akcesoria do ubrań / Przypinki'),
(4180, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Sportowe artykuły kolekcjonerskie / Pamiątki sportowe z autografem / Pamiątki z wyścigów samochodowych z autografem'),
(4181, 'Biznes i przemysł / Handel detaliczny / Obrót środkami pieniężnymi'),
(4182, 'Artykuły biurowe / Sortowanie i utrzymanie porządku / Oprawa dokumentów / Grzbiety do bindowania'),
(4183, 'Artykuły biurowe / Sortowanie i utrzymanie porządku / Oprawa dokumentów / Akcesoria do segregatorów / Przekładki'),
(4191, 'Meble / Meble biurowe / Biurka'),
(4192, 'Sprzęt sportowy / Sport / Koszykówka / Części i akcesoria do koszy / Obręcze do koszy'),
(4194, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Sztyce podsiodłowe'),
(4195, 'Meble / Szafy i przechowywanie / Komody'),
(4199, 'Sprzęt / Akcesoria do sprzętu / Przechowywanie i porządkowanie narzędzi / Przechowywanie węży ogrodowych'),
(4200, 'Artykuły biurowe / Artykuły różne / Etykiety i przekładki indeksujące / Etykiety wysyłkowe'),
(4201, 'Dom i ogród / Trawnik i ogród / Nawadnianie / Dysze do węży ogrodowych'),
(4203, 'Dom i ogród / Bielizna stołowa i pościelowa / Bielizna stołowa / Serwetki'),
(4205, 'Meble / Szafy i przechowywanie / Skrzynie i kufry'),
(4211, 'Zdrowie i uroda / Higiena osobista / Środki ułatwiające zasypianie / Poduszki podróżne'),
(4212, 'Artykuły biurowe / Sortowanie i utrzymanie porządku / Oprawa dokumentów / Akcesoria do segregatorów / Mechanizmy'),
(4214, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Szczypce do rozłupywania / Dziadki do orzechów / Ozdobne dziadki do orzechów'),
(4215, 'Biznes i przemysł / Piercing i tatuaże / Sprzęt do tatuażu / Atramenty do tatuażu'),
(4216, 'Gry i zabawki / Zabawki / Zabawki ruchowe / Gry w kamienie'),
(4217, 'Biznes i przemysł / Usługi gastronomiczne / Pojemniki na brudne naczynia'),
(4218, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja paznokci / Tipsy'),
(4220, 'Dom i ogród / Artykuły gospodarstwa domowego / Dezynsekcja / Packi na muchy'),
(4222, 'Dom i ogród / Kuchnia i jadalnia / Naczynia barowe / Shakery i przybory do drinków / Szpikulce do lodu'),
(4224, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Części do laptopów'),
(4226, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Ozdobne spinki i zamknięcia / Guziki czterodziurkowe'),
(4231, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Sprzęt laboratoryjny / Piece laboratoryjne'),
(4233, 'Dom i ogród / Ozdoby / Kufry'),
(4237, 'Dom i ogród / Artykuły gospodarstwa domowego / Przechowywanie i utrzymanie porządku / Przechowywanie zdjęć / Pudełka na zdjęcia'),
(4240, 'Pojazdy i części / Akcesoria i części do pojazdów / Przewóz i przechowywanie bagażu / Bagażniki samochodowe / Kosze bagażowe'),
(4241, 'Meble / Ławki / Pufy do toaletek'),
(4243, 'Pojazdy i części / Akcesoria i części do pojazdów / Przewóz i przechowywanie bagażu / Przyczepy / Przyczepy kempingowe'),
(4244, 'Biznes i przemysł / Handel detaliczny / Wieszaki sklepowe do ubrań'),
(4245, 'Biznes i przemysł / Branża medyczna / Sprzęt medyczny / Otoskopy i oftalmoskopy'),
(4247, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Zgniatarki do puszek'),
(4248, 'Zdrowie i uroda / Opieka zdrowotna / Artykuły dla osób mających trudności z poruszaniem się / Pomoce ułatwiające chodzenie / Kule'),
(4254, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Golf / Części i akcesoria do kijów golfowych / Rękojeści kijów golfowych'),
(4255, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Akcesoria laboratoryjne'),
(4257, 'Dom i ogród / Bielizna stołowa i pościelowa / Ręczniki / Ściereczki kuchenne'),
(4268, 'Meble / Szafy i przechowywanie / Skrzynie i kufry / Skrzynie na zabawki'),
(4270, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Części do laptopów / Obudowy do laptopów'),
(4274, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria optyczne / Akcesoria do termowizorów'),
(4276, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Akcesoria laboratoryjne / Szalki Petriego'),
(4279, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Sportowe artykuły kolekcjonerskie / Pamiątki sportowe z autografem / Pamiątki koszykarskie z autografem'),
(4282, 'Sprzęt sportowy / Sport / Boks i sztuki walki / Broń do sztuk walki'),
(4283, 'Biznes i przemysł / Handel detaliczny / Obrót środkami pieniężnymi / Kasy fiskalne i produkty związane z obsługą terminali / Szuflady kasowe'),
(4285, 'Biznes i przemysł / Piercing i tatuaże'),
(4290, 'Biznes i przemysł / Handel detaliczny / Obrót środkami pieniężnymi / Testery do banknotów'),
(4292, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Podgrzewacze do żywności / Bemary gastronomiczne'),
(4294, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Poziomice / Niwelatory optyczne'),
(4295, 'Dom i ogród / Ozdoby / Świnki-skarbonki i słoiki na pieniądze'),
(4297, 'Biznes i przemysł / Informacja wizualna / Znaki elektroniczne'),
(4299, 'Meble / Meble zewnętrzne'),
(4301, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Części do laptopów / Zapasowe klawiatury do laptopów'),
(4303, 'Artykuły biurowe / Sortowanie i utrzymanie porządku / Oprawa dokumentów / Segregatory'),
(4306, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Akcesoria laboratoryjne / Probówki'),
(4310, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Akcesoria laboratoryjne / Zlewki'),
(4312, 'Artykuły biurowe / Sortowanie i utrzymanie porządku / Oprawa dokumentów'),
(4313, 'Zdrowie i uroda / Higiena osobista / Środki ułatwiające zasypianie / Maski na oczy'),
(4316, 'Zdrowie i uroda / Higiena osobista / Higiena jamy ustnej / Wykałaczki'),
(4317, 'Meble / Meble biurowe / Stoły do pracy / Stoły konferencyjne'),
(4322, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Organizery kuchenne / Pojemniki na wykałaczki'),
(4325, 'Sprzęt / Narzędzia / Ubijaki'),
(4326, 'Biznes i przemysł / Piercing i tatuaże / Sprzęt do tatuażu'),
(4327, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Paralotniarstwo i spadochroniarstwo / Lotnie'),
(4329, 'Biznes i przemysł / Handel detaliczny / Obrót środkami pieniężnymi / Saszetki na pieniądze'),
(4332, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Siekacze do ciasta'),
(4333, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Sportowe artykuły kolekcjonerskie / Pamiątki sportowe z autografem'),
(4334, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Podpórki na łyżki'),
(4335, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Sprzęt laboratoryjny'),
(4336, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Sprzęt laboratoryjny / Wirówki'),
(4340, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Teodolity'),
(4341, 'Artykuły biurowe / Akcesoria biurowe / Pieczątki biurowe'),
(4343, 'Dom i ogród / Bielizna stołowa i pościelowa / Bielizna stołowa / Serwety'),
(4350, 'Biznes i przemysł / Piercing i tatuaże / Sprzęt do piercingu'),
(4351, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Serpentyna w sprayu'),
(4352, 'Gry i zabawki / Zabawki / Zabawki ruchowe'),
(4355, 'Meble / Stoły / Stoły do kuchni i jadalni'),
(4358, 'Dom i ogród / Parasole'),
(4360, 'Dom i ogród / Artykuły gospodarstwa domowego / Przechowywanie i utrzymanie porządku / Przechowywanie zdjęć'),
(4364, 'Biznes i przemysł / Branża medyczna / Sprzęt medyczny / Stetoskopy'),
(4366, 'Dom i ogród / Akcesoria łazienkowe / Poduszki do kąpieli'),
(4368, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Przechowywanie negatywów i slajdów'),
(4372, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Naczynia do serwowania żywności / Patery piętrowe'),
(4375, 'Dom i ogród / Ozdoby / Dekoracje okienne / Ekrany okienne'),
(4377, 'Artykuły biurowe / Artykuły różne / Etykiety i przekładki indeksujące / Etykiety adresowe'),
(4379, 'Biznes i przemysł / Piercing i tatuaże / Sprzęt do tatuażu / Maszynki do tatuażu'),
(4386, 'Dzieci i niemowlęta / Akcesoria do transportu niemowląt'),
(4387, 'Dzieci i niemowlęta / Akcesoria do transportu niemowląt / Akcesoria do wózków dla niemowląt'),
(4415, 'Elektronika / Telekomunikacja / Urządzenia radiowe / Skanery radiowe'),
(4416, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria do obiektywów / Konwertery do obiektywów'),
(4417, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Pamięci komputerowe / Akcesoria do dysków twardych / Stacje dokujące dysków twardych'),
(4418, 'Żywność, napoje i tytoń / Żywność / Nabiał / Śmietanki do kawy'),
(4419, 'Sprzęt / Narzędzia / Klocki do szlifowania'),
(4420, 'Dom i ogród / Bielizna stołowa i pościelowa / Artykuły pościelowe / Ochraniacze na materace / Pokrowce na materace'),
(4421, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Patelnie i woki elektryczne'),
(4423, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Garnki / Patelnie saute'),
(4424, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Akcesoria do garnków'),
(4427, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Akcesoria do garnków / Akcesoria do woków'),
(4432, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Obiektywy do aparatów i kamer / Obiektywy do aparatów'),
(4433, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla kotów / Legowiska dla kotów'),
(4434, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla psów / Legowiska dla psów'),
(4435, 'Biznes i przemysł / Branża medyczna / Meble medyczne / Łóżka do opieki domowej i szpitalnej'),
(4437, 'Meble / Łóżka i akcesoria / Akcesoria do łóżek'),
(4441, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria do obiektywów / Torby na obiektywy'),
(4450, 'Elektronika / Elektronika morska / Radia morskie'),
(4451, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Meble kempingowe / Akcesoria do materaców i poduszek dmuchanych'),
(4452, 'Dom i ogród / Bielizna stołowa i pościelowa / Artykuły pościelowe / Ochraniacze na materace'),
(4453, 'Dom i ogród / Ozdoby / Poduszki na krzesła i sofy'),
(4454, 'Dom i ogród / Ozdoby / Poduszki dekoracyjne'),
(4456, 'Dom i ogród / Ozdoby / Poduszki pod plecy'),
(4458, 'Elektronika / Wideo / Akcesoria do wideo / Akcesoria i części do telewizorów / Uchwyty do telewizorów i monitorów'),
(4459, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Garnki / Patelnie do paelli'),
(4463, 'Elektronika / Akcesoria elektroniczne / Adaptery / Adaptery audio i wideo'),
(4465, 'Artykuły biurowe / Sprzęt do prezentacji / Bezprzewodowe akcesoria do prezentacji'),
(4466, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Golf / Rękawice golfowe'),
(4467, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Golf / Ręczniki golfowe'),
(4468, 'Sprzęt / Materiały budowlane / Drzwi / Drzwi garażowe'),
(4469, 'Sprzęt / Akcesoria do sprzętu / Łańcuchy, druty i sznury / Łańcuszki do włączników'),
(4470, 'Artykuły biurowe / Akcesoria biurowe / Akcesoria do przyborów piśmienniczych i rysunkowych'),
(4471, 'Artykuły biurowe / Akcesoria biurowe / Akcesoria do przyborów piśmienniczych i rysunkowych / Atramenty i naboje'),
(4472, 'Artykuły biurowe / Akcesoria biurowe / Akcesoria do przyborów piśmienniczych i rysunkowych / Wkłady do ołówków automatycznych'),
(4474, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Sprzęt laboratoryjny / Miksery laboratoryjne'),
(4475, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja włosów / Akcesoria do stylizacji włosów / Akcesoria do suszarek do włosów'),
(4482, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Maszynki do mochi'),
(4483, 'Dom i ogród / Sprzęt AGD / Suszarki do futonów'),
(4484, 'Meble / Stoły / Kotatsu'),
(4485, 'Dom i ogród / Sprzęt AGD / Ogrzewanie, wentylacja i klimatyzacja / Wentylatory / Łopaty wentylatorów'),
(4486, 'Dzieci i niemowlęta / Akcesoria do transportu niemowląt / Akcesoria do fotelików samochodowych dla niemowląt i dzieci starszych'),
(4487, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do szlifowania'),
(4488, 'Elektronika / Urządzenia do pobierania opłat drogowych'),
(4490, 'Dom i ogród / Przygotowanie na sytuacje kryzysowe / Alarmy o trzęsieniu ziemi'),
(4491, 'Dom i ogród / Przygotowanie na sytuacje kryzysowe / Kątowniki do mebli'),
(4492, 'Artykuły biurowe / Sprzęt do prezentacji / Rzutniki'),
(4493, 'Elektronika / Audio / Elementy audio / Wzmacniacze audio / Wzmacniacze do słuchawek'),
(4495, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Piekarniki i rożna do drobiu'),
(4497, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Drzwiczki dla zwierząt'),
(4499, 'Artykuły biurowe / Akcesoria biurowe / Rozszywacze'),
(4500, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do ekspresów do kawy i espresso / Uchwyty na filtry do kawy'),
(4501, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Akcesoria do garnków / Akcesoria do szybkowarów'),
(4502, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Akcesoria do form do pieczenia'),
(4503, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Akcesoria do form do pieczenia / Podkładki do pieczenia'),
(4506, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Termometr na podczerwień'),
(4507, 'Zdrowie i uroda / Higiena osobista / Golenie i strzyżenie / Usuwanie włosów'),
(4508, 'Zdrowie i uroda / Higiena osobista / Golenie i strzyżenie / Usuwanie włosów / Depilatory'),
(4509, 'Zdrowie i uroda / Higiena osobista / Golenie i strzyżenie / Usuwanie włosów / Urządzenia do depilacji metodą elektrolizy'),
(4510, 'Zdrowie i uroda / Higiena osobista / Golenie i strzyżenie / Usuwanie włosów / Epilatory'),
(4511, 'Zdrowie i uroda / Higiena osobista / Golenie i strzyżenie / Usuwanie włosów / Zestawy do depilacji woskiem'),
(4512, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Urządzenia wejściowe / Klawiatury numeryczne'),
(4513, 'Meble / Meble zewnętrzne / Siedzenia ogrodowe / Sofy ogrodowe'),
(4515, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Szufelki'),
(4516, 'Dom i ogród / Artykuły gospodarstwa domowego / Przechowywanie śmieci'),
(4517, 'Dom i ogród / Artykuły gospodarstwa domowego / Przechowywanie śmieci / Pojemniki do recyclingu'),
(4519, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do wyciskarek soków'),
(4520, 'Dom i ogród / Kuchnia i jadalnia / Transportery do żywności i napojów / Pokrowce na kubki'),
(4521, 'Dom i ogród / Kuchnia i jadalnia / Transportery do żywności i napojów / Pokrowce na kubki / Pokrowce na puszki i butelki'),
(4522, 'Dom i ogród / Kuchnia i jadalnia / Transportery do żywności i napojów / Pokrowce na kubki / Pokrowce na filiżanki'),
(4525, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Golf / Akcesoria do toreb golfowych / Pokrowce na torby golfowe'),
(4527, 'Zdrowie i uroda / Opieka zdrowotna / Pierwsza pomoc / Środki do przemywania oczu'),
(4528, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Automaty do herbaty'),
(4529, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Akcesoria do garnków / Koszyki do parowarów'),
(4530, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Miski kuchenne'),
(4532, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Parowary i garnki do gotowania na parze'),
(4537, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Golf / Akcesoria do toreb golfowych / Wózki na torby golfowe'),
(4539, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Lodówki do wina'),
(4540, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty dęte drewniane / Fagoty'),
(4541, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty dęte drewniane / Oboje'),
(4544, 'Sprzęt / Materiały budowlane / Pokrycia dachowe / Akcesoria do rynien'),
(4546, 'Dom i ogród / Ozdoby / Zegary / Budziki'),
(4548, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do czyszczenia dywanów i myjek parowych'),
(4550, 'Elektronika / Telekomunikacja / Telefonia / Akcesoria do telefonów komórkowych / Zawieszki i paski do telefonów komórkowych'),
(4551, 'Zdrowie i uroda / Opieka zdrowotna / Środki ochrony układu oddechowego'),
(4552, 'Zdrowie i uroda / Opieka zdrowotna / Środki ochrony układu oddechowego / Nebulizatory'),
(4554, 'Gry i zabawki / Gry / Przenośne gry elektroniczne'),
(4555, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Sprzęt laboratoryjny / Akcesoria do mikroskopu'),
(4556, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Sprzęt laboratoryjny / Akcesoria do mikroskopu / Okulary i adaptery do mikroskopu'),
(4557, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Sprzęt laboratoryjny / Akcesoria do mikroskopu / Kamery mikroskopowe'),
(4558, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Sprzęt laboratoryjny / Akcesoria do mikroskopu / Szkiełka podstawowe do mikroskopu'),
(4559, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Zaparzacze do herbaty'),
(4560, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do grilla na zewnątrz / Kosze do grilla na zewnątrz'),
(4562, 'Dom i ogród / Kuchnia i jadalnia / Naczynia barowe / Zatyczki do butelek'),
(4563, 'Dom i ogród / Kuchnia i jadalnia / Naczynia barowe / Aeratory do wina'),
(4564, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych'),
(4565, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Akcesoria do pił łańcuchowych'),
(4566, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Akcesoria do kosiarek'),
(4567, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Akcesoria do odśnieżarek'),
(4569, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja włosów / Akcesoria do stylizacji włosów / Akcesoria do prostownic do włosów'),
(4570, 'Elektronika / Wideo / Akcesoria do wideo / Akcesoria do projektorów / Stojaki do ekranów projekcyjnych'),
(4571, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Opony rowerowe'),
(4572, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Dętki rowerowe'),
(4574, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Części układu hamulcowego roweru / Szczęki hamulcowe'),
(4575, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Części układu hamulcowego roweru / Klamki hamulcowe'),
(4576, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Części układu hamulcowego roweru / Tarcze hamulcowe'),
(4577, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Części układu hamulcowego roweru / Zestawy hamulców rowerowych'),
(4579, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Igły i szydełka / Igły do maszyn do szycia'),
(4580, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Akcesoria do narzędzi dla artystów i rękodzielników / Futerały i osłony na maszyny do szycia'),
(4582, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Kierownice rowerowe'),
(4583, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Mostki rowerowe'),
(4585, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Części do napędów rowerowych'),
(4586, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Części do napędów rowerowych / Kasety rowerowe'),
(4587, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Części do napędów rowerowych / Łańcuchy rowerowe'),
(4588, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Części do napędów rowerowych / Przerzutki rowerowe'),
(4589, 'Sprzęt sportowy / Fitness / Trening kardio / Sprzęt kardio'),
(4590, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Części do napędów rowerowych / Suporty'),
(4591, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Części do napędów rowerowych / Zębatki'),
(4592, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Części do napędów rowerowych / Korby rowerowe'),
(4593, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Części do napędów rowerowych / Pedały'),
(4594, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Części do napędów rowerowych / Manetki'),
(4595, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Zaciski sztycy podsiodłowej'),
(4596, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Drobne części rowerowe'),
(4597, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Części kół rowerowych'),
(4598, 'Sprzęt sportowy / Fitness / Trening kardio / Akcesoria do sprzętu kardio'),
(4599, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Części kół rowerowych / Piasty rowerowe'),
(4600, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Części kół rowerowych / Szprychy rowerowe'),
(4601, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Części kół rowerowych / Nyple rowerowe'),
(4602, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Części kół rowerowych / Obręcze rowerowe'),
(4603, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Widelce rowerowe'),
(4605, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Golf / Akcesoria do toreb golfowych'),
(4608, 'Żywność, napoje i tytoń / Żywność / Przyprawy i wzmacniacze smaku'),
(4610, 'Żywność, napoje i tytoń / Żywność / Przyprawy i wzmacniacze smaku / Glutaminian sodu'),
(4611, 'Żywność, napoje i tytoń / Żywność / Przyprawy i wzmacniacze smaku / Sól'),
(4613, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Pasta fasolowa'),
(4614, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Sosy ostre'),
(4615, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Sos do sataya'),
(4616, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Sos sojowy'),
(4617, 'Elektronika / Akcesoria elektroniczne / Środki do czyszczenia urządzeń elektronicznych'),
(4627, 'Żywność, napoje i tytoń / Żywność / Mięso, owoce morza i jajka / Jajka'),
(4628, 'Żywność, napoje i tytoń / Żywność / Mięso, owoce morza i jajka / Mięso'),
(4629, 'Żywność, napoje i tytoń / Żywność / Mięso, owoce morza i jajka / Owoce morza'),
(4630, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Przybory do dekorowania ciast'),
(4631, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do perkusji / Młotki perkusyjne'),
(4632, 'Biznes i przemysł / Usługi gastronomiczne / Zastawa jednorazowa'),
(4633, 'Sprzęt / Narzędzia / Piły / Pilarki taśmowe'),
(4634, 'Sprzęt / Materiały budowlane / Drzwi / Drzwi wejściowe'),
(4636, 'Dom i ogród / Oświetlenie / Lampy'),
(4638, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria do statywów trójnożnych i jednonożnych'),
(4639, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria do statywów trójnożnych i jednonożnych / Głowice do statywów trójnożnych i jednonożnych'),
(4640, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria do statywów trójnożnych i jednonożnych / Futerały na statywy trójnożne i jednonożne'),
(4641, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Akcesoria do kosiarek / Noże do kosiarek'),
(4642, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Akcesoria do kosiarek / Osłony do kosiarek'),
(4643, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Akcesoria do kosiarek / Paski do kosiarek'),
(4644, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Akcesoria do kosiarek / Koła pasowe i zębate do kosiarek'),
(4645, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Akcesoria do kosiarek / Kosze na skoszoną trawę'),
(4646, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Akcesoria do pił łańcuchowych / Łańcuchy do pił łańcuchowych'),
(4647, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Akcesoria do pił łańcuchowych / Prowadnice do pił łańcuchowych'),
(4648, 'Gry i zabawki / Stopery do gier'),
(4650, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do ogrzewaczy ogrodowych'),
(4651, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do ogrzewaczy ogrodowych / Osłony do ogrzewaczy ogrodowych'),
(4652, 'Elektronika / Audio / Odtwarzacze i nagrywarki audio / Szafy grające'),
(4653, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do suszarek do żywności'),
(4654, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do suszarek do żywności / Tacki do suszarek do żywności'),
(4655, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do suszarek do żywności / Siatki do suszarek do żywności'),
(4656, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły do pralni / Podkładki i pokrowce na deski do prasowania'),
(4657, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły do pralni / Odplamiacze do tkanin'),
(4658, 'Sprzęt / Akcesoria do narzędzi / Stojaki na narzędzia'),
(4659, 'Sprzęt / Akcesoria do narzędzi / Stojaki na narzędzia / Stojaki na piły'),
(4660, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Akcesoria do garnków / Pokrywki do garnków i patelni'),
(4661, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Akcesoria do garnków / Uchwyty do garnków i patelni'),
(4662, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Akcesoria do garnków / Akcesoria do woków / Pierścienie do woków'),
(4663, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Akcesoria do garnków / Akcesoria do woków / Szczotki do woków'),
(4664, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Sprzęt laboratoryjny / Akcesoria do mikroskopu / Zapasowe żarówki do mikroskopu'),
(4665, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Sprzęt laboratoryjny / Akcesoria do mikroskopu / Obiektywy do mikroskopu'),
(4666, 'Elektronika / Telekomunikacja / Telefonia / Telefony konferencyjne'),
(4667, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do osuszaczy'),
(4669, 'Sprzęt sportowy / Fitness / Maty pod sprzęt do ćwiczeń'),
(4670, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Szczotki do szorowania'),
(4671, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Kije do szczotek i mopów'),
(4672, 'Sprzęt / Narzędzia / Szczotki do betonu'),
(4674, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do maszynek do lodów'),
(4675, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do maszynek do lodów / Miski do mrożenia do maszynek do lodów'),
(4676, 'Sprzęt sportowy / Sport / Koszykówka / Części i akcesoria do koszy'),
(4677, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Szczotki do dywanów'),
(4678, 'Dzieci i niemowlęta / Kąpanie niemowlęcia'),
(4679, 'Dzieci i niemowlęta / Kąpanie niemowlęcia / Wanny i krzesełka kąpielowe dla niemowląt'),
(4682, 'Żywność, napoje i tytoń / Żywność / Ziarna, ryż i płatki / Ryż'),
(4683, 'Żywność, napoje i tytoń / Żywność / Ziarna, ryż i płatki / Amarant'),
(4684, 'Żywność, napoje i tytoń / Żywność / Ziarna, ryż i płatki / Gryka'),
(4686, 'Żywność, napoje i tytoń / Żywność / Ziarna, ryż i płatki / Proso'),
(4687, 'Żywność, napoje i tytoń / Żywność / Ziarna, ryż i płatki / Jęczmień'),
(4688, 'Żywność, napoje i tytoń / Żywność / Ziarna, ryż i płatki / Pszenica'),
(4689, 'Żywność, napoje i tytoń / Żywność / Ziarna, ryż i płatki / Płatki śniadaniowe i granola'),
(4690, 'Żywność, napoje i tytoń / Żywność / Ziarna, ryż i płatki / Płatki owsiane, kasza kukurydziana i płatki śniadaniowe na ciepło'),
(4692, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Tahini'),
(4696, 'Sprzęt / Akcesoria do sprzętu / Wyposażenie do szaf'),
(4697, 'Sprzęt / Akcesoria do sprzętu / Wyposażenie do szaf / Szyldy do szaf'),
(4698, 'Sprzęt / Akcesoria do sprzętu / Wyposażenie do szaf / Zatrzaski do szaf'),
(4699, 'Sprzęt / Akcesoria do sprzętu / Wyposażenie do szaf / Drzwi do szaf'),
(4700, 'Sprzęt / Akcesoria do sprzętu / Wyposażenie do szaf / Gałki i uchwyty do szaf'),
(4705, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Akcesoria do wałków'),
(4706, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Akcesoria do wałków / Osłonki na wałek'),
(4707, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Akcesoria do wałków / Pierścienie do wałków'),
(4708, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Ściereczki do pieczenia ciasta'),
(4709, 'Sprzęt / Materiały elektryczne / Akcesoria do agregatów prądotwórczych'),
(4714, 'Sprzęt / Materiały elektryczne / Panele słoneczne'),
(4715, 'Sprzęt / Materiały elektryczne / Zestawy do instalacji słonecznych'),
(4716, 'Sprzęt / Narzędzia / Zestawy narzędzi / Zestawy elektronarzędzi wielofunkcyjnych'),
(4717, 'Dom i ogród / Artykuły gospodarstwa domowego / Akcesoria do przechowywania śmieci / Pokrywki do pojemników na śmieci'),
(4718, 'Dom i ogród / Trawnik i ogród / Nawadnianie / Osprzęt i zawory do węży ogrodowych'),
(4720, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Zestawy do fondue'),
(4721, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Garnki / Zestawy garnków'),
(4722, 'Dom i ogród / Kuchnia i jadalnia / Transportery do żywności i napojów / Kubki termiczne'),
(4726, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Akcesoria do form do pieczenia / Kratki do piekarnika'),
(4728, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Części do prysznica / Brodziki'),
(4730, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Świeczki urodzinowe'),
(4735, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Naczynia do serwowania żywności / Wazy na poncz'),
(4736, 'Elektronika / Akcesoria elektroniczne / Akcesoria komputerowe / Akcesoria do palmtopów / Akcesoria do czytników e-booków'),
(4737, 'Elektronika / Akcesoria elektroniczne / Akcesoria komputerowe / Akcesoria do palmtopów / Akcesoria do urządzeń PDA'),
(4738, 'Elektronika / Akcesoria elektroniczne / Akcesoria komputerowe / Akcesoria do palmtopów / Akcesoria do czytników e-booków / Futerały na czytniki e-booków'),
(4739, 'Elektronika / Akcesoria elektroniczne / Akcesoria komputerowe / Akcesoria do palmtopów / Akcesoria do urządzeń PDA / Futerały na urządzenia PDA'),
(4740, 'Dom i ogród / Ozdoby / Zapachy do domu / Potpourri'),
(4741, 'Dom i ogród / Ozdoby / Akcesoria do zapachów do domu / Uchwyty na kadzidełka'),
(4742, 'Biznes i przemysł / Usługi gastronomiczne / Wózki gastronomiczne'),
(4743, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty dęte drewniane / Harmonijki'),
(4744, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty dęte drewniane / Drumle'),
(4745, 'Elektronika / Komputery / Tablety'),
(4746, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Tacki do kostek lodu'),
(4748, 'Żywność, napoje i tytoń / Żywność / Cukierki i czekolady'),
(4752, 'Artykuły biurowe / Akcesoria biurowe / Przybory piśmiennicze i rysunkowe / Pastele'),
(4753, 'Zdrowie i uroda / Opieka zdrowotna / Pierwsza pomoc / Leczenie ciepłem i zimnem / Worki z lodem'),
(4754, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Higrometry'),
(4755, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Wilgotnościomierze'),
(4756, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Sejsmometry'),
(4757, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Sonometry'),
(4758, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Mierniki UV'),
(4759, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Wibrometry'),
(4760, 'Elektronika / Wideo / Akcesoria do wideo / Okulary 3D'),
(4762, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Młynki do przypraw'),
(4763, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do rozdrabniaczy do żywności'),
(4764, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Formy do pieczenia / Zestawy form do pieczenia'),
(4765, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Zestawy przyborów kuchennych'),
(4766, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja włosów / Preparaty przeciw wypadaniu włosów'),
(4767, 'Zdrowie i uroda / Opieka zdrowotna / Monitory funkcji życiowych / Monitory do badania pracy serca płodu'),
(4768, 'Dzieci i niemowlęta / Karmienie / Zegary do pielęgnacji niemowląt'),
(4770, 'Dom i ogród / Ozdoby / Świece bezpłomieniowe'),
(4771, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Rękawy do pieczenia'),
(4775, 'Zdrowie i uroda / Higiena osobista / Higiena jamy ustnej / Akcesoria do szczoteczek do zębów'),
(4776, 'Zdrowie i uroda / Higiena osobista / Higiena jamy ustnej / Akcesoria do szczoteczek do zębów / Końcówki do szczoteczek do zębów'),
(4777, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Dozowniki przypraw'),
(4778, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Zestawy do sztuki i rękodzieła / Zestawy do rysowania i malowania'),
(4779, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż / Charakteryzacja i makijaż sceniczny'),
(4785, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Kompasy'),
(4786, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do ekspresów do kawy i espresso / Akcesoria do młynków do kawy'),
(4788, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Akcesoria do młynków do przypraw'),
(4789, 'Sprzęt / Materiały elektryczne / Przedłużacze'),
(4790, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do fagotów'),
(4791, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do klarnetów'),
(4792, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do fletów'),
(4793, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do obojów'),
(4794, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do saksofonów'),
(4797, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów dętych blaszanych / Elementy konserwujące i czyszczące do instrumentów dętych blaszanych'),
(4798, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów dętych blaszanych / Części zamienne do instrumentów dętych blaszanych'),
(4806, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych / Elementy konserwujące i czyszczące do instrumentów strunowych'),
(4809, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do fagotów / Elementy konserwujące i czyszczące do fagotów'),
(4810, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do fagotów / Pokrowce i futerały na fagoty'),
(4811, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do fagotów / Części fagotów'),
(4812, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do fagotów / Stroiki do fagotów'),
(4813, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do fagotów / Stojaki na fagoty'),
(4814, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do fagotów / Paski i podpórki do fagotów'),
(4815, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do fagotów / Elementy konserwujące i czyszczące do fagotów / Ściereczki do fagotów'),
(4816, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do fagotów / Części fagotów / Esy do fagotów'),
(4817, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do fagotów / Części fagotów / Drobne części fagotów'),
(4818, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do klarnetów / Elementy konserwujące i czyszczące do klarnetów'),
(4819, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do klarnetów / Pokrowce i futerały na klarnety'),
(4820, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do klarnetów / Ligatury i osłonki ustników do klarnetów'),
(4822, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do klarnetów / Części do klarnetów'),
(4823, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do klarnetów / Wieszaki i stojaki na klarnety'),
(4824, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do klarnetów / Stroiki do klarnetów'),
(4825, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do klarnetów / Paski i podpórki do klarnetów'),
(4826, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do klarnetów / Elementy konserwujące i czyszczące do klarnetów / Zestawy do konserwacji klarnetów'),
(4827, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do klarnetów / Elementy konserwujące i czyszczące do klarnetów / Wyciory do klarnetów'),
(4828, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do klarnetów / Elementy konserwujące i czyszczące do klarnetów / Ściereczki do klarnetów'),
(4829, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do klarnetów / Części do klarnetów / Baryłki do klarnetów'),
(4830, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do klarnetów / Części do klarnetów / Czary głosowe do klarnetów'),
(4831, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do klarnetów / Części do klarnetów / Ustniki do klarnetów'),
(4832, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do klarnetów / Części do klarnetów / Drobne części do klarnetów'),
(4833, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do fletów / Elementy konserwujące i czyszczące do fletów'),
(4834, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do fletów / Pokrowce i futerały na flety'),
(4836, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do fletów / Części fletów'),
(4837, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do fletów / Wieszaki i stojaki na flety'),
(4838, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do fletów / Elementy konserwujące i czyszczące do fletów / Zestawy do konserwacji fletów'),
(4839, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do fletów / Elementy konserwujące i czyszczące do fletów / Wyciory do fletów'),
(4840, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do fletów / Elementy konserwujące i czyszczące do fletów / Ściereczki do fletów'),
(4841, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do fletów / Części fletów / Korony fletu'),
(4842, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do fletów / Części fletów / Drobne części fletów'),
(4843, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do obojów / Elementy konserwujące i czyszczące do obojów'),
(4844, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do obojów / Pokrowce i futerały na oboje'),
(4845, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do obojów / Części do obojów'),
(4846, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do obojów / Wieszaki i stojaki na oboje'),
(4847, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do obojów / Stroiki do obojów'),
(4848, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do obojów / Paski i podpórki do obojów'),
(4849, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do obojów / Elementy konserwujące i czyszczące do obojów / Zestawy do konserwacji obojów'),
(4850, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do obojów / Elementy konserwujące i czyszczące do obojów / Ściereczki do obojów'),
(4851, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do obojów / Części do obojów / Drobne części do obojów'),
(4852, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do saksofonów / Elementy konserwujące i czyszczące do saksofonów'),
(4853, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do saksofonów / Pokrowce i futerały na saksofony'),
(4854, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do saksofonów / Ligatury i osłonki ustników do saksofonów'),
(4856, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do saksofonów / Części do saksofonów'),
(4857, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do saksofonów / Wieszaki i stojaki na saksofony'),
(4858, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do saksofonów / Stroiki do saksofonów'),
(4859, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do saksofonów / Paski i podpórki do saksofonów'),
(4860, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do saksofonów / Elementy konserwujące i czyszczące do saksofonów / Zestawy do konserwacji saksofonów'),
(4861, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do saksofonów / Elementy konserwujące i czyszczące do saksofonów / Wyciory do saksofonów'),
(4862, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do saksofonów / Elementy konserwujące i czyszczące do saksofonów / Ściereczki do saksofonów'),
(4863, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do saksofonów / Części do saksofonów / Ustniki do saksofonów'),
(4864, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do saksofonów / Części do saksofonów / Szyjki do saksofonów'),
(4865, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do saksofonów / Części do saksofonów / Drobne części do saksofonów'),
(4866, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Smary do korków instrumentów dętych drewnianych'),
(4867, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Ściereczki do polerowania instrumentów dętych drewnianych'),
(4890, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów dętych blaszanych / Elementy konserwujące i czyszczące do instrumentów dętych blaszanych / Szczoteczki, wyciory i patyczki do czyszczenia instrume'),
(4891, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów dętych blaszanych / Elementy konserwujące i czyszczące do instrumentów dętych blaszanych / Zestawy do konserwacji instrumentów dętych blaszanych'),
(4892, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów dętych blaszanych / Elementy konserwujące i czyszczące do instrumentów dętych blaszanych / Środki do czyszczenia i dezynfekcji instrumentów dętych'),
(4893, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów dętych blaszanych / Elementy konserwujące i czyszczące do instrumentów dętych blaszanych / Osłony do instrumentów dętych blaszanych'),
(4894, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów dętych blaszanych / Elementy konserwujące i czyszczące do instrumentów dętych blaszanych / Środki smarujące do instrumentów dętych blaszanych'),
(4895, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów dętych blaszanych / Elementy konserwujące i czyszczące do instrumentów dętych blaszanych / Ściereczki do polerowania instrumentów dętych blaszanyc'),
(4911, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych / Elementy konserwujące i czyszczące do instrumentów strunowych / Środki czyszczące do instrumentów strunowych'),
(4912, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych / Elementy konserwujące i czyszczące do instrumentów strunowych / Środki do polerowania instrumentów strunowych'),
(4914, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Sztuczne ognie i petardy'),
(4915, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Zimne ognie'),
(4916, 'Dzieci i niemowlęta / Akcesoria do transportu niemowląt / Akcesoria do nosidełek dla niemowląt');
INSERT INTO `google_kategorie` (`id`, `nazwa`) VALUES
(4918, 'Dom i ogród / Akcesoria do kominków i pieców opalanych węglem / Wentylatory i dmuchawy do pieców opalanych drewnem'),
(4919, 'Sprzęt / Narzędzia / Zestawy narzędzi'),
(4921, 'Elektronika / Audio / Audio dla DJ-ów i specjalistyczne'),
(4922, 'Elektronika / Audio / Audio dla DJ-ów i specjalistyczne / Odtwarzacze CD dla DJ-ów'),
(4923, 'Elektronika / Audio / Audio dla DJ-ów i specjalistyczne / Systemy dla DJ-ów'),
(4926, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Kołowrotki wędkarskie'),
(4927, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Wędki'),
(4928, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Akumulatory / Akumulatory do zastosowań ogólnych'),
(4929, 'Zdrowie i uroda / Higiena osobista / Waciki'),
(4931, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Poziomice / Poziomice laserowe'),
(4932, 'Dom i ogród / Oświetlenie / Listwy oświetleniowe / Oprawy listew oświetleniowych'),
(4939, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Noże do stroików do instrumentów drewnianych'),
(4941, 'Artykuły biurowe / Akcesoria do książek / Lampki do czytania'),
(4942, 'Zdrowie i uroda / Higiena osobista / Higiena jamy ustnej / Akcesoria do szczoteczek do zębów / Sterylizatory do szczoteczek do zębów'),
(4943, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Syropy'),
(4947, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Miody'),
(4949, 'Oprogramowanie / Programy komputerowe / Programy do multimediów i projektowania / Domowe programy DTP'),
(4950, 'Oprogramowanie / Programy komputerowe / Programy do multimediów i projektowania / Programy do edycji animacji'),
(4951, 'Oprogramowanie / Programy komputerowe / Programy do multimediów i projektowania / Programy graficzne i ilustracyjne'),
(4952, 'Oprogramowanie / Programy komputerowe / Programy do multimediów i projektowania / Programy do obróbki dźwięku'),
(4953, 'Oprogramowanie / Programy komputerowe / Programy do multimediów i projektowania / Programy do obróbki wideo'),
(4954, 'Oprogramowanie / Programy komputerowe / Programy do multimediów i projektowania / Programy do tworzenia stron www'),
(4955, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do harmonijek'),
(4956, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do harmonijek / Futerały na harmonijki'),
(4957, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Futerały na stroiki do instrumentów drewnianych'),
(4971, 'Dom i ogród / Akcesoria łazienkowe / Dozowniki mydła i płynów'),
(4972, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Akcesoria do narzędzi ogrodniczych / Części do taczek'),
(4973, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Środki czyszczące do gospodarstwa domowego'),
(4974, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Środki czyszczące do gospodarstwa domowego / Środki czyszczące do dywanów'),
(4975, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Środki czyszczące do gospodarstwa domowego / Płyny do mycia naczyń i mydła'),
(4976, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Środki czyszczące do gospodarstwa domowego / Środki do czyszczenia powierzchni szklanych'),
(4977, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Środki czyszczące do gospodarstwa domowego / Środki do czyszczenia podłóg drewnianych'),
(4978, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Środki czyszczące do gospodarstwa domowego / Środki do czyszczenia piekarnika'),
(4979, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Środki czyszczące do gospodarstwa domowego / Neutralizatory zapachów zwierzęcych i wywabiacze plam'),
(4980, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Środki czyszczące do gospodarstwa domowego / Środki do czyszczenia toalet'),
(4981, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Środki czyszczące do gospodarstwa domowego / Środki do czyszczenia wanien i ceramiki'),
(4982, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły do pralni / Wybielacze'),
(4983, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Akordeony i koncertyny'),
(4984, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Dudy'),
(4986, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Zestawy do sztuki i rękodzieła / Dziecięce zestawy do majsterkowania'),
(4988, 'Sprzęt / Akcesoria do sprzętu / Plandeki wodoodporne'),
(4989, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla ptaków / Klatki i stojaki dla ptaków'),
(4990, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla ptaków / Karma dla ptaków'),
(4991, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla ptaków / Drabinki i grzędy dla ptaków'),
(4992, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla ptaków / Zabawki dla ptaków'),
(4993, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla ptaków / Smakołyki dla ptaków'),
(4997, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla kotów / Meble dla kotów'),
(4999, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla kotów / Żwirek dla kotów'),
(5000, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla kotów / Kuwety dla kotów'),
(5001, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla kotów / Zabawki dla kotów'),
(5002, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla kotów / Smakołyki dla kotów'),
(5004, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla psów / Ubranka dla psów'),
(5010, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla psów / Zabawki dla psów'),
(5011, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla psów / Smakołyki dla psów'),
(5013, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla małych zwierząt'),
(5014, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla małych zwierząt / Legowiska dla małych zwierząt'),
(5015, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla małych zwierząt / Karma dla małych zwierząt'),
(5016, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla małych zwierząt / Akcesoria do terrariów dla małych zwierząt'),
(5017, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla małych zwierząt / Pomieszczenia i klatki dla małych zwierząt'),
(5019, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla rybek / Ozdoby akwariowe'),
(5020, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla rybek / Filtry akwariowe'),
(5021, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla rybek / Żwirek i podłoża akwariowe'),
(5023, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla rybek / Szafki pod akwaria'),
(5024, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla rybek / Pokarm dla rybek'),
(5025, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Produkty do pielęgnacji koni / Artykuły do pielęgnacji koni'),
(5026, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla gadów i płazów / Karma dla gadów i płazów'),
(5027, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla gadów i płazów / Akcesoria do terrariów dla gadów i płazów'),
(5028, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla gadów i płazów / Ogrzewanie i oświetlanie terrariów dla gadów i płazów'),
(5029, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla gadów i płazów / Terraria dla gadów i płazów'),
(5030, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla gadów i płazów / Podłoża dla gadów i płazów'),
(5032, 'Oprogramowanie / Towary i waluty cyfrowe'),
(5034, 'Oprogramowanie / Towary i waluty cyfrowe / Ikony'),
(5035, 'Oprogramowanie / Towary i waluty cyfrowe / Tapety pulpitu'),
(5036, 'Oprogramowanie / Towary i waluty cyfrowe / Czcionki'),
(5037, 'Media / Instrukcje obsługi'),
(5038, 'Media / Instrukcje obsługi / Instrukcje obsługi elektroniki'),
(5039, 'Media / Instrukcje obsługi / Instrukcje obsługi AGD'),
(5040, 'Media / Instrukcje obsługi / Instrukcje obsługi urządzeń kuchennych'),
(5041, 'Media / Instrukcje obsługi / Instrukcje naprawy pojazdów'),
(5042, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do kruszarek do lodu'),
(5043, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Słomki do picia i mieszadełka'),
(5044, 'Meble / Meble zewnętrzne / Siedzenia ogrodowe / Ławki ogrodowe'),
(5046, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do harmonijek / Uchwyty na harmonijki'),
(5048, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Części do prysznica / Filtry do wody do prysznica'),
(5049, 'Dzieci i niemowlęta / Bezpieczeństwo niemowląt / Szelki i smycze dla dzieci'),
(5050, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Jazda na nartach i snowboardzie / Akcesoria do gogli narciarskich i snowboardowych / Szkła do gogli narciarskich i snowboardowych'),
(5054, 'Żywność, napoje i tytoń / Żywność / Wypieki / Krówki'),
(5055, 'Artykuły dla dorosłych / Artykuły erotyczne / Żywność i jadalne akcesoria erotyczne'),
(5056, 'Dom i ogród / Artykuły gospodarstwa domowego / Akcesoria do zgniatarek śmieci'),
(5057, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Maszynki do mleka sojowego'),
(5059, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Organizery kuchenne / Pojemniki na słomki'),
(5060, 'Dom i ogród / Kuchnia i jadalnia / Transportery do żywności i napojów / Pokrywki do pojemników na napoje'),
(5062, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Drut do rękodzieła / Kolorowe wyciory do fajki'),
(5065, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do ekspresów do kawy i espresso / Filtry wody do ekspresów do kawy'),
(5066, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do ekspresów do kawy i espresso / Dzbanki do spieniania'),
(5067, 'Artykuły dla dorosłych / Broń / Konserwacja i akcesoria do broni palnej / Latarki na broń palną'),
(5068, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Narzędzia do naprawy pojazdów / Urządzenia rozruchowe'),
(5070, 'Artykuły biurowe / Sortowanie i utrzymanie porządku / Organizery na kartki okolicznościowe'),
(5071, 'Zdrowie i uroda / Opieka zdrowotna / Akcesoria do monitorów funkcji życiowych'),
(5072, 'Zdrowie i uroda / Opieka zdrowotna / Akcesoria do monitorów funkcji życiowych / Akcesoria do wag osobowych'),
(5075, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do maszynek do popcornu'),
(5076, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do maszyn do waty cukrowej'),
(5077, 'Sprzęt / Narzędzia / Frezarki'),
(5078, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Widelce do drobiu'),
(5079, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla rybek / Oświetlenie do akwariów'),
(5080, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Maty i kratki do zlewu'),
(5081, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Witaminy i suplementy diety dla zwierząt'),
(5082, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla kotów / Ubranka dla kotów'),
(5084, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły do pralni / Kule do prania'),
(5085, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły do pralni / Torebki i koszyki do prania'),
(5086, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Leki dla zwierząt'),
(5087, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Kojce dla zwierząt'),
(5088, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Jazda na nartach i snowboardzie / Części do wiązań snowboardowych'),
(5089, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do wentylatorów'),
(5090, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Maszynki do temperingu czekolady'),
(5091, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Wręczanie prezentów / Pakowanie prezentów / Pudełka i puszki na prezenty'),
(5092, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Dzwonki i zawieszki dla zwierząt'),
(5093, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Identyfikatory dla zwierząt'),
(5094, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla psów / Budy dla psów'),
(5096, 'Oprogramowanie / Programy komputerowe / Programy do multimediów i projektowania / Programy do komponowania muzyki'),
(5097, 'Biznes i przemysł / Usługi gastronomiczne / Pojemniki na wynos'),
(5098, 'Biznes i przemysł / Usługi gastronomiczne / Zastawa jednorazowa / Miski jednorazowe'),
(5099, 'Biznes i przemysł / Usługi gastronomiczne / Zastawa jednorazowa / Kubki jednorazowe'),
(5100, 'Biznes i przemysł / Usługi gastronomiczne / Zastawa jednorazowa / Sztućce jednorazowe'),
(5101, 'Biznes i przemysł / Usługi gastronomiczne / Zastawa jednorazowa / Talerze jednorazowe'),
(5102, 'Biznes i przemysł / Usługi gastronomiczne / Opakowania do przekąsek'),
(5103, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Podgrzewacze do żywności'),
(5104, 'Biznes i przemysł / Usługi gastronomiczne / Podgrzewacze do naczyń i potraw'),
(5105, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Cukier do waty cukrowej'),
(5106, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Ochraniacze motocyklowe / Gogle motocyklowe'),
(5107, 'Sprzęt sportowy / Fitness / Joga i pilates / Pokrowce na maty do jogi'),
(5109, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Praski'),
(5110, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Garnki / Prasy do grilla'),
(5111, 'Zdrowie i uroda / Higiena osobista / Golenie i strzyżenie / Zestawy do golenia'),
(5112, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Pamięci komputerowe / Duplikatory dysków / Duplikatory dysków USB'),
(5113, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Rękawiczki do sprzątania'),
(5114, 'Ubrania i akcesoria / Akcesoria do ubrań / Wachlarze dekoracyjne'),
(5119, 'Sprzęt / Akcesoria do sprzętu / Łańcuchy, druty i sznury / Dratwy'),
(5120, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Akcesoria do narzędzi dla artystów i rękodzielników / Pedały do maszyn do szycia'),
(5121, 'Meble / Stoły / Stoły pod maszyny do szycia'),
(5122, 'Ubrania i akcesoria / Biżuteria / Akcesoria do zegarków'),
(5123, 'Ubrania i akcesoria / Biżuteria / Akcesoria do zegarków / Paski do zegarków'),
(5124, 'Zdrowie i uroda / Czyszczenie i pielęgnacja biżuterii / Zestawy do naprawy zegarków'),
(5127, 'Oprogramowanie / Programy komputerowe / Słowniki i translatory'),
(5128, 'Dom i ogród / Artykuły gospodarstwa domowego / Przechowywanie i utrzymanie porządku / Skrzynki na sztućce'),
(5129, 'Elektronika / Audio / Elementy audio / Transmitery audio'),
(5130, 'Elektronika / Audio / Elementy audio / Transmitery audio / Transmitery Bluetooth'),
(5133, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Akumulatory / Baterie do PDA'),
(5134, 'Dom i ogród / Kuchnia i jadalnia / Przechowywanie żywności / Słoiki na miód'),
(5135, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Naczynia do serwowania żywności / Podkłady okrągłe pod torty'),
(5136, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do wycinania i wytłaczania / Skalpele modelarskie'),
(5137, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Maty do cięcia'),
(5138, 'Dom i ogród / Sprzęt AGD / Urządzenia do prania / Żelazka parowe'),
(5139, 'Dom i ogród / Sprzęt AGD / Urządzenia do prania / Żelazka i systemy do prasowania'),
(5140, 'Dom i ogród / Sprzęt AGD / Urządzenia do prania / Prasowalnice'),
(5141, 'Dom i ogród / Akcesoria łazienkowe / Suszarki do rąk'),
(5142, 'Sprzęt / Materiały elektryczne / Falowniki'),
(5143, 'Dom i ogród / Artykuły gospodarstwa domowego / Przechowywanie śmieci / Pojemniki na odpady niebezpieczne'),
(5144, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Kagańce dla zwierząt'),
(5145, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Kołnierze weterynaryjne dla zwierząt'),
(5150, 'Sztuka i rozrywka / Hobby i sztuki piękne / Modelarstwo / Modele pociągów i zestawy pociągów'),
(5151, 'Sztuka i rozrywka / Hobby i sztuki piękne / Modelarstwo / Akcesoria do modeli pociągów'),
(5152, 'Gry i zabawki / Zabawki / Pojazdy do zabawy / Kolejki i zestawy'),
(5153, 'Gry i zabawki / Zabawki / Akcesoria do pojazdów do zabawy / Akcesoria do kolejek'),
(5154, 'Zdrowie i uroda / Higiena osobista / Higiena jamy ustnej / Wyciskacze i dozowniki do pasty do zębów'),
(5155, 'Zdrowie i uroda / Higiena osobista / Higiena jamy ustnej / Kleje do protez'),
(5156, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Maszyny do produkcji lodów'),
(5157, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Organizery kuchenne / Stojaki i uchwyty na noże'),
(5158, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do prania / Akcesoria do żelazek parowych'),
(5159, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do prania / Akcesoria do żelazek'),
(5160, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do prania / Akcesoria do prasowalnic'),
(5161, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla rybek / Preparaty do wody w akwariach'),
(5162, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Pojemniki na karmę dla zwierząt'),
(5163, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Łyżki do pokarmu dla zwierząt'),
(5164, 'Zdrowie i uroda / Opieka zdrowotna / Artykuły dla osób mających trudności z poruszaniem się / Pomoce ułatwiające chodzenie'),
(5165, 'Zdrowie i uroda / Opieka zdrowotna / Artykuły dla osób mających trudności z poruszaniem się / Pomoce ułatwiające chodzenie / Laski'),
(5166, 'Zdrowie i uroda / Opieka zdrowotna / Artykuły dla osób mających trudności z poruszaniem się / Pomoce ułatwiające chodzenie / Balkoniki rehabilitacyjne'),
(5167, 'Biznes i przemysł / Branża medyczna / Meble medyczne'),
(5168, 'Biznes i przemysł / Branża medyczna / Meble medyczne / Stoły do chiropraktyki'),
(5169, 'Biznes i przemysł / Branża medyczna / Meble medyczne / Fotele i stoły do badań lekarskich'),
(5170, 'Biznes i przemysł / Branża medyczna / Meble medyczne / Szafy medyczne'),
(5171, 'Biznes i przemysł / Branża medyczna / Meble medyczne / Wózki medyczne'),
(5172, 'Biznes i przemysł / Branża medyczna / Meble medyczne / Stoły chirurgiczne'),
(5173, 'Biznes i przemysł / Branża medyczna / Meble medyczne / Wózki medyczne / Wózki reanimacyjne'),
(5174, 'Biznes i przemysł / Branża medyczna / Meble medyczne / Wózki medyczne / Stojaki na kroplówki i wózki'),
(5181, 'Torby, walizki i akcesoria podróżne'),
(5182, 'Ubrania i akcesoria / Ubrania / Stroje jednoczęściowe'),
(5183, 'Ubrania i akcesoria / Ubrania / Garnitury / Garnitury ze spodniami'),
(5192, 'Ubrania i akcesoria / Przebrania i akcesoria / Akcesoria do przebrań'),
(5193, 'Ubrania i akcesoria / Przebrania i akcesoria / Przebrania'),
(5194, 'Ubrania i akcesoria / Przebrania i akcesoria / Maski'),
(5207, 'Ubrania i akcesoria / Akcesoria do ubrań / Chusteczki'),
(5250, 'Ubrania i akcesoria / Ubrania / Stroje jednoczęściowe / Kombinezony jednoczęściowe'),
(5251, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Nożyce kuchenne'),
(5252, 'Dzieci i niemowlęta / Zdrowie niemowląt'),
(5253, 'Dzieci i niemowlęta / Zdrowie niemowląt / Aspiratory do nosa'),
(5254, 'Elektronika / Komputery / Komputery typu barebone'),
(5255, 'Elektronika / Komputery / Infokioski'),
(5256, 'Elektronika / Komputery / Palmtopy / Kolektory danych'),
(5257, 'Elektronika / Wideo / Akcesoria do wideo / Akcesoria do projektorów / Uchwyty do projektorów'),
(5258, 'Elektronika / Drukowanie, kopiowanie, skanowanie i faksowanie / Akcesoria do kopiarek, drukarek i faksów / Materiały eksploatacyjne do drukarek'),
(5259, 'Elektronika / Drukowanie, kopiowanie, skanowanie i faksowanie / Akcesoria do kopiarek, drukarek i faksów / Materiały eksploatacyjne do drukarek / Bębny i zestawy bębnów do drukarek'),
(5260, 'Elektronika / Drukowanie, kopiowanie, skanowanie i faksowanie / Akcesoria do kopiarek, drukarek i faksów / Materiały eksploatacyjne do drukarek / Taśmy do drukarek'),
(5261, 'Elektronika / Drukowanie, kopiowanie, skanowanie i faksowanie / Akcesoria do kopiarek, drukarek i faksów / Materiały eksploatacyjne do drukarek / Głowice do drukarek'),
(5262, 'Elektronika / Drukowanie, kopiowanie, skanowanie i faksowanie / Akcesoria do kopiarek, drukarek i faksów / Materiały eksploatacyjne do drukarek / Zestawy do konserwacji drukarek'),
(5264, 'Artykuły biurowe / Artykuły różne / Artykuły papiernicze / Puste identyfikatory'),
(5265, 'Elektronika / Drukowanie, kopiowanie, skanowanie i faksowanie / Akcesoria do kopiarek, drukarek i faksów / Dupleksy do drukarek'),
(5266, 'Elektronika / Drukowanie, kopiowanie, skanowanie i faksowanie / Akcesoria do kopiarek, drukarek i faksów / Materiały eksploatacyjne do drukarek / Filtry do drukarek'),
(5268, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Pamięci komputerowe / Duplikatory dysków'),
(5269, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Pamięci komputerowe / Sieciowe systemy pamięci'),
(5271, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Pamięci komputerowe / Duplikatory dysków / Duplikatory dysków twardych'),
(5272, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Pamięci komputerowe / Macierze dyskowe'),
(5273, 'Elektronika / Akcesoria elektroniczne / Zarządzanie kablami / Krosownice'),
(5274, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Obudowy zasilaczy'),
(5275, 'Elektronika / Telekomunikacja / Identyfikatory dzwoniącego'),
(5276, 'Elektronika / Wideo / Odtwarzacze i nagrywarki wideo / Domowe centra multimedialne'),
(5278, 'Elektronika / Wideo / Serwery wideo'),
(5280, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Obiektywy do aparatów i kamer / Obiektywy do kamer wideo'),
(5282, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria optyczne / Akcesoria do lornetek i lunet'),
(5283, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria optyczne / Akcesoria do dalmierzy'),
(5284, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria optyczne / Akcesoria do teleskopów'),
(5286, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Ekspresy do kawy i espresso / Próżniowe ekspresy do kawy'),
(5287, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Podgrzewacze do napojów'),
(5289, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Tostery i grille'),
(5291, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Tostery i grille / Maszynki do pizzelle'),
(5292, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Tostery i grille / Maszynki do tortilli i pity'),
(5294, 'Dom i ogród / Sprzęt AGD / Polerki do podłóg'),
(5295, 'Zdrowie i uroda / Higiena osobista / Higiena jamy ustnej / Irygatory dentystyczne'),
(5296, 'Dzieci i niemowlęta / Karmienie / Podgrzewacze butelek i sterylizatory'),
(5298, 'Dzieci i niemowlęta / Karmienie / Poduszki do karmienia'),
(5299, 'Oprogramowanie / Programy komputerowe / Programy antywirusowe i zabezpieczające'),
(5300, 'Oprogramowanie / Programy komputerowe / Programy dla firm i do pracy'),
(5301, 'Oprogramowanie / Programy komputerowe / Programy pomocnicze i konserwacyjne'),
(5302, 'Oprogramowanie / Programy komputerowe / Programy sieciowe'),
(5303, 'Oprogramowanie / Programy komputerowe / Programy biurowe'),
(5304, 'Oprogramowanie / Programy komputerowe / Programy finansowe, podatkowe i księgowe'),
(5308, 'Elektronika / Akcesoria elektroniczne / Akcesoria komputerowe / Rysiki'),
(5309, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Urządzenia wejściowe / Długopisy cyfrowe'),
(5310, 'Biznes i przemysł / Handel detaliczny / Obrót środkami pieniężnymi / Kasy fiskalne i produkty związane z obsługą terminali / Pady do składania podpisu'),
(5311, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Broń kolekcjonerska / Noże kolekcjonerskie'),
(5312, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Nurkowanie i snorkeling / Noże do nurkowania'),
(5317, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja włosów / Akcesoria do stylizacji włosów / Wałki do włosów i spinki do wałków'),
(5318, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Tostery i grille / Maszynki do muffinów i babeczek'),
(5319, 'Sprzęt sportowy / Fitness / Wałki do ćwiczeń'),
(5322, 'Ubrania i akcesoria / Ubrania / Ubrania sportowe'),
(5327, 'Ubrania i akcesoria / Ubrania / Bielizna i skarpety / Suspensoria'),
(5329, 'Ubrania i akcesoria / Ubrania / Suknie ślubne i sukienki na wesele / Suknie ślubne'),
(5330, 'Ubrania i akcesoria / Ubrania / Suknie ślubne i sukienki na wesele / Sukienki na wesele'),
(5338, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja skóry / Samoopalacze, kosmetyki brązujące i podkreślające opaleniznę / Samoopalacze'),
(5339, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja skóry / Samoopalacze, kosmetyki brązujące i podkreślające opaleniznę / Przyspieszacze opalania'),
(5340, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Garnki / Tażiny'),
(5343, 'Ubrania i akcesoria / Ubrania / Stroje tradycyjne i ceremonialne / Kimona'),
(5344, 'Ubrania i akcesoria / Ubrania / Spódniczki ze spodenkami'),
(5346, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Obiektywy do aparatów i kamer / Obiektywy do kamer przemysłowych'),
(5349, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Podgrzewacze do żywności / Lampy do podgrzewania żywności'),
(5359, 'Dom i ogród / Ozdoby / Ozdoby sezonowe i świąteczne / Kalendarze adwentowe'),
(5362, 'Dom i ogród / Trawnik i ogród / Odśnieżanie'),
(5363, 'Dom i ogród / Trawnik i ogród / Odśnieżanie / Łopaty do odśnieżania'),
(5364, 'Dom i ogród / Trawnik i ogród / Odśnieżanie / Skrobaczki do lodu i szczotki do śniegu'),
(5366, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Urządzenia wejściowe / Czytniki linii papilarnych'),
(5367, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych / Akcesoria do gitar / Kapodastery'),
(5368, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych / Akcesoria do gitar / Slidery do gitar'),
(5369, 'Elektronika / Audio / Elementy audio / Procesory sygnałowe / Przedwzmacniacze gramofonowe'),
(5370, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Sitka do mąki'),
(5371, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Liczniki Geigera'),
(5378, 'Ubrania i akcesoria / Ubrania / Ubrania sportowe / Szorty bokserskie'),
(5379, 'Ubrania i akcesoria / Ubrania / Ubrania sportowe / Spodenki do sztuk walki'),
(5380, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Urządzenia do ochrony przeciwprzepięciowej'),
(5381, 'Elektronika / Audio / Elementy audio / Wzmacniacze audio / Wzmacniacze mocy'),
(5385, 'Ubrania i akcesoria / Akcesoria do butów / Ochraniacze na buty'),
(5387, 'Ubrania i akcesoria / Przebrania i akcesoria / Buty do przebrań'),
(5388, 'Ubrania i akcesoria / Ubrania / Stroje tradycyjne i ceremonialne'),
(5390, 'Ubrania i akcesoria / Akcesoria do ubrań / Akcesoria do strojów tradycyjnych'),
(5394, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wspinaczka / Odzież i akcesoria do wspinaczki / Raki'),
(5395, 'Elektronika / Audio / Akcesoria audio / Akcesoria do systemów karaoke'),
(5396, 'Elektronika / Audio / Akcesoria audio / Akcesoria do systemów karaoke / Kartridże do karaoke'),
(5399, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Odzież żeglarska i do uprawiania sportów wodnych / Pianki w częściach / Pianki - akcesoria'),
(5400, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Odzież żeglarska i do uprawiania sportów wodnych / Pianki w częściach / Pianki - części dolne'),
(5401, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Odzież żeglarska i do uprawiania sportów wodnych / Pianki w częściach / Pianki - części górne'),
(5402, 'Aparaty, kamery i przyrządy optyczne / Aparaty i kamery / Kamery myśliwskie'),
(5403, 'Gry i zabawki / Gry / Żetony i zestawy pokerowe'),
(5404, 'Elektronika / Telekomunikacja / Akcesoria do interkomów'),
(5406, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Spodniobuty'),
(5408, 'Ubrania i akcesoria / Ubrania / Ubrania dla dzieci i niemowląt / Spodnie dla dzieci i niemowląt'),
(5409, 'Ubrania i akcesoria / Ubrania / Ubrania dla dzieci i niemowląt / Stroje kąpielowe dla dzieci i niemowląt'),
(5410, 'Ubrania i akcesoria / Ubrania / Ubrania dla dzieci i niemowląt / Topy dla dzieci i niemowląt'),
(5411, 'Ubrania i akcesoria / Ubrania / Ubrania dla dzieci i niemowląt / Pajacyki i body dla niemowląt'),
(5412, 'Ubrania i akcesoria / Ubrania / Ubrania dla dzieci i niemowląt / Piżamy i beciki dla niemowląt'),
(5422, 'Ubrania i akcesoria / Akcesoria do ubrań / Akcesoria do ubrań dziecięcych i niemowlęcych'),
(5423, 'Ubrania i akcesoria / Ubrania / Ubrania dla dzieci i niemowląt / Skarpety i rajstopy dla dzieci'),
(5424, 'Ubrania i akcesoria / Ubrania / Ubrania dla dzieci i niemowląt / Sukienki dla dzieci i niemowląt'),
(5425, 'Ubrania i akcesoria / Ubrania / Ubrania dla dzieci i niemowląt / Ubrania wierzchnie dla dzieci i niemowląt'),
(5426, 'Ubrania i akcesoria / Przebrania i akcesoria / Akcesoria do przebrań / Kapelusze do przebrań'),
(5429, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria i części do aparatów / Paski do aparatów i kamer'),
(5431, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Oświetlenie i studio / Kontrolery oświetlenia / Blendy'),
(5432, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Oświetlenie i studio / Kontrolery oświetlenia / Softboksy'),
(5434, 'Elektronika / Audio / Odtwarzacze i nagrywarki audio / Nagrywarki wielośladowe'),
(5435, 'Elektronika / Audio / Elementy audio / Procesory sygnałowe / Crossovery'),
(5438, 'Pojazdy i części / Akcesoria i części do pojazdów / Wyposażenie elektroniczne pojazdów silnikowych / Odtwarzacze kasetowe'),
(5441, 'Ubrania i akcesoria / Ubrania / Suknie ślubne i sukienki na wesele'),
(5443, 'Ubrania i akcesoria / Akcesoria do ubrań / Akcesoria dla panny młodej'),
(5446, 'Ubrania i akcesoria / Akcesoria do ubrań / Akcesoria dla panny młodej / Welony ślubne'),
(5450, 'Elektronika / Wideo / Transmitery wideo'),
(5452, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Upominki dla gości'),
(5453, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Upominki dla gości / Upominki dla gości weselnych'),
(5455, 'Artykuły religijne i dewocjonalia / Artykuły ślubne'),
(5456, 'Artykuły religijne i dewocjonalia / Artykuły ślubne / Koszyki dla druhen'),
(5457, 'Artykuły religijne i dewocjonalia / Artykuły ślubne / Poduszki na obrączki'),
(5459, 'Elektronika / Drukowanie, kopiowanie, skanowanie i faksowanie / Akcesoria do kopiarek, drukarek i faksów / Stojaki do drukarek'),
(5460, 'Ubrania i akcesoria / Ubrania / Ubrania sportowe / Ubrania na polowanie'),
(5461, 'Ubrania i akcesoria / Ubrania / Ubrania sportowe / Ubrania na polowanie / Kamizelki dla myśliwych i wędkarzy'),
(5462, 'Ubrania i akcesoria / Ubrania / Ubrania sportowe / Ubrania na polowanie / Ghillie Suits'),
(5463, 'Ubrania i akcesoria / Ubrania / Ubrania sportowe / Stroje ochronne na motor / Kombinezony motocyklowe'),
(5466, 'Elektronika / Akcesoria elektroniczne / Folie i osłonki na sprzęt elektroniczny'),
(5467, 'Elektronika / Akcesoria elektroniczne / Folie i osłonki na sprzęt elektroniczny / Filtry prywatności'),
(5468, 'Elektronika / Akcesoria elektroniczne / Folie i osłonki na sprzęt elektroniczny / Osłony na ekran'),
(5469, 'Elektronika / Akcesoria elektroniczne / Folie i osłonki na sprzęt elektroniczny / Nakładki na klawiaturę'),
(5471, 'Elektronika / Wideo / Akcesoria do wideo / Akcesoria i części do telewizorów / Wymienne lampy do telewizorów'),
(5472, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Stojaki na winietki'),
(5473, 'Elektronika / Akcesoria elektroniczne / Wzmacniacze sygnału'),
(5476, 'Elektronika / Akcesoria elektroniczne / Akcesoria do anten'),
(5477, 'Elektronika / Akcesoria elektroniczne / Akcesoria do anten / Uchwyty i wsporniki do anten'),
(5478, 'Elektronika / Akcesoria elektroniczne / Akcesoria do anten / Rotory antenowe'),
(5479, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria i części do aparatów / Akcesoria do lamp błyskowych'),
(5481, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty dęte drewniane / Harmonijki klawiszowe'),
(5483, 'Ubrania i akcesoria / Ubrania / Stroje tradycyjne i ceremonialne / Stroje na ceremonie religijne'),
(5484, 'Ubrania i akcesoria / Ubrania / Uniformy / Spodnie robocze i kombinezony robocze'),
(5487, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Odległościomierze'),
(5488, 'Zdrowie i uroda / Opieka zdrowotna / Artykuły dla osób mających trudności z poruszaniem się / Meble i armatura ułatwiające poruszanie się'),
(5489, 'Elektronika / Akcesoria elektroniczne / Akcesoria komputerowe / Podnośniki do monitorów i stoliki na laptopy'),
(5490, 'Ubrania i akcesoria / Ubrania / Stroje jednoczęściowe / Trykoty i długie kombinezony'),
(5491, 'Dom i ogród / Bezpieczeństwo domu i biura / Atrapy kamer przemysłowych'),
(5493, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Akcesoria do parasoli i pawilonów ogrodowych / Podstawy parasoli ogrodowych'),
(5494, 'Dom i ogród / Artykuły gospodarstwa domowego / Przechowywanie i utrzymanie porządku / Haczyki i stojaki do przechowywania / Stojaki na parasolki'),
(5496, 'Elektronika / Sieci / Mostki i routery / Bezprzewodowe punkty dostępowe'),
(5497, 'Elektronika / Sieci / Mostki i routery / Routery bezprzewodowe'),
(5499, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Oświetlenie i studio / Akcesoria do światłomierzy'),
(5502, 'Artykuły biurowe / Artykuły różne / Etykiety i przekładki indeksujące / Taśmy do etykietowania'),
(5503, 'Elektronika / Wideo / Akcesoria do wideo / Akcesoria i części do telewizorów / Konwertery telewizyjne'),
(5504, 'Dom i ogród / Ozdoby / Ozdoby sezonowe i świąteczne / Osłony na stojaki choinkowe'),
(5506, 'Ubrania i akcesoria / Ubrania / Odzież wierzchnia / Skórzane spodnie kowbojskie'),
(5507, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja oczu / Akcesoria do okularów / Futerały i uchwyty do okularów'),
(5508, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Akcesoria do wanien / Obudowy do wanien'),
(5512, 'Pojazdy i części / Akcesoria i części do pojazdów / Przewóz i przechowywanie bagażu / Sakwy i torby motocyklowe'),
(5513, 'Ubrania i akcesoria / Ubrania / Piżamy i ubrania na co dzień / Koszule nocne'),
(5514, 'Ubrania i akcesoria / Ubrania / Odzież wierzchnia / Spodnie przeciwdeszczowe'),
(5515, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Mierniki jakości powietrza'),
(5516, 'Elektronika / Wideo / Akcesoria do wideo / Akcesoria do monitorów komputerowych / Kalibratory kolorów'),
(5517, 'Ubrania i akcesoria / Ubrania / Ubrania sportowe / Stroje ochronne na motor'),
(5521, 'Dom i ogród / Ozdoby / Akcesoria do karmników ogrodowych'),
(5523, 'Elektronika / Akcesoria elektroniczne / Folie i osłonki na sprzęt elektroniczny / Naklejki i kalkomanie na sprzęt elektroniczny'),
(5524, 'Gry i zabawki / Zabawki ogrodowe / Akcesoria do trampolin'),
(5525, 'Pojazdy i części / Akcesoria i części do pojazdów / Wyposażenie elektroniczne pojazdów silnikowych / Adaptery kasetowe'),
(5526, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do narzędzi pomiarowych i czujników'),
(5529, 'Gry i zabawki / Zabawki / Zabawki edukacyjne / Zabawki do nauki czytania'),
(5531, 'Artykuły biurowe / Sortowanie i utrzymanie porządku / Fiszki'),
(5532, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria i części do aparatów / Piloty do aparatów i kamer'),
(5533, 'Sprzęt / Materiały elektryczne / Przetwornice napięcia'),
(5537, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Zastawy obiadowe / Komplety obiadowe'),
(5540, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Akcesoria elektroniczne do rowerów'),
(5542, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria optyczne / Akcesoria do lunet obserwacyjnych'),
(5543, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria optyczne / Akcesoria do celowników'),
(5545, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria optyczne / Torby i futerały na przyrządy optyczne'),
(5546, 'Dom i ogród / Baseny i spa / Akcesoria do basenów i spa / Zjeżdżalnie wodne'),
(5547, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Ochraniacze motocyklowe'),
(5549, 'Ubrania i akcesoria / Ubrania / Ubrania dla dzieci i niemowląt / Ceratki na pieluchy dla dzieci i niemowląt'),
(5551, 'Zdrowie i uroda / Opieka zdrowotna / Monitory funkcji życiowych / Pulsoksymetry'),
(5552, 'Ubrania i akcesoria / Ubrania / Ubrania sportowe / Ubrania na polowanie / Spodnie na polowanie i wojskowe'),
(5555, 'Ubrania i akcesoria / Ubrania / Ubrania sportowe / Ubrania do paintballu'),
(5556, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do narzędzi pomiarowych i czujników / Akcesoria do wykrywaczy gazu'),
(5557, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do narzędzi pomiarowych i czujników / Akcesoria do wykrywaczy metalu i napięcia'),
(5558, 'Dom i ogród / Artykuły gospodarstwa domowego / Przechowywanie i utrzymanie porządku / Przechowywanie ubrań'),
(5559, 'Dom i ogród / Artykuły gospodarstwa domowego / Przechowywanie i utrzymanie porządku / Przechowywanie ubrań / Stojaki i pojemniki na buty'),
(5561, 'Elektronika / Wideo / Telewizja satelitarna i kablowa'),
(5562, 'Elektronika / Wideo / Telewizja satelitarna i kablowa / Odbiorniki telewizji kablowej'),
(5564, 'Ubrania i akcesoria / Ubrania / Uniformy / Stroje sportowe / Stroje do piłki nożnej'),
(5566, 'Elektronika / Telekomunikacja / Telefonia / Akcesoria do telefonów komórkowych / Podstawki pod telefony komórkowe'),
(5567, 'Ubrania i akcesoria / Akcesoria do butów / Ocieplacze do butów'),
(5569, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Produkty do pielęgnacji koni / Buty i ocieplacze dla koni'),
(5570, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do maszynek do jogurtu'),
(5571, 'Sprzęt / Akcesoria do narzędzi / Gniazda narzędziowe'),
(5572, 'Pojazdy i części / Akcesoria i części do pojazdów / Wyposażenie elektroniczne pojazdów silnikowych / Zestawy głośnomówiące'),
(5573, 'Zdrowie i uroda / Czyszczenie i pielęgnacja biżuterii'),
(5576, 'Elektronika / Sieci / Akcesoria do modemów'),
(5577, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Syfony do napojów'),
(5578, 'Meble / Szafy i przechowywanie / Regały na wino'),
(5579, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Kitesurfing'),
(5580, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Kitesurfing / Deski do kitesurfingu'),
(5581, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Kitesurfing / Elementy desek do kitesurfingu'),
(5582, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Kitesurfing / Latawce do kitesurfingu'),
(5583, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Kitesurfing / Uprzęże do kitesurfingu i windsurfingu'),
(5584, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Kitesurfing / Pokrowce na deski do kitesurfingu'),
(5587, 'Sprzęt / Narzędzia / Elektronarzędzia wielofunkcyjne'),
(5588, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria do obiektywów / Adaptery do obiektywów i filtrów'),
(5589, 'Elektronika / Akcesoria elektroniczne / Zagłuszacze sygnału / Zagłuszacze radarów'),
(5590, 'Dom i ogród / Rośliny / Rośliny wodne'),
(5591, 'Biznes i przemysł / Środki ochrony osobistej / Rękawice ochronne'),
(5592, 'Sprzęt / Narzędzia / Lampy robocze'),
(5593, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Rząd koński'),
(5594, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Sprzęt jeździecki'),
(5596, 'Elektronika / Audio / Elementy audio / Procesory sygnałowe / Bramki szumów i kompresory'),
(5597, 'Elektronika / Audio / Elementy audio / Procesory sygnałowe / Systemy do zarządzania PA'),
(5598, 'Ubrania i akcesoria / Ubrania / Odzież wierzchnia / Płaszcze i kurtki'),
(5599, 'Elektronika / Wideo / Akcesoria do wideo / Akcesoria do projektorów / Osłony do projektorów i statywów'),
(5600, 'Dom i ogród / Artykuły gospodarstwa domowego / Środki i narzędzia do pielęgnacji obuwia / Przyrządy do zdejmowania butów'),
(5601, 'Dom i ogród / Artykuły gospodarstwa domowego / Środki i narzędzia do pielęgnacji obuwia / Łyżki do butów'),
(5602, 'Biznes i przemysł / Branża medyczna / Czepki lekarskie'),
(5604, 'Dom i ogród / Artykuły gospodarstwa domowego / Środki i narzędzia do pielęgnacji obuwia / Środki do pielęgnacji i farbowania butów'),
(5605, 'Artykuły religijne i dewocjonalia'),
(5606, 'Artykuły religijne i dewocjonalia / Artykuły pogrzebowe'),
(5607, 'Artykuły religijne i dewocjonalia / Artykuły pogrzebowe / Urny'),
(5608, 'Torby, walizki i akcesoria podróżne / Torby na zakupy'),
(5609, 'Dom i ogród / Ozdoby / Figurki'),
(5612, 'Elektronika / Akcesoria elektroniczne / Zagłuszacze sygnału / Zagłuszacze sygnału GPS'),
(5613, 'Pojazdy i części / Akcesoria i części do pojazdów'),
(5614, 'Pojazdy i części / Pojazdy'),
(5620, 'Torby, walizki i akcesoria podróżne / Akcesoria podróżne / Organizery podróżne'),
(5621, 'Ubrania i akcesoria / Ubrania / Ubrania dla dzieci i niemowląt / Bielizna dziecięca'),
(5622, 'Ubrania i akcesoria / Ubrania / Ubrania dla dzieci i niemowląt / Komplety dla dzieci i niemowląt'),
(5623, 'Ubrania i akcesoria / Akcesoria do ubrań / Akcesoria do ubrań dziecięcych i niemowlęcych / Paski dla dzieci i niemowląt'),
(5624, 'Ubrania i akcesoria / Akcesoria do ubrań / Akcesoria do ubrań dziecięcych i niemowlęcych / Rękawiczki i mitenki dla dzieci i niemowląt'),
(5625, 'Ubrania i akcesoria / Akcesoria do ubrań / Akcesoria do ubrań dziecięcych i niemowlęcych / Kapelusze dla dzieci i niemowląt'),
(5626, 'Ubrania i akcesoria / Akcesoria do ubrań / Akcesoria do ubrań dziecięcych i niemowlęcych / Odzież ochronna dla niemowląt'),
(5627, 'Sprzęt / Materiały elektryczne / Zabezpieczenia do kontaktów'),
(5628, 'Dzieci i niemowlęta / Przewijanie / Maty i lady do przewijania'),
(5629, 'Dzieci i niemowlęta / Karmienie / Ściereczki do odbijania'),
(5630, 'Dzieci i niemowlęta / Karmienie / Smoczki do butelek'),
(5631, 'Dom i ogród / Artykuły gospodarstwa domowego / Przechowywanie i utrzymanie porządku / Pojemniki do przechowywania'),
(5632, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Akcesoria ogrodnicze'),
(5633, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Akcesoria ogrodnicze / Torby na narzędzia ogrodnicze'),
(5635, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Systemy nawadniania'),
(5636, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Worki kompresyjne'),
(5642, 'Dom i ogród / Kuchnia i jadalnia / Przechowywanie żywności / Zawijanie żywności / Papier do pieczenia'),
(5644, 'Pojazdy i części / Pojazdy / Jednostki pływające / Jachty'),
(5646, 'Sprzęt / Hydraulika / Dystrybucja i filtrowanie wody / Sól do zmiękczania wody'),
(5647, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Sztućce / Zestawy sztućców'),
(5650, 'Torby, walizki i akcesoria podróżne / Akcesoria podróżne / Dyskretne saszetki'),
(5651, 'Torby, walizki i akcesoria podróżne / Akcesoria podróżne / Identyfikatory do bagażu'),
(5652, 'Torby, walizki i akcesoria podróżne / Akcesoria podróżne / Troki'),
(5654, 'Dom i ogród / Baseny i spa / Akcesoria do basenów i spa / Podgrzewacze basenów'),
(5655, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Akcesoria do namiotów'),
(5656, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Akcesoria do namiotów / Podłogi do namiotów'),
(5657, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Akcesoria do namiotów / Podłogi absydy'),
(5658, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Akcesoria do namiotów / Maszty i śledzie'),
(5659, 'Elektronika / Sieci / Mostki i routery / Mostki sieciowe'),
(5663, 'Zdrowie i uroda / Higiena osobista / Masaż i relaks'),
(5664, 'Zdrowie i uroda / Higiena osobista / Masaż i relaks / Olejki do masażu'),
(5665, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Akcesoria do toalet i bidetów / Spłuczki'),
(5666, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Akcesoria do toalet i bidetów / Nakładki na spłuczki'),
(5669, 'Elektronika / Akcesoria elektroniczne / Akcesoria komputerowe / Wymienne stalówki do rysików'),
(5670, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do grilla na zewnątrz / Wózki do grilla na zewnątrz'),
(5671, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do grilla na zewnątrz / Pellety do grilla'),
(5672, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do grilla na zewnątrz / Deski do grillowania na zewnątrz'),
(5673, 'Ubrania i akcesoria / Ubrania / Stroje tradycyjne i ceremonialne / Odzież wierzchnia na kimono'),
(5674, 'Ubrania i akcesoria / Ubrania / Stroje tradycyjne i ceremonialne / Spodnie hakama'),
(5676, 'Ubrania i akcesoria / Ubrania / Stroje tradycyjne i ceremonialne / Yukata'),
(5685, 'Ubrania i akcesoria / Akcesoria do ubrań / Akcesoria do strojów tradycyjnych / Skarpety tabi'),
(5687, 'Ubrania i akcesoria / Akcesoria do ubrań / Akcesoria do strojów tradycyjnych / Obi'),
(5690, 'Zdrowie i uroda / Opieka zdrowotna / Aparaty słuchowe'),
(5693, 'Sprzęt sportowy / Fitness / Stopery'),
(5694, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do grilla na zewnątrz / Brykiety węglowe'),
(5695, 'Elektronika / Akcesoria elektroniczne / Zagłuszacze sygnału'),
(5696, 'Elektronika / Akcesoria elektroniczne / Zagłuszacze sygnału / Zagłuszacze sygnału sieci komórkowej'),
(5697, 'Ubrania i akcesoria / Ubrania / Ubrania sportowe / Stroje rowerowe'),
(5702, 'Zdrowie i uroda / Opieka zdrowotna / Zdrowy tryb życia i dieta / Odżywcze napoje i szejki'),
(5704, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły do pralni / Klamerki do bielizny'),
(5705, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły do pralni / Golarki do tkanin'),
(5706, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja uszu / Świece do konchowania uszu'),
(5707, 'Dom i ogród / Artykuły gospodarstwa domowego / Przechowywanie i utrzymanie porządku / Haczyki i stojaki do przechowywania / Haczyki do zastosowań ogólnych'),
(5708, 'Dom i ogród / Ozdoby / Półki do przedpokoju'),
(5709, 'Sztuka i rozrywka / Przyjęcia i świętowanie'),
(5710, 'Sztuka i rozrywka / Hobby i sztuki piękne'),
(5711, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Efekty specjalne / Kule dyskotekowe'),
(5713, 'Ubrania i akcesoria / Ubrania / Piżamy i ubrania na co dzień / Ubrania na co dzień'),
(5714, 'Dom i ogród / Artykuły gospodarstwa domowego / Przechowywanie i utrzymanie porządku / Przechowywanie ubrań / Stojaki na odzież'),
(5716, 'Dom i ogród / Artykuły gospodarstwa domowego / Przechowywanie i utrzymanie porządku / Przechowywanie ubrań / Wieszaki stojące na ubrania'),
(5718, 'Dzieci i niemowlęta / Karmienie / Jedzenie dla dzieci i niemowląt / Soczki dla niemowląt'),
(5719, 'Dzieci i niemowlęta / Karmienie / Jedzenie dla dzieci i niemowląt / Jedzenie dla niemowląt'),
(5720, 'Dzieci i niemowlęta / Karmienie / Jedzenie dla dzieci i niemowląt / Przekąski dla niemowląt'),
(5721, 'Dzieci i niemowlęta / Karmienie / Jedzenie dla dzieci i niemowląt / Płatki dla niemowląt'),
(5723, 'Żywność, napoje i tytoń / Napoje / Napoje izotoniczne i energetyzujące'),
(5724, 'Żywność, napoje i tytoń / Napoje / Mleko roślinne'),
(5725, 'Żywność, napoje i tytoń / Napoje / Napoje alkoholowe / Mieszanki koktajlowe'),
(5740, 'Żywność, napoje i tytoń / Żywność / Dipy i smarowidła'),
(5741, 'Żywność, napoje i tytoń / Żywność / Dipy i smarowidła / Hummus'),
(5742, 'Żywność, napoje i tytoń / Żywność / Dipy i smarowidła / Guacamole'),
(5743, 'Żywność, napoje i tytoń / Żywność / Przekąski / Puddingi i przekąski z żelatyny'),
(5744, 'Żywność, napoje i tytoń / Żywność / Przekąski / Przekąski owocowe'),
(5745, 'Żywność, napoje i tytoń / Żywność / Przekąski / Mieszanka studencka i mieszanki przekąsek'),
(5746, 'Żywność, napoje i tytoń / Żywność / Przekąski / Grzanki'),
(5747, 'Żywność, napoje i tytoń / Żywność / Przekąski / Batony zbożowe i z granoli'),
(5748, 'Żywność, napoje i tytoń / Żywność / Wypieki / Tortille i wrapy');
INSERT INTO `google_kategorie` (`id`, `nazwa`) VALUES
(5749, 'Żywność, napoje i tytoń / Żywność / Wypieki / Placki i tarty'),
(5750, 'Żywność, napoje i tytoń / Żywność / Wypieki / Ciasta i bułki drożdżowe'),
(5751, 'Żywność, napoje i tytoń / Żywność / Wypieki / Pączki'),
(5752, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Surowe ciasta'),
(5753, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Surowe ciasta / Spody z kruchego ciasta'),
(5755, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Surowe ciasta / Surowe ciasto na chleb i słodkie wypieki'),
(5756, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Surowe ciasta / Surowe ciasta na ciastka i brownies'),
(5759, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Sosy do makaronu'),
(5760, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Oliwki i kapary'),
(5762, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Sosy pieczeniowe'),
(5763, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Sos do ryb'),
(5765, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Tłuszcz do pieczenia i smalec'),
(5766, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Melasy'),
(5767, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Pianki cukrowe'),
(5768, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Żelatyna'),
(5769, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Polewy i lukry'),
(5770, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Syropy kukurydziane'),
(5771, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Skrobie'),
(5773, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Bułka tarta'),
(5774, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Soda do pieczenia'),
(5775, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Aromaty i ekstrakty do pieczenia'),
(5776, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Czekolada do pieczenia'),
(5777, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Wina do gotowania'),
(5778, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Mieszanki do robienia wafli i naleśników'),
(5785, 'Żywność, napoje i tytoń / Żywność / Dipy i smarowidła / Twarożki'),
(5786, 'Żywność, napoje i tytoń / Żywność / Nabiał / Śmietany'),
(5787, 'Żywność, napoje i tytoń / Żywność / Nabiał / Śmietany kwaśne'),
(5788, 'Żywność, napoje i tytoń / Żywność / Mrożone desery i dodatki'),
(5789, 'Żywność, napoje i tytoń / Żywność / Mrożone desery i dodatki / Lody na patyku'),
(5790, 'Żywność, napoje i tytoń / Żywność / Wypieki / Rożki do lodów'),
(5792, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Warzywa liściaste / Mieszanki sałat'),
(5793, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa'),
(5794, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Sosy owocowe'),
(5795, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce'),
(5796, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Suszona fasola'),
(5797, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Fasola w puszce'),
(5798, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Warzywa w puszce'),
(5799, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Owoce w puszce i słoiku'),
(5800, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Nadzienia do ciast'),
(5804, 'Żywność, napoje i tytoń / Żywność / Mięso, owoce morza i jajka / Mięso / Wędliny'),
(5805, 'Żywność, napoje i tytoń / Żywność / Mięso, owoce morza i jajka / Mięso / Świeże i mrożone mięsa'),
(5807, 'Żywność, napoje i tytoń / Żywność / Produkty z tofu, soi i wegetariańskie'),
(5808, 'Żywność, napoje i tytoń / Żywność / Produkty z tofu, soi i wegetariańskie / Seitan'),
(5809, 'Żywność, napoje i tytoń / Żywność / Produkty z tofu, soi i wegetariańskie / Tofu'),
(5810, 'Żywność, napoje i tytoń / Żywność / Produkty z tofu, soi i wegetariańskie / Tempeh'),
(5811, 'Żywność, napoje i tytoń / Żywność / Mięso, owoce morza i jajka / Mięso / Mięsa konserwowe'),
(5812, 'Żywność, napoje i tytoń / Żywność / Mięso, owoce morza i jajka / Owoce morza / Świeże i mrożone owoce morza'),
(5813, 'Żywność, napoje i tytoń / Żywność / Mięso, owoce morza i jajka / Owoce morza / Owoce morza w puszce'),
(5814, 'Żywność, napoje i tytoń / Żywność / Dania gotowe'),
(5820, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja skóry / Preparaty przeciw owadom'),
(5821, 'Zdrowie i uroda / Higiena osobista / Środki do higieny intymnej dla kobiet / Płyny do higieny intymnej'),
(5823, 'Zdrowie i uroda / Higiena osobista / Higiena jamy ustnej / Ochraniacze zębów'),
(5824, 'Zdrowie i uroda / Higiena osobista / Higiena jamy ustnej / Środki do czyszczenia protez'),
(5825, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Środki czyszczące do gospodarstwa domowego / Środki do czyszczenia i polerowania mebli'),
(5826, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do klimatyzatorów / Osłony do klimatyzatorów'),
(5827, 'Żywność, napoje i tytoń / Żywność / Nabiał / Masła i margaryny'),
(5828, 'Sprzęt / Narzędzia / Korki spustowe oleju'),
(5829, 'Artykuły biurowe / Zgrzewarki do folii'),
(5830, 'Biznes i przemysł / Magazynowanie'),
(5831, 'Biznes i przemysł / Magazynowanie / Kontenery transportowe'),
(5832, 'Biznes i przemysł / Magazynowanie / Szafy przemysłowe'),
(5833, 'Biznes i przemysł / Magazynowanie / Regały przemysłowe'),
(5834, 'Ubrania i akcesoria / Ubrania / Bielizna i skarpety / Slipki'),
(5835, 'Dom i ogród / Przygotowanie na sytuacje kryzysowe'),
(5836, 'Dom i ogród / Przygotowanie na sytuacje kryzysowe / Racje żywnościowe'),
(5837, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do przechowywania żywności / Druciki w osłonce i klipsy do torebek'),
(5838, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Wręczanie prezentów / Pakowanie prezentów / Torby na prezenty'),
(5841, 'Ubrania i akcesoria / Akcesoria do torebek i portfeli / Łańcuchy do portfela'),
(5842, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Foteliki rowerowe'),
(5843, 'Dzieci i niemowlęta / Karmienie / Przykrycia do karmienia'),
(5845, 'Dzieci i niemowlęta / Akcesoria do transportu niemowląt / Pokrowce do wózków sklepowych i krzesełek do karmienia'),
(5847, 'Dom i ogród / Ozdoby / Zapachy do domu / Olejki zapachowe'),
(5848, 'Zdrowie i uroda / Opieka zdrowotna / Pierwsza pomoc / Leczenie ciepłem i zimnem / Maści rozgrzewające'),
(5849, 'Zdrowie i uroda / Opieka zdrowotna / Akupunktura'),
(5850, 'Zdrowie i uroda / Opieka zdrowotna / Akupunktura / Modele do akupunktury'),
(5851, 'Zdrowie i uroda / Opieka zdrowotna / Akupunktura / Igły do akupunktury'),
(5853, 'Biznes i przemysł / Piercing i tatuaże / Sprzęt do tatuażu / Zakrycia tatuaży'),
(5859, 'Dzieci i niemowlęta / Zestawy prezentów dla dzieci i niemowląt'),
(5860, 'Media / Instrukcje obsługi / Instrukcje obsługi modeli i zabawek'),
(5861, 'Media / Instrukcje obsługi / Poradniki sportowe'),
(5863, 'Biznes i przemysł / Reklama i marketing'),
(5864, 'Biznes i przemysł / Reklama i marketing / Lady wystawiennicze'),
(5865, 'Biznes i przemysł / Reklama i marketing / Stoiska targowe'),
(5866, 'Dom i ogród / Trawnik i ogród / Silnikowe narzędzia ogrodowe / Ciągniki'),
(5867, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Części i akcesoria do ciągników'),
(5868, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Trofea i nagrody'),
(5869, 'Sprzęt sportowy / Fitness / Taśmy do ćwiczeń'),
(5870, 'Zdrowie i uroda / Opieka zdrowotna / Sprzęt do fizjoterapii'),
(5872, 'Gry i zabawki / Zabawki / Zabawki biurowe / Zabawki magnetyczne'),
(5873, 'Sprzęt / Narzędzia / Magnesy podnoszące'),
(5875, 'Dom i ogród / Ozdoby / Talerze ozdobne'),
(5876, 'Dom i ogród / Ozdoby / Magnesy na lodówkę'),
(5877, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Paralotniarstwo i spadochroniarstwo / Kostiumy powietrzne'),
(5878, 'Ubrania i akcesoria / Ubrania / Uniformy / Kombinezony pilotów'),
(5879, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Hulajnogi'),
(5880, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do paznokci / Suszarki do paznokci'),
(5881, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Wkładki do śpiworów'),
(5883, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do mierzenia i oznaczania / Maszynki do szablonów'),
(5884, 'Biznes i przemysł / Reklama i marketing / Broszury'),
(5885, 'Dom i ogród / Ozdoby / Zabawne tabliczki'),
(5886, 'Meble / Krzesła i fotele / Krzesła do kuchni i jadalni'),
(5887, 'Żywność, napoje i tytoń / Napoje / Napoje alkoholowe / Aromatyzowane napoje alkoholowe'),
(5892, 'Biznes i przemysł / Informacja wizualna / Znaki bezpieczeństwa i ostrzegawcze'),
(5893, 'Biznes i przemysł / Informacja wizualna / Tablice ostrzegawcze'),
(5894, 'Biznes i przemysł / Informacja wizualna / Znaki awaryjne i ewakuacyjne'),
(5895, 'Biznes i przemysł / Informacja wizualna / Znaki drogowe'),
(5896, 'Biznes i przemysł / Informacja wizualna / Znaki parkingowe i zezwolenia'),
(5897, 'Biznes i przemysł / Informacja wizualna / Znaki informacyjne w budynkach'),
(5898, 'Biznes i przemysł / Informacja wizualna / Plakaty i znaki reklamowe'),
(5899, 'Biznes i przemysł / Informacja wizualna / Reklamy chodnikowe i ogrodowe'),
(5900, 'Biznes i przemysł / Informacja wizualna / Oznaczenia informacyjne'),
(5904, 'Żywność, napoje i tytoń / Żywność / Wypieki / Mieszanki ciastek'),
(5907, 'Ubrania i akcesoria / Przebrania i akcesoria / Akcesoria do przebrań / Peleryny do przebrań'),
(5908, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Ochraniacze motocyklowe / Rękawice motocyklowe'),
(5909, 'Ubrania i akcesoria / Ubrania / Odzież wierzchnia / Spodnie i kombinezony na śnieg'),
(5910, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Koce plażowe'),
(5911, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Koce plażowe / Maty plażowe'),
(5912, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Koce plażowe / Pałatki'),
(5913, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Koce plażowe / Koce piknikowe'),
(5914, 'Ubrania i akcesoria / Akcesoria do ubrań / Akcesoria do włosów / Tiary'),
(5915, 'Ubrania i akcesoria / Akcesoria do ubrań / Akcesoria do włosów / Wianki do włosów'),
(5916, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Wręczanie prezentów / Przypinki kwiatowe i dodatki do butonierek'),
(5917, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Myślistwo / Sprzęt dla psów myśliwskich'),
(5918, 'Artykuły biurowe / Artykuły różne / Artykuły papiernicze / Formularze i rachunki'),
(5919, 'Artykuły biurowe / Artykuły różne / Artykuły papiernicze / Rolki do kas fiskalnych'),
(5921, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Komory bezcieniowe'),
(5922, 'Dom i ogród / Ozdoby / Gabloty'),
(5923, 'Zdrowie i uroda / Opieka zdrowotna / Lubrykanty chirurgiczne'),
(5927, 'Sprzęt / Narzędzia / Palniki'),
(5928, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Opalarki kuchenne'),
(5930, 'Dom i ogród / Ozdoby / Ozdoby sezonowe i świąteczne / Tradycyjne lalki japońskie'),
(5935, 'Oprogramowanie / Towary i waluty cyfrowe / Waluty wirtualne'),
(5937, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria i części do aparatów / Akcesoria do kamer przemysłowych'),
(5938, 'Meble / Szafy i przechowywanie / Szafki biurowe'),
(5939, 'Ubrania i akcesoria / Akcesoria do ubrań / Nakrycia głowy / Turbany'),
(5941, 'Ubrania i akcesoria / Akcesoria do ubrań / Ocieplacze na nogi'),
(5942, 'Ubrania i akcesoria / Akcesoria do ubrań / Ocieplacze na ramiona'),
(5949, 'Ubrania i akcesoria / Ubrania / Uniformy / Mundury wojskowe'),
(5959, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Ochraniacze motocyklowe / Ochraniacze motocyklowe klatki piersiowej i pleców'),
(5960, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Ochraniacze motocyklowe / Pasy nerkowe'),
(5961, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Ochraniacze motocyklowe / Motocyklowe stabilizatory szyi'),
(5962, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Ochraniacze motocyklowe / Ochraniacze motocyklowe kolan i piszczeli'),
(5963, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Ochraniacze motocyklowe / Ochraniacze motocyklowe nadgarstków i łokci'),
(5965, 'Zdrowie i uroda / Opieka zdrowotna / Plakietki i biżuteria z informacjami medycznymi'),
(5966, 'Zdrowie i uroda / Opieka zdrowotna / Systemy alarmów medycznych'),
(5967, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Efekty specjalne / Kontrolery efektów specjalnych'),
(5968, 'Gry i zabawki / Zabawki / Zabawki zdalnie sterowane / Motocykle zdalnie sterowane'),
(5969, 'Gry i zabawki / Zabawki / Zabawki zdalnie sterowane / Czołgi zdalnie sterowane'),
(5970, 'Gry i zabawki / Zabawki / Akcesoria do pojazdów do zabawy'),
(5971, 'Gry i zabawki / Zabawki / Akcesoria do pojazdów do zabawy / Akcesoria do wyścigówek'),
(5974, 'Zdrowie i uroda / Czyszczenie i pielęgnacja biżuterii / Przechowywanie biżuterii'),
(5975, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja paznokci / Ozdoby do paznokci'),
(5976, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja skóry / Toniki do twarzy'),
(5977, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja włosów / Akcesoria do farbowania włosów'),
(5978, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż / Makijaż ciała'),
(5979, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż / Makijaż ciała / Farby i podkłady do ciała'),
(5980, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja skóry / Talki do ciała'),
(5981, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż / Makijaż ciała / Kosmetyki rozświetlające do włosów i ciała'),
(5982, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Ozdoby / Stras'),
(5989, 'Dom i ogród / Ozdoby / Dekoracje okienne / Folie okienne'),
(5990, 'Dom i ogród / Ozdoby / Ozdoby sezonowe i świąteczne / Uchwyty do skarpet świątecznych'),
(5991, 'Dom i ogród / Ozdoby / Ozdoby sezonowe i świąteczne / Skarpety świąteczne'),
(5992, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Igły i szydełka / Igły do szycia'),
(5994, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Elementy ozdobne do pojazdów / Ramki do tablic rejestracyjnych'),
(5995, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Elementy ozdobne do pojazdów / Emblematy marek i figurki na maskę'),
(5996, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Ozdoby / Zapięcia do biżuterii'),
(5997, 'Artykuły biurowe / Sortowanie i utrzymanie porządku / Książki adresowe'),
(5998, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Akcesoria do systemów nawadniających'),
(5999, 'Sztuka i rozrywka / Hobby i sztuki piękne / Modelarstwo'),
(6000, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Akcesoria do modelarstwa redukcyjnego'),
(6001, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Elektroniczne instrumenty muzyczne'),
(6002, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Elektroniczne instrumenty muzyczne / Kontrolery MIDI'),
(6003, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Elektroniczne instrumenty muzyczne / Syntetyzatory dźwięku'),
(6006, 'Ubrania i akcesoria / Ubrania / Ubrania sportowe / Stroje ochronne na motor / Kurtki motocyklowe'),
(6016, 'Elektronika / Akcesoria elektroniczne / Akcesoria do anten / Konwertery satelitarne'),
(6017, 'Zdrowie i uroda / Higiena osobista / Środki ułatwiające zasypianie / Środki przeciwko chrapaniu i bezdechowi sennemu'),
(6018, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja włosów / Akcesoria do stylizacji włosów'),
(6019, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja włosów / Narzędzia do stylizacji włosów'),
(6027, 'Oprogramowanie / Programy komputerowe / Programy do multimediów i projektowania / Programy do modelowania 3D'),
(6028, 'Oprogramowanie / Programy komputerowe / Programy do multimediów i projektowania / Programy do odtwarzania multimediów'),
(6029, 'Oprogramowanie / Programy komputerowe / Programy do multimediów i projektowania / Programy do projektowania wnętrz'),
(6030, 'Elektronika / Telekomunikacja / Telefonia / Akcesoria do telefonów komórkowych / Karty SIM do telefonów komórkowych'),
(6031, 'Ubrania i akcesoria / Ubrania / Stroje tradycyjne i ceremonialne / Dirndl'),
(6034, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja skóry / Kosmetyki do demakijażu'),
(6037, 'Gry i zabawki / Gry / Kości i gry z kośćmi'),
(6038, 'Gry i zabawki / Gry / Gry z żetonami'),
(6040, 'Artykuły dla dorosłych / Artykuły erotyczne / Gry erotyczne'),
(6041, 'Pojazdy i części / Akcesoria i części do pojazdów / Przewóz i przechowywanie bagażu / Bagażniki samochodowe / Bazowe bagażniki samochodowe'),
(6042, 'Pojazdy i części / Akcesoria i części do pojazdów / Przewóz i przechowywanie bagażu / Bagażniki samochodowe / Bagażniki na deski do sportów wodnych'),
(6043, 'Pojazdy i części / Akcesoria i części do pojazdów / Przewóz i przechowywanie bagażu / Bagażniki samochodowe / Bagażniki na sprzęt narciarski i snowboardowy'),
(6044, 'Pojazdy i części / Akcesoria i części do pojazdów / Przewóz i przechowywanie bagażu / Bagażniki samochodowe / Bagażniki do przewozu skuterów i motocykli'),
(6046, 'Pojazdy i części / Akcesoria i części do pojazdów / Przewóz i przechowywanie bagażu / Bagażniki samochodowe / Bagażniki do przewozu sprzętu wędkarskiego'),
(6047, 'Pojazdy i części / Akcesoria i części do pojazdów / Przewóz i przechowywanie bagażu / Bagażniki samochodowe / Bagażniki do przewozu kajaków'),
(6048, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Bagażniki rowerowe na deskę surfingową'),
(6049, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Naczynia do napojów / Filiżanki do kawy i herbaty'),
(6051, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Naczynia do napojów / Spodki do kawy i herbaty'),
(6052, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja włosów / Utrwalacze i środki do prostowania'),
(6053, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja włosów / Ściągacze koloru'),
(6054, 'Gry i zabawki / Gry / Gry zręcznościowe'),
(6055, 'Sprzęt sportowy / Sport / Piłka nożna / Akcesoria do bramek do piłki nożnej'),
(6056, 'Gry i zabawki / Zabawki / Lalki, zestawy i figurki / Akcesoria do pacynek i teatrzyków'),
(6057, 'Gry i zabawki / Zabawki / Lalki, zestawy i figurki / Teatrzyki'),
(6058, 'Gry i zabawki / Zabawki / Lalki, zestawy i figurki / Zabawki i figurki akcji'),
(6059, 'Gry i zabawki / Zabawki / Zabawki zdalnie sterowane / Roboty zdalnie sterowane'),
(6062, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Jazda na nartach i snowboardzie / Buty narciarskie'),
(6063, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Jazda na nartach i snowboardzie / Wiązania do nart'),
(6064, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Jazda na nartach i snowboardzie / Narty'),
(6065, 'Artykuły biurowe / Akcesoria biurowe / Przybory piśmiennicze i rysunkowe / Pióra, długopisy i ołówki'),
(6066, 'Artykuły biurowe / Akcesoria biurowe / Przybory piśmiennicze i rysunkowe / Pióra, długopisy i ołówki / Zestawy piór, długopisów i ołówków'),
(6067, 'Artykuły biurowe / Akcesoria biurowe / Przybory piśmiennicze i rysunkowe / Wielofunkcyjne przybory do pisania'),
(6068, 'Artykuły biurowe / Akcesoria biurowe / Przybory piśmiennicze i rysunkowe / Pióra, długopisy i ołówki / Ołówki'),
(6069, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Zestawy kosmetyków'),
(6070, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia'),
(6071, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Zestawy kombo garnków i form do pieczenia'),
(6072, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż / Utrwalacze makijażu w sprayu'),
(6073, 'Dom i ogród / Oświetlenie / Oprawy oświetleniowe / Ścienne oprawy oświetleniowe'),
(6074, 'Sprzęt sportowy / Sport / Łyżwiarstwo figurowe i hokej / Bramki hokejowe'),
(6076, 'Sprzęt sportowy / Sport / Łyżwiarstwo figurowe i hokej / Kije hokejowe'),
(6077, 'Sprzęt sportowy / Sport / Łyżwiarstwo figurowe i hokej / Piłki i krążki hokejowe'),
(6078, 'Sprzęt sportowy / Sport / Łyżwiarstwo figurowe i hokej / Ochraniacze hokejowe / Rękawice hokejowe'),
(6080, 'Sprzęt sportowy / Sport / Łyżwiarstwo figurowe i hokej / Ochraniacze hokejowe / Kaski hokejowe'),
(6083, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Alarmy i zamki samochodowe / Alarmy i blokady motocyklowe'),
(6084, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Alarmy i zamki samochodowe / Akcesoria do alarmów samochodowych'),
(6085, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla rybek / Nawozy do roślin akwariowych'),
(6086, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Naczynia do serwowania żywności / Maselniczki'),
(6087, 'Ubrania i akcesoria / Ubrania / Ubrania sportowe / Stroje rowerowe / Strój kolarski'),
(6088, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Koła do pojazdów silnikowych / Koła i felgi / Koła i felgi motocyklowe'),
(6090, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Koła do pojazdów silnikowych / Koła i felgi / Koła i felgi samochodowe'),
(6091, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Koła do pojazdów silnikowych / Opony / Opony motocyklowe'),
(6093, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Koła do pojazdów silnikowych / Opony / Opony samochodowe'),
(6094, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Akcesoria do kosiarek / Koła do kosiarek'),
(6095, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Akcesoria do kosiarek / Opony do kosiarek'),
(6097, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Pływanie łodzią i rafting / Rowery wodne'),
(6098, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Gongi'),
(6099, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja włosów / Produkty maskujące przerzedzenia włosów'),
(6100, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Wręczanie prezentów / Szpilki na przypinki kwiatowe i dodatki do butonierek'),
(6101, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Agrafki'),
(6102, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Drut do rękodzieła / Druty do koralików i biżuterii'),
(6103, 'Sprzęt sportowy / Fitness / Obciążniki'),
(6104, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja skóry / Oliwki do ciała'),
(6105, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Architektura ogrodowa / Akcesoria do altan i tarasów'),
(6106, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Architektura ogrodowa / Akcesoria do altan i tarasów / Ramy do altan i tarasów'),
(6107, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Architektura ogrodowa / Akcesoria do altan i tarasów / Zestawy ścianek do altan i tarasów'),
(6108, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Architektura ogrodowa / Akcesoria do altan i tarasów / Dachy do altan i tarasów'),
(6109, 'Artykuły dla dorosłych / Broń / Noże szturmowe'),
(6110, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Rękodzieło z papieru / Papiery transferowe'),
(6117, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Kształty i bazy / Rękodzieło z piany i styropianu'),
(6119, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do wycinania i wytłaczania / Noże do zaginania i nacinania papieru'),
(6121, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Proszek do wytłaczania'),
(6122, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do wycinania i wytłaczania / Pisaki do wytłaczania'),
(6125, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do mierzenia i oznaczania / Rakle do sitodruku'),
(6126, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do mierzenia i oznaczania / Wałki i rolki'),
(6127, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Igły i szydełka / Szydełka'),
(6133, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Maty i podkładki do filcowania'),
(6134, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Urządzenia do tekstyliów / Igły do filcowania'),
(6135, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Szablony i formy / Formy do filcowania'),
(6136, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Akcesoria do narzędzi dla artystów i rękodzielników / Akcesoria do kołowrotków'),
(6137, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Urządzenia do tekstyliów / Kołowrotki przędzalnicze'),
(6138, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do nici i przędzy / Wrzeciona ręczne'),
(6139, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Igły i szydełka / Druty do szycia'),
(6140, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Włókna do rękodzieła / Włókna nieprzędzione'),
(6142, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Materiały do wypychania i wypełniania'),
(6144, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Tekstylia / Płótna artystyczne / Kanwy plastikowe'),
(6145, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Ozdobne spinki i zamknięcia / Rzepy do odzieży'),
(6146, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Ozdoby / Elastyczne'),
(6150, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Manekiny krawieckie'),
(6151, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Druty do blokowania'),
(6152, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Maty do blokowania'),
(6153, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do nici i przędzy / Nawijarki do nici i przędzy'),
(6154, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do nici i przędzy / Szpulki do nici i przędzy'),
(6155, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do nici i przędzy / Prowadnice nici i przędzy'),
(6156, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Naparstki krawieckie'),
(6157, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do mierzenia i oznaczania / Miary i centymetry krawieckie'),
(6158, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Ramy, tamborki i rozpinacze do tkanin'),
(6159, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Szpilki'),
(6160, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do mierzenia i oznaczania / Znaczniki i liczniki rzędów'),
(6161, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do wycinania i wytłaczania / Nożyki do szwów'),
(6163, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do nici i przędzy / Nawlekacze do nici'),
(6164, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do nici i przędzy / Zgrzeblarki'),
(6166, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do nici i przędzy / Czółenka tkackie'),
(6167, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do nici i przędzy / Bidła'),
(6168, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Igły i szydełka / Igły smyrneńskie'),
(6169, 'Ubrania i akcesoria / Torebki, portfele i etui / Etui na wizytówki'),
(6170, 'Ubrania i akcesoria / Torebki, portfele i etui / Kieszonki na identyfikatory i przepustki'),
(6171, 'Artykuły biurowe / Sortowanie i utrzymanie porządku / Stojaki na wizytówki'),
(6173, 'Dom i ogród / Kotwy i pokrowce do parasoli'),
(6174, 'Artykuły biurowe / Akcesoria do książek'),
(6175, 'Artykuły biurowe / Akcesoria do książek / Pulpity i podpórki do czytania'),
(6176, 'Artykuły biurowe / Akcesoria do książek / Okładki do książek'),
(6177, 'Artykuły biurowe / Sortowanie i utrzymanie porządku / Koszulki na karty'),
(6178, 'Artykuły biurowe / Obróbka papieru / Podkładki do pisania i rysowania'),
(6179, 'Artykuły biurowe / Wózki biurowe / Wózki uniwersalne'),
(6180, 'Artykuły biurowe / Wózki biurowe / Wózki do dokumentów'),
(6181, 'Artykuły biurowe / Wózki biurowe / Wózki pocztowe'),
(6182, 'Artykuły biurowe / Wózki biurowe / Wózki biblioteczne'),
(6183, 'Ubrania i akcesoria / Akcesoria do ubrań / Akcesoria do włosów / Siatki do włosów'),
(6186, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Sportowe artykuły kolekcjonerskie / Pamiątki sportowe z autografem / Pamiątki tenisowe z autografem'),
(6187, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Sportowe artykuły kolekcjonerskie / Akcesoria dla fanów sportu / Akcesoria dla fanów tenisa'),
(6189, 'Zdrowie i uroda / Higiena osobista / Higiena jamy ustnej / Odświeżacze do ust'),
(6190, 'Artykuły biurowe / Sortowanie i utrzymanie porządku / Wizytowniki'),
(6191, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Chemikalia / Odmrażacze'),
(6192, 'Żywność, napoje i tytoń / Żywność / Przekąski / Ptysie serowe'),
(6194, 'Żywność, napoje i tytoń / Żywność / Przekąski / Skwarki'),
(6195, 'Żywność, napoje i tytoń / Żywność / Wypieki / Babeczki'),
(6196, 'Żywność, napoje i tytoń / Żywność / Wypieki / Ciasta do kawy'),
(6199, 'Żywność, napoje i tytoń / Żywność / Przyprawy i wzmacniacze smaku / Pieprz'),
(6203, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Sosy do pizzy'),
(6204, 'Żywność, napoje i tytoń / Żywność / Dipy i smarowidła / Polewa jabłkowa z przyprawami'),
(6205, 'Zdrowie i uroda / Opieka zdrowotna / Pierwsza pomoc / Leczenie ciepłem i zimnem / Podkładki rozgrzewające'),
(6206, 'Zdrowie i uroda / Opieka zdrowotna / Pierwsza pomoc / Osłony na gips i bandaż'),
(6219, 'Żywność, napoje i tytoń / Żywność / Owoce w czekoladzie i kandyzowane'),
(6227, 'Ubrania i akcesoria / Ubrania / Stroje tradycyjne i ceremonialne / Odświętne czarne stroje japońskie'),
(6238, 'Ubrania i akcesoria / Akcesoria do ubrań / Nauszniki'),
(6240, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły do pralni / Odświeżacze do tkanin'),
(6242, 'Zdrowie i uroda / Opieka zdrowotna / Zdrowy tryb życia i dieta / Gumy do żucia i żele energetyczne'),
(6246, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Sosy Worcestershire'),
(6248, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Preparaty na pchły i kleszcze dla zwierząt'),
(6249, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Smycze dla zwierząt'),
(6250, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Obroże i szelki dla zwierząt'),
(6251, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Transportery i przenoski dla zwierząt'),
(6252, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Miski, podajniki karmy i poidełka dla zwierząt'),
(6253, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Przedłużenia smyczy dla zwierząt'),
(6254, 'Dom i ogród / Ozdoby / Akcesoria do okien'),
(6255, 'Dom i ogród / Ozdoby / Akcesoria do okien / Uchwyty do podwiązania zasłon i frędzle'),
(6256, 'Dom i ogród / Ozdoby / Akcesoria do okien / Pierścienie do zasłon'),
(6257, 'Dom i ogród / Ozdoby / Akcesoria do okien / Karnisze do zasłon'),
(6258, 'Dom i ogród / Trawnik i ogród / Silnikowe narzędzia ogrodowe / Kosiarki / Kosiarki ciągnione'),
(6259, 'Żywność, napoje i tytoń / Żywność / Ziarna, ryż i płatki / Komosa ryżowa'),
(6260, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do pielęgnacji twarzy / Szczotki i zestawy do oczyszczania twarzy'),
(6261, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do pielęgnacji twarzy / Przyrządy do usuwania zaskórników'),
(6262, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja skóry / Maski i peelingi do twarzy'),
(6263, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Końcówki do miotełek do kurzu'),
(6264, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Końcówki do mopów'),
(6265, 'Dom i ogród / Ozdoby / Sztuczne rośliny'),
(6266, 'Dom i ogród / Ozdoby / Sztuczna żywność'),
(6267, 'Dom i ogród / Ozdoby / Wieńce i girlandy'),
(6268, 'Ubrania i akcesoria / Akcesoria do ubrań / Lei'),
(6269, 'Gry i zabawki / Zabawki ogrodowe / Akcesoria do huśtawek i placów zabaw'),
(6270, 'Gry i zabawki / Zabawki ogrodowe / Huśtawki i bujaki'),
(6271, 'Gry i zabawki / Zabawki ogrodowe / Zestawy huśtawek i placów zabaw'),
(6272, 'Dom i ogród / Oświetlenie / Listwy oświetleniowe / Akcesoria do listew oświetleniowych'),
(6273, 'Dom i ogród / Oświetlenie / Listwy oświetleniowe / Ramy listew oświetleniowych'),
(6274, 'Dom i ogród / Oświetlenie / Listwy oświetleniowe'),
(6275, 'Biznes i przemysł / Branża medyczna / Zasłony szpitalne'),
(6276, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Wózki dla zwierząt'),
(6277, 'Ubrania i akcesoria / Akcesoria do torebek i portfeli / Smycze'),
(6278, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Tostery i grille / Piecyki i piece do pizzy'),
(6279, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Parowary i garnki do gotowania na parze / Piekarniki do sous-vide'),
(6280, 'Biznes i przemysł / Branża medyczna / Sprzęt medyczny / Monitory oznak życiowych'),
(6281, 'Biznes i przemysł / Branża medyczna / Instrumenty medyczne / Kleszcze medyczne'),
(6282, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do makijażu / Gumki do zalotki'),
(6284, 'Zdrowie i uroda / Opieka zdrowotna / Akcesoria do monitorów funkcji życiowych / Akcesoria do ciśnieniomierzy'),
(6285, 'Zdrowie i uroda / Opieka zdrowotna / Akcesoria do monitorów funkcji życiowych / Akcesoria do ciśnieniomierzy / Rękawy do ciśnieniomierzy'),
(6286, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Surfing / Deski do skimboardingu'),
(6287, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Surfing / Bodyboardy'),
(6288, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Surfing / Deski do paddleboardingu'),
(6289, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Akumulatory / Akumulatory do zasilaczy UPS'),
(6290, 'Dzieci i niemowlęta / Zdrowie niemowląt / Zestawy do pielęgnacji niemowląt'),
(6291, 'Elektronika / Akcesoria elektroniczne / Akcesoria komputerowe / Podpórki pod nadgarstki do klawiatur i myszek'),
(6292, 'Sprzęt / Akcesoria do narzędzi / Ładowarki do elektronarzędzi'),
(6293, 'Pojazdy i części / Akcesoria i części do pojazdów / Części i akcesoria do jednostek pływających / Oświetlenie do jednostek pływających'),
(6295, 'Sprzęt / Akcesoria do narzędzi / Baterie do elektronarzędzi'),
(6296, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Sporty wodne holowane / Jazda na nartach wodnych / Pokrowce i torby na narty wodne'),
(6297, 'Sprzęt / Akcesoria do sprzętu / Łańcuchy, druty i sznury / Pasy mocujące'),
(6298, 'Sprzęt / Akcesoria do sprzętu / Łańcuchy, druty i sznury / Linki elastyczne'),
(6300, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do paznokci / Zestawy do manicure'),
(6301, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Sporty wodne holowane / Tratwy i rury do holowania'),
(6302, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Sporty wodne holowane / Jazda na nartach wodnych / Wiązania do nart wodnych'),
(6303, 'Biznes i przemysł / Branża medyczna / Pościel szpitalna'),
(6304, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż / Makijaż twarzy / Rozświetlacze do twarzy'),
(6305, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż / Makijaż twarzy / Róże i bronzery'),
(6306, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż / Makijaż ust / Pomadki w pisaku i róże w sztyfcie'),
(6307, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria i części do aparatów / Obudowy podwodne do aparatów i kamer'),
(6308, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria i części do aparatów / Torby i pokrowce na aparaty i kamery'),
(6311, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Zestawy balonów'),
(6312, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Pływanie łodzią i rafting / Akcesoria kajakarskie'),
(6314, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Pływanie łodzią i rafting / Akcesoria do kanadyjek'),
(6317, 'Dom i ogród / Ozdoby / Butelki ozdobne'),
(6318, 'Dom i ogród / Trawnik i ogród / Nawadnianie / Konewki'),
(6319, 'Artykuły biurowe / Artykuły różne / Pinezki z łbem metalowym'),
(6321, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Systemy ogrodzeń dla zwierząt'),
(6322, 'Dom i ogród / Bielizna stołowa i pościelowa / Bielizna stołowa / Falbany stołowe'),
(6323, 'Zdrowie i uroda / Opieka zdrowotna / Akcesoria do monitorów funkcji życiowych / Akcesoria do glukometrów / Roztwory kontrolne do glukometrów'),
(6324, 'Dom i ogród / Ozdoby / Pierścienie do serwetek'),
(6325, 'Dom i ogród / Bielizna stołowa i pościelowa / Bielizna stołowa / Bieżniki'),
(6328, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Akcesoria do myjek ciśnieniowych'),
(6329, 'Gry i zabawki / Gry / Gra battle tops'),
(6330, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Pływanie / Akcesoria do okularów i masek'),
(6331, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Preparaty do czyszczenia akcesoriów kosmetycznych'),
(6333, 'Dom i ogród / Ozdoby / Akcesoria do skrzynek pocztowych'),
(6334, 'Dom i ogród / Ozdoby / Akcesoria do skrzynek pocztowych / Słupki na skrzynkę pocztową'),
(6336, 'Dom i ogród / Ozdoby / Akcesoria do zapachów do domu / Lampiony i podgrzewacze świecowe'),
(6337, 'Sprzęt sportowy / Fitness / Akcesoria do rollerów'),
(6338, 'Sprzęt sportowy / Fitness / Akcesoria do rollerów / Pokrowce na rollery'),
(6340, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż / Makijaż oka / Odżywki do rzęs i brwi'),
(6341, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do paznokci / Polerki do paznokci'),
(6342, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Rękawy cukiernicze'),
(6343, 'Sprzęt / Materiały budowlane / Wyposażenie do drzwi'),
(6344, 'Sprzęt sportowy / Sport / Sprzęt ogólnosportowy / Siedziska i poduszki stadionowe'),
(6345, 'Meble / Zestawy mebli'),
(6346, 'Meble / Zestawy mebli / Zestawy mebli do sypialni'),
(6347, 'Meble / Zestawy mebli / Zestawy mebli do kuchni i jadalni'),
(6348, 'Meble / Zestawy mebli / Zestawy mebli do salonu'),
(6349, 'Meble / Meble dla dzieci i niemowląt / Zestawy mebli dla dzieci i niemowląt'),
(6351, 'Meble / Stoły / Stoliki dla dzieci'),
(6356, 'Meble / Szafy i przechowywanie'),
(6357, 'Meble / Szafy i przechowywanie / Szafy na wino i likiery'),
(6358, 'Meble / Szafy i przechowywanie / Stojaki na płyty'),
(6360, 'Meble / Szafy i przechowywanie / Toaletki / Toaletki do sypialni'),
(6362, 'Meble / Meble biurowe'),
(6363, 'Meble / Meble biurowe / Stoły do pracy'),
(6367, 'Meble / Meble zewnętrzne / Zestawy mebli zewnętrznych'),
(6368, 'Meble / Meble zewnętrzne / Siedzenia ogrodowe'),
(6369, 'Meble / Stoły / Stoliki'),
(6372, 'Meble / Półki i regały / Półki ścienne'),
(6373, 'Artykuły biurowe / Wózki biurowe'),
(6374, 'Meble / Wózki / Wyspy kuchenne'),
(6375, 'Sprzęt / Materiały elektryczne / Akcesoria do przedłużaczy'),
(6378, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do wiercenia / Ostrzałki do wierteł'),
(6379, 'Gry i zabawki / Zabawki / Zabawki do jeżdżenia / Wózki – przyczepki'),
(6381, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Agrowłókiny'),
(6382, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Zestawy do sztuki i rękodzieła / Zestawy do naprawy tkanin'),
(6383, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły groomerskie dla zwierząt'),
(6384, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły groomerskie dla zwierząt / Trymery dla zwierząt'),
(6385, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły groomerskie dla zwierząt / Grzebienie i szczotki dla zwierząt'),
(6386, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Produkty do pielęgnacji koni / Artykuły do pielęgnacji koni / Nożyce i trymery dla koni'),
(6387, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły do pralni / Krochmal'),
(6388, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Łyżki durszlakowe'),
(6392, 'Meble / Stoły'),
(6393, 'Meble / Meble dla dzieci i niemowląt / Koszyki i kołyski'),
(6394, 'Meble / Meble dla dzieci i niemowląt / Łóżeczka i łóżka dziecinne'),
(6396, 'Gry i zabawki / Zabawki ogrodowe / Zamki dmuchane'),
(6397, 'Gry i zabawki / Zabawki ogrodowe / Namioty i tunele dla dzieci'),
(6402, 'Elektronika / Akcesoria elektroniczne / Zarządzanie kablami / Trasy kablowe'),
(6403, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla rybek / Karmniki dla rybek'),
(6404, 'Artykuły biurowe / Sprzęt biurowy / Rejestratory czasu pracy'),
(6406, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły groomerskie dla zwierząt / Szampony i odżywki dla zwierząt'),
(6407, 'Gry i zabawki / Zabawki / Zabawki do jeżdżenia / Koniki na kiju'),
(6408, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Łopatki do ciast'),
(6411, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Akcesoria do noży elektrycznych'),
(6412, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Akcesoria do noży elektrycznych / Ostrza zapasowe do noży elektrycznych'),
(6413, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Akcesoria do agrowłókniny'),
(6415, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Organizery kuchenne / Cukiernice'),
(6416, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Części do laptopów / Zawiasy do laptopów'),
(6418, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Kleje i magnesy dla rękodzieła / Taśmy termoprzylepne do tkanin'),
(6419, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Środki ochronne do materiałów i tapicerki'),
(6421, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Akcesoria do agrowłókniny / Taśmy do agrowłókniny'),
(6422, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Akcesoria do agrowłókniny / Zszywki i spinki do agrowłókniny'),
(6424, 'Dom i ogród / Ozdoby / Przedmioty ozdobne do wazonów i kamienie dekoracyjne'),
(6425, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Akcesoria do naczyń do serwowania żywności'),
(6426, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Akcesoria do naczyń do serwowania żywności / Stojaki na wazy'),
(6427, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Akcesoria do naczyń do serwowania żywności / Pokrywki do waz'),
(6428, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Kwietniki'),
(6429, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja włosów / Podgrzewacze i czapki podgrzewające do włosów'),
(6433, 'Meble / Łóżka i akcesoria'),
(6434, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Akcesoria do naczyń do serwowania żywności / Stojaki do waz na poncz'),
(6436, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Kompostowanie / Kompostowniki'),
(6437, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Wiadra'),
(6439, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Sztućce / Akcesoria do pałeczek'),
(6440, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Materiały do wiązania przynęty / Plecionka wędkarska'),
(6441, 'Zdrowie i uroda / Higiena osobista / Higiena jamy ustnej / Szczoteczki do czyszczenia języka'),
(6442, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Pokrowce na rowery'),
(6445, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Pokrowce i nakładki na siodełka rowerowe'),
(6446, 'Sprzęt / Materiały budowlane / Wyposażenie do drzwi / Automaty do zamykania drzwi'),
(6447, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do wycinania i wytłaczania / Obcinacze do nici i przędzy'),
(6449, 'Dom i ogród / Kuchnia i jadalnia / Transportery do żywności i napojów / Torby na wino'),
(6450, 'Gry i zabawki / Zabawki ogrodowe / Huśtawki'),
(6452, 'Sprzęt sportowy / Fitness / Podnoszenie ciężarów / Akcesoria do wolnych ciężarów'),
(6454, 'Pojazdy i części / Akcesoria i części do pojazdów / Przewóz i przechowywanie bagażu / Akcesoria do bagażników bazowych'),
(6455, 'Zdrowie i uroda / Higiena osobista / Higiena jamy ustnej / Końcówki zapasowe do irygatorów dentystycznych'),
(6456, 'Dom i ogród / Ozdoby / Tace ozdobne'),
(6457, 'Dom i ogród / Ozdoby / Półmiski ozdobne'),
(6458, 'Sprzęt / Materiały budowlane / Wyposażenie do drzwi / Zaczepy do drzwi'),
(6459, 'Sprzęt / Materiały elektryczne / Przełączniki elektryczne'),
(6460, 'Ubrania i akcesoria / Akcesoria do torebek i portfeli / Okładki na książeczki czekowe'),
(6461, 'Sprzęt / Narzędzia / Taśma perlonowa'),
(6462, 'Artykuły biurowe / Maty biurowe / Maty antypoślizgowe'),
(6463, 'Ubrania i akcesoria / Biżuteria / Komplety biżuterii'),
(6464, 'Gry i zabawki / Zabawki ogrodowe / Zabawki do wody'),
(6465, 'Gry i zabawki / Zabawki ogrodowe / Zabawki do wody / Spryskiwacze do zabawy'),
(6466, 'Gry i zabawki / Zabawki / Zabawki edukacyjne / Liczydła'),
(6467, 'Artykuły biurowe / Obróbka papieru / Dziurkacze'),
(6471, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do wkrętarek'),
(6473, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Pływanie / Łapki do pływania'),
(6474, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Środki czyszczące do gospodarstwa domowego / Środki dezynfekujące do gospodarstwa domowego'),
(6475, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Akcesoria do urządzeń wejściowych'),
(6476, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Akcesoria do urządzeń wejściowych / Stojaki do skanerów kodów paskowych'),
(6478, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do przechowywania żywności'),
(6479, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do przechowywania żywności / Pochłaniacze tlenu'),
(6480, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Organizery kuchenne / Organizery na puszki'),
(6481, 'Dom i ogród / Kuchnia i jadalnia / Przechowywanie żywności / Pokrywki do pojemników na żywność'),
(6482, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Narzędzia do naprawy pojazdów / Samochodowe skanery diagnostyczne'),
(6486, 'Artykuły biurowe / Obróbka papieru / Naparstki biurowe'),
(6487, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Organizery kuchenne / Organizery do szaf kuchennych'),
(6488, 'Sprzęt / Zamki i klucze / Surówki kluczy'),
(6490, 'Biznes i przemysł / Branża medyczna / Sprzęt do szkoleń medycznych'),
(6491, 'Biznes i przemysł / Branża medyczna / Sprzęt do szkoleń medycznych / Manekiny do szkoleń z pierwszej pomocy'),
(6492, 'Dom i ogród / Ozdoby / Dekoracje okienne / Panele z kolorowego szkła'),
(6493, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Ochraniacze motocyklowe / Części i akcesoria do kasków motocyklowych');
INSERT INTO `google_kategorie` (`id`, `nazwa`) VALUES
(6495, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Pojemniki na przynętę wędkarską'),
(6496, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Odzież żeglarska i do uprawiania sportów wodnych / Akcesoria do kamizelek ratunkowych'),
(6497, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Miksery i shakery do dressingów do sałatek'),
(6499, 'Meble / Krzesła i fotele / Fotele, leżanki i fotele rozkładane'),
(6501, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do pił / Akcesoria do pił stołowych'),
(6503, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do pił / Akcesoria do ukośnic'),
(6506, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Pompki do amortyzatorów'),
(6508, 'Elektronika / Sieci / Zasilacze typu power injector i power splitter'),
(6509, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja oczu / Pielęgnacja szkieł kontaktowych / Płyny do szkieł kontaktowych'),
(6510, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja oczu / Pielęgnacja szkieł kontaktowych / Pojemniki na szkła kontaktowe'),
(6511, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Pływanie / Płetwy pływackie'),
(6512, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Pływanie / Płetwy pływackie / Monopłetwy'),
(6513, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Pływanie / Pasy do pływania'),
(6514, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Nurkowanie i snorkeling / Pasy balastowe'),
(6515, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Pływanie / Zaciski na nos'),
(6516, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Tostery i grille / Maszynki do precli'),
(6517, 'Biznes i przemysł / Usługi gastronomiczne / Wózki z hot-dogami'),
(6518, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Garnki / Garnki gliniane do fermentacji i marynowania'),
(6519, 'Artykuły biurowe / Maty biurowe'),
(6520, 'Artykuły biurowe / Maty biurowe / Maty wejściowe'),
(6521, 'Artykuły biurowe / Maty biurowe / Maty ochronne pod krzesła obrotowe'),
(6522, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Obieraki do cytrusów'),
(6523, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Parowary i garnki do gotowania na parze / Thermocookery'),
(6524, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Płyty gazowe'),
(6526, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Dozowniki oleju i octu'),
(6530, 'Dom i ogród / Ozdoby / Magnesy okienne'),
(6531, 'Dom i ogród / Ozdoby / Ozdoby sezonowe i świąteczne / Szopki bożonarodzeniowe'),
(6532, 'Dom i ogród / Ozdoby / Ozdoby sezonowe i świąteczne / Dekoracje świąteczne'),
(6534, 'Dom i ogród / Kuchnia i jadalnia / Przechowywanie żywności / Słoje na cukierki'),
(6535, 'Dom i ogród / Ozdoby / Kule śnieżne'),
(6536, 'Artykuły dla dorosłych / Artykuły erotyczne / Zestawy do tańca na rurze'),
(6539, 'Meble / Szafy i przechowywanie / Gazetowniki'),
(6540, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Włókna do rękodzieła / Nici do koralików i biżuterii'),
(6541, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Akcesoria do kosiarek / Zamiataczki trawnikowe'),
(6542, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Akcesoria do kosiarek / Końcówki do przycinania krzewów'),
(6543, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Urządzenia do napojów gorących'),
(6544, 'Elektronika / Lokalizatory GPS'),
(6545, 'Elektronika / Audio / Elementy audio / Tory sygnałowe'),
(6546, 'Elektronika / Audio / Elementy audio / DI-boksy'),
(6547, 'Dom i ogród / Ozdoby / Ozdoby trawnikowe i rzeźby ogrodowe'),
(6548, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Podgrzewacze do żywności / Podgrzewacze gastronomiczne'),
(6549, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do pił'),
(6550, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Pływanie / Ciężarki do aqua aerobiku'),
(6551, 'Ubrania i akcesoria / Torebki, portfele i etui'),
(6552, 'Ubrania i akcesoria / Akcesoria do torebek i portfeli'),
(6553, 'Torby, walizki i akcesoria podróżne / Kufry podróżne'),
(6554, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Rożna'),
(6555, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do makijażu / Szablony do brwi'),
(6556, 'Sprzęt / Narzędzia / Narzędzia malarskie / Wałki do malowania rogów'),
(6557, 'Sprzęt / Narzędzia / Narzędzia malarskie / Tacki malarskie'),
(6558, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Farby, tusze i laserunki / Nośniki malarskie'),
(6559, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja uszu / Krople do uszu'),
(6560, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja uszu / Suszarki do uszu'),
(6561, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja uszu / Strzykawki do uszu'),
(6562, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja uszu / Zestawy do usuwania woskowiny usznej'),
(6563, 'Dom i ogród / Akcesoria do kominków i pieców opalanych węglem / Paleniska kominków i pieców opalanych węglem'),
(6566, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Jabłka'),
(6567, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Owoce pestkowe / Morele'),
(6568, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Szparagi'),
(6569, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Warzywa liściaste / Rukola'),
(6570, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Karczochy'),
(6571, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Atemoya'),
(6572, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Awokado'),
(6573, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Babako'),
(6574, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Banany'),
(6577, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Fasolki'),
(6580, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Buraki'),
(6581, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Warzywa liściaste / Boćwinka'),
(6582, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Jagody'),
(6584, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Warzywa liściaste / Kapusta chińska'),
(6585, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Słodkie ziemniaki'),
(6586, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Ziemniaki'),
(6587, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Ogórecznik'),
(6589, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Owoce chlebowca'),
(6590, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Brukselki'),
(6591, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Brokuły'),
(6592, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Kapusty'),
(6593, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Opuncje figowe'),
(6594, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Karambola'),
(6595, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Marchewki'),
(6596, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Karczoch hiszpański'),
(6597, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Warzywa liściaste / Buraki liściowe'),
(6598, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Korzenie selera'),
(6599, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Selery'),
(6600, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Kalafiory'),
(6601, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Owoce pestkowe / Wiśnie'),
(6602, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Cherimoya'),
(6603, 'Dom i ogród / Ozdoby / Ozdoby sezonowe i świąteczne / Stojaki choinkowe'),
(6608, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Ogórki'),
(6609, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Kukurydza'),
(6610, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Warzywa liściaste / Kapusta właściwa polna'),
(6613, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Bakłażany'),
(6614, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Akka Sellowa'),
(6615, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Pędy paproci'),
(6616, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Kapusta sitowata'),
(6617, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Gai Lan'),
(6618, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Korzenie łopianu'),
(6619, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Korzenie imbiru'),
(6620, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Czosnek'),
(6621, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Cytrusy / Grejpfruty'),
(6622, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Warzywa liściaste'),
(6624, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Owoce homli'),
(6625, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Gujawy'),
(6626, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Winogrona'),
(6627, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Pory'),
(6628, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Kalarepy'),
(6629, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Warzywa liściaste / Jarmuż zielony'),
(6630, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Kłębian kątowaty'),
(6631, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Korzenie chrzanu'),
(6632, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Cytrusy / Kumkwaty'),
(6633, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Kiwi'),
(6636, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Cytrusy / Cytryny'),
(6637, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Warzywa liściaste / Sałaty'),
(6638, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Liczi'),
(6639, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Nieśplik japoński'),
(6640, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Longan'),
(6641, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Cytrusy / Limekwaty'),
(6642, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Cytrusy / Limonki'),
(6643, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Malanga'),
(6644, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Korzenie lotusa'),
(6645, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Mangostan'),
(6646, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Owoce pestkowe / Mango'),
(6647, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Mamea'),
(6649, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Melony'),
(6653, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Grzyby'),
(6655, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Cebule'),
(6656, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Warzywa liściaste / Wilec wodny'),
(6657, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Okra'),
(6658, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Cytrusy / Pomarańcze'),
(6661, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Papaja'),
(6663, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Pasternaki'),
(6664, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Korzenie pietruszki'),
(6665, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Gruszki'),
(6667, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Męczennica jadalna'),
(6668, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Papryki'),
(6669, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Groszki'),
(6670, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Ananasy'),
(6671, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Miechunka'),
(6672, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Persymona'),
(6673, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Granaty'),
(6674, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Owoce pestkowe / Śliwki'),
(6675, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Owoce pestkowe / Śliwy brzoskwiniowe'),
(6676, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Pitaje'),
(6678, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Rambutan'),
(6679, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Pigwy'),
(6681, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Rabarbar'),
(6682, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Rzodkiewki'),
(6687, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Sapota'),
(6688, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Pigwice'),
(6691, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Flaszowce'),
(6692, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Cyfomandra'),
(6693, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Topinambur'),
(6694, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Trzcina cukrowa'),
(6695, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Warzywa liściaste / Szpinak'),
(6697, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Cytrusy / Tangelo'),
(6698, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Tamaryndowce'),
(6700, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Rukiew wodna'),
(6701, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Kasztany wodne'),
(6703, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Pomidory'),
(6704, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Kolokazja'),
(6705, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Korzenie juki'),
(6706, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Warzywa liściaste / Kapusta rzepak'),
(6708, 'Sprzęt sportowy / Sport / Łyżwiarstwo figurowe i hokej / Części i akcesoria do łyżew / Buty do łyżew figurowych'),
(6709, 'Dom i ogród / Sprzęt AGD / Ogrzewanie, wentylacja i klimatyzacja / Zewnętrzne systemy mgielne'),
(6712, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Wręczanie prezentów / Pakowanie prezentów / Bibuły'),
(6714, 'Biznes i przemysł / Branża medyczna / Sprzęt medyczny / Akcesoria do monitorów oznak życiowych'),
(6715, 'Zdrowie i uroda / Higiena osobista / Higiena jamy ustnej / Flossery'),
(6716, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Arracacha'),
(6717, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Warzywa liściaste / Cykorie'),
(6721, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty dęte drewniane / Flażolety'),
(6723, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Noże do ciasta'),
(6725, 'Gry i zabawki / Łamigłówki / Łamigłówki drewniane i układanki drewniane'),
(6726, 'Dom i ogród / Artykuły gospodarstwa domowego / Akcesoria do przechowywania śmieci / Stelaże do pojemników na śmieci'),
(6727, 'Dom i ogród / Sprzęt AGD / Ogrzewanie, wentylacja i klimatyzacja / Systemy chłodzenia wyparnego'),
(6728, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty dęte drewniane / Gwizdki pociągu'),
(6729, 'Sprzęt sportowy / Sport / Akcesoria sędziowskie / Kartki i flagi karne'),
(6730, 'Sprzęt sportowy / Sport / Akcesoria sędziowskie / Gwizdki sędziowskie'),
(6731, 'Sprzęt sportowy / Sport / Akcesoria sędziowskie / Chorągiewki sędziów liniowych'),
(6732, 'Sprzęt / Hydraulika / Mocowania hydrauliczne / Uszczelki hydrauliczne'),
(6734, 'Sprzęt sportowy / Sport / Sprzęt do gry w broomball'),
(6737, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Akcesoria i części do hamaków'),
(6739, 'Sprzęt sportowy / Sport / Akcesoria sędziowskie'),
(6740, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Komplety herbaciane i kawowe'),
(6741, 'Dom i ogród / Sprzęt AGD / Piloty i klawiatury do bram garażowych'),
(6743, 'Sprzęt sportowy / Fitness / Joga i pilates / Ręczniki do jogi i pilatesu'),
(6744, 'Pojazdy i części / Akcesoria i części do pojazdów / Przewóz i przechowywanie bagażu / Siatki bagażowe'),
(6746, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Pojemniki do zlewozmywaków'),
(6747, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do usuwania odpadów'),
(6748, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Paintball i ASG / Paintball / Granatniki do paintballu'),
(6749, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Foremki i pieczątki do makaronu'),
(6750, 'Sprzęt sportowy / Fitness / Joga i pilates / Klocki do jogi i pilatesu'),
(6751, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Akcesoria do parasoli i pawilonów ogrodowych'),
(6753, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja skóry / Wazelina'),
(6754, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Groszki czekoladowe'),
(6756, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Formy do pieczenia / Tace do podgrzewania bezpośrednio na ogniu'),
(6757, 'Dom i ogród / Artykuły gospodarstwa domowego / Akcesoria do przechowywania śmieci'),
(6758, 'Dom i ogród / Artykuły gospodarstwa domowego / Akcesoria do przechowywania śmieci / Kółka do pojemników na śmieci'),
(6760, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do pielęgnacji twarzy / Sauny do twarzy'),
(6761, 'Żywność, napoje i tytoń / Napoje / Napoje alkoholowe / Cydr'),
(6762, 'Dom i ogród / Rośliny / Rośliny doniczkowe i ogrodowe'),
(6763, 'Dom i ogród / Ozdoby / Fontanny i stawy / Fontanny'),
(6764, 'Biznes i przemysł / Środki ochrony osobistej / Fartuchy ochronne'),
(6765, 'Dom i ogród / Artykuły gospodarstwa domowego / Akcesoria do przechowywania śmieci / Pojemniki na śmieci na kółkach'),
(6766, 'Dom i ogród / Baseny i spa / Akcesoria do basenów i spa / Drabinki, schodki i podesty basenowe'),
(6767, 'Dom i ogród / Ozdoby / Zapachy do domu / Woski zapachowe'),
(6769, 'Sprzęt / Akcesoria do sprzętu / Węże pneumatyczne'),
(6770, 'Sprzęt / Akcesoria do sprzętu / Węże do lubryfikacji'),
(6771, 'Dom i ogród / Baseny i spa / Akcesoria do basenów i spa / Węże czyszczące do basenów'),
(6772, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Sos koktajlowy'),
(6773, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do pieców i podgrzewaczy'),
(6774, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Mieszanki na panierkę i polewę'),
(6775, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Jadalne dekoracje do wypieków'),
(6779, 'Artykuły biurowe / Sortowanie i utrzymanie porządku / Pudełka na karty z przepisami'),
(6780, 'Elektronika / Akcesoria elektroniczne / Zarządzanie kablami / Osłonki na kable'),
(6781, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Paintball i ASG / Paintball / Pasy na faty paintballowe'),
(6782, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Sosy chrzanowe'),
(6783, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Sos tatarski'),
(6784, 'Żywność, napoje i tytoń / Żywność / Dipy i smarowidła / Tapenada'),
(6785, 'Żywność, napoje i tytoń / Żywność / Przekąski / Ciastka z kremem w środku'),
(6786, 'Biznes i przemysł / Usługi gastronomiczne / Płuczki i suszarki do żywności'),
(6787, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Formy do żelatyny'),
(6788, 'Dom i ogród / Trawnik i ogród / Silnikowe narzędzia ogrodowe / Kosiarki / Kosiarki automatyczne'),
(6789, 'Dom i ogród / Trawnik i ogród / Silnikowe narzędzia ogrodowe / Odkurzacze ogrodowe'),
(6790, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Moduły PCU'),
(6791, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja skóry / Paski do usuwania wągrów'),
(6792, 'Dom i ogród / Kominki'),
(6794, 'Gry i zabawki / Gry / Akcesoria do gry battle tops'),
(6795, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Chemikalia / Amoniak'),
(6797, 'Żywność, napoje i tytoń / Napoje / Maślanki'),
(6799, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Kamery termowizyjne'),
(6800, 'Meble / Krzesła i fotele / Fotele do grania'),
(6806, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do wiercenia / Otwornice'),
(6807, 'Sprzęt / Materiały elektryczne / Tablice bezpiecznikowe'),
(6808, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Liście kaktusa'),
(6809, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Kokosy'),
(6810, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Figi'),
(6811, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Wieszaki na ubranka dla zwierząt'),
(6812, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Daktyle'),
(6813, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Madrono'),
(6814, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Jabłka cukrowe'),
(6816, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Koper włoski'),
(6817, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Akcesoria do akumulatorów / Kontrolery ładowania'),
(6818, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Szalotki'),
(6819, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Tostery i grille / Maszynki do pączków'),
(6821, 'Żywność, napoje i tytoń / Żywność / Nabiał / Bite śmietany'),
(6822, 'Meble / Meble zewnętrzne / Otomany na zewnątrz'),
(6828, 'Meble / Meble zewnętrzne / Siedzenia ogrodowe / Krzesła ogrodowe'),
(6829, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Zestawy do sztuki i rękodzieła / Zestawy do mozaik'),
(6830, 'Żywność, napoje i tytoń / Żywność / Dipy i smarowidła / Dipy warzywne'),
(6831, 'Żywność, napoje i tytoń / Żywność / Dipy i smarowidła / Serki do smarowania'),
(6832, 'Sprzęt / Hydraulika / Sterowniki nawadniania'),
(6833, 'Sprzęt / Materiały elektryczne / Kontrolki i sensory do gniazdek'),
(6834, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Zbiorniki na deszczówkę'),
(6838, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Garnki / Patelnie na naleśniki crêpe i bliny'),
(6839, 'Żywność, napoje i tytoń / Żywność / Produkty z tofu, soi i wegetariańskie / Zamienniki sera'),
(6840, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Kompostowanie / Przerzucarki do kompostu'),
(6841, 'Sprzęt / Akcesoria do sprzętu / Węże do gazu'),
(6842, 'Zdrowie i uroda / Higiena osobista / Golenie i strzyżenie / Akcesoria do trymerów do włosów'),
(6843, 'Żywność, napoje i tytoń / Żywność / Produkty z tofu, soi i wegetariańskie / Substytuty mięsa'),
(6844, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Polewy do deserów / Syropy do lodów'),
(6845, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Polewy do deserów'),
(6846, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Pomoce do szkolenia zwierząt / Maty treningowe dla zwierząt'),
(6847, 'Żywność, napoje i tytoń / Żywność / Przekąski / Polewy do sałatek'),
(6848, 'Żywność, napoje i tytoń / Napoje / Napoje w proszku'),
(6850, 'Meble / Ławki / Ławki do kuchni i jadalni'),
(6851, 'Meble / Ławki / Ławki do garderoby i przedpokoju'),
(6852, 'Sprzęt sportowy / Sport / Łyżwiarstwo figurowe i hokej / Części kijów hokejowych / Łopatki kijów hokejowych'),
(6853, 'Gry i zabawki / Gry / Akcesoria do gier karcianych'),
(6854, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Polewy do deserów / Polewy owocowe'),
(6857, 'Sprzęt sportowy / Sport / Łyżwiarstwo figurowe i hokej / Sledge hokejowe'),
(6858, 'Dom i ogród / Akcesoria łazienkowe / Zestawy akcesoriów łazienkowych'),
(6859, 'Meble / Krzesła i fotele / Fotele bez podłokietników'),
(6860, 'Meble / Ramy do futonów'),
(6861, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Plastry i bandaże dla zwierząt'),
(6862, 'Zdrowie i uroda / Higiena osobista / Środki do higieny intymnej dla kobiet / Dezodoranty do higieny intymnej'),
(6863, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja skóry / Środki do usuwania kurzajek'),
(6864, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Golf / Znaczniki piłek golfowych'),
(6865, 'Elektronika / Drukowanie, kopiowanie, skanowanie i faksowanie / Drukarki 3D'),
(6867, 'Sprzęt sportowy / Fitness / Ściskacze do rąk'),
(6868, 'Sprzęt / Narzędzia / Strugarki wzdłużne'),
(6869, 'Dzieci i niemowlęta / Bezpieczeństwo niemowląt / Akcesoria do bramek ochronnych dla dzieci i zwierząt'),
(6870, 'Ubrania i akcesoria / Biżuteria / Akcesoria do zegarków / Rotomaty do zegarków'),
(6871, 'Zdrowie i uroda / Opieka zdrowotna / Zdrowy tryb życia i dieta / Purée energetyczne'),
(6873, 'Żywność, napoje i tytoń / Żywność / Mrożone desery i dodatki / Przekąski lodowe'),
(6876, 'Sprzęt / Akcesoria do sprzętu / Przechowywanie i porządkowanie narzędzi / Torby na narzędzia'),
(6878, 'Dom i ogród / Akcesoria dla palaczy / Humidory'),
(6879, 'Dom i ogród / Akcesoria dla palaczy / Obcinacze do cygar'),
(6880, 'Dom i ogród / Akcesoria dla palaczy / Akcesoria do humidorów'),
(6881, 'Dom i ogród / Akcesoria dla palaczy / Papierośnice'),
(6882, 'Dom i ogród / Akcesoria dla palaczy / Pudełka na cygara'),
(6883, 'Dzieci i niemowlęta / Przewijanie / Organizery na pieluchy'),
(6884, 'Artykuły biurowe / Sortowanie i utrzymanie porządku / Teczki i ofertówki'),
(6885, 'Artykuły biurowe / Sortowanie i utrzymanie porządku / Organizery typu portfolio i padfolio'),
(6886, 'Elektronika / Audio / Odtwarzacze i nagrywarki audio / Radia'),
(6887, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do narzędzi malarskich / Akcesoria do aerografów'),
(6888, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do ekspresów do kawy i espresso / Podgrzewacze do dzbanków do kawy'),
(6891, 'Żywność, napoje i tytoń / Żywność / Wypieki / Muszle do taco'),
(6892, 'Meble / Meble zewnętrzne / Łóżka ogrodowe'),
(6893, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja paznokci / Kleje do tipsów'),
(6897, 'Dom i ogród / Przygotowanie na sytuacje kryzysowe / Koce ratunkowe'),
(6898, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Produkty do pielęgnacji koni / Derki i koce dla koni'),
(6899, 'Dzieci i niemowlęta / Beciki i kocyki niemowlęce'),
(6904, 'Sprzęt / Akcesoria do sprzętu / Łańcuchy, druty i sznury / Druty'),
(6905, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Sosy curry'),
(6906, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Kapelusze imprezowe'),
(6907, 'Sprzęt / Akcesoria do narzędzi / Osadzarki'),
(6908, 'Meble / Meble biurowe / Stanowiska pracy i boksy'),
(6909, 'Meble / Akcesoria do mebli biurowych / Akcesoria do stanowisk pracy i boksów'),
(6910, 'Meble / Akcesoria stołowe / Blaty stołowe'),
(6911, 'Meble / Akcesoria stołowe / Nogi stołowe'),
(6912, 'Dom i ogród / Ozdoby / Zegary / Zegary biurkowe'),
(6913, 'Meble / Akcesoria stołowe'),
(6914, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Sprzęt jeździecki / Bryczesy'),
(6915, 'Meble / Akcesoria do przepierzeń'),
(6919, 'Torby, walizki i akcesoria podróżne / Akcesoria podróżne / Butelki i pojemniki podróżne'),
(6920, 'Zdrowie i uroda / Higiena osobista / Higiena jamy ustnej / Akcesoria do szczoteczek do zębów / Osłony na szczoteczki do zębów'),
(6921, 'Zdrowie i uroda / Higiena osobista / Namioty do opalania natryskowego'),
(6922, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Pasta pomidorowa'),
(6927, 'Dom i ogród / Ozdoby / Poduszki do otoman'),
(6928, 'Sprzęt / Narzędzia / Drabiny i rusztowania / Drabiny'),
(6929, 'Zdrowie i uroda / Opieka zdrowotna / Artykuły dla osób mających trudności z poruszaniem się / Akcesoria do pomocy ułatwiających chodzenie'),
(6930, 'Artykuły biurowe / Artykuły różne / Artykuły papiernicze / Czeki'),
(6932, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Szafy na serwery blade'),
(6933, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Akcesoria do zasilaczy'),
(6934, 'Meble / Szafy i przechowywanie / Szafki kuchenne'),
(6935, 'Dom i ogród / Ozdoby / Ekokule'),
(6936, 'Dom i ogród / Ozdoby / Kwiaty suszone'),
(6938, 'Sprzęt / Narzędzia / Piaskarki'),
(6939, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do piaskowania'),
(6940, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do piaskowania / Kabiny do piaskowania'),
(6942, 'Sprzęt sportowy / Sport / Łyżwiarstwo figurowe i hokej / Części kijów hokejowych / Rękojeści kijów hokejowych'),
(6943, 'Sprzęt / Materiały budowlane / Okiennice'),
(6944, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do zestawów do fondue'),
(6945, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do zestawów do fondue / Widelce do fondue'),
(6946, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do zestawów do fondue / Stojaki do garnków do fondue'),
(6947, 'Meble / Szafy i przechowywanie / Skrzynie i kufry / Skrzynie'),
(6949, 'Dzieci i niemowlęta / Przewijanie / Ceraty do pieluch'),
(6950, 'Dzieci i niemowlęta / Karmienie / Kubki-niekapki'),
(6951, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla rybek / Pudełka przelewowe do akwariów'),
(6952, 'Dzieci i niemowlęta / Trening czystości'),
(6953, 'Dzieci i niemowlęta / Trening czystości / Zestawy do treningu czystości'),
(6955, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Ozdoby / Łatki z tkaniny'),
(6956, 'Dom i ogród / Kuchnia i jadalnia / Naczynia barowe / Shakery i przybory do drinków / Shakery do drinków'),
(6957, 'Dom i ogród / Kuchnia i jadalnia / Naczynia barowe / Zestawy przyborów do drinków'),
(6958, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Naczynia do napojów / Zestawy naczyń do napojów'),
(6960, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Rogi rowerowe'),
(6962, 'Artykuły biurowe / Sortowanie i utrzymanie porządku / Sortery przesyłek pocztowych'),
(6963, 'Meble / Akcesoria do mebli zewnętrznych'),
(6964, 'Meble / Akcesoria do mebli zewnętrznych / Pokrowce na meble zewnętrzne'),
(6965, 'Sprzęt / Narzędzia / Zestawy narzędzi / Zestawy narzędzi ręcznych'),
(6966, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Samochodowe elementy bezpieczeństwa / Samochodowe chorągiewki ostrzegawcze'),
(6967, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Narzędzia ogrodnicze / Piły ogrodnicze'),
(6968, 'Pojazdy i części / Akcesoria i części do pojazdów / Wyposażenie elektroniczne pojazdów silnikowych / Kamery parkowania'),
(6969, 'Meble / Meble dla dzieci i niemowląt / Akcesoria do wysokich krzeseł i fotelików samochodowych'),
(6970, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do wzmacniaczy do instrumentów muzycznych / Wzmacniacze kolumnowe do instrumentów muzycznych'),
(6971, 'Dzieci i niemowlęta / Przewijanie / Woreczki na pieluchy'),
(6972, 'Biznes i przemysł / Branża medyczna / Sprzęt medyczny / Młotki neurologiczne'),
(6973, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Stopnie i rampy dla zwierząt'),
(6974, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Dozowniki cukru'),
(6975, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Odczynniki biochemiczne'),
(6977, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja oczu / Szkła do okularów przeciwsłonecznych'),
(6978, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Urządzenia do pomiarów biometrycznych dla zwierząt'),
(6979, 'Elektronika / Akcesoria elektroniczne / Akcesoria komputerowe / Podstawki pod klawiaturę i myszkę'),
(6980, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Urządzenia do pomiarów biometrycznych dla zwierząt / Glukometry dla zwierząt'),
(6981, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Urządzenia do pomiarów biometrycznych dla zwierząt / Termometry dla zwierząt'),
(6982, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Urządzenia do pomiarów biometrycznych dla zwierząt / Krokomierze dla zwierząt'),
(6983, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Sprzęt do agility dla zwierząt'),
(6984, 'Ubrania i akcesoria / Akcesoria do ubrań / Usztywnienia kołnierzyka'),
(6985, 'Ubrania i akcesoria / Akcesoria do ubrań / Spinki'),
(6986, 'Dom i ogród / Artykuły gospodarstwa domowego / Przechowywanie i utrzymanie porządku / Pojemniki do przechowywania z przegródkami'),
(6987, 'Biznes i przemysł / Transport materiałów'),
(6988, 'Biznes i przemysł / Transport materiałów / Przenośniki'),
(6989, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Zestawy do sztuki i rękodzieła / Zestawy do robienia kadzidełek'),
(6990, 'Biznes i przemysł / Rolnictwo / Hodowla zwierząt / Karmniki dla zwierząt hodowlanych'),
(6991, 'Biznes i przemysł / Rolnictwo / Hodowla zwierząt'),
(6992, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Myślistwo / Karmniki dla dzikiej zwierzyny'),
(6993, 'Dom i ogród / Ozdoby / Karmniki'),
(6994, 'Dom i ogród / Ozdoby / Karmniki / Karmniki dla wiewiórek'),
(6995, 'Dom i ogród / Ozdoby / Karmniki / Karmniki dla motyli'),
(6996, 'Dom i ogród / Baseny i spa / Akcesoria do basenów i spa / Szczotki do basenów'),
(6997, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Karty kolekcjonerskie'),
(6999, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Szablony i formy / Wzory do koralików'),
(7000, 'Sprzęt sportowy / Sport / Łyżwiarstwo figurowe i hokej / Części i akcesoria do łyżew / Płozy łyżew'),
(7001, 'Dzieci i niemowlęta / Przewijanie / Akcesoria do kubłów na pieluchy'),
(7002, 'Zdrowie i uroda / Opieka zdrowotna / Antykoncepcja'),
(7003, 'Ubrania i akcesoria / Ubrania / Ubrania sportowe / Stroje ochronne na motor / Spodnie motocyklowe'),
(7004, 'Sprzęt / Materiały budowlane / Klatki schodowe'),
(7006, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do kuchenek przenośnych'),
(7007, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Dekoracje koktajlowe'),
(7008, 'Dom i ogród / Kuchnia i jadalnia / Naczynia barowe / Zawieszki do kieliszków do wina'),
(7010, 'Sprzęt sportowy / Gry towarzyskie / Stoły do wielu gier'),
(7011, 'Sprzęt sportowy / Sport / Łyżwiarstwo figurowe i hokej / Części kijów hokejowych'),
(7012, 'Sprzęt sportowy / Sport / Łyżwiarstwo figurowe i hokej / Środki do konserwacji kijów hokejowych'),
(7014, 'Dzieci i niemowlęta / Przewijanie / Zestawy do przewijania'),
(7016, 'Dzieci i niemowlęta / Zdrowie niemowląt / Klipsy i uchwyty do smoczków'),
(7018, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do pielęgnacji twarzy / Masażery do twarzy'),
(7019, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do narzędzi malarskich'),
(7020, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do narzędzi malarskich / Akcesoria do wałków malarskich'),
(7022, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Elementy ozdobne do pojazdów / Osłona na tablice rejestracyjne'),
(7029, 'Dom i ogród / Akcesoria do kominków i pieców opalanych węglem / Akcesoria do stojaków i pojemników na drewno'),
(7030, 'Sprzęt / Narzędzia / Rozdrabniacze do drewna'),
(7031, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Osłony i pokrowce na pojazdy / Dachy miękkie'),
(7052, 'Dom i ogród / Ozdoby / Akcesoria do skrzynek pocztowych / Osłony na skrzynki pocztowe'),
(7053, 'Sprzęt / Materiały budowlane / Panele ścienne'),
(7054, 'Ubrania i akcesoria / Akcesoria do ubrań / Nakrycia głowy / Toczki'),
(7055, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do młotków / Trzonki do młotków'),
(7056, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do młotków'),
(7057, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do młotków / Obuchy do młotków'),
(7058, 'Dom i ogród / Przygotowanie na sytuacje kryzysowe / Zestawy ratunkowe'),
(7059, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wspinaczka'),
(7060, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wspinaczka / Sprzęt do wspinaczki lodowej'),
(7061, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wspinaczka / Śruby lodowe'),
(7062, 'Sprzęt / Akcesoria do sprzętu / Akcesoria mocujące do sprzętu / Nity'),
(7063, 'Biznes i przemysł / Branża medyczna / Medyczne materiały eksploatacyjne / Osłonki na palce'),
(7064, 'Sprzęt / Narzędzia / Narzędzia do nitowania'),
(7065, 'Sprzęt / Narzędzia / Narzędzia do nitowania / Młotki nitownicze'),
(7066, 'Sprzęt / Narzędzia / Narzędzia do nitowania / Szczypce do nitów'),
(7068, 'Meble / Meble dla dzieci i niemowląt / Akcesoria do koszyków i kołysek'),
(7070, 'Meble / Meble dla dzieci i niemowląt / Akcesoria do łóżeczek i łóżek dziecinnych'),
(7071, 'Meble / Meble dla dzieci i niemowląt / Akcesoria do łóżeczek i łóżek dziecinnych / Barierki do łóżeczek dziecinnych'),
(7072, 'Meble / Meble dla dzieci i niemowląt / Akcesoria do łóżeczek i łóżek dziecinnych / Osłonki do łóżeczek dziecinnych'),
(7075, 'Dom i ogród / Kuchnia i jadalnia / Naczynia barowe / Fontanny do absyntu'),
(7076, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Tekstylia / Flizelina'),
(7077, 'Sprzęt / Narzędzia / Piły / Przecinarki do cegieł i glazury'),
(7078, 'Ubrania i akcesoria / Akcesoria do butów / Sztylpy'),
(7080, 'Artykuły biurowe / Sortowanie i utrzymanie porządku / Oprawa dokumentów / Bindownice'),
(7081, 'Gry i zabawki / Łamigłówki / Akcesoria do puzzli'),
(7082, 'Dzieci i niemowlęta / Kąpanie niemowlęcia / Daszki pod prysznic'),
(7085, 'Biznes i przemysł / Środki ochrony osobistej / Uwięzie zabezpieczające'),
(7086, 'Sprzęt / Akcesoria do sprzętu / Odlewy z betonu'),
(7087, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do młotków / Akcesoria do młotków pneumatycznych'),
(7088, 'Biznes i przemysł / Usługi gastronomiczne / Naczynia jednorazowe'),
(7089, 'Biznes i przemysł / Usługi gastronomiczne / Naczynia jednorazowe / Tacki jednorazowe'),
(7090, 'Gry i zabawki / Zabawki / Zabawki zdalnie sterowane / Sterowce zdalnie sterowane'),
(7091, 'Dom i ogród / Akcesoria do kominków i pieców opalanych węglem / Podkładki niepalne przed palenisko'),
(7092, 'Sprzęt / Akcesoria do sprzętu / Wsporniki'),
(7093, 'Dom i ogród / Akcesoria łazienkowe / Mocowania akcesoriów łazienkowych'),
(7096, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Zestawy do sztuki i rękodzieła / Zestawy do haftu gobelinowego'),
(7097, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Stojaki i pojemniki na kartki z życzeniami'),
(7099, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do perkusji / Osprzęt do perkusji'),
(7100, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do perkusji / Futerały na talerze i bębny'),
(7101, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do perkusji / Osprzęt do perkusji / Stopy perkusyjne'),
(7102, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do perkusji / Osprzęt do perkusji / Mocowania perkusji'),
(7103, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do perkusji / Osprzęt do perkusji / Bijaki do bębna basowego'),
(7104, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Pływanie / Skrzydełka do pływania'),
(7107, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Akcesoria do parasoli i pawilonów ogrodowych / Pokrowce na parasole ogrodowe'),
(7108, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Akcesoria do parasoli i pawilonów ogrodowych / Tkaniny do parasoli i pawilonów ogrodowych'),
(7109, 'Dom i ogród / Akcesoria do kominków i pieców opalanych węglem / Ekrany kominkowe'),
(7110, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do kaloryferów'),
(7111, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do kaloryferów / Folie odbijające ciepło'),
(7112, 'Sprzęt / Materiały budowlane / Odlewy'),
(7113, 'Dom i ogród / Ozdoby / Słoje ozdobne'),
(7114, 'Gry i zabawki / Zabawki / Lalki, zestawy i figurki / Figurki bobblehead'),
(7115, 'Pojazdy i części / Akcesoria i części do pojazdów / Przewóz i przechowywanie bagażu / Bagażniki samochodowe / Uchwyty do broni'),
(7116, 'Sprzęt sportowy / Sport / Boks i sztuki walki / Trening bokserski i sztuk walki / Kukły do walki na chwyty'),
(7117, 'Artykuły biurowe / Akcesoria biurowe / Akcesoria do przyborów piśmienniczych i rysunkowych / Tusze do markerów i zakreślaczy'),
(7118, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do dystrybutorów wody'),
(7119, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do dystrybutorów wody / Butelki do dystrybutorów wody'),
(7120, 'Artykuły religijne i dewocjonalia / Artykuły religijne / Ołtarzyki'),
(7121, 'Dom i ogród / Sprzęt AGD / Parowe odklejacze do tapet'),
(7122, 'Pojazdy i części / Akcesoria i części do pojazdów / Przewóz i przechowywanie bagażu / Akcesoria do bagażników bazowych / Akcesoria do bagażników do przewozu rowerów'),
(7125, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Materiały do wiązania przynęty / Koraliki wędkarskie'),
(7127, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Barwniki spożywcze'),
(7128, 'Biznes i przemysł / Handel detaliczny / Części manekinów'),
(7129, 'Sprzęt sportowy / Sport / Boks i sztuki walki / Trening bokserski i sztuk walki / Akcesoria do worków treningowych'),
(7130, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Części do prysznica / Ścianki prysznicowe'),
(7131, 'Dom i ogród / Kuchnia i jadalnia / Garnki i formy do pieczenia / Akcesoria do form do pieczenia / Wagi do ciasta'),
(7132, 'Ubrania i akcesoria / Ubrania / Stroje jednoczęściowe / Ogrodniczki'),
(7133, 'Ubrania i akcesoria / Akcesoria do ubrań / Mufki'),
(7134, 'Zdrowie i uroda / Higiena osobista / Zestawy do lewatywy'),
(7135, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Zasilacze podróżne'),
(7136, 'Sprzęt / Materiały budowlane / Płytki'),
(7137, 'Dom i ogród / Artykuły gospodarstwa domowego / Dezynsekcja / Środki odstraszające / Środki odstraszające zwierzęta'),
(7138, 'Zdrowie i uroda / Opieka zdrowotna / Artykuły dla osób mających trudności z poruszaniem się / Sprzęt ułatwiający poruszanie się / Platformy przyschodowe'),
(7139, 'Dom i ogród / Kuchnia i jadalnia / Naczynia barowe / Noże do folii'),
(7140, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do wiercenia / Przedłużenia do wierteł'),
(7142, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla kotów / Maty pod kuwetę'),
(7143, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Podkładki pod miski dla zwierząt'),
(7144, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Środki do pielęgnacji jamy ustnej zwierząt'),
(7148, 'Gry i zabawki / Zabawki / Zabawki ruchowe / Części i akcesoria do jojo'),
(7149, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Dystrybutory do napojów'),
(7150, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do perkusji / Akcesoria do pałek i miotełek perkusyjnych'),
(7151, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do perkusji / Akcesoria do pałek i miotełek perkusyjnych / Pokrowce na pałki i miotełki perkusyjne'),
(7152, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do perkusji / Klucze do strojenia perkusji'),
(7153, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do perkusji / Naciągi perkusyjne'),
(7154, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Akcesoria kijków trekkingowych'),
(7156, 'Sprzęt sportowy / Sport / Palant'),
(7157, 'Sprzęt sportowy / Sport / Palant / Rękawice do gry rounders'),
(7158, 'Sprzęt sportowy / Sport / Palant / Kije do gry rounders'),
(7159, 'Żywność, napoje i tytoń / Żywność / Przekąski / Grissini'),
(7160, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Gry na przyjęcia'),
(7163, 'Elektronika / Audio / Akcesoria audio / Akcesoria do głośników'),
(7165, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Płyty i grille elektryczne'),
(7166, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Akcesoria do akumulatorów'),
(7167, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Akcesoria do akumulatorów / Ładowarki do akumulatorów do zastosowań ogólnych'),
(7168, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Akcesoria do dmuchaw do liści'),
(7169, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Akcesoria do nożyc do chwastów'),
(7170, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Akcesoria do nożyc do chwastów / Ostrza i żyłki do nożyc'),
(7171, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Akcesoria do dmuchaw do liści / Przedłużki do dmuchaw do liści'),
(7172, 'Dom i ogród / Ozdoby / Łapacze snów'),
(7173, 'Dom i ogród / Ozdoby / Witrażyki'),
(7174, 'Sprzęt sportowy / Fitness / Piłki zręcznościowe'),
(7175, 'Artykuły dla dorosłych / Broń / Włócznie'),
(7176, 'Dom i ogród / Ozdoby / Akcesoria do skrzynek pocztowych / Flagi do skrzynek pocztowych'),
(7177, 'Dom i ogród / Ozdoby / Akcesoria do skrzynek pocztowych / Ozdobne osłony skrzynek pocztowych'),
(7178, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Stojaki na jednostki pływające'),
(7182, 'Elektronika / Akcesoria elektroniczne / Adaptery / Adaptery USB'),
(7183, 'Sprzęt / Materiały elektryczne / Dławiki i startery'),
(7184, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Akcesoria ogrodnicze / Stoły do przesadzania roślin'),
(7186, 'Zdrowie i uroda / Opieka zdrowotna / Środki plemnikobójcze'),
(7187, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do maszynek do lodu'),
(7188, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty dęte drewniane / Flutofony'),
(7190, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do pielęgnacji twarzy / Pilniki do stóp'),
(7191, 'Dzieci i niemowlęta / Zabawki dla niemowląt / Akcesoria do karuzel dla niemowląt'),
(7192, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Kleje i magnesy dla rękodzieła / Taśmy florystyczne'),
(7193, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Trawa pszeniczna'),
(7196, 'Żywność, napoje i tytoń / Żywność / Ziarna, ryż i płatki / Kasze kuskus'),
(7197, 'Meble / Krzesła i fotele / Krzesła wiszące'),
(7198, 'Dzieci i niemowlęta / Zabawki dla niemowląt / Zabawki interaktywne dla niemowląt'),
(7199, 'Zdrowie i uroda / Higiena osobista / Golenie i strzyżenie / Usuwanie włosów / Urządzenia do depilacji laserowej i IPL'),
(7200, 'Dzieci i niemowlęta / Przewijanie / Zasobniki i podgrzewacze na chusteczki dla niemowląt'),
(7202, 'Gry i zabawki / Zabawki / Akcesoria do pojazdów dziecięcych'),
(7206, 'Dom i ogród / Ozdoby / Dzwonki dekoracyjne'),
(7207, 'Ubrania i akcesoria / Ubrania / Bielizna i skarpety / Dodatki do biustonoszy'),
(7208, 'Ubrania i akcesoria / Ubrania / Bielizna i skarpety / Dodatki do biustonoszy / Podkładki pod ramiączka'),
(7209, 'Ubrania i akcesoria / Ubrania / Bielizna i skarpety / Dodatki do biustonoszy / Osłonki na sutki'),
(7210, 'Ubrania i akcesoria / Ubrania / Bielizna i skarpety / Dodatki do biustonoszy / Wkładki do biustonoszy'),
(7211, 'Ubrania i akcesoria / Ubrania / Bielizna i skarpety / Dodatki do biustonoszy / Ramiączka i przedłużki do biustonoszy');
INSERT INTO `google_kategorie` (`id`, `nazwa`) VALUES
(7212, 'Meble / Akcesoria do sof'),
(7213, 'Meble / Akcesoria do sof / Podpórki pod sofy i krzesła'),
(7214, 'Dom i ogród / Artykuły gospodarstwa domowego / Podkładki ochronne pod meble'),
(7215, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Akcesoria do uprzęży jeździeckich'),
(7216, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Akcesoria do uprzęży jeździeckich / Osprzęt do siodeł / Stojaki na siodła'),
(7217, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Materiały do wiązania przynęty'),
(7218, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Sprzęt laboratoryjny / Maszyny do suchego lodu'),
(7219, 'Gry i zabawki / Zabawki ogrodowe / Akcesoria do zamków dmuchanych'),
(7220, 'Zdrowie i uroda / Opieka zdrowotna / Baseny dla chorych'),
(7221, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Więcierze'),
(7222, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Przybory wędkarskie / Przypony'),
(7223, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Strzemiona i zaczepy'),
(7224, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Jazda na nartach i snowboardzie / Torby narciarskie i snowboardowe'),
(7225, 'Oprogramowanie / Programy komputerowe / Dyski ratunkowe'),
(7226, 'Dom i ogród / Bezpieczeństwo powodziowe, ogniowe i gazowe / Panele ochrony przeciwpożarowej'),
(7227, 'Dom i ogród / Bezpieczeństwo powodziowe, ogniowe i gazowe / Wykrywacze ciepła'),
(7230, 'Ubrania i akcesoria / Akcesoria do ubrań / Kominy na szyję'),
(7231, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do perkusji / Tłumiki do perkusji'),
(7232, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Pady ćwiczeniowe'),
(7234, 'Dzieci i niemowlęta / Karmienie / Pojemniki na pokarm matki'),
(7235, 'Ubrania i akcesoria / Ubrania / Uniformy / Odzież gastronomiczna'),
(7236, 'Ubrania i akcesoria / Ubrania / Uniformy / Odzież gastronomiczna / Spodnie szefa kuchni'),
(7237, 'Ubrania i akcesoria / Ubrania / Uniformy / Odzież gastronomiczna / Czapki szefa kuchni'),
(7238, 'Dom i ogród / Kuchnia i jadalnia / Naczynia barowe / Stojaki na podkładki'),
(7240, 'Biznes i przemysł / Fryzjerstwo i kosmetologia'),
(7241, 'Biznes i przemysł / Fryzjerstwo i kosmetologia / Fotele do salonów kosmetycznych'),
(7242, 'Biznes i przemysł / Fryzjerstwo i kosmetologia / Fotele do pedicure'),
(7243, 'Zdrowie i uroda / Opieka zdrowotna / Artykuły dla osób mających trudności z poruszaniem się / Meble i armatura ułatwiające poruszanie się / Siedziska prysznicowe'),
(7244, 'Sprzęt / Hydraulika / Armatury / Kabiny i zestawy prysznicowe'),
(7245, 'Dom i ogród / Trawnik i ogród / Silnikowe narzędzia ogrodowe / Zestawy elektronarzędzi ogrodowych'),
(7247, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Maty do sushi'),
(7248, 'Meble / Akcesoria do krzeseł'),
(7249, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty dęte drewniane / Okaryny'),
(7250, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty dęte drewniane / Inne instrumenty dęte drewniane'),
(7251, 'Sprzęt sportowy / Sport / Koszykówka / Części i akcesoria do koszy / Obicia do koszy'),
(7252, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Koła do pojazdów silnikowych / Opony / Opony do pojazdów terenowych'),
(7253, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Koła do pojazdów silnikowych / Koła i felgi / Felgi i koła do pojazdów terenowych'),
(7255, 'Dom i ogród / Artykuły gospodarstwa domowego / Przechowywanie i utrzymanie porządku / Systemy szuflad'),
(7256, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do makijażu / Akcesoria do sztucznych rzęs / Kleje do sztucznych rzęs'),
(7257, 'Elektronika / Płyty drukowane i komponenty / Półprzewodniki / Układy scalone'),
(7258, 'Elektronika / Płyty drukowane i komponenty / Filtry elektroniczne'),
(7259, 'Elektronika / Płyty drukowane i komponenty / Układy kodujące i dekodujące'),
(7260, 'Elektronika / Płyty drukowane i komponenty / Elementy pasywne obwodów / Oscylatory elektroniczne'),
(7261, 'Biznes i przemysł / Komponenty automatyki przemysłowej'),
(7262, 'Biznes i przemysł / Komponenty automatyki przemysłowej / Sterowniki częstotliwości i prędkości obrotowej'),
(7263, 'Biznes i przemysł / Komponenty automatyki przemysłowej / Sterowniki PLC'),
(7264, 'Elektronika / Płyty drukowane i komponenty / Obwody drukowane'),
(7265, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Akcesoria do nożyc do żywopłotu'),
(7270, 'Sprzęt / Akcesoria do sprzętu / Kołki i sworznie'),
(7271, 'Sprzęt / Narzędzia / Heblarki'),
(7274, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla psów / Budy i boksy dla psów'),
(7275, 'Sprzęt / Materiały elektryczne / Silniki elektryczne'),
(7277, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do stojaków na nuty'),
(7278, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do stojaków na nuty / Klipsy do nut'),
(7279, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do stojaków na nuty / Torby na stojaki na nuty'),
(7280, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do stojaków na nuty / Oświetlenie do stojaków na nuty'),
(7281, 'Ubrania i akcesoria / Ubrania / Stroje tradycyjne i ceremonialne / Tradycyjne spodnie skórzane'),
(7282, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do perkusji / Akcesoria do ręcznych instrumentów perkusyjnych'),
(7283, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do perkusji / Akcesoria do ręcznych instrumentów perkusyjnych / Torby i pokrowce na ręczne instrumenty perkusyjne'),
(7284, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do perkusji / Akcesoria do ręcznych instrumentów perkusyjnych / Stojaki do ręcznych instrumentów perkusyjnych'),
(7285, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Ręczne instrumenty perkusyjne'),
(7286, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Ręczne instrumenty perkusyjne / Tarki i grzechotki'),
(7287, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Ręczne instrumenty perkusyjne / Przeszkadzajki'),
(7288, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Ręczne instrumenty perkusyjne / Saggaty'),
(7289, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Ręczne instrumenty perkusyjne / Klawesy i kastaniety'),
(7290, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Ręczne instrumenty perkusyjne / Trójkąty muzyczne'),
(7291, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Ręczne instrumenty perkusyjne / Klocki muzyczne'),
(7293, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Ręczne instrumenty perkusyjne / Cowbelle'),
(7294, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Ręczne instrumenty perkusyjne / Vibraslapy'),
(7295, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Ręczne instrumenty perkusyjne / Bębny ręczne'),
(7296, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Ręczne instrumenty perkusyjne / Bębny ręczne / Kongi'),
(7297, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Ręczne instrumenty perkusyjne / Bębny ręczne / Kachony'),
(7298, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Ręczne instrumenty perkusyjne / Bębny ręczne / Bongosy'),
(7299, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Ręczne instrumenty perkusyjne / Bębny ręczne / Bębny kielichowe'),
(7300, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Ręczne instrumenty perkusyjne / Bębny ręczne / Bębny obręczowe'),
(7301, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Ręczne instrumenty perkusyjne / Bębny ręczne / Bębny gadające'),
(7302, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Ręczne instrumenty perkusyjne / Bębny ręczne / Table'),
(7303, 'Biznes i przemysł / Usługi gastronomiczne / Pudełka cukiernicze'),
(7304, 'Ubrania i akcesoria / Przebrania i akcesoria / Akcesoria do przebrań / Czepki imitujące łysinę'),
(7305, 'Ubrania i akcesoria / Akcesoria do ubrań / Akcesoria do włosów / Akcesoria do peruk'),
(7306, 'Ubrania i akcesoria / Akcesoria do ubrań / Akcesoria do włosów / Akcesoria do peruk / Kleje i taśmy do peruk'),
(7307, 'Ubrania i akcesoria / Akcesoria do ubrań / Akcesoria do włosów / Akcesoria do peruk / Czepki pod peruki'),
(7308, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do perkusji / Stojaki do perkusji'),
(7309, 'Dzieci i niemowlęta / Zdrowie niemowląt / Chusteczki do smoczków'),
(7310, 'Meble / Meble zewnętrzne / Pojemniki i skrzynie ogrodowe'),
(7311, 'Gry i zabawki / Zabawki / Akcesoria do basenów kulkowych'),
(7312, 'Gry i zabawki / Zabawki / Akcesoria do basenów kulkowych / Kulki do basenów kulkowych'),
(7313, 'Ubrania i akcesoria / Ubrania / Komplety ubrań'),
(7314, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Golf / Zestawy do gry w golfa'),
(7316, 'Zdrowie i uroda / Opieka zdrowotna / Środki ochrony układu oddechowego / Maski CPAP'),
(7317, 'Zdrowie i uroda / Opieka zdrowotna / Środki ochrony układu oddechowego / Aparaty CPAP'),
(7318, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły groomerskie dla zwierząt / Lakiery do pazurów'),
(7319, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły groomerskie dla zwierząt / Narzędzia do pielęgnacji pazurów'),
(7320, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły do pralni / Zestawy do prania chemicznego'),
(7322, 'Biznes i przemysł / Informacja wizualna / Tablice z godzinami otwarcia'),
(7323, 'Biznes i przemysł / Informacja wizualna / Szyldy „Otwarte” i „Zamknięte”'),
(7324, 'Biznes i przemysł / Branża medyczna / Medyczne materiały eksploatacyjne / Szpatułki'),
(7325, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Zestawy autopsyjne'),
(7326, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do siekier'),
(7327, 'Żywność, napoje i tytoń / Żywność / Przekąski / Ciastka ryżowe'),
(7328, 'Dom i ogród / Sprzęt AGD / Ogrzewanie, wentylacja i klimatyzacja / Piece nadmuchowe'),
(7329, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Torebki do gotowania na parze'),
(7330, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Środki czyszczące do gospodarstwa domowego / Uniwersalne środki czyszczące'),
(7331, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Akcesoria do termometrów kuchennych'),
(7332, 'Dom i ogród / Trawnik i ogród / Silnikowe narzędzia ogrodowe / Głowice do elektronarzędzi ogrodowych'),
(7333, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Akumulatory do elektronarzędzi ogrodowych'),
(7334, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Przystawki do wielofunkcyjnych narzędzi ogrodowych / Nasadki do nożyc do żywopłotu'),
(7335, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Przystawki do wielofunkcyjnych narzędzi ogrodowych / Przystawki do nożyc do chwastów'),
(7336, 'Zdrowie i uroda / Opieka zdrowotna / Testy medyczne / Testy wykrywające HIV'),
(7337, 'Zdrowie i uroda / Opieka zdrowotna / Testy medyczne / Testy do badania grupy krwi'),
(7338, 'Dom i ogród / Akcesoria oświetleniowe / Podstawy pod lampy'),
(7339, 'Dom i ogród / Ozdoby / Akcesoria do skrzynek pocztowych / Drzwiczki zamienne do skrzynek pocztowych'),
(7340, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do wycinania i wytłaczania / Narzędzia do wytłaczania na gorąco'),
(7342, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Osęki'),
(7343, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Harpuny'),
(7344, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Narzędzia do usuwania haczyków'),
(7345, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do pił / Akcesoria do ręcznych pilarek tarczowych'),
(7346, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do pił / Akcesoria do wyrzynarek'),
(7347, 'Elektronika / Telekomunikacja / Telefonia / Akcesoria do telefonów komórkowych / Części zamienne do telefonów komórkowych'),
(7349, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Części do tabletów'),
(7351, 'Dom i ogród / Artykuły gospodarstwa domowego / Materiały do wykładania szuflad i półek'),
(7353, 'Biznes i przemysł / Usługi gastronomiczne / Pojemniki na lód'),
(7354, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Kultury starterowe'),
(7355, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do patelni i woków elektrycznych'),
(7356, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do makijażu / Kleje i taśmy do tworzenia drugiej powieki'),
(7357, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do keyboardów / Torby i futerały na keyboardy'),
(7358, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Akcesoria do toalet i bidetów / Nakładki na deski sedesowe'),
(7360, 'Dzieci i niemowlęta / Zabawki dla niemowląt / Smoczki dla niemowląt'),
(7362, 'Elektronika / Drukowanie, kopiowanie, skanowanie i faksowanie / Akcesoria do kopiarek, drukarek i faksów / Materiały eksploatacyjne do drukarek / Zestawy do napełniania kartridży'),
(7363, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja oczu / Pielęgnacja szkieł kontaktowych / Zestawy do pielęgnacji szkieł kontaktowych'),
(7364, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do wzmacniaczy do instrumentów muzycznych / Stojaki na wzmacniacze'),
(7365, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Akcesoria do noży do pizzy'),
(7366, 'Gry i zabawki / Zabawki / Akcesoria do zabawek latających'),
(7368, 'Gry i zabawki / Zabawki / Akcesoria do zabawek latających / Akcesoria do latawców'),
(7369, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do siekier / Głowice siekier'),
(7370, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do siekier / Rękojeści siekier'),
(7371, 'Gry i zabawki / Zabawki / Akcesoria do zabawek latających / Akcesoria do latawców / Kołowrotki i nawijarki do latawców'),
(7372, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla psów / Podkładki i wkładki higieniczne dla psów'),
(7373, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Myślistwo / Wzmacniacze słuchu'),
(7374, 'Żywność, napoje i tytoń / Żywność / Ziarna, ryż i płatki / Żyto'),
(7375, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Buggykiting'),
(7376, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Buggykiting / Wózki do buggykitingu'),
(7377, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Buggykiting / Akcesoria do buggykitingu'),
(7380, 'Dom i ogród / Ozdoby / Wycinanki z kartonu'),
(7382, 'Dom i ogród / Ozdoby / Cyfry i litery elewacyjne'),
(7383, 'Gry i zabawki / Gry / Akcesoria do żetonów pokerowych'),
(7384, 'Gry i zabawki / Gry / Akcesoria do żetonów pokerowych / Pojemniki na żetony pokerowe'),
(7385, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla ptaków / Akcesoria do klatek dla ptaków'),
(7386, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla ptaków / Akcesoria do klatek dla ptaków / Pojemniki na żywność i wodę dla ptaków'),
(7387, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Warzywa suszone'),
(7393, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Sprzęt laboratoryjny / Spektrometry'),
(7394, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Przetworniki'),
(7395, 'Elektronika / Elementy / Akcelerometry'),
(7396, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Środki przeciwsłoneczne dla zwierząt'),
(7397, 'Sprzęt sportowy / Sport / Sprzęt ogólnosportowy / Torby i wózki na piłki'),
(7398, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla ptaków / Zestawy rekreacyjne dla ptaków'),
(7399, 'Dom i ogród / Oświetlenie / Pochodnie tiki i lampy oliwne'),
(7400, 'Dom i ogród / Oświetlenie / Oświetlenie ścieżek ogrodowych'),
(7401, 'Dom i ogród / Oświetlenie / Oświetlenie naziemne'),
(7402, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Knoty'),
(7403, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Blaszki do knotów'),
(7404, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja pleców / Poduszki pod plecy i lędźwie'),
(7406, 'Dom i ogród / Artykuły gospodarstwa domowego / Pochłaniacze wilgoci'),
(7407, 'Biznes i przemysł / Środki ochrony osobistej / Maski ochronne / Maski przeciwpyłowe'),
(7411, 'Gry i zabawki / Gry / Zestawy do gry w blackjacka i craps'),
(7412, 'Gry i zabawki / Gry / Zestawy i koła do gry w ruletkę'),
(7413, 'Zdrowie i uroda / Opieka zdrowotna / Zdrowy tryb życia i dieta / Suplementy do karmienia przez sondę'),
(7414, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Narzędzia do naprawy pojazdów / Prostowniki do akumulatorów'),
(7415, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do narzędzi pomiarowych i czujników / Akcesoria do multimetrów'),
(7416, 'Sprzęt / Narzędzia / Lusterka inspekcyjne'),
(7417, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria i kosmetyki do kąpieli / Mydła w proszku do rąk'),
(7419, 'Dom i ogród / Ozdoby / Akcesoria do flag i wiatrowskazów'),
(7420, 'Dom i ogród / Ozdoby / Akcesoria do flag i wiatrowskazów / Oświetlenie flag i wiatrowskazów'),
(7421, 'Dom i ogród / Ozdoby / Akcesoria do flag i wiatrowskazów / Maszty do flag i wiatrowskazów'),
(7422, 'Dom i ogród / Ozdoby / Kwiatony'),
(7423, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Architektura ogrodowa / Akcesoria do altan i tarasów / Maszty do altan'),
(7424, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Architektura ogrodowa / Akcesoria do altan i tarasów / Obciążniki do altan'),
(7425, 'Gry i zabawki / Zabawki / Zabawki ruchowe / Zabawki z paletką i piłką'),
(7426, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Środki czyszczące do gospodarstwa domowego / Środki do czyszczenia i polerowania stali nierdzewnej'),
(7427, 'Żywność, napoje i tytoń / Żywność / Przekąski / Paluszki sezamowe'),
(7428, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla psów / Akcesoria do bud i boksów dla psów'),
(7429, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja skóry / Kosmetyki przeciwzmarszczkowe'),
(7430, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Gry na świeżym powietrzu / Zestawy do gier z rakietkami'),
(7431, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Dzwonki i ksylofony'),
(7433, 'Sprzęt sportowy / Sport / Sprzęt ogólnosportowy / Akcesoria do pompek'),
(7434, 'Sprzęt sportowy / Sport / Sprzęt ogólnosportowy / Pompki do piłek'),
(7435, 'Sprzęt sportowy / Sport / Sprzęt ogólnosportowy / Akcesoria do pompek / Igły do pompek'),
(7436, 'Dom i ogród / Ozdoby / Zdobienia otworów na listy'),
(7437, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Sprzęt laboratoryjny / Mikrotomy'),
(7438, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Akumulatory / Baterie do tabletów'),
(7439, 'Sprzęt / Materiały budowlane / Materiały dźwiękochłonne'),
(7444, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do tosterów'),
(7445, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja paznokci / Rozcieńczalniki lakieru do paznokci'),
(7446, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Akcesoria do toalet i bidetów / Nakładki na pokrywy desek sedesowych'),
(7447, 'Dom i ogród / Akcesoria oświetleniowe / Mocowania do lamp'),
(7448, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Koraliki na szprychy'),
(7449, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Pływanie łodzią i rafting / Rękawice żeglarskie'),
(7451, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Surfing / Rękawice surfingowe'),
(7452, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Sporty wodne holowane / Rękawiczki do sportów wodnych holowanych'),
(7453, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Odzież i akcesoria dla rowerzystów / Akcesoria do pedałów zatrzaskowych / Osłony na pedały zatrzaskowe'),
(7455, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do perkusji / Automaty perkusyjne'),
(7457, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły do pralni / Wkładki do odzieży pochłaniające pot'),
(7459, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Produkty do pielęgnacji koni / Witaminy i suplementy dla koni'),
(7460, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Odzież ochronna myśliwska i strzelecka'),
(7461, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Odzież ochronna myśliwska i strzelecka / Rękawice myśliwskie i strzeleckie'),
(7462, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Środki czyszczące do gospodarstwa domowego / Środki czyszczące do pralek'),
(7467, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja skóry / Zestawy do oczyszczania twarzy'),
(7468, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Sprzęt laboratoryjny / Akcesoria do spektrometrów'),
(7470, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Chemikalia / Środki czyszczące do szamb'),
(7471, 'Ubrania i akcesoria / Biżuteria / Akcesoria do zegarków / Naklejki na tarcze'),
(7472, 'Sprzęt / Akcesoria do narzędzi / Magnetyzery i demagnetyzery'),
(7473, 'Gry i zabawki / Zabawki / Zabawki ruchowe / Zabawki ze wstęgą'),
(7474, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Odzież i akcesoria dla rowerzystów / Akcesoria do pedałów zatrzaskowych'),
(7475, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Odzież i akcesoria dla rowerzystów / Akcesoria do pedałów zatrzaskowych / Podkładki do pedałów zatrzaskowych'),
(7476, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Odzież i akcesoria dla rowerzystów / Akcesoria do pedałów zatrzaskowych / Śruby mocujące'),
(7477, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Stery rowerowe'),
(7478, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Części do sterów'),
(7479, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Części do sterów / Dystanse sterów'),
(7480, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Części do sterów / Łożyska sterów'),
(7481, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Produkty do pielęgnacji koni / Przysmaki dla koni'),
(7482, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Produkty do pielęgnacji koni / Karma dla koni'),
(7484, 'Sprzęt / Narzędzia / Narzędzia murarskie / Gąbki do fugowania'),
(7485, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Porcjowniki mięsa suszonego „jerky”'),
(7486, 'Żywność, napoje i tytoń / Napoje / Napoje alkoholowe / Bittery'),
(7490, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do paznokci / Wiertła do paznokci'),
(7493, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do makijażu / Akcesoria do sztucznych rzęs / Aplikatory do sztucznych rzęs'),
(7494, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do paznokci / Separatory palców do pedicure'),
(7495, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja stóp / Rozdzielacze do palców'),
(7496, 'Dom i ogród / Baseny i spa / Akcesoria do basenów i spa / Obrzeża basenów'),
(7497, 'Biznes i przemysł / Stomatologia'),
(7498, 'Biznes i przemysł / Stomatologia / Narzędzia dentystyczne / Lusterka dentystyczne'),
(7499, 'Biznes i przemysł / Stomatologia / Narzędzia dentystyczne'),
(7500, 'Biznes i przemysł / Stomatologia / Cement dentystyczny'),
(7501, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Części do laptopów / Kable do laptopów'),
(7502, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły do pralni / Środki przeciw zagnieceniom i elektryzowaniu się'),
(7503, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Chemikalia / Środki czyszczące do tarasów i płotów'),
(7504, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Chemikalia / Środki czyszczące do betonu i kamienia'),
(7506, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Zestawy z ciasteczkami do dekoracji'),
(7509, 'Dom i ogród / Akcesoria łazienkowe / Końcówki szczotek toaletowych'),
(7510, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Środki czyszczące do gospodarstwa domowego / Środki do czyszczenia zmywarek'),
(7514, 'Dom i ogród / Artykuły gospodarstwa domowego / Przechowywanie i utrzymanie porządku / Przechowywanie ubrań / Pudełka na kapelusze'),
(7515, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do pił / Akcesoria do pił taśmowych'),
(7516, 'Media / Instrukcje obsługi / Instrukcje obsługi sprzętu elektrycznego'),
(7517, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla małych zwierząt / Przekąski dla małych zwierząt'),
(7518, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Odzież ochronna myśliwska i strzelecka / Kurtki myśliwskie i strzeleckie'),
(7519, 'Gry i zabawki / Zabawki / Zabawki ruchowe / Zabawki zręcznościowe typu kendama'),
(7521, 'Torby, walizki i akcesoria podróżne / Akcesoria podróżne / Ochraniacze na plecaki'),
(7522, 'Biznes i przemysł / Branża medyczna / Sprzęt medyczny / Podnośniki pacjenta'),
(7523, 'Dom i ogród / Akcesoria do kominków i pieców opalanych węglem / Ruszty kominkowe'),
(7525, 'Artykuły biurowe / Sprzęt do prezentacji / Tablice do prezentacji / Akcesoria do tablic biurowych'),
(7526, 'Artykuły biurowe / Sprzęt do prezentacji / Tablice do prezentacji / Akcesoria do tablic biurowych / Obramowania do tablic biurowych'),
(7527, 'Dom i ogród / Sprzęt AGD / Ogrzewanie, wentylacja i klimatyzacja / Wentylatory / Wentylatory ręczne'),
(7528, 'Żywność, napoje i tytoń / Napoje / Napoje na bazie octu'),
(7530, 'Elektronika / Akcesoria elektroniczne / Akcesoria komputerowe / Pokrowce i futerały na komputery'),
(7531, 'Biznes i przemysł / Stomatologia / Narzędzia dentystyczne / Zestawy narzędzi dentystycznych'),
(7532, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Elementy ozdobne do pojazdów / Magnesy na karoserię'),
(7537, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Narzędzia ogrodnicze / Narzędzia do sadzenia cebulek'),
(7538, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Części kół rowerowych / Pegi rowerowe'),
(7539, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Sanki'),
(7540, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do grilla na zewnątrz / Kominki grillowe na węgiel drzewny'),
(7542, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja uszu / Dozowniki stoperów'),
(7550, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Naczynia do serwowania żywności / Kieliszki do jajek'),
(7551, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Akumulatory / Baterie do czytników e-booków'),
(7552, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Środki czyszczące do gospodarstwa domowego / Środki nabłyszczające'),
(7553, 'Biznes i przemysł / Usługi gastronomiczne / Patelnie przechylne'),
(7555, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty perkusyjne / Ręczne instrumenty perkusyjne / Dzwonki ręczne i dzwony rurowe'),
(7556, 'Sprzęt / Narzędzia / Smarownice'),
(7557, 'Sprzęt / Akcesoria do sprzętu / Taśmy ostrzegawcze i sygnalizacyjne'),
(7558, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Jazda na nartach i snowboardzie / Stojaki na narty i deski snowboardowe'),
(7559, 'Meble / Akcesoria do mebli biurowych / Akcesoria do krzeseł i foteli biurowych'),
(7561, 'Dom i ogród / Trawnik i ogród / Nawadnianie / Zraszacze i końcówki do zraszaczy'),
(7562, 'Sprzęt / Narzędzia / Przecinaki / Obcęgi'),
(7563, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Akcesoria do obcinarek krawędzi trawnika'),
(7564, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Przystawki do wielofunkcyjnych narzędzi ogrodowych / Nasadki do obcinarek krawędzi trawnika'),
(7565, 'Biznes i przemysł / Finanse i ubezpieczenia / Sztabki metali szlachetnych'),
(7566, 'Elektronika / Audio / Akcesoria audio / Akcesoria do odtwarzaczy MP3 / Zestawy akcesoriów do odtwarzaczy mp3 i telefonów komórkowych'),
(7567, 'Artykuły dla dorosłych / Broń / Pałki'),
(7568, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Naczynia do napojów / Kufle piwne'),
(7569, 'Dom i ogród / Kuchnia i jadalnia / Naczynia barowe / Kamienie do whisky'),
(7570, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do zamrażarek'),
(7789, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na deskorolce / Stroje ochronne dla deskorolkarzy / Rękawice na deskorolkę'),
(8005, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Paintball i ASG / ASG / Akcesoria do ASG / Akumulatory do ASG'),
(8006, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Formy kuchenne'),
(8007, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Szablony i formy / Formy rękodzielnicze'),
(8008, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Akcesoria do urządzeń wejściowych / Akcesoria do kontrolerów gier'),
(8011, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Myślistwo / Wabiki na zwierzynę'),
(8012, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Organizery kuchenne / Pojemniki na sztućce'),
(8015, 'Artykuły biurowe / Artykuły różne / Folie i koszulki do laminowania'),
(8016, 'Dom i ogród / Akcesoria łazienkowe / Akcesoria do suszarek do rąk'),
(8017, 'Ubrania i akcesoria / Przebrania i akcesoria / Akcesoria do przebrań / Zestawy dodatków do przebrań'),
(8018, 'Ubrania i akcesoria / Przebrania i akcesoria / Akcesoria do przebrań / Biżuteria zabawkowa'),
(8020, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Akcesoria do parasoli i pawilonów ogrodowych / Oświetlenie do parasoli ogrodowych'),
(8021, 'Gry i zabawki / Zabawki / Lalki, zestawy i figurki / Lalki papierowe i magnetyczne'),
(8022, 'Oprogramowanie / Towary i waluty cyfrowe / Szablony dokumentów'),
(8023, 'Meble / Akcesoria do półek i regałów'),
(8024, 'Meble / Akcesoria do półek i regałów / Półki zapasowe'),
(8025, 'Biznes i przemysł / Akcesoria magazynowe'),
(8026, 'Biznes i przemysł / Branża medyczna / Instrumenty medyczne / Skalpele'),
(8029, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja skóry / Skompresowane maseczki kosmetyczne'),
(8030, 'Żywność, napoje i tytoń / Napoje / Ajerkoniaki'),
(8031, 'Dom i ogród / Artykuły gospodarstwa domowego / Środki i narzędzia do pielęgnacji obuwia / Skrobaczki do butów'),
(8032, 'Dom i ogród / Artykuły gospodarstwa domowego / Środki i narzędzia do pielęgnacji obuwia / Polerki do butów'),
(8033, 'Dom i ogród / Artykuły gospodarstwa domowego / Środki i narzędzia do pielęgnacji obuwia / Zestawy do pielęgnacji obuwia'),
(8034, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Akcesoria do nożyc do chwastów / Osłony na żyłkę'),
(8036, 'Żywność, napoje i tytoń / Napoje / Napoje o smaku owocowym'),
(8038, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Programy wydarzeń'),
(8039, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do przechowywania żywności / Podajniki do folii spożywczej'),
(8042, 'Dom i ogród / Ozdoby / Akcesoria do okien / Części do żaluzji i rolet'),
(8043, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Środki czyszczące do gospodarstwa domowego / Środki do czyszczenia tkanin i obić'),
(8044, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Golf / Narzędzia do naprawy darni'),
(8046, 'Dom i ogród / Kuchnia i jadalnia / Zastawy stołowe / Klipsy i obciążniki do obrusów'),
(8047, 'Elektronika / Audio / Akcesoria audio / Akcesoria do głośników / Torby na stojaki do głośników'),
(8049, 'Elektronika / Audio / Akcesoria audio / Akcesoria do głośników / Stojaki do głośników'),
(8050, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Krople do oczu i lubrykanty dla zwierząt'),
(8051, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do bemarów'),
(8052, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do bemarów / Pokrywki do bemarów'),
(8053, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do bemarów / Pojemniki do bemarów'),
(8058, 'Dom i ogród / Artykuły gospodarstwa domowego / Przechowywanie i utrzymanie porządku / Wkłady do szuflad'),
(8059, 'Biznes i przemysł / Usługi gastronomiczne / Pokrywki jednorazowe'),
(8061, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Odzież i akcesoria dla rowerzystów / Ochraniacze rowerowe'),
(8062, 'Sprzęt sportowy / Fitness / Spadochrony oporowe'),
(8064, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Sygnalizatory brań'),
(8065, 'Sprzęt sportowy / Gry towarzyskie / Bilard / Części i akcesoria do stołów bilardowych / Pokrowce na stoły bilardowe'),
(8066, 'Sprzęt sportowy / Fitness / Treningowe urządzenia wibracyjne'),
(8067, 'Sprzęt / Zamki i klucze / Nasadki na klucze'),
(8068, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Zestawy pierwszej pomocy dla zwierząt'),
(8069, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla kotów / Worki do kuwet'),
(8070, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Torebki na odchody zwierząt'),
(8071, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Ręczniki jednorazowe'),
(8072, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do wzmacniaczy do instrumentów muzycznych'),
(8073, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do wzmacniaczy do instrumentów muzycznych / Przełączniki nożne do wzmacniaczy do instrumentów muzycznych'),
(8074, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Jazda na nartach i snowboardzie / Smary do nart i desek snowboardowych'),
(8075, 'Dzieci i niemowlęta / Karmienie / Poszewki na poduszki do karmienia'),
(8076, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Kasze'),
(8077, 'Sprzęt sportowy / Sport / Sprzęt ogólnosportowy / Siatki i ekrany ochronne'),
(8078, 'Artykuły biurowe / Podkładki na biurko'),
(8079, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Parawany'),
(8080, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Myślistwo / Wabiki na zwierzynę / Wabiki i kamuflaże zapachowe'),
(8081, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Myślistwo / Wabiki na zwierzynę / Przynęta, karma i odżywki dla dzikiej zwierzyny'),
(8082, 'Zdrowie i uroda / Opieka zdrowotna / Pojemniki na próbki do badań laboratoryjnych'),
(8083, 'Sprzęt sportowy / Fitness / Podnoszenie ciężarów / Akcesoria do wolnych ciężarów / Stojaki na ciężary'),
(8085, 'Elektronika / Sprzęt do gier zręcznościowych / Automaty do koszykówki'),
(8086, 'Pojazdy i części / Akcesoria i części do pojazdów / Przewóz i przechowywanie bagażu / Akcesoria do bagażników bazowych / Akcesoria do bagażników na sprzęt narciarski i snowboardowy'),
(8087, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do okapów kuchennych'),
(8090, 'Dom i ogród / Sprzęt AGD / Ogrzewanie, wentylacja i klimatyzacja / Wentylatory / Wentylatory ścienne'),
(8092, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Akcesoria do kołowrotków wędkarskich'),
(8093, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Akcesoria do wędek'),
(8094, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Akcesoria do kołowrotków wędkarskich / Smary do kołowrotków wędkarskich'),
(8105, 'Zdrowie i uroda / Opieka zdrowotna / Opaski uciskowe na kikuty'),
(8106, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do chłodziarek do wina'),
(8107, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Akcesoria do uprzęży jeździeckich / Osprzęt do siodeł'),
(8108, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Akcesoria do uprzęży jeździeckich / Osprzęt do siodeł / Podkładki pod siodło'),
(8109, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Rząd koński / Strzemiona'),
(8110, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Nadmuchiwane dekoracje okolicznościowe'),
(8111, 'Elektronika / Telekomunikacja / Telefonia / Akcesoria do telefonów komórkowych / Akcesoria fotograficzne do telefonów komórkowych'),
(8112, 'Sprzęt / Akcesoria do sprzętu / Śledzie i kotwy'),
(8113, 'Sprzęt / Akcesoria do sprzętu / Podstawy słupów'),
(8114, 'Dom i ogród / Akcesoria łazienkowe / Uchwyty łazienkowe'),
(8115, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Akcesoria do baterii / Perlatory do kranów'),
(8116, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Akcesoria do baterii / Uchwyty do baterii'),
(8117, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do nożyc'),
(8118, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do nożyc / Noże do wycinarek'),
(8119, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Preparaty laboratoryjne'),
(8120, 'Biznes i przemysł / Stomatologia / Narzędzia dentystyczne / Głowice do polerowania zębów'),
(8121, 'Biznes i przemysł / Stomatologia / Narzędzia dentystyczne / Gumki do polerowania zębów'),
(8122, 'Zdrowie i uroda / Higiena osobista / Środki do higieny intymnej dla kobiet / Kubeczki menstruacyjne'),
(8123, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla psów / Bieżnie dla psów'),
(8127, 'Gry i zabawki / Zabawki / Akcesoria do zabawek sportowych'),
(8128, 'Gry i zabawki / Zabawki / Akcesoria do zabawek sportowych / Akcesoria do zabawek fitness / Akcesoria do hula-hoop'),
(8129, 'Gry i zabawki / Zabawki / Akcesoria do zabawek sportowych / Akcesoria do zabawek fitness'),
(8130, 'Biznes i przemysł / Stomatologia / Pasty do polerowania zębów'),
(8132, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do pielęgnacji twarzy / Głowice do szczotek do oczyszczania twarzy'),
(8134, 'Elektronika / Elektronika morska / Morskie odbiorniki obrazu i dźwięku'),
(8135, 'Zdrowie i uroda / Higiena osobista / Masaż i relaks / Kamienie do masażu'),
(8136, 'Zdrowie i uroda / Higiena osobista / Golenie i strzyżenie / Usuwanie włosów / Podgrzewacze do wosku do depilacji'),
(8137, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Silniki'),
(8142, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Elementy ozdobne do pojazdów / Gałki zmiany biegów'),
(8144, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Lakiery samochodowe / Lakiery na zaciski hamulcowe'),
(8145, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Elementy ozdobne do pojazdów / Nakładki na haki samochodowe'),
(8147, 'Pojazdy i części / Akcesoria i części do pojazdów / Przewóz i przechowywanie bagażu / Rampy do załadunku'),
(8149, 'Ubrania i akcesoria / Ubrania / Stroje tradycyjne i ceremonialne / Stroje na ceremonie religijne / Sukienki i komplety do chrztu i komunii'),
(8155, 'Biznes i przemysł / Informacja wizualna / Monitory informacyjne'),
(8156, 'Elektronika / Akcesoria elektroniczne / Rozdzielacze i przełączniki do przesyłania obrazu i dźwięku'),
(8158, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Śledzie do kart graficznych'),
(8159, 'Elektronika / Audio / Megafony'),
(8160, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Części do laptopów / Digitizery do laptopów'),
(8161, 'Dom i ogród / Kuchnia i jadalnia / Gotowe kuchnie i aneksy kuchenne'),
(8162, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Części do czytników e-booków'),
(8163, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Części do czytników e-booków / Zapasowe ekrany do czytników e-booków'),
(8164, 'Elektronika / Akcesoria elektroniczne / Rozdzielacze i przełączniki do przesyłania obrazu i dźwięku / Rozdzielacze i przełączniki do HDMI'),
(8167, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły groomerskie dla zwierząt / Suszarki do sierści'),
(8168, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Rękodzieło z papieru / Folie ozdobne'),
(8172, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Stojaki na jednostki pływające / Stojaki na łodzie'),
(8173, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Stojaki na jednostki pływające / Stojaki na deski do pływania'),
(8174, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria i części do aparatów / Zestawy akcesoriów do aparatów'),
(8200, 'Ubrania i akcesoria / Przebrania i akcesoria / Akcesoria do przebrań / Rękawiczki do przebrań'),
(8202, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Elementy ozdobne do pojazdów / Okleiny karoserii'),
(8203, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Jazda na nartach i snowboardzie / Akcesoria do gogli narciarskich i snowboardowych'),
(8204, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja oczu / Akcesoria do okularów / Sznurki i łańcuszki do okularów'),
(8206, 'Meble / Akcesoria do krzeseł / Części zapasowe do krzeseł wiszących'),
(8208, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Akcesoria do kołowrotków wędkarskich / Szpule zapasowe do kołowrotków wędkarskich'),
(8209, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych / Akcesoria do instrumentów strunowych dla orkiestr / Futerały na smyczki'),
(8210, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Sportowe artykuły kolekcjonerskie / Pamiątki sportowe z autografem / Pamiątki bokserskie z autografem'),
(8213, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Wręczanie prezentów / Pakowanie prezentów / Bileciki i etykiety do prezentów'),
(8214, 'Zdrowie i uroda / Higiena osobista / Golenie i strzyżenie / Rozjaśniacze owłosienia twarzy i ciała'),
(8215, 'Sprzęt sportowy / Fitness / Uchwyty do pompek i drążki do podciągania'),
(8216, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Pompony ozdobne'),
(8217, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż / Makijaż ust / Bazy pod szminkę'),
(8218, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż / Makijaż twarzy / Bazy pod makijaż'),
(8219, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż / Makijaż oka / Bazy pod tusz do rzęs'),
(8220, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Makijaż / Makijaż oka / Bazy pod cienie do powiek'),
(8222, 'Sprzęt sportowy / Sport / Sprzęt ogólnosportowy / Maski do treningu wysokościowego'),
(8227, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Karoserie i ramy do pojazdów silnikowych'),
(8228, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Sprzęt holowniczy'),
(8231, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Układy elektryczne i zapłonowe'),
(8232, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Wykładziny i tapicerka'),
(8233, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Wyposażenie wnętrza'),
(8234, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Wskaźniki i czujniki'),
(8235, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Elementy układu kierowniczego'),
(8236, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Narzędzia do naprawy pojazdów'),
(8237, 'Pojazdy i części / Akcesoria i części do pojazdów / Przewóz i przechowywanie bagażu'),
(8238, 'Pojazdy i części / Akcesoria i części do pojazdów / Części do pojazdów silnikowych / Fotele i siedzenia'),
(8239, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Podpórki rowerowe'),
(8243, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Akcesoria do akumulatorów / Gniazda baterii'),
(8248, 'Ubrania i akcesoria / Ubrania / Stroje tradycyjne i ceremonialne / Sari i lehenga'),
(8258, 'Sprzęt / Akcesoria do narzędzi / Smycze na narzędzia'),
(8259, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Narzędzia do naprawy pojazdów / Narzędzia do wymiany i regulacji docisku sprzęgła'),
(8260, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Narzędzia do naprawy pojazdów / Zestawy do naprawy hamulców'),
(8261, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Narzędzia do naprawy pojazdów / Narzędzia do naprawy opon'),
(8269, 'Biznes i przemysł / Środki ochrony osobistej / Akcesoria do masek gazowych'),
(8270, 'Sprzęt / Materiały budowlane / Pokrycia dachowe / Płytki dachowe i dachówki'),
(8271, 'Elektronika / Audio / Odtwarzacze i nagrywarki audio / Odtwarzacze i magnetofony szpulowe'),
(8272, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Akcesoria do wędek / Torby i pokrowce na wędki'),
(8273, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Akcesoria do kołowrotków wędkarskich / Torby i pokrowce na kołowrotki wędkarskie'),
(8274, 'Biznes i przemysł / Magazynowanie / Przegrody, klatki i drzwi z siatki'),
(8275, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do wiercenia / Stojaki i prowadnice do wiertarek'),
(8276, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do wiercenia / Głowice do wiertarek'),
(8277, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Części do prysznica / Baterie prysznicowe elektryczne i wysokociśnieniowe'),
(8278, 'Biznes i przemysł / Budownictwo / Słupki i barierki drogowe'),
(8295, 'Gry i zabawki / Zabawki / Zabawa przez imitację / Zabawa w zakupy spożywcze'),
(8298, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Elementy ozdobne do pojazdów / Mocowania do tablic rejestracyjnych'),
(8301, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów'),
(8302, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Alarmy i zamki samochodowe / Zamki do drzwi samochodowych i ich elementy'),
(8303, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Alarmy i zamki samochodowe / Zamki do drzwi samochodowych i ich elementy / Zamki i systemy ryglowania drzwi samochodowych'),
(8304, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Alarmy i zamki samochodowe / Zamki do drzwi samochodowych i ich elementy / Rygle do drzwi samochodowych'),
(8305, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Alarmy i zamki samochodowe / Zamki do drzwi samochodowych i ich elementy / Blokady drzwi do pojazdów'),
(8306, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Osłony i pokrowce na pojazdy / Osłony do wózków golfowych'),
(8308, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Osłony i pokrowce na pojazdy / Plandeki na skrzynie ładunkowe'),
(8309, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Osłony i pokrowce na pojazdy / Pokrowce na pojazdy'),
(8310, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Osłony i pokrowce na pojazdy / Pokrowce na pojazdy / Pokrowce na samochody'),
(8311, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Osłony i pokrowce na pojazdy / Pokrowce na pojazdy / Pokrowce na pojazdy kempingowe'),
(8312, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Osłony i pokrowce na pojazdy / Pokrowce na pojazdy / Pokrowce na jednostki pływające');
INSERT INTO `google_kategorie` (`id`, `nazwa`) VALUES
(8313, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Osłony i pokrowce na pojazdy / Pokrowce na pojazdy / Pokrowce na motocykle'),
(8314, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Osłony i pokrowce na pojazdy / Pokrowce na pojazdy / Pokrowce na wózki golfowe'),
(8316, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Osłony i pokrowce na pojazdy / Osłony na przednią szybę'),
(8319, 'Sprzęt sportowy / Sport / Sprzęt ogólnosportowy / Słupki do ćwiczenia zwinności'),
(8320, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Części do prysznica / Dysze do wanien i pryszniców'),
(8378, 'Pojazdy i części / Akcesoria i części do pojazdów / Przewóz i przechowywanie bagażu / Pojemniki i sortowniki do skrzyń ładunkowych'),
(8436, 'Dzieci i niemowlęta / Karmienie / Jedzenie dla dzieci i niemowląt / Napoje i szejki dla niemowląt'),
(8445, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Samochodowe elementy bezpieczeństwa / Flary ostrzegawcze'),
(8446, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Samochodowe elementy bezpieczeństwa / Klatki bezpieczeństwa do pojazdów'),
(8447, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Samochodowe elementy bezpieczeństwa / Siatki na okna w samochodzie'),
(8448, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Samochodowe elementy bezpieczeństwa / Części do poduszek powietrznych'),
(8449, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Samochodowe elementy bezpieczeństwa / Pasy bezpieczeństwa'),
(8450, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Lakiery samochodowe / Lakiery do karoserii'),
(8451, 'Ubrania i akcesoria / Akcesoria do ubrań / Akcesoria do włosów / Grzebyki i gumki do włosów'),
(8452, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja włosów / Zestawy do pielęgnacji włosów'),
(8461, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do wzmacniaczy do instrumentów muzycznych / Pokrowce na wzmacniacze do instrumentów muzycznych'),
(8462, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do wzmacniaczy do instrumentów muzycznych / Pokrętła do wzmacniaczy do instrumentów muzycznych'),
(8463, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Elementy ozdobne do pojazdów / Mieszki drążka zmiany biegów'),
(8464, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Elementy ozdobne do pojazdów / Pokrowce na kierownicę'),
(8469, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Elementy ozdobne do pojazdów / Zestawy akcesoriów dekoracyjnych do pojazdów'),
(8470, 'Sprzęt / Akcesoria do sprzętu / Prowadnice do szuflad'),
(8471, 'Sprzęt sportowy / Fitness / Kliny do ćwiczeń'),
(8472, 'Gry i zabawki / Gry / Automaty wrzutowe'),
(8473, 'Elektronika / Elektronika morska / Głośniki morskie'),
(8474, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Przegrody do przewożenia zwierząt'),
(8475, 'Pojazdy i części / Akcesoria i części do pojazdów / Przewóz i przechowywanie bagażu / Wieszaki na zagłówek'),
(8476, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Samochodowe elementy bezpieczeństwa / Taśmy do pasów bezpieczeństwa'),
(8477, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Samochodowe elementy bezpieczeństwa / Zapięcia pasów bezpieczeństwa'),
(8478, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Elementy ozdobne do pojazdów / Ozdoby na lusterka wsteczne'),
(8480, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do wzmacniaczy do instrumentów muzycznych / Wzmacniacze basowe do instrumentów muzycznych'),
(8483, 'Pojazdy i części / Akcesoria i części do pojazdów / Wyposażenie elektroniczne pojazdów silnikowych / Mocowania do samochodowych monitorów wideo'),
(8485, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Przystawki do wielofunkcyjnych narzędzi ogrodowych'),
(8487, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Przystawki do wielofunkcyjnych narzędzi ogrodowych / Przystawki do dmuchaw do liści'),
(8488, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Przystawki do wielofunkcyjnych narzędzi ogrodowych / Przystawki do glebogryzarek i kultywatorów'),
(8489, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Przystawki do wielofunkcyjnych narzędzi ogrodowych / Przystawki do pił na wysięgniku'),
(8490, 'Biznes i przemysł / Stomatologia / Narzędzia dentystyczne / Małe naczynia do mieszania'),
(8493, 'Dom i ogród / Kuchnia i jadalnia / Naczynia barowe / Stojaki na wino'),
(8499, 'Artykuły biurowe / Przenośne biurka'),
(8505, 'Sprzęt sportowy / Sport / Akcesoria sędziowskie / Tablice wyników'),
(8506, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Samochodowe elementy bezpieczeństwa / Blokady pod koła'),
(8507, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Ochraniacze motocyklowe / Osłony rąk do motocykli'),
(8513, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Stojaki na miski dla zwierząt'),
(8514, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Paintball i ASG / Paintball / Akcesoria do markerów paintballowych / Obniżki do markerów paintballowych'),
(8515, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Jamy'),
(8516, 'Elektronika / Płyty drukowane i komponenty / Obwody drukowane / Obwody drukowane do telewizorów'),
(8522, 'Dom i ogród / Artykuły gospodarstwa domowego / Maty garażowe'),
(8526, 'Pojazdy i części / Akcesoria i części do pojazdów / Wyposażenie elektroniczne pojazdów silnikowych'),
(8528, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Części kół rowerowych / Osie i szprychy do rowerów'),
(8529, 'Gry i zabawki / Zabawki / Zabawki sportowe / Zabawkowe zestawy wędkarskie'),
(8530, 'Zdrowie i uroda / Higiena osobista / Masaż i relaks / Podgrzewacze do kamieni do masażu'),
(8531, 'Zdrowie i uroda / Higiena osobista / Golenie i strzyżenie / Miseczki i kubki do golenia'),
(8532, 'Biznes i przemysł / Usługi gastronomiczne / Okładki na rachunek'),
(8533, 'Biznes i przemysł / Usługi gastronomiczne / Klosze do przykrywania potraw'),
(8534, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Kanistry na paliwo'),
(8535, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria i części do aparatów / Akcesoria do obudów do zdjęć podwodnych'),
(8537, 'Dzieci i niemowlęta / Akcesoria do transportu niemowląt / Materacyki i śpiwory do wózków i fotelików'),
(8539, 'Elektronika / Komputery / Komputery typu cienki klient i zerowy klient'),
(8541, 'Zdrowie i uroda / Opieka zdrowotna / Sprzęt do fizjoterapii / Elektryczne stymulatory mięśni'),
(8543, 'Zdrowie i uroda / Higiena osobista / Higiena jamy ustnej / Zestawy do naprawy protez'),
(8544, 'Elektronika / Płyty drukowane i komponenty / Obwody drukowane / Obwody drukowane do drukarek, kopiarek i faksów'),
(8545, 'Elektronika / Płyty drukowane i komponenty / Obwody drukowane / Obwody drukowane do domowych urządzeń elektrycznych'),
(8546, 'Elektronika / Płyty drukowane i komponenty / Obwody drukowane / Obwody drukowane do komputerów / Obwody drukowane do dysków twardych'),
(8549, 'Elektronika / Płyty drukowane i komponenty / Obwody drukowane / Obwody drukowane do basenów i spa'),
(43616, 'Elektronika / Wideo / Akcesoria do wideo / Akcesoria i części do telewizorów / Wymienne głośniki do telewizorów'),
(43617, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Części do laptopów / Zapasowe głośniki do laptopów'),
(45262, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Części do tabletów / Zapasowe głośniki do tabletów'),
(99338, 'Sprzęt / Materiały budowlane / Wyposażenie do drzwi / Szyldy do zamków'),
(230911, 'Dom i ogród / Ozdoby / Akcesoria do domków dla ptaków'),
(230912, 'Dom i ogród / Trawnik i ogród / Nawadnianie / Kroplowniki'),
(230913, 'Biznes i przemysł / Branża medyczna / Instrumenty medyczne'),
(232166, 'Biznes i przemysł / Branża medyczna / Instrumenty medyczne / Ostrza do skalpeli'),
(232167, 'Sprzęt / Akcesoria do sprzętu / Wyposażenie do szaf / Szyldy do zamków w szafkach'),
(232168, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Akcesoria do narzędzi dla artystów i rękodzielników / Noże do papieru i ostrza'),
(233419, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Pielęgnacja paznokci / Krople i spraye przyspieszające schnięcie lakieru do paznokci'),
(233420, 'Zdrowie i uroda / Higiena osobista / Masaż i relaks / Poduszki na oczy'),
(234670, 'Sprzęt sportowy / Sport / Baseball i softball / Podwyższenie dla miotacza w baseballu i softballu'),
(234671, 'Sprzęt sportowy / Sport / Baseball i softball / Maty do rzutów w baseballu i softballu'),
(235920, 'Dom i ogród / Sprzęt AGD / Suszarki do podłóg i dywanów'),
(235921, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Alarmy i zamki samochodowe / Zamki do haków samochodowych'),
(237166, 'Sprzęt sportowy / Fitness / Podesty do aerobiku'),
(296246, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria i części do aparatów / Części zapasowe do korpusów aparatów'),
(296247, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria i części do aparatów / Przyciski i pokrętła do aparatów i kamer'),
(296248, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria i części do aparatów / Sensory obrazu do aparatów i kamer'),
(296249, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria i części do aparatów / Ekrany i wyświetlacze do aparatów'),
(298419, 'Elektronika / Płyty drukowane i komponenty / Obwody drukowane / Obwody drukowane do aparatów i kamer'),
(298420, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria i części do aparatów / Bloki tylne'),
(326120, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Samochodowe elementy bezpieczeństwa / Osłony pasów bezpieczeństwa'),
(326122, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Akcesoria do uprzęży jeździeckich / Osprzęt do siodeł / Torby i sakwy do siodeł'),
(328060, 'Artykuły religijne i dewocjonalia / Artykuły religijne / Obrazki do modlitwy'),
(328061, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Kartki okolicznościowe z sentencją'),
(328062, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do narzędzi malarskich / Produkty do czyszczenia pędzli'),
(352853, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja oczu / Akcesoria do okularów / Płyny do czyszczenia okularów'),
(355576, 'Sprzęt sportowy / Fitness / Ławki i systemy inwersyjne'),
(362737, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Sprzęt ochronny do jazdy pojazdami terenowymi'),
(362738, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Sprzęt ochronny do jazdy pojazdami terenowymi / Osłony poprzeczki kierownicy do quadów i pojazdów użytkowych'),
(461567, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria i części do aparatów / Trybiki do aparatów i kamer'),
(461568, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria i części do aparatów / Zoomy do obiektywów'),
(463625, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Zapasowe kable do aparatów, kamer i przyrządów optycznych'),
(465846, 'Meble / Szafy i przechowywanie / Szafki do prasowania'),
(499673, 'Dom i ogród / Bezpieczeństwo powodziowe, ogniowe i gazowe / Czujniki dymu i dwutlenku węgla'),
(499674, 'Dom i ogród / Artykuły gospodarstwa domowego / Tymczasowe zabezpieczenia podłóg'),
(499675, 'Elektronika / Płyty drukowane i komponenty / Obwody drukowane / Obwody drukowane do skanerów'),
(499676, 'Żywność, napoje i tytoń / Napoje / Napoje alkoholowe'),
(499680, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Akcesoria do namiotów / Sypialnie do namiotów'),
(499681, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Jazda na nartach i snowboardzie / Smycze narciarskie i snowboardowe'),
(499682, 'Elektronika / Drukowanie, kopiowanie, skanowanie i faksowanie / Akcesoria do drukarek 3D'),
(499684, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Osłony linek rowerowych'),
(499685, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Linki rowerowe'),
(499686, 'Elektronika / Akcesoria elektroniczne / Zarządzanie kablami / Markery do kabli i przewodów'),
(499687, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Odzież żeglarska i do uprawiania sportów wodnych / Kaski do uprawiania sportów wodnych'),
(499688, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych / Akcesoria do gitar / Paski do gitar'),
(499691, 'Torby, walizki i akcesoria podróżne / Akcesoria podróżne / Stojaki i podstawki na bagaż'),
(499692, 'Zdrowie i uroda / Opieka zdrowotna / Środki ochrony układu oddechowego / Butle tlenowe'),
(499693, 'Dom i ogród / Ozdoby / Miarki wzrostu'),
(499694, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Łaty i zestawy naprawcze do opon rowerowych'),
(499696, 'Biznes i przemysł / Branża medyczna / Medyczne materiały eksploatacyjne / Igły i strzykawki medyczne'),
(499697, 'Sprzęt / Hydraulika / Mocowania hydrauliczne / Stelaże i uchwyty do montowania w ścianie'),
(499698, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do paznokci / Akcesoria do frezarek do paznokci'),
(499699, 'Sprzęt sportowy / Fitness / Trening kardio / Akcesoria do sprzętu kardio / Akcesoria do bieżni elektrycznych'),
(499700, 'Sprzęt sportowy / Fitness / Trening kardio / Akcesoria do sprzętu kardio / Akcesoria do schodów treningowych i stepperów'),
(499701, 'Sprzęt sportowy / Fitness / Trening kardio / Akcesoria do sprzętu kardio / Akcesoria do ergowioseł'),
(499702, 'Sprzęt sportowy / Fitness / Trening kardio / Akcesoria do sprzętu kardio / Akcesoria do rowerów stacjonarnych'),
(499703, 'Sprzęt sportowy / Fitness / Trening kardio / Akcesoria do sprzętu kardio / Akcesoria do urządzeń do treningu eliptycznego'),
(499707, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Granulaty tapioki'),
(499708, 'Biznes i przemysł / Środki ochrony osobistej / Siatki bezpieczeństwa'),
(499709, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Rząd koński / Uwiązy dla koni'),
(499710, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Rząd koński / Kantary dla koni'),
(499711, 'Artykuły religijne i dewocjonalia / Artykuły religijne / Karty do tarota'),
(499712, 'Gry i zabawki / Zabawki / Kosze prezentowe z zabawkami'),
(499713, 'Sprzęt sportowy / Sport'),
(499715, 'Sprzęt sportowy / Sport / Baseball i softball / Odzież ochronna do baseballa i softballa / Ochraniacze tułowia do baseballa i softballa'),
(499716, 'Sprzęt sportowy / Sport / Baseball i softball / Odzież ochronna do baseballa i softballa / Zestawy akcesoriów dla łapacza'),
(499717, 'Sprzęt sportowy / Sport / Baseball i softball / Odzież ochronna do baseballa i softballa / Kaski i maski dla łapacza'),
(499718, 'Sprzęt sportowy / Sport / Baseball i softball / Odzież ochronna do baseballa i softballa / Ochraniacze nóg do baseballa i softballa'),
(499719, 'Sprzęt sportowy / Sport / Boks i sztuki walki'),
(499720, 'Sprzęt sportowy / Sport / Boks i sztuki walki / Trening bokserski i sztuk walki'),
(499721, 'Sprzęt sportowy / Sport / Boks i sztuki walki / Trening bokserski i sztuk walki / Tarcze treningowe'),
(499722, 'Sprzęt sportowy / Sport / Boks i sztuki walki / Ochraniacze do boksu i sztuk walki / Kaski bokserskie i do sztuk walki'),
(499723, 'Sprzęt sportowy / Sport / Boks i sztuki walki / Ochraniacze do boksu i sztuk walki / Ochraniacze tułowia do boksu i sztuk walki'),
(499724, 'Sprzęt sportowy / Sport / Boks i sztuki walki / Ochraniacze do boksu i sztuk walki / Ochraniacze na goleń do MMA'),
(499725, 'Sprzęt sportowy / Sport / Boks i sztuki walki / Ochraniacze do boksu i sztuk walki / Ochraniacze na ręce do boksu i sztuk walki'),
(499726, 'Sprzęt sportowy / Sport / Boks i sztuki walki / Ochraniacze do boksu i sztuk walki / Bandaże bokserskie i do MMA'),
(499727, 'Sprzęt sportowy / Sport / Akcesoria sędziowskie / Etui na kartki sędziowskie'),
(499729, 'Sprzęt sportowy / Sport / Akcesoria sędziowskie / Opaski dla kapitanów drużyn'),
(499730, 'Sprzęt sportowy / Sport / Akcesoria sędziowskie / Liczniki sędziowskie'),
(499731, 'Sprzęt sportowy / Sport / Akcesoria sędziowskie / Liczniki rzutów miotacza'),
(499732, 'Sprzęt sportowy / Sport / Akcesoria sędziowskie / Żetony sędziowskie'),
(499733, 'Sprzęt sportowy / Sport / Akcesoria sędziowskie / Stanowiska i krzesła sędziowskie'),
(499734, 'Sprzęt sportowy / Sport / Krykiet / Słupki do bramek krykietowych'),
(499735, 'Sprzęt sportowy / Sport / Krykiet / Zestawy do gry w krykieta'),
(499736, 'Sprzęt sportowy / Sport / Krykiet / Ochraniacze do krykieta'),
(499737, 'Sprzęt sportowy / Sport / Krykiet / Akcesoria do kijów krykietowych'),
(499738, 'Sprzęt sportowy / Sport / Krykiet / Akcesoria do kijów krykietowych / Nakładki na rączkę do kijów krykietowych'),
(499739, 'Sprzęt sportowy / Sport / Krykiet / Ochraniacze do krykieta / Krykietowe ochraniacze na nogi'),
(499740, 'Sprzęt sportowy / Sport / Szermierka / Ochraniacze do szermierki / Plastrony i kamizelki elektryczne do szermierki'),
(499741, 'Sprzęt sportowy / Sport / Hokej na trawie i lacrosse'),
(499742, 'Sprzęt sportowy / Sport / Hokej na trawie i lacrosse / Zestawy do lacrosse'),
(499743, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Maty grzewcze dla zwierząt'),
(499744, 'Sprzęt sportowy / Sport / Hokej na trawie i lacrosse / Ochraniacze do hokeja na trawie i lacrosse'),
(499745, 'Sprzęt sportowy / Sport / Hokej na trawie i lacrosse / Ochraniacze do hokeja na trawie i lacrosse / Rękawice do hokeja na trawie i lacrosse'),
(499746, 'Sprzęt sportowy / Sport / Hokej na trawie i lacrosse / Ochraniacze do hokeja na trawie i lacrosse / Kaski do hokeja na trawie i lacrosse'),
(499747, 'Sprzęt sportowy / Sport / Hokej na trawie i lacrosse / Ochraniacze do hokeja na trawie i lacrosse / Maski i gogle do hokeja na trawie i lacrosse'),
(499751, 'Sprzęt sportowy / Sport / Koszykówka / Sprzęt treningowy do koszykówki'),
(499755, 'Sprzęt sportowy / Sport / Łyżwiarstwo figurowe i hokej / Ochraniacze hokejowe / Hokejowe ochraniacze na nogi i golenie'),
(499756, 'Sprzęt sportowy / Sport / Łyżwiarstwo figurowe i hokej / Ochraniacze hokejowe / Hokejowe ochraniacze łokci'),
(499757, 'Sprzęt sportowy / Sport / Łyżwiarstwo figurowe i hokej / Ochraniacze hokejowe / Hokejowe naramienniki i ochraniacze tułowia'),
(499759, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na wrotkach i łyżworolkach / Części do łyżworolek'),
(499760, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na wrotkach i łyżworolkach / Części do wrotek'),
(499761, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na wrotkach i łyżworolkach'),
(499766, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Czyszczenie i mycie pojazdów / Zestawy do czyszczenia wlewu paliwa'),
(499767, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Końcówki do szczotek do szorowania'),
(499768, 'Sprzęt / Materiały elektryczne / Osłony i izolacje kabli'),
(499769, 'Sprzęt sportowy / Sport / Boks i sztuki walki / Trening bokserski i sztuk walki / Rękawice bokserskie i do MMA'),
(499770, 'Sprzęt / Materiały elektryczne / Osłony i izolacje kabli / Osłony do kabli i przewodów elektrycznych'),
(499771, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na wrotkach i łyżworolkach / Sprzęt ochronny do jazdy na wrotkach i łyżworolkach'),
(499772, 'Sprzęt / Materiały budowlane / Osprzęt okienny'),
(499773, 'Sprzęt / Materiały budowlane / Osprzęt okienny / Korbki i dźwignie do okien'),
(499774, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Narzędzia do naprawy pojazdów / Szpachle do karoserii'),
(499775, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na wrotkach i łyżworolkach / Sprzęt ochronny do jazdy na wrotkach i łyżworolkach / Ochraniacze do jazdy na wrotkach i łyżworolkach'),
(499776, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na deskorolce / Stroje ochronne dla deskorolkarzy / Kaski do jazdy na deskorolce'),
(499778, 'Sprzęt sportowy / Sport / Futbol amerykański / Ochraniacze do futbolu amerykańskiego / Ochraniacze żeber do futbolu amerykańskiego'),
(499779, 'Sprzęt sportowy / Sport / Futbol amerykański / Sprzęt treningowy do futbolu amerykańskiego / Tarcze i słupy do futbolu amerykańskiego'),
(499780, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Golf / Części i akcesoria do kijów golfowych / Szafty do kijów golfowych'),
(499781, 'Sprzęt sportowy / Sport / Gimnastyka / Ochraniacze do gimnastyki / Uchwyty treningowe do gimnastyki artystycznej'),
(499782, 'Sprzęt sportowy / Sport / Rugby / Ochraniacze do rugby'),
(499783, 'Sprzęt sportowy / Sport / Racquetball i squash / Rękawice do gry w squasha i raquetballa'),
(499784, 'Sprzęt sportowy / Sport / Piłka nożna / Odzież ochronna do piłki nożnej'),
(499785, 'Sprzęt sportowy / Sport / Piłka ręczna / Piłki do piłki ręcznej'),
(499786, 'Sprzęt sportowy / Sport / Lekkoatletyka / Bloki startowe'),
(499787, 'Sprzęt sportowy / Sport / Siatkówka / Sprzęt do treningu siatkarskiego'),
(499788, 'Sprzęt sportowy / Sport / Siatkówka / Sprzęt ochronny do siatkówki'),
(499789, 'Sprzęt sportowy / Sport / Siatkówka / Sprzęt ochronny do siatkówki / Siatkarskie ochraniacze na kolana'),
(499790, 'Sprzęt sportowy / Sport / Zapasy / Ochraniacze zapaśnicze / Zapaśnicze ochraniacze na kolana'),
(499791, 'Sprzęt sportowy / Sport / Zapasy / Ochraniacze zapaśnicze / Kaski zapaśnicze'),
(499792, 'Sprzęt sportowy / Fitness / Trening kardio'),
(499793, 'Sprzęt sportowy / Fitness / Podnoszenie ciężarów'),
(499794, 'Sprzęt sportowy / Fitness / Podnoszenie ciężarów / Akcesoria do wolnych ciężarów / Mocowania do sztang'),
(499795, 'Sprzęt sportowy / Fitness / Ławki treningowe'),
(499796, 'Sprzęt sportowy / Fitness / Trening równowagi'),
(499797, 'Sprzęt sportowy / Fitness / Kółka i wałki do ćwiczeń'),
(499798, 'Sprzęt sportowy / Fitness / Pasy treningowe'),
(499799, 'Sprzęt sportowy / Sport / Sprzęt ogólnosportowy'),
(499800, 'Sprzęt sportowy / Sport / Sprzęt ogólnosportowy / Puchary sportowe'),
(499801, 'Sprzęt sportowy / Sport / Sprzęt ogólnosportowy / Ochraniacze sportowe na zęby'),
(499802, 'Sprzęt sportowy / Sport / Sprzęt ogólnosportowy / Płotki i drabinki do ćwiczenia zwinności'),
(499803, 'Sprzęt sportowy / Sport / Sprzęt ogólnosportowy / Maty gimnastyczne'),
(499804, 'Dom i ogród / Ozdoby / Ozdoby sezonowe i świąteczne / Zawieszki na ozdoby choinkowe'),
(499805, 'Dom i ogród / Ozdoby / Ozdoby sezonowe i świąteczne / Zestawy do dekoracji pisanek'),
(499810, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Akumulatory / Baterie do odtwarzaczy mp3'),
(499811, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne'),
(499813, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Odzież żeglarska i do uprawiania sportów wodnych'),
(499814, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Odzież żeglarska i do uprawiania sportów wodnych / Pianki w częściach'),
(499815, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wspinaczka / Odzież i akcesoria do wspinaczki'),
(499816, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wspinaczka / Odzież i akcesoria do wspinaczki / Rękawice wspinaczkowe'),
(499817, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Produkty do pielęgnacji koni / Maski dla koni przeciw owadom'),
(499818, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Produkty do pielęgnacji koni / Artykuły do pielęgnacji koni / Rękawice, szczotki i grzebienie do czyszczenia koni'),
(499819, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Produkty do pielęgnacji koni / Preparaty dla koni przeciw pasożytom'),
(499820, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Akcesoria do uprzęży jeździeckich / Skrzynki na przybory jeździeckie'),
(499821, 'Media / Instrukcje obsługi / Instrukcje obsługi aparatów, kamer i przyrządów optycznych'),
(499822, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do makijażu / Wielorazowe palety do makijażu'),
(499823, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Przybory wędkarskie'),
(499824, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo'),
(499825, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Łucznictwo / Łuki i kusze'),
(499826, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Łucznictwo / Akcesoria do łuków i kusz'),
(499830, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Łucznictwo / Części i akcesoria do strzał / Groty do strzał'),
(499831, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Łucznictwo / Części i akcesoria do strzał / Lotki do strzał'),
(499832, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Łucznictwo / Części i akcesoria do strzał / Końcówki strzał'),
(499833, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Łucznictwo / Rękawice oraz spusty łucznicze'),
(499834, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Paintball i ASG'),
(499835, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Paintball i ASG / Sprzęt ochronny do gry w paintballa i ASG'),
(499836, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Paintball i ASG / Sprzęt ochronny do gry w paintballa i ASG / Rękawice do paintballa i ASG'),
(499837, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Paintball i ASG / Sprzęt ochronny do gry w paintballa i ASG / Kamizelki do paintballa i ASG'),
(499838, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Paintball i ASG / Sprzęt ochronny do gry w paintballa i ASG / Gogle i maski do paintballa i ASG'),
(499839, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Paintball i ASG / Sprzęt ochronny do gry w paintballa i ASG / Ochraniacze do gry w paintballa i ASG'),
(499840, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Akcesoria strzeleckie'),
(499841, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Akcesoria strzeleckie / Statywy i podstawki pod broń'),
(499842, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Myślistwo i strzelectwo / Akcesoria strzeleckie / Stojany do przestrzeliwania broni'),
(499844, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą'),
(499845, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Jazda na nartach i snowboardzie'),
(499846, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Gry na świeżym powietrzu'),
(499847, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Gry na świeżym powietrzu / Pickleball / Piłki do gry w pickleball'),
(499848, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Gry na świeżym powietrzu / Pickleball / Rakietki do pickleballa'),
(499849, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Gry na świeżym powietrzu / Tenis łopatkowy i platformowy / Piłki do tenisa platformowego'),
(499850, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Gry na świeżym powietrzu / Tenis łopatkowy i platformowy / Rakiety do tenisa łopatkowego i platformowego'),
(499853, 'Artykuły dla dorosłych / Broń / Konserwacja i akcesoria do broni palnej / Pasy do broni'),
(499854, 'Artykuły dla dorosłych / Broń / Konserwacja i akcesoria do broni palnej / Czyszczenie broni / Rozpuszczalniki do czyszczenia broni'),
(499855, 'Artykuły dla dorosłych / Broń / Konserwacja i akcesoria do broni palnej / Czyszczenie broni / Flejtuchy'),
(499856, 'Artykuły dla dorosłych / Broń / Konserwacja i akcesoria do broni palnej / Czyszczenie broni / Przecieraki'),
(499857, 'Artykuły dla dorosłych / Broń / Konserwacja i akcesoria do broni palnej / Artykuły i sprzęt związany z elaboracją amunicji / Prasy elaboracyjne'),
(499858, 'Biznes i przemysł / Branża medyczna / Sprzęt medyczny / Nosze ręczne i nosze na kółkach'),
(499859, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do szlifierek'),
(499860, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do szlifierek / Kamienie i tarcze szlifierskie'),
(499861, 'Sprzęt sportowy / Sport / Sprzęt do gry w wallyball'),
(499864, 'Artykuły biurowe / Sprzęt biurowy / Akcesoria do kalkulatorów'),
(499865, 'Dom i ogród / Bezpieczeństwo domu i biura / Akcesoria do sejfów'),
(499866, 'Media / Instrukcje obsługi / Instrukcje obsługi do artykułów biurowych'),
(499867, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Nurkowanie i snorkeling / Zestawy do nurkowania z rurką lub akwalungiem'),
(499868, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Grupy osprzętu do rowerów'),
(499869, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Nakrętki na wentyle rowerowe'),
(499870, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Wentyle rowerowe'),
(499871, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Przejściówki do wentyli rowerowych'),
(499872, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Akcesoria do kosiarek / Dętki do opon do kosiarek'),
(499873, 'Sprzęt / Wentylacja'),
(499874, 'Sprzęt / Wentylacja / Kanały wentylacyjne'),
(499875, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Części kół rowerowych / Opaski na obręcze rowerowe'),
(499876, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Materiały murarskie / Fugi'),
(499877, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Zabezpieczenia lawinowe / Plecaki lawinowe'),
(499878, 'Elektronika / Akcesoria elektroniczne / Statywy do telefonów komórkowych i tabletów'),
(499879, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Farby, tusze i laserunki / Szkliwo ceramiczne'),
(499880, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Części i akcesoria do ciągników / Opony do ciągników'),
(499881, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Części i akcesoria do ciągników / Koła do ciągników'),
(499882, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Gry na świeżym powietrzu / Piłka na uwięzi / Słupki do gry w piłkę na uwięzi'),
(499883, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Gry na świeżym powietrzu / Piłka na uwięzi / Zestawy do gry w piłkę na uwięzi'),
(499884, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Gry na świeżym powietrzu / Piłka na uwięzi / Piłki do gry w piłkę na uwięzi'),
(499885, 'Dom i ogród / Artykuły gospodarstwa domowego / Nakładki na schody'),
(499886, 'Sprzęt / Akcesoria do narzędzi / Mieszadła'),
(499887, 'Sprzęt / Narzędzia / Mieszarki ręczne'),
(499888, 'Sprzęt / Narzędzia / Narzędzia malarskie / Mieszalniki farb'),
(499889, 'Elektronika / Płyty drukowane i komponenty / Obwody drukowane / Obwody drukowane do urządzeń do ćwiczeń'),
(499890, 'Sprzęt sportowy / Sport / Łyżwiarstwo figurowe i hokej / Ochraniacze hokejowe / Zestawy ochronne dla bramkarzy hokejowych'),
(499891, 'Sztuka i rozrywka / Hobby i sztuki piękne / Warzenie piwa i pędzenie wina / Zestawy do domowego wyrobu wina i piwa'),
(499892, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Szczotki mioteł'),
(499893, 'Sprzęt / Materiały elektryczne / Szczotki węglowe'),
(499894, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Siatki i podpory dla roślin'),
(499897, 'Biznes i przemysł / Handel detaliczny / Rekwizyty i modele na wystawy sklepowe'),
(499898, 'Elektronika / Płyty drukowane i komponenty / Obwody drukowane / Obwody drukowane do komputerów'),
(499899, 'Elektronika / Płyty drukowane i komponenty / Obwody drukowane / Obwody drukowane do komputerów / Inwertery do komputerów'),
(499900, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla psów / Pieluchy dla psów'),
(499903, 'Sprzęt sportowy / Sport / Sprzęt ogólnosportowy / Wózki i stojaki na maty gimnastyczne'),
(499904, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Gry na świeżym powietrzu / Tenis łopatkowy i platformowy'),
(499905, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Mieszanki warzywne'),
(499906, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Mieszanki owocowe'),
(499907, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Markizy'),
(499908, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Akcesoria do markiz'),
(499912, 'Sprzęt sportowy / Fitness / Poręcze wielofunkcyjne'),
(499913, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria i kosmetyki do kąpieli / Nawilżane chusteczki dla dorosłych'),
(499915, 'Sprzęt sportowy / Sport / Łyżwiarstwo figurowe i hokej'),
(499916, 'Elektronika / Telekomunikacja / Telefonia / Akcesoria do telefonów komórkowych / Przyrządy do wyjmowania karty SIM'),
(499917, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły groomerskie dla zwierząt / Nawilżane chusteczki dla zwierząt'),
(499918, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Akcesoria do narzędzi dla artystów i rękodzielników / Bloczki do stempli'),
(499919, 'Zdrowie i uroda / Czyszczenie i pielęgnacja biżuterii / Środki czyszczące i polerujące do biżuterii'),
(499921, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Akcesoria do kosiarek / Zestawy do robienia wzorów na trawie'),
(499922, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Narzędzia ogrodnicze / Walce do trawy'),
(499923, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Akcesoria do kosiarek / Zestawy mulczujące do kosiarek'),
(499924, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do przechowywania żywności / Etykiety na żywność i napoje'),
(499926, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do pielęgnacji twarzy / Aplikatory balsamu i kremu do opalania'),
(499927, 'Biznes i przemysł / Środki ochrony osobistej / Hełmy dla spawaczy'),
(499928, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Akcesoria do akumulatorów / Testery baterii do ogólnego zastosowania'),
(499929, 'Pojazdy i części / Akcesoria i części do pojazdów / Konserwacja, pielęgnacja i ozdabianie pojazdów / Narzędzia do naprawy pojazdów / Urządzenia testujące akumulatory'),
(499930, 'Dom i ogród / Artykuły gospodarstwa domowego / Przechowywanie i utrzymanie porządku / Haczyki i stojaki do przechowywania / Haki i stojaki na deskę do prasowania'),
(499931, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły do pralni / Części zamienne desek do prasowania'),
(499932, 'Sprzęt / Materiały elektryczne / Przełączniki elektryczne / Specjalne włączniki światła'),
(499933, 'Sprzęt / Akcesoria do sprzętu / Sprężyny'),
(499934, 'Zdrowie i uroda / Opieka zdrowotna / Testy medyczne / Zestawy do wykrywania alergii'),
(499935, 'Biznes i przemysł / Branża medyczna / Instrumenty medyczne / Igły i nici chirurgiczne'),
(499937, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły do pralni / Podstawki pod żelazka'),
(499938, 'Gry i zabawki / Zabawki / Zabawki edukacyjne / Zabawki i modele astronomiczne'),
(499942, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Wędkarstwo / Akcesoria do wędek / Stojaki i wieszaki na wędki'),
(499944, 'Elektronika / Akcesoria elektroniczne / Rozdzielacze i przełączniki do przesyłania obrazu i dźwięku / Rozdzielacze i przełączniki DVI'),
(499945, 'Elektronika / Akcesoria elektroniczne / Rozdzielacze i przełączniki do przesyłania obrazu i dźwięku / Rozdzielacze i przełączniki VGA'),
(499946, 'Biznes i przemysł / Rolnictwo / Hodowla zwierząt / Uzdy dla zwierząt hodowlanych'),
(499947, 'Sprzęt / Akcesoria do narzędzi / Akcesoria spawalnicze'),
(499948, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Akcesoria do parasoli i pawilonów ogrodowych / Pokrowce do parasoli zewnętrznych'),
(499949, 'Sprzęt / Materiały budowlane / Poręcze i zestawy poręczy'),
(499950, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Urządzenia wejściowe / Urządzenia do kontroli gestami'),
(499951, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Zabezpieczenia lawinowe'),
(499952, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Zabezpieczenia lawinowe / Sondy lawinowe'),
(499953, 'Sztuka i rozrywka / Hobby i sztuki piękne / Artykuły kolekcjonerskie / Broń kolekcjonerska / Pistolety i strzelby kolekcjonerskie'),
(499954, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla ptaków / Akcesoria do klatek dla ptaków / Wanienki dla ptaków'),
(499955, 'Dom i ogród / Trawnik i ogród / Akcesoria ogrodowe / Akcesoria do huśtawek ogrodowych'),
(499956, 'Elektronika / Akcesoria elektroniczne / Akcesoria komputerowe / Stacje dokujące i stojaki na tablety'),
(499958, 'Sprzęt / Ogrodzenia i barierki / Barierki bezpieczeństwa'),
(499959, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jeździectwo / Akcesoria do uprzęży jeździeckich / Osprzęt do siodeł / Pokrowce i futerały na siodła'),
(499960, 'Dom i ogród / Trawnik i ogród / Akcesoria do elektronarzędzi ogrodowych / Akcesoria do kosiarek / Dodatki mulczujące do kosiarek'),
(499961, 'Biznes i przemysł / Środki ochrony osobistej / Ochraniacze na kolana'),
(499962, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Wkładki do doniczek'),
(499963, 'Żywność, napoje i tytoń / Wyroby tytoniowe / Fajki do palenia'),
(499964, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Pływanie łodzią i rafting / Smycze kajakowe'),
(499965, 'Gry i zabawki / Zabawki / Zabawki sportowe / Piłki zabawkowe'),
(499966, 'Sprzęt / Materiały elektryczne / Wtyczki elektryczne'),
(499969, 'Sztuka i rozrywka / Bilety na wydarzenia'),
(499970, 'Sprzęt / Materiały budowlane / Wyposażenie do drzwi / Powierzchnie dotykowe na drzwi'),
(499971, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Porządek w warsztacie / Zestawy do szycia'),
(499972, 'Ubrania i akcesoria / Akcesoria do ubrań / Szarfy'),
(499975, 'Sprzęt sportowy / Sport / Łyżwiarstwo figurowe i hokej / Ochraniacze hokejowe / Szelki i pasy hokejowe'),
(499976, 'Aparaty, kamery i przyrządy optyczne / Aparaty i kamery / Boreskopy'),
(499978, 'Sprzęt sportowy / Fitness / Zestawy urządzeń do ćwiczeń'),
(499979, 'Ubrania i akcesoria / Ubrania / Ubrania sportowe / Sukienki, spódnice i kostiumy do tańca'),
(499981, 'Sprzęt / Akcesoria do sprzętu / Kółka do mebli'),
(499982, 'Sprzęt / Małe silniki'),
(499985, 'Sprzęt / Narzędzia / Piły / Piły panelowe'),
(499986, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Sok z cytryny i limonki'),
(499988, 'Żywność, napoje i tytoń / Żywność / Dania gotowe / Gotowe posiłki i przystawki'),
(499989, 'Żywność, napoje i tytoń / Żywność / Dania gotowe / Gotowe przystawki i dodatki do dań'),
(499990, 'Dom i ogród / Kuchnia i jadalnia / Naczynia barowe / Nakrętki i kapsle na butelki'),
(499991, 'Żywność, napoje i tytoń / Żywność / Mrożone desery i dodatki / Lody i mrożony jogurt'),
(499992, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja włosów / Narzędzia do stylizacji włosów / Zestawy urządzeń do stylizacji włosów'),
(499993, 'Sprzęt sportowy / Gry towarzyskie / Bilard / Akcesoria do kijów bilardowych / Pokrowce na kije bilardowe'),
(499994, 'Sprzęt sportowy / Gry towarzyskie / Bilard / Akcesoria do kijów bilardowych / Kreda bilardowa'),
(499995, 'Media / Instrukcje stolarskie „zrób to sam”'),
(499996, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do maszynek do gofrów'),
(499997, 'Biznes i przemysł / Rolnictwo / Hodowla zwierząt / Inkubatory jaj'),
(499998, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria i części do aparatów / Stabilizatory i statywy do aparatów'),
(499999, 'Sprzęt / Hydraulika / Armatury / Zestawy łazienkowe'),
(500000, 'Meble / Zestawy mebli / Zestawy mebli łazienkowych'),
(500001, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Pulciki i przenośne pulpity na nuty'),
(500002, 'Elektronika / Komputery / Okulary typu „smart glasses”'),
(500003, 'Dom i ogród / Oświetlenie / Pływające oświetlenie'),
(500004, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do płyt kuchennych, piekarników i kuchenek'),
(500005, 'Gry i zabawki / Zabawki / Akcesoria do zabawkowej broni'),
(500007, 'Dom i ogród / Akcesoria dla palaczy / Cygarniczki'),
(500008, 'Ubrania i akcesoria / Przebrania i akcesoria / Akcesoria do przebrań / Produkty imitujące wyroby tytoniowe'),
(500009, 'Zdrowie i uroda / Opieka zdrowotna / Monitory funkcji życiowych / Urządzenia do pomiaru aktywności fizycznej'),
(500013, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Części do tabletów / Monitory i digitizery do tabletów'),
(500015, 'Gry i zabawki / Zabawki / Zabawki edukacyjne / Karty edukacyjne'),
(500016, 'Dom i ogród / Trawnik i ogród / Silnikowe narzędzia ogrodowe / Zamiatarki'),
(500024, 'Zdrowie i uroda / Higiena osobista / Pielęgnacja uszu / Akcesoria i łyżeczki do czyszczenia uszu'),
(500025, 'Dom i ogród / Bezpieczeństwo domu i biura / Lustra bezpieczeństwa'),
(500026, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Akcesoria do transporterów dla zwierząt'),
(500027, 'Elektronika / Płyty drukowane i komponenty / Akcesoria do płyt drukowanych'),
(500028, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Odzież i akcesoria dla rowerzystów / Części i akcesoria do hełmów rowerowych'),
(500029, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na wrotkach i łyżworolkach / Narty rollerski'),
(500030, 'Sprzęt / Akcesoria do sprzętu / Pokrowce meblowe do przeprowadzek'),
(500033, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Doniczki i spodki pod donice ogrodowe'),
(500034, 'Dom i ogród / Trawnik i ogród / Silnikowe narzędzia ogrodowe / Aeratory i urządzenia oczyszczające do trawników'),
(500035, 'Elektronika / Akcesoria elektroniczne / Kable / Kable do przesyłu danych'),
(500036, 'Elektronika / Akcesoria elektroniczne / Zarządzanie kablami / Pistolety do spinania kabli'),
(500037, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria i części do aparatów / Uchwyty do aparatów'),
(500038, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla rybek / Narzędzia i środki do czyszczenia akwariów'),
(500039, 'Dom i ogród / Artykuły gospodarstwa domowego / Przechowywanie śmieci / Śmietniki'),
(500040, 'Elektronika / Akcesoria elektroniczne / Akcesoria komputerowe / Zestawy akcesoriów komputerowych'),
(500042, 'Dom i ogród / Baseny i spa / Akcesoria do basenów i spa / Generatory chloru do basenów'),
(500043, 'Sprzęt / Wentylacja / Sterowniki HVAC / Hydrostaty'),
(500044, 'Dom i ogród / Ozdoby / Dzieła sztuki / Plakaty, nadruki, prace plastyczne'),
(500045, 'Dom i ogród / Ozdoby / Dzieła sztuki / Makaty dekoracyjne'),
(500046, 'Oprogramowanie / Towary i waluty cyfrowe / Cyfrowe prace plastyczne'),
(500048, 'Artykuły dla dorosłych / Broń / Konserwacja i akcesoria do broni palnej / Pokrowce na broń i akcesoria'),
(500049, 'Sprzęt / Materiały elektryczne / Wirniki i stojany'),
(500050, 'Dom i ogród / Baseny i spa / Akcesoria do basenów i spa / Akcesoria do pokryw basenowych'),
(500051, 'Meble / Krzesła i fotele / Krzesła bez nóg'),
(500052, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Akcesoria do urządzeń wejściowych / Akcesoria do myszek i manipulatorów kulkowych'),
(500053, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Części rowerowe / Części kół rowerowych / Części piast rowerowych'),
(500054, 'Sprzęt / Akcesoria do sprzętu / Akcesoria mocujące do sprzętu'),
(500055, 'Sprzęt / Akcesoria do sprzętu / Akcesoria mocujące do sprzętu / Pręty gwintowane'),
(500056, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Ozdobne spinki i zamknięcia / Suwaki do zamków błyskawicznych'),
(500057, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Sprzęt laboratoryjny / Urządzenia do liofilizacji'),
(500058, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Anemometry'),
(500059, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla kotów / Akcesoria do mebli dla kotów'),
(500060, 'Zdrowie i uroda / Higiena osobista / Masaż i relaks / Drapaczki do pleców'),
(500061, 'Meble / Meble biurowe / Zestawy mebli biurowych'),
(500062, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla rybek / Regulatory temperatury do akwariów'),
(500063, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do podgrzewaczy wody / Dodatkowe zbiorniki do bojlerów'),
(500064, 'Meble / Akcesoria do sof / Segmenty wersalek'),
(500065, 'Dom i ogród / Artykuły gospodarstwa domowego / Artykuły domowe do sprzątania / Środki czyszczące do gospodarstwa domowego / Odkamieniacze i odwapniacze'),
(500066, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do czajników elektrycznych'),
(500067, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Akcesoria do krzesełek rowerowych dla dzieci'),
(500074, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Marynaty i sosy do grillowania'),
(500075, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Sosy typu chutney'),
(500076, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Marynowane owoce i warzywa'),
(500077, 'Pojazdy i części / Akcesoria i części do pojazdów / Zabezpieczenia pojazdów / Alarmy i zamki samochodowe / Blokady kół samochodowych'),
(500078, 'Dom i ogród / Ozdoby / Domki dla ptaków i zwierząt'),
(500079, 'Dom i ogród / Ozdoby / Domki dla ptaków i zwierząt / Domki dla nietoperzy'),
(500080, 'Dom i ogród / Ozdoby / Domki dla ptaków i zwierząt / Domki dla motyli'),
(500081, 'Dom i ogród / Sprzęt AGD / Urządzenia czyszczące ultradźwiękami'),
(500082, 'Zdrowie i uroda / Czyszczenie i pielęgnacja biżuterii / Narzędzia do czyszczenia biżuterii'),
(500083, 'Zdrowie i uroda / Czyszczenie i pielęgnacja biżuterii / Urządzenia parowe do czyszczenia biżuterii'),
(500084, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Akcesoria do łóżek dla zwierząt'),
(500085, 'Dom i ogród / Akcesoria do sprzętu AGD / Akcesoria do prania / Akcesoria do pralek i suszarek'),
(500086, 'Biznes i przemysł / Wózki do sprzątania'),
(500087, 'Zdrowie i uroda / Opieka zdrowotna / Lampy terapeutyczne'),
(500088, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Chemikalia / Płyny do zapalniczek'),
(500089, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Sosy słodko-kwaśne'),
(500090, 'Sprzęt / Wentylacja / Osuszacze filtrujące'),
(500091, 'Elektronika / Radary do mierzenia prędkości'),
(500092, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Uchwyty i elementy dekoracyjne do kierownic rowerowych'),
(500093, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Mleko w proszku i puszce'),
(500094, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Farby, tusze i laserunki / Utrwalacze malarskie'),
(500095, 'Gry i zabawki / Zabawki ogrodowe / Zabawki do wody / Wodne place zabaw i zjeżdżalnie do basenów ogrodowych'),
(500096, 'Sprzęt / Pompy'),
(500097, 'Sprzęt / Pompy / Pompy do nawadniania i spryskiwania'),
(500098, 'Sprzęt / Pompy / Pompy do basenów, fontann i oczek wodnych'),
(500099, 'Sprzęt / Pompy / Pompy do urządzeń domowych'),
(500100, 'Sprzęt / Pompy / Pompy i systemy do studni'),
(500101, 'Sprzęt / Pompy / Pompy przenośne'),
(500102, 'Sprzęt / Pompy / Pompy do systemów oczyszczania'),
(500103, 'Sprzęt / Akcesoria do sprzętu / Przechowywanie i porządkowanie narzędzi / Organizery do narzędzi'),
(500104, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria i części do aparatów / Urządzenia do ustawiania ostrości'),
(500105, 'Żywność, napoje i tytoń / Żywność / Przyprawy i sosy / Sosy białe'),
(500106, 'Elektronika / Drukowanie, kopiowanie, skanowanie i faksowanie / Drukarki, kopiarki i faksy'),
(500107, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria i części do aparatów / Dodatkowe ekrany do aparatu'),
(500109, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Cześci do wakeboardingu'),
(500110, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Akcesoria do mat grzewczych dla zwierząt');
INSERT INTO `google_kategorie` (`id`, `nazwa`) VALUES
(500111, 'Meble / Meble zewnętrzne / Siedzenia ogrodowe / Zewnętrzne sofy modułowe'),
(500112, 'Elektronika / Audio / Akcesoria audio / Akcesoria do głośników / Torby, pokrowce i futerały na głośniki'),
(500113, 'Gry i zabawki / Zabawki / Zabawki sportowe / Fingerboardy i zestawy'),
(500114, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Sprzęt laboratoryjny / Chłodziarki laboratoryjne'),
(500115, 'Dom i ogród / Artykuły gospodarstwa domowego / Akcesoria do przechowywania śmieci / Znaki i etykiety na pojemniki na śmieci'),
(500117, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Akumulatory / Akumulatory do konsol do gier i kontrolerów'),
(500118, 'Ubrania i akcesoria / Przebrania i akcesoria / Akcesoria do przebrań / Specjalne dodatki'),
(500119, 'Elektronika / Audio / Akcesoria audio / Akcesoria do głośników / Przetworniki dotykowe'),
(500120, 'Elektronika / Audio / Akcesoria audio / Akcesoria do głośników / Części do głośników i zestawy'),
(500121, 'Dom i ogród / Ozdoby / Akcesoria do zapachów do domu'),
(500122, 'Dom i ogród / Ozdoby / Akcesoria do zapachów do domu / Gasidła do świec'),
(502966, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Łyżki do nabierania / Łopatki do popcornu i frytek'),
(502969, 'Zdrowie i uroda / Opieka zdrowotna / Artykuły dla osób mających trudności z poruszaniem się / Sprzęt ułatwiający poruszanie się / Deski transferowe i maty do przenoszenia'),
(502970, 'Sprzęt sportowy / Sport / Hokej na trawie i lacrosse / Ochraniacze do hokeja na trawie i lacrosse / Ochraniacze hokejowe i do lacrosse'),
(502972, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Zestawy akcesoriów na przyjęcia'),
(502973, 'Sprzęt / Ogrodzenia i barierki / Panele ogrodzeń'),
(502974, 'Torby, walizki i akcesoria podróżne / Futerały'),
(502975, 'Sprzęt / Zbiorniki paliwa'),
(502976, 'Żywność, napoje i tytoń / Napoje / Napoje alkoholowe / Likiery i trunki / Shochu i soju'),
(502977, 'Sprzęt / Akcesoria do sprzętu / Łańcuchy, druty i sznury'),
(502978, 'Sprzęt / Akcesoria do sprzętu / Haki, klamry i łączniki / Opaski uniwersalne'),
(502979, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Zestawy do sztuki i rękodzieła / Zestawy do wyrobu biżuterii'),
(502980, 'Sztuka i rozrywka / Hobby i sztuki piękne / Warzenie piwa i pędzenie wina / Butelki'),
(502981, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Serpentyny i zasłony'),
(502982, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Pojemniki i dozowniki z torebkami na odchody'),
(502983, 'Sprzęt / Ogrodzenia i barierki / Akcesoria do bram i ogrodzeń'),
(502984, 'Sprzęt / Ogrodzenia i barierki / Siatki'),
(502986, 'Sprzęt / Ogrodzenia i barierki / Ogrodzenia i barierki do ogrodu'),
(502987, 'Ubrania i akcesoria / Akcesoria do ubrań / Pasy ciążowe'),
(502988, 'Ubrania i akcesoria / Akcesoria do ubrań / Akcesoria do włosów / Szpilki, klamry i spinki do włosów'),
(502989, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do syfonów'),
(502990, 'Elektronika / Drukowanie, kopiowanie, skanowanie i faksowanie / Akcesoria do kopiarek, drukarek i faksów'),
(502991, 'Elektronika / Drukowanie, kopiowanie, skanowanie i faksowanie / Akcesoria do kopiarek, drukarek i faksów / Części zamienne  do kopiarek, drukarek i faksów'),
(502992, 'Sprzęt / Akcesoria do sprzętu / Haki, klamry i łączniki / Praktyczne klamry'),
(502993, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Przenośne toalety i prysznice'),
(502994, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Przenośne toalety i prysznice / Przenośne prysznice i parawany'),
(502995, 'Elektronika / Komputery / Komputery touch table'),
(502996, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do makijażu / Akcesoria do sztucznych rzęs'),
(502997, 'Zdrowie i uroda / Higiena osobista / Kosmetyki / Akcesoria kosmetyczne / Akcesoria do makijażu / Akcesoria do sztucznych rzęs / Preparat do usuwania sztucznych rzęs'),
(502999, 'Dzieci i niemowlęta / Przewijanie / Maty i blaty do przewijania'),
(503000, 'Dom i ogród / Ozdoby / Klepsydry'),
(503001, 'Elektronika / Elementy / Konwertery / Konwertery audio'),
(503002, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Części do tabletów / Obudowy tabletów'),
(503003, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Akcesoria do urządzeń wejściowych / Klawisze do klawiatur i nakładki na klawisze'),
(503004, 'Elektronika / Audio / Akcesoria audio / Akcesoria do słuchawek i zestawów słuchawkowych / Poduszki i nakładki do słuchawek'),
(503005, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Lejki'),
(503006, 'Biznes i przemysł / Branża medyczna / Sprzęt medyczny / Pasy transportowe'),
(503007, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do narzędzi pomiarowych i czujników / Akcesoria do skal pomiarowych'),
(503008, 'Elektronika / Audio / Akcesoria audio / Akcesoria do odbiorników audio-wideo'),
(503009, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Camping i chodzenie po górach / Przenośne toalety i prysznice / Przenośne toalety i pisuary'),
(503010, 'Dom i ogród / Ozdoby / Akcesoria do flag i wiatrowskazów / Osprzęt i zestawy do montażu flag i wiatrowskazów na maszcie'),
(503011, 'Biznes i przemysł / Transport materiałów / Palety i platformy załadowcze'),
(503014, 'Torby, walizki i akcesoria podróżne / Akcesoria podróżne / Wyściółki i wkładki do futerałów'),
(503016, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria do statywów trójnożnych i jednonożnych / Trójnóg z odłączaną stopą'),
(503017, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Oświetlenie i studio / Stojaki pod monitory studyjne i akcesoria do montażu'),
(503018, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Oświetlenie i studio / Oświetlenie studyjne i akcesoria oświetleniowe'),
(503019, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria i części do aparatów / Osłony przeciwsłoneczne obiektywu i dołączane osłony wizjera'),
(503020, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria i części do aparatów / Tłumiki dźwięku i kamery dźwiękowe'),
(503021, 'Artykuły dla dorosłych / Broń / Konserwacja i akcesoria do broni palnej / Czyszczenie broni'),
(503026, 'Artykuły dla dorosłych / Broń / Konserwacja i akcesoria do broni palnej / Artykuły i sprzęt związany z elaboracją amunicji'),
(503028, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Efekty specjalne / Stojaki do lamp specjalnych'),
(503031, 'Pojazdy i części / Pojazdy / Pojazdy silnikowe / Pojazdy terenowe i quady'),
(503032, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych / Akcesoria do gitar / Części i osprzęt do gitar'),
(503033, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych / Akcesoria do instrumentów strunowych dla orkiestr'),
(503034, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych / Akcesoria do instrumentów strunowych dla orkiestr / Struny do instrumentów strunowych dla orkiestr '),
(503035, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych / Akcesoria do instrumentów strunowych dla orkiestr / Stojaki do instrumentów strunowych dla orkiestr '),
(503036, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych / Akcesoria do instrumentów strunowych dla orkiestr / Przystawki do instrumentów strunowych dla orkiestr '),
(503037, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych / Akcesoria do instrumentów strunowych dla orkiestr / Tłumiki do instrumentów strunowych dla orkiestr '),
(503038, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych / Akcesoria do instrumentów strunowych dla orkiestr / Części i osprzęt do instrumentów strunowych dla orkiestr'),
(503039, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych / Akcesoria do instrumentów strunowych dla orkiestr / Futerały na instrumenty strunowe dla orkiestr'),
(503040, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych / Akcesoria do instrumentów strunowych dla orkiestr / Smyczki do instrumentów strunowych dla orkiestr'),
(503348, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Akcesoria do narzędzi dla artystów i rękodzielników / Części zamienne do maszyn do szycia'),
(503721, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów strunowych / Akcesoria do gitar / Korbki do nawijania strun'),
(503722, 'Biznes i przemysł / Badania naukowe i laboratoryjne / Sprzęt laboratoryjny / Lejki laboratoryjne'),
(503723, 'Artykuły religijne i dewocjonalia / Artykuły ślubne / Dywany ślubne'),
(503724, 'Biznes i przemysł / Środki ochrony osobistej / Maski ochronne'),
(503725, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do zestawów do fondue / Paliwa w żelu'),
(503726, 'Aparaty, kamery i przyrządy optyczne / Akcesoria do aparatów, kamer i przyrządów optycznych / Akcesoria do statywów trójnożnych i jednonożnych / Trójnogi i statywy bezpieczeństwa'),
(503727, 'Sprzęt / Materiały budowlane / Wyposażenie do drzwi / Ościeżnice'),
(503728, 'Sprzęt / Materiały budowlane / Osprzęt okienny / Ramy okienne'),
(503729, 'Sprzęt / Materiały elektryczne / Końcówki, zaciski i złączki'),
(503730, 'Sprzęt / Zamki i klucze / Blokady i zasuwki'),
(503731, 'Sprzęt / Akcesoria do sprzętu / Formy do odlewów metalowych'),
(503733, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły groomerskie dla zwierząt / Zapachy i dezodoranty dla zwierząt'),
(503734, 'Żywność, napoje i tytoń / Żywność / Składniki do gotowania i pieczenia / Cukier i słodziki'),
(503735, 'Aparaty, kamery i przyrządy optyczne / Fotografia / Artykuły zabezpieczające i ochronne'),
(503736, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do ekspresów do kawy i espresso / Części zamienne do kawiarek i ekspresów'),
(503737, 'Sprzęt / Narzędzia / Narzędzia pomiarowe i czujniki / Elektroniczne narzędzia pomiarowe'),
(503738, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Materiały malarskie / Lakiery i inne powłoki malarskie'),
(503739, 'Sprzęt / Materiały eksploatacyjne dla budownictwa'),
(503740, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Materiały malarskie'),
(503741, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Rozpuszczalniki, rozcieńczalniki i środki do usuwania powłok'),
(503742, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Kleje'),
(503743, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Materiały murarskie'),
(503744, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Powłoki ochronne i uszczelnienia'),
(503745, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Kleje i magnesy dla rękodzieła / Kleje dla hobbystów i do biura'),
(503746, 'Artykuły biurowe / Akcesoria biurowe / Podajniki na taśmę klejącą'),
(503747, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do fletów prostych'),
(503748, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do fletów prostych / Futerały na flet prosty'),
(503749, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do fletów prostych / Konserwacja i czyszczenie fletów prostych'),
(503750, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Instrumenty dęte / Akcesoria do fletów prostych / Części fletów prostych'),
(503751, 'Dom i ogród / Baseny i spa / Akcesoria do basenów i spa / Folie basenowe'),
(503752, 'Sprzęt sportowy / Sport / Racquetball i squash'),
(503753, 'Sprzęt sportowy / Sport / Racquetball i squash / Piłki do racquetballa i squasha'),
(503755, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Karty i adaptery I/O / Karty riser'),
(503756, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Akcesoria ogrodnicze / Ogrodowe krzesełka na kółkach i klęczniki'),
(503757, 'Dom i ogród / Kuchnia i jadalnia / Naczynia barowe / Shakery i przybory do drinków / Rozgniatacze do owoców'),
(503758, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Zestawy do sztuki i rękodzieła / Scrapbooking i stemplowanie'),
(503759, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Cytrusy'),
(503760, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Owoce pestkowe'),
(503761, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Kiełki'),
(503762, 'Dzieci i niemowlęta / Karmienie / Wkładki laktacyjne i osłonki na brodawki'),
(503763, 'Sprzęt sportowy / Sport / Gimnastyka / Drążki gimnastyczne i równoważnie'),
(503764, 'Sprzęt / Akcesoria do sprzętu / Haki, klamry i łączniki / Spinki i ogniwa łańcuchów'),
(503765, 'Meble / Akcesoria do mebli biurowych'),
(503766, 'Meble / Akcesoria do mebli biurowych / Części i akcesoria do biurek'),
(503767, 'Biznes i przemysł / Transport materiałów / Narzędzia do wciągania i podnoszenia / Podesty ruchome'),
(503768, 'Biznes i przemysł / Transport materiałów / Narzędzia do wciągania i podnoszenia / Podnośniki, dźwigi i wózki'),
(503769, 'Biznes i przemysł / Transport materiałów / Narzędzia do wciągania i podnoszenia / Krążki linowe, zblocza i wielokrążki'),
(503770, 'Sprzęt / Akcesoria do sprzętu / Haki, klamry i łączniki / Haki do podnoszenia, zaciski i szekle'),
(503771, 'Biznes i przemysł / Transport materiałów / Narzędzia do wciągania i podnoszenia / Podnośniki i wózki widłowe'),
(503772, 'Biznes i przemysł / Transport materiałów / Narzędzia do wciągania i podnoszenia / Wyciągarki'),
(503773, 'Sprzęt / Akcesoria do sprzętu / Haki, klamry i łączniki'),
(503774, 'Sprzęt / Narzędzia / Zapalniczki i zapałki'),
(503775, 'Sprzęt / Materiały budowlane / Listwy wykończeniowe'),
(503776, 'Sprzęt / Materiały budowlane / Płyty gipsowo-kartonowe'),
(503777, 'Sprzęt / Materiały budowlane / Kraty i siatki zbrojeniowe'),
(504419, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Kształty i bazy / Wieńce i ramki z kwiatów'),
(504633, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Podgrzewacze do żywności / Pojemniki na ryż'),
(504634, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Akcesoria do toalet i bidetów / Kurki i dysze do bidetów'),
(504635, 'Sprzęt / Hydraulika / Armatura wodociągowa i części'),
(504636, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Odpływy'),
(504637, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Części do odpływów'),
(504638, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Części do prysznica / Wylewki i złączki do pryszniców'),
(504639, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników'),
(504640, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do wycinania i wytłaczania'),
(504641, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do wycinania i wytłaczania / Nożyczki dla hobbystów i do biura'),
(504642, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do wycinania i wytłaczania / Urządzenia do wycinania i wytłaczania'),
(504643, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Akcesoria do narzędzi dla artystów i rękodzielników'),
(505284, 'Gry i zabawki / Zabawki / Zabawki sportowe / Zabawki związane ze sportami rakietowymi'),
(505285, 'Dom i ogród / Rośliny / Zestawy do uprawy roślin i ziół'),
(505286, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Akcesoria do narzędzi dla artystów i rękodzielników / Dodatkowe blaty do maszyn do szycia'),
(505287, 'Dom i ogród / Bielizna stołowa i pościelowa / Artykuły pościelowe / Kołdry'),
(505288, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Batuty'),
(505291, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Sporty wodne holowane / Wakeboarding / Części do wakeboardingu'),
(505292, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Narzędzia ogrodnicze / Sierpy i maczety ogrodnicze'),
(505293, 'Zdrowie i uroda / Opieka zdrowotna / Testy medyczne'),
(505294, 'Zdrowie i uroda / Opieka zdrowotna / Testy medyczne / Testy do wykrywania infekcji dróg moczowych'),
(505295, 'Elektronika / Akcesoria elektroniczne / Zasilanie / Zasilacze i ładowarki'),
(505296, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Jazda na nartach i snowboardzie / Mocowania do nart'),
(505297, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Urządzenia i narzędzia do usuwania odchodów'),
(505298, 'Elektronika / Audio / Elementy audio / Zestawy do studiów nagraniowych'),
(505299, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Karty i adaptery I/O / Karty i adaptery do komputerów'),
(505300, 'Sprzęt / Materiały budowlane / Uszczelnienia do drzwi i okien'),
(505301, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone owoce / Owoce pestkowe / Brzoskwinie i nektarynki'),
(505302, 'Sprzęt sportowy / Fitness / Sportowe światła i reflektory zabezpieczające'),
(505303, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla rybek / Rurki do akwariów i oczek wodnych'),
(505304, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Pomoce do szkolenia zwierząt / Podkładki i pojemniki na maty szkoleniowe'),
(505305, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Grunt do łączenia rur'),
(505306, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla rybek / Podbieraki akwarystyczne'),
(505307, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Artykuły dla rybek / Kamienie napowietrzające i dyfuzory'),
(505308, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów dętych blaszanych / Ustniki do instrumentów dętych blaszanych'),
(505309, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów dętych blaszanych / Paski i stojaki do instrumentów dętych blaszanych'),
(505310, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów dętych blaszanych / Futerały i torby na instrumenty dęte blaszane'),
(505311, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Pomoce do szkolenia zwierząt / Spraye i płyny szkoleniowe'),
(505312, 'Dom i ogród / Akcesoria oświetleniowe / Paliwo do lamp naftowych'),
(505313, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Pomoce do szkolenia zwierząt / Klikery i dozowniki nagród'),
(505314, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Pomoce do szkolenia zwierząt'),
(505315, 'Sprzęt / Narzędzia / Dziurkacze i przebijaki'),
(505316, 'Dom i ogród / Kuchnia i jadalnia / Narzędzia i sprzęty kuchenne / Szablony do potraw i napojów'),
(505317, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Żegluga i sporty wodne / Sporty wodne holowane / Wakeboarding / Mocowania do kiteboardingu i wakeboardingu'),
(505318, 'Sprzęt / Materiały elektryczne / Transformatory i regulatory napięcia'),
(505319, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Chemikalia / Spraye schładzające dla elektryków'),
(505320, 'Sprzęt / Akcesoria do sprzętu / Podnośniki i podpórki budowlane'),
(505321, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Akcesoria do narzędzi ogrodniczych / Części robocze narzędzi ogrodniczych'),
(505322, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Akcesoria do narzędzi ogrodniczych / Rączki do narzędzi ogrodniczych'),
(505323, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do oskardów i kilofów'),
(505324, 'Sprzęt / Akcesoria do narzędzi / Akcesoria do oskardów i kilofów / Styliska do oskardów i kilofów'),
(505325, 'Sprzęt / Narzędzia / Narzędzia malarskie / Sitka do farb'),
(505326, 'Dom i ogród / Trawnik i ogród / Ogrodnictwo / Akcesoria do narzędzi ogrodniczych'),
(505327, 'Dom i ogród / Kuchnia i jadalnia / Naczynia barowe / Shakery i przybory do drinków / Sitka barowe'),
(505328, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Ławeczki i stołki dla muzyków'),
(505329, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Rzepa i brukiew'),
(505330, 'Sprzęt sportowy / Gry towarzyskie / Air hockey / Sprzęt do air hockey\'a'),
(505352, 'Zdrowie i uroda / Opieka zdrowotna / Sprzęt do fizjoterapii / Huśtawki terapeutyczne'),
(505354, 'Żywność, napoje i tytoń / Żywność / Owoce i warzywa / Świeże i mrożone warzywa / Dynie, cukinie i kabaczki'),
(505364, 'Sprzęt / Narzędzia / Wbijanie / Młoty mechaniczne'),
(505365, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Metronomy'),
(505366, 'Dzieci i niemowlęta / Karmienie / Akcesoria do laktatorów'),
(505367, 'Zdrowie i uroda / Higiena osobista / Higiena jamy ustnej / Pudełeczka na aparaty ortodontyczne'),
(505368, 'Sprzęt / Hydraulika / Armatura wodociągowa i części / Akcesoria do wanien / Podstawki i nóżki do wanien'),
(505369, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Porządek w warsztacie'),
(505370, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Zestawy do sztuki i rękodzieła'),
(505371, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Szablony i formy'),
(505372, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników'),
(505373, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Szablony i formy / Szablony do haftu na kanwie'),
(505374, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Zestawy do sztuki i rękodzieła / Zestawy do wyrobu świec'),
(505375, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Surowy wosk na świece'),
(505376, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Kleje i magnesy dla rękodzieła'),
(505377, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Drut do rękodzieła'),
(505378, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Farby, tusze i laserunki'),
(505379, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Ozdoby'),
(505380, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Ozdobne spinki i zamknięcia'),
(505381, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Kształty i bazy'),
(505382, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Włókna do rękodzieła'),
(505383, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Skóra i winyl'),
(505384, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Tekstylia'),
(505386, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do tworzenia ozdób'),
(505387, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do nici i przędzy'),
(505388, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Urządzenia do tekstyliów'),
(505391, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do mieszania kolorów'),
(505392, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do mierzenia i oznaczania'),
(505393, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Igły i szydełka'),
(505394, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Porządek w warsztacie / Organizery do igieł, szpilek i szydełek'),
(505395, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Porządek w warsztacie / Organizery do nici i przędzy'),
(505396, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Tekstylia / Tkaniny do nadruków'),
(505397, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Tekstylia / Płótna artystyczne'),
(505398, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Tekstylia / Płótna artystyczne / Płótno do haftu na kanwie'),
(505399, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Rękodzieło z papieru / Tektura i papier do scrapbookingu'),
(505400, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Rękodzieło z papieru / Papier do rysowania i malowania'),
(505401, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Garncarstwo i rzeźbiarstwo / Mieszanki masy papierowej'),
(505402, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Garncarstwo i rzeźbiarstwo / Szlikiery'),
(505403, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Kształty i bazy / Kształty z masy papierowej'),
(505404, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Kształty i bazy / Drewno i kształty'),
(505405, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Materiały do wypychania i wypełniania / Wypełnienia poduszek'),
(505406, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Materiały do wypychania i wypełniania / Granulaty do wypychania'),
(505407, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Materiały do wypychania i wypełniania / Wypełnienia'),
(505408, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Ozdobne spinki i zamknięcia / Klamerki i haczyki'),
(505409, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Ozdobne spinki i zamknięcia / Oczka i przelotki'),
(505410, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Ozdoby / Cekiny i brokat'),
(505411, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Ozdoby / Pióra'),
(505412, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Ozdoby / Wstążki i taśmy'),
(505413, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Ozdoby / Kokardy i kwiaty'),
(505414, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Farby, tusze i laserunki / Poduszeczki z tuszem'),
(505415, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Farby, tusze i laserunki / Barwniki'),
(505416, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Farby, tusze i laserunki / Tusze'),
(505417, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Farby, tusze i laserunki / Farby dla artystów i rękodzielników'),
(505418, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Drut do rękodzieła / Drut florystyczny'),
(505419, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Kleje i magnesy dla rękodzieła / Taśmy dekoracyjne'),
(505420, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Narzędzia do mierzenia i oznaczania / Narzędzia do wypalania w drewnie'),
(505421, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Urządzenia do tekstyliów / Krosna mechaniczne'),
(505422, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Narzędzia dla artystów i rękodzielników / Urządzenia do tekstyliów / Krosna ręczne'),
(505666, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria kuchenne / Miksery i blendery'),
(505667, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do grilla na zewnątrz / Części zamienne do grilli ogrodowych'),
(505668, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na rowerze / Akcesoria do rowerów / Narzędzia do rowerów'),
(505669, 'Zdrowie i uroda / Opieka zdrowotna / Środki ochrony układu oddechowego / Inhalatory parowe'),
(505670, 'Biznes i przemysł / Fryzjerstwo i kosmetologia / Peleryny fryzjerskie i osłony na szyję'),
(505761, 'Żywność, napoje i tytoń / Napoje / Napoje alkoholowe / Likiery i trunki / Absynt'),
(505762, 'Artykuły dla dorosłych / Broń / Konserwacja i akcesoria do broni palnej / Etui na amunicję'),
(505763, 'Sztuka i rozrywka / Przyjęcia i świętowanie / Artykuły na przyjęcia / Dekoracje krzeseł'),
(505764, 'Meble / Łóżka i akcesoria / Łóżka i ramy łóżek'),
(505765, 'Dom i ogród / Kuchnia i jadalnia / Akcesoria do sprzętów kuchennych / Akcesoria do mikserów i blenderów'),
(505766, 'Pojazdy i części / Akcesoria i części do pojazdów / Wyposażenie elektroniczne pojazdów silnikowych / Samochodowe odtwarzacze AV i systemy do montażu w desce rozdzielczej'),
(505767, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Pamięci komputerowe / Akcesoria do dysków twardych / Obudowy i mocowania do dysków twardych'),
(505768, 'Sztuka i rozrywka / Hobby i sztuki piękne / Akcesoria do instrumentów muzycznych / Akcesoria do instrumentów dętych blaszanych / Tłumiki do instrumentów dętych blaszanych'),
(505769, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty dęte blaszane / Rogi altowe i barytonowe'),
(505770, 'Sztuka i rozrywka / Hobby i sztuki piękne / Instrumenty muzyczne / Instrumenty dęte blaszane / Trąbki i kornety'),
(505771, 'Elektronika / Audio / Elementy audio / Słuchawki i zestawy słuchawkowe'),
(505772, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Sporty zimowe i rekreacja zimą / Jazda na nartach i snowboardzie / Narzędzia do tuningu nart i snowboardu'),
(505797, 'Elektronika / Audio / Akcesoria audio / Akcesoria do słuchawek i zestawów słuchawkowych'),
(505801, 'Elektronika / Akcesoria elektroniczne / Elementy komputera / Urządzenia wejściowe / Czytniki kart elektronicznych'),
(505802, 'Sprzęt / Materiały eksploatacyjne dla budownictwa / Szpachle i gipsy do ścian'),
(505803, 'Dom i ogród / Bielizna stołowa i pościelowa / Artykuły pościelowe / Baldachimy nad łóżko'),
(505804, 'Sztuka i rozrywka / Hobby i sztuki piękne / Rękodzieło i hobby / Materiały dla artystów i rękodzielników / Garncarstwo i rzeźbiarstwo / Gaza do modelowania'),
(505805, 'Artykuły biurowe / Artykuły różne / Taśmy dwustronne klejące'),
(505806, 'Dom i ogród / Kuchnia i jadalnia / Naczynia barowe / Pojemniki do schładzania napojów'),
(505808, 'Biznes i przemysł / Handel detaliczny / Obrót środkami pieniężnymi / Kasy fiskalne i produkty związane z obsługą terminali / Terminale płatnicze'),
(505809, 'Dom i ogród / Ozdoby / Ozdoby sezonowe i świąteczne / Szopki świąteczne i domki z dodatkami'),
(505810, 'Sprzęt / Akcesoria do narzędzi / Kliny do stylisk'),
(505811, 'Zwierzęta i artykuły dla zwierząt / Artykuły dla zwierząt / Akcesoria do drzwiczek dla zwierząt'),
(505812, 'Sprzęt / Akcesoria do narzędzi / Kliny'),
(505813, 'Sprzęt sportowy / Sport / Akcesoria sędziowskie / Materiały i narzędzia do oznaczania granic boisk i kortów'),
(505814, 'Dom i ogród / Trawnik i ogród / Nawadnianie / Dodatki do konewek'),
(505815, 'Dom i ogród / Baseny i spa / Akcesoria do basenów i spa / Zestawy do utrzymania basenów i spa'),
(505817, 'Sprzęt sportowy / Rekreacja na świeżym powietrzu / Jazda na deskorolce / Części do deskorolek / Małe części do deskorolek'),
(505818, 'Gry i zabawki / Zabawki / Zabawki artystyczne i malarskie / Plastelina i modelina'),
(505819, 'Zdrowie i uroda / Opieka zdrowotna / Akcesoria do monitorów funkcji życiowych / Akcesoria do monitorów aktywności'),
(505820, 'Zdrowie i uroda / Opieka zdrowotna / Żele i balsamy przewodzące'),
(505821, 'Biznes i przemysł / Rolnictwo / Hodowla zwierząt / Pasza dla zwierząt gospodarskich'),
(505822, 'Zdrowie i uroda / Opieka zdrowotna / Monitory funkcji życiowych / Monitory płodności i testy owulacyjne'),
(505824, 'Biznes i przemysł / Handel detaliczny / Obrót środkami pieniężnymi / Kasy fiskalne i elektroniczne terminale kasowe'),
(505825, 'Biznes i przemysł / Handel detaliczny / Obrót środkami pieniężnymi / Kasy fiskalne i produkty związane z obsługą terminali'),
(505826, 'Dom i ogród / Oświetlenie / Lampki nocne i iluminacja'),
(505827, 'Dom i ogród / Ozdoby / Części zegarów'),
(505828, 'Biznes i przemysł / Branża medyczna / Medyczne materiały eksploatacyjne / Produkty stomijne'),
(505830, 'Artykuły biurowe / Akcesoria biurowe / Noże do papieru'),
(505831, 'Sprzęt / Akcesoria do narzędzi / Ostrza do narzędzi / Ostrza do noży, przecinaków i skrobaków'),
(505832, 'Dom i ogród / Bielizna stołowa i pościelowa / Tekstylia kuchenne');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `help_info`
--

CREATE TABLE `help_info` (
  `id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `field` varchar(100) NOT NULL,
  `info` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `help_info`
--

INSERT INTO `help_info` (`id`, `type`, `field`, `info`) VALUES
(1, 'towar', 'punkty', '<p>Ilość punktów jakie otrzyma klient po zakupie tego produktu</p>'),
(2, 'towar', 'gratis_wartosc', '<p>Minimalna wartość zamówienia którą trzeba osiągnąć aby ten produkt mógł być uznany jako gratis.</p>');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `hot_deal`
--

CREATE TABLE `hot_deal` (
  `id` int(11) NOT NULL,
  `towar_id` int(11) NOT NULL,
  `cena` float DEFAULT NULL,
  `nazwa` varchar(100) DEFAULT NULL,
  `data_od` datetime DEFAULT NULL,
  `data_do` datetime NOT NULL,
  `ilosc` int(11) DEFAULT NULL,
  `sprzedano` int(11) DEFAULT NULL,
  `aktywna` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `hot_deal`
--

INSERT INTO `hot_deal` (`id`, `towar_id`, `cena`, `nazwa`, `data_od`, `data_do`, `ilosc`, `sprzedano`, `aktywna`) VALUES
(1, 1, 100, 'Zestaw Playmobil nazwa tego produktu dalej, którą ustalił admin. test test', '2019-12-04 12:00:00', '2020-04-30 23:30:00', 2, 3, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `hot_deal_i18n`
--

CREATE TABLE `hot_deal_i18n` (
  `id` int(11) NOT NULL,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `hot_deal_i18n`
--

INSERT INTO `hot_deal_i18n` (`id`, `locale`, `model`, `foreign_key`, `field`, `content`) VALUES
(1, 'pl_PL', 'HotDeal', 1, 'nazwa', 'Zestaw Playmobil nazwa tego produktu dalej, którą ustalił admin. test test');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `jednostka`
--

CREATE TABLE `jednostka` (
  `id` int(11) NOT NULL,
  `jednostka` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `jednostka`
--

INSERT INTO `jednostka` (`id`, `jednostka`) VALUES
(1, 'szt.');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `jednostka_i18n`
--

CREATE TABLE `jednostka_i18n` (
  `id` int(11) NOT NULL,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `jednostka_i18n`
--

INSERT INTO `jednostka_i18n` (`id`, `locale`, `model`, `foreign_key`, `field`, `content`) VALUES
(1, 'pl_PL', 'Jednostka', 1, 'jednostka', 'szt.');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `jezyk`
--

CREATE TABLE `jezyk` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(100) NOT NULL,
  `locale` varchar(10) NOT NULL,
  `symbol` varchar(100) NOT NULL,
  `waluta_id` int(11) NOT NULL,
  `domyslny` tinyint(4) DEFAULT '0',
  `aktywny` tinyint(4) DEFAULT '0',
  `flaga` varchar(128) DEFAULT NULL,
  `domena` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `jezyk`
--

INSERT INTO `jezyk` (`id`, `nazwa`, `locale`, `symbol`, `waluta_id`, `domyslny`, `aktywny`, `flaga`, `domena`) VALUES
(1, 'Polski', 'pl_PL', 'pl', 1, 1, 1, '5b892ce6e7c4d8.63539591.png', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `karta_podarunkowa`
--

CREATE TABLE `karta_podarunkowa` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(100) NOT NULL,
  `wartosc` float NOT NULL,
  `cena` float NOT NULL,
  `data_dodania` datetime NOT NULL,
  `aktywna` tinyint(1) NOT NULL,
  `zdjecie` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `karta_podarunkowa_kod`
--

CREATE TABLE `karta_podarunkowa_kod` (
  `id` int(11) NOT NULL,
  `karta_podarunkowa_id` int(11) NOT NULL,
  `zamowienie_id` int(11) NOT NULL,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `kod` varchar(30) NOT NULL,
  `pin` varchar(8) DEFAULT NULL,
  `wartosc` float NOT NULL,
  `data_dodania` datetime NOT NULL,
  `data_uzycia` datetime DEFAULT NULL,
  `data_waznosci` datetime DEFAULT NULL,
  `aktywny` tinyint(1) DEFAULT '0',
  `uzyty` tinyint(1) DEFAULT '0',
  `wyslany` tinyint(1) DEFAULT '0',
  `data_wyslania` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kategoria`
--

CREATE TABLE `kategoria` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `ukryta` tinyint(4) NOT NULL,
  `kolejnosc` int(11) DEFAULT '0',
  `data` date NOT NULL,
  `wyswietl_miniatury` tinyint(4) NOT NULL DEFAULT '0',
  `zdjecie` varchar(255) DEFAULT NULL,
  `nazwa` varchar(100) NOT NULL,
  `naglowek` text,
  `opis_short` text,
  `seo_tekst` text,
  `seo_tytul` varchar(255) DEFAULT NULL,
  `seo_opis` text,
  `seo_slowa` varchar(255) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `sciezka` varchar(255) DEFAULT NULL,
  `ikona` varchar(128) DEFAULT NULL,
  `preorder` tinyint(1) DEFAULT '0',
  `towar_promocja_id` int(11) DEFAULT NULL,
  `google_kategoria_id` int(11) DEFAULT NULL,
  `promuj` tinyint(1) DEFAULT '0',
  `icon_class` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `kategoria`
--

INSERT INTO `kategoria` (`id`, `parent_id`, `ukryta`, `kolejnosc`, `data`, `wyswietl_miniatury`, `zdjecie`, `nazwa`, `naglowek`, `opis_short`, `seo_tekst`, `seo_tytul`, `seo_opis`, `seo_slowa`, `lft`, `rght`, `sciezka`, `ikona`, `preorder`, `towar_promocja_id`, `google_kategoria_id`, `promuj`, `icon_class`) VALUES
(1, NULL, 0, 0, '2019-12-04', 0, NULL, 'Klocki Blocki', '', NULL, '', '', '', '', 1, 2, '/Klocki Blocki', '5e6f66b3957930.83082276.png', 0, NULL, NULL, 0, ''),
(2, NULL, 0, 0, '2019-12-04', 0, NULL, 'Zabawki drewniane', '', NULL, '', '', '', '', 3, 4, '/Zabawki drewniane', '5e6f66cb6438e2.03620008.png', 0, NULL, NULL, 0, ''),
(3, NULL, 0, 0, '2019-12-04', 0, NULL, 'Zabawki', '', NULL, '', '', '', '', 5, 24, '/Zabawki', '', 0, NULL, NULL, 0, ''),
(4, NULL, 0, 0, '2019-12-04', 0, NULL, 'Clementoni', '', NULL, '', '', '', '', 25, 26, '/Clementoni', '5e6f6b42a1ba30.87661671.png', 0, NULL, NULL, 0, ''),
(5, NULL, 0, 0, '2019-12-04', 0, NULL, 'B-kids', '', NULL, '', '', '', '', 27, 28, '/B-kids', '5e6f6b4b224407.44207211.png', 0, NULL, NULL, 0, ''),
(6, NULL, 0, 0, '2019-12-04', 0, NULL, 'GONZO', '', NULL, '', '', '', '', 29, 30, '/GONZO', '5e6f6b549c3458.84049747.png', 0, NULL, NULL, 0, ''),
(7, NULL, 0, 0, '2019-12-04', 0, NULL, 'Maty podłogowe', '', NULL, '', '', '', '', 31, 32, '/Maty podłogowe', '5e6f6b5f577ec0.01935433.png', 0, NULL, NULL, 0, ''),
(8, NULL, 0, 0, '2019-12-04', 0, NULL, 'Zegary ścienne', '', NULL, '', '', '', '', 33, 34, '/Zegary ścienne', '5e6f6b6a00fa28.53709473.png', 0, NULL, NULL, 0, ''),
(9, NULL, 0, 0, '2019-12-04', 0, NULL, 'Baseny', '', NULL, '', '', '', '', 35, 36, '/Baseny', '5e6f6b731e0c19.34829113.png', 0, NULL, NULL, 0, ''),
(10, NULL, 0, 0, '2019-12-04', 0, NULL, 'Zabawki ogrodowe', '', NULL, '', '', '', '', 37, 38, '/Zabawki ogrodowe', '5e6f6b83b76780.70296198.png', 0, NULL, NULL, 0, ''),
(11, NULL, 0, 0, '2019-12-04', 0, NULL, 'Materace', '', NULL, '', '', '', '', 39, 40, '/Materace', '5e6f6b8e43c9d2.23634576.png', 0, NULL, NULL, 0, ''),
(12, NULL, 0, 0, '2019-12-04', 0, NULL, 'Naklejki ścienne', '', NULL, '', '', '', '', 41, 42, '/Naklejki ścienne', '5e6f6b9824eff3.01867314.png', 0, NULL, NULL, 0, ''),
(13, 3, 0, 0, '2019-12-05', 0, NULL, 'Licencja Disney\'a', '', NULL, '', '', '', '', 6, 7, '/Zabawki/Licencja Disney\'a', '5e6f6a67181472.01024604.png', 0, NULL, NULL, 0, ''),
(14, 3, 0, 0, '2019-12-05', 0, NULL, 'Akcesoria dla dzieci', '', NULL, '', '', '', '', 8, 9, '/Zabawki/Akcesoria dla dzieci', '5e6f6a6f8ddbd7.76192442.png', 0, NULL, NULL, 0, ''),
(15, 3, 0, 0, '2019-12-05', 0, NULL, 'Dla niemowląt', '', NULL, '', '', '', '', 10, 11, '/Zabawki/Dla niemowląt', '5e6f6a783bd0f4.57335094.png', 0, NULL, NULL, 0, ''),
(16, 3, 0, 0, '2019-12-05', 0, NULL, 'Do kąpieli', '', NULL, '', '', '', '', 12, 17, '/Zabawki/Do kąpieli', '5e6f6a805df7e3.67277776.png', 0, NULL, NULL, 0, ''),
(17, 3, 0, 0, '2019-12-05', 0, NULL, 'Zestawy do sprzątania', '', NULL, '', '', '', '', 18, 19, '/Zabawki/Zestawy do sprzątania', '5e6f6a884b4164.02875476.png', 0, NULL, NULL, 0, ''),
(18, 3, 0, 0, '2019-12-05', 0, NULL, 'Drobne AGD', '', NULL, '', '', '', '', 20, 21, '/Zabawki/Drobne AGD', '5e6f6a91d47ef5.15778556.png', 0, NULL, NULL, 0, ''),
(19, 3, 0, 0, '2019-12-05', 0, NULL, 'Gry', '', NULL, '', '', '', '', 22, 23, '/Zabawki/Gry', '5e6f6a9a6e3783.26215197.png', 0, NULL, NULL, 0, ''),
(20, 16, 0, 0, '2020-01-27', 0, NULL, 'gumowe', '', NULL, '', '', '', '', 13, 14, '/Zabawki/Do kąpieli/gumowe', '', 0, NULL, NULL, 0, ''),
(21, 16, 0, 0, '2020-01-27', 0, NULL, 'statki', '', NULL, '', '', '', '', 15, 16, '/Zabawki/Do kąpieli/statki', '', 0, NULL, NULL, 0, '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kategoria_i18n`
--

CREATE TABLE `kategoria_i18n` (
  `id` int(11) NOT NULL,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `kategoria_i18n`
--

INSERT INTO `kategoria_i18n` (`id`, `locale`, `model`, `foreign_key`, `field`, `content`) VALUES
(1, 'pl_PL', 'Kategoria', 1, 'nazwa', 'Klocki Blocki'),
(2, 'pl_PL', 'Kategoria', 1, 'naglowek', ''),
(3, 'pl_PL', 'Kategoria', 1, 'seo_tekst', ''),
(4, 'pl_PL', 'Kategoria', 1, 'seo_tytul', ''),
(5, 'pl_PL', 'Kategoria', 1, 'seo_slowa', ''),
(6, 'pl_PL', 'Kategoria', 1, 'seo_opis', ''),
(7, 'pl_PL', 'Kategoria', 2, 'nazwa', 'Zabawki drewniane'),
(8, 'pl_PL', 'Kategoria', 2, 'naglowek', ''),
(9, 'pl_PL', 'Kategoria', 2, 'seo_tekst', ''),
(10, 'pl_PL', 'Kategoria', 2, 'seo_tytul', ''),
(11, 'pl_PL', 'Kategoria', 2, 'seo_slowa', ''),
(12, 'pl_PL', 'Kategoria', 2, 'seo_opis', ''),
(13, 'pl_PL', 'Kategoria', 3, 'nazwa', 'Zabawki'),
(14, 'pl_PL', 'Kategoria', 3, 'naglowek', ''),
(15, 'pl_PL', 'Kategoria', 3, 'seo_tekst', ''),
(16, 'pl_PL', 'Kategoria', 3, 'seo_tytul', ''),
(17, 'pl_PL', 'Kategoria', 3, 'seo_slowa', ''),
(18, 'pl_PL', 'Kategoria', 3, 'seo_opis', ''),
(19, 'pl_PL', 'Kategoria', 4, 'nazwa', 'Clementoni'),
(20, 'pl_PL', 'Kategoria', 4, 'naglowek', ''),
(21, 'pl_PL', 'Kategoria', 4, 'seo_tekst', ''),
(22, 'pl_PL', 'Kategoria', 4, 'seo_tytul', ''),
(23, 'pl_PL', 'Kategoria', 4, 'seo_slowa', ''),
(24, 'pl_PL', 'Kategoria', 4, 'seo_opis', ''),
(25, 'pl_PL', 'Kategoria', 5, 'nazwa', 'B-kids'),
(26, 'pl_PL', 'Kategoria', 5, 'naglowek', ''),
(27, 'pl_PL', 'Kategoria', 5, 'seo_tekst', ''),
(28, 'pl_PL', 'Kategoria', 5, 'seo_tytul', ''),
(29, 'pl_PL', 'Kategoria', 5, 'seo_slowa', ''),
(30, 'pl_PL', 'Kategoria', 5, 'seo_opis', ''),
(31, 'pl_PL', 'Kategoria', 6, 'nazwa', 'GONZO'),
(32, 'pl_PL', 'Kategoria', 6, 'naglowek', ''),
(33, 'pl_PL', 'Kategoria', 6, 'seo_tekst', ''),
(34, 'pl_PL', 'Kategoria', 6, 'seo_tytul', ''),
(35, 'pl_PL', 'Kategoria', 6, 'seo_slowa', ''),
(36, 'pl_PL', 'Kategoria', 6, 'seo_opis', ''),
(37, 'pl_PL', 'Kategoria', 7, 'nazwa', 'Maty podłogowe'),
(38, 'pl_PL', 'Kategoria', 7, 'naglowek', ''),
(39, 'pl_PL', 'Kategoria', 7, 'seo_tekst', ''),
(40, 'pl_PL', 'Kategoria', 7, 'seo_tytul', ''),
(41, 'pl_PL', 'Kategoria', 7, 'seo_slowa', ''),
(42, 'pl_PL', 'Kategoria', 7, 'seo_opis', ''),
(43, 'pl_PL', 'Kategoria', 8, 'nazwa', 'Zegary ścienne'),
(44, 'pl_PL', 'Kategoria', 8, 'naglowek', ''),
(45, 'pl_PL', 'Kategoria', 8, 'seo_tekst', ''),
(46, 'pl_PL', 'Kategoria', 8, 'seo_tytul', ''),
(47, 'pl_PL', 'Kategoria', 8, 'seo_slowa', ''),
(48, 'pl_PL', 'Kategoria', 8, 'seo_opis', ''),
(49, 'pl_PL', 'Kategoria', 9, 'nazwa', 'Baseny'),
(50, 'pl_PL', 'Kategoria', 9, 'naglowek', ''),
(51, 'pl_PL', 'Kategoria', 9, 'seo_tekst', ''),
(52, 'pl_PL', 'Kategoria', 9, 'seo_tytul', ''),
(53, 'pl_PL', 'Kategoria', 9, 'seo_slowa', ''),
(54, 'pl_PL', 'Kategoria', 9, 'seo_opis', ''),
(55, 'pl_PL', 'Kategoria', 10, 'nazwa', 'Zabawki ogrodowe'),
(56, 'pl_PL', 'Kategoria', 10, 'naglowek', ''),
(57, 'pl_PL', 'Kategoria', 10, 'seo_tekst', ''),
(58, 'pl_PL', 'Kategoria', 10, 'seo_tytul', ''),
(59, 'pl_PL', 'Kategoria', 10, 'seo_slowa', ''),
(60, 'pl_PL', 'Kategoria', 10, 'seo_opis', ''),
(61, 'pl_PL', 'Kategoria', 11, 'nazwa', 'Materace'),
(62, 'pl_PL', 'Kategoria', 11, 'naglowek', ''),
(63, 'pl_PL', 'Kategoria', 11, 'seo_tekst', ''),
(64, 'pl_PL', 'Kategoria', 11, 'seo_tytul', ''),
(65, 'pl_PL', 'Kategoria', 11, 'seo_slowa', ''),
(66, 'pl_PL', 'Kategoria', 11, 'seo_opis', ''),
(67, 'pl_PL', 'Kategoria', 12, 'nazwa', 'Naklejki ścienne'),
(68, 'pl_PL', 'Kategoria', 12, 'naglowek', ''),
(69, 'pl_PL', 'Kategoria', 12, 'seo_tekst', ''),
(70, 'pl_PL', 'Kategoria', 12, 'seo_tytul', ''),
(71, 'pl_PL', 'Kategoria', 12, 'seo_slowa', ''),
(72, 'pl_PL', 'Kategoria', 12, 'seo_opis', ''),
(73, 'pl_PL', 'Kategoria', 13, 'nazwa', 'Licencja Disney\'a'),
(74, 'pl_PL', 'Kategoria', 13, 'naglowek', ''),
(75, 'pl_PL', 'Kategoria', 13, 'seo_tekst', ''),
(76, 'pl_PL', 'Kategoria', 13, 'seo_tytul', ''),
(77, 'pl_PL', 'Kategoria', 13, 'seo_slowa', ''),
(78, 'pl_PL', 'Kategoria', 13, 'seo_opis', ''),
(79, 'pl_PL', 'Kategoria', 14, 'nazwa', 'Akcesoria dla dzieci'),
(80, 'pl_PL', 'Kategoria', 14, 'naglowek', ''),
(81, 'pl_PL', 'Kategoria', 14, 'seo_tekst', ''),
(82, 'pl_PL', 'Kategoria', 14, 'seo_tytul', ''),
(83, 'pl_PL', 'Kategoria', 14, 'seo_slowa', ''),
(84, 'pl_PL', 'Kategoria', 14, 'seo_opis', ''),
(85, 'pl_PL', 'Kategoria', 15, 'nazwa', 'Dla niemowląt'),
(86, 'pl_PL', 'Kategoria', 15, 'naglowek', ''),
(87, 'pl_PL', 'Kategoria', 15, 'seo_tekst', ''),
(88, 'pl_PL', 'Kategoria', 15, 'seo_tytul', ''),
(89, 'pl_PL', 'Kategoria', 15, 'seo_slowa', ''),
(90, 'pl_PL', 'Kategoria', 15, 'seo_opis', ''),
(91, 'pl_PL', 'Kategoria', 16, 'nazwa', 'Do kąpieli'),
(92, 'pl_PL', 'Kategoria', 16, 'naglowek', ''),
(93, 'pl_PL', 'Kategoria', 16, 'seo_tekst', ''),
(94, 'pl_PL', 'Kategoria', 16, 'seo_tytul', ''),
(95, 'pl_PL', 'Kategoria', 16, 'seo_slowa', ''),
(96, 'pl_PL', 'Kategoria', 16, 'seo_opis', ''),
(97, 'pl_PL', 'Kategoria', 17, 'nazwa', 'Zestawy do sprzątania'),
(98, 'pl_PL', 'Kategoria', 17, 'naglowek', ''),
(99, 'pl_PL', 'Kategoria', 17, 'seo_tekst', ''),
(100, 'pl_PL', 'Kategoria', 17, 'seo_tytul', ''),
(101, 'pl_PL', 'Kategoria', 17, 'seo_slowa', ''),
(102, 'pl_PL', 'Kategoria', 17, 'seo_opis', ''),
(103, 'pl_PL', 'Kategoria', 18, 'nazwa', 'Drobne AGD'),
(104, 'pl_PL', 'Kategoria', 18, 'naglowek', ''),
(105, 'pl_PL', 'Kategoria', 18, 'seo_tekst', ''),
(106, 'pl_PL', 'Kategoria', 18, 'seo_tytul', ''),
(107, 'pl_PL', 'Kategoria', 18, 'seo_slowa', ''),
(108, 'pl_PL', 'Kategoria', 18, 'seo_opis', ''),
(109, 'pl_PL', 'Kategoria', 19, 'nazwa', 'Gry'),
(110, 'pl_PL', 'Kategoria', 19, 'naglowek', ''),
(111, 'pl_PL', 'Kategoria', 19, 'seo_tekst', ''),
(112, 'pl_PL', 'Kategoria', 19, 'seo_tytul', ''),
(113, 'pl_PL', 'Kategoria', 19, 'seo_slowa', ''),
(114, 'pl_PL', 'Kategoria', 19, 'seo_opis', ''),
(115, 'pl_PL', 'Kategoria', 20, 'nazwa', 'gumowe'),
(116, 'pl_PL', 'Kategoria', 20, 'naglowek', ''),
(117, 'pl_PL', 'Kategoria', 20, 'seo_tekst', ''),
(118, 'pl_PL', 'Kategoria', 20, 'seo_tytul', ''),
(119, 'pl_PL', 'Kategoria', 20, 'seo_slowa', ''),
(120, 'pl_PL', 'Kategoria', 20, 'seo_opis', ''),
(121, 'pl_PL', 'Kategoria', 21, 'nazwa', 'statki'),
(122, 'pl_PL', 'Kategoria', 21, 'naglowek', ''),
(123, 'pl_PL', 'Kategoria', 21, 'seo_tekst', ''),
(124, 'pl_PL', 'Kategoria', 21, 'seo_tytul', ''),
(125, 'pl_PL', 'Kategoria', 21, 'seo_slowa', ''),
(126, 'pl_PL', 'Kategoria', 21, 'seo_opis', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kategoria_map`
--

CREATE TABLE `kategoria_map` (
  `id` int(11) NOT NULL,
  `kategoria_id` int(11) NOT NULL,
  `hurtownia` varchar(30) NOT NULL,
  `nazwa` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kody_rabatowe`
--

CREATE TABLE `kody_rabatowe` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(50) NOT NULL,
  `kod` varchar(10) NOT NULL,
  `rabat` float NOT NULL DEFAULT '0',
  `data_start` datetime DEFAULT NULL,
  `data_end` datetime DEFAULT NULL,
  `zablokuj` tinyint(1) DEFAULT '0',
  `data_dodania` datetime DEFAULT NULL,
  `typ` enum('kwota','procent') DEFAULT 'kwota',
  `zamowienie_id` int(11) DEFAULT NULL,
  `rodzaj` varchar(30) DEFAULT 'ogolny',
  `opis` text,
  `min_order` float DEFAULT NULL,
  `klient_typ` enum('b2c','b2b') NOT NULL DEFAULT 'b2c',
  `multi` tinyint(1) DEFAULT '0' COMMENT '1 - może łączyć się z innymi kodami, 0 - nie może łączyć się z innymi kodami'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `kody_rabatowe`
--

INSERT INTO `kody_rabatowe` (`id`, `nazwa`, `kod`, `rabat`, `data_start`, `data_end`, `zablokuj`, `data_dodania`, `typ`, `zamowienie_id`, `rodzaj`, `opis`, `min_order`, `klient_typ`, `multi`) VALUES
(14, 'dla producenta test', 'GRPNDP', 15, '2018-12-20 03:06:00', '2020-03-31 03:06:00', 0, '2018-12-21 03:07:30', 'procent', NULL, 'producent', '', NULL, 'b2b', 0),
(15, 'Rabat kwota', 'UENITR', 15, '2018-12-21 03:48:00', '2020-03-25 03:48:00', 0, '2018-12-21 03:48:30', 'kwota', NULL, 'kategoria', '', NULL, 'b2c', 0),
(16, 'Rabat na zabawki', '9LG8M3', 5, '2020-02-07 08:40:00', '2020-02-29 08:40:00', 0, '2020-02-07 08:42:23', 'procent', NULL, 'kategoria', 'Wszystkie zabawki, rabat dla klientów hurtowych', 1500, 'b2b', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kody_rabatowe_kategoria`
--

CREATE TABLE `kody_rabatowe_kategoria` (
  `id` int(11) NOT NULL,
  `kategoria_id` int(11) NOT NULL,
  `kody_rabatowe_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `kody_rabatowe_kategoria`
--

INSERT INTO `kody_rabatowe_kategoria` (`id`, `kategoria_id`, `kody_rabatowe_id`) VALUES
(1, 14, 15),
(2, 15, 15),
(3, 18, 15),
(4, 3, 16),
(5, 13, 16),
(6, 14, 16),
(7, 15, 16),
(8, 16, 16),
(9, 17, 16),
(10, 18, 16),
(11, 19, 16),
(12, 20, 16),
(13, 21, 16);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kody_rabatowe_producent`
--

CREATE TABLE `kody_rabatowe_producent` (
  `id` int(11) NOT NULL,
  `producent_id` int(11) NOT NULL,
  `kody_rabatowe_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `kody_rabatowe_producent`
--

INSERT INTO `kody_rabatowe_producent` (`id`, `producent_id`, `kody_rabatowe_id`) VALUES
(1, 1, 14);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kolor`
--

CREATE TABLE `kolor` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(100) DEFAULT NULL,
  `hex` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `konfiguracja`
--

CREATE TABLE `konfiguracja` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(150) NOT NULL,
  `wartosc` text,
  `typ` varchar(100) NOT NULL DEFAULT 'mail',
  `rodzaj` enum('text','textarea','select','select_multiple','checkbox','email','number','password','radio') NOT NULL DEFAULT 'text',
  `html` tinyint(1) DEFAULT '0',
  `label` varchar(100) DEFAULT NULL,
  `options` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `konfiguracja`
--

INSERT INTO `konfiguracja` (`id`, `nazwa`, `wartosc`, `typ`, `rodzaj`, `html`, `label`, `options`) VALUES
(1, 'nazwa_serwisu', 'Demo V3', 'admin', 'text', 0, 'Nazwa serwisu', NULL),
(2, 'znak_wodny_polozenie', 'right_bottom', 'zdjecia', 'select', 0, 'Położenie znaku wodnego', 'left_top=Lewy górny róg|right_top=Prawy górny róg|right_bottom=Prawy dolny róg|left_bottom=Lewy dolny róg|center=Środek'),
(3, 'rodzaj', 'brutto', 'ceny', 'select', 0, 'Rodzaj wyświetlanych cen', 'netto=netto|brutto=brutto'),
(5, 'host', 'projekt-samatix.pl', 'smtp', 'text', 0, 'Serwer smtp - host', NULL),
(6, 'port', '587', 'smtp', 'text', 0, 'Serwer smtp - port', NULL),
(7, 'username', 'test@projekt-samatix.pl', 'smtp', 'text', 0, 'Serwer smtp - login', NULL),
(8, 'password', 'Qwerty1234', 'smtp', 'password', 0, 'Serwer smtp - hasło', NULL),
(9, 'tls', '0', 'smtp', 'checkbox', 0, 'Serwer smtp - TLS', NULL),
(10, 'domyslny_email', 'test@projekt-samatix.pl', 'mail', 'email', 0, 'Domyślny email', NULL),
(14, 'title', 'Demo V3', 'seo', 'text', 0, 'SEO - Tytuł strony', NULL),
(15, 'nazwa_firmy', 'Demo V3', 'dane', 'text', 0, 'Nazwa firmy', NULL),
(16, 'telefon', '+48 434 232 232', 'dane', 'text', 0, 'Telefon', NULL),
(17, 'dane_firmy', 'Demo V3', 'dane', 'textarea', 0, 'Dane firmy', NULL),
(18, 'title_prefix', NULL, 'seo', 'text', 0, 'Seo title prefix', NULL),
(19, 'title_sufix', NULL, 'seo', 'text', 0, 'Seo title sufix', NULL),
(20, 'mapa', '', 'dane', 'textarea', 0, 'Mapa na stronie kontakt', NULL),
(21, 'token', 'iopae54ytrgd67A22q', 'cron', 'text', 0, 'Token pozwalający na uruchomienie crona', NULL),
(22, 'token', 'oetfge4rds43lP87', 'newsletter', 'text', 0, 'Token umożliwiający wysyłkę newslettera', NULL),
(23, 'facebook', '#fb', 'social', 'text', 0, 'Link do strony facebook', NULL),
(27, 'limit', '100', 'newsletter', 'number', 0, 'Limit adresów e-mail do jednorazowej wysyłki.', NULL),
(28, 'head', '', 'script', 'textarea', 0, 'Skrypty w sekcji HEAD', NULL),
(29, 'body', '', 'script', 'textarea', 0, 'Skrypty w sekcji BODY', NULL),
(30, 'znak_wodny', '1', 'zdjecia', 'checkbox', 0, 'Wyświetlaj znak wodny w galerii', NULL),
(31, 'telefon_2', '+48 000 000 111', 'dane', 'text', 0, 'Telefon 2', NULL),
(32, 'default_country', 'PL', 'adres', 'text', 0, 'Domyślny kraj', NULL),
(34, 'rodzaj_import', 'brutto', 'ceny', 'select', 0, 'Rodzaj importowanych cen', 'netto=netto|brutto=brutto'),
(35, 'vat_import', '23', 'ceny', 'number', 0, 'Stawka VAT importowanych cen', ''),
(37, 'layoutSchemaColors', '{\"[@color-1]\":\"#2dca71\",\"[@color-2]\":\"#000000\",\"[@color-3]\":\"#218838\"}', 'admin', 'text', 0, 'Schemat kolorów strony', NULL),
(39, 'punkty_stacjonarne', '#', 'links', 'text', 0, 'Link do strony z punktami stacjonarnymi', NULL),
(40, 'infoZwrot', '#', 'links', 'text', 0, 'Link do strony z informacją o zwrotach', NULL),
(41, 'infoPreorder', '#', 'links', 'text', 0, 'Link do strony z informacją o preorderach', NULL),
(42, 'etykietyPromocji', 'promocja,wyprzedaz,produkt_tygodnia', 'ceny', 'select_multiple', 0, 'Etykiety które uwzględniają cenę promocyjną', 'promocja=Promocja|wyprzedaz=Wyprzedaż|nowosc=Nowość|polecany=Polecany|wyrozniony=Bestseller|produkt_tygodnia=Hit tygodnia'),
(43, 'showBadge', '0', 'hot_deal', 'checkbox', 0, 'Pokazuj etykietę hot deal na produktach.', NULL),
(44, 'zwrotInfo', '#', 'links', 'text', 0, 'Link do strony z informacją o zwrocie kupionych gier', NULL),
(45, 'block_days', '15-08|11-11|24-12|25-12|26-12|01-01|06-01|01-11|01-05|02-05|03-05|03-01|02-01|31-12', 'dostawa', 'text', 0, 'Dni wykluczone z dostawy', NULL),
(46, 'maxTime', '14:00', 'dostawa', 'text', 0, 'Godzina do której są wysyłane przesyłki', NULL),
(47, 'kartaPodarunkowa', '/pages/view/5/karty-podarunkowe', 'links', 'text', 0, 'Link do strony z informacją o kartach podarunkowych', NULL),
(48, 'krajPodstawowy', 'PL', 'wysylka', 'select', 0, 'Podstawowy kraj wysyłki', NULL),
(49, 'krajKosztDodatkowy', '50', 'wysylka', 'number', 0, 'Koszt dodatkowy dla innych krajów niż podstawowy', NULL),
(50, 'CompanyOrName', 'Sklep Internetowy \"Demo V3\"', 'ups', 'text', 0, NULL, NULL),
(51, 'Address1', 'test', 'ups', 'text', 0, NULL, NULL),
(52, 'CountryTerritory', 'PL', 'ups', 'text', 0, NULL, NULL),
(53, 'PostalCode', '75210', 'ups', 'text', 0, NULL, NULL),
(54, 'CityOrTown', 'test', 'ups', 'text', 0, NULL, NULL),
(55, 'StateProvinceCounty', 'Wielkopolskie', 'ups', 'text', 0, NULL, NULL),
(56, 'Telephone', '123321123', 'ups', 'text', 0, NULL, NULL),
(57, 'UpsAccountNumber', 'XXXXXX', 'ups', 'text', 0, NULL, NULL),
(58, 'ReceiverUpsAccountNumber', '', 'ups', 'text', 0, NULL, NULL),
(59, 'email', 'biuro@example.pl', 'ups', 'email', 0, 'Adres e-mail', NULL),
(60, 'ProfileName', 'Demo V3', 'ups', 'text', 0, 'Profile name', NULL),
(61, 'poczta_xml_nadawca_nazwa', '', 'poczta', 'text', 0, 'Nazwa nadawcy', NULL),
(62, 'poczta_xml_nadawca_nazwa_skrucona', '', 'poczta', 'text', 0, 'Skrucona nazwa nadawcy', NULL),
(63, 'poczta_xml_nadawca_ulica', '', 'poczta', 'text', 0, 'Nadawca ulica', NULL),
(64, 'poczta_xml_nadawca_dom', '', 'poczta', 'text', 0, 'Nadawca nr domu', NULL),
(65, 'poczta_xml_nadawca_lokal', '', 'poczta', 'text', 0, 'Nadawca nr lokalu', NULL),
(66, 'poczta_xml_nadawca_miasto', '', 'poczta', 'text', 0, 'Nadawca miasto', NULL),
(67, 'poczta_xml_nadawca_kod', '', 'poczta', 'text', 0, 'Nadawca kod', NULL),
(68, 'poczta_xml_nadawca_nip', '', 'poczta', 'text', 0, 'Nadawca nip', NULL),
(69, 'poczta_xml_nadawca_guid', '', 'poczta', 'text', 0, 'Nadawca guid', NULL),
(70, 'allow_kraj', '1', 'dpd', 'checkbox', 0, 'Uwzględniaj kraj w pliku DPD', NULL),
(71, 'email_nadawca', 'biuro@example.pl', 'dhl', 'text', 0, NULL, NULL),
(72, 'username', 'KONRAD871030', 'dhl', 'text', 0, 'Nazwa użytkownika webApi', NULL),
(73, 'password', 'cYrbW,U38dl7IXp', 'dhl', 'password', 0, 'Hasło użytkownika webApi', NULL),
(74, 'accountNumber', '6000000', 'dhl', 'text', 0, 'Identyfikator klienta', NULL),
(75, 'test', '1', 'dhl', 'checkbox', 0, 'Tryb testowy', NULL),
(76, 'shipperName', 'Ogródmacieja', 'dhl', 'text', 0, 'Nazwa nadawcy', NULL),
(77, 'shipperPostalCode', '61619', 'dhl', 'text', 0, 'Kod pocztowy nadawcy (bez - np. 61200)', NULL),
(78, 'shipperCity', 'Poznań', 'dhl', 'text', 0, 'Miasto nadawcy', NULL),
(79, 'shipperStreet', 'Testowa', 'dhl', 'text', 0, 'Ulica nadawcy', NULL),
(80, 'shipperHouseNumber', '12', 'dhl', 'text', 0, 'Numer domu nadawcy', NULL),
(81, 'shipperApartmentNumber', '', 'dhl', 'text', 0, 'Numer lokalu nadawcy', NULL),
(82, 'shipperCountry', 'PL', 'dhl', 'text', 0, 'Kraj nadawcy (kod 2 cyfrowy np. PL)', NULL),
(83, 'shipperContactPerson', '', 'dhl', 'text', 0, 'Osoba kontaktowa (Imię i Nazwisko)', NULL),
(84, 'shipperContactPhone', '', 'dhl', 'text', 0, 'Telefon do osoby kontaktowej', NULL),
(85, 'shipperContactEmail', '', 'dhl', 'text', 0, 'Email do osoby kontaktowej', NULL),
(86, 'labelType', 'LP', 'dhl', 'text', 0, 'Typ etykiety (LP, BLP - laserowa, ZBLP - termiczna)', NULL),
(87, 'numer_referencyjny', '', 'inpost', 'text', 0, 'Numer referencyjny inpost', NULL),
(88, 'link_sledzenia', 'https://sprawdz.dhl.com.pl/szukaj.aspx?m=0&sn=', 'dhl', 'text', 0, 'Link do śledzenia przeslyki', NULL),
(89, 'link_sledzenia', 'https://inpost.pl/sledzenie-przesylek?number=', 'inpost', 'text', 0, 'Link do śledzenia przeslyki', NULL),
(90, 'link_sledzenia', 'http://sledzenie.poczta-polska.pl/?numer=', 'poczta', 'text', 0, 'Link do śledzenia przeslyki', NULL),
(91, 'link_sledzenia', 'http://wwwapps.ups.com/WebTracking/processInputRequest?loc=pl_PL&tracknum=@', 'ups', 'text', 0, 'Link do śledzenia przeslyki', NULL),
(92, 'link_sledzenia', 'https://tracktrace.dpd.com.pl/parcelDetails?typ=1&p1=', 'dpd', 'text', 0, 'Link do śledzenia przeslyki', NULL),
(93, 'zamowienieZgoda', '', 'script', 'textarea', 0, 'Skrypty po złożeniu zamówienia który wyświetli po zaznaczeniu pierwszej zgody', NULL),
(94, 'zamowienieZgoda', '', 'script', 'textarea', 0, 'Skrypty po złożeniu zamówienia który wyświetli po zaznaczeniu drugiej zgody', NULL),
(95, 'after_order', '', 'script', 'textarea', 0, 'Ogólny skrypt po złożeniu zamówienia, wyświetla się bez względu na zgody.', NULL),
(96, 'title', '', 'seoHome', 'text', 0, 'Tytuł strony głównej', NULL),
(97, 'keywords', '', 'seoHome', 'text', 0, 'Słowa kluczowe strony głównej', NULL),
(98, 'description', '', 'seoHome', 'text', 0, 'Opis strony głównej', NULL),
(99, 'title', '', 'seoLogin', 'text', 0, 'Tytuł strony logowania i rejestracji', NULL),
(100, 'keywords', '', 'seoLogin', 'text', 0, 'Słowa kluczowe strony logowania i rejestracji', NULL),
(101, 'description', '', 'seoLogin', 'text', 0, 'Opis strony logowania i rejestracji', NULL),
(102, 'title', '', 'seoPassword', 'text', 0, 'Tytuł strony restartu hasła', NULL),
(103, 'keywords', '', 'seoPassword', 'text', 0, 'Słowa kluczowe strony restartu hasła', NULL),
(104, 'description', '', 'seoPassword', 'text', 0, 'Opis strony restartu hasła', NULL),
(105, 'stopka_dane', '<div class=\"footer-head\">Nazwa firmy</div>\r\n<p>ul. ulica 151<br>\r\n  21-700 Miasto<br>\r\n  NIP: 6258645445<br>\r\n  tel. + 48 111 222 333<br>\r\n  Infolinia: 222 333 444<br>\r\n  e-mail: sklep@example.pl</p>', 'dane', 'textarea', 1, 'Dane firmy w stopce', NULL),
(106, 'telefon2', '+48 343 232 234', 'dane', 'text', 0, 'Drugi telefon', NULL),
(107, 'email', 'biuro@example.pl', 'dane', 'text', 0, 'Email kontaktowy', NULL),
(108, 'homePromo1', '#', 'links', 'text', 0, 'Link na stronie głównej w menu obok banera - pozycja 1', NULL),
(109, 'homePromo2', '#', 'links', 'text', 0, 'Link na stronie głównej w menu obok banera - pozycja 2', NULL),
(110, 'homePromo3', '#', 'links', 'text', 0, 'Link na stronie głównej w menu obok banera - pozycja 3', NULL),
(111, 'wartosc_min', '100', 'gratis', 'number', 0, 'Minimalna wartość zamówienia aby otrzymać gratis', NULL),
(112, 'limit', '500', 'wysylka', 'number', 0, 'Minimalna wartość zamówienia dla darmowej wysyłki', NULL),
(113, 'appId', '2168791899846908', 'facebook', 'text', 0, 'ID aplikacji facebook', NULL),
(114, 'miesjce_wystawienia', 'Wrocław', 'subiekt', 'text', 0, 'Miejsce wystawienia faktur', NULL),
(115, 'id_programu', 'Subiekt GT', 'subiekt', 'text', 0, 'Identyfikator programu', NULL),
(116, 'id_nadawcy', 'GT', 'subiekt', 'text', 0, 'Identyfikator nadawcy', NULL),
(117, 'nazwa_krotka', 'Demo V3', 'subiekt', 'text', 0, 'Nazwa skrócona firmy', NULL),
(118, 'nazwa_pelna', 'Firma przykładowa systemu InsERT GT', 'subiekt', 'text', 0, 'Pełna nazwa firmy', NULL),
(119, 'miasto', 'Wrocław', 'subiekt', 'text', 0, 'Miasto', NULL),
(120, 'kod_pocztowy', '54-445', 'subiekt', 'text', 0, 'Kod pocztowy', NULL),
(121, 'adres', 'Bławatkowa 25/3', 'subiekt', 'text', 0, 'Adres', NULL),
(122, 'nip', '111-111-11-11', 'subiekt', 'text', 0, 'NIP', NULL),
(123, 'nip_ue', '0', 'subiekt', 'text', 0, 'NIP UE', NULL),
(124, 'vat_ue', '0', 'subiekt', 'checkbox', 0, 'Zarejestrowany jako VAT UE', NULL),
(125, 'stawkaVat', '23', 'wysylka', 'number', 0, 'Stawka VAT kosztów wysyłki', NULL),
(126, 'podtytul_fv', 'Zamówienie nr: {numer}', 'subiekt', 'text', 0, 'Podtytuł zamówienia do subiekta zmienna {numer}', NULL),
(127, 'platnoscGotowka', 'Gotówka', 'faktura', 'text', 0, 'Nazwa płatności gotówkowej na fakturze', NULL),
(128, 'platnoscKarta', 'Karta płatnicza', 'faktura', 'text', 0, 'Nazwa płatności kartą na fakturze', NULL),
(129, 'platnoscPrzelew', 'Przelew', 'faktura', 'text', 0, 'Nazwa płatności przelewem na fakturze', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `konfiguracja_i18n`
--

CREATE TABLE `konfiguracja_i18n` (
  `id` int(11) NOT NULL,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `koszyk`
--

CREATE TABLE `koszyk` (
  `id` int(11) NOT NULL,
  `session_id` varchar(128) NOT NULL,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `data` datetime NOT NULL,
  `last_update` datetime DEFAULT NULL,
  `zamowienie_id` int(11) DEFAULT NULL,
  `aktywny` tinyint(1) DEFAULT '1',
  `domena` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `koszyk`
--

INSERT INTO `koszyk` (`id`, `session_id`, `uzytkownik_id`, `data`, `last_update`, `zamowienie_id`, `aktywny`, `domena`) VALUES
(21, '88ee1ab545414d1593a06bcb1fa3541c', NULL, '2020-04-20 16:24:49', '2020-04-20 16:25:26', NULL, 1, 'demo-v3.projekt-samatix.pl'),
(22, '3197e293adf76d0bcfc4f9ca6c3ec1a3', NULL, '2020-04-22 20:53:26', '2020-04-22 20:53:26', NULL, 1, 'demo-v3.projekt-samatix.pl');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `koszyk_towar`
--

CREATE TABLE `koszyk_towar` (
  `id` int(11) NOT NULL,
  `koszyk_id` int(11) NOT NULL,
  `towar_id` int(11) NOT NULL,
  `koszyk_key` varchar(150) DEFAULT NULL,
  `dodany` tinyint(4) DEFAULT '1',
  `usuniety` tinyint(4) DEFAULT '0',
  `data_dodania` datetime DEFAULT NULL,
  `data_usuniecia` datetime DEFAULT NULL,
  `ilosc` int(11) DEFAULT NULL,
  `cena` float DEFAULT NULL,
  `notatki` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `koszyk_towar`
--

INSERT INTO `koszyk_towar` (`id`, `koszyk_id`, `towar_id`, `koszyk_key`, `dodany`, `usuniety`, `data_dodania`, `data_usuniecia`, `ilosc`, `cena`, `notatki`) VALUES
(60, 21, 9, '9', 1, 0, '2020-04-20 16:24:49', NULL, 1, 130, NULL),
(61, 21, 1, 'z_5e0876b9688e6_t_1', 1, 0, '2020-04-20 16:25:26', NULL, 1, 93.6, NULL),
(62, 21, 3, 'z_5e0876b9688e6_t_3', 1, 0, '2020-04-20 16:25:26', NULL, 1, 101.4, NULL),
(63, 22, 1, '1', 1, 0, '2020-04-22 20:53:25', NULL, 1, 100, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kupon`
--

CREATE TABLE `kupon` (
  `id` int(11) NOT NULL,
  `sklep_id` int(11) DEFAULT NULL,
  `nazwa` varchar(150) NOT NULL,
  `text` text NOT NULL,
  `success` text NOT NULL,
  `error` text NOT NULL,
  `licznik` int(11) DEFAULT NULL,
  `typ` enum('text','transport','rabat') NOT NULL DEFAULT 'text',
  `od` int(11) DEFAULT NULL,
  `kod` varchar(10) NOT NULL,
  `aktywny` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `jezyk_id` int(11) DEFAULT NULL,
  `nazwa` varchar(100) NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(160) DEFAULT NULL,
  `target` varchar(7) DEFAULT '_self',
  `ukryty` tinyint(1) DEFAULT '0',
  `kolejnosc` int(11) DEFAULT '0',
  `typ` varchar(15) DEFAULT 'top'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `menu`
--

INSERT INTO `menu` (`id`, `parent_id`, `jezyk_id`, `nazwa`, `url`, `title`, `target`, `ukryty`, `kolejnosc`, `typ`) VALUES
(2, NULL, NULL, 'Kategorie', '#', '', '_self', 0, 0, 'top'),
(3, NULL, NULL, 'Bestsellery', '#', '', '_self', 0, 0, 'top'),
(4, NULL, NULL, 'Nowości', '#', '', '_self', 0, 0, 'top'),
(5, NULL, NULL, 'Promocje', '#', '', '_self', 0, 0, 'top'),
(6, NULL, NULL, 'Polecane', '#4', '', '_self', 0, 0, 'top'),
(7, NULL, 1, 'Aktualności', '#5', '', '_self', 0, 0, 'top'),
(8, NULL, NULL, 'Dlaczego my?', '#6', '', '_self', 0, 0, 'top'),
(9, NULL, NULL, 'Zwroty i reklamacje', '#6', '', '_self', 0, 0, 'top'),
(10, NULL, NULL, 'Kontakt', '/kontakt', '', '_self', 0, 0, 'top');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `menu_i18n`
--

CREATE TABLE `menu_i18n` (
  `id` int(11) NOT NULL,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `newsletter`
--

CREATE TABLE `newsletter` (
  `id` int(11) NOT NULL,
  `temat` varchar(100) NOT NULL,
  `tresc` text NOT NULL,
  `wyslany` tinyint(1) DEFAULT '0',
  `data` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `typ` int(11) DEFAULT '0' COMMENT '0 - zwykły, 1 - do klientów b2b, 2 - rodo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `newsletter_adres`
--

CREATE TABLE `newsletter_adres` (
  `id` int(11) NOT NULL,
  `email` varchar(120) NOT NULL,
  `potwierdzony` tinyint(1) DEFAULT '0',
  `token` varchar(32) DEFAULT NULL,
  `data` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `wyslany` tinyint(2) DEFAULT '0',
  `status` tinyint(2) DEFAULT '0',
  `locale` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `newsletter_adres`
--

INSERT INTO `newsletter_adres` (`id`, `email`, `potwierdzony`, `token`, `data`, `wyslany`, `status`, `locale`) VALUES
(1, 'test2@projekt-samatix.pl', 0, 'b641607813dcd588e4fa32e0b3f66718', '2019-09-09 18:19:28', 0, 0, 'pl_PL');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `okazje`
--

CREATE TABLE `okazje` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(50) NOT NULL,
  `kolejnosc` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `okazje`
--

INSERT INTO `okazje` (`id`, `nazwa`, `kolejnosc`) VALUES
(1, 'Komunia', 0),
(2, 'Urodziny', 0),
(3, 'Chrzest', 0),
(4, 'Pod choinkę', 0),
(5, 'Mikołajki', 0),
(6, 'Baby Shower', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `okazje_towar`
--

CREATE TABLE `okazje_towar` (
  `id` int(11) NOT NULL,
  `okazje_id` int(11) NOT NULL,
  `towar_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `okazje_towar`
--

INSERT INTO `okazje_towar` (`id`, `okazje_id`, `towar_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 4, 1),
(4, 5, 1),
(5, 2, 3),
(6, 3, 3),
(7, 6, 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `opakowanie`
--

CREATE TABLE `opakowanie` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `opakowanie`
--

INSERT INTO `opakowanie` (`id`, `nazwa`) VALUES
(1, 'oryginalne / producenta'),
(2, 'oryginalne'),
(3, 'oryginalne / uszkodzone');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `opinia`
--

CREATE TABLE `opinia` (
  `id` int(11) NOT NULL,
  `tresc` text NOT NULL,
  `podpis` varchar(50) NOT NULL,
  `ukryta` tinyint(1) DEFAULT NULL,
  `kolejnosc` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `opinia`
--

INSERT INTO `opinia` (`id`, `tresc`, `podpis`, `ukryta`, `kolejnosc`) VALUES
(1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sem odio, finibus id mattis in, efficitur mollis ex. Donec magna libero, euismod non interdum at, euismod vitae ligula. Nulla facilisi. Sed et porta dui, ut vestibulum odio. Nunc gravida est ac mi eleifend lacinia. Sed interdum condimentum magna, ac pulvinar neque dignissim id. Ut ligula felis, pretium et neque quis, aliquet gravida odio. \r\n\r\nPraesent consectetur velit sit amet nunc sodales, sed interdum nisl venenatis. Donec ornare leo vitae felis egestas, at mollis odio commodo. Donec vel laoreet elit, id vestibulum massa. ', 'Katarzyna Wienstal', 0, 0),
(2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sem odio, finibus id mattis in, efficitur mollis ex. Donec magna libero, euismod non interdum at, euismod vitae ligula. Nulla facilisi. Sed et porta dui, ut vestibulum odio. Nunc gravida est ac mi eleifend lacinia. Sed interdum condimentum magna, ac pulvinar neque dignissim id. Ut ligula felis, pretium et neque quis, aliquet gravida odio. Praesent consectetur velit sit amet nunc sodales, sed interdum nisl venenatis. Donec ornare leo vitae felis egestas, at mollis odio commodo. Donec vel laoreet elit, id vestibulum massa. Sed pretium porta est, id tristique justo viverra nec. Suspendisse in massa mollis tortor semper hendrerit. Ut mattis faucibus ex, a rhoncus nisi feugiat eget. Nunc nec rutrum urna.', 'Jan Kowalski', 0, 0),
(3, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sem odio, finibus id mattis in, efficitur mollis ex. Donec magna libero, euismod non interdum at, euismod vitae ligula. Nulla facilisi. Sed et porta dui, ut vestibulum odio. Nunc gravida est ac mi eleifend lacinia. Sed interdum condimentum magna, ac pulvinar neque dignissim id. Ut ligula felis, pretium et neque quis, aliquet gravida odio. Praesent consectetur velit sit amet nunc sodales, sed interdum nisl venenatis. Donec ornare leo vitae felis egestas, at mollis odio commodo. Donec vel laoreet elit, id vestibulum massa. Sed pretium porta est, id tristique justo viverra nec. Suspendisse in massa mollis tortor semper hendrerit. Ut mattis faucibus ex, a rhoncus nisi feugiat eget. Nunc nec rutrum urna.', 'Grzegorz Malinowski', 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `platforma`
--

CREATE TABLE `platforma` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(50) NOT NULL,
  `logo` varchar(128) DEFAULT NULL,
  `logo_2` varchar(128) DEFAULT NULL,
  `preorder` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `platforma`
--

INSERT INTO `platforma` (`id`, `nazwa`, `logo`, `logo_2`, `preorder`) VALUES
(1, 'Playstation 4', '5c0188dc29f126.24217969.png', '5c02c54d79b4d3.02101456.png', 1),
(2, 'Xbox One', '5c018920415a92.95767510.png', NULL, 1),
(3, 'PC', '5c018a4e09c0d3.23663335.png', NULL, 0),
(4, 'NINTENDO SWITCH', '5c018de3d6ba57.26373532.png', NULL, 0),
(5, 'NINTENDO 3DS', '5c018ecadca3a5.08830387.png', NULL, 0),
(6, 'Playstation 3', '5c018f523fd683.22091109.png', NULL, 0),
(7, 'Xbox 360', '5c018fd3445342.59497240.png', NULL, 0),
(8, 'Playstation Vita', '5c01902c1d09d1.44369985.png', NULL, 0),
(9, 'WiiU', '5c019065ed1f25.79888492.png', NULL, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `platnosc`
--

CREATE TABLE `platnosc` (
  `id` int(11) NOT NULL,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `zamowienie_id` int(11) NOT NULL,
  `rodzaj_platnosci_id` int(11) NOT NULL,
  `waluta_id` int(11) NOT NULL,
  `kwota` float NOT NULL,
  `opis` varchar(200) DEFAULT NULL,
  `opis_lng` varchar(200) DEFAULT NULL,
  `token` varchar(32) NOT NULL,
  `token2` varchar(32) NOT NULL,
  `status` enum('wykonana','odrzucona','rozpoczeta','oczekuje') NOT NULL DEFAULT 'oczekuje',
  `status2` int(11) DEFAULT NULL,
  `blad` tinytext,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_rozpoczecia` timestamp NULL DEFAULT NULL,
  `doplata` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `powiadomienia`
--

CREATE TABLE `powiadomienia` (
  `id` int(11) NOT NULL,
  `typ` varchar(50) NOT NULL,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `imie` varchar(150) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `zgoda` tinyint(4) DEFAULT '0',
  `towar_id` int(11) DEFAULT NULL,
  `cena` float DEFAULT NULL,
  `data_dodania` datetime NOT NULL,
  `wyslane` tinyint(1) DEFAULT '0',
  `data_wyslania` datetime DEFAULT NULL,
  `tresc` text,
  `punkt_odbioru` int(11) DEFAULT NULL,
  `odpowiedz` text,
  `dostepny` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `producent`
--

CREATE TABLE `producent` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(100) NOT NULL,
  `ukryty` tinyint(4) NOT NULL DEFAULT '0',
  `kolejnosc` int(11) NOT NULL DEFAULT '0',
  `logo` varchar(255) DEFAULT NULL,
  `na_zamowienie` int(11) DEFAULT NULL,
  `gwarancja` text,
  `www` varchar(200) DEFAULT NULL,
  `naglowek` text,
  `nazwa_ikonka` varchar(100) DEFAULT NULL,
  `nazwa_centralazabawek` varchar(100) DEFAULT NULL,
  `nazwa_gatito` varchar(100) DEFAULT NULL,
  `nazwa_molos` varchar(100) DEFAULT NULL,
  `nazwa_ptakmodahurt` varchar(100) DEFAULT NULL,
  `nazwa_vmp` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `producent`
--

INSERT INTO `producent` (`id`, `nazwa`, `ukryty`, `kolejnosc`, `logo`, `na_zamowienie`, `gwarancja`, `www`, `naglowek`, `nazwa_ikonka`, `nazwa_centralazabawek`, `nazwa_gatito`, `nazwa_molos`, `nazwa_ptakmodahurt`, `nazwa_vmp`) VALUES
(1, 'test', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Lego', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'testowy 2', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'Testowy 3', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `producent_i18n`
--

CREATE TABLE `producent_i18n` (
  `id` int(11) NOT NULL,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `punkty_odbioru`
--

CREATE TABLE `punkty_odbioru` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(100) NOT NULL,
  `adres` tinytext,
  `mapa` varchar(1024) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `telefon` varchar(40) DEFAULT NULL,
  `info` text,
  `aktywny` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `punkty_odbioru_i18n`
--

CREATE TABLE `punkty_odbioru_i18n` (
  `id` int(11) NOT NULL,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `reklamacja`
--

CREATE TABLE `reklamacja` (
  `id` int(11) NOT NULL,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `cdn_id` varchar(50) DEFAULT NULL,
  `numer_reklamacji` varchar(30) DEFAULT NULL,
  `zamowienie_numer_cdn` varchar(50) NOT NULL,
  `nr_faktury` varchar(30) DEFAULT NULL,
  `data_zlozenia` datetime NOT NULL,
  `data_przyjecia` datetime DEFAULT NULL,
  `status` varchar(15) NOT NULL,
  `uwagi` text,
  `notatki` text,
  `tresc` text,
  `odpowiedz` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `reklamacja_pliki`
--

CREATE TABLE `reklamacja_pliki` (
  `id` int(11) NOT NULL,
  `reklamacja_id` int(11) NOT NULL,
  `plik` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `reklamacja_towar`
--

CREATE TABLE `reklamacja_towar` (
  `id` int(11) NOT NULL,
  `reklamacja_id` int(11) NOT NULL,
  `towar_id` int(11) DEFAULT NULL,
  `opis_wady` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rodo`
--

CREATE TABLE `rodo` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `token` varchar(128) DEFAULT NULL,
  `data_zatwierdzenia` datetime DEFAULT NULL,
  `zgoda_1` tinyint(4) DEFAULT '0',
  `zgoda_2` tinyint(4) DEFAULT '0',
  `zgoda_3` tinyint(4) DEFAULT '0',
  `zgoda_4` tinyint(4) DEFAULT '0',
  `zgoda_5` tinyint(4) DEFAULT '0',
  `zgoda_6` tinyint(4) DEFAULT '0',
  `zgoda_7` tinyint(4) DEFAULT '0',
  `zgoda_8` tinyint(4) DEFAULT '0',
  `telefon` varchar(30) DEFAULT NULL,
  `wyslany` tinyint(4) DEFAULT '0',
  `data_wyslania` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rodzaj_platnosci`
--

CREATE TABLE `rodzaj_platnosci` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(50) NOT NULL,
  `nazwa_wyswietlana` varchar(50) NOT NULL,
  `platnosc_elektroniczna` tinyint(1) NOT NULL DEFAULT '0',
  `pobranie` tinyint(1) NOT NULL DEFAULT '0',
  `opis` text,
  `konfig1` varchar(255) DEFAULT NULL,
  `konfig2` varchar(255) DEFAULT NULL,
  `konfig3` varchar(255) DEFAULT NULL,
  `konfig4` varchar(255) DEFAULT NULL,
  `waluta` varchar(50) NOT NULL,
  `aktywna` tinyint(1) NOT NULL DEFAULT '1',
  `koszt_dodatkowy` tinyint(4) NOT NULL DEFAULT '0',
  `prowizja` float(5,2) DEFAULT NULL,
  `darmowa_wysylka` float(8,2) DEFAULT '0.00',
  `ikona` varchar(50) DEFAULT NULL,
  `kolejnosc` int(11) DEFAULT '0',
  `test` tinyint(1) DEFAULT '0',
  `auto_submit` tinyint(1) DEFAULT '0',
  `disabled_kraj` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `rodzaj_platnosci`
--

INSERT INTO `rodzaj_platnosci` (`id`, `nazwa`, `nazwa_wyswietlana`, `platnosc_elektroniczna`, `pobranie`, `opis`, `konfig1`, `konfig2`, `konfig3`, `konfig4`, `waluta`, `aktywna`, `koszt_dodatkowy`, `prowizja`, `darmowa_wysylka`, `ikona`, `kolejnosc`, `test`, `auto_submit`, `disabled_kraj`) VALUES
(1, 'Płatność za pobraniem', 'Przy odbiorze', 0, 1, '', NULL, NULL, NULL, NULL, ';1;2;3;4;', 1, 1, NULL, 0.00, 'monety.png', 5, 0, 0, 1),
(2, 'Przelew tradycyjny', 'Przedpłata - przelew na konto', 0, 0, '', '', '', '', NULL, ';1;2;3;4;', 1, 0, NULL, NULL, 'bank.png', 6, 0, 0, 0),
(3, 'Przelew on-line (dotpay)', 'Dotpay', 1, 0, 'Informacje dodatkowe o płatności za pomocą Dotpay.', '', '', NULL, NULL, ';1;2;3;4;', 0, 0, NULL, 0.00, 'dotpay.png', 7, 0, 0, 0),
(4, 'Przelewy24', 'Przelewy24', 1, 0, '', '', '1', NULL, NULL, ';1;', 0, 0, NULL, 0.00, NULL, 3, 0, 0, 0),
(5, 'PayU', 'PayU', 1, 0, NULL, NULL, NULL, NULL, NULL, ';1;', 1, 0, 0.00, 0.00, 'payu.png', 2, 1, 0, 0),
(6, 'PayPal', 'PayPal', 1, 0, 'Opis paypal', '', '', '0', NULL, ';1;2;3;4;', 0, 0, NULL, 0.00, NULL, 1, 0, 0, 0),
(7, 'Płatność w punkcie odbioru', 'Płatność w punkcie odbioru', 0, 0, NULL, NULL, NULL, NULL, NULL, '', 1, 1, NULL, 0.00, 'dlon.png', 4, 0, 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rodzaj_platnosci_i18n`
--

CREATE TABLE `rodzaj_platnosci_i18n` (
  `id` int(11) NOT NULL,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `rodzaj_platnosci_i18n`
--

INSERT INTO `rodzaj_platnosci_i18n` (`id`, `locale`, `model`, `foreign_key`, `field`, `content`) VALUES
(1, 'pl_PL', 'RodzajPlatnosci', 3, 'nazwa', 'Przelew on-line (dotpay)'),
(2, 'pl_PL', 'RodzajPlatnosci', 4, 'nazwa', 'Przelewy24'),
(3, 'pl_PL', 'RodzajPlatnosci', 5, 'nazwa', 'PayU'),
(4, 'pl_PL', 'RodzajPlatnosci', 6, 'nazwa', 'PayPal'),
(5, 'pl_PL', 'RodzajPlatnosci', 2, 'nazwa', 'Przelew tradycyjny'),
(6, 'pl_PL', 'Wysylka', 1, 'nazwa', 'Wysyłka 48h'),
(7, 'pl_PL', 'Wysylka', 1, 'opis', 'Paczki wysyłane kurierem DPD'),
(8, 'pl_PL', 'Wysylka', 2, 'nazwa', 'Odbiór osobisty'),
(9, 'pl_PL', 'Wysylka', 2, 'opis', ''),
(10, 'pl_PL', 'RodzajPlatnosci', 1, 'nazwa', 'Płatność za pobraniem'),
(11, 'pl_PL', 'RodzajPlatnosci', 7, 'nazwa', 'Płatność w punkcie odbioru'),
(12, 'pl_PL', 'Wysylka', 3, 'nazwa', 'Paczkomat Inpost'),
(13, 'pl_PL', 'Wysylka', 3, 'opis', ''),
(20, 'pl_PL', 'Wysylka', 2, 'komunikat', '<p><span style=\"color: rgb(255, 0, 0);\"><span style=\"font-weight: bold; font-size: 18px;\">Uwaga!</span><br><span style=\"font-size: 14px;\">W przypadku odbioru osobistego musisz poczekać na informacje czy zamówienie jest przygotowane do odbioru. Informacja zostanie wysłana na podany w zamówieniu adres email lub przekazana telefonicznie.</span></span></p>'),
(21, 'pl_PL', 'Wysylka', 4, 'nazwa', 'E - wysyłka'),
(22, 'pl_PL', 'Wysylka', 4, 'opis', ''),
(23, 'pl_PL', 'Wysylka', 4, 'komunikat', '<p style=\"text-align: center; \"><span style=\"color: rgb(255, 0, 0); font-weight: bold;\">Karty podarunkowe wysyłane są w formie PDF na podany w zamówieniu adres email.</span></p>'),
(27, 'pl_PL', 'Wysylka', 1, 'komunikat', ''),
(28, 'pl_PL', 'Wysylka', 3, 'komunikat', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `rola` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `roles`
--

INSERT INTO `roles` (`id`, `rola`, `alias`) VALUES
(1, 'admin', 'Administrator');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `routing`
--

CREATE TABLE `routing` (
  `id` int(11) NOT NULL,
  `controller` varchar(50) NOT NULL,
  `action` varchar(50) NOT NULL,
  `pass` varchar(150) DEFAULT NULL,
  `domena` varchar(150) NOT NULL DEFAULT 'pl',
  `maska` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rozmiar`
--

CREATE TABLE `rozmiar` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(50) NOT NULL,
  `kolejnosc` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `schowek`
--

CREATE TABLE `schowek` (
  `id` int(11) NOT NULL,
  `uzytkownik_id` int(11) NOT NULL,
  `towar_id` int(11) NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `statystyki`
--

CREATE TABLE `statystyki` (
  `id` int(11) NOT NULL,
  `session_id` varchar(255) NOT NULL,
  `start` datetime DEFAULT NULL,
  `last` datetime DEFAULT NULL,
  `agent` varchar(255) DEFAULT NULL,
  `pages_count` int(11) DEFAULT '0',
  `pages` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `statystyki`
--

INSERT INTO `statystyki` (`id`, `session_id`, `start`, `last`, `agent`, `pages_count`, `pages`) VALUES
(106, '88ee1ab545414d1593a06bcb1fa3541c', '2020-04-20 16:14:09', '2020-04-20 16:25:31', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', 5, '/ | /kontakt | /towar/view/9/tytul-tego-przedmiotu-o-dlugosci-jaka-przedstawia-ten-wpis | /towar/view/1/tytul-tego-przedmiotu-o-dlugosci-jaka-przedstawia-ten-wpis | /cart'),
(107, 'eebf603ffa93466739fb14205ebd8cd1', '2020-04-20 16:14:23', '2020-04-20 16:14:23', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(108, '682e750b2d1a4bd1b00e7d8f2a5e5e0d', '2020-04-20 16:14:23', '2020-04-20 16:14:23', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(109, '7a2a83c633e3d7a7c5b30880c781bf90', '2020-04-20 16:14:23', '2020-04-20 16:14:23', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(110, 'ded3f145a30053b52fcfec78409eec7e', '2020-04-20 16:14:23', '2020-04-20 16:14:23', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(111, '61997dbd17f2b81ee393896c075fb4f8', '2020-04-20 16:14:24', '2020-04-20 16:14:24', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(112, '23be48ae6ec827339e4631fd4d1c1445', '2020-04-20 16:14:24', '2020-04-20 16:14:24', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(113, 'b1732296e3ce2822e5079544a910a343', '2020-04-20 16:14:24', '2020-04-20 16:14:24', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(114, 'e41821160fa10e775eb3b4df9146518d', '2020-04-20 16:14:24', '2020-04-20 16:14:24', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(115, '5ccc29d5d97468261cd11a23ba19d6a5', '2020-04-20 16:14:37', '2020-04-20 16:14:37', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu,-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(116, '61637ab180e587ac96950caf1b247686', '2020-04-20 16:14:38', '2020-04-20 16:14:38', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu,-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(117, 'c5e8036a07f29ba4ed6c3c901462dd4a', '2020-04-20 16:14:38', '2020-04-20 16:14:38', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu,-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(118, '5ad78d63025957328f8b496a3bf089f2', '2020-04-20 16:14:39', '2020-04-20 16:14:39', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu,-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(119, '03c7d14d77d169eb534e31647b724e7d', '2020-04-20 16:24:39', '2020-04-20 16:24:39', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(120, 'c30f8415377ccf62154d5ba27e9f29bf', '2020-04-20 16:24:39', '2020-04-20 16:24:39', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(121, 'f81597ca771705afbbebc70092c529bd', '2020-04-20 16:24:39', '2020-04-20 16:24:39', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(122, 'db54b7cf97dffb38e1aafbfc86357625', '2020-04-20 16:24:39', '2020-04-20 16:24:39', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(123, '91416804c2b4372e4886db1a0432ef75', '2020-04-20 18:28:18', '2020-04-20 18:28:18', 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E; InfoPath.2)', 1, '/'),
(124, '1a18e35c9f2af159726f26a16a0a1af6', '2020-04-20 18:28:26', '2020-04-20 18:28:26', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(125, '6fa261f9781512ddf20cd327bc54e1ad', '2020-04-20 18:28:27', '2020-04-20 18:28:27', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(126, '020a8e40e808bb17cf7d3e09007e71de', '2020-04-20 18:28:27', '2020-04-20 18:28:27', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(127, '5d4e0a245e17042ae4fc771742261210', '2020-04-20 18:28:27', '2020-04-20 18:28:27', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(128, 'a80c19e0d780d625b8ce6f0bdd00e3bc', '2020-04-20 21:06:22', '2020-04-20 21:06:22', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(129, 'e48b76ecb7ca7f8d6b4d91a6d4a796c9', '2020-04-20 21:06:22', '2020-04-20 21:06:22', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(130, 'f330b572250023d563436dd434c6332e', '2020-04-20 21:06:22', '2020-04-20 21:06:22', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(131, '1aceb729c6092f2f6393c7c576bd5cf3', '2020-04-20 21:06:22', '2020-04-20 21:06:22', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(132, '1c591e83b523741f921ddc4a60ce7858', '2020-04-20 23:07:25', '2020-04-20 23:07:25', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(133, 'e293b326ed7de1d24c8a409d8cc1a24f', '2020-04-20 23:07:25', '2020-04-20 23:07:25', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(134, '181cbe41b82befcf9fc161aeca3f8c98', '2020-04-20 23:07:25', '2020-04-20 23:07:25', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(135, '783fe76d80547ba3512f30c0d56a1e89', '2020-04-20 23:07:26', '2020-04-20 23:07:26', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(136, '16562b657c6fa704be5cedff7fe38660', '2020-04-21 01:43:26', '2020-04-21 01:43:26', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(137, '555d860709e5703b367e29c8a1f638c9', '2020-04-21 01:43:27', '2020-04-21 01:43:27', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(138, 'af4261273631ef608b0f1777e113496e', '2020-04-21 01:43:27', '2020-04-21 01:43:27', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(139, '443339f47bea5217581b12dd594164e6', '2020-04-21 01:43:27', '2020-04-21 01:43:27', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(140, '0a5a9470a0858f44afb0288d4590dc7e', '2020-04-21 01:44:48', '2020-04-21 01:44:48', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0', 1, '/'),
(141, 'ad0f522cfe7844daba6956c77ab54ebb', '2020-04-21 03:52:52', '2020-04-21 03:52:52', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(142, '8c3e7f6ef3bbb4c96608d1e1ec88638e', '2020-04-21 03:52:52', '2020-04-21 03:52:52', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(143, '3722a58c66257e9a95150ec46617583e', '2020-04-21 03:52:52', '2020-04-21 03:52:52', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(144, '6c6f91b395a0453f049f1ea5527ec321', '2020-04-21 03:52:56', '2020-04-21 03:52:56', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(145, '0cec67e7f7ca5abc8436362a76fa82a3', '2020-04-21 07:31:31', '2020-04-21 07:31:31', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(146, '49d1a44ec4a560541dc307e93f08fc6e', '2020-04-21 07:31:31', '2020-04-21 07:31:31', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(147, 'ce86d8918134f6f71368e122c0f0cf8f', '2020-04-21 07:31:31', '2020-04-21 07:31:31', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(148, 'd7482347bf694d838f23987427892b97', '2020-04-21 07:31:31', '2020-04-21 07:31:31', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(149, 'ed75aae3648fc426e08801b03aacba3b', '2020-04-21 09:40:14', '2020-04-21 09:40:14', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(150, '9b8090a874924bd026679a8908012763', '2020-04-21 09:40:15', '2020-04-21 09:40:15', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(151, '28f0e11b29ea112747ccedd6d4115439', '2020-04-21 10:54:15', '2020-04-21 10:54:15', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(152, '62b9fd27982d819da2ab8250aa0d923e', '2020-04-21 10:54:15', '2020-04-21 10:54:15', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(153, 'b4d40fc54680bf35804204122b366462', '2020-04-21 10:54:15', '2020-04-21 10:54:15', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(154, '09324ae52416a9ba301d9716b5027290', '2020-04-21 11:43:51', '2020-04-21 11:43:51', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(155, 'eea412bfde4c6824451b0f620fa2142c', '2020-04-21 11:44:39', '2020-04-21 11:44:39', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(156, 'b542c969e5bfe666c77ac68a5a60ed8e', '2020-04-21 12:54:43', '2020-04-21 12:54:43', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(157, '15a500b381d523a3f58c410ef4e1bee2', '2020-04-21 12:54:43', '2020-04-21 12:54:43', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(158, '4018afe8256800e6c7aeb8e9dbc9864d', '2020-04-21 12:54:43', '2020-04-21 12:54:43', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(159, '2162a696d614cf75922b8f66052e81af', '2020-04-21 13:46:50', '2020-04-21 13:46:50', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(160, '1cbf0a0127c84a7efc8f81464f50d27c', '2020-04-21 15:23:30', '2020-04-21 15:23:30', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(161, '5e4871c0b96eb3c41b91a688533c5f0d', '2020-04-21 15:26:44', '2020-04-21 15:26:44', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(162, '32f4c04fb3ef8df8b0e3302d1ac27364', '2020-04-21 15:26:44', '2020-04-21 15:26:44', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(163, '6e7c47b24b0f69a14b9f009b8a3d27c1', '2020-04-21 15:57:53', '2020-04-21 15:57:53', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(164, 'e93b449ff593e6ac2c18df911662d419', '2020-04-21 17:24:14', '2020-04-21 17:24:14', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(165, '5cc7244e0e9e24fd7dafabad175c61f6', '2020-04-21 17:49:49', '2020-04-21 17:49:49', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(166, '2c30b4f2c208ddc6fc3a99b7774c0b34', '2020-04-21 17:49:50', '2020-04-21 17:49:50', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(167, '775962dbc2303ca188e11361253c926f', '2020-04-21 17:58:41', '2020-04-21 17:58:41', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(168, '5fcd612068f3b6194742687c0f09a121', '2020-04-21 19:26:01', '2020-04-21 19:26:01', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(169, '261cfee93116b74eb8b356c67709d2b8', '2020-04-21 20:41:06', '2020-04-21 20:41:06', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(170, '1e78a4534ed8304b7f9298080a89f11d', '2020-04-21 20:41:06', '2020-04-21 20:41:06', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(171, 'a18e8d910717b0506588e794a15a2aeb', '2020-04-21 20:41:06', '2020-04-21 20:41:06', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(172, 'f452a2705312c9e95224cf719e054902', '2020-04-21 21:38:22', '2020-04-21 21:38:22', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(173, '44064db597858bef57dcf441a1e3767f', '2020-04-21 22:59:55', '2020-04-21 22:59:55', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(174, '740bc90f290531154294c94edcb4d118', '2020-04-21 22:59:55', '2020-04-21 22:59:55', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(175, '5a958d5c83add6408a8e9d1fd5202c66', '2020-04-21 22:59:56', '2020-04-21 22:59:56', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(176, '98b99f71750eb1989405a2c02c6b493f', '2020-04-22 01:58:44', '2020-04-22 01:58:44', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(177, '90bc24a51a0efce38837fba4bd0c965d', '2020-04-22 01:58:44', '2020-04-22 01:58:44', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(178, '1d7cf887d4475175a04bac5f275dd316', '2020-04-22 01:58:44', '2020-04-22 01:58:44', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(179, '77e09915f9c4f719e8ff4a904827e80f', '2020-04-22 01:58:44', '2020-04-22 01:58:44', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(180, '57aaac36b45da7616902a1d41b4086ec', '2020-04-22 04:21:48', '2020-04-22 04:21:48', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(181, '1f5bdef59563c2ee6a1a49408f0b0402', '2020-04-22 04:21:48', '2020-04-22 04:21:48', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(182, '9371b4c0464c35e74524c30bc878d811', '2020-04-22 04:21:48', '2020-04-22 04:21:48', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(183, '3379f598de171ca5efa24601ceaa937a', '2020-04-22 04:21:48', '2020-04-22 04:21:48', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(184, '4e9d80394d4cfca4950d9d74bbb8d5a2', '2020-04-22 08:22:41', '2020-04-22 08:22:41', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(185, '2a0cc376cbf4b1444b17d4a9f1469b80', '2020-04-22 08:22:41', '2020-04-22 08:22:41', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(186, '0a316d25111b6b47c0223d2c82bd20a2', '2020-04-22 08:22:41', '2020-04-22 08:22:41', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(187, '8e93614569aa423ab95f302029c3d74a', '2020-04-22 08:22:41', '2020-04-22 08:22:41', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(188, 'd5e5dedd9c25e8d0e48549c763df2eda', '2020-04-22 10:40:46', '2020-04-22 10:40:46', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(189, 'fb846c9ac039ea871c15f08143021c0e', '2020-04-22 10:46:25', '2020-04-22 10:46:25', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(190, '7224743c821077528c90a262f301b11b', '2020-04-22 10:46:25', '2020-04-22 10:46:25', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(191, 'e1013d27f36c8e2a8d2b252eba8213ab', '2020-04-22 10:46:26', '2020-04-22 10:46:26', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(192, '6aa13e9084859f57ef6b4d50a8844f1e', '2020-04-22 10:46:26', '2020-04-22 10:46:26', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(193, 'a7a884259c41c5e917565c6905e385a5', '2020-04-22 10:46:29', '2020-04-22 10:46:29', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(194, '4685fe60e2a7ff558c2a3577ea90dfa6', '2020-04-22 10:46:29', '2020-04-22 10:46:29', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(195, 'fe9989dee876a89cc664ba9c21f1e13d', '2020-04-22 10:46:30', '2020-04-22 10:46:30', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(196, 'ce0b7fc8f1891b201ab9cbbc09e086b5', '2020-04-22 10:46:31', '2020-04-22 10:46:31', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(197, '6983e5028814166ad057a8e572261738', '2020-04-22 14:48:34', '2020-04-22 14:48:34', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(198, 'de3e5150d924f62a1fed25d75104a13e', '2020-04-22 14:48:34', '2020-04-22 14:48:34', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(199, 'f3098dfb3a5fabd37bc3564f1b628ffb', '2020-04-22 14:48:34', '2020-04-22 14:48:34', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(200, 'fdfcd560b251dc328fd0f20241355fbe', '2020-04-22 14:48:35', '2020-04-22 14:48:35', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(201, '6ef0c92292db9a6715247f97d23bcc1b', '2020-04-22 15:41:35', '2020-04-22 15:41:35', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', 1, '/'),
(202, 'dfadd46164655f45d01e14185a30ec0d', '2020-04-22 15:41:41', '2020-04-22 15:41:41', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(203, 'c1bfc6138917ec85079a28bea2539ed8', '2020-04-22 15:41:41', '2020-04-22 15:41:41', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(204, '785af2671bb14efe096cbf85e19c9256', '2020-04-22 15:41:41', '2020-04-22 15:41:41', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(205, '8c6f046a4795385829bf29fdcb928560', '2020-04-22 15:41:41', '2020-04-22 15:41:41', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(206, '3197e293adf76d0bcfc4f9ca6c3ec1a3', '2020-04-22 16:06:20', '2020-04-22 21:13:15', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0', 2, '/ | /cart'),
(207, '2957d96842737e8a83133336c3c0c36f', '2020-04-22 16:06:31', '2020-04-22 16:06:31', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(208, '7d9ca1c2e47c7e1629514c6084e4ce35', '2020-04-22 16:06:31', '2020-04-22 16:06:31', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(209, '344395f11b5680425c2a75567c863bf3', '2020-04-22 17:46:25', '2020-04-22 17:46:25', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(210, '355058ff888685ce8b4f2418e9aedd54', '2020-04-22 17:47:04', '2020-04-22 17:47:04', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(211, '6971170a99cb1644ff19b914dca35b2c', '2020-04-22 17:50:34', '2020-04-22 17:50:34', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(212, 'e0d4ad3629a8cc91d6e9df5244d61978', '2020-04-22 17:50:35', '2020-04-22 17:50:35', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(213, 'c3f28cd41823897bc08d8db3d4bcc319', '2020-04-22 17:51:15', '2020-04-22 17:51:15', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(214, '6064aae18fadb0a69afdddbb50036021', '2020-04-22 18:19:47', '2020-04-22 18:19:47', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(215, '830368321adfdead18aaf54dbdf9273f', '2020-04-22 18:19:47', '2020-04-22 18:19:47', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(216, '6nml0rb60m5o2na63pm6hqmvvj', '2020-04-22 19:24:23', '2020-04-22 19:25:12', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0', 2, '/ | /towar/view/3/tytul-tego-przedmiotu-o-dlugosci-jaka-przedstawia-ten-wpis'),
(217, '684e287fbe3af6d7f9be64dc762c7079', '2020-04-22 20:49:20', '2020-04-22 20:49:20', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(218, '59ac086f7753ef2ee7315272aff1f7da', '2020-04-22 20:49:20', '2020-04-22 20:49:20', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(219, 'a7f39a9c7e3ab96d1fe35f01ca2dcdef', '2020-04-22 20:49:20', '2020-04-22 20:49:20', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(220, '6134e7d5f63fdb7e428613cd6fd1b850', '2020-04-22 20:49:20', '2020-04-22 20:49:20', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(221, 'b81279bb10d00ba689f2f4e1b02bf7da', '2020-04-22 20:52:56', '2020-04-22 20:52:56', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(222, 'f3a3b8942a0d2a206224381d9277b080', '2020-04-22 20:52:56', '2020-04-22 20:52:56', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(223, '6f708018ddbe93fc672418a3f453f49d', '2020-04-22 20:52:56', '2020-04-22 20:52:56', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(224, 'd07e4ab95d29657f393d7b2e9cfd41b3', '2020-04-22 20:52:56', '2020-04-22 20:52:56', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(225, 'e75bd922d21d2668cd0f2ebca412e790', '2020-04-22 23:01:08', '2020-04-22 23:01:08', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(226, '5c85e8dcd8ac373259a8a2635d1b40fb', '2020-04-22 23:01:08', '2020-04-22 23:01:08', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(227, '3157677e73fe8b11b837d358f8810d75', '2020-04-22 23:01:08', '2020-04-22 23:01:08', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(228, '4f56cdb9d00b437a2066b1d59edc7ddf', '2020-04-22 23:01:08', '2020-04-22 23:01:08', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(229, '3c18b0946d392d18dc59f1a4b8973bf0', '2020-04-23 01:46:17', '2020-04-23 01:46:17', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(230, '9af385188f08ea0243cc21c749964d87', '2020-04-23 01:46:17', '2020-04-23 01:46:17', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(231, 'c71da880e88a04482781d627e976d478', '2020-04-23 01:46:27', '2020-04-23 01:46:27', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(232, '93b582d89904e67dbcceaea03d563011', '2020-04-23 01:46:27', '2020-04-23 01:46:27', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(233, '1c48c1f6758251e8e5df717504bde981', '2020-04-23 01:46:27', '2020-04-23 01:46:27', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(234, 'b3eb7acb7f20abe4c01ac3b546316a68', '2020-04-23 05:03:52', '2020-04-23 05:03:52', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(235, '1388b4c008d97ac3281796fd8ecab782', '2020-04-23 05:03:52', '2020-04-23 05:03:52', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(236, 'ddf4589fee32eabfbd85d8770f0fd71c', '2020-04-23 05:03:52', '2020-04-23 05:03:52', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(237, '5e180b1d1e4a250e65ce07eec81f5193', '2020-04-23 05:03:53', '2020-04-23 05:03:53', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(238, 'cfc58ca5ffdbe34be3963337f5c5e337', '2020-04-23 05:16:20', '2020-04-23 05:16:20', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(239, 'f560e8c0dbb589808b7002443896b68c', '2020-04-23 07:22:03', '2020-04-23 07:22:03', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(240, 'bc2bdad4fb54109633467c91bfcb8031', '2020-04-23 09:00:38', '2020-04-23 09:00:38', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/3/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(241, '0677de96952715c0ae636012a9d083aa', '2020-04-23 09:00:38', '2020-04-23 09:00:38', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/1/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(242, '676d8ec205b6948f2d5e9ef5a1f14c05', '2020-04-23 09:00:39', '2020-04-23 09:00:39', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/4/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora'),
(243, 'ddc8e5d38e31c37fef93ce3bcbcb824a', '2020-04-23 09:00:39', '2020-04-23 09:00:39', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 1, '/blog/artykul/2/tytul-tego-artykulu%2C-o-dowolnej-tresci-dodanej-przez-admina-i-autora');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `strona`
--

CREATE TABLE `strona` (
  `id` int(11) NOT NULL,
  `menu_dolne` tinyint(1) DEFAULT '0',
  `ukryta` tinyint(1) DEFAULT '0',
  `data` datetime DEFAULT NULL,
  `nazwa` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `tresc` longtext COLLATE utf8_polish_ci,
  `seo_tytul` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `seo_slowa` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `seo_opis` text COLLATE utf8_polish_ci,
  `allow_all` tinyint(1) DEFAULT '0',
  `type` varchar(50) COLLATE utf8_polish_ci DEFAULT 'type_default',
  `locale` varchar(8) COLLATE utf8_polish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `strona`
--

INSERT INTO `strona` (`id`, `menu_dolne`, `ukryta`, `data`, `nazwa`, `tresc`, `seo_tytul`, `seo_slowa`, `seo_opis`, `allow_all`, `type`, `locale`) VALUES
(1, 0, 0, NULL, 'Kontakt', '[%-_blok_4_-%]', '', '', '', 0, 'type_default', 'pl_PL'),
(2, 1, 0, NULL, 'O nas', '<div class=\"html-content\">\r\n    <div class=\"container\">\r\n  	<div class=\"row\">\r\n    	<div class=\"col-12 fs-24 fw-bold\">O nas</div>\r\n	</div>\r\n	<div class=\"row\">\r\n    	<div class=\"col-12\">\r\n      		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tincidunt vel mauris ut vestibulum. Suspendisse venenatis, sem nec ultricies blandit, lectus est semper lectus, id consequat leo lorem et leo. Curabitur ante elit, ultrices et lectus id, finibus imperdiet arcu. Maecenas semper metus neque, suscipit rhoncus mi venenatis at. Aliquam erat volutpat. Suspendisse at magna et ligula sollicitudin efficitur sit amet ut sem. Pellentesque sollicitudin mi ac lorem molestie, vel porta magna vulputate. Vestibulum tempus, urna vel posuere maximus, leo est consequat mauris, placerat auctor metus elit a lacus.</p>\r\n    	</div>\r\n	</div>\r\n	</div>\r\n</div>', '', '', '', 0, 'type_default', 'pl_PL'),
(3, 0, 0, NULL, 'Regulamin', '<div class=\"html-content\"><p style=\"text-align: center; \">\r\n<b>REGULAMIN\r\nSKLEPU INTERNETOWEGO</b></p><p style=\"text-align: center;\"><b>www.ecopitoriono.pl</b><br></p><p style=\"text-align: center;\">\r\n<br>\r\n</p><p style=\"text-align: center;\">\r\n<b>§\r\n1</b></p><p style=\"text-align: center;\">\r\n<b>POSTANOWIENIA\r\nOGÓLNE</b></p><ol>\r\n	<li><p>\r\n	Sklep\r\n	www.ecopitoriono.pl działa na zasadach określonych w niniejszym\r\n	Regulaminie.</p>\r\n	</li><li><p>\r\n	Regulamin\r\n	określa warunki zawierania i rozwiązywania Umów Sprzedaży\r\n	Produktu oraz tryb postępowania reklamacyjnego, a także rodzaje i\r\n	zakres usług świadczonych drogą elektroniczną przez Sklep\r\n	www.ecopitoriono.pl, zasady świadczenia tych usług, warunki zawierania\r\n	i rozwiązywania umów o świadczenie usług drogą elektroniczną.</p>\r\n	</li><li><p>\r\n	Każdy\r\n	Usługobiorca z chwilą podjęcia czynności zmierzających do\r\n	korzystania z Usług Elektronicznych Sklepu www.ecopitoriono.pl\r\n	zobowiązany jest do przestrzegania postanowień niniejszego\r\n	Regulaminu.</p>\r\n	</li><li><p>\r\n	W\r\n	sprawach nieuregulowanych w niniejszym Regulaminie mają\r\n	zastosowanie przepisy:</p>\r\n	<ol>\r\n		<li><p>\r\n		Ustawy\r\n		o świadczeniu usług drogą elektroniczną z dnia 18 lipca 2002 r.\r\n		(Dz. U. Nr 144, poz. 1204 ze zm.), \r\n		</p>\r\n		</li><li><p>\r\n		Ustawy\r\n		o prawach konsumenta z dnia 30 maja 2014 r. (Dz. U. 2014 poz. 827),</p>\r\n		</li><li><p>\r\n		Ustawy\r\n		<span>o pozasądowym rozwiązywaniu sporów\r\n		konsumenckich</span><span> z dnia 23 września 2016 r.\r\n		(</span><span>Dz.U. 2016 poz. 1823),</span></p>\r\n		</li><li><p>\r\n		Ustawy\r\n		Kodeks cywilny z dnia 23 kwietnia 1964 r. (Dz. U. Nr 16, poz. 93 ze\r\n		zm.) oraz inne właściwe przepisy prawa polskiego.</p>\r\n	</li></ol>\r\n</li></ol><p><br>\r\n</p><p style=\"text-align: center;\">\r\n<b>§\r\n2</b></p><p style=\"text-align: center;\">\r\n<b>DEFINICJE\r\nZAWARTE W REGULAMINIE</b></p><ol>\r\n	<li><p>\r\n	\r\n	<span>\r\n	– formularz dostępny na stronie internetowej www.ecopitoriono.pl\r\n	umożliwiający utworzenie Konta.</span></p>\r\n	</li><li><p>\r\n	\r\n	<span>\r\n	– formularz dostępny na stronie internetowej www.ecopitoriono.pl\r\n	umożliwiający złożenie Zamówienia.</span></p>\r\n	</li><li><p>\r\n	\r\n	– Usługobiorca będący Konsumentem, który zamierza zawrzeć lub\r\n	zawarł Umowę Sprzedaży ze Sprzedawcą.</p>\r\n	</li><li><p>\r\n	\r\n	– osoba fizyczna, która dokonuje z przedsiębiorcą czynności\r\n	prawnej niezwiązanej bezpośrednio z jej działalnością\r\n	gospodarczą lub zawodową.</p>\r\n	</li><li><p>\r\n	\r\n	– oznaczony indywidualną nazwą (loginem) oraz hasłem, zbiór\r\n	zasobów w systemie teleinformatycznym Usługodawcy, w którym\r\n	gromadzone są dane Usługobiorcy w tym informacje o złożonych\r\n	Zamówieniach.</p>\r\n	</li><li><p>\r\n	–\r\n	Usługa Elektroniczna pozwalająca Usługobiorcy na subskrybowanie i\r\n	otrzymywanie na podany przez Usługobiorcę adres e-mail bezpłatnych\r\n	informacji pochodzących od Usługodawcy, dotyczących Produktów\r\n	dostępnych w Sklepie.</p>\r\n	</li><li><p>\r\n	\r\n	– dostępna w Sklepie rzecz ruchoma albo usługa, będąca\r\n	przedmiotem Umowy Sprzedaży między Klientem a Sprzedawcą.</p>\r\n	</li><li><p>\r\n	\r\n	- niniejszy regulamin Sklepu.</p>\r\n	</li><li><p>\r\n	\r\n	- Sklep internetowy Usługodawcy działający pod adresem\r\n	www.ecopitoriono.pl.</p>\r\n	</li><li><p>\r\n	,\r\n	<span>\r\n	–</span><span> IMIE I NAZWISKO wykonujący działalność\r\n	gospodarczą w formie spółki cywilnej pod firmą PEŁNA NAZWA FIRMY wpisaną\r\n	do Centralnej Ewidencji i Informacji o Działalności Gospodarczej\r\n	Rzeczypospolitej Polskiej prowadzonej przez ministra właściwego do\r\n	spraw gospodarki, miejsce wykonywania działalności oraz adres do\r\n	doręczeń: ADRES FIRMY</span><span>,\r\n	NIP: XXXXXXXXXXX, REGON: XXXXXXXXX, adres poczty elektronicznej\r\n	(e-mail): </span><span>,\r\n	numer telefonu: +48&nbsp;XXXXXXXXX</span><span>.</span></p>\r\n	</li><li><p>\r\n	\r\n	<span>\r\n	– Umowa Sprzedaży Produktu zawarta między Klientem, a Sprzedawcą\r\n	za pośrednictwem Sklepu.</span></p>\r\n	</li><li><p>\r\n	\r\n	<span>\r\n	– usługa świadczona drogą elektroniczną przez Usługodawcę na\r\n	rzecz Usługobiorcy za pośrednictwem Sklepu.</span></p>\r\n	</li><li><p>\r\n	\r\n	– osoba fizyczna, osoba prawna albo jednostka organizacyjna\r\n	nieposiadająca osobowości prawnej, której ustawa przyznaje\r\n	zdolność prawną korzystająca z Usługi Elektronicznej.</p>\r\n	</li><li><p>\r\n	\r\n	- oświadczenie woli Klienta stanowiące ofertę zawarcia Umowy\r\n	Sprzedaży Produktu ze Sprzedawcą.</p>\r\n</li></ol><p>\r\n<br>\r\n</p><p style=\"text-align: center;\">\r\n<b>§\r\n3</b></p><p style=\"text-align: center;\">\r\n<b>INFORMACJE\r\nDOTYCZĄCE PRODUKTÓW ORAZ ICH ZAMAWIANIA</b></p><ol>\r\n	<li><p>\r\n	Sklep\r\n	www.ecopitoriono.pl prowadzi sprzedaż detaliczną Produktów za\r\n	pośrednictwem sieci Internet, <span><u>wyłącznie\r\n	na rzecz Konsumentów</u></span><span>\r\n	</span>\r\n	</p>\r\n	</li><li><p>\r\n	Produkty\r\n	oferowane w Sklepie są&nbsp;<span><b>używane\r\n	lub nowe</b></span><span>, wolne od wad fizycznych i\r\n	prawnych i zostały legalnie wprowadzone na rynek polski. </span>\r\n	</p>\r\n	</li><li><p>\r\n	Informacje\r\n	znajdujące się na stronach internetowych Sklepu nie stanowią\r\n	oferty w rozumieniu przepisów prawa. Klient, składając\r\n	Zamówienie, składa ofertę kupna określonego Produktu na\r\n	warunkach podanych w jego opisie.</p>\r\n	</li><li><p>\r\n	Cena\r\n	Produktu uwidoczniona na stronie internetowej Sklepu podana jest w\r\n	złotych polskich (PLN) i zawiera wszystkie składniki, w tym\r\n	podatek VAT. Cena nie zawiera kosztów dostawy. \r\n	</p>\r\n	</li><li><p>\r\n	Cena\r\n	Produktu uwidoczniona na stronie Sklepu jest wiążąca w chwili\r\n	złożenia przez Klienta Zamówienia. Cena ta nie ulegnie zmianie\r\n	niezależnie od zmian cen w Sklepie, które mogą się pojawić w\r\n	odniesieniu do poszczególnych Produktów po złożeniu przez\r\n	Klienta Zamówienia.</p>\r\n	</li><li><p>\r\n	Zamówienia\r\n	można składać:</p>\r\n</li></ol><ol start=\"6\">\r\n	<ol>\r\n		<li><p>\r\n		poprzez\r\n		witrynę internetową za pomocą Formularza Zamówień (Sklep\r\n		www.ecopitoriono.pl) – 24 godziny na dobę przez cały rok,</p>\r\n		</li><li><p>\r\n		za\r\n		pośrednictwem poczty elektronicznej na adres:<b>\r\n		</b><span>,</span></p>\r\n		</li><li><p>\r\n		telefonicznie\r\n		pod numerem telefonu:<span>\r\n		</span><span>.</span></p>\r\n	</li></ol>\r\n</ol><ol start=\"7\">\r\n	<li><p>\r\n	W\r\n	celu złożenia Zamówienia, Klient nie ma obowiązku rejestracji\r\n	Konta w Sklepie.</p>\r\n	</li><li><p>\r\n	Warunkiem\r\n	złożenia Zamówienia w Sklepie przez Klienta jest zapoznanie się\r\n	z Regulaminem i akceptacja jego postanowień w czasie składania\r\n	Zamówienia.</p>\r\n	</li><li><p>\r\n	Sklep\r\n	realizuje Zamówienia złożone od poniedziałku do piątku w\r\n	godzinach pracy Sklepu tj. od 8 do 12 w dni robocze. Zamówienia\r\n	złożone w dni robocze po godz. 12, w soboty, niedziele oraz\r\n	święta,&nbsp;będą rozpatrywane następnego dnia roboczego.</p>\r\n	</li><li><p>\r\n	Produkty\r\n	w promocji (wyprzedaży) posiadają limitowaną liczbę sztuk i\r\n	Zamówienia na nie będą realizowane według kolejności ich\r\n	wpływania aż do wyczerpania się zapasów danego Produktu.</p>\r\n</li></ol><p><br>\r\n</p><p style=\"text-align: center;\">\r\n<b>§\r\n4</b></p><p style=\"text-align: center;\">\r\n<b>ZAWARCIE\r\nUMOWY SPRZEDAŻY</b></p><ol>\r\n	<li><p>\r\n	Do\r\n	zawarcia Umowy Sprzedaży, niezbędne jest wcześniejsze złożenie\r\n	przez Klienta Zamówienia udostępnionymi przez Sprzedawcę\r\n	sposobami, zgodnie z § 3 pkt 6 oraz 8.</p>\r\n	</li><li><p>\r\n	Po\r\n	złożeniu Zamówienia Sprzedawca niezwłocznie potwierdza jego\r\n	otrzymanie.</p>\r\n	</li><li><p>\r\n	Potwierdzenie\r\n	przyjęcia Zamówienia do realizacji powoduje związanie Klienta\r\n	jego Zamówieniem. Potwierdzenie otrzymania oraz przyjęcie\r\n	Zamówienia do realizacji następuje poprzez przesłanie wiadomości\r\n	e-mail.  \r\n	</p>\r\n	</li><li><p>\r\n	Potwierdzenie\r\n	przyjęcia Zamówienia do realizacji zawiera:</p>\r\n	<ol>\r\n		<li><p>\r\n		potwierdzenie\r\n		wszystkich istotnych elementów Zamówienia,</p>\r\n		</li><li><p>\r\n		formularz\r\n		odstąpienia od umowy,</p>\r\n		</li><li><p>\r\n		niniejszy\r\n		Regulamin zawierający pouczenie o prawie do odstąpienia od umowy.</p>\r\n	</li></ol>\r\n	</li><li><p>\r\n	Z\r\n	chwilą otrzymania przez Klienta wiadomości e-mail, o której mowa\r\n	w pkt 4 zostaje zawarta Umowa Sprzedaży między Klientem, a\r\n	Sprzedawcą.</p>\r\n	</li><li><p>\r\n	Każda\r\n	Umowa Sprzedaży będzie potwierdzana dowodem zakupu (paragon lub\r\n	faktura VAT na życzenie Klienta), który będzie dołączany do\r\n	Produktu.</p>\r\n</li></ol><p>\r\n<br>\r\n</p><p style=\"text-align: center;\">\r\n<b>§\r\n5</b></p><p style=\"text-align: center;\">\r\n<b>SPOSOBY\r\nPŁATNOŚCI</b></p><ol>\r\n	<li><p>\r\n	Sprzedawca\r\n	udostępnia następujące sposoby płatności: \r\n	</p>\r\n	<ol>\r\n		<li><p>\r\n		płatność\r\n		przelewem tradycyjnym na rachunek bankowy Sprzedawcy,</p>\r\n		</li><li><p>\r\n		płatność\r\n		za pośrednictwem elektronicznych serwisów płatności,</p>\r\n		</li><li><p>\r\n		płatność\r\n		przy odbiorze za tzw. pobraniem.</p>\r\n	</li></ol>\r\n	</li><li><p>\r\n	W\r\n	przypadku płatności przelewem tradycyjnym, wpłaty należy dokonać\r\n	na rachunek bankowy numer: XX XXXX NUMER KONTA (Bank\r\n	NAZWA BANKU)<span>\r\n	</span><span>NAZWA FIRMY, ADRES</span><span>, NIP:\r\n	XXXXXXXXXXX. W tytule przelewu należy wpisać „Zamówienie\r\n	nr........”.</span></p>\r\n	</li><li><p>\r\n	W\r\n	przypadku płatności za pośrednictwem elektronicznych serwisów\r\n	płatności Klient dokonuje zapłaty przed rozpoczęciem realizacji\r\n	Zamówienia. Elektroniczne serwisy płatności umożliwiają\r\n	dokonanie płatności za pomocą karty kredytowej lub szybkiego\r\n	przelewu z wybranych polskich banków.</p>\r\n	</li><li><p>\r\n	W\r\n	przypadku płatności przy odbiorze przesyłka wysyłana jest po\r\n	weryfikacji poprawności danych adresowych.</p>\r\n	</li><li><p>\r\n	Klient\r\n	zobowiązany jest do dokonania zapłaty ceny z tytułu Umowy\r\n	Sprzedaży w terminie 2 dni roboczych od dnia jej zawarcia, chyba że\r\n	Umowa Sprzedaży stanowi inaczej.</p>\r\n	</li><li><p>\r\n	W\r\n	przypadku wyboru płatności opisanych w pkt 1.1 oraz 1.2 Produkt\r\n	zostanie wysłany dopiero po jego opłaceniu.</p>\r\n</li></ol><p><br>\r\n</p><p style=\"text-align: center;\">\r\n<b>§\r\n6</b></p><p style=\"text-align: center;\">\r\n<b>KOSZT,\r\nTERMIN I SPOSOBY DOSTAWY PRODUKTU</b></p><ol>\r\n	<li><p>\r\n	Koszty\r\n	dostawy Produktu są ustalane w trakcie procesu składania\r\n	Zamówienia i są uzależnione od wyboru sposobu płatności oraz\r\n	sposobu dostawy zakupionego Produktu.</p>\r\n	</li><li><p>\r\n	Na\r\n	termin dostawy Produktu składa się czas kompletowania Produktu\r\n	oraz czas dostawy Produktu przez przewoźnika:</p>\r\n	<ol>\r\n		<li><p>\r\n		Czas\r\n		kompletowania Produktu wynosi 1 dzień roboczy od przyjęcia\r\n		Zamówienia do realizacji.</p>\r\n		</li><li><p>\r\n		Dostawa\r\n		Produktu przez przewoźnika następuje w terminie przez niego\r\n		deklarowanym tj. od 1 do 5 dni roboczych od nadania przesyłki\r\n		(dostawa następuje wyłącznie w dni robocze z wyłączeniem\r\n		sobót, niedziel i świąt).</p>\r\n	</li></ol>\r\n	</li><li><p>\r\n	Zakupione\r\n	w Sklepie Produkty są wysyłane wyłącznie na terenie Polski za\r\n	pośrednictwem Poczty Polskiej, paczkomatów (InPost) oraz firmy\r\n	kurierskiej (<span>).\r\n	</span>\r\n	</p>\r\n	</li><li><p>\r\n	W\r\n	przypadku Produktów stanowiących elektroniczne karty podarunkowe,\r\n	dostawa następuje za pośrednictwem poczty elektronicznej na adres\r\n	e-mail wskazany w Formularzu Zamówienia.</p>\r\n	</li><li><p>\r\n	Klient,\r\n	w tym Klient spoza Polski, może odebrać Produkt osobiście po\r\n	wcześniejszym uzgodnieniu telefonicznym lub mailowym. \r\n	</p>\r\n</li></ol><p>\r\n<br>\r\n</p><p style=\"text-align: center;\">\r\n<b>§\r\n7</b></p><p style=\"text-align: center;\">\r\n<b>REKLAMACJA\r\nPRODUKTU</b></p><ol>\r\n	<li><p>\r\n	<b>Reklamacja\r\n	z tytułu gwarancji.</b></p>\r\n	<ol>\r\n		<li><p>\r\n		Wszystkie\r\n		Produkty oferowane w Sklepie posiadają gwarancję&nbsp;Sprzedawcy\r\n		obowiązującą na terenie Rzeczypospolitej Polskiej. \r\n		</p>\r\n		</li><li><p>\r\n		Okres\r\n		gwarancji dla Produktów wynosi 24 miesiące i jest liczony od dnia\r\n		dostarczenia Produktu do Klienta.</p>\r\n		</li><li><p>\r\n		Dokumentem\r\n		uprawniającym do ochrony gwarancyjnej jest dowód zakupu.</p>\r\n		</li><li><p>\r\n		Gwarancja\r\n		nie wyłącza uprawnień Konsumenta wynikających z rękojmi za\r\n		wady fizyczne i prawne Produktu, określone w Kodeksie cywilnym.</p>\r\n	</li></ol>\r\n	</li><li><p>\r\n	<b>Reklamacja\r\n	z tytułu rękojmi.</b></p>\r\n	<ol>\r\n		<li><p>\r\n		Podstawa\r\n		i zakres odpowiedzialności Sprzedawcy wobec Klienta będącego\r\n		Konsumentem, z tytułu rękojmi obejmującej wady fizyczne i\r\n		prawne, są określone w ustawie Kodeks cywilny z dnia 23 kwietnia\r\n		1964 r. (Dz. U. nr 16, poz. 93 ze zm.).</p>\r\n		</li><li><p>\r\n		Zawiadomienia\r\n		o wadach dotyczących Produktu oraz zgłoszenie odpowiedniego\r\n		żądania można dokonać za pośrednictwem poczty elektronicznej\r\n		na adres: <span>\r\n		lub pisemnie na adres: </span><span><b>ul. ADRES</b></span><span>.</span></p>\r\n		</li><li><p>\r\n		W\r\n		powyższej wiadomości w formie pisemnej lub elektronicznej należy\r\n		podać jak najwięcej informacji i okoliczności dotyczących\r\n		przedmiotu reklamacji, w szczególności rodzaj i datę wystąpienia\r\n		nieprawidłowości oraz dane kontaktowe. Podane informacje znacznie\r\n		ułatwią i przyspieszą rozpatrzenie reklamacji przez Sprzedawcę.</p>\r\n		</li><li><p>\r\n		Dla\r\n		oceny wad fizycznych Produktu, należy go dostarczyć na adres: ul.\r\n		ADRES.</p>\r\n		</li><li><p>\r\n		Sprzedawca\r\n		ustosunkuje się do żądania Klienta niezwłocznie, nie później\r\n		niż w terminie 14 dni od momentu zgłoszenia reklamacji.</p>\r\n		</li><li><p>\r\n		W\r\n		przypadku reklamacji Klienta będącego Konsumentem –\r\n		nierozpatrzenie reklamacji w terminie 14 dni od jej zgłoszenia\r\n		jest jednoznaczne z jej uwzględnieniem. W związku z uzasadnioną\r\n		reklamacją Klienta będącego Konsumentem Sprzedawca pokrywa\r\n		koszty odbioru, dostawy i wymiany Produktu na wolny od wad.</p>\r\n		</li><li><p>\r\n		Odpowiedź\r\n		na reklamację jest przekazywana Konsumentowi na papierze lub innym\r\n		trwałym nośniku.</p>\r\n	</li></ol>\r\n</li></ol><p><br>\r\n</p><p style=\"text-align: center;\">\r\n<b>§\r\n8</b></p><p style=\"text-align: center;\">\r\n<b>PRAWO\r\nODSTĄPIENIA OD UMOWY</b></p><ol>\r\n	<li><p>\r\n	Z\r\n	zastrzeżeniem pkt 10 niniejszego paragrafu, Klient będący\r\n	jednocześnie Konsumentem, który zawarł umowę na odległość,\r\n	może od niej odstąpić bez podania przyczyn, składając stosowne\r\n	oświadczenie w terminie 14 dni. Do zachowania tego terminu\r\n	wystarczy wysłanie udostępnionego przez Sklep oświadczenia o\r\n	odstąpieniu od umowy.<span>\r\n	</span><span>Wygenerowanie oświadczenia\r\n	jest możliwe za pośrednictwem generatora dokumentów&nbsp;dostępnego\r\n	<a href=\"https://www.lexlab.pl/certyfikaty/LWS-65021a4c30139d79e427f39a3f9d5373\">TUTAJ</a>.</span></p>\r\n	</li><li><p>\r\n	W\r\n	razie odstąpienia od umowy, Umowa Sprzedaży jest uważana za\r\n	niezawartą, a Konsument ma obowiązek zwrócić Produkt Sprzedawcy\r\n	lub przekazać go osobie upoważnionej przez Sprzedawcę do odbioru\r\n	niezwłocznie, jednak nie później niż 14 dni od dnia, w którym\r\n	odstąpił od umowy, chyba że Sprzedawca zaproponował, że sam\r\n	odbierze Produkt. Do zachowania terminu wystarczy odesłanie\r\n	Produktu przed jego upływem.</p>\r\n	</li><li><p>\r\n	W\r\n	przypadku odstąpienia od Umowy Sprzedaży należy dokonać zwrotu\r\n	Produktu na adres: <span><b>ul. ADRES.</b></span></p>\r\n	</li><li><p>\r\n	Konsument\r\n	ponosi odpowiedzialność za zmniejszenie wartości Produktu będące\r\n	wynikiem korzystania z niego w sposób wykraczający poza konieczny\r\n	do stwierdzenia charakteru, cech i funkcjonowania Produktu.</p>\r\n	</li><li><p>\r\n	Z\r\n	zastrzeżeniem pkt 6 oraz 8 Sprzedawca dokona zwrotu wartości\r\n	Produktu wraz z kosztami jego dostawy przy użyciu takiego samego\r\n	sposobu zapłaty, jakiego użył Konsument, chyba że Konsument\r\n	wyraźnie zgodził się na inny sposób zwrotu, który nie wiąże\r\n	się dla niego z żadnymi kosztami.</p>\r\n	</li><li><p>\r\n	Jeżeli\r\n	Konsument wybrał sposób dostawy Produktu inny niż najtańszy\r\n	zwykły sposób dostarczenia oferowany przez Sklep, Sprzedawca nie\r\n	jest zobowiązany do zwrotu Konsumentowi poniesionych przez niego\r\n	dodatkowych kosztów.</p>\r\n	</li><li><p>\r\n	Jeżeli\r\n	Sprzedawca nie zaproponował, że sam odbierze Produkt od\r\n	Konsumenta, może wstrzymać się ze zwrotem płatności otrzymanych\r\n	od Konsumenta do chwili otrzymania rzeczy z powrotem lub\r\n	dostarczenia przez Konsumenta dowodu jej odesłania, w zależności\r\n	od tego, które zdarzenie nastąpi wcześniej.</p>\r\n	</li><li><p>\r\n	Konsument\r\n	odstępujący od Umowy Sprzedaży, zgodnie z pkt 1 niniejszego\r\n	paragrafu, ponosi jedynie koszty odesłania Produktu do Sprzedawcy.</p>\r\n	</li><li><p>\r\n	Termin\r\n	czternastodniowy, w którym Konsument może odstąpić od umowy,\r\n	liczy się od dnia, w którym Konsument objął Produkt w\r\n	posiadanie, a w przypadku usługi od dnia zawarcia umowy.</p>\r\n	</li><li><p>\r\n	Prawo\r\n	odstąpienia od umowy zawartej na odległość nie przysługuje\r\n	Konsumentowi w wypadku Umowy Sprzedaży:</p>\r\n</li></ol><ol start=\"10\">\r\n	<ol>\r\n		<li><p>\r\n		w\r\n		której przedmiotem świadczenia jest rzecz nieprefabrykowana,\r\n		wyprodukowana według specyfikacji Konsumenta lub służąca\r\n		zaspokojeniu jego zindywidualizowanych potrzeb,</p>\r\n		</li><li><p>\r\n		w\r\n		której przedmiotem świadczenia jest rzecz dostarczana w\r\n		zapieczętowanym opakowaniu, której po otwarciu opakowania nie\r\n		można zwrócić ze względu na ochronę zdrowia lub ze względów\r\n		higienicznych, jeżeli opakowanie zostało otwarte po dostarczeniu,</p>\r\n		</li><li><p>\r\n		w\r\n		której przedmiotem świadczenia są rzeczy, które po\r\n		dostarczeniu, ze względu na swój charakter, zostają&nbsp;<span>nierozłącznie\r\n		połączone z innymi rzeczami,</span></p>\r\n		</li><li><p>\r\n		w\r\n		której przedmiotem świadczenia jest usługa, jeżeli&nbsp;<span>Sprzedawca\r\n		wykonał w pełni usługę za wyraźną zgodą Konsumenta</span><span>,\r\n		który został poinformowany przed rozpoczęciem świadczenia, że\r\n		po spełnieniu świadczenia przez Sprzedawcę utraci prawo\r\n		odstąpienia od umowy,</span></p>\r\n		</li><li><p>\r\n		w\r\n		której przedmiotem świadczenia są nagrania dźwiękowe lub\r\n		wizualne albo programy komputerowe dostarczane w zapieczętowanym\r\n		opakowaniu, jeżeli opakowanie zostało otwarte po dostarczeniu,</p>\r\n		</li><li><p>\r\n		o\r\n		dostarczanie treści cyfrowych, które nie są zapisane na nośniku\r\n		materialnym, jeżeli spełnianie świadczenia rozpoczęło się za\r\n		wyraźną zgodą Konsumenta przed upływem terminu do odstąpienia\r\n		od umowy i po poinformowaniu go przez Sprzedawcę o utracie prawa\r\n		odstąpienia od umowy.</p>\r\n	</li></ol>\r\n</ol><ol start=\"11\">\r\n	<li><p>\r\n	Prawo\r\n	odstąpienia od Umowy Sprzedaży przysługuje zarówno Sprzedawcy,\r\n	jak i Klientowi (Konsumentowi), w przypadku niewykonania przez drugą\r\n	stronę umowy swojego zobowiązania w terminie ściśle określonym.</p>\r\n</li></ol><p><br>\r\n</p><p style=\"text-align: center;\">\r\n<b>§\r\n9</b></p><p style=\"text-align: center;\">\r\n<b>RODZAJ\r\nI ZAKRES USŁUG ELEKTRONICZNYCH</b></p><ol>\r\n	<li><p>\r\n	Usługodawca\r\n	umożliwia za pośrednictwem Sklepu korzystanie z Usług\r\n	Elektronicznych takich jak: \r\n	</p>\r\n	<ol>\r\n		<li><p>\r\n		zawieranie\r\n		Umów Sprzedaży Produktu,</p>\r\n		</li><li><p>\r\n		prowadzenie\r\n		Konta w Sklepie,</p>\r\n		</li><li><p>\r\n		Newsletter.</p>\r\n	</li></ol>\r\n	</li><li><p>\r\n	Świadczenie\r\n	Usług Elektronicznych na rzecz Usługobiorców w Sklepie odbywa się\r\n	na warunkach określonych w Regulaminie.</p>\r\n	</li><li><p>\r\n	Usługodawca\r\n	ma prawo do zamieszczania na stronie internetowej Sklepu treści\r\n	reklamowych. Treści te, stanowią integralną część Sklepu i\r\n	prezentowanych w nim materiałów.</p>\r\n</li></ol><p><br>\r\n</p><p style=\"text-align: center;\">\r\n<b>§\r\n10</b></p><p style=\"text-align: center;\">\r\n<b>WARUNKI\r\nŚWIADCZENIA I ZAWIERANIA UMÓW O ŚWIADCZENIE USŁUG ELEKTRONICZNYCH</b></p><ol>\r\n	<li><p>\r\n	Świadczenie\r\n	Usług Elektronicznych określonych w § 9 pkt 1 Regulaminu przez\r\n	Usługodawcę jest nieodpłatne.</p>\r\n	</li><li><p>\r\n	Okres\r\n	na jaki umowa zostaje zawarta: \r\n	</p>\r\n	<ol>\r\n		<li><p>\r\n		umowa\r\n		o świadczenie Usługi Elektronicznej polegającej na prowadzeniu\r\n		Konta w Sklepie zawierana jest na czas nieoznaczony.</p>\r\n		</li><li><p>\r\n		umowa\r\n		o świadczenie Usługi Elektronicznej polegającej na umożliwieniu\r\n		złożenia Zamówienia w Sklepie zawierana jest na czas oznaczony i\r\n		ulega rozwiązaniu z chwilą złożenia Zamówienia albo\r\n		zaprzestania jego składania przez Usługobiorcę.</p>\r\n		</li><li><p>\r\n		umowa\r\n		o świadczenie Usługi Elektronicznej polegającej na korzystaniu z\r\n		Newslettera zawierana jest na czas nieoznaczony.</p>\r\n	</li></ol>\r\n	</li><li><p>\r\n	Wymagania\r\n	techniczne niezbędne do współpracy z systemem teleinformatycznym,\r\n	którym posługuje się Usługodawca: \r\n	</p>\r\n	<ol>\r\n		<li><p>\r\n		komputer\r\n		(lub urządzenie mobilne) z dostępem do Internetu,</p>\r\n		</li><li><p>\r\n		dostęp\r\n		do poczty elektronicznej,</p>\r\n		</li><li><p>\r\n		przeglądarka\r\n		internetowa,</p>\r\n		</li><li><p>\r\n		włączenie\r\n		w przeglądarce internetowej Cookies oraz Javascript.</p>\r\n	</li></ol>\r\n	</li><li><p>\r\n	Usługobiorca\r\n	zobowiązany jest do korzystania ze Sklepu w sposób zgodny z prawem\r\n	i dobrymi obyczajami mając na uwadze poszanowanie dóbr osobistych\r\n	i praw własności intelektualnej osób trzecich.</p>\r\n	</li><li><p>\r\n	Usługobiorca\r\n	zobowiązany jest do wprowadzania danych zgodnych ze stanem\r\n	faktycznym.</p>\r\n	</li><li><p>\r\n	Usługobiorcę\r\n	obowiązuje zakaz dostarczania treści o charakterze bezprawnym.</p>\r\n</li></ol><p><br>\r\n</p><p style=\"text-align: center;\">\r\n<b>§\r\n11</b></p><p style=\"text-align: center;\">\r\n<b>REKLAMACJE\r\nZWIĄZANE ZE ŚWIADCZENIEM USŁUG ELEKTRONICZNYCH</b></p><ol>\r\n	<li><p>\r\n	Reklamacje\r\n	związane ze świadczeniem Usług Elektronicznych za pośrednictwem\r\n	Sklepu Usługobiorca może składać za pośrednictwem poczty\r\n	elektronicznej na adres: <span>.</span></p>\r\n	</li><li><p>\r\n	W\r\n	powyższej wiadomości e-mail, należy podać jak najwięcej\r\n	informacji i okoliczności dotyczących przedmiotu reklamacji, w\r\n	szczególności rodzaj i datę wystąpienia nieprawidłowości oraz\r\n	dane kontaktowe. Podane informacje znacznie ułatwią i przyspieszą\r\n	rozpatrzenie reklamacji przez Usługodawcę.</p>\r\n	</li><li><p>\r\n	Rozpatrzenie\r\n	reklamacji przez Usługodawcę następuje niezwłocznie, nie później\r\n	niż w terminie 14 dni od momentu zgłoszenia.</p>\r\n	</li><li><p>\r\n	Odpowiedź\r\n	Usługodawcy w sprawie reklamacji jest wysyłana na adres e-mail\r\n	Usługobiorcy podany w zgłoszeniu reklamacyjnym lub w inny podany\r\n	przez Usługobiorcę sposób.</p>\r\n</li></ol><p><br>\r\n</p><p style=\"text-align: center;\">\r\n<b>§\r\n12</b></p><p style=\"text-align: center;\">\r\n<b>WARUNKI\r\nROZWIĄZYWANIA UMÓW O ŚWIADCZENIE USŁUG ELEKTRONICZNYCH</b></p><ol>\r\n	<li><p>\r\n	Wypowiedzenie\r\n	umowy o świadczenie Usługi Elektronicznej:</p>\r\n	<ol>\r\n		<li><p>\r\n		Wypowiedzeniu\r\n		może ulec umowa o świadczenie Usługi Elektronicznej o\r\n		charakterze ciągłym i bezterminowym (np. prowadzenie Konta).</p>\r\n		</li><li><p>\r\n		Usługobiorca\r\n		może wypowiedzieć umowę ze skutkiem natychmiastowym i bez\r\n		wskazywania przyczyn poprzez przesłanie stosownego oświadczenia\r\n		za pośrednictwem poczty elektronicznej na adres: sklep@ecopitoriono.pl.</p>\r\n		</li><li><p>\r\n		Usługodawca\r\n		może wypowiedzieć umowę o świadczenie Usługi Elektronicznej o\r\n		charakterze ciągłym i bezterminowym w przypadku, gdy Usługobiorca\r\n		narusza Regulamin, w szczególności, gdy dostarcza treści o\r\n		charakterze bezprawnym po bezskutecznym wcześniejszym wezwaniu do\r\n		zaprzestania naruszeń z wyznaczeniem odpowiedniego terminu. Umowa\r\n		w takim wypadku wygasa po upływie 7 dni od dnia złożenia\r\n		oświadczenia woli o jej wypowiedzeniu (okres wypowiedzenia).</p>\r\n		</li><li><p>\r\n		Wypowiedzenie\r\n		prowadzi do ustania stosunku prawnego ze skutkiem na przyszłość.</p>\r\n	</li></ol>\r\n	</li><li><p>\r\n	Usługodawca\r\n	i Usługobiorca mogą rozwiązać umowę o świadczenie Usługi\r\n	Elektronicznej w każdym czasie w drodze porozumienia stron.</p>\r\n</li></ol><p><br>\r\n</p><p style=\"text-align: center;\">\r\n<b>§\r\n13</b></p><p style=\"text-align: center;\">\r\n<b>WŁASNOŚĆ\r\nINTELEKTUALNA</b></p><ol>\r\n	<li><p>\r\n	Wszystkie\r\n	treści zamieszczone na stronie internetowej pod adresem\r\n	www.ecopitoriono.pl korzystają z ochrony prawnoautorskiej i (z\r\n	zastrzeżeniem § 13 pkt<span>3\r\n	oraz elementów wykorzystywanych na zasadzie licencji, przeniesienia\r\n	praw autorskich lub dozwolonego użytku)</span><span><b>\r\n	</b></span><span>są własnością NAZWA FIRMY, ADRES</span><span>, NIP: XXXXXXXXXXX, REGON:\r\n	XXXXXXXXX. Usługobiorca ponosi pełną odpowiedzialność za szkodę\r\n	wyrządzoną Usługodawcy, będącą następstwem użycia\r\n	jakiejkolwiek zawartości strony www.ecopitoriono.pl, bez zgody\r\n	Usługodawcy.</span></p>\r\n	</li><li><p>\r\n	Jakiekolwiek\r\n	wykorzystanie przez kogokolwiek, bez wyraźnej pisemnej zgody\r\n	Usługodawcy, któregokolwiek z elementów składających się na\r\n	treść oraz zawartość strony www.ecopitoriono.pl stanowi naruszenie\r\n	prawa autorskiego przysługującego Usługodawcy i skutkuje\r\n	odpowiedzialnością cywilnoprawną oraz karną.</p>\r\n	</li><li><p>\r\n	Wszystkie\r\n	nazwy handlowe, nazwy Produktów, nazwy firm i ich logo użyte na\r\n	stronie internetowej Sklepu pod adresem www.ecopitoriono.pl należą do\r\n	ich właścicieli i są używane wyłącznie w celach\r\n	identyfikacyjnych. Mogą być one zastrzeżonymi znakami towarowymi.\r\n	Wszystkie materiały, opisy i zdjęcia prezentowane na stronie\r\n	internetowej Sklepu pod adresem www.ecopitoriono.pl użyte są w celach\r\n	informacyjnych.</p>\r\n</li></ol><p>\r\n<br>\r\n</p><p style=\"text-align: center;\">\r\n<b>§\r\n14</b></p><p style=\"text-align: center;\">\r\n<b>POSTANOWIENIA\r\nKOŃCOWE</b></p><ol>\r\n	<li><p>\r\n	Umowy\r\n	zawierane poprzez Sklep zawierane są zgodnie z prawem polskim.</p>\r\n	</li><li><p>\r\n	W\r\n	przypadku niezgodności jakiejkolwiek części Regulaminu z\r\n	obowiązującym prawem, w miejsce zakwestionowanego przepisu\r\n	Regulaminu zastosowanie mają właściwe przepisy prawa polskiego.</p>\r\n	</li><li><p>\r\n	Wszelkie\r\n	spory wynikłe z Umów Sprzedaży między Sklepem, a Konsumentami\r\n	będą rozstrzygane w pierwszej kolejności na drodze negocjacji, z\r\n	intencją polubownego zakończenia sporu, z uwzględnieniem ustawy <span>o\r\n	pozasądowym rozwiązywaniu sporów konsumenckich.</span><span>\r\n	Jeśli jednak nie byłoby to możliwe, lub też byłoby\r\n	niesatysfakcjonujące dla którejkolwiek ze stron, spory będą\r\n	rozstrzygane przez właściwy sąd powszechny, zgodnie z pkt 4\r\n	niniejszego paragrafu.</span></p>\r\n	</li><li><p>\r\n	Sądowe\r\n	rozstrzyganie sporów:</p>\r\n	<ol>\r\n		<li><p>\r\n		Ewentualne\r\n		spory powstałe pomiędzy Usługodawcą, a Usługobiorcą\r\n		(Klientem) będącym jednocześnie Konsumentem zostają poddane\r\n		sądom właściwym zgodnie z przepisami kodeksu postępowania\r\n		cywilnego z dnia 17 listopada 1964 r. (Dz. U. Nr 43, poz. 296 ze\r\n		zm.).</p>\r\n		</li><li><p>\r\n		Ewentualne\r\n		spory powstałe pomiędzy Usługodawcą, a Usługobiorcą\r\n		niebędącym jednocześnie Konsumentem zostają poddane sądowi\r\n		właściwemu ze względu na siedzibę Usługodawcy.</p>\r\n	</li></ol>\r\n	</li><li><p>\r\n	Klient\r\n	będący Konsumentem ma również prawo do skorzystania z\r\n	pozasądowych sposobów rozstrzygania sporów w szczególności\r\n	poprzez złożenie po zakończeniu postępowania reklamacyjnego\r\n	wniosku o wszczęcie mediacji lub wniosku o rozpatrzenie sprawy\r\n	przez sąd polubowny (wniosek można pobrać na stronie internetowej\r\n	http://www.uokik.gov.pl/download.php?plik=6223). Wykaz Stałych\r\n	Polubownych Sądów Konsumenckich działających przy Wojewódzkich\r\n	Inspektoratach Inspekcji Handlowej dostępny jest na stronie\r\n	internetowej: http://www.uokik.gov.pl/wazne_adresy.php#faq596.\r\n	Konsument może skorzystać także z bezpłatnej pomocy powiatowego\r\n	(miejskiego) rzecznika konsumentów lub organizacji społecznej, do\r\n	której zadań statutowych należy ochrona konsumentów.&nbsp;Pozasądowe\r\n	dochodzenie roszczeń po zakończeniu postępowania reklamacyjnego\r\n	jest bezpłatne.</p>\r\n	</li><li><p>\r\n	Konsument\r\n	w celu polubownego rozwiązania sporu może w szczególności złożyć\r\n	skargę za pośrednictwem platformy internetowej ODR (Online Dispute\r\n	Resolution), dostępnej pod\r\n	adresem:&nbsp;http://ec.europa.eu/consumers/odr/.</p>\r\n</li></ol><p align=\"CENTER\">\r\n</p><p><br>\r\n</p></div>', '', '', '', 0, 'type_default', 'pl_PL'),
(4, 0, 0, NULL, 'Polityka prywatności', '<div class=\"html-content\"><p align=\"CENTER\"><span><b>POLITYKA\r\nPRYWATNOŚCI SKLEPU INTERNETOWEGO</b></span></p><p align=\"CENTER\"><span><b>www.ecopitoriono.pl</b></span></p><p>\r\n<br>\r\n</p><p>\r\n<br>\r\n</p><p align=\"CENTER\"><span><b>§\r\n1</b></span></p><p align=\"CENTER\"><span><b>POSTANOWIENIA\r\nOGÓLNE</b></span></p><ol>\r\n	<li><p align=\"JUSTIFY\">\r\n	<span>Administratorem\r\n	danych osobowych zbieranych za pośrednictwem Sklepu internetowego \r\n	www.ecopitoriono.pl jest</span><span> NAZWA FIRMY wpisaną\r\n	do Centralnej Ewidencji i Informacji o Działalności Gospodarczej\r\n	Rzeczypospolitej Polskiej prowadzonej przez ministra właściwego do\r\n	spraw gospodarki, miejsce wykonywania działalności oraz adres do\r\n	doręczeń: ADRES, NIP:\r\n	XXXXXXXXXXX, REGON: XXXXXXXXX, adres poczty elektronicznej (e-mail):\r\n	sklep@ecopitoriono.pl, numer telefonu: +48 XXX XXX XXX, </span><span>zwana\r\n	dalej „Administratorem\" i będąca jednocześnie\r\n	„Usługodawcą”.</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>Dane\r\n	osobowe zbierane przez Administratora za pośrednictwem strony\r\n	internetowej są przetwarzane zgodnie z&nbsp;Rozporządzeniem\r\n	Parlamentu Europejskiego i&nbsp;Rady (UE) 2016/679 z&nbsp;dnia 27\r\n	kwietnia 2016 r. w&nbsp;sprawie ochrony osób fizycznych w&nbsp;związku\r\n	z&nbsp;przetwarzaniem danych osobowych i&nbsp;w sprawie swobodnego\r\n	przepływu takich danych oraz uchylenia dyrektywy 95/46/WE (ogólne\r\n	rozporządzenie o&nbsp;ochronie danych), zwane dalej </span><span><b>RODO.</b></span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>Wszelkie\r\n	wyrazy lub wyrażenia pisane w treści niniejszej Polityki\r\n	Prywatności z dużej litery należy rozumieć zgodnie z ich\r\n	definicją zawartą w Regulaminie Sklepu internetowego\r\n	www.ecopitoriono.pl.</span></p>\r\n</li></ol><p>\r\n<br>\r\n</p><p align=\"CENTER\"><span><b>§\r\n2</b></span></p><p align=\"CENTER\"><span><b>RODZAJ\r\nPRZETWARZANYCH DANYCH OSOBOWYCH, CEL I ZAKRES ZBIERANIA DANYCH</b></span></p><ol>\r\n	<li><p align=\"JUSTIFY\">\r\n	<span><b>CEL\r\n	PRZETWARZANIA I PODSTAWA PRAWNA.</b></span><span>\r\n	Administrator przetwarza dane osobowe Usługobiorców Sklepu\r\n	www.ecopitoriono.pl w przypadku:</span></p>\r\n</li></ol><ol type=\"a\">\r\n	<li><p align=\"JUSTIFY\">\r\n	<span>rejestracji\r\n	Konta w&nbsp;Sklepie, w&nbsp;celu utworzenia indywidualnego konta\r\n	i&nbsp;zarządzania tym Kontem, na podstawie art. 6 ust. 1 lit. b\r\n	RODO (realizacja umowy o świadczenie usługi drogą elektroniczną\r\n	zgodnie z&nbsp;Regulaminem Sklepu),</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>składania\r\n	zamówienia w&nbsp;Sklepie, w&nbsp;celu wykonania umowy sprzedaży,\r\n	na podstawie art. 6 ust. 1 lit. b RODO (realizacja umowy sprzedaży),</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>zapisania\r\n	się do Newslettera w celu przesyłania informacji handlowych drogą\r\n	elektroniczną. Dane osobowe są przetwarzane po wyrażeniu odrębnej\r\n	zgody, na podstawie art. 6 ust. 1 lit. a) RODO.</span></p>\r\n</li></ol><ol start=\"2\">\r\n	<li><p align=\"JUSTIFY\">\r\n	<span><b>RODZAJ\r\n	PRZETWARZANYCH DANYCH OSOBOWYCH.</b></span><span>\r\n	W przypadku:</span></p>\r\n</li></ol><ol type=\"a\">\r\n	<li><p align=\"JUSTIFY\">\r\n	<span>Konta\r\n	Usługobiorca podaje:</span></p>\r\n</li></ol><ul>\r\n	<li><p align=\"JUSTIFY\">\r\n	<span>Imię\r\n	i nazwisko,</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>Login,</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>Adres,</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>Adres\r\n	e-mail.</span></p>\r\n</li></ul><ol type=\"a\" start=\"2\">\r\n	<li><p align=\"JUSTIFY\">\r\n	<span>Zamówienia\r\n	Usługobiorca podaje:</span></p>\r\n</li></ol><ul>\r\n	<li><p align=\"JUSTIFY\">\r\n	<span>Imię\r\n	i nazwisko,</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>NIP,</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>Adres,</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>Adres\r\n	e-mail,</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>Numer\r\n	telefonu,</span></p>\r\n</li></ul><ol type=\"a\" start=\"3\">\r\n	<li><p align=\"JUSTIFY\">\r\n	<span>Newslettera\r\n	Usługobiorca podaje</span></p>\r\n</li></ol><ul>\r\n	<li><p align=\"JUSTIFY\">\r\n	<span>Imię\r\n	i nazwisko,</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>adres\r\n	e-mail.</span></p>\r\n</li></ul><ol start=\"3\">\r\n	<li><p align=\"JUSTIFY\">\r\n	<span><b>OKRES\r\n	ARCHIWIZACJI DANYCH OSOBOWYCH.</b></span><span> Dane\r\n	osobowe Usługobiorców przechowywane są przez Administratora: </span>\r\n	</p>\r\n</li></ol><ol type=\"a\">\r\n	<li><p align=\"JUSTIFY\">\r\n	<span>w\r\n	przypadku, gdy podstawą przetwarzania danych jest wykonanie umowy,\r\n	tak długo, jak jest to niezbędne do wykonania umowy, a&nbsp;po tym\r\n	czasie przez okres odpowiadający okresowi przedawnienia roszczeń.\r\n	Jeżeli przepis szczególny nie stanowi inaczej, termin\r\n	przedawnienia wynosi lat sześć, a&nbsp;dla roszczeń o&nbsp;świadczenia\r\n	okresowe oraz roszczeń związanych z&nbsp;prowadzeniem działalności\r\n	gospodarczej - trzy lata.</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>w\r\n	przypadku, gdy podstawą przetwarzania danych jest zgoda, tak długo,\r\n	aż zgoda nie zostanie odwołana, a&nbsp;po odwołaniu zgody przez\r\n	okres czasu odpowiadający okresowi przedawnienia roszczeń jakie\r\n	może podnosić Administrator i&nbsp;jakie mogą być podnoszone\r\n	wobec niego. Jeżeli przepis szczególny nie stanowi inaczej, termin\r\n	przedawnienia wynosi lat sześć, a&nbsp;dla roszczeń o&nbsp;świadczenia\r\n	okresowe oraz roszczeń związanych z&nbsp;prowadzeniem działalności\r\n	gospodarczej - trzy lata.</span></p>\r\n</li></ol><ol start=\"4\">\r\n	<li><p align=\"JUSTIFY\">\r\n	<span>Podczas\r\n	korzystania ze Sklepu mogą być pobierane dodatkowe informacje,\r\n	w&nbsp;szczególności: adres IP przypisany do komputera\r\n	Usługobiorcy lub zewnętrzny adres IP dostawcy Internetu, nazwa\r\n	domeny, rodzaj przeglądarki, czas dostępu, typ systemu\r\n	operacyjnego.</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>Po\r\n	wyrażeniu odrębnej zgody, na podstawie art. 6 ust. 1 lit. a) RODO\r\n	dane mogą być przetwarzane również w celu przesyłania\r\n	informacji handlowych drogą elektroniczną lub wykonywania\r\n	telefonicznych połączeń w celu marketingu bezpośredniego –\r\n	odpowiednio w związku z art. 10 ust. 2 Ustawy z dnia 18 lipca 2002\r\n	roku o świadczeniu usług drogą elektroniczną lub art. 172 ust. 1\r\n	Ustawy z dnia 16 lipca 2004 roku – Prawo Telekomunikacyjne, w tym\r\n	kierowanych w wyniku profilowania, o ile Usługobiorca wyraził\r\n	stosowną zgodę.</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>Od\r\n	Usługobiorców mogą być także gromadzone dane nawigacyjne, w&nbsp;tym\r\n	informacje o&nbsp;linkach i&nbsp;odnośnikach, w&nbsp;które\r\n	zdecydują się kliknąć lub innych czynnościach, podejmowanych w\r\n	Sklepie. Podstawą prawną tego rodzaju czynności jest prawnie\r\n	uzasadniony interes Administratora (art. 6 ust. 1 lit. f RODO),\r\n	polegający na ułatwieniu korzystania z&nbsp;usług świadczonych\r\n	drogą elektroniczną oraz na poprawie funkcjonalności tych usług.</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>Podanie\r\n	danych osobowych przez Usługobiorcę jest dobrowolne.</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>Dane\r\n	osobowe będą przetwarzane także w sposób zautomatyzowany w\r\n	formie profilowania, o ile Usługobiorca wyrazi na to zgodę na\r\n	podstawie art. 6 ust. 1 lit. a) RODO. Konsekwencją profilowania\r\n	będzie przypisanie danej osobie profilu w celu podejmowania\r\n	dotyczących jej decyzji bądź analizy lub przewidywania jej\r\n	preferencji, zachowań i postaw.</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>Administrator\r\n	dokłada szczególnej staranności w celu ochrony interesów osób,\r\n	których dane dotyczą, a w szczególności zapewnia, że zbierane\r\n	przez niego dane są:</span></p>\r\n</li></ol><ol>\r\n	<ol type=\"a\">\r\n		<li><p align=\"JUSTIFY\">\r\n		<span>przetwarzane\r\n		zgodnie z prawem, </span>\r\n		</p>\r\n		</li><li><p align=\"JUSTIFY\">\r\n		<span>zbierane\r\n		dla oznaczonych, zgodnych z prawem celów i niepoddawane dalszemu\r\n		przetwarzaniu niezgodnemu z tymi celami,</span></p>\r\n		</li><li><p align=\"JUSTIFY\">\r\n		<span>merytorycznie\r\n		poprawne i adekwatne w stosunku do celów, w jakich są\r\n		przetwarzane oraz przechowywane w postaci umożliwiającej\r\n		identyfikację osób, których dotyczą, nie dłużej niż jest to\r\n		niezbędne do osiągnięcia celu przetwarzania.</span></p>\r\n	</li></ol>\r\n</ol><p>\r\n<br>\r\n</p><p align=\"CENTER\"><span><b>§\r\n3</b></span></p><p align=\"CENTER\"><span><b>UDOSTĘPNIENIE\r\nDANYCH OSOBOWYCH</b></span></p><ol>\r\n	<li><p align=\"JUSTIFY\">\r\n	<span>Dane\r\n	osobowe Usługobiorców przekazywane są dostawcom usług, z&nbsp;których\r\n	korzysta Administrator przy prowadzeniu Sklepu. Dostawcy usług,\r\n	którym przekazywane są dane osobowe, w&nbsp;zależności od\r\n	uzgodnień umownych i&nbsp;okoliczności, albo podlegają poleceniom\r\n	Administratora co do celów i&nbsp;sposobów przetwarzania tych\r\n	danych (podmioty przetwarzające) albo samodzielnie określają cele\r\n	i&nbsp;sposoby ich przetwarzania (administratorzy).</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	Dane\r\n	osobowe Usługobiorców są przechowywane wyłącznie na terenie\r\n	Europejskiego Obszaru Gospodarczego (EOG).</p>\r\n</li></ol><p align=\"JUSTIFY\"><br>\r\n</p><p align=\"CENTER\"><span><b>§\r\n4</b></span></p><p align=\"CENTER\"><span><b>PRAWO\r\nKONTROLI, DOSTĘPU DO TREŚCI WŁASNYCH DANYCH ORAZ ICH POPRAWIANIA</b></span></p><ol>\r\n	<li><p align=\"JUSTIFY\">\r\n	<span>Osoba,\r\n	której dane dotyczą, ma prawo dostępu do treści swoich danych\r\n	osobowych oraz prawo ich sprostowania, usunięcia, ograniczenia\r\n	przetwarzania, prawo do przenoszenia danych, prawo wniesienia\r\n	sprzeciwu, prawo do cofnięcia zgody w dowolnym momencie bez wpływu\r\n	na zgodność z prawem przetwarzania, którego dokonano na podstawie\r\n	zgody przed jej cofnięciem.</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>Podstawy\r\n	prawne żądania Usługobiorcy: </span>\r\n	</p>\r\n</li></ol><ol type=\"a\">\r\n	<li><p align=\"JUSTIFY\">\r\n	<span><b>Dostęp\r\n	do danych</b></span><span>\r\n	– art. 15 RODO.</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span><b>Sprostowanie\r\n	danych</b></span><span>\r\n	– art. 16 RODO.</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span><b>Usunięcie\r\n	danych (tzw. prawo do bycia zapomnianym) </b></span><span>–\r\n	art. 17 RODO.</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span><b>Ograniczenie\r\n	przetwarzania</b></span><span>\r\n	– art. 18 RODO.</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span><b>Przeniesienie\r\n	danych </b></span><span>–\r\n	art. 20 RODO.</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span><b>Sprzeciw\r\n	</b></span><span>–\r\n	art. 21 RODO</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span><b>Cofnięcie\r\n	zgody </b></span><span>–\r\n	art. 7 ust. 3 RODO.</span></p>\r\n</li></ol><ol start=\"3\">\r\n	<li><p align=\"JUSTIFY\">\r\n	<span>W\r\n	celu realizacji uprawnień, o których mowa w pkt 2 można wysłać\r\n	stosowną wiadomość e-mail na adres:</span>\r\n	<span>.</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>W\r\n	sytuacji wystąpienia przez Usługobiorcę z&nbsp;uprawnieniem\r\n	wynikającym z&nbsp;powyższych praw, Administrator spełnia żądanie\r\n	albo odmawia jego spełnienia niezwłocznie, nie później jednak\r\n	niż w&nbsp;ciągu miesiąca po jego otrzymaniu. Jeżeli jednak -\r\n	z&nbsp;uwagi na skomplikowany charakter żądania lub liczbę żądań\r\n	– Administrator nie będzie mógł spełnić żądania w&nbsp;ciągu\r\n	miesiąca, spełni je w&nbsp;ciągu kolejnych dwóch miesięcy\r\n	informując Usługobiorcę uprzednio w&nbsp;terminie miesiąca od\r\n	otrzymania żądania - o&nbsp;zamierzonym przedłużeniu terminu\r\n	oraz jego przyczynach.</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>W\r\n	przypadku stwierdzenia, że przetwarzanie danych osobowych narusza\r\n	przepisy RODO, osoba, której dane dotyczą, ma prawo wnieść\r\n	skargę do Prezesa Urzędu Ochrony Danych Osobowych.</span></p>\r\n</li></ol><p>\r\n<br>\r\n</p><p align=\"CENTER\"><span><b>§\r\n5</b></span></p><p align=\"CENTER\"><span><b>PLIKI\r\n\"COOKIES\"</b></span></p><ol>\r\n	<li><p align=\"JUSTIFY\">\r\n	<span>Strona\r\n	Administratora&nbsp;</span><span>używa\r\n	plików</span><span>.</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>Instalacja\r\n	plików „</span><span>”\r\n	jest konieczna do prawidłowego świadczenia usług na stronie\r\n	internetowej Sklepu. W plikach „</span><span znajdują=\"\" się=\"\" informacje=\"\" niezbędne=\"\" do=\"\" prawidłowego=\"\" funkcjonowania=\"\" strony,=\"\" a=\"\" także=\"\" <=\"\" span=\"\"><span>dają one także\r\n	możliwość opracowywania ogólnych statystyk odwiedzin strony\r\n	internetowej.</span></span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>W\r\n	ramach strony stosowane są dwa rodzaje plików „</span><span>”:\r\n	„sesyjne” oraz „stałe”.</span></p>\r\n</li></ol><ol type=\"a\">\r\n	<li><p align=\"JUSTIFY\">\r\n	<span>”\r\n	„sesyjne” są plikami tymczasowymi, które przechowywane są w\r\n	urządzeniu końcowym Usługobiorcy do czasu wylogowania\r\n	(opuszczenia strony). </span>\r\n	</p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>Stałe”\r\n	pliki „</span><span>”\r\n	przechowywane są w urządzeniu końcowym Usługobiorcy przez czas\r\n	określony w parametrach plików „</span><span>”\r\n	lub do czasu ich usunięcia przez Usługobiorcę.</span></p>\r\n</li></ol><ol start=\"4\">\r\n	<li><p align=\"JUSTIFY\">\r\n	<span>Administrator\r\n	wykorzystuje własne pliki cookies w celu </span><span>lepszego\r\n	poznania sposobu interakcji Usługobiorców w zakresie zawartości\r\n	strony. Pliki gromadzą informacje o sposobie korzystania ze strony\r\n	internetowej przez Usługobiorcę, typie strony, z jakiej\r\n	Usługobiorca został przekierowany oraz liczbie odwiedzin i czasie\r\n	wizyty Usługobiorcy na stronie internetowej. Informacje te nie\r\n	rejestrują konkretnych danych osobowych Usługobiorcy, lecz służą\r\n	do opracowania statystyk korzystania ze strony.</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>Administrator\r\n	wykorzystuje zewnętrzne pliki cookies</span><span><i>\r\n	</i></span><span>w\r\n	celu </span><span>zbierania ogólnych\r\n	i&nbsp;anonimowych danych statycznych za pośrednictwem narzędzi\r\n	analitycznych Google Analytics (administrator cookies zewnętrznego:\r\n	Google Inc. z&nbsp;siedzibą w&nbsp;USA).</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>Usługobiorca\r\n	ma prawo zadecydowania w zakresie dostępu plików „</span><span>”\r\n	do swojego komputera poprzez ich uprzedni wybór w oknie swojej\r\n	przeglądarki. </span><span>Szczegółowe\r\n	informacje o możliwości i sposobach obsługi plików „</span><span>”\r\n	dostępne są w ustawieniach oprogramowania (przeglądarki\r\n	internetowej).</span></p>\r\n</li></ol><p>\r\n<br>\r\n</p><p align=\"CENTER\"><span><b>§\r\n6</b></span></p><p align=\"CENTER\"><span><b>POSTANOWIENIA\r\nKOŃCOWE</b></span></p><p>\r\n</p><ol>\r\n	<li><p align=\"JUSTIFY\">\r\n	<span>Administrator\r\n	stosuje środki techniczne i organizacyjne zapewniające ochronę\r\n	przetwarzanych danych osobowych odpowiednią do zagrożeń oraz\r\n	kategorii danych objętych ochroną, a w szczególności zabezpiecza\r\n	dane przed ich udostępnieniem osobom nieupoważnionym, zabraniem\r\n	przez osobę nieuprawnioną, przetwarzaniem z naruszeniem\r\n	obowiązujących przepisów oraz zmianą, utratą, uszkodzeniem lub\r\n	zniszczeniem.</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>Administrator\r\n	udostępnia odpowiednie środki techniczne zapobiegające\r\n	pozyskiwaniu i modyfikowaniu przez osoby nieuprawnione, danych\r\n	osobowych przesyłanych drogą elektroniczną.</span></p>\r\n	</li><li><p align=\"JUSTIFY\">\r\n	<span>W\r\n	sprawach nieuregulowanych niniejszą Polityką prywatności stosuje\r\n	się odpowiednio </span><span>przepisy\r\n	RODO</span><span>\r\n	oraz inne właściwe przepisy prawa polskiego.</span></p>\r\n</li></ol></div>', '', '', '', 0, 'type_default', 'pl_PL'),
(6, 1, 0, NULL, 'Dostawa', '<div class=\"html-content\"><p>Dostawa INFO</p></div>[%-_lista-kodow_-%]', '', '', '', 0, 'type_default', 'pl_PL');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `strona_i18n`
--

CREATE TABLE `strona_i18n` (
  `id` int(11) NOT NULL,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `szablon`
--

CREATE TABLE `szablon` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(100) NOT NULL,
  `label` varchar(100) NOT NULL,
  `temat` varchar(100) NOT NULL,
  `tresc` text NOT NULL,
  `opis` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `szablon`
--

INSERT INTO `szablon` (`id`, `nazwa`, `label`, `temat`, `tresc`, `opis`) VALUES
(1, 'odzyskiwanie_hasla', 'Odzyskiwanie hasła', 'Restart hasła', '<p>[%odzyskiwanie-hasla-link%]<br />[%odzyskiwanie-hasla-url%]<br />[%adres-ip%]</p>', ''),
(2, 'zamowienie_status_przyjete', 'Twoje zamówienie zostało przyjęte do realizacji', 'Twoje zamówienie zostało przyjęte do realizacji', '<div style=\"text-align: center;\">\r\n            <img src=\"http://ecopitoriono.projekt-samatix.pl/img/logo.png\" alt=\"Ecopitoriono\" style=\"font-size: 42px;\">\r\n        </div>\r\n        <div style=\"display: block;margin: 15px auto;max-width: 700px;padding: 15px;background-color: #fbfbfb;border: 1px solid #aaaaaa;border-radius: 5px;\">\r\n            <p style=\"font-size: 15px;font-weight: 700;\">Witamy serdecznie!</p>\r\n            <p style=\"font-size: 14px;\">Dziękujemy za skorzystanie z oferty naszego sklepu.</p>\r\n            <p style=\"font-size: 14px;\">Twoje zamówienie nr <span style=\"font-weight: bold;\">[%zamowienie-numer%]</span> otrzymało status <b>przyjęte do realizacji</b>.</p>\r\n            <p style=\"font-size: 14px;\">Przewidywany termin wysyłki zamówionych produktów to:&nbsp;<span style=\"font-weight: bold;\">[%termin-wysylki%]</span></p>\r\n\r\n            <p>\r\n                ----------------------<br>\r\n                Pozdrawiamy,<br>\r\n                Zespół Ecopitoriono.pl\r\n            </p>\r\n            <p style=\"text-align:center;font-size: 10px;\">Regulamin Platformy znajdą Państwo pod linkiem:&nbsp;<a href=\"http://ecopitoriono.projekt-samatix.pl/pages/view/3/regulamin\" target=\"_blank\" style=\"background-color: rgb(255, 255, 255);\">Regulamin</a></p>\r\n        </div>\r\n', 'Email po zmianie statusu na przyjęte do realizacji'),
(3, 'zamowienie_status_zrealizowane', 'Twoje zamówienie zostało zrealizowane', 'Twoje zamówienie [%zamowienie-numer%] zostało wysłane', '<div style=\"text-align: center;\">\r\n            <img src=\"http://ecopitoriono.projekt-samatix.pl/img/logo.png\" alt=\"Ecopitoriono\" style=\"font-size: 42px;\">\r\n        </div>\r\n        <div style=\"display: block;margin: 15px auto;max-width: 700px;padding: 15px;background-color: #fbfbfb;border: 1px solid #aaaaaa;border-radius: 5px;\">\r\n            <p style=\"font-size: 15px;font-weight: 700;\">Witamy serdecznie!</p>\r\n            <p style=\"font-size: 14px;\">Dziękujemy za skorzystanie z oferty naszego sklepu.</p>\r\n            <p style=\"font-size: 14px;\">Twoje zamówienie nr <span style=\"font-weight: bold;\">[%zamowienie-numer%]</span>&nbsp;zostało wysłane.</p>\r\n\r\n            <p>\r\n                ----------------------<br>\r\n                Pozdrawiamy,<br>\r\n                Zespół Ecopitoriono.pl\r\n            </p>\r\n            <p style=\"text-align:center;font-size: 10px;\">Regulamin Platformy znajdą Państwo pod linkiem:&nbsp;<a href=\"http://ecopitoriono.projekt-samatix.pl/pages/view/3/regulamin\" target=\"_blank\" style=\"background-color: rgb(255, 255, 255);\">Regulamin</a></p>\r\n        </div>\r\n', 'Email po zmianie statusu na zrealizowane'),
(4, 'zamowienie_status_anulowane', 'Twoje zamówienie zostało anulowane', 'Twoje zamówienie zostało anulowane', '<div style=\"text-align: center;\">\r\n            <img src=\"http://ecopitoriono.projekt-samatix.pl/img/logo.png\" alt=\"Ecopitoriono\" style=\"font-size: 42px;\">\r\n        </div>\r\n        <div style=\"display: block;margin: 15px auto;max-width: 700px;padding: 15px;background-color: #fbfbfb;border: 1px solid #aaaaaa;border-radius: 5px;\">\r\n            <p style=\"font-size: 15px;font-weight: 700;\">Witamy serdecznie!</p>\r\n            <p style=\"font-size: 14px;\">Dziękujemy za skorzystanie z oferty naszego sklepu.</p>\r\n            <p style=\"font-size: 14px;\">Twoje zamówienie nr <span style=\"font-weight: bold;\">[%zamowienie-numer%]</span>&nbsp;zostało anulowane.</p>\r\n\r\n            <p>\r\n                ----------------------<br>\r\n                Pozdrawiamy,<br>\r\n                Zespół Ecopitoriono.pl\r\n            </p>\r\n            <p style=\"text-align:center;font-size: 10px;\">Regulamin Platformy znajdą Państwo pod linkiem:&nbsp;<a href=\"http://ecopitoriono.projekt-samatix.pl/pages/view/3/regulamin\" target=\"_blank\" style=\"background-color: rgb(255, 255, 255);\">Regulamin</a></p>\r\n        </div>\r\n', 'Email po zmianie statusu na anulowane'),
(5, 'zamowienie_zlozone', 'Złożone zamówienie', 'Potwierdzenie zamówienia', '<div style=\"text-align: center;\">\r\n            <img src=\"http://ecopitoriono.projekt-samatix.pl/img/logo.png\" alt=\"Ecopitoriono\" style=\"font-size: 42px;\">\r\n        </div>\r\n        <div style=\"display: block;margin: 15px auto;max-width: 700px;padding: 15px;background-color: #fbfbfb;border: 1px solid #aaaaaa;border-radius: 5px;\">\r\n            <p style=\"font-size: 15px;font-weight: 700;\">Witamy serdecznie!</p>\r\n            <p style=\"font-size: 14px;\">Dziękujemy za skorzystanie z oferty naszego sklepu.</p>\r\n            <p style=\"font-size: 14px;\">Pragniemy poinformować, że Państwa zamówienie złożone dnia: [%zamowienie-data%] otrzymało numer: <b>[%zamowienie-numer%]</b>.</p>\r\n            <p style=\"font-size: 14px;\">Data złożenia zamówienia: <b>[%zamowienie-data%]</b>\r\n                <br>Imię: <b>[%zamowienie-imie%]</b>\r\n                <br>Nazwisko: <b>[%zamowienie-nazwisko%]</b>\r\n                <br>Telefon: <b>[%zamowienie-telefon%]</b>\r\n                <br>E-mail: <b>[%zamowienie-email%]</b>\r\n                <br>Rodzaj wysyłki i sposób płatności: <b>[%zamowienie-platnosc-wysylka%]</b>\r\n            </p>\r\n            <p style=\"font-size: 18px;font-weight: 700;text-align: center;\">Wartość Twojego zamówienia wynosi: <span style=\"font-size: 24px;font-weight: 700;color: #f68a21;\">[%zamowienie-wartosc-razem%]</span></p>\r\n            <hr>\r\n            <p class=\"text-center fs-15 fw-bold\">Poniżej znajdują się informacje dotyczące zamówienia</p>\r\n            <p>[%zamowienie-adres-wysylki%]</p>\r\n            <p>[%zamowienie-adres-faktury%]</p>\r\n            <p>Uwagi do zamówienia: [%zamowienie-uwagi%]<br></p>\r\n            <div>[%paczkomat-info%]</div>\r\n            <div>[%zamowienie-bonus%]</div>\r\n            [%zamowienie-preorder-info%]<div>[%towary%]\r\n                <table class=\"towar-table\" style=\"width:100%;\">\r\n                    <tbody>\r\n                        <tr>\r\n                            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Lp</th>\r\n                            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Nazwa</th>\r\n                            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Kod</th>\r\n                            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Ilość</th>\r\n                            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Cena brutto</th>\r\n                            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;text-align: center;\">Wartość brutto</th>\r\n                        </tr>\r\n                        <tr>\r\n                            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-lp%]</td>\r\n                            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-nazwa%] [%t-platforma%]</td>\r\n                            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-kod%]</td>\r\n                            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-ilosc%] [%t-jednostka%]</td>\r\n                            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-cena%]</td>\r\n                            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;text-align: center;\">[%t-wartosc%]</td>\r\n                        </tr>\r\n\r\n                    </tbody>\r\n                </table>\r\n                [%/towary%]\r\n            </div>\r\n            <div>[%zamowienie-rabaty%]</div>\r\n            <p>Wartość produktów: [%zamowienie-wartosc-produktow%]</p>\r\n            <p>[%zamowienie-platnosc-wysylka%]: [%zamowienie-koszt-wysylki%]</p>\r\n            <div>[%zamowienie-bonus-uzyty%]</div>\r\n            <p style=\"font-size: 18px;font-weight: 700;text-align: center;\">Łączna wartość Twojego zamówienia wynosi: <span style=\"font-size: 24px;font-weight: 700;color: #f68a21;\">[%zamowienie-wartosc-razem%]</span></p>\r\n            <div>\r\n                [%zamowienie-platnosc-info%]</div><div>[%zamowienie-punkt-odbioru%]<br></div><div>\r\n            </div>\r\n            <p>\r\n                ----------------------<br>\r\n                Pozdrawiamy,<br>\r\n                Zespół Ecopitoriono.pl\r\n            </p>\r\n            <p style=\"text-align:center;font-size: 10px;\">Regulamin Platformy znajdą Państwo pod linkiem:&nbsp;<a href=\"http://ecopitoriono.projekt-samatix.pl/pages/view/3/regulamin\" target=\"_blank\" style=\"background-color: rgb(255, 255, 255);\">Regulamin</a></p>\r\n        </div>', ''),
(6, 'zamowienie_status_wyslane', 'Zamówienie zostało wysłane', 'Zamówienie zostało wysłane', '<div style=\"text-align: center;\">\r\n            <img src=\"http://ecopitoriono.projekt-samatix.pl/img/logo.png\" alt=\"Ecopitoriono\" style=\"font-size: 42px;\">\r\n        </div>\r\n        <div style=\"display: block;margin: 15px auto;max-width: 700px;padding: 15px;background-color: #fbfbfb;border: 1px solid #aaaaaa;border-radius: 5px;\">\r\n            <p style=\"font-size: 15px;font-weight: 700;\">Witamy serdecznie!</p>\r\n            <p style=\"font-size: 14px;\">Dziękujemy za skorzystanie z oferty naszego sklepu.</p>\r\n            <p style=\"font-size: 14px;\">Twoje zamówienie nr <span style=\"font-weight: bold;\">[%zamowienie-numer%]</span>&nbsp;zostało wysłane.</p><p style=\"font-size: 14px;\">Numer listu przewozowego: <b>[%nr-listu-przewozowego%]</b></p><p style=\"text-align: center; font-size: 14px;\"><span style=\"color: rgb(255, 0, 0);\">Możesz monitorować przesyłkę korzystając z poniższego linku</span></p><p style=\"text-align: center; \"><span style=\"font-size: 14px;\"><b>[%link-sledzenia%]</b></span><br></p>\r\n\r\n            <p>\r\n                ----------------------<br>\r\n                Pozdrawiamy,<br>\r\n                Zespół Ecopitoriono.pl\r\n            </p>\r\n            <p style=\"text-align:center;font-size: 10px;\">Regulamin Platformy znajdą Państwo pod linkiem:&nbsp;<a href=\"http://ecopitoriono.projekt-samatix.pl/pages/view/3/regulamin\" target=\"_blank\" style=\"background-color: rgb(255, 255, 255);\">Regulamin</a></p>\r\n        </div>\r\n', 'Zmiana statusu na wysłane'),
(7, 'zamowienie_pdf', 'Szablon PDF zamówienia', '-', '<style type=\"text/css\">\r\n.towar-table td{ padding 5px; border:1px solid #ccc;}\r\n.towar-table th{ padding 5px; border:1px solid #ccc; background:#ccc;}\r\n</style>\r\n<p style=\"text-align: center; \"><span style=\"font-size: 18px;\">Zamówienie z dnia <b>[%zamowienie-data%]</b> nr: <b>[%zamowienie-numer%]</b></span></p><p><br></p><table style=\"width:100%;\"><tbody><tr><td><b><span style=\"font-size: 14px;\">Dane do wysyłki</span></b></td><td><b><span style=\"font-size: 14px;\">Dane do faktury</span></b></td><td><b><span style=\"font-size: 14px;\">Dane klienta</span></b></td></tr><tr><td>[%zamowienie-adres-wysylki%]<br></td><td>[%zamowienie-adres-faktury%]<br></td><td>[%zamowienie-dane-klienta%]<br></td></tr></tbody></table><p>Uwagi klienta:</p><p>[%zamowienie-uwagi%]</p><p>Produkty w zamówieniu</p><p>[%towary%]</p><p>&nbsp;</p><table class=\"towar-table\" style=\"width:100%;\"><tbody><tr><th>Lp</th><th>Nazwa</th><th>Ilość</th><th>Cena</th><th style=\"text-align: center;\">Wartość</th></tr><tr><td>[%t-lp%]</td><td>[%t-nazwa%]</td><td>[%t-ilosc%] [%t-jednostka%]</td><td>[%t-cena%]</td><td style=\"text-align: right;\">[%t-wartosc%]</td></tr><tr><td colspan=\"4\" style=\"text-align: right;\">Wartość produktów</td><td style=\"text-align: right;\">[%zamowienie-wartosc-produktow%]</td></tr><tr><td colspan=\"4\" style=\"text-align: right;\">Łączna wartość [%zamowienie-rabat%]</td><td style=\"text-align: right;\">[%zamowienie-wartosc-razem%]</td></tr></tbody></table><p>[%/towary%]</p>', 'Generowanie pdf z zamówieniem'),
(8, 'zamowienie_zlozone_telefoniczne', 'Zamówienie przyjęte przez admministratora', 'Nowe zamówienie', '<style type=\"text/css\">\r\n.towar-table td{ padding 5px; border:1px solid #ccc;}\r\n.towar-table th{ padding 5px; border:1px solid #ccc; background:#ccc;}\r\n</style>\r\n<p>Witam serdecznie!<br>Dziękuję za skorzystanie z oferty naszego sklepu.<br>Pragnę poinformować, że Państwa zamówienie złożone dnia: [%zamowienie-data%] otrzymało numer: [%zamowienie-numer%].<br><br><br>Data złożenia zamówienia: [%zamowienie-data%]<br>Imię: [%zamowienie-imie%]<br>Nazwisko: [%zamowienie-nazwisko%]<br>Telefon: [%zamowienie-telefon%]<br>E-mail: [%zamowienie-email%]<br>Rodzaj wysyłki i sposób płatności: [%zamowienie-platnosc-wysylka%].<br><br>Poniżej znajdują się informacje dotyczące zamówienia:</p>\r\n<p>Twoje uwagi [%zamowienie-uwagi%]<br><br>[%towary%]</p>\r\n<p>&nbsp;</p>\r\n<table class=\"towar-table\" style=\"width:100%;\"><tbody><tr><th>Lp</th><th>Nazwa</th><th>Ilość</th><th>Cena</th><th style=\"text-align: center;\">Wartość</th></tr><tr><td>[%t-lp%]</td><td>[%t-nazwa%]</td><td>[%t-ilosc%] [%t-jednostka%]</td><td>[%t-cena%]</td><td style=\"text-align: right;\">[%t-wartosc%]</td></tr><tr><td colspan=\"4\" style=\"text-align: right;\">Wartość produktów</td><td style=\"text-align: right;\">[%zamowienie-wartosc-produktow%]</td></tr><tr><td colspan=\"4\" style=\"text-align: right;\">Łączna wartość [%zamowienie-rabat%]</td><td style=\"text-align: right;\">[%zamowienie-wartosc-razem%]</td></tr></tbody></table>\r\n<p>[%/towary%]<br></p>\r\n<h3>&nbsp;</h3>', 'Mail po dodaniu zamówienia przez administratora'),
(9, 'admin_info_nowe_zamowienie', 'Informacja dla handlowca o nowym zamówieniu', 'Nowe zamówienie', '<div style=\"text-align: center;\">\r\n            <img src=\"http://ecopitoriono.projekt-samatix.pl/img/logo.png\" alt=\"Ecopitoriono\" style=\"font-size: 42px;\">\r\n        </div>\r\n        <div style=\"display: block;margin: 15px auto;max-width: 700px;padding: 15px;background-color: #fbfbfb;border: 1px solid #aaaaaa;border-radius: 5px;\">\r\n            <p style=\"font-size: 15px;font-weight: 700;\">Witamy serdecznie!</p>\r\n            <p style=\"font-size: 14px;\">W systemie pojawiło się nowe zamówienie. Kliknij <a href=\"http://ecopitoriono.projekt-samatix.pl/admin/zamownienie/view/[%zamowienie-id%]\" target=\"_blank\">TUTAJ</a> aby otworzyć zamówienie w panelu.</p>\r\n            <p style=\"font-size: 14px;\">Data złożenia zamówienia: <b>[%zamowienie-data%]</b></p>\r\n          	<p style=\"font-size: 14px;\">numer:&nbsp;<span style=\"font-weight: 700;\">[%zamowienie-numer%]</span><b><br></b></p>\r\n          	<p style=\"font-size: 14px;\">Imię: <b>[%zamowienie-imie%]</b>\r\n                <br>Nazwisko: <b>[%zamowienie-nazwisko%]</b>\r\n                <br>Telefon: <b>[%zamowienie-telefon%]</b>\r\n                <br>E-mail: <b>[%zamowienie-email%]</b>\r\n                <br>Rodzaj wysyłki i sposób płatności: <b>[%zamowienie-platnosc-wysylka%]</b>\r\n            </p>\r\n            <p style=\"font-size: 18px;font-weight: 700;text-align: center;\">Wartość zamówienia wynosi: <span style=\"font-size: 24px;font-weight: 700;color: #f68a21;\">[%zamowienie-wartosc-razem%]</span></p>\r\n            <hr>\r\n            <p class=\"text-center fs-15 fw-bold\">Poniżej znajdują się informacje dotyczące zamówienia</p>\r\n            <p>[%zamowienie-adres-wysylki%]</p>\r\n            <p>[%zamowienie-adres-faktury%]</p>\r\n            <p>Uwagi do zamówienia: [%zamowienie-uwagi%]<br></p>\r\n            <div>[%paczkomat-info%]</div>\r\n            <div>[%zamowienie-bonus%]</div>\r\n            [%zamowienie-preorder-info%]<div>[%towary%]\r\n                <table class=\"towar-table\" style=\"width:100%;\">\r\n                    <tbody>\r\n                        <tr>\r\n                            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Lp</th>\r\n                            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Nazwa</th>\r\n                            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Kod</th>\r\n                            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Ilość</th>\r\n                            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Cena brutto</th>\r\n                            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;text-align: center;\">Wartość brutto</th>\r\n                        </tr>\r\n                        <tr>\r\n                            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-lp%]</td>\r\n                            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-nazwa%] [%t-platforma%]</td>\r\n                            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-kod%]</td>\r\n                            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-ilosc%] [%t-jednostka%]</td>\r\n                            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-cena%]</td>\r\n                            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;text-align: center;\">[%t-wartosc%]</td>\r\n                        </tr>\r\n\r\n                    </tbody>\r\n                </table>\r\n                [%/towary%]\r\n            </div>\r\n            <div>[%zamowienie-rabaty%]</div>\r\n            <p>Wartość produktów: [%zamowienie-wartosc-produktow%]</p>\r\n            <p>[%zamowienie-platnosc-wysylka%]: [%zamowienie-koszt-wysylki%]</p>\r\n            <div>[%zamowienie-bonus-uzyty%]</div>\r\n            <p style=\"font-size: 18px;font-weight: 700;text-align: center;\">Łączna wartość zamówienia wynosi: <span style=\"font-size: 24px;font-weight: 700;color: #f68a21;\">[%zamowienie-wartosc-razem%]</span></p>\r\n            <div>\r\n                [%zamowienie-platnosc-info%]\r\n            </div>\r\n            [%zamowienie-punkt-odbioru%]<p>----------------------<br>\r\n                Pozdrawiamy,<br>\r\n                Zespół Ecopitoriono.pl\r\n            </p>\r\n            <p style=\"text-align:center;font-size: 10px;\">Regulamin Platformy znajdą Państwo pod linkiem:&nbsp;<a href=\"http://ecopitoriono.projekt-samatix.pl/pages/view/3/regulamin\" target=\"_blank\" style=\"background-color: rgb(255, 255, 255);\">Regulamin</a></p>\r\n        </div>', 'Informacja dla handlowca o nowym zamówieniu'),
(10, 'admin_info_zamowienie_telefoniczne', 'Informacja dla handlowca o nowym zamówieniu telefonicznym', 'Zamówienie telefoniczne', '<p>Pojawiło się nowe zamówienie [%zamowienie-numer%] </p><p><a href=\"https://example.pl/admin/zamowienie/view/[%zamowienie-id%]\">Zobacz szczegóły</a></p><p>z admina</p>', 'Informacja dla handlowca o nowym zamówieniu telefonicznym'),
(11, 'admin_info_nowy_klient', 'Nowy klient', 'Nowy klient', 'Zarejestrował się nowy klient uzupełnij jego dane <a href=\"https://example.pl/admin/uzytkownik/edit/[%uzytkownik-id%]\">[%uzytkownik-imie%] [%uzytkownik-nazwisko%]</a>', 'Informacja dla administratora o nowym kliencie'),
(12, 'aktywacja_konta', 'Aktywacja konta', 'Twoje konto zostało aktywowane', 'Twoje konto w serwisie example.pl zostało aktywowane.\r\n\r\nTwój partner handlowy to [%uzytkownik-ph%]\r\nMożesz się z nim kontaktować bezpośrednio na adres [%uzytkownik-ph-email%]\r\n\r\nMożesz się <a href=\"https://example.pl/home/login\">zalogować</a>\r\n\r\nŻyczymy udanych zakupów.', 'Email do klienta z informacją że jego konto zostało aktywowane'),
(13, 'rejestracja', 'Email po zarejestrowaniu się przez stronę', 'Potwierdź rejestrację konta.', '<p>Witaj [%uzytkownik-imie%],</p><p>Dziękujemy za rejestracje w naszym serwisie. Aby potwierdzić swoje dane kliknij w link poniżej.</p><p><a href=\"[%uzytkownik-potwierdz-url%]\" target=\"_blank\">[%uzytkownik-potwierdz-url%]</a></p><p><br></p><p>Pozdrawiamy,<br>example.pl</p>', 'Email po zarejestrowaniu się przez stronę'),
(14, 'newsletter_potwierdzenie', 'Potwierdzenie zapisu na newsletter', 'Potwierdź swój adres email.', '<div style=\"text-align: center;\">\r\n            <img src=\"http://ecopitoriono.projekt-samatix.pl/img/logo.png\" alt=\"Ecopitoriono\" style=\"font-size: 42px;\">\r\n        </div>\r\n        <div style=\"display: block;margin: 15px auto;max-width: 700px;padding: 15px;background-color: #fbfbfb;border: 1px solid #aaaaaa;border-radius: 5px;\">\r\n            <p style=\"font-size: 15px;font-weight: 700;\">Witamy serdecznie!</p>\r\n            <p style=\"font-size: 14px;\">Dziękujemy za dołączenie do naszego newslettera. Aby potwierdzić chęć otrzymywania od nas informacji o promocjach prosimy o kliknięcie w link potwierdzający</p>\r\n            <p style=\"text-align: center; font-size: 14px;\"><span style=\"font-size: 13px; text-align: start; background-color: rgb(255, 255, 255);\">[%newsletter-potwierdzenie-link%]</span><br></p>\r\n\r\n            <p>\r\n                ----------------------<br>\r\n                Pozdrawiamy,<br>\r\n                Zespół Ecopitoriono.pl\r\n            </p>\r\n            <p style=\"text-align:center;font-size: 10px;\">Regulamin Platformy znajdą Państwo pod linkiem:&nbsp;<a href=\"http://ecopitoriono.projekt-samatix.pl/pages/view/3/regulamin\" target=\"_blank\" style=\"background-color: rgb(255, 255, 255);\">Regulamin</a></p></div>', ''),
(15, 'potwierdzenie_zapytania', 'Potwierdzenie zapytania o grę', 'Otrzymaliśmy Twoje zapytanie', '<div style=\"text-align: center;\">\r\n            <img src=\"http://ecopitoriono.projekt-samatix.pl/img/logo.png\" alt=\"Ecopitoriono\" style=\"font-size: 42px;\">\r\n        </div>\r\n        <div style=\"display: block;margin: 15px auto;max-width: 700px;padding: 15px;background-color: #fbfbfb;border: 1px solid #aaaaaa;border-radius: 5px;\">\r\n            <p style=\"font-size: 15px;font-weight: 700;\">Witamy serdecznie!</p>\r\n            <p style=\"font-size: 14px;\">Dziękujemy za kontakt.</p>\r\n            <p style=\"font-size: 14px;\">Twoje zgłoszenie otrzymało numer&nbsp;<span style=\"font-size: 13px; background-color: rgb(255, 255, 255);\"><b>[%nr_zgloszenia%]</b></span>.</p>\r\n\r\n            <p>\r\n                ----------------------<br>\r\n                Pozdrawiamy,<br>\r\n                Zespół Ecopitoriono.pl\r\n            </p>\r\n            <p style=\"text-align:center;font-size: 10px;\">Regulamin Platformy znajdą Państwo pod linkiem:&nbsp;<a href=\"http://ecopitoriono.projekt-samatix.pl/pages/view/3/regulamin\" target=\"_blank\" style=\"background-color: rgb(255, 255, 255);\">Regulamin</a></p></div>', 'Email do klienta z potwierdzeniem złożenia zapytania o grę'),
(16, 'karta_podarunkowa_pdf', 'Szablon karty podarunkowej', 'Szablon karty podarunkowej', '        <div style=\"display: block;margin: 15px;padding: 15px;background-color: #fbfbfb;\">\r\n          <div style=\"text-align: center; margin-top: 15px; margin-bottom:15px;\">\r\n            <img src=\"http://ecopitoriono.projekt-samatix.pl/img/logo.png\" alt=\"Ecopitoriono\" style=\"font-size: 42px;\">\r\n        </div>\r\n            <p style=\"text-align: center;\"><span style=\"font-size: 24px;\"><span style=\"font-weight: 700;\">[%karta_podarunkowa-nazwa%]</span></span></p><p style=\"text-align: center;\"><span style=\"font-size: 36px;\"><span style=\"font-weight: 700;\">Wartość karty: [%wartosc%] zł</span></span></p><p style=\"text-align: center;\"><span style=\"font-size: 18px;\">Twój kod:&nbsp;<span style=\"font-weight: 700;\">[%kod%]</span></span></p><p style=\"text-align: center;\"><span style=\"font-size: 18px;\">Kod pin:&nbsp;<span style=\"font-weight: 700;\">[%pin%]</span></span></p><p style=\"text-align: center;\"><span style=\"font-size: 12px;\">Aby wykorzystać kartę wejdź na stronę <b>www.exgame.pl</b></span></p></div>', 'Szablon PDF karty podarunkowej'),
(17, 'karta_podarunkowa_mail', 'Mail z kartami podarunkowymi', 'Twoje karty podarunkowe', '<div style=\"text-align: center;\">\r\n            <img src=\"http://ecopitoriono.projekt-samatix.pl/img/logo.png\" alt=\"Ecopitoriono\" style=\"font-size: 42px;\">\r\n        </div>\r\n        <div style=\"display: block;margin: 15px auto;max-width: 700px;padding: 15px;background-color: #fbfbfb;border: 1px solid #aaaaaa;border-radius: 5px;\">\r\n            <p style=\"font-size: 15px;font-weight: 700;\">Witamy serdecznie!</p>\r\n            <p style=\"font-size: 14px;\">W załączniku przesyłamy karty podarunkowe z zamówienia&nbsp;<b>[%zamowienie-numer%]</b>.<br></p>\r\n            <p style=\"font-size: 14px;\">Aby wykorzystać kartę podarunkową należy założyć konto w naszym serwisie i wprowadzić kod oraz pin z karty w panelu użytkownika.</p><div>\r\n            </div>\r\n            <p>\r\n                ----------------------<br>\r\n                Pozdrawiamy,<br>\r\n                Zespół Ecopitoriono.pl\r\n            </p>\r\n            <p style=\"text-align:center;font-size: 10px;\">Regulamin Platformy znajdą Państwo pod linkiem:&nbsp;<a href=\"http://ecopitoriono.projekt-samatix.pl/pages/view/3/regulamin\" target=\"_blank\" style=\"background-color: rgb(255, 255, 255);\">Regulamin</a></p>\r\n        </div>', 'Mail z kartami podarunkowymi'),
(18, 'odpowiedz_na_zapytanie_dostepny', 'Odpowiedź na zapytanie (produkt dostępny)', 'Produkt jest dostępny', '<p>Witaj [%imie%],</p><p>Informujemy, że produkt [%towar%] jest <b>dostępny </b>w punkcie odbioru.</p><p>Produkt jest możliwy do odebrania pod adresem<br>[%adres%]</p>', 'Odpowiedź na zapytanie (produkt dostępny)'),
(19, 'odpowiedz_na_zapytanie_niedostepny', 'Odpowiedź na zapytanie (produkt niedostępny)', 'Produkt jest niedostępny', '<p>Witaj [%imie%],</p><p>Z przykrością informujemy, że produkt [%towar%] jest&nbsp;<span style=\"font-weight: 700;\">niedostępny</span>.</p>', 'Odpowiedź na zapytanie (produkt niedostępny)'),
(20, 'odpowiedz_na_zapytanie_dostepny_premiera', 'Odpowiedź na zapytanie o premierę (produkt dostępny)', 'Odpowiedź na zapytanie o premierę', '<p>Witaj [%imie%],</p><p>Informujemy, że produkt [%towar%] będzie dostępny w sprzedaży po dacie premiery która przypada na [%data-premiery%].</p><p>Przewidujemy uruchomienie preorderów od [%data-preorder%]</p>', 'Odpowiedź na zapytanie o premierę (produkt dostępny)'),
(21, 'odpowiedz_na_zapytanie_niedostepny_premiera', 'Odpowiedź na zapytanie o premierę (produkt niedostępny)', 'Produkt premierowy jest niedostępny', '<p>Witaj [%imie%],</p><p>Z przykrością informujemy, że produkt aktualnie nie wiemy kiedy produkt będzie dostępny w sprzedaży.</p>', 'Odpowiedź na zapytanie o premierę (produkt niedostępny)'),
(22, 'odpowiedz_na_zapytanie', 'Odpowiedź na zapytanie o grę', 'Odpowiedź na zapytanie nr: [%nr_zgloszenia%]', '<p>Witaj [%imie%],</p><p>W odpowiedzi na zapytanie [%nr_zgloszenia%] z dnia [%data_dodania_data%] godz. [%data_dodania_czas%],</p><p>informujemy że aktualnie nie prowadzimy sprzedaży wskazanej gry.</p><p><br></p><p>--------------------------------------------------------------------------------------------------------------<br>Treść Twojego zgłoszenia:</p><p>[%tresc%]</p>', 'Odpowiedź na zapytanie o grę'),
(23, 'powiadomienie_o_cenie', 'Powiadomienie o cenie', 'Powiadomienie o cenie', '<p>Witaj [%imie%],</p><p>Informujemy, że cena produktu [%produkt%] zmieniła się z [%stara_cena%] na [%nowa_cena%]</p><p>Zapraszamy do zakupów</p>', 'Powiadomienie o cenie'),
(24, 'odpowiedz_na_zapytanie_o_produkt_tak', 'Odpowiedź na zapytanie o produkt (pozytywna)', 'Twoje zapytanie o produkt', '<p>Witaj [%imie%],</p><p>W odpowiedzi na Twoje zapytanie o produkt [%towar%]</p><p>Informujemy, że:</p><p>[tutaj wpisz treść odpowiedzi]</p>', ''),
(25, 'odpowiedz_na_zapytanie_o_produkt_nie', 'Odpowiedź na zapytanie o produkt (negatywna)', 'Twoje zapytanie o produkt', '<p>Witaj [%imie%],</p><p>W odpowiedzi na Twoje zapytanie o produkt [%towar%]</p><p>Informujemy, że:</p><p>[tutaj wpisz treść odpowiedzi]</p>', ''),
(26, 'faktura', 'Szablon faktury', 'Szablon faktury', '<div style=\"text-align:right; margin-bottom:20px;\">\r\n    Miejsce wystawienia: [%miejsce-wystawienia%]<br>\r\n    Data wystawienia: [%data-wystawienia%]<br>\r\n    Data sprzedaży: [%data-sprzedazy%]<br>\r\n    Termin płatności: [%termin-platnosci%]\r\n</div>\r\n<table style=\"width:100%;table-layout: fixed;\">\r\n    <tbody>\r\n        <tr>\r\n            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Sprzedawca</th>\r\n            <th style=\"width:100px;\"></th>\r\n            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Nabywca</th>\r\n        </tr>\r\n        <tr>\r\n            <td>[%dane-sklepu%]</td>\r\n            <td></td>\r\n            <td>[%dane-klienta%]</td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<table style=\"width:100%;table-layout: fixed;\">\r\n    <tbody>\r\n        <tr>\r\n            <td style=\"text-align:center;\">Faktura numer [%numer%]<br>[%podtytul%]</td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n[%towary%]\r\n<table style=\"width:100%;\">\r\n    <tbody>\r\n        <tr>\r\n            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Lp</th>\r\n            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Nazwa</th>\r\n            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Ilość</th>\r\n            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">j.m.</th>\r\n            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">VAT [%]</th>\r\n            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Cena jednostkowa brutto</th>\r\n            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Wartość netto</th>\r\n            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Wartość VAT</th>\r\n            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;text-align: center;\">Wartość brutto</th>\r\n        </tr>\r\n        <tr>\r\n            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-lp%]</td>\r\n            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-nazwa%]&nbsp;</td>\r\n            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-ilosc%]</td>\r\n            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-jednostka%]</td>\r\n            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-vat%]</td>\r\n            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-cena-brutto%]</td>\r\n            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-wartosc-netto%]</td>\r\n            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-wartosc-vat%]</td>\r\n            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-wartosc-brutto%]</td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n[%/towary%]\r\n<div style=\"margin-top:20px; text-align:right;\">\r\n    Rodzaj płatności: [%rodaj-platnosci%]<br>\r\n    Razem do zapłaty: [%wartosc-faktury%]\r\n</div>', 'Szablon faktury');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `szablon_i18n`
--

CREATE TABLE `szablon_i18n` (
  `id` int(11) NOT NULL,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `szablon_i18n`
--

INSERT INTO `szablon_i18n` (`id`, `locale`, `model`, `foreign_key`, `field`, `content`) VALUES
(1, 'pl_PL', 'Szablon', 5, 'temat', 'Potwierdzenie zamówienia'),
(2, 'pl_PL', 'Szablon', 5, 'tresc', '<div style=\"text-align: center;\">\r\n            <img src=\"http://ecopitoriono.projekt-samatix.pl/img/logo.png\" alt=\"Ecopitoriono\" style=\"font-size: 42px;\">\r\n        </div>\r\n        <div style=\"display: block;margin: 15px auto;max-width: 700px;padding: 15px;background-color: #fbfbfb;border: 1px solid #aaaaaa;border-radius: 5px;\">\r\n            <p style=\"font-size: 15px;font-weight: 700;\">Witamy serdecznie!</p>\r\n            <p style=\"font-size: 14px;\">Dziękujemy za skorzystanie z oferty naszego sklepu.</p>\r\n            <p style=\"font-size: 14px;\">Pragniemy poinformować, że Państwa zamówienie złożone dnia: [%zamowienie-data%] otrzymało numer: <b>[%zamowienie-numer%]</b>.</p>\r\n            <p style=\"font-size: 14px;\">Data złożenia zamówienia: <b>[%zamowienie-data%]</b>\r\n                <br>Imię: <b>[%zamowienie-imie%]</b>\r\n                <br>Nazwisko: <b>[%zamowienie-nazwisko%]</b>\r\n                <br>Telefon: <b>[%zamowienie-telefon%]</b>\r\n                <br>E-mail: <b>[%zamowienie-email%]</b>\r\n                <br>Rodzaj wysyłki i sposób płatności: <b>[%zamowienie-platnosc-wysylka%]</b>\r\n            </p>\r\n            <p style=\"font-size: 18px;font-weight: 700;text-align: center;\">Wartość Twojego zamówienia wynosi: <span style=\"font-size: 24px;font-weight: 700;color: #f68a21;\">[%zamowienie-wartosc-razem%]</span></p>\r\n            <hr>\r\n            <p class=\"text-center fs-15 fw-bold\">Poniżej znajdują się informacje dotyczące zamówienia</p>\r\n            <p>[%zamowienie-adres-wysylki%]</p>\r\n            <p>[%zamowienie-adres-faktury%]</p>\r\n            <p>Uwagi do zamówienia: [%zamowienie-uwagi%]<br></p>\r\n            <div>[%paczkomat-info%]</div>\r\n            <div>[%zamowienie-bonus%]</div>\r\n            [%zamowienie-preorder-info%]<div>[%towary%]\r\n                <table class=\"towar-table\" style=\"width:100%;\">\r\n                    <tbody>\r\n                        <tr>\r\n                            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Lp</th>\r\n                            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Nazwa</th>\r\n                            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Kod</th>\r\n                            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Ilość</th>\r\n                            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Cena brutto</th>\r\n                            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;text-align: center;\">Wartość brutto</th>\r\n                        </tr>\r\n                        <tr>\r\n                            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-lp%]</td>\r\n                            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-nazwa%] [%t-platforma%]</td>\r\n                            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-kod%]</td>\r\n                            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-ilosc%] [%t-jednostka%]</td>\r\n                            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-cena%]</td>\r\n                            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;text-align: center;\">[%t-wartosc%]</td>\r\n                        </tr>\r\n\r\n                    </tbody>\r\n                </table>\r\n                [%/towary%]\r\n            </div>\r\n            <div>[%zamowienie-rabaty%]</div>\r\n            <p>Wartość produktów: [%zamowienie-wartosc-produktow%]</p>\r\n            <p>[%zamowienie-platnosc-wysylka%]: [%zamowienie-koszt-wysylki%]</p>\r\n            <div>[%zamowienie-bonus-uzyty%]</div>\r\n            <p style=\"font-size: 18px;font-weight: 700;text-align: center;\">Łączna wartość Twojego zamówienia wynosi: <span style=\"font-size: 24px;font-weight: 700;color: #f68a21;\">[%zamowienie-wartosc-razem%]</span></p>\r\n            <div>\r\n                [%zamowienie-platnosc-info%]</div><div>[%zamowienie-punkt-odbioru%]<br></div><div>\r\n            </div>\r\n            <p>\r\n                ----------------------<br>\r\n                Pozdrawiamy,<br>\r\n                Zespół Ecopitoriono.pl\r\n            </p>\r\n            <p style=\"text-align:center;font-size: 10px;\">Regulamin Platformy znajdą Państwo pod linkiem:&nbsp;<a href=\"http://ecopitoriono.projekt-samatix.pl/pages/view/3/regulamin\" target=\"_blank\" style=\"background-color: rgb(255, 255, 255);\">Regulamin</a></p>\r\n        </div>'),
(3, 'pl_PL', 'Szablon', 13, 'temat', 'Potwierdź rejestrację konta.'),
(4, 'pl_PL', 'Szablon', 13, 'tresc', '<p>Witaj [%uzytkownik-imie%],</p><p>Dziękujemy za rejestracje w naszym serwisie. Aby potwierdzić swoje dane kliknij w link poniżej.</p><p><a href=\"[%uzytkownik-potwierdz-url%]\" target=\"_blank\">[%uzytkownik-potwierdz-url%]</a></p><p><br></p><p>Pozdrawiamy,<br>example.pl</p>'),
(5, 'pl_PL', 'Szablon', 1, 'temat', 'Restart hasła'),
(6, 'pl_PL', 'Szablon', 1, 'tresc', '<p>[%odzyskiwanie-hasla-link%]<br />[%odzyskiwanie-hasla-url%]<br />[%adres-ip%]</p>'),
(7, 'en_US', 'Szablon', 1, 'temat', 'Reset password'),
(8, 'en_US', 'Szablon', 1, 'tresc', '<p>[%odzyskiwanie-hasla-link%]<br>[%odzyskiwanie-hasla-url%]<br>[%adres-ip%]<br></p>'),
(9, 'pl_PL', 'Szablon', 14, 'temat', 'Potwierdź swój adres email.'),
(10, 'pl_PL', 'Szablon', 14, 'tresc', '<div style=\"text-align: center;\">\r\n            <img src=\"http://ecopitoriono.projekt-samatix.pl/img/logo.png\" alt=\"Ecopitoriono\" style=\"font-size: 42px;\">\r\n        </div>\r\n        <div style=\"display: block;margin: 15px auto;max-width: 700px;padding: 15px;background-color: #fbfbfb;border: 1px solid #aaaaaa;border-radius: 5px;\">\r\n            <p style=\"font-size: 15px;font-weight: 700;\">Witamy serdecznie!</p>\r\n            <p style=\"font-size: 14px;\">Dziękujemy za dołączenie do naszego newslettera. Aby potwierdzić chęć otrzymywania od nas informacji o promocjach prosimy o kliknięcie w link potwierdzający</p>\r\n            <p style=\"text-align: center; font-size: 14px;\"><span style=\"font-size: 13px; text-align: start; background-color: rgb(255, 255, 255);\">[%newsletter-potwierdzenie-link%]</span><br></p>\r\n\r\n            <p>\r\n                ----------------------<br>\r\n                Pozdrawiamy,<br>\r\n                Zespół Ecopitoriono.pl\r\n            </p>\r\n            <p style=\"text-align:center;font-size: 10px;\">Regulamin Platformy znajdą Państwo pod linkiem:&nbsp;<a href=\"http://ecopitoriono.projekt-samatix.pl/pages/view/3/regulamin\" target=\"_blank\" style=\"background-color: rgb(255, 255, 255);\">Regulamin</a></p></div>'),
(11, 'en_US', 'Szablon', 14, 'temat', 'Confirm your email'),
(12, 'en_US', 'Szablon', 14, 'tresc', '<p>[%newsletter-potwierdzenie-link%]<br>[%newsletter-potwierdzenie-url%]<br>[%adres-ip%]<br></p>'),
(13, 'pl_PL', 'Szablon', 9, 'temat', 'Nowe zamówienie'),
(14, 'pl_PL', 'Szablon', 9, 'tresc', '<div style=\"text-align: center;\">\r\n            <img src=\"http://ecopitoriono.projekt-samatix.pl/img/logo.png\" alt=\"Ecopitoriono\" style=\"font-size: 42px;\">\r\n        </div>\r\n        <div style=\"display: block;margin: 15px auto;max-width: 700px;padding: 15px;background-color: #fbfbfb;border: 1px solid #aaaaaa;border-radius: 5px;\">\r\n            <p style=\"font-size: 15px;font-weight: 700;\">Witamy serdecznie!</p>\r\n            <p style=\"font-size: 14px;\">W systemie pojawiło się nowe zamówienie. Kliknij <a href=\"http://ecopitoriono.projekt-samatix.pl/admin/zamownienie/view/[%zamowienie-id%]\" target=\"_blank\">TUTAJ</a> aby otworzyć zamówienie w panelu.</p>\r\n            <p style=\"font-size: 14px;\">Data złożenia zamówienia: <b>[%zamowienie-data%]</b></p>\r\n          	<p style=\"font-size: 14px;\">numer:&nbsp;<span style=\"font-weight: 700;\">[%zamowienie-numer%]</span><b><br></b></p>\r\n          	<p style=\"font-size: 14px;\">Imię: <b>[%zamowienie-imie%]</b>\r\n                <br>Nazwisko: <b>[%zamowienie-nazwisko%]</b>\r\n                <br>Telefon: <b>[%zamowienie-telefon%]</b>\r\n                <br>E-mail: <b>[%zamowienie-email%]</b>\r\n                <br>Rodzaj wysyłki i sposób płatności: <b>[%zamowienie-platnosc-wysylka%]</b>\r\n            </p>\r\n            <p style=\"font-size: 18px;font-weight: 700;text-align: center;\">Wartość zamówienia wynosi: <span style=\"font-size: 24px;font-weight: 700;color: #f68a21;\">[%zamowienie-wartosc-razem%]</span></p>\r\n            <hr>\r\n            <p class=\"text-center fs-15 fw-bold\">Poniżej znajdują się informacje dotyczące zamówienia</p>\r\n            <p>[%zamowienie-adres-wysylki%]</p>\r\n            <p>[%zamowienie-adres-faktury%]</p>\r\n            <p>Uwagi do zamówienia: [%zamowienie-uwagi%]<br></p>\r\n            <div>[%paczkomat-info%]</div>\r\n            <div>[%zamowienie-bonus%]</div>\r\n            [%zamowienie-preorder-info%]<div>[%towary%]\r\n                <table class=\"towar-table\" style=\"width:100%;\">\r\n                    <tbody>\r\n                        <tr>\r\n                            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Lp</th>\r\n                            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Nazwa</th>\r\n                            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Kod</th>\r\n                            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Ilość</th>\r\n                            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Cena brutto</th>\r\n                            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;text-align: center;\">Wartość brutto</th>\r\n                        </tr>\r\n                        <tr>\r\n                            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-lp%]</td>\r\n                            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-nazwa%] [%t-platforma%]</td>\r\n                            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-kod%]</td>\r\n                            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-ilosc%] [%t-jednostka%]</td>\r\n                            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-cena%]</td>\r\n                            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;text-align: center;\">[%t-wartosc%]</td>\r\n                        </tr>\r\n\r\n                    </tbody>\r\n                </table>\r\n                [%/towary%]\r\n            </div>\r\n            <div>[%zamowienie-rabaty%]</div>\r\n            <p>Wartość produktów: [%zamowienie-wartosc-produktow%]</p>\r\n            <p>[%zamowienie-platnosc-wysylka%]: [%zamowienie-koszt-wysylki%]</p>\r\n            <div>[%zamowienie-bonus-uzyty%]</div>\r\n            <p style=\"font-size: 18px;font-weight: 700;text-align: center;\">Łączna wartość zamówienia wynosi: <span style=\"font-size: 24px;font-weight: 700;color: #f68a21;\">[%zamowienie-wartosc-razem%]</span></p>\r\n            <div>\r\n                [%zamowienie-platnosc-info%]\r\n            </div>\r\n            [%zamowienie-punkt-odbioru%]<p>----------------------<br>\r\n                Pozdrawiamy,<br>\r\n                Zespół Ecopitoriono.pl\r\n            </p>\r\n            <p style=\"text-align:center;font-size: 10px;\">Regulamin Platformy znajdą Państwo pod linkiem:&nbsp;<a href=\"http://ecopitoriono.projekt-samatix.pl/pages/view/3/regulamin\" target=\"_blank\" style=\"background-color: rgb(255, 255, 255);\">Regulamin</a></p>\r\n        </div>'),
(15, 'pl_PL', 'Szablon', 2, 'temat', 'Twoje zamówienie zostało przyjęte do realizacji'),
(16, 'pl_PL', 'Szablon', 2, 'tresc', '<div style=\"text-align: center;\">\r\n            <img src=\"http://ecopitoriono.projekt-samatix.pl/img/logo.png\" alt=\"Ecopitoriono\" style=\"font-size: 42px;\">\r\n        </div>\r\n        <div style=\"display: block;margin: 15px auto;max-width: 700px;padding: 15px;background-color: #fbfbfb;border: 1px solid #aaaaaa;border-radius: 5px;\">\r\n            <p style=\"font-size: 15px;font-weight: 700;\">Witamy serdecznie!</p>\r\n            <p style=\"font-size: 14px;\">Dziękujemy za skorzystanie z oferty naszego sklepu.</p>\r\n            <p style=\"font-size: 14px;\">Twoje zamówienie nr <span style=\"font-weight: bold;\">[%zamowienie-numer%]</span> otrzymało status <b>przyjęte do realizacji</b>.</p>\r\n            <p style=\"font-size: 14px;\">Przewidywany termin wysyłki zamówionych produktów to:&nbsp;<span style=\"font-weight: bold;\">[%termin-wysylki%]</span></p>\r\n\r\n            <p>\r\n                ----------------------<br>\r\n                Pozdrawiamy,<br>\r\n                Zespół Ecopitoriono.pl\r\n            </p>\r\n            <p style=\"text-align:center;font-size: 10px;\">Regulamin Platformy znajdą Państwo pod linkiem:&nbsp;<a href=\"http://ecopitoriono.projekt-samatix.pl/pages/view/3/regulamin\" target=\"_blank\" style=\"background-color: rgb(255, 255, 255);\">Regulamin</a></p>\r\n        </div>\r\n'),
(17, 'pl_PL', 'Szablon', 3, 'temat', 'Twoje zamówienie [%zamowienie-numer%] zostało wysłane'),
(18, 'pl_PL', 'Szablon', 3, 'tresc', '<div style=\"text-align: center;\">\r\n            <img src=\"http://ecopitoriono.projekt-samatix.pl/img/logo.png\" alt=\"Ecopitoriono\" style=\"font-size: 42px;\">\r\n        </div>\r\n        <div style=\"display: block;margin: 15px auto;max-width: 700px;padding: 15px;background-color: #fbfbfb;border: 1px solid #aaaaaa;border-radius: 5px;\">\r\n            <p style=\"font-size: 15px;font-weight: 700;\">Witamy serdecznie!</p>\r\n            <p style=\"font-size: 14px;\">Dziękujemy za skorzystanie z oferty naszego sklepu.</p>\r\n            <p style=\"font-size: 14px;\">Twoje zamówienie nr <span style=\"font-weight: bold;\">[%zamowienie-numer%]</span>&nbsp;zostało wysłane.</p>\r\n\r\n            <p>\r\n                ----------------------<br>\r\n                Pozdrawiamy,<br>\r\n                Zespół Ecopitoriono.pl\r\n            </p>\r\n            <p style=\"text-align:center;font-size: 10px;\">Regulamin Platformy znajdą Państwo pod linkiem:&nbsp;<a href=\"http://ecopitoriono.projekt-samatix.pl/pages/view/3/regulamin\" target=\"_blank\" style=\"background-color: rgb(255, 255, 255);\">Regulamin</a></p>\r\n        </div>\r\n'),
(19, 'pl_PL', 'Szablon', 4, 'temat', 'Twoje zamówienie zostało anulowane'),
(20, 'pl_PL', 'Szablon', 4, 'tresc', '<div style=\"text-align: center;\">\r\n            <img src=\"http://ecopitoriono.projekt-samatix.pl/img/logo.png\" alt=\"Ecopitoriono\" style=\"font-size: 42px;\">\r\n        </div>\r\n        <div style=\"display: block;margin: 15px auto;max-width: 700px;padding: 15px;background-color: #fbfbfb;border: 1px solid #aaaaaa;border-radius: 5px;\">\r\n            <p style=\"font-size: 15px;font-weight: 700;\">Witamy serdecznie!</p>\r\n            <p style=\"font-size: 14px;\">Dziękujemy za skorzystanie z oferty naszego sklepu.</p>\r\n            <p style=\"font-size: 14px;\">Twoje zamówienie nr <span style=\"font-weight: bold;\">[%zamowienie-numer%]</span>&nbsp;zostało anulowane.</p>\r\n\r\n            <p>\r\n                ----------------------<br>\r\n                Pozdrawiamy,<br>\r\n                Zespół Ecopitoriono.pl\r\n            </p>\r\n            <p style=\"text-align:center;font-size: 10px;\">Regulamin Platformy znajdą Państwo pod linkiem:&nbsp;<a href=\"http://ecopitoriono.projekt-samatix.pl/pages/view/3/regulamin\" target=\"_blank\" style=\"background-color: rgb(255, 255, 255);\">Regulamin</a></p>\r\n        </div>\r\n'),
(21, 'pl_PL', 'Szablon', 6, 'temat', 'Zamówienie zostało wysłane'),
(22, 'pl_PL', 'Szablon', 6, 'tresc', '<div style=\"text-align: center;\">\r\n            <img src=\"http://ecopitoriono.projekt-samatix.pl/img/logo.png\" alt=\"Ecopitoriono\" style=\"font-size: 42px;\">\r\n        </div>\r\n        <div style=\"display: block;margin: 15px auto;max-width: 700px;padding: 15px;background-color: #fbfbfb;border: 1px solid #aaaaaa;border-radius: 5px;\">\r\n            <p style=\"font-size: 15px;font-weight: 700;\">Witamy serdecznie!</p>\r\n            <p style=\"font-size: 14px;\">Dziękujemy za skorzystanie z oferty naszego sklepu.</p>\r\n            <p style=\"font-size: 14px;\">Twoje zamówienie nr <span style=\"font-weight: bold;\">[%zamowienie-numer%]</span>&nbsp;zostało wysłane.</p><p style=\"font-size: 14px;\">Numer listu przewozowego: <b>[%nr-listu-przewozowego%]</b></p><p style=\"text-align: center; font-size: 14px;\"><span style=\"color: rgb(255, 0, 0);\">Możesz monitorować przesyłkę korzystając z poniższego linku</span></p><p style=\"text-align: center; \"><span style=\"font-size: 14px;\"><b>[%link-sledzenia%]</b></span><br></p>\r\n\r\n            <p>\r\n                ----------------------<br>\r\n                Pozdrawiamy,<br>\r\n                Zespół Ecopitoriono.pl\r\n            </p>\r\n            <p style=\"text-align:center;font-size: 10px;\">Regulamin Platformy znajdą Państwo pod linkiem:&nbsp;<a href=\"http://ecopitoriono.projekt-samatix.pl/pages/view/3/regulamin\" target=\"_blank\" style=\"background-color: rgb(255, 255, 255);\">Regulamin</a></p>\r\n        </div>\r\n'),
(23, 'pl_PL', 'Szablon', 15, 'temat', 'Otrzymaliśmy Twoje zapytanie'),
(24, 'pl_PL', 'Szablon', 15, 'tresc', '<div style=\"text-align: center;\">\r\n            <img src=\"http://ecopitoriono.projekt-samatix.pl/img/logo.png\" alt=\"Ecopitoriono\" style=\"font-size: 42px;\">\r\n        </div>\r\n        <div style=\"display: block;margin: 15px auto;max-width: 700px;padding: 15px;background-color: #fbfbfb;border: 1px solid #aaaaaa;border-radius: 5px;\">\r\n            <p style=\"font-size: 15px;font-weight: 700;\">Witamy serdecznie!</p>\r\n            <p style=\"font-size: 14px;\">Dziękujemy za kontakt.</p>\r\n            <p style=\"font-size: 14px;\">Twoje zgłoszenie otrzymało numer&nbsp;<span style=\"font-size: 13px; background-color: rgb(255, 255, 255);\"><b>[%nr_zgloszenia%]</b></span>.</p>\r\n\r\n            <p>\r\n                ----------------------<br>\r\n                Pozdrawiamy,<br>\r\n                Zespół Ecopitoriono.pl\r\n            </p>\r\n            <p style=\"text-align:center;font-size: 10px;\">Regulamin Platformy znajdą Państwo pod linkiem:&nbsp;<a href=\"http://ecopitoriono.projekt-samatix.pl/pages/view/3/regulamin\" target=\"_blank\" style=\"background-color: rgb(255, 255, 255);\">Regulamin</a></p></div>'),
(25, 'pl_PL', 'Szablon', 16, 'temat', 'Szablon karty podarunkowej'),
(26, 'pl_PL', 'Szablon', 16, 'tresc', '        <div style=\"display: block;margin: 15px;padding: 15px;background-color: #fbfbfb;\">\r\n          <div style=\"text-align: center; margin-top: 15px; margin-bottom:15px;\">\r\n            <img src=\"http://ecopitoriono.projekt-samatix.pl/img/logo.png\" alt=\"Ecopitoriono\" style=\"font-size: 42px;\">\r\n        </div>\r\n            <p style=\"text-align: center;\"><span style=\"font-size: 24px;\"><span style=\"font-weight: 700;\">[%karta_podarunkowa-nazwa%]</span></span></p><p style=\"text-align: center;\"><span style=\"font-size: 36px;\"><span style=\"font-weight: 700;\">Wartość karty: [%wartosc%] zł</span></span></p><p style=\"text-align: center;\"><span style=\"font-size: 18px;\">Twój kod:&nbsp;<span style=\"font-weight: 700;\">[%kod%]</span></span></p><p style=\"text-align: center;\"><span style=\"font-size: 18px;\">Kod pin:&nbsp;<span style=\"font-weight: 700;\">[%pin%]</span></span></p><p style=\"text-align: center;\"><span style=\"font-size: 12px;\">Aby wykorzystać kartę wejdź na stronę <b>www.exgame.pl</b></span></p></div>'),
(27, 'pl_PL', 'Szablon', 17, 'temat', 'Twoje karty podarunkowe'),
(28, 'pl_PL', 'Szablon', 17, 'tresc', '<div style=\"text-align: center;\">\r\n            <img src=\"http://ecopitoriono.projekt-samatix.pl/img/logo.png\" alt=\"Ecopitoriono\" style=\"font-size: 42px;\">\r\n        </div>\r\n        <div style=\"display: block;margin: 15px auto;max-width: 700px;padding: 15px;background-color: #fbfbfb;border: 1px solid #aaaaaa;border-radius: 5px;\">\r\n            <p style=\"font-size: 15px;font-weight: 700;\">Witamy serdecznie!</p>\r\n            <p style=\"font-size: 14px;\">W załączniku przesyłamy karty podarunkowe z zamówienia&nbsp;<b>[%zamowienie-numer%]</b>.<br></p>\r\n            <p style=\"font-size: 14px;\">Aby wykorzystać kartę podarunkową należy założyć konto w naszym serwisie i wprowadzić kod oraz pin z karty w panelu użytkownika.</p><div>\r\n            </div>\r\n            <p>\r\n                ----------------------<br>\r\n                Pozdrawiamy,<br>\r\n                Zespół Ecopitoriono.pl\r\n            </p>\r\n            <p style=\"text-align:center;font-size: 10px;\">Regulamin Platformy znajdą Państwo pod linkiem:&nbsp;<a href=\"http://ecopitoriono.projekt-samatix.pl/pages/view/3/regulamin\" target=\"_blank\" style=\"background-color: rgb(255, 255, 255);\">Regulamin</a></p>\r\n        </div>'),
(29, 'pl_PL', 'Szablon', 18, 'temat', 'Produkt jest dostępny'),
(30, 'pl_PL', 'Szablon', 18, 'tresc', '<p>Witaj [%imie%],</p><p>Informujemy, że produkt [%towar%] jest <b>dostępny </b>w punkcie odbioru.</p><p>Produkt jest możliwy do odebrania pod adresem<br>[%adres%]</p>'),
(31, 'pl_PL', 'Szablon', 19, 'temat', 'Produkt jest niedostępny'),
(32, 'pl_PL', 'Szablon', 19, 'tresc', '<p>Witaj [%imie%],</p><p>Z przykrością informujemy, że produkt [%towar%] jest&nbsp;<span style=\"font-weight: 700;\">niedostępny</span>.</p>'),
(33, 'pl_PL', 'Szablon', 20, 'temat', 'Odpowiedź na zapytanie o premierę'),
(34, 'pl_PL', 'Szablon', 20, 'tresc', '<p>Witaj [%imie%],</p><p>Informujemy, że produkt [%towar%] będzie dostępny w sprzedaży po dacie premiery która przypada na [%data-premiery%].</p><p>Przewidujemy uruchomienie preorderów od [%data-preorder%]</p>'),
(35, 'pl_PL', 'Szablon', 21, 'temat', 'Produkt premierowy jest niedostępny'),
(36, 'pl_PL', 'Szablon', 21, 'tresc', '<p>Witaj [%imie%],</p><p>Z przykrością informujemy, że produkt aktualnie nie wiemy kiedy produkt będzie dostępny w sprzedaży.</p>'),
(37, 'pl_PL', 'Szablon', 22, 'temat', 'Odpowiedź na zapytanie nr: [%nr_zgloszenia%]'),
(38, 'pl_PL', 'Szablon', 22, 'tresc', '<p>Witaj [%imie%],</p><p>W odpowiedzi na zapytanie [%nr_zgloszenia%] z dnia [%data_dodania_data%] godz. [%data_dodania_czas%],</p><p>informujemy że aktualnie nie prowadzimy sprzedaży wskazanej gry.</p><p><br></p><p>--------------------------------------------------------------------------------------------------------------<br>Treść Twojego zgłoszenia:</p><p>[%tresc%]</p>'),
(39, 'pl_PL', 'Szablon', 23, 'temat', 'Powiadomienie o cenie'),
(40, 'pl_PL', 'Szablon', 23, 'tresc', '<p>Witaj [%imie%],</p><p>Informujemy, że cena produktu [%produkt%] zmieniła się z [%stara_cena%] na [%nowa_cena%]</p><p>Zapraszamy do zakupów</p>'),
(41, 'pl_PL', 'Szablon', 24, 'temat', 'Twoje zapytanie o produkt'),
(42, 'pl_PL', 'Szablon', 24, 'tresc', '<p>Witaj [%imie%],</p><p>W odpowiedzi na Twoje zapytanie o produkt [%towar%]</p><p>Informujemy, że:</p><p>[tutaj wpisz treść odpowiedzi]</p>'),
(43, 'pl_PL', 'Szablon', 25, 'temat', 'Twoje zapytanie o produkt'),
(44, 'pl_PL', 'Szablon', 25, 'tresc', '<p>Witaj [%imie%],</p><p>W odpowiedzi na Twoje zapytanie o produkt [%towar%]</p><p>Informujemy, że:</p><p>[tutaj wpisz treść odpowiedzi]</p>'),
(45, 'pl_PL', 'Szablon', 26, 'temat', 'Szablon faktury'),
(46, 'pl_PL', 'Szablon', 26, 'tresc', '<div style=\"text-align:right; margin-bottom:20px;\">\r\n    Miejsce wystawienia: [%miejsce-wystawienia%]<br>\r\n    Data wystawienia: [%data-wystawienia%]<br>\r\n    Data sprzedaży: [%data-sprzedazy%]<br>\r\n    Termin płatności: [%termin-platnosci%]\r\n</div>\r\n<table style=\"width:100%;table-layout: fixed;\">\r\n    <tbody>\r\n        <tr>\r\n            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Sprzedawca</th>\r\n            <th style=\"width:100px;\"></th>\r\n            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Nabywca</th>\r\n        </tr>\r\n        <tr>\r\n            <td>[%dane-sklepu%]</td>\r\n            <td></td>\r\n            <td>[%dane-klienta%]</td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<table style=\"width:100%;table-layout: fixed;\">\r\n    <tbody>\r\n        <tr>\r\n            <td style=\"text-align:center;\">Faktura numer [%numer%]<br>[%podtytul%]</td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n[%towary%]\r\n<table style=\"width:100%;\">\r\n    <tbody>\r\n        <tr>\r\n            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Lp</th>\r\n            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Nazwa</th>\r\n            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Ilość</th>\r\n            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">j.m.</th>\r\n            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">VAT [%]</th>\r\n            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Cena jednostkowa brutto</th>\r\n            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Wartość netto</th>\r\n            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;\">Wartość VAT</th>\r\n            <th style=\"padding: 5px; border:1px solid #353535; color: #ffffff; background:#353535;text-align: center;\">Wartość brutto</th>\r\n        </tr>\r\n        <tr>\r\n            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-lp%]</td>\r\n            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-nazwa%]&nbsp;</td>\r\n            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-ilosc%]</td>\r\n            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-jednostka%]</td>\r\n            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-vat%]</td>\r\n            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-cena-brutto%]</td>\r\n            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-wartosc-netto%]</td>\r\n            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-wartosc-vat%]</td>\r\n            <td style=\"padding: 5px; border:1px solid #353535; background-color: #ffffff; color: #000000;\">[%t-wartosc-brutto%]</td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n[%/towary%]\r\n<div style=\"margin-top:20px; text-align:right;\">\r\n    Rodzaj płatności: [%rodaj-platnosci%]<br>\r\n    Razem do zapłaty: [%wartosc-faktury%]\r\n</div>');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `towar`
--

CREATE TABLE `towar` (
  `id` int(11) NOT NULL,
  `kod` varchar(50) DEFAULT NULL,
  `ilosc` int(11) NOT NULL,
  `max_ilosc` int(11) DEFAULT NULL,
  `producent_id` int(11) DEFAULT NULL,
  `platforma_id` int(11) DEFAULT NULL,
  `wersja_id` int(11) DEFAULT NULL,
  `ukryty` tinyint(4) DEFAULT '0',
  `promocja` tinyint(4) DEFAULT '0',
  `polecany` tinyint(4) DEFAULT '0',
  `wyprzedaz` tinyint(4) DEFAULT '0',
  `wyrozniony` tinyint(4) DEFAULT '0',
  `nowosc` tinyint(4) DEFAULT '0',
  `produkt_tygodnia` tinyint(1) DEFAULT '0',
  `nazwa` varchar(255) NOT NULL,
  `nazwa_promocja` varchar(50) DEFAULT NULL,
  `opis` longtext,
  `opis2` longtext,
  `seo_tytul` varchar(255) DEFAULT NULL,
  `seo_slowa` varchar(255) DEFAULT NULL,
  `seo_opis` text,
  `zestaw_nazwa` varchar(100) DEFAULT NULL,
  `ean` varchar(100) DEFAULT NULL,
  `tagi` text,
  `waga` float DEFAULT '0',
  `opinia_avg` float DEFAULT NULL,
  `wysylka_info_dostepny` varchar(20) DEFAULT NULL,
  `wysylka_info_niedostepny` varchar(20) DEFAULT NULL,
  `data_utworzenia` datetime DEFAULT NULL,
  `data_modyfikacji` datetime DEFAULT NULL,
  `data_pobrania` datetime DEFAULT NULL,
  `pegi_wiek` int(11) DEFAULT NULL,
  `pegi_typ` varchar(120) DEFAULT NULL,
  `data_premiery` date DEFAULT NULL,
  `preorder` tinyint(1) DEFAULT NULL,
  `preorder_od` datetime DEFAULT NULL,
  `preorder_do` datetime DEFAULT NULL,
  `preorder_limit` int(11) DEFAULT NULL,
  `bonus_id` int(11) DEFAULT NULL,
  `p_skapiec` tinyint(1) DEFAULT '1',
  `p_google` tinyint(1) DEFAULT '1',
  `p_ceneo` tinyint(1) DEFAULT '1',
  `ilosc_log` text,
  `hurtownia` varchar(50) DEFAULT NULL,
  `get_date` datetime DEFAULT NULL,
  `hurt_id` int(11) DEFAULT NULL,
  `wiek_id` int(11) DEFAULT NULL,
  `opakowanie_id` int(11) DEFAULT NULL,
  `plec` varchar(10) DEFAULT NULL,
  `gwarancja` varchar(50) DEFAULT NULL,
  `zwrot` varchar(50) DEFAULT NULL,
  `punkty` int(11) DEFAULT NULL,
  `gratis` tinyint(1) DEFAULT NULL,
  `gratis_wartosc` float DEFAULT NULL,
  `wymiary` varchar(100) DEFAULT NULL,
  `wymiary_opakowania` varchar(100) DEFAULT NULL,
  `waga_kg` varchar(100) DEFAULT NULL,
  `baterie` varchar(100) DEFAULT NULL,
  `ilosc_w_kartonie` int(11) DEFAULT '1',
  `prezent` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `towar`
--

INSERT INTO `towar` (`id`, `kod`, `ilosc`, `max_ilosc`, `producent_id`, `platforma_id`, `wersja_id`, `ukryty`, `promocja`, `polecany`, `wyprzedaz`, `wyrozniony`, `nowosc`, `produkt_tygodnia`, `nazwa`, `nazwa_promocja`, `opis`, `opis2`, `seo_tytul`, `seo_slowa`, `seo_opis`, `zestaw_nazwa`, `ean`, `tagi`, `waga`, `opinia_avg`, `wysylka_info_dostepny`, `wysylka_info_niedostepny`, `data_utworzenia`, `data_modyfikacji`, `data_pobrania`, `pegi_wiek`, `pegi_typ`, `data_premiery`, `preorder`, `preorder_od`, `preorder_do`, `preorder_limit`, `bonus_id`, `p_skapiec`, `p_google`, `p_ceneo`, `ilosc_log`, `hurtownia`, `get_date`, `hurt_id`, `wiek_id`, `opakowanie_id`, `plec`, `gwarancja`, `zwrot`, `punkty`, `gratis`, `gratis_wartosc`, `wymiary`, `wymiary_opakowania`, `waga_kg`, `baterie`, `ilosc_w_kartonie`, `prezent`) VALUES
(1, 'ast2665', 71, NULL, 1, NULL, NULL, 0, 1, 1, 1, 1, 1, 0, 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', '', '<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec accumsan venenatis diam, at finibus mi faucibus eu. Pellentesque vel diam ullamcorper, viverra odio pharetra, consequat purus. Etiam laoreet sagittis tortor eu ultrices. Nullam aliquet velit metus, at luctus libero sodales eu. Etiam sit amet sapien sit amet ex pulvinar placerat vel ac quam. In in faucibus quam, vestibulum convallis nulla. Fusce ut elit tellus. Aliquam erat volutpat. Duis vel dictum risus. Nulla facilisi. Ut euismod viverra elit.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Pellentesque tempor imperdiet nisi, a rhoncus massa malesuada id. Donec at elit tincidunt, tincidunt mi vel, suscipit felis. Aenean id odio quam. Etiam tristique rhoncus ex, sed viverra erat tincidunt eget. Quisque quis laoreet lacus. Duis quis purus vel enim rutrum tincidunt. Etiam dignissim orci augue, at blandit lacus scelerisque at. Donec suscipit orci mattis nisl sodales consequat. Nunc sit amet leo pellentesque, dictum nulla vel, tristique tortor. Duis luctus et justo non laoreet. Curabitur faucibus interdum velit quis auctor.</p>', '<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec accumsan venenatis diam, at finibus mi faucibus eu. Pellentesque vel diam ullamcorper, viverra odio pharetra, consequat purus. Etiam laoreet sagittis tortor eu ultrices. Nullam aliquet velit metus, at luctus libero sodales eu. Etiam sit amet sapien sit amet ex pulvinar placerat vel ac quam. In in faucibus quam, vestibulum convallis nulla. Fusce ut elit tellus. Aliquam erat volutpat. Duis vel dictum risus. Nulla facilisi. Ut euismod viverra elit.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Pellentesque tempor imperdiet nisi, a rhoncus massa malesuada id. Donec at elit tincidunt, tincidunt mi vel, suscipit felis. Aenean id odio quam. Etiam tristique rhoncus ex, sed viverra erat tincidunt eget. Quisque quis laoreet lacus. Duis quis purus vel enim rutrum tincidunt. Etiam dignissim orci augue, at blandit lacus scelerisque at. Donec suscipit orci mattis nisl sodales consequat. Nunc sit amet leo pellentesque, dictum nulla vel, tristique tortor. Duis luctus et justo non laoreet. Curabitur faucibus interdum velit quis auctor.</p>', '', '', '', 'Klocki lego test', '561613265165165', '', 3, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, '[\"2020-01-08 15:34:48 - Zmiana ilosci z 5 na 0\",\"2020-01-09 14:07:07 - Zmiana ilosci z 0 na 80\"]', NULL, NULL, NULL, 3, 2, 'girl', '2 lata', '365 dni na zwrot', 15, 1, 350, '30x20x15 cm', '31x21x16 cm', '2,5 kg', '3x AAA (brak w zestawie)', 8, 1),
(2, '', 3, NULL, NULL, NULL, NULL, 0, 1, 1, 1, 1, 1, 0, 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', '', '', '', '', '', '', '', '', '', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0),
(3, '', 3, NULL, NULL, NULL, NULL, 0, 1, 1, 1, 1, 1, 0, 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', '', '', '', '', '', '', '', '', '', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, '[\"2020-03-17 10:51:50 - Zmiana ilosci z 1 na 3\"]', NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, 0, NULL, '', '', '', '', 1, 1),
(4, '', 3, NULL, NULL, NULL, NULL, 0, 1, 1, 1, 1, 1, 0, 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', '', '', '', '', '', '', '', '', '', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0),
(5, '', 3, NULL, NULL, NULL, NULL, 0, 1, 1, 1, 1, 1, 0, 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', '', '', '', '', '', '', '', '', '', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0),
(6, '', 3, NULL, NULL, NULL, NULL, 0, 1, 1, 1, 1, 1, 0, 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', '', '', '', '', '', '', '', '', '', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0),
(7, '', 3, NULL, NULL, NULL, NULL, 0, 1, 1, 1, 1, 1, 0, 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', '', '', '', '', '', '', '', '', '', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0),
(8, '', 3, NULL, NULL, NULL, NULL, 0, 1, 1, 1, 1, 1, 0, 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', '', '', '', '', '', '', '', '', '', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0),
(9, '', 1, NULL, NULL, NULL, NULL, 0, 1, 1, 1, 1, 1, 0, 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', '', '', '', '', '', '', '', '', '', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0),
(10, '', 0, NULL, NULL, NULL, NULL, 0, 1, 1, 1, 1, 1, 0, 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', '', '', '', '', '', '', '', '', '', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, '[\"2019-12-30 10:05:52 - Zmiana ilosci z 5 na 0\"]', NULL, NULL, NULL, 4, 3, 'boy', '2 lata', '30 dni na zwrot', 25, 1, 350, NULL, NULL, NULL, NULL, 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `towar_akcesoria`
--

CREATE TABLE `towar_akcesoria` (
  `id` int(11) NOT NULL,
  `towar_id` int(11) NOT NULL,
  `podobny_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `towar_atrybut`
--

CREATE TABLE `towar_atrybut` (
  `id` int(11) NOT NULL,
  `atrybut_id` int(11) NOT NULL,
  `towar_id` int(11) NOT NULL,
  `wartosc` float DEFAULT NULL,
  `atrybut_podrzedne_id` int(11) DEFAULT NULL,
  `wariant` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `towar_cena`
--

CREATE TABLE `towar_cena` (
  `id` int(11) NOT NULL,
  `towar_id` int(11) NOT NULL,
  `waga` float(6,2) DEFAULT '0.00',
  `cena_katalogowa` float DEFAULT NULL,
  `cena_sprzedazy` float NOT NULL,
  `cena_promocja` float DEFAULT NULL,
  `jednostka_id` int(11) NOT NULL,
  `vat_id` int(11) NOT NULL,
  `rodzaj` enum('netto','brutto') DEFAULT 'brutto',
  `waluta_id` int(11) DEFAULT NULL,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `aktywna` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `towar_cena`
--

INSERT INTO `towar_cena` (`id`, `towar_id`, `waga`, `cena_katalogowa`, `cena_sprzedazy`, `cena_promocja`, `jednostka_id`, `vat_id`, `rodzaj`, `waluta_id`, `uzytkownik_id`, `aktywna`) VALUES
(1, 1, 0.00, 0, 130, 120, 1, 3, 'brutto', 1, NULL, 1),
(2, 2, 0.00, 0, 130, NULL, 1, 3, 'brutto', 1, NULL, 1),
(3, 3, 0.00, 0, 130, NULL, 1, 3, 'brutto', 1, NULL, 1),
(4, 4, 0.00, 0, 130, NULL, 1, 3, 'brutto', 1, NULL, 1),
(5, 5, 0.00, 0, 130, NULL, 1, 3, 'brutto', 1, NULL, 1),
(6, 6, 0.00, 0, 130, NULL, 1, 3, 'brutto', 1, NULL, 1),
(7, 7, 0.00, 0, 130, NULL, 1, 3, 'brutto', 1, NULL, 1),
(8, 8, 0.00, 0, 130, NULL, 1, 3, 'brutto', 1, NULL, 1),
(9, 9, 0.00, 0, 130, NULL, 1, 3, 'brutto', 1, NULL, 1),
(10, 10, 0.00, 0, 130, 100, 1, 3, 'brutto', 1, NULL, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `towar_certyfikat`
--

CREATE TABLE `towar_certyfikat` (
  `id` int(11) NOT NULL,
  `towar_id` int(11) NOT NULL,
  `certyfikat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `towar_certyfikat`
--

INSERT INTO `towar_certyfikat` (`id`, `towar_id`, `certyfikat_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 6),
(5, 1, 8),
(6, 1, 9),
(7, 1, 4),
(8, 1, 5),
(9, 1, 7),
(10, 1, 10),
(11, 10, 1),
(12, 10, 4);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `towar_gatunek`
--

CREATE TABLE `towar_gatunek` (
  `id` int(11) NOT NULL,
  `towar_id` int(11) NOT NULL,
  `gatunek_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `towar_i18n`
--

CREATE TABLE `towar_i18n` (
  `id` int(11) NOT NULL,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `towar_i18n`
--

INSERT INTO `towar_i18n` (`id`, `locale`, `model`, `foreign_key`, `field`, `content`) VALUES
(1, 'pl_PL', 'Towar', 1, 'nazwa', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis'),
(2, 'pl_PL', 'Towar', 1, 'opis', '<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec accumsan venenatis diam, at finibus mi faucibus eu. Pellentesque vel diam ullamcorper, viverra odio pharetra, consequat purus. Etiam laoreet sagittis tortor eu ultrices. Nullam aliquet velit metus, at luctus libero sodales eu. Etiam sit amet sapien sit amet ex pulvinar placerat vel ac quam. In in faucibus quam, vestibulum convallis nulla. Fusce ut elit tellus. Aliquam erat volutpat. Duis vel dictum risus. Nulla facilisi. Ut euismod viverra elit.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Pellentesque tempor imperdiet nisi, a rhoncus massa malesuada id. Donec at elit tincidunt, tincidunt mi vel, suscipit felis. Aenean id odio quam. Etiam tristique rhoncus ex, sed viverra erat tincidunt eget. Quisque quis laoreet lacus. Duis quis purus vel enim rutrum tincidunt. Etiam dignissim orci augue, at blandit lacus scelerisque at. Donec suscipit orci mattis nisl sodales consequat. Nunc sit amet leo pellentesque, dictum nulla vel, tristique tortor. Duis luctus et justo non laoreet. Curabitur faucibus interdum velit quis auctor.</p>'),
(3, 'pl_PL', 'Towar', 1, 'opis2', '<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec accumsan venenatis diam, at finibus mi faucibus eu. Pellentesque vel diam ullamcorper, viverra odio pharetra, consequat purus. Etiam laoreet sagittis tortor eu ultrices. Nullam aliquet velit metus, at luctus libero sodales eu. Etiam sit amet sapien sit amet ex pulvinar placerat vel ac quam. In in faucibus quam, vestibulum convallis nulla. Fusce ut elit tellus. Aliquam erat volutpat. Duis vel dictum risus. Nulla facilisi. Ut euismod viverra elit.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Pellentesque tempor imperdiet nisi, a rhoncus massa malesuada id. Donec at elit tincidunt, tincidunt mi vel, suscipit felis. Aenean id odio quam. Etiam tristique rhoncus ex, sed viverra erat tincidunt eget. Quisque quis laoreet lacus. Duis quis purus vel enim rutrum tincidunt. Etiam dignissim orci augue, at blandit lacus scelerisque at. Donec suscipit orci mattis nisl sodales consequat. Nunc sit amet leo pellentesque, dictum nulla vel, tristique tortor. Duis luctus et justo non laoreet. Curabitur faucibus interdum velit quis auctor.</p>'),
(4, 'pl_PL', 'Towar', 1, 'seo_tytul', ''),
(5, 'pl_PL', 'Towar', 1, 'seo_slowa', ''),
(6, 'pl_PL', 'Towar', 1, 'seo_opis', ''),
(7, 'pl_PL', 'Towar', 1, 'zestaw_nazwa', 'Klocki lego test'),
(8, 'pl_PL', 'Towar', 2, 'nazwa', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis'),
(9, 'pl_PL', 'Towar', 2, 'opis', ''),
(10, 'pl_PL', 'Towar', 2, 'opis2', ''),
(11, 'pl_PL', 'Towar', 2, 'seo_tytul', ''),
(12, 'pl_PL', 'Towar', 2, 'seo_slowa', ''),
(13, 'pl_PL', 'Towar', 2, 'seo_opis', ''),
(14, 'pl_PL', 'Towar', 2, 'zestaw_nazwa', ''),
(15, 'pl_PL', 'Towar', 3, 'nazwa', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis'),
(16, 'pl_PL', 'Towar', 3, 'opis', ''),
(17, 'pl_PL', 'Towar', 3, 'opis2', ''),
(18, 'pl_PL', 'Towar', 3, 'seo_tytul', ''),
(19, 'pl_PL', 'Towar', 3, 'seo_slowa', ''),
(20, 'pl_PL', 'Towar', 3, 'seo_opis', ''),
(21, 'pl_PL', 'Towar', 3, 'zestaw_nazwa', ''),
(22, 'pl_PL', 'Towar', 4, 'nazwa', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis'),
(23, 'pl_PL', 'Towar', 4, 'opis', ''),
(24, 'pl_PL', 'Towar', 4, 'opis2', ''),
(25, 'pl_PL', 'Towar', 4, 'seo_tytul', ''),
(26, 'pl_PL', 'Towar', 4, 'seo_slowa', ''),
(27, 'pl_PL', 'Towar', 4, 'seo_opis', ''),
(28, 'pl_PL', 'Towar', 4, 'zestaw_nazwa', ''),
(29, 'pl_PL', 'Towar', 5, 'nazwa', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis'),
(30, 'pl_PL', 'Towar', 5, 'opis', ''),
(31, 'pl_PL', 'Towar', 5, 'opis2', ''),
(32, 'pl_PL', 'Towar', 5, 'seo_tytul', ''),
(33, 'pl_PL', 'Towar', 5, 'seo_slowa', ''),
(34, 'pl_PL', 'Towar', 5, 'seo_opis', ''),
(35, 'pl_PL', 'Towar', 5, 'zestaw_nazwa', ''),
(36, 'pl_PL', 'Towar', 6, 'nazwa', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis'),
(37, 'pl_PL', 'Towar', 6, 'opis', ''),
(38, 'pl_PL', 'Towar', 6, 'opis2', ''),
(39, 'pl_PL', 'Towar', 6, 'seo_tytul', ''),
(40, 'pl_PL', 'Towar', 6, 'seo_slowa', ''),
(41, 'pl_PL', 'Towar', 6, 'seo_opis', ''),
(42, 'pl_PL', 'Towar', 6, 'zestaw_nazwa', ''),
(43, 'pl_PL', 'Towar', 7, 'nazwa', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis'),
(44, 'pl_PL', 'Towar', 7, 'opis', ''),
(45, 'pl_PL', 'Towar', 7, 'opis2', ''),
(46, 'pl_PL', 'Towar', 7, 'seo_tytul', ''),
(47, 'pl_PL', 'Towar', 7, 'seo_slowa', ''),
(48, 'pl_PL', 'Towar', 7, 'seo_opis', ''),
(49, 'pl_PL', 'Towar', 7, 'zestaw_nazwa', ''),
(50, 'pl_PL', 'Towar', 8, 'nazwa', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis'),
(51, 'pl_PL', 'Towar', 8, 'opis', ''),
(52, 'pl_PL', 'Towar', 8, 'opis2', ''),
(53, 'pl_PL', 'Towar', 8, 'seo_tytul', ''),
(54, 'pl_PL', 'Towar', 8, 'seo_slowa', ''),
(55, 'pl_PL', 'Towar', 8, 'seo_opis', ''),
(56, 'pl_PL', 'Towar', 8, 'zestaw_nazwa', ''),
(57, 'pl_PL', 'Towar', 9, 'nazwa', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis'),
(58, 'pl_PL', 'Towar', 9, 'opis', ''),
(59, 'pl_PL', 'Towar', 9, 'opis2', ''),
(60, 'pl_PL', 'Towar', 9, 'seo_tytul', ''),
(61, 'pl_PL', 'Towar', 9, 'seo_slowa', ''),
(62, 'pl_PL', 'Towar', 9, 'seo_opis', ''),
(63, 'pl_PL', 'Towar', 9, 'zestaw_nazwa', ''),
(64, 'pl_PL', 'Towar', 10, 'nazwa', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis'),
(65, 'pl_PL', 'Towar', 10, 'opis', ''),
(66, 'pl_PL', 'Towar', 10, 'opis2', ''),
(67, 'pl_PL', 'Towar', 10, 'seo_tytul', ''),
(68, 'pl_PL', 'Towar', 10, 'seo_slowa', ''),
(69, 'pl_PL', 'Towar', 10, 'seo_opis', ''),
(70, 'pl_PL', 'Towar', 10, 'zestaw_nazwa', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `towar_kategoria`
--

CREATE TABLE `towar_kategoria` (
  `id` int(11) NOT NULL,
  `towar_id` int(11) NOT NULL,
  `kategoria_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `towar_kategoria`
--

INSERT INTO `towar_kategoria` (`id`, `towar_id`, `kategoria_id`) VALUES
(1, 1, 3),
(2, 2, 3),
(3, 3, 3),
(4, 4, 3),
(5, 5, 3),
(6, 6, 3),
(7, 7, 3),
(8, 8, 3),
(9, 9, 3),
(10, 10, 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `towar_opinia`
--

CREATE TABLE `towar_opinia` (
  `id` int(11) NOT NULL,
  `towar_id` int(11) DEFAULT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `autor` varchar(50) NOT NULL,
  `ocena` int(11) NOT NULL DEFAULT '5',
  `opinia` text NOT NULL,
  `plusy` text,
  `minusy` text,
  `jezyk` varchar(10) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `towar_opinia`
--

INSERT INTO `towar_opinia` (`id`, `towar_id`, `data`, `autor`, `ocena`, `opinia`, `plusy`, `minusy`, `jezyk`, `status`) VALUES
(1, 1, '2019-12-29 17:22:56', 'test', 6, 'test', NULL, NULL, 'pl_PL', 2),
(2, 1, '2019-12-29 17:23:09', 'test 2', 3, 'test 2', NULL, NULL, 'pl_PL', 2),
(3, 1, '2019-12-29 17:23:24', 'test 3', 2, 'test 3', NULL, NULL, 'pl_PL', 2),
(4, 1, '2019-12-29 17:28:14', 'test', 3, 'test', NULL, NULL, 'pl_PL', 2),
(5, 1, '2019-12-29 17:29:08', 'test', 3, 'test', NULL, NULL, 'pl_PL', 2),
(6, 1, '2019-12-29 17:30:17', 'test', 2, 'test', NULL, NULL, 'pl_PL', 2),
(7, 1, '2019-12-29 17:31:01', 'test', 1, 'test', NULL, NULL, 'pl_PL', 2),
(8, 1, '2019-12-29 17:32:54', 'test', 3, 'test', NULL, NULL, 'pl_PL', 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `towar_podobne`
--

CREATE TABLE `towar_podobne` (
  `id` int(11) NOT NULL,
  `towar_id` int(11) NOT NULL,
  `podobny_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `towar_polecane`
--

CREATE TABLE `towar_polecane` (
  `id` int(11) NOT NULL,
  `towar_id` int(11) NOT NULL,
  `podobny_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `towar_polecane`
--

INSERT INTO `towar_polecane` (`id`, `towar_id`, `podobny_id`) VALUES
(1, 1, 2),
(2, 1, 3),
(3, 1, 4),
(4, 1, 5),
(5, 1, 6),
(6, 1, 7),
(7, 1, 8),
(8, 1, 9),
(9, 1, 10);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `towar_wariant`
--

CREATE TABLE `towar_wariant` (
  `id` int(11) NOT NULL,
  `towar_id` int(11) NOT NULL,
  `rozmiar_id` int(11) DEFAULT NULL,
  `kolor_id` int(11) DEFAULT NULL,
  `ilosc` int(11) DEFAULT '0',
  `ean` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `towar_zdjecie`
--

CREATE TABLE `towar_zdjecie` (
  `id` int(11) NOT NULL,
  `towar_id` int(11) NOT NULL,
  `plik` varchar(255) DEFAULT NULL,
  `kolejnosc` int(11) DEFAULT '0',
  `domyslne` tinyint(1) DEFAULT '0',
  `alt` varchar(200) DEFAULT NULL,
  `ukryte` tinyint(1) DEFAULT '0',
  `wariant_id` int(11) DEFAULT NULL,
  `remote_url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `towar_zdjecie`
--

INSERT INTO `towar_zdjecie` (`id`, `towar_id`, `plik`, `kolejnosc`, `domyslne`, `alt`, `ukryte`, `wariant_id`, `remote_url`) VALUES
(1, 1, '5de7995f4872a4.08603193.png', 0, 0, NULL, 0, NULL, NULL),
(2, 5, '5de79a052c00a0.95387861.png', 0, 1, NULL, 0, NULL, NULL),
(3, 2, '5de79a0d002aa2.38731714.png', 0, 1, NULL, 0, NULL, NULL),
(4, 3, '5de79a163fc5b8.54998713.png', 0, 1, NULL, 0, NULL, NULL),
(5, 4, '5de79a204f89b9.96784697.png', 0, 1, NULL, 0, NULL, NULL),
(6, 1, '5de79b92c11c01.83553988.png', 0, 1, NULL, 0, NULL, NULL),
(7, 6, '5de8debb174802.62668780.jpg', 0, 1, NULL, 0, NULL, NULL),
(8, 7, '5de8debf797cc6.62785456.jpeg', 0, 1, NULL, 0, NULL, NULL),
(9, 8, '5de8dec4468761.62213972.jpg', 0, 1, NULL, 0, NULL, NULL),
(10, 9, '5de8dec8b5eb56.16476701.png', 0, 1, NULL, 0, NULL, NULL),
(11, 10, '5de8decf7f5399.92344427.jpg', 0, 1, NULL, 0, NULL, NULL),
(12, 1, '5e05c5f2487710.04110507.png', 0, 0, NULL, 0, NULL, NULL),
(13, 1, '5e05c5f2b03b15.49696567.png', 0, 0, NULL, 0, NULL, NULL),
(14, 1, '5e05c5f3016e21.69534195.png', 0, 0, NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uzytkownik`
--

CREATE TABLE `uzytkownik` (
  `id` int(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telefon` varchar(15) NOT NULL,
  `imie` varchar(50) DEFAULT NULL,
  `nazwisko` varchar(70) DEFAULT NULL,
  `dane` text,
  `token` char(32) DEFAULT NULL,
  `token_data` datetime DEFAULT NULL,
  `data_rejestracji` datetime DEFAULT NULL,
  `aktywny` tinyint(1) DEFAULT '0',
  `firma` varchar(255) DEFAULT NULL,
  `nip` varchar(100) DEFAULT NULL,
  `newsletter_wyslany` tinyint(1) DEFAULT '0',
  `newsletter` tinyint(1) DEFAULT '0',
  `remember_token` varchar(128) DEFAULT NULL,
  `bonusy_suma` float DEFAULT NULL,
  `wysylka_id` int(11) DEFAULT NULL,
  `rodzaj_platnosci_id` int(11) DEFAULT NULL,
  `paczkomat` varchar(20) DEFAULT NULL,
  `paczkomat_info` varchar(100) DEFAULT NULL,
  `typ` enum('b2b','b2c') NOT NULL DEFAULT 'b2c',
  `firma_ulica` varchar(150) DEFAULT NULL,
  `firma_nr_domu` varchar(30) DEFAULT NULL,
  `firma_nr_lokalu` varchar(30) DEFAULT NULL,
  `firma_kod` varchar(10) DEFAULT NULL,
  `firma_miasto` varchar(150) DEFAULT NULL,
  `api_token` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `uzytkownik`
--

INSERT INTO `uzytkownik` (`id`, `password`, `email`, `telefon`, `imie`, `nazwisko`, `dane`, `token`, `token_data`, `data_rejestracji`, `aktywny`, `firma`, `nip`, `newsletter_wyslany`, `newsletter`, `remember_token`, `bonusy_suma`, `wysylka_id`, `rodzaj_platnosci_id`, `paczkomat`, `paczkomat_info`, `typ`, `firma_ulica`, `firma_nr_domu`, `firma_nr_lokalu`, `firma_kod`, `firma_miasto`, `api_token`) VALUES
(1, '$2y$10$GjThOCGn3t17yggWm3NmCuNv8G55FkQmLxZ4AETip.8eEQO08FxR.', 'test2@projekt-samatix.pl', '123123123', 'Jan', 'Kowalski', NULL, '5e15c27376e04', NULL, '2020-01-08 12:52:19', 0, 'testowa firma', '6751854018', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'b2c', NULL, NULL, NULL, NULL, NULL, NULL),
(2, '$2y$10$nISua123HUlXc/JsMN8A7uMne.zQZBOL.3kxi72ovIZaZen1myjN.', 'test@projekt-samatix.pl', '253655123', 'Jan', 'Mikołajczyk', NULL, '5e15c4cb0b869', NULL, '2020-01-08 13:02:19', 1, 'testtowa S.C.', '78998787998', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'b2b', 'test', '11', '', '12-565', 'Testowo', 'eae83d456c604ab4f742d59709ed3dd8f17da2fc'),
(7, '$2y$10$nyp65pZhXOBfiV1EwZmiy.yFFs1.Pf4cniTSKbQN2rW.uqF9GazyO', 'test3@projekt-samatix.pl', '123123123', 'Jan', 'Kowalski', NULL, '5e3d0a23a0bd5', NULL, '2020-02-07 07:56:35', 1, 'testowa nowa', '11122233387', 0, 0, NULL, NULL, 1, 2, NULL, NULL, 'b2b', 'testowa, 24', '24', 'test', '78-111', 'test', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uzytkownik_adres`
--

CREATE TABLE `uzytkownik_adres` (
  `id` int(11) NOT NULL,
  `uzytkownik_id` int(11) NOT NULL,
  `nazwa_adresu` varchar(30) DEFAULT NULL,
  `imie` varchar(50) DEFAULT NULL,
  `nazwisko` varchar(70) DEFAULT NULL,
  `firma` varchar(255) DEFAULT NULL,
  `nip` varchar(15) DEFAULT NULL,
  `telefon` varchar(50) DEFAULT NULL,
  `ulica` varchar(70) NOT NULL,
  `nr_domu` varchar(7) DEFAULT NULL,
  `nr_lokalu` varchar(7) DEFAULT NULL,
  `kod` varchar(6) NOT NULL,
  `miasto` varchar(70) NOT NULL,
  `domyslny_wysylka` tinyint(1) DEFAULT '0',
  `domyslny_faktura` tinyint(1) DEFAULT '0',
  `typ` smallint(6) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `kraj` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `uzytkownik_adres`
--

INSERT INTO `uzytkownik_adres` (`id`, `uzytkownik_id`, `nazwa_adresu`, `imie`, `nazwisko`, `firma`, `nip`, `telefon`, `ulica`, `nr_domu`, `nr_lokalu`, `kod`, `miasto`, `domyslny_wysylka`, `domyslny_faktura`, `typ`, `email`, `kraj`) VALUES
(2, 7, NULL, 'Jan', 'Kowalski', 'testowa nowa', '11122233387', NULL, 'testowa, 24', '24', 'test', '78-111', 'test', 0, 0, 2, NULL, 'PL'),
(3, 7, NULL, 'Jan', 'Kowalski', NULL, NULL, '600900600', 'test, 11', NULL, NULL, '78-111', 'London', 0, 0, 1, 'test3@projekt-samatix.pl', 'GB');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uzytkownik_koszyk`
--

CREATE TABLE `uzytkownik_koszyk` (
  `id` int(11) NOT NULL,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `items` text,
  `active` tinyint(1) DEFAULT '0',
  `create_date` datetime DEFAULT NULL,
  `last_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `uzytkownik_koszyk`
--

INSERT INTO `uzytkownik_koszyk` (`id`, `uzytkownik_id`, `items`, `active`, `create_date`, `last_update`) VALUES
(58, NULL, '9:1,z_5e0876b9688e6_t_1:1,z_5e0876b9688e6_t_3:1', 1, '2020-04-20 04:24:49', '2020-04-20 04:25:26'),
(59, NULL, '1:1', 1, '2020-04-22 08:53:26', '2020-04-22 08:53:26');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `vat`
--

CREATE TABLE `vat` (
  `id` int(11) NOT NULL,
  `stawka` int(11) NOT NULL,
  `nazwa` varchar(20) NOT NULL DEFAULT '',
  `zwolniony` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `vat`
--

INSERT INTO `vat` (`id`, `stawka`, `nazwa`, `zwolniony`) VALUES
(1, 0, '0%', 0),
(2, 8, '8%', 0),
(3, 23, '23%', 0),
(4, 0, 'zw', 1),
(24, 46, '46%', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `waluta`
--

CREATE TABLE `waluta` (
  `id` int(11) NOT NULL,
  `symbol` varchar(45) NOT NULL,
  `nazwa` varchar(45) NOT NULL,
  `kurs` float(6,4) NOT NULL DEFAULT '1.0000',
  `domyslna` tinyint(4) DEFAULT '0',
  `separator_d` varchar(1) NOT NULL,
  `separator_t` varchar(1) NOT NULL,
  `symbol_przed` tinyint(4) DEFAULT '0',
  `space` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `waluta`
--

INSERT INTO `waluta` (`id`, `symbol`, `nazwa`, `kurs`, `domyslna`, `separator_d`, `separator_t`, `symbol_przed`, `space`) VALUES
(1, 'PLN', 'Złoty', 1.0000, 1, ',', '', 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wersja`
--

CREATE TABLE `wersja` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `wersja`
--

INSERT INTO `wersja` (`id`, `nazwa`) VALUES
(1, 'Klucz'),
(2, 'Blu-ray'),
(3, 'Pudełko'),
(4, 'Standard'),
(5, 'Specjalna'),
(6, 'CD/DVD'),
(7, 'DVD'),
(8, 'CD');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wersja_i18n`
--

CREATE TABLE `wersja_i18n` (
  `id` int(11) NOT NULL,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wiek`
--

CREATE TABLE `wiek` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(100) NOT NULL,
  `miesiace_od` int(11) DEFAULT NULL,
  `miesiace_do` int(11) DEFAULT NULL,
  `lata_od` int(11) DEFAULT NULL,
  `lata_do` int(11) DEFAULT NULL,
  `kolejnosc` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `wiek`
--

INSERT INTO `wiek` (`id`, `nazwa`, `miesiace_od`, `miesiace_do`, `lata_od`, `lata_do`, `kolejnosc`) VALUES
(1, '0 - 6 miesięcy', 0, 6, NULL, NULL, 0),
(2, '6 - 18 miesięcy', 6, 18, NULL, NULL, 0),
(3, '18 - 36 miesięcy', 18, 36, 1, 3, 0),
(4, '3 - 4 lat', 36, 48, 3, 4, 0),
(5, '5 - 6 lat', 60, 72, 5, 6, 0),
(6, '7+ lat', 84, NULL, 7, NULL, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wysylka`
--

CREATE TABLE `wysylka` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(70) NOT NULL,
  `vat_id` int(11) NOT NULL,
  `koszt_netto` float DEFAULT NULL,
  `koszt_brutto` float DEFAULT NULL,
  `koszt_dodatkowy` float(8,2) NOT NULL DEFAULT '0.00',
  `opis` text,
  `aktywna` tinyint(1) NOT NULL DEFAULT '1',
  `obrazek` varchar(37) DEFAULT NULL,
  `duzy_gabaryt` tinyint(2) DEFAULT '0',
  `paczkomat` tinyint(4) DEFAULT '0',
  `komunikat` text,
  `elektroniczna` tinyint(4) DEFAULT '0',
  `odbior_osobisty` tinyint(1) DEFAULT '0',
  `koszty_kraj` tinyint(1) DEFAULT '0',
  `is_long` tinyint(1) DEFAULT '0',
  `subiekt_kod` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `wysylka`
--

INSERT INTO `wysylka` (`id`, `nazwa`, `vat_id`, `koszt_netto`, `koszt_brutto`, `koszt_dodatkowy`, `opis`, `aktywna`, `obrazek`, `duzy_gabaryt`, `paczkomat`, `komunikat`, `elektroniczna`, `odbior_osobisty`, `koszty_kraj`, `is_long`, `subiekt_kod`) VALUES
(1, 'Wysyłka 48h', 2, NULL, NULL, 9.00, 'Paczki wysyłane kurierem DPD', 1, NULL, 2, 0, '', 0, 0, 1, 0, 'kurier_48'),
(2, 'Odbiór osobisty', 1, NULL, NULL, 5.00, '', 0, NULL, 2, 0, '<p><span style=\"color: rgb(255, 0, 0);\"><span style=\"font-weight: bold; font-size: 18px;\">Uwaga!</span><br><span style=\"font-size: 14px;\">W przypadku odbioru osobistego musisz poczekać na informacje czy zamówienie jest przygotowane do odbioru. Informacja zostanie wysłana na podany w zamówieniu adres email lub przekazana telefonicznie.</span></span></p>', 0, 1, 0, 0, NULL),
(3, 'Paczkomat Inpost', 1, NULL, NULL, 0.00, '', 0, NULL, 0, 1, '', 0, 0, 0, 0, NULL),
(4, 'E - wysyłka', 1, NULL, NULL, 0.00, '', 0, NULL, 0, 0, '<p style=\"text-align: center; \"><span style=\"color: rgb(255, 0, 0); font-weight: bold;\">Karty podarunkowe wysyłane są w formie PDF na podany w zamówieniu adres email.</span></p>', 1, 0, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wysylka_i18n`
--

CREATE TABLE `wysylka_i18n` (
  `id` int(11) NOT NULL,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wysylka_koszt`
--

CREATE TABLE `wysylka_koszt` (
  `id` int(11) NOT NULL,
  `wysylka_id` int(11) NOT NULL,
  `od` int(11) NOT NULL,
  `koszt_netto` float DEFAULT NULL,
  `koszt_brutto` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `wysylka_koszt`
--

INSERT INTO `wysylka_koszt` (`id`, `wysylka_id`, `od`, `koszt_netto`, `koszt_brutto`) VALUES
(2, 1, 100, 29.4118, 30),
(3, 1, 0, 9.80392, 10),
(4, 1, 300, 49.0196, 50),
(5, 1, 1001, 147.059, 150),
(6, 1, 5001, 1470.59, 1500),
(7, 3, 0, 9, 9),
(8, 3, 15, 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wysylka_kraje_koszty`
--

CREATE TABLE `wysylka_kraje_koszty` (
  `id` int(11) NOT NULL,
  `wysylka_id` int(11) NOT NULL,
  `kraj` varchar(10) NOT NULL,
  `koszt` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `wysylka_kraje_koszty`
--

INSERT INTO `wysylka_kraje_koszty` (`id`, `wysylka_id`, `kraj`, `koszt`) VALUES
(1, 1, 'AF', 150),
(3, 1, 'AL', 151),
(5, 1, 'DZ', 250),
(7, 1, 'AS', 200),
(9, 1, 'AD', 200);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wysylka_platnosc`
--

CREATE TABLE `wysylka_platnosc` (
  `id` int(11) NOT NULL,
  `wysylka_id` int(11) NOT NULL,
  `rodzaj_platnosci_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `wysylka_platnosc`
--

INSERT INTO `wysylka_platnosc` (`id`, `wysylka_id`, `rodzaj_platnosci_id`) VALUES
(1, 1, 1),
(7, 1, 2),
(8, 1, 5),
(10, 2, 7),
(11, 2, 5),
(12, 3, 2),
(13, 3, 5),
(14, 4, 2),
(15, 4, 5);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wysylka_punkty_odbioru`
--

CREATE TABLE `wysylka_punkty_odbioru` (
  `id` int(11) NOT NULL,
  `wysylka_id` int(11) NOT NULL,
  `punkty_odbioru_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zamowienie`
--

CREATE TABLE `zamowienie` (
  `id` int(11) NOT NULL,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `numer` varchar(50) DEFAULT NULL,
  `waluta_id` int(11) DEFAULT NULL,
  `waluta_symbol` varchar(15) DEFAULT NULL,
  `lng` varchar(15) DEFAULT NULL,
  `imie` varchar(50) NOT NULL,
  `nazwisko` varchar(70) NOT NULL,
  `email` varchar(100) NOT NULL,
  `firma` varchar(255) DEFAULT NULL,
  `nip` varchar(15) DEFAULT NULL,
  `adres_faktury` tinytext,
  `adres_wysylki` tinytext,
  `rodzaj_platnosci_id` int(11) DEFAULT NULL,
  `wysylka_id` int(11) DEFAULT NULL,
  `punkty_odbioru_id` int(11) DEFAULT NULL,
  `punkty_odbioru_info` tinytext,
  `platnosc_wysylka` varchar(100) DEFAULT NULL,
  `wysylka_ulica` varchar(150) DEFAULT NULL,
  `wysylka_nr_domu` varchar(15) DEFAULT NULL,
  `wysylka_nr_lokalu` varchar(15) DEFAULT NULL,
  `wysylka_kod` varchar(15) DEFAULT NULL,
  `wysylka_miasto` varchar(150) DEFAULT NULL,
  `wysylka_imie` varchar(150) DEFAULT NULL,
  `wysylka_nazwisko` varchar(150) DEFAULT NULL,
  `wysylka_firma` varchar(150) DEFAULT NULL,
  `wysylka_nip` varchar(50) DEFAULT NULL,
  `wysylka_telefon` varchar(30) DEFAULT NULL,
  `wysylka_email` varchar(150) DEFAULT NULL,
  `wysylka_kraj` varchar(30) DEFAULT NULL,
  `faktura` tinyint(1) DEFAULT '0',
  `faktura_imie` varchar(50) DEFAULT NULL,
  `faktura_nazwisko` varchar(70) DEFAULT NULL,
  `faktura_firma` varchar(200) DEFAULT NULL,
  `faktura_nip` varchar(15) DEFAULT NULL,
  `faktura_ulica` varchar(90) DEFAULT NULL,
  `faktura_nr_domu` varchar(15) DEFAULT NULL,
  `faktura_nr_lokalu` varchar(15) DEFAULT NULL,
  `faktura_kod` varchar(6) DEFAULT NULL,
  `faktura_miasto` varchar(70) DEFAULT NULL,
  `faktura_kraj` varchar(30) DEFAULT NULL,
  `telefon` varchar(20) DEFAULT NULL,
  `wartosc_produktow` float(10,3) NOT NULL,
  `wartosc_produktow_netto` float DEFAULT NULL,
  `wysylka_koszt` float DEFAULT '0',
  `wysylka_long_koszt` float DEFAULT NULL,
  `wartosc_vat` float(10,3) DEFAULT NULL,
  `wartosc_razem` float(10,3) NOT NULL,
  `wartosc_razem_netto` float(10,3) DEFAULT NULL,
  `uwagi` text,
  `notatki` text,
  `status` varchar(50) NOT NULL DEFAULT 'wyslane',
  `data` datetime NOT NULL,
  `data_realizacji` datetime DEFAULT NULL,
  `rabat` varchar(20) DEFAULT NULL,
  `status_log` varchar(200) DEFAULT NULL,
  `nr_fv` varchar(50) DEFAULT NULL,
  `notatka_status` varchar(128) DEFAULT NULL,
  `token` varchar(200) DEFAULT NULL,
  `termin_wysylki` date DEFAULT NULL,
  `nr_listu_przewozowego` varchar(128) DEFAULT NULL,
  `kurier_typ` varchar(30) DEFAULT NULL,
  `rabat_zestaw` float DEFAULT NULL,
  `kod_rabatowy` varchar(50) DEFAULT NULL,
  `kod_rabatowy_typ` varchar(15) DEFAULT NULL,
  `kod_rabatowy_wartosc` float DEFAULT NULL,
  `telefoniczne` tinyint(1) DEFAULT '0',
  `bonus_suma` float DEFAULT NULL,
  `bonus_uzyty` float DEFAULT NULL,
  `paczkomat` varchar(20) DEFAULT NULL,
  `paczkomat_info` varchar(100) DEFAULT NULL,
  `bonus_przyznany` float DEFAULT NULL,
  `regulamin` tinyint(1) DEFAULT '1',
  `zgoda` tinyint(1) DEFAULT '0',
  `zgoda2` tinyint(1) DEFAULT '0',
  `subiekt` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `zamowienie`
--

INSERT INTO `zamowienie` (`id`, `uzytkownik_id`, `numer`, `waluta_id`, `waluta_symbol`, `lng`, `imie`, `nazwisko`, `email`, `firma`, `nip`, `adres_faktury`, `adres_wysylki`, `rodzaj_platnosci_id`, `wysylka_id`, `punkty_odbioru_id`, `punkty_odbioru_info`, `platnosc_wysylka`, `wysylka_ulica`, `wysylka_nr_domu`, `wysylka_nr_lokalu`, `wysylka_kod`, `wysylka_miasto`, `wysylka_imie`, `wysylka_nazwisko`, `wysylka_firma`, `wysylka_nip`, `wysylka_telefon`, `wysylka_email`, `wysylka_kraj`, `faktura`, `faktura_imie`, `faktura_nazwisko`, `faktura_firma`, `faktura_nip`, `faktura_ulica`, `faktura_nr_domu`, `faktura_nr_lokalu`, `faktura_kod`, `faktura_miasto`, `faktura_kraj`, `telefon`, `wartosc_produktow`, `wartosc_produktow_netto`, `wysylka_koszt`, `wysylka_long_koszt`, `wartosc_vat`, `wartosc_razem`, `wartosc_razem_netto`, `uwagi`, `notatki`, `status`, `data`, `data_realizacji`, `rabat`, `status_log`, `nr_fv`, `notatka_status`, `token`, `termin_wysylki`, `nr_listu_przewozowego`, `kurier_typ`, `rabat_zestaw`, `kod_rabatowy`, `kod_rabatowy_typ`, `kod_rabatowy_wartosc`, `telefoniczne`, `bonus_suma`, `bonus_uzyty`, `paczkomat`, `paczkomat_info`, `bonus_przyznany`, `regulamin`, `zgoda`, `zgoda2`, `subiekt`) VALUES
(1, 7, 'VOC7KY', 1, 'PLN', 'pl_PL', 'Jan', 'Kowalski', 'test3@projekt-samatix.pl', NULL, NULL, 'test, 11<br/>78-111 London<br/>Wales', 'test, 11<br/>78-111 London<br/>Wales', 2, 1, NULL, NULL, 'Przelew tradycyjny - Wysyłka 48h', 'test, 11', NULL, NULL, '78-111', 'London', 'Jan', 'Kowalski', NULL, NULL, '600900600', 'test3@projekt-samatix.pl', 'GB', 0, 'Jan', 'Kowalski', NULL, NULL, 'test, 11', NULL, NULL, '78-111', 'London', 'GB', '600900600', 1444.000, 1444, 60, NULL, 274.434, 1504.000, 1229.566, 'Zamówienie z kodem rabatowym', NULL, 'zlozone', '2020-02-07 11:16:48', NULL, NULL, NULL, NULL, NULL, '5e3d39106ef1c', NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, 1, 1, 0, 1),
(2, 7, 'P8ICXR', 1, 'PLN', 'pl_PL', 'Jan', 'Kowalski', 'test3@projekt-samatix.pl', NULL, NULL, 'test, 11<br/>78-111 London<br/>Wales', 'test, 11<br/>78-111 London<br/>Wales', 2, 1, NULL, NULL, 'Przelew tradycyjny - Wysyłka 48h', 'test, 11', NULL, NULL, '78-111', 'London', 'Jan', 'Kowalski', NULL, NULL, '123123123', 'test3@projekt-samatix.pl', 'GB', 0, 'Jan', 'Kowalski', NULL, NULL, 'test, 11', NULL, NULL, '78-111', 'London', 'GB', '123123123', 1463.000, 1463, 60, NULL, 277.994, 1523.000, 1245.006, 'test zam rabat', NULL, 'zlozone', '2020-02-07 11:49:33', NULL, NULL, NULL, NULL, NULL, '5e3d40bd51934', NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, 1, 1, 0, 1),
(3, 7, 'CYH0NP', 1, 'PLN', 'pl_PL', 'Jan', 'Kowalski', 'test3@projekt-samatix.pl', NULL, NULL, 'test, 11<br/>78-111 London<br/>Wales', 'test, 11<br/>78-111 London<br/>Wales', 2, 1, NULL, NULL, 'Przelew tradycyjny - Wysyłka 48h', 'test, 11', NULL, NULL, '78-111', 'London', 'Jan', 'Kowalski', NULL, NULL, '123123123', 'test3@projekt-samatix.pl', 'GB', 0, 'Jan', 'Kowalski', NULL, NULL, 'test, 11', NULL, NULL, '78-111', 'London', 'GB', '123123123', 345.000, 345, 60, NULL, 68.954, 405.000, 336.046, 'test', NULL, 'zlozone', '2020-02-07 12:55:24', NULL, NULL, NULL, NULL, NULL, '5e3d502ca78e5', NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, 1, 1, 0, 1),
(4, NULL, '5UOUDY', 1, 'PLN', 'pl_PL', 'Jan', 'Kowalski', 'test@projekt-samatix.pl', NULL, NULL, 'Testowa 25/8<br/>75-852 Gdziesiowo<br/>Polska', 'Testowa 25/8<br/>75-852 Gdziesiowo<br/>Polska', 2, 5, NULL, NULL, 'Przelew tradycyjny - Wysyłka do 14 dni', 'Testowa 25/8', NULL, NULL, '75-852', 'Gdziesiowo', 'Jan', 'Kowalski', NULL, NULL, '123456789', 'test@projekt-samatix.pl', 'PL', 0, 'Jan', 'Kowalski', NULL, NULL, 'Testowa 25/8', NULL, NULL, '75-852', 'Gdziesiowo', 'PL', '123456789', 100.000, 100, 8, NULL, 18.700, 108.000, 89.300, 'test zamówienia do subiekta', NULL, 'zlozone', '2020-04-01 10:44:22', NULL, NULL, NULL, NULL, NULL, '5e845466a9868', NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, 1, 0, 0, 1),
(5, NULL, 'FE6KSR', 1, 'PLN', 'pl_PL', 'Marian', 'Testowy', 'test@projekt-samatix.pl', NULL, NULL, 'Gromska 22<br/>25-800 Łuków<br/>Polska', 'Testówko 25/88<br/>78-200 Gdziesiowo<br/>Polska', 1, 1, NULL, NULL, 'Płatność za pobraniem - Wysyłka 48h', 'Testówko 25/88', NULL, NULL, '78-200', 'Gdziesiowo', 'Marian', 'Testowy', NULL, NULL, '123132123', 'test@projekt-samatix.pl', 'PL', 1, 'Emil', 'Kwiatkowski', 'JanLand S.C.', '6511568458', 'Gromska 22', NULL, NULL, '25-800', 'Łuków', 'PL', '123132123', 100.000, 100, 19, NULL, 20.107, 119.000, 98.893, 'testowe zamówienie do subiekta', NULL, 'zlozone', '2020-04-01 14:25:40', NULL, NULL, NULL, NULL, NULL, '5e8488447a84a', NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, 1, 0, 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zamowienie_dhl`
--

CREATE TABLE `zamowienie_dhl` (
  `id` int(11) NOT NULL,
  `zamowienie_id` int(11) DEFAULT NULL,
  `allegro_zamowienie_id` int(11) DEFAULT NULL,
  `shipmentId` varchar(128) DEFAULT NULL,
  `shipmentDate` date NOT NULL,
  `labelType` varchar(100) DEFAULT NULL,
  `labelName` varchar(100) DEFAULT NULL,
  `labelData` longtext,
  `labelMimeType` varchar(100) DEFAULT NULL,
  `orderId` varchar(128) DEFAULT NULL,
  `data_dodania` datetime DEFAULT NULL,
  `opis` text,
  `godzina_od` time DEFAULT NULL,
  `godzina_do` time DEFAULT NULL,
  `type` varchar(5) DEFAULT NULL,
  `uwagi` text,
  `requestData` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zamowienie_towar`
--

CREATE TABLE `zamowienie_towar` (
  `id` int(11) NOT NULL,
  `zamowienie_id` int(11) NOT NULL,
  `ilosc` int(11) NOT NULL,
  `cena_podstawowa` float DEFAULT NULL,
  `cena_za_sztuke_netto` float DEFAULT NULL,
  `cena_za_sztuke_brutto` float DEFAULT NULL,
  `cena_razem_netto` float NOT NULL,
  `cena_razem_brutto` float NOT NULL,
  `kod` varchar(100) DEFAULT NULL,
  `nazwa` varchar(150) NOT NULL,
  `towar_id` int(11) DEFAULT NULL,
  `wariant_id` int(11) DEFAULT NULL,
  `jednostka_id` int(11) DEFAULT NULL,
  `jednostka_str` varchar(30) DEFAULT NULL,
  `vat_id` int(11) DEFAULT NULL,
  `vat_stawka` float DEFAULT NULL,
  `bonusowe_zlotowki` float DEFAULT NULL,
  `preorder` tinyint(1) DEFAULT '0',
  `data_premiery` date DEFAULT NULL,
  `platforma` varchar(100) DEFAULT NULL,
  `hot_deal` int(11) DEFAULT NULL,
  `karta_podarunkowa_id` int(11) DEFAULT NULL,
  `karta_podarunkowa_wartosc` float DEFAULT NULL,
  `hurtownia` varchar(50) DEFAULT NULL,
  `ws_long` tinyint(1) DEFAULT '0',
  `rabat` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `zamowienie_towar`
--

INSERT INTO `zamowienie_towar` (`id`, `zamowienie_id`, `ilosc`, `cena_podstawowa`, `cena_za_sztuke_netto`, `cena_za_sztuke_brutto`, `cena_razem_netto`, `cena_razem_brutto`, `kod`, `nazwa`, `towar_id`, `wariant_id`, `jednostka_id`, `jednostka_str`, `vat_id`, `vat_stawka`, `bonusowe_zlotowki`, `preorder`, `data_premiery`, `platforma`, `hot_deal`, `karta_podarunkowa_id`, `karta_podarunkowa_wartosc`, `hurtownia`, `ws_long`, `rabat`) VALUES
(1, 1, 1, 130, 105.69, 130, 105.69, 130, '', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', 8, NULL, 1, 'szt.', 3, 23, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(2, 1, 1, 130, 105.69, 130, 105.69, 130, '', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', 3, NULL, 1, 'szt.', 3, 23, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(3, 1, 1, 130, 105.69, 130, 105.69, 130, '', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', 2, NULL, 1, 'szt.', 3, 23, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(4, 1, 1, 130, 105.69, 130, 105.69, 130, '', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', 5, NULL, 1, 'szt.', 3, 23, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(5, 1, 1, 130, 105.69, 130, 105.69, 130, '', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', 4, NULL, 1, 'szt.', 3, 23, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(6, 1, 1, 130, 105.69, 130, 105.69, 130, '', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', 6, NULL, 1, 'szt.', 3, 23, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(7, 1, 1, 130, 105.69, 130, 105.69, 130, '', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', 7, NULL, 1, 'szt.', 3, 23, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(8, 1, 1, 130, 105.69, 130, 105.69, 130, '', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', 9, NULL, 1, 'szt.', 3, 23, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(9, 1, 4, 130, 97.56, 120, 390.24, 480, 'ast2665', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', 1, NULL, 1, 'szt.', 3, 23, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(10, 2, 1, 130, 105.69, 130, 105.69, 130, '', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', 8, NULL, 1, 'szt.', 3, 23, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 5),
(11, 2, 1, 130, 105.69, 130, 105.69, 130, '', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', 2, NULL, 1, 'szt.', 3, 23, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 5),
(12, 2, 2, 130, 105.69, 130, 211.38, 260, '', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', 3, NULL, 1, 'szt.', 3, 23, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 5),
(13, 2, 1, 130, 105.69, 130, 105.69, 130, '', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', 4, NULL, 1, 'szt.', 3, 23, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 5),
(14, 2, 1, 130, 105.69, 130, 105.69, 130, '', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', 5, NULL, 1, 'szt.', 3, 23, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 5),
(15, 2, 1, 130, 105.69, 130, 105.69, 130, '', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', 6, NULL, 1, 'szt.', 3, 23, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 5),
(16, 2, 1, 130, 105.69, 130, 105.69, 130, '', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', 7, NULL, 1, 'szt.', 3, 23, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 5),
(17, 2, 2, 130, 105.69, 130, 211.38, 260, '', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', 9, NULL, 1, 'szt.', 3, 23, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 5),
(18, 2, 2, 130, 97.56, 120, 195.12, 240, 'ast2665', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', 1, NULL, 1, 'szt.', 3, 23, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 5),
(19, 3, 1, 130, 105.69, 130, 105.69, 130, '', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', 3, NULL, 1, 'szt.', 3, 23, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(20, 3, 1, 130, 105.69, 130, 105.69, 130, '', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', 9, NULL, 1, 'szt.', 3, 23, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(21, 3, 1, 130, 81.3, 100, 81.3, 100, 'ast2665', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', 1, NULL, 1, 'szt.', 3, 23, 0, 0, NULL, NULL, 5, NULL, NULL, NULL, 0, 15),
(22, 4, 1, 130, 81.3, 100, 81.3, 100, 'ast2665', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', 1, NULL, 1, 'szt.', 3, 23, 0, 0, NULL, NULL, 4, NULL, NULL, NULL, 0, NULL),
(23, 5, 1, 130, 81.3, 100, 81.3, 100, 'ast2665', 'Tytuł tego przedmiotu o długości jaką przedstawia ten wpis', 1, NULL, 1, 'szt.', 3, 23, 0, 0, NULL, NULL, 3, NULL, NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zapytanie`
--

CREATE TABLE `zapytanie` (
  `id` int(11) NOT NULL,
  `nr_zgloszenia` varchar(30) DEFAULT NULL,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `imie` varchar(50) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `zgoda_dane` tinyint(1) NOT NULL DEFAULT '0',
  `telefon` varchar(30) DEFAULT NULL,
  `platforma` int(11) DEFAULT NULL,
  `nosnik` int(11) DEFAULT NULL,
  `tresc` text NOT NULL,
  `data_dodania` datetime NOT NULL,
  `odpowiedz` text,
  `data_wyslania` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zestaw`
--

CREATE TABLE `zestaw` (
  `id` int(11) NOT NULL,
  `towar_id` int(11) NOT NULL,
  `rabat` float(5,2) NOT NULL,
  `kolejnosc` int(11) DEFAULT '0',
  `typ` tinyint(2) DEFAULT '1',
  `nazwa` varchar(120) DEFAULT NULL,
  `token` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `zestaw`
--

INSERT INTO `zestaw` (`id`, `towar_id`, `rabat`, `kolejnosc`, `typ`, `nazwa`, `token`) VALUES
(7, 1, 12.00, 0, 1, 'test 2', '5e0722bbc0920'),
(8, 5, 12.00, 0, 1, 'test 2', '5e0722bbc0920'),
(9, 1, 22.00, 0, 1, 'test2', '5e0876b9688e6'),
(10, 3, 22.00, 0, 1, 'test2', '5e0876b9688e6');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zestaw_towar`
--

CREATE TABLE `zestaw_towar` (
  `id` int(11) NOT NULL,
  `towar_id` int(11) NOT NULL,
  `zestaw_id` int(11) NOT NULL,
  `wymiana` int(2) DEFAULT '0',
  `kategoria_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `zestaw_towar`
--

INSERT INTO `zestaw_towar` (`id`, `towar_id`, `zestaw_id`, `wymiana`, `kategoria_id`) VALUES
(16, 5, 7, 0, NULL),
(17, 1, 8, 0, NULL),
(18, 3, 9, 0, NULL),
(19, 1, 10, 0, NULL);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `administrator`
--
ALTER TABLE `administrator`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`);

--
-- Indeksy dla tabeli `allegro_aukcja`
--
ALTER TABLE `allegro_aukcja`
  ADD PRIMARY KEY (`id`),
  ADD KEY `allegro_aukcja_ibfk_1_idx` (`towar_id`);

--
-- Indeksy dla tabeli `allegro_aukcja_przesylka`
--
ALTER TABLE `allegro_aukcja_przesylka`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `allegro_aukcja_id` (`allegro_aukcja_id`);

--
-- Indeksy dla tabeli `allegro_aukcja_wariant`
--
ALTER TABLE `allegro_aukcja_wariant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `allegro_aukcja_id` (`allegro_aukcja_id`);

--
-- Indeksy dla tabeli `allegro_konto`
--
ALTER TABLE `allegro_konto`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `allegro_pola_kategoria`
--
ALTER TABLE `allegro_pola_kategoria`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `allegro_aukcja_ibfk_1_idx` (`allegro_aukcja_id`);

--
-- Indeksy dla tabeli `allegro_szablon`
--
ALTER TABLE `allegro_szablon`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indeksy dla tabeli `allegro_zamowienie`
--
ALTER TABLE `allegro_zamowienie`
  ADD PRIMARY KEY (`id`),
  ADD KEY `allegro_konto_id` (`allegro_konto_id`);

--
-- Indeksy dla tabeli `allegro_zamowienie_towar`
--
ALTER TABLE `allegro_zamowienie_towar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `towar_id` (`towar_id`),
  ADD KEY `allegro_zamowienie_id` (`allegro_zamowienie_id`);

--
-- Indeksy dla tabeli `atrybut`
--
ALTER TABLE `atrybut`
  ADD PRIMARY KEY (`id`),
  ADD KEY `atrybut_typ_id` (`atrybut_typ_id`);

--
-- Indeksy dla tabeli `atrybut_i18n`
--
ALTER TABLE `atrybut_i18n`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ATRYBUT_I18N_LOCALE_FIELD` (`locale`,`model`,`foreign_key`,`field`),
  ADD KEY `ATRYBUT_I18N_FIELD` (`model`,`foreign_key`,`field`),
  ADD KEY `foreign_key` (`foreign_key`);

--
-- Indeksy dla tabeli `atrybut_podrzedne`
--
ALTER TABLE `atrybut_podrzedne`
  ADD PRIMARY KEY (`id`),
  ADD KEY `atrybut_id` (`atrybut_id`);

--
-- Indeksy dla tabeli `atrybut_typ`
--
ALTER TABLE `atrybut_typ`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `atrybut_typ_parent_id` (`atrybut_typ_parent_id`);

--
-- Indeksy dla tabeli `atrybut_typ_i18n`
--
ALTER TABLE `atrybut_typ_i18n`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ATRYBUT_TYP_I18N_LOCALE_FIELD` (`locale`,`model`,`foreign_key`,`field`),
  ADD KEY `ATRYBUT_TYP_I18N_FIELD` (`model`,`foreign_key`,`field`),
  ADD KEY `foreign_key` (`foreign_key`);

--
-- Indeksy dla tabeli `atrybut_typ_parent`
--
ALTER TABLE `atrybut_typ_parent`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `atrybut_typ_parent_i18n`
--
ALTER TABLE `atrybut_typ_parent_i18n`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ATRYBUT_TYP_PARENT_I18N_LOCALE_FIELD` (`locale`,`model`,`foreign_key`,`field`),
  ADD KEY `ATRYBUT_TYP_PARENT_I18N_FIELD` (`model`,`foreign_key`,`field`),
  ADD KEY `foreign_key` (`foreign_key`);

--
-- Indeksy dla tabeli `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `banner_slides`
--
ALTER TABLE `banner_slides`
  ADD PRIMARY KEY (`id`),
  ADD KEY `banner_id` (`banner_id`);

--
-- Indeksy dla tabeli `banner_slides_elements`
--
ALTER TABLE `banner_slides_elements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `banner_slides_id` (`banner_slides_id`);

--
-- Indeksy dla tabeli `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `blok`
--
ALTER TABLE `blok`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `blok_i18n`
--
ALTER TABLE `blok_i18n`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `BLOK_I18N_LOCALE_FIELD` (`locale`,`model`,`foreign_key`,`field`),
  ADD KEY `BLOK_I18N_FIELD` (`model`,`foreign_key`,`field`),
  ADD KEY `foreign_key` (`foreign_key`);

--
-- Indeksy dla tabeli `bonus`
--
ALTER TABLE `bonus`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `call_back`
--
ALTER TABLE `call_back`
  ADD PRIMARY KEY (`id`),
  ADD KEY `administrator_id` (`administrator_id`);

--
-- Indeksy dla tabeli `certyfikat`
--
ALTER TABLE `certyfikat`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `cron`
--
ALTER TABLE `cron`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `faktura`
--
ALTER TABLE `faktura`
  ADD PRIMARY KEY (`id`),
  ADD KEY `zamowienie_id` (`zamowienie_id`);

--
-- Indeksy dla tabeli `faktura_pozycja`
--
ALTER TABLE `faktura_pozycja`
  ADD PRIMARY KEY (`id`),
  ADD KEY `faktura_id` (`faktura_id`);

--
-- Indeksy dla tabeli `footer_link`
--
ALTER TABLE `footer_link`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `gatunek`
--
ALTER TABLE `gatunek`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `gatunek_i18n`
--
ALTER TABLE `gatunek_i18n`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `GATUNEK_I18N_LOCALE_FIELD` (`locale`,`model`,`foreign_key`,`field`),
  ADD KEY `GATUNEK_I18N_FIELD` (`model`,`foreign_key`,`field`),
  ADD KEY `foreign_key` (`foreign_key`);

--
-- Indeksy dla tabeli `google_kategorie`
--
ALTER TABLE `google_kategorie`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `help_info`
--
ALTER TABLE `help_info`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `hot_deal`
--
ALTER TABLE `hot_deal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `towar_id` (`towar_id`);

--
-- Indeksy dla tabeli `hot_deal_i18n`
--
ALTER TABLE `hot_deal_i18n`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `HOT_DEAL_I18N_LOCALE_FIELD` (`locale`,`model`,`foreign_key`,`field`),
  ADD KEY `HOT_DEAL_I18N_FIELD` (`model`,`foreign_key`,`field`),
  ADD KEY `foreign_key` (`foreign_key`);

--
-- Indeksy dla tabeli `jednostka`
--
ALTER TABLE `jednostka`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `jednostka` (`jednostka`),
  ADD KEY `jednostka_2` (`jednostka`);

--
-- Indeksy dla tabeli `jednostka_i18n`
--
ALTER TABLE `jednostka_i18n`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `JEDNOSTKA_I18N_LOCALE_FIELD` (`locale`,`model`,`foreign_key`,`field`),
  ADD KEY `JEDNOSTKA_I18N_FIELD` (`model`,`foreign_key`,`field`),
  ADD KEY `foreign_key` (`foreign_key`);

--
-- Indeksy dla tabeli `jezyk`
--
ALTER TABLE `jezyk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `waluta` (`waluta_id`);

--
-- Indeksy dla tabeli `karta_podarunkowa`
--
ALTER TABLE `karta_podarunkowa`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `karta_podarunkowa_kod`
--
ALTER TABLE `karta_podarunkowa_kod`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kod` (`kod`),
  ADD KEY `karta_podarunkowa_id` (`karta_podarunkowa_id`),
  ADD KEY `zamowienie_id` (`zamowienie_id`),
  ADD KEY `uzytkownik_id` (`uzytkownik_id`);

--
-- Indeksy dla tabeli `kategoria`
--
ALTER TABLE `kategoria`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nadrzedna` (`parent_id`),
  ADD KEY `towar_promocja_id` (`towar_promocja_id`),
  ADD KEY `google_kategoria_id` (`google_kategoria_id`);

--
-- Indeksy dla tabeli `kategoria_i18n`
--
ALTER TABLE `kategoria_i18n`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `KATEGORIA_I18N_LOCALE_FIELD` (`locale`,`model`,`foreign_key`,`field`),
  ADD KEY `KATEGORIA_I18N_FIELD` (`model`,`foreign_key`,`field`),
  ADD KEY `foreign_key` (`foreign_key`);

--
-- Indeksy dla tabeli `kategoria_map`
--
ALTER TABLE `kategoria_map`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kategoria_id` (`kategoria_id`);

--
-- Indeksy dla tabeli `kody_rabatowe`
--
ALTER TABLE `kody_rabatowe`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kod` (`kod`);

--
-- Indeksy dla tabeli `kody_rabatowe_kategoria`
--
ALTER TABLE `kody_rabatowe_kategoria`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kategoria_id` (`kategoria_id`),
  ADD KEY `kody_rabatowe_id` (`kody_rabatowe_id`);

--
-- Indeksy dla tabeli `kody_rabatowe_producent`
--
ALTER TABLE `kody_rabatowe_producent`
  ADD PRIMARY KEY (`id`),
  ADD KEY `producent_id` (`producent_id`),
  ADD KEY `kody_rabatowe_id` (`kody_rabatowe_id`);

--
-- Indeksy dla tabeli `kolor`
--
ALTER TABLE `kolor`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `konfiguracja`
--
ALTER TABLE `konfiguracja`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `konfiguracja_i18n`
--
ALTER TABLE `konfiguracja_i18n`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `KONFIGURACJA_I18N_LOCALE_FIELD` (`locale`,`model`,`foreign_key`,`field`),
  ADD KEY `KONFIGURACJA_I18N_FIELD` (`model`,`foreign_key`,`field`),
  ADD KEY `foreign_key` (`foreign_key`);

--
-- Indeksy dla tabeli `koszyk`
--
ALTER TABLE `koszyk`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `koszyk_towar`
--
ALTER TABLE `koszyk_towar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `koszyk_id` (`koszyk_id`);

--
-- Indeksy dla tabeli `kupon`
--
ALTER TABLE `kupon`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kod` (`kod`);

--
-- Indeksy dla tabeli `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jezyk_id` (`jezyk_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indeksy dla tabeli `menu_i18n`
--
ALTER TABLE `menu_i18n`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `MENU_I18N_LOCALE_FIELD` (`locale`,`model`,`foreign_key`,`field`),
  ADD KEY `MENU_I18N_FIELD` (`model`,`foreign_key`,`field`),
  ADD KEY `foreign_key` (`foreign_key`);

--
-- Indeksy dla tabeli `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `newsletter_adres`
--
ALTER TABLE `newsletter_adres`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `okazje`
--
ALTER TABLE `okazje`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `okazje_towar`
--
ALTER TABLE `okazje_towar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `okazje_id` (`okazje_id`),
  ADD KEY `towar_id` (`towar_id`);

--
-- Indeksy dla tabeli `opakowanie`
--
ALTER TABLE `opakowanie`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `opinia`
--
ALTER TABLE `opinia`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `platforma`
--
ALTER TABLE `platforma`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `platnosc`
--
ALTER TABLE `platnosc`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uzytkownik_id` (`uzytkownik_id`),
  ADD KEY `transakcja_id` (`zamowienie_id`),
  ADD KEY `rodzaj_platnosci_id` (`rodzaj_platnosci_id`),
  ADD KEY `waluta_id` (`waluta_id`);

--
-- Indeksy dla tabeli `powiadomienia`
--
ALTER TABLE `powiadomienia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `towar_id` (`towar_id`),
  ADD KEY `uzytkownik_id` (`uzytkownik_id`);

--
-- Indeksy dla tabeli `producent`
--
ALTER TABLE `producent`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `producent_i18n`
--
ALTER TABLE `producent_i18n`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `PRODUCENT_I18N_LOCALE_FIELD` (`locale`,`model`,`foreign_key`,`field`),
  ADD KEY `PRODUCENT_I18N_FIELD` (`model`,`foreign_key`,`field`),
  ADD KEY `foreign_key` (`foreign_key`);

--
-- Indeksy dla tabeli `punkty_odbioru`
--
ALTER TABLE `punkty_odbioru`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `punkty_odbioru_i18n`
--
ALTER TABLE `punkty_odbioru_i18n`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `PUNKTY_ODBIORU_I18N_LOCALE_FIELD` (`locale`,`model`,`foreign_key`,`field`),
  ADD KEY `PUNKTY_ODBIORU_I18N_FIELD` (`model`,`foreign_key`,`field`),
  ADD KEY `foreign_key` (`foreign_key`);

--
-- Indeksy dla tabeli `reklamacja`
--
ALTER TABLE `reklamacja`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uzytkownik_id` (`uzytkownik_id`);

--
-- Indeksy dla tabeli `reklamacja_pliki`
--
ALTER TABLE `reklamacja_pliki`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reklamacja_id` (`reklamacja_id`);

--
-- Indeksy dla tabeli `reklamacja_towar`
--
ALTER TABLE `reklamacja_towar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reklamacja_id` (`reklamacja_id`),
  ADD KEY `towar_id` (`towar_id`);

--
-- Indeksy dla tabeli `rodo`
--
ALTER TABLE `rodo`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `rodzaj_platnosci`
--
ALTER TABLE `rodzaj_platnosci`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `rodzaj_platnosci_i18n`
--
ALTER TABLE `rodzaj_platnosci_i18n`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `RODZAJ_PLATNOSCI_I18N_LOCALE_FIELD` (`locale`,`model`,`foreign_key`,`field`),
  ADD KEY `RODZAJ_PLATNOSCI_I18N_FIELD` (`model`,`foreign_key`,`field`),
  ADD KEY `foreign_key` (`foreign_key`);

--
-- Indeksy dla tabeli `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indeksy dla tabeli `routing`
--
ALTER TABLE `routing`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `rozmiar`
--
ALTER TABLE `rozmiar`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `schowek`
--
ALTER TABLE `schowek`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uzytkownik_id_2` (`uzytkownik_id`,`towar_id`),
  ADD KEY `uzytkownik_id` (`uzytkownik_id`),
  ADD KEY `towar_id` (`towar_id`);

--
-- Indeksy dla tabeli `statystyki`
--
ALTER TABLE `statystyki`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `session_id` (`session_id`);

--
-- Indeksy dla tabeli `strona`
--
ALTER TABLE `strona`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `strona_i18n`
--
ALTER TABLE `strona_i18n`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `STRONA_I18N_LOCALE_FIELD` (`locale`,`model`,`foreign_key`,`field`),
  ADD KEY `STRONA_I18N_FIELD` (`model`,`foreign_key`,`field`),
  ADD KEY `foreign_key` (`foreign_key`);

--
-- Indeksy dla tabeli `szablon`
--
ALTER TABLE `szablon`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indeksy dla tabeli `szablon_i18n`
--
ALTER TABLE `szablon_i18n`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `STRONA_I18N_LOCALE_FIELD` (`locale`,`model`,`foreign_key`,`field`),
  ADD KEY `STRONA_I18N_FIELD` (`model`,`foreign_key`,`field`),
  ADD KEY `foreign_key` (`foreign_key`);

--
-- Indeksy dla tabeli `towar`
--
ALTER TABLE `towar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `producent_id` (`producent_id`),
  ADD KEY `ukryty` (`ukryty`),
  ADD KEY `promocja` (`promocja`),
  ADD KEY `wyprzedaz` (`wyprzedaz`),
  ADD KEY `polecany` (`polecany`),
  ADD KEY `platforma_id` (`platforma_id`),
  ADD KEY `wersja_id` (`wersja_id`),
  ADD KEY `bonus_id` (`bonus_id`),
  ADD KEY `wiek_id` (`wiek_id`),
  ADD KEY `opakowanie_id` (`opakowanie_id`);

--
-- Indeksy dla tabeli `towar_akcesoria`
--
ALTER TABLE `towar_akcesoria`
  ADD PRIMARY KEY (`id`),
  ADD KEY `towar_id` (`towar_id`),
  ADD KEY `podobny_id` (`podobny_id`);

--
-- Indeksy dla tabeli `towar_atrybut`
--
ALTER TABLE `towar_atrybut`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `atrybut2_id` (`atrybut_id`),
  ADD KEY `towar_id` (`towar_id`),
  ADD KEY `atrybut_podrzedne_id` (`atrybut_podrzedne_id`);

--
-- Indeksy dla tabeli `towar_cena`
--
ALTER TABLE `towar_cena`
  ADD PRIMARY KEY (`id`),
  ADD KEY `towar_id` (`towar_id`),
  ADD KEY `vat_id` (`vat_id`),
  ADD KEY `jednostka_id` (`jednostka_id`),
  ADD KEY `uzytkownik_id` (`uzytkownik_id`);

--
-- Indeksy dla tabeli `towar_certyfikat`
--
ALTER TABLE `towar_certyfikat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `towar_id` (`towar_id`),
  ADD KEY `certyfikat_id` (`certyfikat_id`);

--
-- Indeksy dla tabeli `towar_gatunek`
--
ALTER TABLE `towar_gatunek`
  ADD PRIMARY KEY (`id`),
  ADD KEY `towar_id` (`towar_id`),
  ADD KEY `gatunek_id` (`gatunek_id`);

--
-- Indeksy dla tabeli `towar_i18n`
--
ALTER TABLE `towar_i18n`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `TOWAR_I18N_LOCALE_FIELD` (`locale`,`model`,`foreign_key`,`field`),
  ADD KEY `TOWAR_I18N_FIELD` (`model`,`foreign_key`,`field`),
  ADD KEY `foreign_key` (`foreign_key`);

--
-- Indeksy dla tabeli `towar_kategoria`
--
ALTER TABLE `towar_kategoria`
  ADD PRIMARY KEY (`id`),
  ADD KEY `towar_id` (`towar_id`),
  ADD KEY `kategoria_id` (`kategoria_id`);

--
-- Indeksy dla tabeli `towar_opinia`
--
ALTER TABLE `towar_opinia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `towar_id` (`towar_id`);

--
-- Indeksy dla tabeli `towar_podobne`
--
ALTER TABLE `towar_podobne`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `towar_podobne_ibfk_1` (`towar_id`),
  ADD KEY `towar_podobne_ibfk_2` (`podobny_id`);

--
-- Indeksy dla tabeli `towar_polecane`
--
ALTER TABLE `towar_polecane`
  ADD PRIMARY KEY (`id`),
  ADD KEY `towar_id` (`towar_id`),
  ADD KEY `podobny_id` (`podobny_id`);

--
-- Indeksy dla tabeli `towar_wariant`
--
ALTER TABLE `towar_wariant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `towar_id` (`towar_id`),
  ADD KEY `rozmiar_id` (`rozmiar_id`),
  ADD KEY `kolor_id` (`kolor_id`);

--
-- Indeksy dla tabeli `towar_zdjecie`
--
ALTER TABLE `towar_zdjecie`
  ADD PRIMARY KEY (`id`),
  ADD KEY `towar_id` (`towar_id`);

--
-- Indeksy dla tabeli `uzytkownik`
--
ALTER TABLE `uzytkownik`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`) USING BTREE;

--
-- Indeksy dla tabeli `uzytkownik_adres`
--
ALTER TABLE `uzytkownik_adres`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uzytkownik_id` (`uzytkownik_id`);

--
-- Indeksy dla tabeli `uzytkownik_koszyk`
--
ALTER TABLE `uzytkownik_koszyk`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `vat`
--
ALTER TABLE `vat`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `waluta`
--
ALTER TABLE `waluta`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `wersja`
--
ALTER TABLE `wersja`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `wersja_i18n`
--
ALTER TABLE `wersja_i18n`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `WERSJA_I18N_LOCALE_FIELD` (`locale`,`model`,`foreign_key`,`field`),
  ADD KEY `WERSJA_I18N_FIELD` (`model`,`foreign_key`,`field`),
  ADD KEY `foreign_key` (`foreign_key`);

--
-- Indeksy dla tabeli `wiek`
--
ALTER TABLE `wiek`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `wysylka`
--
ALTER TABLE `wysylka`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vat_id` (`vat_id`);

--
-- Indeksy dla tabeli `wysylka_i18n`
--
ALTER TABLE `wysylka_i18n`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `WYSYLKA_I18N_LOCALE_FIELD` (`locale`,`model`,`foreign_key`,`field`),
  ADD KEY `WYSYLKA_I18N_FIELD` (`model`,`foreign_key`,`field`),
  ADD KEY `foreign_key` (`foreign_key`);

--
-- Indeksy dla tabeli `wysylka_koszt`
--
ALTER TABLE `wysylka_koszt`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wysylka_id` (`wysylka_id`);

--
-- Indeksy dla tabeli `wysylka_kraje_koszty`
--
ALTER TABLE `wysylka_kraje_koszty`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `wysylka_id` (`wysylka_id`,`kraj`);

--
-- Indeksy dla tabeli `wysylka_platnosc`
--
ALTER TABLE `wysylka_platnosc`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wysylka_id` (`wysylka_id`),
  ADD KEY `rodzaj_platnosci_id` (`rodzaj_platnosci_id`);

--
-- Indeksy dla tabeli `wysylka_punkty_odbioru`
--
ALTER TABLE `wysylka_punkty_odbioru`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wysylka_id` (`wysylka_id`),
  ADD KEY `punkty_odbioru_id` (`punkty_odbioru_id`);

--
-- Indeksy dla tabeli `zamowienie`
--
ALTER TABLE `zamowienie`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uzytkownik_id` (`uzytkownik_id`),
  ADD KEY `waluta_id` (`waluta_id`);

--
-- Indeksy dla tabeli `zamowienie_dhl`
--
ALTER TABLE `zamowienie_dhl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `zamowienie_id` (`zamowienie_id`),
  ADD KEY `allegro_zamowienie_id` (`allegro_zamowienie_id`);

--
-- Indeksy dla tabeli `zamowienie_towar`
--
ALTER TABLE `zamowienie_towar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `zamowienie_id` (`zamowienie_id`),
  ADD KEY `towar_id` (`towar_id`),
  ADD KEY `wariant_id` (`wariant_id`),
  ADD KEY `jednostka_id` (`jednostka_id`),
  ADD KEY `vat_id` (`vat_id`),
  ADD KEY `vat_id_2` (`vat_id`);

--
-- Indeksy dla tabeli `zapytanie`
--
ALTER TABLE `zapytanie`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uzytkownik_id` (`uzytkownik_id`);

--
-- Indeksy dla tabeli `zestaw`
--
ALTER TABLE `zestaw`
  ADD PRIMARY KEY (`id`),
  ADD KEY `towar_id` (`towar_id`);

--
-- Indeksy dla tabeli `zestaw_towar`
--
ALTER TABLE `zestaw_towar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `towar_id` (`towar_id`),
  ADD KEY `zestaw_id` (`zestaw_id`);

--
-- AUTO_INCREMENT dla tabel zrzutów
--

--
-- AUTO_INCREMENT dla tabeli `administrator`
--
ALTER TABLE `administrator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `allegro_aukcja`
--
ALTER TABLE `allegro_aukcja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `allegro_aukcja_przesylka`
--
ALTER TABLE `allegro_aukcja_przesylka`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `allegro_aukcja_wariant`
--
ALTER TABLE `allegro_aukcja_wariant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `allegro_konto`
--
ALTER TABLE `allegro_konto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `allegro_pola_kategoria`
--
ALTER TABLE `allegro_pola_kategoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `allegro_szablon`
--
ALTER TABLE `allegro_szablon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `allegro_zamowienie`
--
ALTER TABLE `allegro_zamowienie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `allegro_zamowienie_towar`
--
ALTER TABLE `allegro_zamowienie_towar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `atrybut`
--
ALTER TABLE `atrybut`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT dla tabeli `atrybut_i18n`
--
ALTER TABLE `atrybut_i18n`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT dla tabeli `atrybut_podrzedne`
--
ALTER TABLE `atrybut_podrzedne`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `atrybut_typ`
--
ALTER TABLE `atrybut_typ`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `atrybut_typ_i18n`
--
ALTER TABLE `atrybut_typ_i18n`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `atrybut_typ_parent`
--
ALTER TABLE `atrybut_typ_parent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `atrybut_typ_parent_i18n`
--
ALTER TABLE `atrybut_typ_parent_i18n`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT dla tabeli `banner_slides`
--
ALTER TABLE `banner_slides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT dla tabeli `banner_slides_elements`
--
ALTER TABLE `banner_slides_elements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `blok`
--
ALTER TABLE `blok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `blok_i18n`
--
ALTER TABLE `blok_i18n`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `bonus`
--
ALTER TABLE `bonus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `call_back`
--
ALTER TABLE `call_back`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `certyfikat`
--
ALTER TABLE `certyfikat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT dla tabeli `cron`
--
ALTER TABLE `cron`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `faktura`
--
ALTER TABLE `faktura`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `faktura_pozycja`
--
ALTER TABLE `faktura_pozycja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT dla tabeli `footer_link`
--
ALTER TABLE `footer_link`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT dla tabeli `gatunek`
--
ALTER TABLE `gatunek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `gatunek_i18n`
--
ALTER TABLE `gatunek_i18n`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `google_kategorie`
--
ALTER TABLE `google_kategorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=505833;

--
-- AUTO_INCREMENT dla tabeli `help_info`
--
ALTER TABLE `help_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `hot_deal`
--
ALTER TABLE `hot_deal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `hot_deal_i18n`
--
ALTER TABLE `hot_deal_i18n`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `jednostka`
--
ALTER TABLE `jednostka`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `jednostka_i18n`
--
ALTER TABLE `jednostka_i18n`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `jezyk`
--
ALTER TABLE `jezyk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `karta_podarunkowa`
--
ALTER TABLE `karta_podarunkowa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `karta_podarunkowa_kod`
--
ALTER TABLE `karta_podarunkowa_kod`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `kategoria`
--
ALTER TABLE `kategoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT dla tabeli `kategoria_i18n`
--
ALTER TABLE `kategoria_i18n`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT dla tabeli `kategoria_map`
--
ALTER TABLE `kategoria_map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `kody_rabatowe`
--
ALTER TABLE `kody_rabatowe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT dla tabeli `kody_rabatowe_kategoria`
--
ALTER TABLE `kody_rabatowe_kategoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT dla tabeli `kody_rabatowe_producent`
--
ALTER TABLE `kody_rabatowe_producent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `kolor`
--
ALTER TABLE `kolor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `konfiguracja`
--
ALTER TABLE `konfiguracja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- AUTO_INCREMENT dla tabeli `konfiguracja_i18n`
--
ALTER TABLE `konfiguracja_i18n`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `koszyk`
--
ALTER TABLE `koszyk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT dla tabeli `koszyk_towar`
--
ALTER TABLE `koszyk_towar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT dla tabeli `kupon`
--
ALTER TABLE `kupon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT dla tabeli `menu_i18n`
--
ALTER TABLE `menu_i18n`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `newsletter`
--
ALTER TABLE `newsletter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `newsletter_adres`
--
ALTER TABLE `newsletter_adres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `okazje`
--
ALTER TABLE `okazje`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT dla tabeli `okazje_towar`
--
ALTER TABLE `okazje_towar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `opakowanie`
--
ALTER TABLE `opakowanie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `opinia`
--
ALTER TABLE `opinia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `platforma`
--
ALTER TABLE `platforma`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT dla tabeli `platnosc`
--
ALTER TABLE `platnosc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `powiadomienia`
--
ALTER TABLE `powiadomienia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `producent`
--
ALTER TABLE `producent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `producent_i18n`
--
ALTER TABLE `producent_i18n`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `punkty_odbioru`
--
ALTER TABLE `punkty_odbioru`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `punkty_odbioru_i18n`
--
ALTER TABLE `punkty_odbioru_i18n`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `reklamacja`
--
ALTER TABLE `reklamacja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `reklamacja_pliki`
--
ALTER TABLE `reklamacja_pliki`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `reklamacja_towar`
--
ALTER TABLE `reklamacja_towar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `rodo`
--
ALTER TABLE `rodo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT dla tabeli `rodzaj_platnosci`
--
ALTER TABLE `rodzaj_platnosci`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `rodzaj_platnosci_i18n`
--
ALTER TABLE `rodzaj_platnosci_i18n`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT dla tabeli `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `routing`
--
ALTER TABLE `routing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `rozmiar`
--
ALTER TABLE `rozmiar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `schowek`
--
ALTER TABLE `schowek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `statystyki`
--
ALTER TABLE `statystyki`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=244;

--
-- AUTO_INCREMENT dla tabeli `strona`
--
ALTER TABLE `strona`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT dla tabeli `strona_i18n`
--
ALTER TABLE `strona_i18n`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `szablon`
--
ALTER TABLE `szablon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT dla tabeli `szablon_i18n`
--
ALTER TABLE `szablon_i18n`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT dla tabeli `towar`
--
ALTER TABLE `towar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT dla tabeli `towar_akcesoria`
--
ALTER TABLE `towar_akcesoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `towar_atrybut`
--
ALTER TABLE `towar_atrybut`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `towar_cena`
--
ALTER TABLE `towar_cena`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT dla tabeli `towar_certyfikat`
--
ALTER TABLE `towar_certyfikat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT dla tabeli `towar_gatunek`
--
ALTER TABLE `towar_gatunek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `towar_i18n`
--
ALTER TABLE `towar_i18n`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT dla tabeli `towar_kategoria`
--
ALTER TABLE `towar_kategoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT dla tabeli `towar_opinia`
--
ALTER TABLE `towar_opinia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT dla tabeli `towar_podobne`
--
ALTER TABLE `towar_podobne`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `towar_polecane`
--
ALTER TABLE `towar_polecane`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT dla tabeli `towar_wariant`
--
ALTER TABLE `towar_wariant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `towar_zdjecie`
--
ALTER TABLE `towar_zdjecie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT dla tabeli `uzytkownik`
--
ALTER TABLE `uzytkownik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `uzytkownik_adres`
--
ALTER TABLE `uzytkownik_adres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `uzytkownik_koszyk`
--
ALTER TABLE `uzytkownik_koszyk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT dla tabeli `vat`
--
ALTER TABLE `vat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT dla tabeli `waluta`
--
ALTER TABLE `waluta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `wersja`
--
ALTER TABLE `wersja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT dla tabeli `wersja_i18n`
--
ALTER TABLE `wersja_i18n`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `wiek`
--
ALTER TABLE `wiek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT dla tabeli `wysylka`
--
ALTER TABLE `wysylka`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `wysylka_i18n`
--
ALTER TABLE `wysylka_i18n`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `wysylka_koszt`
--
ALTER TABLE `wysylka_koszt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT dla tabeli `wysylka_kraje_koszty`
--
ALTER TABLE `wysylka_kraje_koszty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT dla tabeli `wysylka_platnosc`
--
ALTER TABLE `wysylka_platnosc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT dla tabeli `wysylka_punkty_odbioru`
--
ALTER TABLE `wysylka_punkty_odbioru`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `zamowienie`
--
ALTER TABLE `zamowienie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `zamowienie_dhl`
--
ALTER TABLE `zamowienie_dhl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `zamowienie_towar`
--
ALTER TABLE `zamowienie_towar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT dla tabeli `zapytanie`
--
ALTER TABLE `zapytanie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `zestaw`
--
ALTER TABLE `zestaw`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT dla tabeli `zestaw_towar`
--
ALTER TABLE `zestaw_towar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `administrator`
--
ALTER TABLE `administrator`
  ADD CONSTRAINT `administrator_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `allegro_aukcja`
--
ALTER TABLE `allegro_aukcja`
  ADD CONSTRAINT `allegro_aukcja_ibfk_1` FOREIGN KEY (`towar_id`) REFERENCES `towar` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `allegro_aukcja_przesylka`
--
ALTER TABLE `allegro_aukcja_przesylka`
  ADD CONSTRAINT `allegro_aukcja_przesylka_ibfk_1` FOREIGN KEY (`allegro_aukcja_id`) REFERENCES `allegro_aukcja` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `allegro_aukcja_wariant`
--
ALTER TABLE `allegro_aukcja_wariant`
  ADD CONSTRAINT `allegro_aukcja_wariant_ibfk_1` FOREIGN KEY (`allegro_aukcja_id`) REFERENCES `allegro_aukcja` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `allegro_pola_kategoria`
--
ALTER TABLE `allegro_pola_kategoria`
  ADD CONSTRAINT `allegro_pola_kategoria_ibfk_1` FOREIGN KEY (`allegro_aukcja_id`) REFERENCES `allegro_aukcja` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `allegro_zamowienie`
--
ALTER TABLE `allegro_zamowienie`
  ADD CONSTRAINT `allegro_zamowienie_ibfk_1` FOREIGN KEY (`allegro_konto_id`) REFERENCES `allegro_konto` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `allegro_zamowienie_towar`
--
ALTER TABLE `allegro_zamowienie_towar`
  ADD CONSTRAINT `allegro_zamowienie_towar_ibfk_1` FOREIGN KEY (`allegro_zamowienie_id`) REFERENCES `allegro_zamowienie` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `atrybut`
--
ALTER TABLE `atrybut`
  ADD CONSTRAINT `atrybut_ibfk_1` FOREIGN KEY (`atrybut_typ_id`) REFERENCES `atrybut_typ` (`id`) ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `atrybut_i18n`
--
ALTER TABLE `atrybut_i18n`
  ADD CONSTRAINT `atrybut_i18n_ibfk_1` FOREIGN KEY (`foreign_key`) REFERENCES `atrybut` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `atrybut_podrzedne`
--
ALTER TABLE `atrybut_podrzedne`
  ADD CONSTRAINT `atrybut_podrzedne_ibfk_1` FOREIGN KEY (`atrybut_id`) REFERENCES `atrybut` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `atrybut_typ`
--
ALTER TABLE `atrybut_typ`
  ADD CONSTRAINT `atrybut_typ_ibfk_1` FOREIGN KEY (`atrybut_typ_parent_id`) REFERENCES `atrybut_typ_parent` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `atrybut_typ_i18n`
--
ALTER TABLE `atrybut_typ_i18n`
  ADD CONSTRAINT `atrybut_typ_i18n_ibfk_1` FOREIGN KEY (`foreign_key`) REFERENCES `atrybut_typ` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `atrybut_typ_parent_i18n`
--
ALTER TABLE `atrybut_typ_parent_i18n`
  ADD CONSTRAINT `atrybut_typ_parent_i18n_ibfk_1` FOREIGN KEY (`foreign_key`) REFERENCES `atrybut_typ_parent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `banner_slides`
--
ALTER TABLE `banner_slides`
  ADD CONSTRAINT `banner_slides_ibfk_1` FOREIGN KEY (`banner_id`) REFERENCES `banner` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `banner_slides_elements`
--
ALTER TABLE `banner_slides_elements`
  ADD CONSTRAINT `banner_slides_elements_ibfk_1` FOREIGN KEY (`banner_slides_id`) REFERENCES `banner_slides` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `blok_i18n`
--
ALTER TABLE `blok_i18n`
  ADD CONSTRAINT `blok_i18n_ibfk_1` FOREIGN KEY (`foreign_key`) REFERENCES `blok` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `call_back`
--
ALTER TABLE `call_back`
  ADD CONSTRAINT `call_back_ibfk_1` FOREIGN KEY (`administrator_id`) REFERENCES `administrator` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `faktura`
--
ALTER TABLE `faktura`
  ADD CONSTRAINT `faktura_ibfk_1` FOREIGN KEY (`zamowienie_id`) REFERENCES `zamowienie` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `faktura_pozycja`
--
ALTER TABLE `faktura_pozycja`
  ADD CONSTRAINT `faktura_pozycja_ibfk_1` FOREIGN KEY (`faktura_id`) REFERENCES `faktura` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `gatunek_i18n`
--
ALTER TABLE `gatunek_i18n`
  ADD CONSTRAINT `gatunek_i18n_ibfk_1` FOREIGN KEY (`foreign_key`) REFERENCES `gatunek` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `hot_deal`
--
ALTER TABLE `hot_deal`
  ADD CONSTRAINT `hot_deal_ibfk_1` FOREIGN KEY (`towar_id`) REFERENCES `towar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `hot_deal_i18n`
--
ALTER TABLE `hot_deal_i18n`
  ADD CONSTRAINT `hot_deal_i18n_ibfk_1` FOREIGN KEY (`foreign_key`) REFERENCES `hot_deal` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `jednostka_i18n`
--
ALTER TABLE `jednostka_i18n`
  ADD CONSTRAINT `jednostka_i18n_ibfk_1` FOREIGN KEY (`foreign_key`) REFERENCES `jednostka` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `jezyk`
--
ALTER TABLE `jezyk`
  ADD CONSTRAINT `jezyk_ibfk_1` FOREIGN KEY (`waluta_id`) REFERENCES `waluta` (`id`) ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `karta_podarunkowa_kod`
--
ALTER TABLE `karta_podarunkowa_kod`
  ADD CONSTRAINT `karta_podarunkowa_kod_ibfk_1` FOREIGN KEY (`karta_podarunkowa_id`) REFERENCES `karta_podarunkowa` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `karta_podarunkowa_kod_ibfk_2` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownik` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `karta_podarunkowa_kod_ibfk_3` FOREIGN KEY (`zamowienie_id`) REFERENCES `zamowienie` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `kategoria`
--
ALTER TABLE `kategoria`
  ADD CONSTRAINT `kategoria_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `kategoria` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `kategoria_ibfk_2` FOREIGN KEY (`towar_promocja_id`) REFERENCES `towar` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `kategoria_ibfk_3` FOREIGN KEY (`google_kategoria_id`) REFERENCES `google_kategorie` (`id`) ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `kategoria_i18n`
--
ALTER TABLE `kategoria_i18n`
  ADD CONSTRAINT `kategoria_i18n_ibfk_1` FOREIGN KEY (`foreign_key`) REFERENCES `kategoria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `kategoria_map`
--
ALTER TABLE `kategoria_map`
  ADD CONSTRAINT `kategoria_map_ibfk_1` FOREIGN KEY (`kategoria_id`) REFERENCES `kategoria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `kody_rabatowe_kategoria`
--
ALTER TABLE `kody_rabatowe_kategoria`
  ADD CONSTRAINT `kody_rabatowe_kategoria_ibfk_1` FOREIGN KEY (`kategoria_id`) REFERENCES `kategoria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kody_rabatowe_kategoria_ibfk_2` FOREIGN KEY (`kody_rabatowe_id`) REFERENCES `kody_rabatowe` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `kody_rabatowe_producent`
--
ALTER TABLE `kody_rabatowe_producent`
  ADD CONSTRAINT `kody_rabatowe_producent_ibfk_1` FOREIGN KEY (`kody_rabatowe_id`) REFERENCES `kody_rabatowe` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kody_rabatowe_producent_ibfk_2` FOREIGN KEY (`producent_id`) REFERENCES `producent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `konfiguracja_i18n`
--
ALTER TABLE `konfiguracja_i18n`
  ADD CONSTRAINT `konfiguracja_i18n_ibfk_1` FOREIGN KEY (`foreign_key`) REFERENCES `konfiguracja` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `koszyk_towar`
--
ALTER TABLE `koszyk_towar`
  ADD CONSTRAINT `koszyk_towar_ibfk_1` FOREIGN KEY (`koszyk_id`) REFERENCES `koszyk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`jezyk_id`) REFERENCES `jezyk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `menu_ibfk_2` FOREIGN KEY (`parent_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `menu_i18n`
--
ALTER TABLE `menu_i18n`
  ADD CONSTRAINT `menu_i18n_ibfk_1` FOREIGN KEY (`foreign_key`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `okazje_towar`
--
ALTER TABLE `okazje_towar`
  ADD CONSTRAINT `okazje_towar_ibfk_1` FOREIGN KEY (`okazje_id`) REFERENCES `okazje` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `okazje_towar_ibfk_2` FOREIGN KEY (`towar_id`) REFERENCES `towar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `platnosc`
--
ALTER TABLE `platnosc`
  ADD CONSTRAINT `platnosc_ibfk_1` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownik` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `platnosc_ibfk_2` FOREIGN KEY (`rodzaj_platnosci_id`) REFERENCES `rodzaj_platnosci` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `platnosc_ibfk_3` FOREIGN KEY (`waluta_id`) REFERENCES `waluta` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `platnosc_ibfk_4` FOREIGN KEY (`zamowienie_id`) REFERENCES `zamowienie` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `powiadomienia`
--
ALTER TABLE `powiadomienia`
  ADD CONSTRAINT `powiadomienia_ibfk_1` FOREIGN KEY (`towar_id`) REFERENCES `towar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `powiadomienia_ibfk_2` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownik` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `producent_i18n`
--
ALTER TABLE `producent_i18n`
  ADD CONSTRAINT `producent_i18n_ibfk_1` FOREIGN KEY (`foreign_key`) REFERENCES `producent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `punkty_odbioru_i18n`
--
ALTER TABLE `punkty_odbioru_i18n`
  ADD CONSTRAINT `punkty_odbioru_i18n_ibfk_1` FOREIGN KEY (`foreign_key`) REFERENCES `punkty_odbioru` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `reklamacja`
--
ALTER TABLE `reklamacja`
  ADD CONSTRAINT `reklamacja_ibfk_1` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownik` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `reklamacja_pliki`
--
ALTER TABLE `reklamacja_pliki`
  ADD CONSTRAINT `reklamacja_pliki_ibfk_1` FOREIGN KEY (`reklamacja_id`) REFERENCES `reklamacja` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `reklamacja_towar`
--
ALTER TABLE `reklamacja_towar`
  ADD CONSTRAINT `reklamacja_towar_ibfk_1` FOREIGN KEY (`reklamacja_id`) REFERENCES `reklamacja` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reklamacja_towar_ibfk_2` FOREIGN KEY (`towar_id`) REFERENCES `towar` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `rodzaj_platnosci_i18n`
--
ALTER TABLE `rodzaj_platnosci_i18n`
  ADD CONSTRAINT `rodzaj_platnosci_i18n_ibfk_1` FOREIGN KEY (`foreign_key`) REFERENCES `rodzaj_platnosci` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `schowek`
--
ALTER TABLE `schowek`
  ADD CONSTRAINT `schowek_ibfk_1` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownik` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `schowek_ibfk_2` FOREIGN KEY (`towar_id`) REFERENCES `towar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `strona_i18n`
--
ALTER TABLE `strona_i18n`
  ADD CONSTRAINT `strona_i18n_ibfk_1` FOREIGN KEY (`foreign_key`) REFERENCES `strona` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `szablon_i18n`
--
ALTER TABLE `szablon_i18n`
  ADD CONSTRAINT `szablon_i18n_ibfk_1` FOREIGN KEY (`foreign_key`) REFERENCES `szablon` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `towar`
--
ALTER TABLE `towar`
  ADD CONSTRAINT `towar_ibfk_1` FOREIGN KEY (`platforma_id`) REFERENCES `platforma` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `towar_ibfk_2` FOREIGN KEY (`wersja_id`) REFERENCES `wersja` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `towar_ibfk_3` FOREIGN KEY (`bonus_id`) REFERENCES `bonus` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `towar_ibfk_4` FOREIGN KEY (`wiek_id`) REFERENCES `wiek` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `towar_ibfk_5` FOREIGN KEY (`opakowanie_id`) REFERENCES `opakowanie` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `towar_akcesoria`
--
ALTER TABLE `towar_akcesoria`
  ADD CONSTRAINT `towar_akcesoria_ibfk_1` FOREIGN KEY (`towar_id`) REFERENCES `towar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `towar_akcesoria_ibfk_2` FOREIGN KEY (`podobny_id`) REFERENCES `towar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `towar_atrybut`
--
ALTER TABLE `towar_atrybut`
  ADD CONSTRAINT `towar_atrybut_ibfk_1` FOREIGN KEY (`atrybut_id`) REFERENCES `atrybut` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `towar_atrybut_ibfk_2` FOREIGN KEY (`towar_id`) REFERENCES `towar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `towar_atrybut_ibfk_3` FOREIGN KEY (`atrybut_podrzedne_id`) REFERENCES `atrybut_podrzedne` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `towar_cena`
--
ALTER TABLE `towar_cena`
  ADD CONSTRAINT `towar_cena_ibfk_1` FOREIGN KEY (`towar_id`) REFERENCES `towar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `towar_cena_ibfk_2` FOREIGN KEY (`jednostka_id`) REFERENCES `jednostka` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `towar_cena_ibfk_3` FOREIGN KEY (`vat_id`) REFERENCES `vat` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `towar_cena_ibfk_4` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownik` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `towar_certyfikat`
--
ALTER TABLE `towar_certyfikat`
  ADD CONSTRAINT `towar_certyfikat_ibfk_1` FOREIGN KEY (`certyfikat_id`) REFERENCES `certyfikat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `towar_certyfikat_ibfk_2` FOREIGN KEY (`towar_id`) REFERENCES `towar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `towar_gatunek`
--
ALTER TABLE `towar_gatunek`
  ADD CONSTRAINT `towar_gatunek_ibfk_1` FOREIGN KEY (`towar_id`) REFERENCES `towar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `towar_gatunek_ibfk_2` FOREIGN KEY (`gatunek_id`) REFERENCES `gatunek` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `towar_i18n`
--
ALTER TABLE `towar_i18n`
  ADD CONSTRAINT `towar_i18n_ibfk_1` FOREIGN KEY (`foreign_key`) REFERENCES `towar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `towar_kategoria`
--
ALTER TABLE `towar_kategoria`
  ADD CONSTRAINT `towar_kategoria_ibfk_1` FOREIGN KEY (`towar_id`) REFERENCES `towar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `towar_kategoria_ibfk_2` FOREIGN KEY (`kategoria_id`) REFERENCES `kategoria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `towar_opinia`
--
ALTER TABLE `towar_opinia`
  ADD CONSTRAINT `towar_opinia_ibfk_1` FOREIGN KEY (`towar_id`) REFERENCES `towar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `towar_podobne`
--
ALTER TABLE `towar_podobne`
  ADD CONSTRAINT `towar_podobne_ibfk_1` FOREIGN KEY (`towar_id`) REFERENCES `towar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `towar_podobne_ibfk_2` FOREIGN KEY (`podobny_id`) REFERENCES `towar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `towar_polecane`
--
ALTER TABLE `towar_polecane`
  ADD CONSTRAINT `towar_polecane_ibfk_1` FOREIGN KEY (`towar_id`) REFERENCES `towar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `towar_polecane_ibfk_2` FOREIGN KEY (`podobny_id`) REFERENCES `towar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `towar_wariant`
--
ALTER TABLE `towar_wariant`
  ADD CONSTRAINT `towar_wariant_ibfk_1` FOREIGN KEY (`towar_id`) REFERENCES `towar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `towar_wariant_ibfk_2` FOREIGN KEY (`kolor_id`) REFERENCES `kolor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `towar_wariant_ibfk_3` FOREIGN KEY (`rozmiar_id`) REFERENCES `rozmiar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `towar_zdjecie`
--
ALTER TABLE `towar_zdjecie`
  ADD CONSTRAINT `towar_zdjecie_ibfk_1` FOREIGN KEY (`towar_id`) REFERENCES `towar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `uzytkownik_adres`
--
ALTER TABLE `uzytkownik_adres`
  ADD CONSTRAINT `uzytkownik_adres_ibfk_1` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownik` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `wersja_i18n`
--
ALTER TABLE `wersja_i18n`
  ADD CONSTRAINT `wersja_i18n_ibfk_1` FOREIGN KEY (`foreign_key`) REFERENCES `wersja` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `wysylka_i18n`
--
ALTER TABLE `wysylka_i18n`
  ADD CONSTRAINT `wysylka_i18n_ibfk_1` FOREIGN KEY (`foreign_key`) REFERENCES `wysylka` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `wysylka_koszt`
--
ALTER TABLE `wysylka_koszt`
  ADD CONSTRAINT `wysylka_koszt_ibfk_1` FOREIGN KEY (`wysylka_id`) REFERENCES `wysylka` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `wysylka_kraje_koszty`
--
ALTER TABLE `wysylka_kraje_koszty`
  ADD CONSTRAINT `wysylka_kraje_koszty_ibfk_1` FOREIGN KEY (`wysylka_id`) REFERENCES `wysylka` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `wysylka_platnosc`
--
ALTER TABLE `wysylka_platnosc`
  ADD CONSTRAINT `wysylka_platnosc_ibfk_1` FOREIGN KEY (`wysylka_id`) REFERENCES `wysylka` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `wysylka_platnosc_ibfk_2` FOREIGN KEY (`rodzaj_platnosci_id`) REFERENCES `rodzaj_platnosci` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `wysylka_punkty_odbioru`
--
ALTER TABLE `wysylka_punkty_odbioru`
  ADD CONSTRAINT `wysylka_punkty_odbioru_ibfk_1` FOREIGN KEY (`punkty_odbioru_id`) REFERENCES `punkty_odbioru` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `wysylka_punkty_odbioru_ibfk_2` FOREIGN KEY (`wysylka_id`) REFERENCES `wysylka` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `zamowienie`
--
ALTER TABLE `zamowienie`
  ADD CONSTRAINT `zamowienie_ibfk_1` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownik` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `zamowienie_ibfk_2` FOREIGN KEY (`waluta_id`) REFERENCES `waluta` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `zamowienie_dhl`
--
ALTER TABLE `zamowienie_dhl`
  ADD CONSTRAINT `zamowienie_dhl_ibfk_1` FOREIGN KEY (`zamowienie_id`) REFERENCES `zamowienie` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `zamowienie_towar`
--
ALTER TABLE `zamowienie_towar`
  ADD CONSTRAINT `zamowienie_towar_ibfk_1` FOREIGN KEY (`zamowienie_id`) REFERENCES `zamowienie` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `zamowienie_towar_ibfk_2` FOREIGN KEY (`towar_id`) REFERENCES `towar` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `zamowienie_towar_ibfk_3` FOREIGN KEY (`wariant_id`) REFERENCES `towar_atrybut` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `zamowienie_towar_ibfk_4` FOREIGN KEY (`jednostka_id`) REFERENCES `jednostka` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `zamowienie_towar_ibfk_5` FOREIGN KEY (`vat_id`) REFERENCES `vat` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `zapytanie`
--
ALTER TABLE `zapytanie`
  ADD CONSTRAINT `zapytanie_ibfk_1` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownik` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `zestaw`
--
ALTER TABLE `zestaw`
  ADD CONSTRAINT `zestaw_ibfk_1` FOREIGN KEY (`towar_id`) REFERENCES `towar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `zestaw_towar`
--
ALTER TABLE `zestaw_towar`
  ADD CONSTRAINT `zestaw_towar_ibfk_1` FOREIGN KEY (`towar_id`) REFERENCES `towar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `zestaw_towar_ibfk_2` FOREIGN KEY (`zestaw_id`) REFERENCES `zestaw` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
