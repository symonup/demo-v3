<?php

namespace AllegroWebAPISoapClient;

use Cake\Core\Configure;
use DOMDocument;
use \SoapClient;
use \SoapFault;
use Cake\Cache\Cache;

class AllegroWebAPISoapClient extends SoapClient {

    private $form_fields = array();
    private $variants = array();
    private $sell_form_fields = array();
    private $allegro;
    public $country_code = 1;
    public $webapi_key;
    public $wsdlAddr = 'https://webapi.allegro.pl/service.php?wsdl';//'https://webapi.allegro.pl.webapisandbox.pl/service.php?wsdl';//

    public function __construct() {
        try {
            parent::__construct($this->wsdlAddr);
        } catch (SoapFault $f) {
            print($f->faultstring);
        }
    }
    public function login($country_code, $login, $password) {
        try {
            $this->country_code = $country_code;
            $password = base64_encode(hash('sha256', $password, true));

            $this->allegro['status'] = $this->doQuerySysStatus(array('sysvar' => 1, 'countryId' => $this->country_code, 'webapiKey' => $this->webapi_key));

            $this->allegro['user'] = $this->doLoginEnc(array(
                'userLogin' => $login, 'userHashPassword' => $password, 'countryCode' => $this->country_code, 'webapiKey' => $this->webapi_key, 'localVersion' => $this->allegro['status']->verKey));

            return $this->allegro;
        } catch (SoapFault $f) {
            return $f->faultstring;
        }
    }

    public function newAuctionExt($aukcja, $login, $password) {
        try {
           $this->login($this->country_code, $login, $password);
           $przesylka=$aukcja['allegro_aukcja_przesylka'];
           $polaKat=$aukcja['allegro_pola_kategoria'];
           $wariantyKat=$aukcja['allegro_aukcja_wariant'];
            unset($aukcja['allegro_pola_kategoria'], $aukcja['allegro_aukcja_przesylka'],$aukcja['allegro_aukcja_wariant']);
            $this->newAuctionPrepareData($aukcja, $przesylka, !empty($polaKat) ? $polaKat : []);
            if (!empty($wariantyKat)) {
                $warianty = array();
                foreach ($wariantyKat as $wariant) {
                    $fidW = $wariant['fid'];
                    $warianty[$wariant['mask']] = $wariant['quantity'];
                }
                $this->setFormVariant($fidW, $warianty);
            }
            foreach ($this->form_fields as $fieldTmp)
                $newField[] = $fieldTmp;
            if(!empty($this->variants)) return $this->doNewAuctionExt(array('sessionHandle' => $this->allegro['user']->sessionHandlePart, 'fields' => $newField, 'variants' => $this->variants));
            else return $this->doNewAuctionExt(array('sessionHandle' => $this->allegro['user']->sessionHandlePart, 'fields' => $newField));
        } catch (SoapFault $f) {
            return $f->faultstring;
        }
    }

    public function checkNewAuctionExt($aukcja, $login, $password) {
        try {
            $this->login($this->country_code, $login, $password);
            $przesylka = $aukcja['allegro_aukcja_przesylka'];
            $kategoria = $aukcja['allegro_pola_kategoria'];
            unset($aukcja['allegro_pola_kategoria'], $aukcja['allegro_aukcja_przesylka']);
            $this->newAuctionPrepareData($aukcja, $przesylka, !empty($kategoria) ? $kategoria : array());

            foreach ($this->form_fields as $fieldTmp)
                $newField[] = $fieldTmp;
//            return $newField;
            if(!empty($this->variants))
                return $this->doCheckNewAuctionExt(array('sessionHandle' => $this->allegro['user']->sessionHandlePart, 'fields' => $newField, 'variants' => $this->variants));
            else return $this->doCheckNewAuctionExt(array('sessionHandle' => $this->allegro['user']->sessionHandlePart, 'fields' => $newField));
        } catch (SoapFault $f) {
            return $f->faultstring;
        }
    }

    public function getCategoriesTree($parent_id = 0, $cache = false) {
        try {
            if ($cache || !$output = Cache::read('allegro_kategorie_' . $this->country_code)) {
                $output = $parents = array();
                $cats_data = $this->doGetCatsData(array('countryId' => $this->country_code, 'localVersion' => null, 'webapiKey' => $this->webapi_key));

                foreach ($cats_data->catsList->item as $value) {
                    $output['options'][$value->{'catParent'}][$value->{'catId'}] = $value->{'catName'};
                    $parents[$value->{'catId'}] = $value->{'catParent'};
                }
                foreach ($parents as $key => $parent) {
                    while (array_key_exists($parent, $parents)) {
                        $output['parents'][$key][] = (string) $parent;
                        $parent = $parents[$parent];
                    }
                    $output['parents'][$key][] = (string) $key;
                }
                if (!$cache) {
                    Cache::write('allegro_kategorie_' . $this->country_code, $output);
                }
            }
            return json_encode($output);
        } catch (SoapFault $f) {
            print($f->faultcode);
        }
    }

    private function newAuctionPrepareData($aukcja, $dostawa, $pola) {
//        if ($aukcja['typ_aukcji'] == 2) {
//            $aukcja['typ_aukcji'] = 0;
//        }
        $this->setForm(array(
            1 => array('fvalueString', $aukcja['tytul']), // Tytuł
            2 => array('fvalueInt', $aukcja['kategoria']), // Kategoria
            4 => array('fvalueInt', $aukcja['czas_trwania']), // Czas trwania aukcji
            5 => array('fvalueInt', $aukcja['ilosc']), // Liczba sztuk
            9 => array('fvalueInt', $aukcja['kraj']), // Kraj
            10 => array('fvalueInt', $aukcja['wojewodztwo']), // Wojewodztwo
            11 => array('fvalueString', $aukcja['miasto']), // Miasto
            12 => array('fvalueInt', $aukcja['koszt_wysylki']), // Transport
            13 => array('fvalueInt', $aukcja['transport']), // Opcje dot. transportu
            14 => array('fvalueInt', $aukcja['platnosc']), // Formy platnosci
            15 => array('fvalueInt', $aukcja['opcje_dodatkowe']), // Opcje dodatkowe
            24 => array('fvalueString', $aukcja['opis']), // Opis
            27 => array('fvalueString', $aukcja['transport_platnosc_opis']), // Dodatkowe informacje o przesyłce i płatności
            29 => array('fvalueInt', (int) $aukcja['typ_aukcji']), // Format sprzedaży
            32 => array('fvalueString', $aukcja['kod']), // Kod pocztowy
            35 => array('fvalueInt', $aukcja['darmowa_opcja_przesylki']), // Darmowe opcje przesyłki
        ));

        //ceny
        if (!empty($aukcja['cena_wywolawcza'])) {
            $this->setFormField(6, 'fvalueFloat', $aukcja['cena_wywolawcza']); // Cena wywolawcza
        }
        if (!empty($aukcja['cena_minimalna'])) {
            $this->setFormField(7, 'fvalueFloat', $aukcja['cena_minimalna']); // Cena minimalna
        }
        if (!empty($aukcja['cena_kup_teraz'])) {
            $this->setFormField(8, 'fvalueFloat', $aukcja['cena_kup_teraz']); // Cena "Kup Teraz"
        }

        // Forma transportu
        foreach ($dostawa as $key => $value) {
            $this->setFormField($value['fid'], 'fvalueFloat', $value['pierwsza_sztuka']);
            if (!empty($value['kolejna_sztuka'])) {
                $this->setFormField("1".$value['fid'], 'fvalueFloat', $value['kolejna_sztuka']);
            }
            if (!empty($value['ilosc_w_paczce'])) {
                $this->setFormField("2".$value['fid'], 'fvalueInt', $value['ilosc_w_paczce']);
            }
        }

        // Zdjęcia
        for ($index = 1; $index <= 8; $index++) {
            if (!empty($aukcja['zdjecie' . $index])) {
                $path = Configure::read('filePath');
                $imagedata = $this->getImage($path['towar'] . $aukcja['zdjecie' . $index]);
                $this->setFormField(15 + $index, 'fvalueImage', $imagedata); // zdjecia 16 - 23
            } else {
                break;
            }
        }

        foreach ($pola as $key => $value) {
            if (!empty($value['value'])) {
                $field = 'fvalueString';
                if ($value['type'] == 2) {
                    $field = 'fvalueInt';
                } elseif ($value['type'] == 3) {
                    $field = 'fvalueFloat';
                } elseif ($value['type'] == 7) {
                    $field = 'fvalueImage';
                } elseif ($value['type'] == 9) {
                    $field = 'fvalueDatetime';
                } elseif ($value['type'] == 13) {
                    $field = 'fvalueDate';
                }
                $this->setFormField($value['fid'], $field, $value['value']);
                if (!empty($value['wariant'])) {
                    $wariants = array();
                    foreach ($value['wariant'] as $vKey => $valOpt) {
                        if (!empty($valOpt))
                            $wariants[$vKey] = $valOpt;
//                        else $wariants[$vKey]=0;
                    }
                    $this->setFormVariant($value['fid'], $wariants);
                }
            }
        }
    }

    private function setFormField($fid, $field, $value) {
        $this->form_fields[$fid] = array(
            'fid' => $fid,
            'fvalueString' => '',
            'fvalueInt' => 0,
            'fvalueFloat' => 0,
            'fvalueImage' => 0,
            'fvalueDatetime' => 0,
            'fvalueDate' => '',
            'fvalueRangeInt' => array(
                'fvalueRangeIntMin' => 0,
                'fvalueRangeIntMax' => 0
            ),
            'fvalueRangeFloat' => array(
                'fvalueRangeFloatMin' => 0,
                'fvalueRangeFloatMax' => 0
            ),
            'fvalueRangeDate' => array(
                'fvalueRangeDateMin' => '',
                'fvalueRangeDateMax' => ''
            )
        );

        if (is_array($value) && count($value) == 2) {
            $this->form_fields[$fid][$field] = array($field . 'Min' => $value[0], $field . 'Max' => $value[1]);
        } else {
            $this->form_fields[$fid][$field] = $value;
        }
    }

    private function setFormVariant($fid, $variants) {
        $warianty = array();
        foreach ($variants as $keyV => $wariant) {
            $warianty[] = array('mask' => $keyV, 'quantity' => $wariant);
        }
        $this->variants[] = array(
            'fid' => $fid,
            'quantities' => $warianty
        );
    }

    private function getImage($url) {
        $image = file_get_contents($url);

        while (strlen(base64_encode($image)) > 200000) {
            $temp = imagecreatefromstring($image);
            $x = ceil(0.9 * imagesx($temp));
            $y = ceil(0.9 * imagesy($temp));

            $image = imagecreatetruecolor($x, $y);
            imagecopyresized($image, $temp, 0, 0, 0, 0, $x, $y, imagesx($temp), imagesy($temp));

            imagejpeg($image, 'temp.jpg', 75);
            $image = file_get_contents('temp.jpg');
            unlink('temp.jpg');
        }

        return $image;
    }

    private function setForm($form_fields) {
        foreach ($form_fields as $key => $value) {
            $this->setFormField($key, $value[0], $value[1]);
        }
    }

    public function getSellFormFieldsOptions($country_code) {
        try {
            $output = array();
            $this->country_code = $country_code;
            if (empty($this->sell_form_fields)) {
                $this->sell_form_fields = $this->doGetSellFormFieldsExt(array('countryCode' => $this->country_code, 'localVersion' => null, 'webapiKey' => $this->webapi_key));
            }
            foreach ($this->sell_form_fields->sellFormFields->item as $value) {

                $fid = $value->{'sellFormId'};
                $option = explode('|', $value->{'sellFormDesc'});
                $index = explode('|', $value->{'sellFormOptsValues'});
                foreach ($option as $key => $value) {
                    if ($value != '-') {
                        $output[$fid][$index[$key]] = $value;
                    }
                }
            }
            return $output;
        } catch (SoapFault $f) {
            return $f->faultstring;
        }
    }

    public function getDeliveryFieldsOptions($country_code, $from = 36, $to = 52) {
        try {
            $output = array();
            $this->country_code = $country_code;
            if (empty($this->sell_form_fields)) {
                $this->sell_form_fields = $this->doGetSellFormFieldsExt($this->country_code, null, $this->webapi_key);
            }
            foreach ($this->sell_form_fields->sellFormFields->item as $value) {
                $fid = $value->{'sellFormId'};
                if ($fid >= $from && $fid <= $to) {
                    $output[$fid] = '<span>' . trim(str_replace('(pierwsza sztuka)', '', $value->{'sellFormTitle'})) . '</span>';
                } elseif ($fid > $to) {
                    break;
                }
            }
            return $output;
        } catch (SoapFault $f) {
            return $f->faultstring;
        }
    }

    public function getSellFormFieldsForCategory($country_code, $category_id, $warianty) {
        $wariantArray = array(26388, 127048);
        try {
            $output = array();
            $sell_form_fields_for_category = $this->doGetSellFormFieldsForCategory(array('webapiKey' => $this->webapi_key, 'countryId' => $country_code, 'categoryId' => $category_id));
//            return $sell_form_fields_for_category->sellFormFieldsForCategory;
            foreach ($sell_form_fields_for_category->sellFormFieldsForCategory->sellFormFieldsList->item as $value) {
                if (!empty($value->{'sellFormCat'})) {
                    $fid = $value->{'sellFormId'};
                    $option = explode('|', $value->{'sellFormDesc'});
                    $index = explode('|', $value->{'sellFormOptsValues'});
                    $output[$fid] = array(
                        'fid' => $fid,
                        'type' => $value->{'sellFormType'},
                        'res-type' => $value->{'sellFormResType'},
                        'required' => $value->{'sellFormOpt'} == 1,
                        'title' => $value->{'sellFormTitle'},
                        'desc' => $value->{'sellFormFieldDesc'},
                        'options' => array()
                    );
                    $paramId = $value->{'sellFormParamId'};
                    foreach ($option as $key => $value) {
                        $wariantValue = '';
                        if ($value != '-') {

                            if (!empty($warianty)) {
                                foreach ($warianty as $wariant) {
                                    if ($wariant['fid'] == $fid && $index[$key] == $wariant['mask'])
                                        $wariantValue = $wariant['quantity'];
                                }
                            }
                            $output[$fid]['options'][$index[$key]] = $value . ((in_array($paramId, $wariantArray)) ? ' <input id="allegro-pola-kategoria' . $fid . '-wariant-' . $index[$key] . '" value="' . $wariantValue . '" name="allegro_pola_kategoria[' . $fid . '][wariant][' . $index[$key] . ']" type="number" placeholder="ilość" class="short"/>' : '');
                            $output[$fid]['variants'][$index[$key]] = $index[$key];
                        }
                    }
                }
            }
            return $output;
        } catch (SoapFault $f) {
            return $f->faultstring;
        }
    }

    public function getStatesInfo($country_code) {
        try {
            $output = array();
            foreach ($this->doGetStatesInfo(array('countryCode' => $country_code, 'webapiKey' => $this->webapi_key))->statesInfoArray->item as $value) {
                $output[$value->{'stateId'}] = $value->{'stateName'};
            }
            return $output;
        } catch (SoapFault $f) {
            return $f->faultstring;
        }
    }

    public function getSumOptionsSelected($value) {
        $output = array();
        foreach (array_reverse(str_split(decbin($value))) as $key => $value) {
            if (!empty($value)) {
                $output[] = bindec(str_pad($value, $key + 1, '0'));
            }
        }
        return $output;
    }

    public function getPostBuyData($items_ids, $login = null, $password = null, $country_code = null) {
        try {
            if (empty($this->allegro['user']->sessionHandlePart)) {
                $this->login($country_code, $login, $password);
            }
            $output = array();

            foreach (array_chunk($items_ids, 25) as $ids) {
//                print_r($this->doGetPostBuyData(array('sessionHandle' =>$this->allegro['user']->sessionHandlePart, 'itemsArray' =>$ids))); die();
                $output = array_merge($output, $this->doGetPostBuyData(array('sessionHandle' => $this->allegro['user']->sessionHandlePart, 'itemsArray' => $ids))->itemsPostBuyData->item);
            }
//            print_r($output); die();
            return $output;
        } catch (SoapFault $f) {
            return $f->faultstring;
        }
    }

    public function getItemsInfo($items_ids, $login = null, $password = null, $country_code = null) {
        try {
            if (empty($this->allegro['user']->sessionHandlePart)) {
                $this->login($country_code, $login, $password);
            }
            $output = array();

            foreach (array_chunk($items_ids, 25) as $ids) {
                $output = array_merge($output, $this->doGetItemsInfo(array('sessionHandle' => $this->allegro['user']->sessionHandlePart, 'itemsIdArray' => $ids))->arrayItemListInfo->item);
            }
            return $output;
        } catch (SoapFault $f) {
            return $f->faultstring;
        }
    }

    public function getBidItem2($item_id, $login = null, $password = null, $country_code = null) {
        try {
            if (empty($this->allegro['user']->sessionHandlePart)) {
                $this->login($country_code, $login, $password);
            }
            return $this->doGetBidItem2(array('sessionHandle' => $this->allegro['user']->sessionHandlePart, 'itemId' => $item_id));
        } catch (SoapFault $f) {
            return $f->faultstring;
        }
    }

    public function changeQuantityItem($item_id, $ilosc, $login = null, $password = null, $country_code = null) {
        try {
            if (empty($this->allegro['user']->sessionHandlePart)) {
                $this->login($country_code, $login, $password);
            }
            return $this->doChangeQuantityItem(array('sessionHandle' => $this->allegro['user']->sessionHandlePart, 'itemId' => $item_id, 'newItemQuantity' => $ilosc));
        } catch (SoapFault $f) {
            return $f->faultstring;
        }
    }

    public function finishItem($item_id, $login = null, $password = null, $country_code = null) {
        try {
            if (empty($this->allegro['user']->sessionHandlePart)) {
                $this->login($country_code, $login, $password);
            }
            return $this->doFinishItem(array('sessionHandle' => $this->allegro['user']->sessionHandlePart, 'finishItemId' => $item_id));
        } catch (SoapFault $f) {
            return $f->faultstring;
        }
    }

    public function getSoldItems() {
        try {
            if (empty($this->allegro['user']->sessionHandlePart)) {
                $this->login($country_code, $login, $password);
            }
            $itemsReturn = array();
            $items = $this->doGetMySoldItems(array('sessionId' => $this->allegro['user']->sessionHandlePart, 'pageSize' => 1000));
            if($items->soldItemsCounter == 0) return false;
            $itemsReturn = $items->soldItemsList->item;
            $pageCount = $items->soldItemsCounter / 1000;
            if ($pageCount > 1) {
                for ($pi = 1; $pi < ceil($pageCount); $pi++) {
                    $items = $this->doGetMySoldItems(array('sessionId' => $this->allegro['user']->sessionHandlePart, 'pageSize' => 1000, 'pageNumber' => $pi));
                    $itemsReturn = array_merge($itemsReturn, $items->soldItemsList->item);
                }
            }
            return $itemsReturn;
        } catch (SoapFault $f) {
            return $f->faultstring;
        }
    }

    public function getTransactionById($trId, $login, $password, $country_code = 1) {
        try {

            if (empty($this->allegro['user']->sessionHandlePart)) {
                $this->login($country_code, $login, $password);
            }
            $rData = $this->doGetPostBuyFormsDataForSellers(array(
                'sessionId' => $this->allegro['user']->sessionHandlePart,
                'transactionsIdsArray' => array($trId)
            ));
            return $rData->postBuyFormData->item;
        } catch (SoapFault $f) {
            return $f->faultstring;
        }
    }

    public function getTransaction($login, $password, $country_code, $aukcjeOld) {
        try {
           $_login=$this->login($country_code, $login, $password);
           if(is_string($_login)) return $_login;
            $soldItems = $this->getSoldItems();
            if($soldItems === false) return false;
            $newAukIds = array();
            foreach ($soldItems as $tmpItem) {
                $newAukIds['_' . $tmpItem->itemId] = $tmpItem->itemId;
            }
            $transactionArray = array();
            foreach (array_chunk($newAukIds, 25) as $ids) {
                $sendData = array(
                    'sessionHandle' => $this->allegro['user']->sessionHandlePart,
                    'itemsIdArray' => $ids,
                    'userRole' => 'seller');
                $rData = $this->doGetTransactionsIDs($sendData);
                $transactionArray = array_merge($transactionArray, $rData->transactionsIdsArray->item);
            }
            $transArr = array_unique($transactionArray);
            $result = array_diff($transArr, $aukcjeOld);
            $posData = array();
            foreach (array_chunk($result, 25) as $TrIds) {
                $rData = $this->doGetPostBuyFormsDataForSellers(array(
                    'sessionId' => $this->allegro['user']->sessionHandlePart,
                    'transactionsIdsArray' => $TrIds
                ));
                $posData = array_merge($posData, (is_array($rData->postBuyFormData->item)?$rData->postBuyFormData->item:[$rData->postBuyFormData->item]));
            }
            return $posData;
        } catch (SoapFault $f) {
            return $f->faultstring;
        }
    }

    public function getWysylka($wsId, $login, $password, $country_code = 1) {
        try {

            if (empty($this->allegro['user']->sessionHandlePart)) {
                $this->login($country_code, $login, $password);
            }
            $wysylka = $this->doGetShipmentData(array(
                'countryId' => 1,
                'webapiKey' => $this->webapi_key));
            $rWysylka = '';
            foreach ($wysylka->shipmentDataList->item as $ws) {
                if ($ws->shipmentId == $wsId)
                    $rWysylka = $ws->shipmentName;
            }
            return $rWysylka;
        } catch (SoapFault $f) {
            return $f->faultstring;
        }
    }

}

?>
