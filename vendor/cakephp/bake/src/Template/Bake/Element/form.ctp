<%
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.1.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Utility\Inflector;

$fields = collection($fields)
    ->filter(function($field) use ($schema) {
        return $schema->columnType($field) !== 'binary';
    });

if (isset($modelObject) && $modelObject->hasBehavior('Tree')) {
    $fields = $fields->reject(function ($field) {
        return $field === 'lft' || $field === 'rght';
    });
}


if (isset($modelObject) && $modelObject->hasBehavior('Translate')) {
    $translate=true;
}
else{
$translate=false;
}
%>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
                <?= $this->Form->create($<%= $singularVar %>) ?>
            <div class="x_title">
                    <h2><?=$this->Txt->printAdmin(__('Admin | <%= Inflector::humanize($action) %> {0}',[__('Admin | <%= $singularHumanName %>')]))?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <% if (strpos($action, 'add') === false): %>
        <li><?= $this->Html->link(
                $this->Html->Tag('i','',['class'=>'fa fa-trash']).' '.$this->Txt->printAdmin(__('Admin | Usuń')),
                ['action' => 'delete', $<%= $singularVar %>-><%= $primaryKey[0] %>],
                ['confirm-btn-yes'=>$this->Txt->printAdmin(__('Admin | Tak')),'confirm-btn-no'=>$this->Txt->printAdmin(__('Admin | Nie')),'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $<%= $singularVar %>-><%= $primaryKey[0] %>)),'class'=>'btn btn-xs btn-danger confirm-link','escape'=>false]
            )?></li>
<% endif; %>
                      <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> '.__('Admin | Wróć do {0}',[__('Admin | Listy')])), ['action' => 'index'],['escape'=>false,'class'=>'btn btn-xs btn-warning']) ?></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
            <div class="x_content">
    <fieldset>
        <?php
<%
        
            if($translate && !empty($associations['HasOne'])){
                $tmpHasOne=array_keys($associations['HasOne']);
            }
            %>
            $lngInputs=[];
            <%
        foreach ($fields as $field) {
            if (in_array($field, $primaryKey)) {
                continue;
            }
            if($translate && !empty($tmpHasOne)){
                if(array_search($modelClass.'_'.$field.'_translation', $tmpHasOne)!==false){
            %>
                    $lngInputs['<%= $field %>']=['label'=>$this->Txt->printAdmin(__('Admin | <%= $field %> - {0}'))];
            <%
                    continue;
                }
            }
            if (isset($keyFields[$field])) {
                $fieldData = $schema->column($field);
                if (!empty($fieldData['null'])) {
%>
            echo $this->Form->control('<%= $field %>', ['options' => $<%= $keyFields[$field] %>, 'empty' => true,'label'=>$this->Txt->printAdmin(__('Admin | <%= $field %>'))]);
<%
                } else {
%>
            echo $this->Form->control('<%= $field %>', ['options' => $<%= $keyFields[$field] %>,'label'=>$this->Txt->printAdmin(__('Admin | <%= $field %>'))]);
<%
                }
                continue;
            }
            if (!in_array($field, ['created', 'modified', 'updated'])) {
                $fieldData = $schema->column($field);
                if (in_array($fieldData['type'], ['date', 'datetime', 'time']) && (!empty($fieldData['null']))) {
%>
            echo $this->Form->control('<%= $field %>', ['type'=>'text','datepicker'=>'<%= $fieldData['type'] %>','empty' => true,'label'=>$this->Txt->printAdmin(__('Admin | <%= $field %>')),'default' => date('Y-m-d H:i:s')]);
<%
                } else {
%>
            echo $this->Form->control('<%= $field %>',['label'=>$this->Txt->printAdmin(__('Admin | <%= $field %>'))]);
<%
                }
            }
        }
        if($translate){
            %>
                    echo $this->Translation->inputs($this->Form, $languages, $lngInputs);
                    <%
        }
        if (!empty($associations['BelongsToMany'])) {
            foreach ($associations['BelongsToMany'] as $assocName => $assocData) {
%>
            echo $this->Form->control('<%= $assocData['property'] %>._ids', ['options' => $<%= $assocData['variable'] %>]);
<%
            }
        }
%>
        ?>
    </fieldset>
            </div>
            <div class="x_footer text-right">
    <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')),['class'=>'btn btn-sm btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
