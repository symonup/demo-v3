<?php

namespace Inpost;

class Inpost {

    protected $apiUrl = 'https://api-shipx-pl.easypack24.net/';
    protected $sandboxUrl = 'https://sandbox-api-shipx-pl.easypack24.net/';
    private $url;
    public $allPoints = [];

    public function __construct($test = false) {
        $this->url = ($test ? $this->sandboxUrl : $this->apiUrl);
    }

    private function sendRequest($url) {
        if (empty($url)) {
            return false;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result);
    }

    public function getPoints($page = 1, $perPage = 100) {
        $results = $this->sendRequest($this->url . 'v1/points?page=' . $page . '&per_page=' . $perPage);
        if (!empty($results)) {
            $this->allPoints = array_merge($this->allPoints, $results->items);
            if (!empty($results->total_pages) && $results->total_pages > $page) {
                return $this->getPoints(($page + 1), $perPage);
            } else {
                return $this->allPoints;
            }
        } else {
            return false;
        }
    }

    public function newShip() {
        $ship = [
            "comments" => "dowolny komentarz",
            "receiver" => [
                "first_name" => "Jan",
                "last_name" => "Kowalski",
                "name" => "Nazwa",
                "email" => "receiver@example.com",
                "phone" => "888000000",
                "address" => [
                    "street" => "Malborska",
                    "building_number" => "130",
                    "city" => "Kraków",
                    "post_code" => "30-624",
                    "country_code" => "PL"
                ]
            ],
            "parcels" => [
                [
                    "id" => "small package",
                    "template" => "small",
                    "dimensions" => [
                        "length" => "80",
                        "width" => "360",
                        "height" => "640",
                        "unit" => "mm"
                    ],
                    "weight" => [
                        "amount" => "25",
                        "unit" => "kg"
                    ],
                    "tracking_number" => null,
                    "is_non_standard" => false
                ]
            ],
            "custom_attributes" => [
                "target_point" => "KRA010"
            ],
            "insurance" => [
                "amount" => 25,
                "currency" => "PLN"
            ],
            "cod" => [
                "amount" => 12.50,
                "currency" => "PLN"
            ],
            "service" => "inpost_courier_standard",
            "additional_services" => ["email", "sms"]
        ];
    }

}
