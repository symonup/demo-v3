<?php

class dhl extends SoapClient {

    const WSDLTEST = 'https://sandbox.dhl24.com.pl/webapi2';
    const WSDL = 'https://dhl24.com.pl/webapi2';

    private $authData = [];

    //https://sandbox.dhl24.com.pl/webapi2
    //'https://dhl24.com.pl/webapi2';
    public function __construct($username,$password,$test=true) {
        parent::__construct(($test?self::WSDLTEST:self::WSDL), array('trace' => 1));
        $this->setAuth($username,$password);
    }

    public function version() {
        return $this->getVersion();
    }

    private function setAuth($username,$password) {
        $this->authData = ['username' => $username, 'password' => $password];
    }

    private function getAuth() {
        return $this->authData;
    }

    public function getPostCodes($postCode, $date = null) {
        try {
            if (empty($date)) {
                $date = date('Y-m-d');
            }
            $params = [
                'authData' => $this->getAuth(),
                'postCode' => $postCode,
                'pickupDate' => $date
            ];
            return $this->getPostalCodeServices($params);
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }

    public function newShipment($shipmentData) {
        try {
            $params = [
                'authData' => $this->getAuth(),
                'shipments' => $shipmentData
            ];
            return $this->createShipments($params);
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }
    
    public function getLabel($items){
        try {
            $params = [
                'authData' => $this->getAuth(),
                'itemsToPrint' => $items
            ];
            return $this->getLabels($params);
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }
    public function book($data){
        try {
            $params= array_merge(['authData' => $this->getAuth()],$data);
        return $this->bookCourier($params);    
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }
    
    public function cancel_book($data){
        try {
            $params= array_merge(['authData' => $this->getAuth()],$data);
        return $this->cancelCourierBooking($params);    
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }
    public function deleteShipment($data){
        try {
            $params= array_merge(['authData' => $this->getAuth()],$data);
        return $this->deleteShipments($params);    
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }
    public function getShipments($data){
        try {
            $params= array_merge(['authData' => $this->getAuth()],$data);
        return $this->getMyShipments($params);    
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }

}
