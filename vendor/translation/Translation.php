<?php

//$this->Translation->get('default.TestTranslacjiZParameterm1{0}IParametrem2{1}Dziala', ['test1', 'test2']);

namespace Translation;

use Cake\Core\Configure;
use Cake\I18n\I18n;
use DOMDocument;

class Translation {

    private $xml;
    private $path;
    private $active;
    private $files = [];
    private $values = ['{' => '_-', '}' => '-_'];

    public function __construct() {
        $locale = I18n::locale();
        $this->path = Configure::read('App.paths.locales.0');
        $this->active = $this->path . $locale . '.xml';
        if (!file_exists($this->active)) {
            $this->add($locale);
        }
        if (file_exists($this->active)) {
            $this->xml = simplexml_load_file($this->active);
        }
    }

    public function setPath($path) {
        $this->path=$path;
    }
    public function setLocale($locale) {
        $this->active = $this->path . $locale . '.xml';
        if (!file_exists($this->active)) {
            $this->add($locale);
        }
        if (file_exists($this->active)) {
            $this->xml = simplexml_load_file($this->active);
        }
    }

    public function get($key, $param = [],$wysiwyg=false) {
        if (!strpos($key, '.')) {
            $key = 'default.' . $key;
        }
        list($prefix, $key) = explode('.', $key);
        $value = $key;
        $key = $this->getKey($key);
        $node = $this->xml->{$prefix}->{$key};
        if (empty($node)) {
            foreach (scandir(substr($this->path, 0, -1)) as $file) {
                if (empty($this->files[$file]) && !is_dir($this->path . $file) && $xml = simplexml_load_file($this->path . $file)) {
                    $this->files[$file] = $xml;
                }
                if (!empty($this->files[$file])) {
                    $_node = $this->files[$file]->{$prefix}->{$key};
                    if (empty($_node)) {
                        $this->set($prefix, $key, $value, $this->files[$file],$wysiwyg);
                    }
                }
            }

            return __((string) $value, $param);
        }
        return __((string) $node, $param);
    }

    public function add($locale) {
        copy($this->path . 'default.xml', $this->path . $locale . '.xml');
        mkdir($this->path . $locale);
    }

    public function check($locale) {
        return file_exists($this->path . $locale . '.xml');
    }

    public function getLang() {
        return $this->xml;
    }

    public function save($data) {
        try {
            foreach ($data as $prefix => $values) {
                if(!empty($values)){
                foreach ($values as $key => $value) {
                    if ((string) $this->xml->{$prefix}->{$key} != $value) {
                        $this->set($prefix, $key, $value);
                    }
                }
                }
            }
            $this->xml->asXml($this->active);
            return true;
        } catch (Exception $exc) {
            return false;
        }
    }

    public function saveFiles() {
        if (!empty($this->files)) {
            foreach ($this->files as $file => $xml) {
                $xml->asXml($this->path . $file);
                unset($this->files[$file]);
            }
        }
    }

    public function delete($file) {
        unlink($this->path . $file . '.xml');
        rmdir($this->path . $file);
    }

    private function set($prefix, $key, $value, $xml = null,$wysiwyg=false) {
        if (empty($xml)) {
            $xml = $this->xml;
        }
        $key = $this->getKey($key);
        $xml->{$prefix}->{$key} = '';
        $child = $xml->{$prefix}->{$key};
        if (empty($child)) {
            $child = $xml->addChild($key);
        }
        if($wysiwyg){
            $child->addAttribute('wysiwyg',true);
        }
        $node = dom_import_simplexml($child);
        $owner = $node->ownerDocument;
        $node->appendChild($owner->createCDATASection($value));
    }

    public function key($key) {
        $key = str_replace('_num_', '', $key);
        return str_replace(array_values($this->values), array_keys($this->values), $key);
    }

    private function getKey($key) {
        if (is_numeric((string) $key[0]))
            $key = '_num_' . $key;
        return str_replace(array_keys($this->values), array_values($this->values), $key);
    }

}
