<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Sitemap Controller
 *
 * @property \App\Model\Table\SitemapTable $Sitemap
 */
class SitemapController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->loadModel('Towar');
        $this->loadModel('Kategoria');
        $this->loadModel('Strona');
        $this->Auth->allow(['index', 'xml']);
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index() {
        return $this->redirect(['action'=>'xml','all','_ext'=>'xml']);
        $towary = $this->Towar->find('all', ['conditions' => ['Towar.ukryty' => 0]]);
        $this->set('towary', $towary);
        $strony = $this->Strona->find('all', ['Strona.ukryta' => 0]);
        $this->set('strony', $strony);
    }

    /**
     * View method
     *
     * @param string|null $id Sitemap id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function xml($typ = 'all',$page=1) {
        $this->viewBuilder()->layout('xml');
        switch ($typ) {
            case 'produkty': {
                    $this->paginate = [
                        'limit' => 500,
                        'maxLimit'=>500,
                        'page'=>$page,
                        'conditions' => ['Towar.ukryty' => 0],
                        'order' => ['Towar.id' => 'asc'],
                        'contain'=>['TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1], 'sort' => ['TowarZdjecie.domyslne' => 'desc', 'TowarZdjecie.kolejnosc' => 'desc']]]
                    ];
                    $towary = $this->paginate($this->Towar);
                    $this->set(compact('towary','typ'));
                }break;
            case 'strony' : {
                $this->loadModel('Gatunek');
                    $kategorie = $this->Kategoria->find('all', ['order' => ['Kategoria.parent_id' => 'ASC', 'Kategoria.kolejnosc' => 'DESC'], 'conditions' => ['Kategoria.ukryta' => 0]]);
                    $gatunki = $this->Gatunek->find('all', ['Gatunek.ukryty' => 0]);
                    $strony = $this->Strona->find('all', ['Strona.ukryta' => 0]);
                    $this->set(compact('strony','kategorie','gatunki','typ'));
            }break;
            default : {
                
                    $towaryCount=$this->Towar->find('all',[
                        'conditions' => ['Towar.ukryty' => 0]
                    ])->count();
                    $towarPages= ceil($towaryCount/500);
                    $this->set(compact('towarPages','typ'));
                } break;
        }
    }

}
