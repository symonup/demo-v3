<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * PunktyOdbioru Controller
 *
 * @property \App\Model\Table\PunktyOdbioruTable $PunktyOdbioru
 *
 * @method \App\Model\Entity\PunktyOdbioru[] paginate($object = null, array $settings = [])
 */
class PunktyOdbioruController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
    $cfgArr=[];
$punktyOdbioru = $this->PunktyOdbioru->find('all',$cfgArr);

        $this->set(compact('punktyOdbioru'));
        $this->set('_serialize', ['punktyOdbioru']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $punktyOdbioru = $this->PunktyOdbioru->newEntity();
        if ($this->request->is('post')) {
        $rqData=$this->request->getData();
            $punktyOdbioru = $this->PunktyOdbioru->patchEntity($punktyOdbioru, $rqData,['translations'=>$this->PunktyOdbioru->hasBehavior('Translate')]);
            if ($this->PunktyOdbioru->save($punktyOdbioru)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | punkty odbioru'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | punkty odbioru'))])));
        }
        $wysylka = $this->PunktyOdbioru->Wysylka->find('list',['keyField'=>'id','valueField'=>'nazwa'])->toArray();
        $this->set(compact('punktyOdbioru', 'wysylka'));
        $this->set('_serialize', ['punktyOdbioru']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Punkty Odbioru id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
    
    if($this->PunktyOdbioru->hasBehavior('Translate')){
        $punktyOdbioru = $this->PunktyOdbioru->find('translations', [
            'locales' => $this->lngIdList,
            'conditions' => ['PunktyOdbioru.'.$this->PunktyOdbioru->primaryKey() => $id],
            'contain' => ['Wysylka']
        ])->first();
        }
        else{
        $punktyOdbioru = $this->PunktyOdbioru->find('all', [
            'conditions'=>['PunktyOdbioru.'.$this->PunktyOdbioru->primaryKey()=>$id],
            'contain' => ['Wysylka']
        ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData=$this->request->getData();
            $punktyOdbioru = $this->PunktyOdbioru->patchEntity($punktyOdbioru, $rqData,['translations'=>$this->PunktyOdbioru->hasBehavior('Translate')]);
            if ($this->PunktyOdbioru->save($punktyOdbioru)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | punkty odbioru'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | punkty odbioru'))])));
        }
        
        $_punktyOdbioru = $punktyOdbioru->toArray();
        if ($this->PunktyOdbioru->hasBehavior('Translate') && empty($_punktyOdbioru['_translations'])) {
            $transFields=$this->PunktyOdbioru->associations()->keys();
            $tFields=[];
            foreach ($transFields as $field){
                if(strpos($field, '_translation')!==false){
                    $field= substr($field, (strpos($field, '_')+1));
                    $field=str_replace('_translation','', $field);
                    $tFields[$field]=$_punktyOdbioru[$field]; 
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $punktyOdbioru->set('_translations',$translation);
        }
        
        
        $wysylka = $this->PunktyOdbioru->Wysylka->find('list',['keyField'=>'id','valueField'=>'nazwa'])->toArray();
        $this->set(compact('punktyOdbioru', 'wysylka'));
        $this->set('_serialize', ['punktyOdbioru']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Punkty Odbioru id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $punktyOdbioru = $this->PunktyOdbioru->get($id);
        if ($this->PunktyOdbioru->delete($punktyOdbioru)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.',[$this->Txt->printAdmin(__('Admin | punkty odbioru'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.',[$this->Txt->printAdmin(__('Admin | punkty odbioru'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

/**
     * setField method
     *
     * @param string|null $id Punkty Odbioru id.
     * @param string|null $field Punkty Odbioru field name.
     * @param mixed $value value to set.
     */

public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->PunktyOdbioru->find('all', ['conditions' => ['PunktyOdbioru.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                        $item->{$field} = $value;
                        if ($this->PunktyOdbioru->save($item)) {
                            $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, (!empty($value)?0:1)])), 'action' => $retAction];
                        } else {
                            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                        }
                    
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    /**
* function sort
* 
*/
public function sort($field='kolejnosc'){
        $this->autoRender=false;
        $returnData=[];
        if($this->request->is('post')){
            $rqData=$this->request->getData();
            foreach ($rqData as $data){
                if(!empty($data['id'])){
                    $this->PunktyOdbioru->updateAll($data, ['id'=>$data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    }
