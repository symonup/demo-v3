<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * Administrator Controller
 *
 * @property \App\Model\Table\AdministratorTable $Administrator
 *
 * @method \App\Model\Entity\Administrator[] paginate($object = null, array $settings = [])
 */
class KonfiguracjaController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $cfgArr = [];
        $konfiguracja = $this->Konfiguracja->find('all', $cfgArr);

        $this->set(compact('konfiguracja'));
        $this->set('_serialize', ['konfiguracja']);
        $tmpView = new \Cake\View\View();
        $tips = [
            'konfiguracja' => $tmpView->element($this->tipsPath . 'konfiguracja')
        ];
        $this->set('tips', $tips);
    }

    public function add() {
        $konfiguracja = $this->Konfiguracja->newEntity();
        if ($this->request->is('post')) {
            $konfiguracja = $this->Konfiguracja->patchEntity($konfiguracja, $this->request->getData(), ['translations' => $this->Konfiguracja->hasBehavior('Translate')]);
            if ($this->Konfiguracja->save($konfiguracja)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | konfiguracja'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | konfiguracja'))])));
        }
        $this->set(compact('konfiguracja'));
        $this->set('_serialize', ['konfiguracja']);
    }

    public function edit($id = null) {

        if ($this->Konfiguracja->hasBehavior('Translate')) {
            $konfiguracja = $this->Konfiguracja->find('translations', [
                        'locales' => $this->lngIdList,
                        'conditions' => ['Konfiguracja.' . $this->Konfiguracja->primaryKey() => $id],
                        'contain' => []
                    ])->first();
        } else {
            $konfiguracja = $this->Konfiguracja->find('all', [
                        'conditions' => ['Konfiguracja.' . $this->Konfiguracja->primaryKey() => $id],
                        'contain' => []
                    ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData=$this->request->getData();
            if($konfiguracja->rodzaj=='select_multiple'){
                $rqData['wartosc']= join(',', $rqData['wartosc']);
            }
            $konfiguracja = $this->Konfiguracja->patchEntity($konfiguracja, $rqData, ['translations' => $this->Konfiguracja->hasBehavior('Translate')]);
            if ($this->Konfiguracja->save($konfiguracja)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | konfiguracja'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | konfiguracja'))])));
        }

        $_konfiguracja = $konfiguracja->toArray();
        if ($this->Konfiguracja->hasBehavior('Translate') && empty($_konfiguracja['_translations'])) {
            $transFields = $this->Konfiguracja->associations()->keys();
            $tFields = [];
            foreach ($transFields as $field) {
                if (strpos($field, '_translation') !== false) {
                    $field = substr($field, (strpos($field, '_') + 1));
                    $field = str_replace('_translation', '', $field);
                    $tFields[$field] = $_konfiguracja[$field];
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $konfiguracja->set('_translations', $translation);
        }


        $this->set(compact('konfiguracja'));
        $this->set('_serialize', ['konfiguracja']);
    }

    public function ajaxUpdate() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $dane = $this->request->getData();
            if (!empty($dane['typ']) && !empty($dane['nazwa'])) {
                $cfg = $this->Konfiguracja->find('all', ['conditions' => ['Konfiguracja.typ' => $dane['typ'], 'Konfiguracja.nazwa' => $dane['nazwa']]])->first();
                if (!empty($cfg)) {
                    $cfg->wartosc = $dane['wartosc'];
                    if ($this->Konfiguracja->save($cfg)) {
                        Configure::write($cfg->typ . '.' . $cfg->nazwa, $cfg->wartosc);
                        $returnData = ['status' => 'success','message'=>'Dane zostały zapisane'];
                        if ($cfg->typ == 'dostawa' && ($cfg->nazwa == 'blokuj_soboty' || $cfg->nazwa == 'blokuj_niedziele')) {
                            $returnData['action'] = 'getDostawyKalendarz';
                        }
                        if ($cfg->typ == 'dostawa' && $cfg->nazwa == 'block_days') {
                            $returnData['action'] = 'blockDays';
                            $allBlockDays = $this->getAllBlockDaysArr();
                            $dayList = [];
                            if (!empty($allBlockDays)) {
                                foreach ($allBlockDays as $tmpDate => $tmpItem) {
                                    $dayList[] = $this->Txt->getBlockDateItem($tmpDate, $tmpItem);
                                }
                            }
                            $returnData['items'] = join('', $dayList);
                        }
                    } else {
                        $returnData = ['status' => 'error'];
                    }
                } else {
                    $returnData = ['status' => 'error'];
                }
            } else {
                $returnData = ['status' => 'error'];
            }
        } else {
            $returnData = ['status' => 'error'];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    public function css() {
        $path = $this->filePath['webroot'] . 'css' . DS . 'custom.css';

        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData = $this->request->getData();
            $tresc = $rqData['tresc'];
            $file = fopen($path, 'w+');
            fwrite($file, $tresc);
            fclose($file);
            $this->Flash->success($this->Txt->printAdmin(__('Css został zapisany.')));

            return $this->redirect(['action' => 'css']);
        }

        $rType = 'a+';
        if (file_exists($path)) {
            $rType = 'r+';
        }
        $file = fopen($path, $rType);
        $tresc = '';
        $fileSize = filesize($path);
        if ($fileSize > 0) {
            $tresc = fread($file, $fileSize);
        }
        fclose($file);
        $this->set('tresc', $tresc);
        $tmpView = new \Cake\View\View();
        $tips = [
            'css' => $tmpView->element($this->tipsPath . 'css')
        ];
        $this->set('tips', $tips);
    }
public function kalendarz() {
        $allBlockDays = $this->getAllBlockDaysArr();
        $this->set('allBlockDays', $allBlockDays);
    }
    public function getKalendarzDostaw() {
        if (empty($this->KalendarzDostaw)) {
            $this->loadModel('KalendarzDostaw');
        }
        $cfgBlockDays = Configure::read('dostawa.block_days');
        $allBlockDays = [];
        if (!empty($cfgBlockDays)) {
            $blockDays = explode('|', $cfgBlockDays);
            foreach ($blockDays as $blockDate) {
                $allBlockDays[$blockDate] = date('d', strtotime($blockDate . '-2000')) . ' ' . $this->fullMonths[date('n', strtotime($blockDate . '-2000'))];
            }
        }
        $blokujNiedziele = Configure::read('dostawa.blokuj_niedziele');
        $blokujSoboty = Configure::read('dostawa.blokuj_soboty');
        $conditions = [];
        $start = $end = '';
        if (!empty($this->request->query['start'])) {
            $conditions['KalendarzDostaw.data >='] = $this->request->query['start'];
            $start = $this->request->query['start'];
        }
        if (!empty($this->request->query['end'])) {
            $conditions['KalendarzDostaw.data <='] = $this->request->query['end'];
            $end = $this->request->query['end'];
        }
        $zablokowaneDni = $this->KalendarzDostaw->find('all', ['conditions' => $conditions]);
        $returnBlokada = [];
        $blockedDays = [];
        foreach ($zablokowaneDni as $dzien) {
            if (!empty($allBlockDays) && key_exists(date('d-m', strtotime($dzien->data->format('Y-m-d'))), $allBlockDays)) {
                continue;
            }
            $blockedDays[$dzien->data->format('Y-m-d')] = 1;
            $returnBlokada[] = [
                'id' => $dzien->id,
                'title' => $dzien->przyczyna,
                'start' => $dzien->data->format('Y-m-d'),
                'editable' => false
            ];
        }
        if (!empty($start) && !empty($end)) {
            $dates = $this->Txt->date_range($start, $end);
            foreach ($dates as $date) {
                if (!empty($blockedDays[$date])) {
                    continue;
                }
                if ((!empty($blokujNiedziele) && date('N', strtotime($date)) == 7) || (!empty($blokujSoboty) && date('N', strtotime($date)) == 6)) {
                    continue;
                }
                if (!empty($allBlockDays) && key_exists(date('d-m', strtotime($date)), $allBlockDays)) {
                    continue;
                }
                $returnBlokada[] = [
                    'id' => $date,
                    'title' => $this->Txt->printAdmin(__('Admin | Zablokuj dzien')),
                    'start' => $date,
                    'editable' => false,
                    'color' => '#ffffff',
                    'textColor' => '#999',
                    'className' => 'menageMealsBtn',
                    'borderColor' => '#ddd',
                    'textEscape' => false
                ];
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnBlokada));
        return $this->response;
    }
    public function blockDay($data) {
        $this->viewBuilder()->layout('ajax');
        if (empty($this->KalendarzDostaw)) {
            $this->loadModel('KalendarzDostaw');
        }
        $blockedDay = $this->KalendarzDostaw->find('all', ['conditions' => ['KalendarzDostaw.data' => $data]])->first();
        if (empty($blockedDay)) {
            $blockedDay = $this->KalendarzDostaw->newEntity();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dane = $this->request->getData();
            if (!empty($dane['cyklicznie'])) {
                $ckDate = date('d-m', strtotime($data));
                $cfgDate = Configure::read('dostawa.block_days');
                if (strpos($cfgDate, $ckDate) === false) {
                    $cfgDate .= (!empty($cfgDate) ? '|' : '') . $ckDate;
                    Configure::write('dostawa.block_days', $cfgDate);
                    $this->Konfiguracja->updateAll(['wartosc' => $cfgDate], ['typ' => 'dostawa', 'nazwa' => 'block_days']);
                }
            }
            $blockedDay = $this->KalendarzDostaw->patchEntity($blockedDay, $dane);
            if ($this->KalendarzDostaw->save($blockedDay)) {
                $returnData = ['status' => 'success'];
                $allBlockDays = $this->getAllBlockDaysArr();
                $dayList = [];
                if (!empty($allBlockDays)) {
                    foreach ($allBlockDays as $tmpDate => $tmpItem) {
                        $dayList[] = $this->Txt->getBlockDateItem($tmpDate, $tmpItem);
                    }
                }
                $returnData['items'] = join('', $dayList);
            } else {
                $returnData = ['status' => 'error'];
            }
            $returnData = json_encode($returnData);
            $this->response->type('json');
            $this->response->body($returnData);
            return $this->response;
        }
        $this->set(compact('blockedDay', 'data'));
    }

    public function unBlockDay($data) {
        $this->viewBuilder()->layout('ajax');
        if (!empty($data)) {
            if (empty($this->KalendarzDostaw)) {
                $this->loadModel('KalendarzDostaw');
            }
            if ($this->KalendarzDostaw->deleteAll(['data' => $data])) {
                $returnData = ['status' => 'success'];
            } else {
                $returnData = ['status' => 'error'];
            }
            $returnData = json_encode($returnData);
            $this->response->type('json');
            $this->response->body($returnData);
            return $this->response;
        } else {
            $returnData = json_encode(['status' => 'error']);
            $this->response->type('json');
            $this->response->body($returnData);
            return $this->response;
        }
    }
    private function getAllBlockDaysArr() {
        $cfgBlockDays = Configure::read('dostawa.block_days');
        $allBlockDays = [];
        $tmpArr = [];
        if (!empty($cfgBlockDays)) {
            $blockDays = explode('|', $cfgBlockDays);
            foreach ($blockDays as $blockDate) {
                $tmpArr[date('m', strtotime($blockDate . '-2000'))][date('d', strtotime($blockDate . '-2000'))] = date('d', strtotime($blockDate . '-2000')) . ' ' . $this->fullMonths[date('n', strtotime($blockDate . '-2000'))];
            }
        }
        if (!empty($tmpArr)) {
            ksort($tmpArr);
            foreach ($tmpArr as &$tmpDate) {
                ksort($tmpDate);
            }
            foreach ($tmpArr as $tmpMonth => $tmp_Date) {
                foreach ($tmp_Date as $tmpDay => $date) {
                    $allBlockDays[$tmpDay . '-' . $tmpMonth] = $date;
                }
            }
        }
        return $allBlockDays;
    }



}
