<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * TowarZdjecie Controller
 *
 * @property \App\Model\Table\TowarZdjecieTable $TowarZdjecie
 *
 * @method \App\Model\Entity\TowarZdjecie[] paginate($object = null, array $settings = [])
 */
class TowarZdjecieController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->loadComponent('Upload');
        
        
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index($id = null) {
        
        $tPage = $this->session->read('lastPageTowar');
        if(!empty($tPage)){
        $this->session->write('backToTowar', $tPage);
        $this->session->delete('lastPageTowar');
        }
        
        if (empty($id)) {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | Brak indentyfikatora produktu')));
            if(!empty($this->iframe)){
                die();
            }
            return $this->redirect(['controller' => 'Towar', 'action' => 'index']);
        }
        $towar = $this->TowarZdjecie->Towar->find('all', ['conditions' => ['Towar.id' => $id]])->first();
        if (empty($towar)) {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | Brak produktu o podanym identyfikatorze')));
            if(!empty($this->iframe)){
                die();
            }
            return $this->redirect(['controller' => 'Towar', 'action' => 'index']);
        }
        if ($this->request->is('post')) {
            $errors = [];
            $rqData = $this->request->getData();
            if (!empty($rqData['files'])) {
                $thumbs = \Cake\Core\Configure::read('product_thumbs');
                $path = $this->filePath['towar'];
                $domyslne = $this->TowarZdjecie->find('all', ['conditions' => ['TowarZdjecie.towar_id' => $id, 'TowarZdjecie.domyslne' => 1]])->first();
                if (!empty($domyslne)) {
                    $domyslne = 0;
                } else {
                    $domyslne = 1;
                }
                foreach ($rqData['files'] as $file) {
                    $fileName = $this->Upload->saveFile($path . 'tmp' . DS, $file, null, $thumbs, false);
                    if (!empty($fileName)) {
                        $toSave = [
                            'plik' => $fileName,
                            'towar_id' => $id,
                            'kolejnosc' => 0,
                            'ukryte' => 0,
                            'domyslne' => $domyslne
                        ];
                        $entity = $this->TowarZdjecie->newEntity($toSave);
                        if ($this->TowarZdjecie->save($entity)) {
                            $domyslne = 0;
                            $this->Upload->moveAll($path . 'tmp' . DS, $path . DS . $this->Txt->getKatalog($entity->id) . DS, $fileName, $thumbs);
                        } else {
                            $errors[] = 'save-error: '.$file['name'];
                            if (!empty($fileName)) {
                                if (file_exists($path . 'tmp' . DS . $fileName)) {
                                    $this->Upload->deleteFile($path . 'tmp' . DS . $fileName);
                                }
                                if (!empty($thumbs)) {
                                    foreach ($thumbs as $thName => $thOption) {
                                        if (file_exists($path . 'tmp' . DS . $thName . '_' . $fileName)) {
                                            $this->Upload->deleteFile($path . 'tmp' . DS . $thName . '_' . $fileName);
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        $errors[] = 'upload-error: '.$file['name'];
                    }
                }
            }
            if (!empty($errors)) {
                $this->Flash->error($this->Txt->printAdmin(__('Admin | Błąd zapisu plików: {0}', [join(', ', $errors)])));
            } else {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | Dane zostały zapisane')));
            }
            return $this->redirect(['action' => 'index', $id,'iframe'=>(!empty($this->iframe)?$this->iframe:null)]);
        }
        $cfgArr = [
            'conditions' => ['TowarZdjecie.towar_id' => $id],
            'order'=>['TowarZdjecie.kolejnosc'=>'desc']
        ];
        $towarZdjecie = $this->TowarZdjecie->find('all', $cfgArr);
$this->loadModel('TowarAtrybut');
        $wersje=$this->TowarAtrybut->find('all',['conditions'=>['TowarAtrybut.towar_id'=>$id,'TowarAtrybut.wariant'=>1],'contain'=>['Atrybut'=>['AtrybutPodrzedne']]]);
        $warianty=[];
        foreach ($wersje as $wersja){
            $warianty[$wersja->id]=$wersja->atrybut->nazwa.(!empty($wersja->atrybut->atrybut_podrzedne)?' ('.$wersja->atrybut->atrybut_podrzedne[0]->nazwa.')':'');
        }
        $this->set(compact('towarZdjecie', 'towar','warianty'));
        $this->set('_serialize', ['towarZdjecie']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Towar Zdjecie id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $towarZdjecie = $this->TowarZdjecie->get($id);
        $thumbs = \Cake\Core\Configure::read('product_thumbs');
        $path = $this->filePath['towar'];
        $fileName = $towarZdjecie->plik;
        $katalog = $this->Txt->getKatalog($towarZdjecie->id);
        $towarId=$towarZdjecie->towar_id;
        $domyslne=$towarZdjecie->domyslne;
        if ($this->TowarZdjecie->delete($towarZdjecie)) {
            if (!empty($fileName)) {
                if (file_exists($path . DS . $katalog . DS . $fileName)) {
                    $this->Upload->deleteFile($path . DS . $katalog . DS . $fileName);
                }
                if (!empty($thumbs)) {
                    foreach ($thumbs as $thName => $thOption) {
                        if (file_exists($path . DS . $katalog . DS . $thName . '_' . $fileName)) {
                            $this->Upload->deleteFile($path . DS . $katalog . DS . $thName . '_' . $fileName);
                        }
                    }
                }
            }
            if(!empty($domyslne)){
                $newDomyslne=$this->TowarZdjecie->find('all',['conditions'=>['TowarZdjecie.towar_id'=>$towarId],'order'=>['TowarZdjecie.kolejnosc'=>'desc']])->first();
                if(!empty($newDomyslne)){
                    $newDomyslne->domyslne=1;
                    $this->TowarZdjecie->save($newDomyslne);
                }
            }
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.', [$this->Txt->printAdmin(__('Admin | towar zdjecie'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.', [$this->Txt->printAdmin(__('Admin | towar zdjecie'))])));
        }

        return $this->redirect(['action' => 'index',$towarId,'iframe'=>(!empty($this->iframe)?$this->iframe:null)]);
    }

    public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            if (!empty($rqData['id'])) {
                $entity = $this->TowarZdjecie->find('all', ['conditions' => ['TowarZdjecie.id' => $rqData['id']]])->first();
                if (!empty($entity)) {
                    $entity = $this->TowarZdjecie->patchEntity($entity, $rqData);
                    if ($this->TowarZdjecie->save($entity)) {
                        $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zapisane'))];
                    } else {
                        $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych'))];
                    }
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowy identyfikator'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak identyfikatora'))];
            }
        } else if (!empty($id) && !empty($field)) {
            $item = $this->TowarZdjecie->find('all', ['conditions' => ['TowarZdjecie.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                    if ($field == 'domyslne' && empty($value) && !empty($item->domyslne)) {
                        $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Przynajmniej jedno zdjęcie musi być domyślne.')), 'errors' => $item->errors()];
                    } else {
                        if ($field == 'domyslne' && !empty($value)) {
                            if (empty($item->domyslne)) {
                                $this->TowarZdjecie->updateAll(['domyslne' => 0], ['towar_id' => $item->towar_id]);
                                $retAction = 'reload';
                            }
                        }
                        $item->{$field} = $value;
                        if ($this->TowarZdjecie->save($item)) {
                            $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, !(bool) $value])), 'action' => $retAction];
                        } else {
                            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                        }
                    }
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    public function sort($field='kolejnosc'){
        $this->autoRender=false;
        $returnData=[];
        if($this->request->is('post')){
            $rqData=$this->request->getData();
            foreach ($rqData as $data){
                if(!empty($data['id'])){
                    $this->TowarZdjecie->updateAll($data, ['id'=>$data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
}
