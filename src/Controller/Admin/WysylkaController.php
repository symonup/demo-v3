<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Wysylka Controller
 *
 * @property \App\Model\Table\WysylkaTable $Wysylka
 *
 * @method \App\Model\Entity\Wysylka[] paginate($object = null, array $settings = [])
 */
class WysylkaController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $cfgArr = [];
        $cfgArr = [
            'contain' => ['Vat']
        ];
        $wysylka = $this->Wysylka->find('all', $cfgArr);

        $this->set(compact('wysylka'));
        $this->set('_serialize', ['wysylka']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $wysylka = $this->Wysylka->newEntity();
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            $wysylka = $this->Wysylka->patchEntity($wysylka, $rqData, ['translations' => $this->Wysylka->hasBehavior('Translate')]);
            if ($this->Wysylka->save($wysylka)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | wysylka'))])));

                return $this->redirect(['action' => 'index']);
            }
            var_dump($wysylka->errors());
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | wysylka'))])));
        }
        $vat = $this->Wysylka->Vat->find('list', ['keyField' => 'id', 'valueField' => 'nazwa'])->toArray();
        $rodzajPlatnosci = $this->Wysylka->RodzajPlatnosci->find('list', ['keyField' => 'id', 'valueField' => 'nazwa'])->where(['RodzajPlatnosci.id IN' => [1, 2, 5, 7]])->toArray();
        $punktyOdbioru = $this->Wysylka->PunktyOdbioru->find('list', ['keyField' => 'id', 'valueField' => 'nazwa'])->toArray();
        $this->set(compact('wysylka', 'vat', 'rodzajPlatnosci', 'punktyOdbioru'));
        $this->set('_serialize', ['wysylka']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Wysylka id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {

        if ($this->Wysylka->hasBehavior('Translate')) {
            $wysylka = $this->Wysylka->find('translations', [
                        'locales' => $this->lngIdList,
                        'conditions' => ['Wysylka.' . $this->Wysylka->primaryKey() => $id],
                        'contain' => ['PunktyOdbioru', 'RodzajPlatnosci', 'WysylkaKoszt' => ['sort' => ['WysylkaKoszt.od' => 'asc']]]
                    ])->first();
        } else {
            $wysylka = $this->Wysylka->find('all', [
                        'conditions' => ['Wysylka.' . $this->Wysylka->primaryKey() => $id],
                        'contain' => ['PunktyOdbioru', 'RodzajPlatnosci', 'WysylkaKoszt' => ['sort' => ['WysylkaKoszt.od' => 'asc']]]
                    ])->first();
        }
        $oldWysylkaKoszt = [];
        if (!empty($wysylka->wysylka_koszt)) {
            foreach ($wysylka->wysylka_koszt as $wsK) {
                $oldWysylkaKoszt[$wsK->id] = $wsK->id;
            }
        }
        if ($this->request->is(['patch', 'post', 'put'])) {

            $vatS = $this->Wysylka->Vat->find('list', ['keyField' => 'id', 'valueField' => 'stawka'])->toArray();
            $rqData = $this->request->getData();
            if (!empty($rqData['wysylka_koszt'])) {
                foreach ($rqData['wysylka_koszt'] as &$rWsK) {
                    if (!empty($rWsK['id']) && key_exists($rWsK['id'], $oldWysylkaKoszt)) {
                        unset($oldWysylkaKoszt[$rWsK['id']]);
                    }
                    if ($vatS[$rqData['vat_id']] > 0) {
                        $rWsK['koszt_netto'] = $rWsK['koszt_brutto'] / (($rqData['vat_id'] / 100) + 1);
                    } else {
                        $rWsK['koszt_netto'] = $rWsK['koszt_brutto'];
                    }
                }
            }
            $wysylka = $this->Wysylka->patchEntity($wysylka, $rqData, ['translations' => $this->Wysylka->hasBehavior('Translate')]);
            if ($this->Wysylka->save($wysylka)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | wysylka'))])));
                if (!empty($oldWysylkaKoszt)) {
                    $this->Wysylka->WysylkaKoszt->deleteAll(['id IN' => $oldWysylkaKoszt]);
                }
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | wysylka'))])));
        }

        $_wysylka = $wysylka->toArray();
        if ($this->Wysylka->hasBehavior('Translate') && empty($_wysylka['_translations'])) {
            $transFields = $this->Wysylka->associations()->keys();
            $tFields = [];
            foreach ($transFields as $field) {
                if (strpos($field, '_translation') !== false) {
                    $field = substr($field, (strpos($field, '_') + 1));
                    $field = str_replace('_translation', '', $field);
                    $tFields[$field] = $_wysylka[$field];
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $wysylka->set('_translations', $translation);
        }


        $vat = $this->Wysylka->Vat->find('list', ['keyField' => 'id', 'valueField' => 'nazwa'])->toArray();
        $rodzajPlatnosci = $this->Wysylka->RodzajPlatnosci->find('list', ['keyField' => 'id', 'valueField' => 'nazwa'])->where(['RodzajPlatnosci.id IN' => [1, 2, 5, 7]])->toArray();

        $punktyOdbioru = $this->Wysylka->PunktyOdbioru->find('list', ['keyField' => 'id', 'valueField' => 'nazwa'])->toArray();
        $this->set(compact('wysylka', 'vat', 'rodzajPlatnosci', 'punktyOdbioru'));
        $this->set('_serialize', ['wysylka']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Wysylka id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $wysylka = $this->Wysylka->get($id);
        if ($this->Wysylka->delete($wysylka)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.', [$this->Txt->printAdmin(__('Admin | wysylka'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.', [$this->Txt->printAdmin(__('Admin | wysylka'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * setField method
     *
     * @param string|null $id Wysylka id.
     * @param string|null $field Wysylka field name.
     * @param mixed $value value to set.
     */
    public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->Wysylka->find('all', ['conditions' => ['Wysylka.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                    $item->{$field} = $value;
                    if ($this->Wysylka->save($item)) {
                        $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, !(bool) $value])), 'action' => $retAction];
                    } else {
                        $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                    }
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    /**
     * function sort
     * 
     */
    public function sort($field = 'kolejnosc') {
        $this->autoRender = false;
        $returnData = [];
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            foreach ($rqData as $data) {
                if (!empty($data['id'])) {
                    $this->Wysylka->updateAll($data, ['id' => $data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    public function kraje($wysylkaId = null, $kraj = null) {
        $this->loadModel('WysylkaKrajeKoszty');
        if ($this->request->is('post')) {
            $this->autoRender = false;
            $returnArr = [];
            $rqData = $this->request->getData('wysylka');
            if (!empty($rqData) && key_exists($wysylkaId, $rqData) && $wysylkaId==$rqData[$wysylkaId]['wysylka_id']) {
                $saveData = $rqData[$wysylkaId];
                $wysylkaKoszt = $this->WysylkaKrajeKoszty->find()->where(['wysylka_id' => $wysylkaId, 'kraj' => $kraj])->first();
                if (empty($wysylkaKoszt)) {
                    $wysylkaKoszt = $this->WysylkaKrajeKoszty->newEntity();
                }
                $wysylkaKoszt = $this->WysylkaKrajeKoszty->patchEntity($wysylkaKoszt, $saveData);
                if ($this->WysylkaKrajeKoszty->save($wysylkaKoszt)) {
                    $returnArr = ['status' => 'success'];
                } else {
                    $returnArr = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie udało się zapisać kosztu wysyłki dla kraju {0}', [$kraj]))];
                }
            } else {
                $returnArr = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Dane są nieprawidłowe'))];
            }
            if ($this->request->is('ajax')) {
                $this->response->type('ajax');
                $this->response->body(json_encode($returnArr));
                return $this->response;
            } else {
                if ($returnArr['status'] == 'error') {
                    $this->Flash->error($returnArr['message']);
                }
                return $this->redirect(['action'=>'kraje']);
            }
        }
        $kraje = \Cake\Core\Configure::read('kraje');
        $koszty = $this->WysylkaKrajeKoszty->find('list', ['groupField' => 'wysylka_id', 'keyField' => 'kraj', 'valueField' => 'koszt'])->toArray();
        $wysylki = $this->Wysylka->find('list', ['keyField' => 'id', 'valueField' => 'nazwa'])->where(['elektroniczna' => 0, 'paczkomat' => 0, 'odbior_osobisty' => 0])->toArray();
        $this->set(compact('kraje', 'wysylki', 'koszty'));
    }

public function pocztaForm($id) {
        $this->viewBuilder()->autoLayout(false);
        $daneForm = $this->request->data['dane'];
        $typ = $this->request->data['typ'];
        $this->set('typ', $typ);
        $this->set('zamId',$id);
        $this->set('daneForm', $daneForm);
    }
}
