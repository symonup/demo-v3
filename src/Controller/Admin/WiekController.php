<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Wiek Controller
 *
 * @property \App\Model\Table\WiekTable $Wiek
 *
 * @method \App\Model\Entity\Wiek[] paginate($object = null, array $settings = [])
 */
class WiekController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
    $cfgArr=[];
$wiek = $this->Wiek->find('all',$cfgArr);

        $this->set(compact('wiek'));
        $this->set('_serialize', ['wiek']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $wiek = $this->Wiek->newEntity();
        if ($this->request->is('post')) {
        $rqData=$this->request->getData();
            $wiek = $this->Wiek->patchEntity($wiek, $rqData,['translations'=>$this->Wiek->hasBehavior('Translate')]);
            if ($this->Wiek->save($wiek)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | wiek'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | wiek'))])));
        }
        $this->set(compact('wiek'));
        $this->set('_serialize', ['wiek']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Wiek id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
    
    if($this->Wiek->hasBehavior('Translate')){
        $wiek = $this->Wiek->find('translations', [
            'locales' => $this->lngIdList,
            'conditions' => ['Wiek.'.$this->Wiek->primaryKey() => $id],
            'contain' => []
        ])->first();
        }
        else{
        $wiek = $this->Wiek->find('all', [
            'conditions'=>['Wiek.'.$this->Wiek->primaryKey()=>$id],
            'contain' => []
        ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData=$this->request->getData();
            $wiek = $this->Wiek->patchEntity($wiek, $rqData,['translations'=>$this->Wiek->hasBehavior('Translate')]);
            if ($this->Wiek->save($wiek)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | wiek'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | wiek'))])));
        }
        
        $_wiek = $wiek->toArray();
        if ($this->Wiek->hasBehavior('Translate') && empty($_wiek['_translations'])) {
            $transFields=$this->Wiek->associations()->keys();
            $tFields=[];
            foreach ($transFields as $field){
                if(strpos($field, '_translation')!==false){
                    $field= substr($field, (strpos($field, '_')+1));
                    $field=str_replace('_translation','', $field);
                    $tFields[$field]=$_wiek[$field]; 
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $wiek->set('_translations',$translation);
        }
        
        
        $this->set(compact('wiek'));
        $this->set('_serialize', ['wiek']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Wiek id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $wiek = $this->Wiek->get($id);
        if ($this->Wiek->delete($wiek)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.',[$this->Txt->printAdmin(__('Admin | wiek'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.',[$this->Txt->printAdmin(__('Admin | wiek'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

/**
     * setField method
     *
     * @param string|null $id Wiek id.
     * @param string|null $field Wiek field name.
     * @param mixed $value value to set.
     */

public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->Wiek->find('all', ['conditions' => ['Wiek.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                        $item->{$field} = $value;
                        if ($this->Wiek->save($item)) {
                            $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, (!empty($value)?0:1)])), 'action' => $retAction];
                        } else {
                            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                        }
                    
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    /**
* function sort
* 
*/
public function sort($field='kolejnosc'){
        $this->autoRender=false;
        $returnData=[];
        if($this->request->is('post')){
            $rqData=$this->request->getData();
            foreach ($rqData as $data){
                if(!empty($data['id'])){
                    $this->Wiek->updateAll($data, ['id'=>$data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    }
