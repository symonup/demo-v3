<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * KodyRabatowe Controller
 *
 * @property \App\Model\Table\KodyRabatoweTable $KodyRabatowe
 *
 * @method \App\Model\Entity\KodyRabatowe[] paginate($object = null, array $settings = [])
 */
class KodyRabatoweController extends AppController
{
    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $rabatRodzaj = ['ogolny' => $this->Txt->printAdmin(__('Admin | Ogólny')), 'kategoria' => $this->Txt->printAdmin(__('Admin | Wybrane kategorie')), 'producent' => $this->Txt->printAdmin(__('Admin | Wybrani producenci'))];
        $this->set('rabatRodzaj',$rabatRodzaj);
    }
            
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
    $cfgArr=[];
        $cfgArr = [
            'contain' => ['Zamowienie']
        ];
$kodyRabatowe = $this->KodyRabatowe->find('all',$cfgArr);

        $this->set(compact('kodyRabatowe'));
        $this->set('_serialize', ['kodyRabatowe']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $kodyRabatowe = $this->KodyRabatowe->newEntity();
        if ($this->request->is('post')) {
        $rqData=$this->request->getData();
        $rqData['data_dodania']=date('Y-m-d H:i:s');
            $kodyRabatowe = $this->KodyRabatowe->patchEntity($kodyRabatowe, $rqData,['translations'=>$this->KodyRabatowe->hasBehavior('Translate')]);
            if ($this->KodyRabatowe->save($kodyRabatowe)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | kody rabatowe'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | kody rabatowe'))])));
        }
        $this->loadModel('Producent');
        $this->loadModel('Kategoria');
        $producenci = $this->Producent->find('list',['keyField'=>'id','valueField'=>'nazwa'])->order(['nazwa'=>'asc'])->toArray();
        $katArr = $this->Kategoria->getCategoryTreeAll();
        $this->set(compact('kodyRabatowe','katArr','producenci'));
        $this->set('_serialize', ['kodyRabatowe']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Kody Rabatowe id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
    
    if($this->KodyRabatowe->hasBehavior('Translate')){
        $kodyRabatowe = $this->KodyRabatowe->find('translations', [
            'locales' => $this->lngIdList,
            'conditions' => ['KodyRabatowe.'.$this->KodyRabatowe->primaryKey() => $id],
            'contain' => ['Kategoria','Producent']
        ])->first();
        }
        else{
        $kodyRabatowe = $this->KodyRabatowe->find('all', [
            'conditions'=>['KodyRabatowe.'.$this->KodyRabatowe->primaryKey()=>$id],
            'contain' => ['Kategoria','Producent']
        ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData=$this->request->getData();
            $kodyRabatowe = $this->KodyRabatowe->patchEntity($kodyRabatowe, $rqData,['translations'=>$this->KodyRabatowe->hasBehavior('Translate')]);
            if ($this->KodyRabatowe->save($kodyRabatowe)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | kody rabatowe'))])));

                return $this->redirect(['action' => 'index']);
            }
            var_dump($kodyRabatowe->errors());
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | kody rabatowe'))])));
        }
        
        $_kodyRabatowe = $kodyRabatowe->toArray();
        if ($this->KodyRabatowe->hasBehavior('Translate') && empty($_kodyRabatowe['_translations'])) {
            $transFields=$this->KodyRabatowe->associations()->keys();
            $tFields=[];
            foreach ($transFields as $field){
                if(strpos($field, '_translation')!==false){
                    $field= substr($field, (strpos($field, '_')+1));
                    $field=str_replace('_translation','', $field);
                    $tFields[$field]=$_kodyRabatowe[$field]; 
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $kodyRabatowe->set('_translations',$translation);
        }
        $this->loadModel('Producent');
        $this->loadModel('Kategoria');
        
        $producenci = $this->Producent->find('list',['keyField'=>'id','valueField'=>'nazwa'])->order(['nazwa'=>'asc'])->toArray();
        $kategoriaSelected = $this->KodyRabatowe->KodyRabatoweKategoria->find('list', ['keyField' => 'kategoria_id', 'valueField' => 'kategoria_id', 'conditions' => ['kody_rabatowe_id' => $id]])->toArray();
        $katArr = $this->Kategoria->getCategoryTreeAll();
        $this->set(compact('kodyRabatowe','katArr','kategoriaSelected','producenci'));
        $this->set('_serialize', ['kodyRabatowe']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Kody Rabatowe id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $kodyRabatowe = $this->KodyRabatowe->get($id);
        if ($this->KodyRabatowe->delete($kodyRabatowe)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.',[$this->Txt->printAdmin(__('Admin | kody rabatowe'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.',[$this->Txt->printAdmin(__('Admin | kody rabatowe'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

/**
     * setField method
     *
     * @param string|null $id Kody Rabatowe id.
     * @param string|null $field Kody Rabatowe field name.
     * @param mixed $value value to set.
     */

public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->KodyRabatowe->find('all', ['conditions' => ['KodyRabatowe.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                        $item->{$field} = $value;
                        if ($this->KodyRabatowe->save($item)) {
                            $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, (!empty($value)?0:1)])), 'action' => $retAction];
                        } else {
                            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                        }
                    
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    /**
* function sort
* 
*/
public function sort($field='kolejnosc'){
        $this->autoRender=false;
        $returnData=[];
        if($this->request->is('post')){
            $rqData=$this->request->getData();
            foreach ($rqData as $data){
                if(!empty($data['id'])){
                    $this->KodyRabatowe->updateAll($data, ['id'=>$data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    }
