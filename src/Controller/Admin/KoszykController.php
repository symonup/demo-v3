<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use PHPExcel;

/**
 * Koszyk Controller
 *
 * @property \App\Model\Table\KoszykTable $Koszyk
 */
class KoszykController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $filter=[];
        if ($this->request->is('post')) {
            if (isset($this->request->data['wyczysc_filtr'])) {
                $this->session->delete('Koszyk.filter');
                $this->session->delete('Koszyk.filterRequest');
            } else {
                $filtr = $this->request->data;
                $this->session->write('Koszyk.filterRequest', $this->request->data);
                if(!empty($filtr['domena'])){
                    $filter['Koszyk.domena']=$filtr['domena'];
                }
                if(!empty($filtr['data_od'])){
                    $filter['Koszyk.data >=']=$filtr['data_od'].' 00:00:00';
                }
                if(!empty($filtr['data_do'])){
                    $filter['Koszyk.data <=']=$filtr['data_do'].' 23:59:59';
                }
                if (!empty($filtr['status'])) {
                    if($filtr['status']=='towary_usuniete'){
                        $conditions=['usuniety'=>1];
                        if(!empty($filter['data >='])){
                            $conditions['data_dodania >=']=$filter['data >='];
                        }
                        if(!empty($filter['data <='])){
                            $conditions['data_dodania <=']=$filter['data <='];
                        }
                       $kIds = $this->Koszyk->KoszykTowar->find('list',['keyField'=>'koszyk_id','valueField'=>'koszyk_id','group'=>'koszyk_id','conditions'=>$conditions])->toArray();
                       if(empty($kIds)){
                           $kIds=[null];
                       }
                       $filter['Koszyk.id IN']=$kIds;
                    }
                    else if($filtr['status']=='bez_usunietych'){
                        $conditions=['usuniety'=>1];
                        if(!empty($filter['data >='])){
                            $conditions['data_dodania >=']=$filter['data >='];
                        }
                        if(!empty($filter['data <='])){
                            $conditions['data_dodania <=']=$filter['data <='];
                        }
                       $kIds = $this->Koszyk->KoszykTowar->find('list',['keyField'=>'koszyk_id','valueField'=>'koszyk_id','group'=>'koszyk_id','conditions'=>$conditions])->toArray();
                       if(empty($kIds)){
                           $kIds=[null];
                       }
                       $filter['Koszyk.id NOT IN']=$kIds;
                    } else if($filtr['status']=='zrealizowane'){
                        $filter['Koszyk.zamowienie_id IS NOT']=NULL;
                    } else {
                    $filter['Koszyk.zamowienie_id IS'] = NULL;
                    }
                }
            }
            
                $this->session->write('Koszyk.filter', $filter);
        }
        $filter = $this->session->read('Koszyk.filter');
        if (empty($filter))
            $filter = [];
        $this->paginate = [
            'conditions'=>$filter,
            'contain'=>['KoszykTowar'=>['Towar'],'KoszykTowarUsuniete'],
            'limit'=>100,
            'order'=>['Koszyk.data'=>'desc']
        ];
        $koszyk = $this->paginate($this->Koszyk);

        $this->set(compact('koszyk'));
        $this->set('filter', $this->session->read('Koszyk.filterRequest'));
        $this->set('_serialize', ['koszyk']);
        
    }
    public function raport(){
        $this->autoRender=false;
        $filter = $this->session->read('Koszyk.filter');
        if (empty($filter))
            $filter = [];
        $lista=$this->Koszyk->find('all',[
            'conditions'=>$filter,
            'contain'=>['KoszykTowar'=>['Towar']],
            'limit'=>100,
            'order'=>['Koszyk.data'=>'desc']
        ]);
        $cols = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
        $excel = new \PHPExcel();
        
                $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'Id');
                $excel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'Domena');
                $excel->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'Nr zam.');
                $excel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'Data stworzenia');
                $excel->getActiveSheet()->setCellValueByColumnAndRow(4, 1, 'Ostatnia aktualizacja');
                $excel->getActiveSheet()->setCellValueByColumnAndRow(5, 1, 'Towar');
                $excel->getActiveSheet()->setCellValueByColumnAndRow(6, 1, 'Usunięty');
                $excel->getActiveSheet()->setCellValueByColumnAndRow(7, 1, 'Data dodania');
                $excel->getActiveSheet()->setCellValueByColumnAndRow(8, 1, 'Data Usunięcia');
                $excel->getActiveSheet()->setCellValueByColumnAndRow(9, 1, 'Ilość');
                $excel->getActiveSheet()->setCellValueByColumnAndRow(10, 1, 'Cena');
                $excel->getActiveSheet()->setCellValueByColumnAndRow(11, 1, 'Szczegóły');
        if($lista->count()>0){
            $rowStart=2;
            foreach ($lista as $item){
                $countKoszyk=count($item->koszyk_towar);
                $merrageCell=$countKoszyk+$rowStart-1;
                $excel->getActiveSheet()->mergeCellsByColumnAndRow(0, $rowStart, 0, $merrageCell);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, $rowStart, $item->id);
        $excel->getActiveSheet()->getStyleByColumnAndRow(0, $rowStart)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $excel->getActiveSheet()->getStyleByColumnAndRow(0, $rowStart)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_TOP);
        
                $excel->getActiveSheet()->mergeCellsByColumnAndRow(1, $rowStart, 1, $merrageCell);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(1, $rowStart, $item->domena);
        $excel->getActiveSheet()->getStyleByColumnAndRow(1, $rowStart)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $excel->getActiveSheet()->getStyleByColumnAndRow(1, $rowStart)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_TOP);
        
                $excel->getActiveSheet()->mergeCellsByColumnAndRow(2, $rowStart, 2, $merrageCell);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(2, $rowStart, $item->zamowienie_id);
        $excel->getActiveSheet()->getStyleByColumnAndRow(2, $rowStart)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $excel->getActiveSheet()->getStyleByColumnAndRow(2, $rowStart)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_TOP);
        
                $excel->getActiveSheet()->mergeCellsByColumnAndRow(3, $rowStart, 3, $merrageCell);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(3, $rowStart, (!empty($item->data)?$item->data->format('Y-m-d H:i:s'):''));
        $excel->getActiveSheet()->getStyleByColumnAndRow(3, $rowStart)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $excel->getActiveSheet()->getStyleByColumnAndRow(3, $rowStart)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_TOP);
        
                $excel->getActiveSheet()->mergeCellsByColumnAndRow(4, $rowStart, 4, $merrageCell);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(4, $rowStart, (!empty($item->last_update)?$item->last_update->format('Y-m-d H:i:s'):''));
        $excel->getActiveSheet()->getStyleByColumnAndRow(4, $rowStart)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $excel->getActiveSheet()->getStyleByColumnAndRow(4, $rowStart)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_TOP);
        $startCol=5;
        if(!empty($item->koszyk_towar)){
            $startRow = $rowStart;
        $col = $startCol;
        $row = $startRow;
        $maxCol = 0;
            foreach ($item->koszyk_towar as $cartItem){
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, (!empty($cartItem->towar)?$cartItem->towar->nazwa:$cartItem->towar_id));
        $excel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_TOP);
//            $excel->getActiveSheet()->getStyleByColumnAndRow(($col - 1), $row)->getAlignment()->setWrapText(true);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, (!empty($cartItem->usuniety)?'Tak':'Nie'));
        $excel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_TOP);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, (!empty($cartItem->data_dodania)?$cartItem->data_dodania->format('Y-m-d H:i:s'):''));
        $excel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_TOP);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, (!empty($cartItem->data_usuniecia)?$cartItem->data_usuniecia->format('Y-m-d H:i:s'):''));
        $excel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_TOP);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $cartItem->ilosc);
        $excel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_TOP);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $cartItem->cena);
        $excel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_TOP);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $cartItem->notatki);
$excel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_TOP);
        
            //-------------------------------//
            $col = $startCol;
            $row++;
            }
        }
        $rowStart=$row;
        
            }
        }
        
        $fileName = 'raport_koszykow_' . date('YmdHis') . '.xls';
        header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=$fileName");
header("Pragma: no-cache");
header("Expires: 0");
        $writer = new \PHPExcel_Writer_Excel5($excel);
        $writer->save('php://output');
    }
}
