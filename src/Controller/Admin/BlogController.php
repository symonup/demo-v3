<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Blog Controller
 *
 * @property \App\Model\Table\BlogTable $Blog
 *
 * @method \App\Model\Entity\Blog[] paginate($object = null, array $settings = [])
 */
class BlogController extends AppController
{

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->loadComponent('Upload');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
    $cfgArr=[];
$blog = $this->Blog->find('all',$cfgArr);

        $this->set(compact('blog'));
        $this->set('_serialize', ['blog']);
    }

    /**
     * View method
     *
     * @param string|null $id Blog id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $blog = $this->Blog->get($id, [
            'contain' => []
        ]);

        $this->set('blog', $blog);
        $this->set('_serialize', ['blog']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $blog = $this->Blog->newEntity();
        if ($this->request->is('post')) {
        $rqData=$this->request->getData();
        $rqData['data']=date('Y-m-d H:i:s');
        
         $path = $this->filePath['blog'];
            if (!empty($rqData['zdjecie_crop'])) {
                $imageType='jpeg';
                $mime = $rqData['mime'];
                if($mime=='image/png'){
                    $imageType='png';
                }
                if (preg_match('/^data:image\/(\w+);base64,/', $rqData['zdjecie_crop'], $type)) {
                    $rqData['zdjecie_crop'] = substr($rqData['zdjecie_crop'], strpos($rqData['zdjecie_crop'], ',') + 1);
                    $type = strtolower($type[1]); // jpg, png, gif

                    if (!in_array($type, ['jpg', 'jpeg', 'gif', 'png'])) {
                        throw new \Exception('invalid image type');
                    }

                    $rqData['zdjecie_crop'] = base64_decode($rqData['zdjecie_crop']);

                    if ($rqData['zdjecie_crop'] === false) {
                        throw new \Exception('base64_decode failed');
                    }
                } else {
                    throw new \Exception('did not match data URI with image data');
                }
                $fileName = $this->Upload->imageStream($rqData['zdjecie_crop'], $path, null,[],false,$imageType);
                if (!empty($fileName)) {
                    $rqData['zdjecie'] = $fileName;
                } else {
                    $rqData['zdjecie'] = null;
                }
            } else {
               
            if (!empty($rqData['zdjecie']['name'])) {
                $fileName = $this->Upload->saveFile(
                        $path, $rqData['zdjecie']);
                if (!empty($fileName)) {
                    $rqData['zdjecie'] = $fileName;
                } else {
                    $rqData['zdjecie'] = null;
                }
            } else
                $rqData['zdjecie'] = null;
            }
        
            $blog = $this->Blog->patchEntity($blog, $rqData,['translations'=>$this->Blog->hasBehavior('Translate')]);
            if ($this->Blog->save($blog)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | blog'))])));

                return $this->redirect(['action' => 'index']);
            }else{
                if (!empty($fileName)) {
                    $this->Upload->deleteFile($path . $fileName);
                }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | blog'))])));
            }
        }
        $this->set(compact('blog'));
        $this->set('_serialize', ['blog']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Blog id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
    
    if($this->Blog->hasBehavior('Translate')){
        $blog = $this->Blog->find('translations', [
            'locales' => $this->lngIdList,
            'conditions' => ['Blog.'.$this->Blog->getPrimaryKey() => $id],
            'contain' => []
        ])->first();
        }
        else{
        $blog = $this->Blog->find('all', [
            'conditions'=>['Blog.'.$this->Blog->getPrimaryKey()=>$id],
            'contain' => []
        ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData=$this->request->getData();
            $oldFile = null;
                $path = $this->filePath['blog'];
            
                if (!empty($rqData['zdjecie_crop'])) {
                $imageType='jpeg';
                $mime = $rqData['mime'];
                if($mime=='image/png'){
                    $imageType='png';
                }
                if (preg_match('/^data:image\/(\w+);base64,/', $rqData['zdjecie_crop'], $type)) {
                    $rqData['zdjecie_crop'] = substr($rqData['zdjecie_crop'], strpos($rqData['zdjecie_crop'], ',') + 1);
                    $type = strtolower($type[1]); // jpg, png, gif

                    if (!in_array($type, ['jpg', 'jpeg', 'gif', 'png'])) {
                        throw new \Exception('invalid image type');
                    }

                    $rqData['zdjecie_crop'] = base64_decode($rqData['zdjecie_crop']);

                    if ($rqData['zdjecie_crop'] === false) {
                        throw new \Exception('base64_decode failed');
                    }
                } else {
                    throw new \Exception('did not match data URI with image data');
                }
                $fileName = $this->Upload->imageStream($rqData['zdjecie_crop'], $path, null,[],false,$imageType);
                if (!empty($fileName)) {
                    $rqData['zdjecie'] = $fileName;
                    $oldFile = $blog->zdjecie;
                } else {
                    $rqData['zdjecie'] = $blog->zdjecie;
                }
            }else{
                
            if (empty($rqData['zdjecie']['name'])) {
                $rqData['zdjecie'] = $blog->zdjecie;
            } else {
                $fileName = $this->Upload->saveFile(
                        $path, $rqData['zdjecie']);
                if (!empty($fileName)) {
                    $rqData['zdjecie'] = $fileName;
                    $oldFile = $blog->zdjecie;
                }
            }
            }
            $blog = $this->Blog->patchEntity($blog, $rqData,['translations'=>$this->Blog->hasBehavior('Translate')]);
            if ($this->Blog->save($blog)) {
                if (!empty($oldFile))
                    $this->Upload->deleteFile($path . $oldFile);
                
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | blog'))])));

                return $this->redirect(['action' => 'index']);
            }else{
                if (!empty($fileName)) {
                    $this->Upload->deleteFile($path . $fileName);
                }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | blog'))])));
            }
            
            }
        
        $_blog = $blog->toArray();
        if ($this->Blog->hasBehavior('Translate') && empty($_blog['_translations'])) {
            $transFields=$this->Blog->associations()->keys();
            $tFields=[];
            foreach ($transFields as $field){
                if(strpos($field, '_translation')!==false){
                    $field= substr($field, (strpos($field, '_')+1));
                    $field=str_replace('_translation','', $field);
                    $tFields[$field]=$_blog[$field]; 
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $blog->set('_translations',$translation);
        }
        
        
        $this->set(compact('blog'));
        $this->set('_serialize', ['blog']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Blog id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $blog = $this->Blog->get($id);
        if ($this->Blog->delete($blog)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.',[$this->Txt->printAdmin(__('Admin | blog'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.',[$this->Txt->printAdmin(__('Admin | blog'))])));
        }

        return $this->redirect(['action' => 'index']);
    }
}
