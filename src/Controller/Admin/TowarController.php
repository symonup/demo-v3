<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Towar Controller
 *
 * @property \App\Model\Table\TowarTable $Towar
 *
 * @method \App\Model\Entity\Towar[] paginate($object = null, array $settings = [])
 */
class TowarController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        if (empty($this->Waluta)) {
            $this->loadModel('Waluta');
        }
        if (empty($this->Vat)) {
            $this->loadModel('Vat');
        }
        if (empty($this->Jednostka)) {
            $this->loadModel('Jednostka');
        }
        $this->loadModel('Wyszukiwarka');
        $this->loadModel('Atrybut');
        $this->loadModel('AtrybutTyp');
        $this->loadModel('TowarAtrybut');
        $waluty = $this->Waluta->find('all')->toArray();
        $vats = $this->Vat->find('list', ['keyField' => 'id', 'valueField' => 'nazwa', 'order' => ['stawka' => 'asc']])->toArray();
        $vatStawka = $this->Vat->find('list', ['keyField' => 'id', 'valueField' => 'stawka', 'order' => ['stawka' => 'asc']])->toArray();
        $jednostki = $this->Jednostka->find('list', ['keyField' => 'id', 'valueField' => 'jednostka'])->toArray();
        $this->set(compact('waluty', 'vats', 'vatStawka', 'jednostki'));
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $lastPage = $this->session->read('backToTowar');
        $this->session->delete('backToTowar');
        $page = $this->request->query('page');
        $filter = $this->session->read('filterTowar');
        $likeKeys = [
            'nazwa' => 'nazwa',
            'kod' => 'kod'
        ];
        $forcePage = false;
        if ($this->request->is('post')) {
            $filter=[];
            $filterData = $this->request->getData();
            if (key_exists('clear-filter', $filterData)) {
                $filter = null;
                $this->session->write('filterTowar', $filter);
                return $this->redirect(['action'=>'index']);
            } else {
                foreach ($filterData as $keyFilter => $filterValue) {
                    if (!empty($filterValue)) {
                        $filter[$keyFilter] = $filterValue;
                    }
                }
            }
            $page = 1;
            $forcePage = true;
        }
        $conditions = [];
        if (!empty($filter)) {
            foreach ($filter as $keyFilter => $filterValue) {
                if($keyFilter=='ilosc'){
                    if(is_numeric($filterValue)){
                        $conditions['Towar.' . $keyFilter] = $filterValue;
                    }else{
                        if(strpos($filterValue, '>=')!==false){
                            $znak= '>=';
                            $val=trim(str_replace($znak,'',$filterValue));
                            $conditions['Towar.' . $keyFilter.' '.$znak] = (int)$val;
                        }elseif(strpos($filterValue, '<=')!==false){
                            $znak= '<=';
                            $val=trim(str_replace($znak,'',$filterValue));
                            $conditions['Towar.' . $keyFilter.' '.$znak] = (int)$val;
                        }elseif(strpos($filterValue, '<>')!==false || strpos($filterValue, '!=')!==false){
                            $znak= '!=';
                            $val=trim(str_replace(['<>','!='],['',''],$filterValue));
                            $conditions['Towar.' . $keyFilter.' '.$znak] = (int)$val;
                        }elseif(strpos($filterValue, '>')!==false){
                            $znak= '>';
                            $val=trim(str_replace($znak,'',$filterValue));
                            $conditions['Towar.' . $keyFilter.' '.$znak] = (int)$val;
                        }elseif(strpos($filterValue, '<')!==false){
                            $znak= '<';
                            $val=trim(str_replace($znak,'',$filterValue));
                            $conditions['Towar.' . $keyFilter.' '.$znak] = (int)$val;
                        }else{
                            $conditions['Towar.' . $keyFilter] = (int)$filterValue;
                        }
                    }
                }else if($keyFilter=='hot_deal'){
                    $hotDealIds=[];
                    switch ($filterValue){
                        case 'all':{
                            $hotDealIds=$this->Towar->HotDealEdit->find('list',['keyField'=>'towar_id','valueField'=>'towar_id','group'=>['towar_id']])->toArray();
                        }break;
                        case 'finished':{
                            $hotDealIds=$this->Towar->HotDealEdit->find('list',['keyField'=>'towar_id','valueField'=>'towar_id','group'=>['towar_id']])->where(['OR'=>['data_do <'=>date('Y-m-d H:i'),'ilosc'=>0]])->toArray();
                        }break;
                        case 'active':{
                            $hotDealIds=$this->Towar->HotDeal->find('list',['keyField'=>'towar_id','valueField'=>'towar_id','group'=>['towar_id']])->toArray();
                        }break;
                    }
                    if(empty($hotDealIds)){
                        $hotDealIds=[null];
                    }
                    $conditions['Towar.id IN']=$hotDealIds;
                }
                else if (key_exists($keyFilter, $likeKeys)) {
                    $conditions['Towar.' . $keyFilter . ' LIKE'] = '%' . $filterValue . '%';
                } else {
                    $conditions['Towar.' . $keyFilter] = $filterValue;
                }
            }
        }
        $this->session->write('filterTowar', $filter);
        if (!$forcePage && empty($page) && !empty($lastPage)) {
            $page = $lastPage;
        }
        if (empty($page)) {
            $page = 1;
        }
        $this->session->write('lastPageTowar', $page);
        $cfgArr = [
            'contain' => [
                'Wersja',
                'Certyfikat',
                'Producent',
                'HotDeal',
                'HotDealEdit',
                'TowarCena' => ['Waluta', 'conditions' => ['TowarCena.uzytkownik_id IS' => null,'TowarCena.aktywna'=>1]],
                'TowarZdjecie' => ['sort' => ['domyslne' => 'desc', 'kolejnosc' => 'desc']]
            ],
            'limit' => 100,
            'page' => $page,
            'conditions'=>$conditions,
            'order'=>['Towar.id'=>'asc']
        ];
        $this->paginate = $cfgArr;
        $towar = $this->paginate($this->Towar);
        $producenci = $this->Towar->Producent->find('list', ['keyField' => 'id', 'valueField' => 'nazwa'])->toArray();
        $okazje = $this->Towar->Okazje->find('list', ['keyField' => 'id', 'valueField' => 'nazwa'])->order(['kolejnosc'=>'desc','nazwa'=>'asc'])->toArray();
        $platformy = $this->Towar->Platforma->find('list', ['keyField' => 'id', 'valueField' => 'nazwa'])->toArray();
        $wersje = $this->Towar->Wersja->find('list', ['keyField' => 'id', 'valueField' => 'nazwa'])->toArray();
        $this->set(compact('towar', 'filter','producenci','platformy','wersje','okazje'));
        $this->set('_serialize', ['towar']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($id = null) {
        $towar = $this->Towar->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData = $this->request->getData();
            if (!empty($rqData['towar_atrybut']['atrybut_id'])) {
                $attrWariant = null;
                if (!empty($rqData['towar_atrybut']['attr_wariant'])) {
                    $attrWariant = $rqData['towar_atrybut']['attr_wariant'];
                }
                $atrybuty = [];
                $k = 0;
                foreach ($rqData['towar_atrybut']['atrybut_id'] as $atrybut_id) {
                    if (empty($atrybut_id))
                        continue;
                    $atrybuty[$k] = $this->TowarAtrybut->newEntity();
                    $atrybuty[$k]->atrybut_id = $atrybut_id;
                    $atrybuty[$k]->wartosc = (!empty($rqData['towar_atrybut']['wartosc'][$atrybut_id]) ? $rqData['towar_atrybut']['wartosc'][$atrybut_id] : null);
                    if (!empty($oldAttrs[$atrybut_id])) {
                        $atrybuty[$k]->id = $oldAttrsIds[$atrybut_id];
                        unset($oldAttrsIds[$atrybut_id]);
                    }
                    if (!empty($rqData['towar_atrybut']['atrybut_podrzedne_id'][$atrybut_id])) {
                        foreach ($rqData['towar_atrybut']['atrybut_podrzedne_id'][$atrybut_id] as $attrPod) {
                            if (empty($attrPod)) {
                                continue;
                            }
                            $k++;
                            $atrybuty[$k] = $this->TowarAtrybut->newEntity();
                            $atrybuty[$k]->atrybut_id = $atrybut_id;
                            $atrybuty[$k]->wartosc = (!empty($rqData['towar_atrybut']['wartosc'][$atrybut_id]) ? $rqData['towar_atrybut']['wartosc'][$atrybut_id] : null);
                            $atrybuty[$k]->atrybut_podrzedne_id = $attrPod;
                            if (!empty($attrWariant) && key_exists($atrybut_id, $rqData['towar_atrybut']['atrybut_typ'][$attrWariant])) {
                                $atrybuty[$k]->wariant = 1;
                            }

                            if (!empty($oldAttrsPodrzedne[$attrPod])) {
                                $atrybuty[$k]->id = $oldAttrsPodrzedneIds[$attrPod];
                                unset($oldAttrsPodrzedneIds[$attrPod]);
                            }
                        }
                    } else {
                        if (!empty($attrWariant) && key_exists($atrybut_id, $rqData['towar_atrybut']['atrybut_typ'][$attrWariant])) {
                            $atrybuty[$k]->wariant = 1;
                        }
                    }
                    $k++;
                }
                unset($rqData['towar_atrybut']);
                $towar->towar_atrybut = $atrybuty;
            }
            $towar = $this->Towar->patchEntity($towar, $rqData, ['translations' => $this->Towar->hasBehavior('Translate')]);
            if ($this->Towar->save($towar)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | towar'))])));

                return $this->redirect(['action' => 'index','#'=>(!empty($towar->id)?'row_'.$towar->id:null)]);
            }
            \Cake\Log\Log::write(LOG_ERR, $towar->errors());
            $errors=$towar->errors();
            $errMessages=[];
            foreach ($towar->errors() as $errorField => $errorMessage){
                foreach ($errorMessage as $errMess){
                    if(is_array($errMess)){
                        foreach ($errMess as $errMessField => $errMessItemMessage){
                            $errMessages[]=$errMessField.': '.join(', ', $errMessItemMessage);
                        }
                    }else{
                        $errMessages[]=$errorField.': '.$errMess;
                    }
                }
                
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | towar'))])).' '. join('; ', $errMessages));
        }
        if (!empty($id) && !$this->request->is(['patch', 'post', 'put'])) {
            if ($this->Towar->hasBehavior('Translate')) {
                $towar = $this->Towar->find('translations', [
                            'locales' => $this->lngIdList,
                            'conditions' => ['Towar.' . $this->Towar->primaryKey() => $id],
                            'contain' => ['HotDealEdit','Atrybut', 'TowarAtrybut', 'Kategoria','Certyfikat', 'TowarCena', 'Akcesoria' => ['TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]]], 'Podobne' => ['TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]]], 'Polecane' => ['TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]]]]
                        ])->first();
            } else {
                $towar = $this->Towar->find('all', [
                            'conditions' => ['Towar.' . $this->Towar->primaryKey() => $id],
                            'contain' => ['HotDealEdit','Atrybut', 'TowarAtrybut', 'Kategoria','Certyfikat', 'TowarCena', 'Akcesoria' => ['TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]]], 'Podobne' => ['TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]]], 'Polecane' => ['TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]]]]
                        ])->first();
            }
            if (!empty($towar->towar_cena)) {
                $tmpCeny = [];
                foreach ($towar->towar_cena as $tCena) {
                    $tmpCeny[$tCena->waluta_id] = $tCena;
                }
                $towar->towar_cena = $tmpCeny;
            }
            $_towar = $towar->toArray();
            if ($this->Towar->hasBehavior('Translate') && empty($_towar['_translations'])) {
                $transFields = $this->Towar->associations()->keys();
                $tFields = [];
                foreach ($transFields as $field) {
                    if (strpos($field, '_translation') !== false) {
                        $field = substr($field, (strpos($field, '_') + 1));
                        $field = str_replace('_translation', '', $field);
                        $tFields[$field] = $_towar[$field];
                    }
                }
                $translation = [$this->defLngSymbol => $tFields];
                $towar->set('_translations', $translation);
            }
        }

        $producent = $this->Towar->Producent->find('list', ['keyField' => 'id', 'valueField' => 'nazwa'])->toArray();
        $wersje = $this->Towar->Wersja->find('list', ['keyField' => 'id', 'valueField' => 'nazwa'])->toArray();
        $kategoria = $this->Towar->Kategoria->find('list',['keyField' => 'id', 'valueField' => 'nazwa'])->toArray();
        $kategoriaSelected = $this->Towar->TowarKategoria->find('list', ['keyField' => 'kategoria_id', 'valueField' => 'kategoria_id', 'conditions' => ['towar_id' => $id]])->toArray();
        $katArr = $this->Towar->Kategoria->getCategoryTreeAll();



        $atrybutyWyszukiwaka = [];
        $grupyAtrybutow = [];
        $typyAtrybutow = [];
        $grupyAttrIds = [];
        $typyAttrIds = [];

        $ifWyszukiwarkaAttr = \Cake\Core\Configure::read('wyszukiwarki.wyszukiwarka_atrybutow');
        if (!empty($ifWyszukiwarkaAttr)) {
            $this->loadModel('Wyszukiwarka');
            $wyszukiwarkaAtrIds = $this->Wyszukiwarka->WyszukiwarkaBox->find('list', ['keyField' => 'atrybut', 'valueField' => 'atrybut', 'group' => ['atrybut']])->toArray();
            if (!empty($wyszukiwarkaAtrIds)) {
                foreach ($wyszukiwarkaAtrIds as $attrTypId => $tmp) {
                    if (strpos($attrTypId, 'g_') !== false) {
                        $grupyAttrIds[] = str_replace('g_', '', $attrTypId);
                    } else {
                        $typyAttrIds[] = $attrTypId;
                    }
                    if (!empty($grupyAttrIds)) {
                        $grupyAtrybutow = $this->AtrybutTyp->find('all', ['conditions' => ['AtrybutTyp.atrybut_typ_parent_id IN' => $grupyAttrIds], 'contain' => ['Atrybut' => ['AtrybutPodrzedne' => ['sort' => ['AtrybutPodrzedne.nazwa' => 'asc']]], 'AtrybutTypParent']])->toArray();
                    }
                    if (!empty($typyAttrIds)) {
                        $typyAtrybutow = $this->AtrybutTyp->find('all', ['conditions' => ['AtrybutTyp.id IN' => $typyAttrIds], 'contain' => ['Atrybut' => ['AtrybutPodrzedne' => ['sort' => ['AtrybutPodrzedne.nazwa' => 'asc']]]]])->toArray();
                    }
                }
            }
        }
        $this->set('wyszukiwarkaTypyAtrybutow', $typyAtrybutow);
        $this->set('wyszukiwarkaGrupyAtrybutow', $grupyAtrybutow);
        $attrCond = [];
        if (!empty($typyAttrIds)) {
            $attrCond['id NOT IN'] = $typyAttrIds;
        }
        if (!empty($grupyAttrIds)) {
            $attrCond['OR']['atrybut_typ_parent_id NOT IN'] = $grupyAttrIds;
            $attrCond['OR']['atrybut_typ_parent_id IS'] = NULL;
        }
        $attrWariantSelected = $this->TowarAtrybut->find('all', ['contain' => ['Atrybut'], 'conditions' => ['TowarAtrybut.towar_id' => $towar->id, 'TowarAtrybut.wariant' => 1]])->first();
        $selectedWariantAttrType = null;
        if (!empty($attrWariantSelected)) {
            $selectedWariantAttrType = $attrWariantSelected->atrybut->atrybut_typ_id;
        }
        $this->set('selectedWariantAttrType', $selectedWariantAttrType);
        $atrybut_typ = $this->AtrybutTyp->find('list', ['keyField' => 'id', 'valueField' => 'nazwa', 'order' => 'kolejnosc desc', 'conditions' => $attrCond]);
        $allowAtribute = $this->Atrybut->find('list', ['keyField' => 'id', 'valueField' => 'id', 'condiitons' => ['atrybut_typ_id IN' => array_keys($atrybut_typ->toArray())]])->toArray();
        $this->set('atrybutTyp', $atrybut_typ);
        $towar_atrybut_typ = array_keys($atrybut_typ->toArray());
        $this->set('towar_atrybut_typ', $towar_atrybut_typ);
        $selected_atrybut = $this->Towar->TowarAtrybut->find('list', ['keyField' => 'atrybut_id', 'valueField' => 'wartosc', 'conditions' => ['towar_id' => $id]])->toArray();
        $this->set('selected_atrybut', $selected_atrybut);
        $selected_atrybut_podrzedne = $this->Towar->TowarAtrybut->find('list', ['keyField' => 'atrybut_podrzedne_id', 'valueField' => 'atrybut_id', 'conditions' => ['towar_id' => $id, 'atrybut_podrzedne_id IS NOT NULL']])->toArray();
        $this->set('selected_atrybut_podrzedne', $selected_atrybut_podrzedne);
        $allAttrs = $this->AtrybutTyp->find('all', ['conditions' => ['AtrybutTyp.id IN' => (empty($towar_atrybut_typ) ? [null] : $towar_atrybut_typ)], 'contain' => ['Atrybut' => ['AtrybutPodrzedne' => ['sort' => ['AtrybutPodrzedne.nazwa' => 'asc']]], 'AtrybutTypParent']]);
        $this->set('allAttrs', $allAttrs);

$this->loadModel('Certyfikat');
        $certyfikat = $this->Certyfikat->find('list',['keyField'=>'id','valueField'=>'skrot'])->toArray();
        $this->loadModel('Wiek');
        $wiek = $this->Wiek->find('list',['keyField'=>'id','valueField'=>'nazwa'])->toArray();
        $this->loadModel('Opakowanie');
        $opakowanie = $this->Opakowanie->find('list',['keyField'=>'id','valueField'=>'nazwa'])->toArray();
        $okazje = $this->Towar->Okazje->find('list', ['keyField' => 'id', 'valueField' => 'nazwa'])->order(['kolejnosc'=>'desc','nazwa'=>'asc'])->toArray();
        $this->set(compact('towar', 'producent','opakowanie','wiek','wersje', 'atrybut', 'kategoria', 'katArr', 'kategoriaSelected','certyfikat','okazje'));
        $this->set('_serialize', ['towar']);

        $tPage = $this->session->read('lastPageTowar');
        if (!empty($tPage)) {
            $this->session->write('backToTowar', $tPage);
            $this->session->delete('lastPageTowar');
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Towar id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {

        if ($this->Towar->hasBehavior('Translate')) {
            $towar = $this->Towar->find('translations', [
                        'locales' => $this->lngIdList,
                        'conditions' => ['Towar.' . $this->Towar->primaryKey() => $id],
                        'contain' => ['Okazje','HotDealEdit' => function ($query) {return $query->find('translations');},'Atrybut', 'TowarAtrybut', 'Kategoria','Certyfikat', 'TowarCenaDefault','TowarCena'=>['conditions'=>['TowarCena.uzytkownik_id IS'=>null]], 'Akcesoria' => ['TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]]], 'Podobne' => ['TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]]], 'Polecane' => ['TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]]]]
                    ])->first();
        } else {
            $towar = $this->Towar->find('all', [
                        'conditions' => ['Towar.' . $this->Towar->primaryKey() => $id],
                        'contain' => ['Okazje','HotDealEdit','Atrybut', 'TowarAtrybut', 'Kategoria','Certyfikat', 'TowarCenaDefault','TowarCena'=>['conditions'=>['TowarCena.uzytkownik_id IS'=>null]], 'Akcesoria' => ['TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]]], 'Podobne' => ['TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]]], 'Polecane' => ['TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]]]]
                    ])->first();
        }
        $oldIlosc = $towar->ilosc;
        $oldCena = 0;
        if (!empty($towar->towar_cena)) {
            $tmpCeny = [];
            foreach ($towar->towar_cena as $tCena) {
                $tmpCeny[$tCena->waluta_id] = $tCena;
                $oldCena=$tCena['cena_sprzedazy'];
            }
            $towar->towar_cena = $tmpCeny;
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData = $this->request->getData();
            $oldAttrs = [];
            $oldAttrsIds = [];
            $oldAttrsPodrzedne = [];
            $oldAttrsPodrzedneIds = [];
            if(empty($rqData['hot_deal_edit']) && !empty($towar->hot_deal_edit)){
                $rqData['hot_deal_edit']['aktywna']=0;
            }
            if (!empty($towar->towar_atrybut)) {
                foreach ($towar->towar_atrybut as $tAttr) {
                    if (!empty($tAttr->atrybut_podrzedne_id)) {
                        $oldAttrsPodrzedne[$tAttr->atrybut_podrzedne_id] = $tAttr->atrybut_podrzedne_id;
                        $oldAttrsPodrzedneIds[$tAttr->atrybut_podrzedne_id] = $tAttr->id;
                    } else {
                        $oldAttrs[$tAttr->atrybut_id] = $tAttr->atrybut_id;
                        $oldAttrsIds[$tAttr->atrybut_id] = $tAttr->id;
                    }
                }
            }
            if (!empty($rqData['towar_atrybut']['atrybut_id'])) {
                $attrWariant = null;
                if (!empty($rqData['towar_atrybut']['attr_wariant'])) {
                    $attrWariant = $rqData['towar_atrybut']['attr_wariant'];
                }
                $atrybuty = [];
                $k = 0;
                foreach ($rqData['towar_atrybut']['atrybut_id'] as $atrybut_id) {
                    if (empty($atrybut_id))
                        continue;
                    $atrybuty[$k] = $this->TowarAtrybut->newEntity();
                    $atrybuty[$k]->atrybut_id = $atrybut_id;
                    $atrybuty[$k]->wartosc = (!empty($rqData['towar_atrybut']['wartosc'][$atrybut_id]) ? $rqData['towar_atrybut']['wartosc'][$atrybut_id] : null);
                    if (!empty($oldAttrs[$atrybut_id])) {
                        $atrybuty[$k]->id = $oldAttrsIds[$atrybut_id];
                        unset($oldAttrsIds[$atrybut_id]);
                    }
                    if (!empty($rqData['towar_atrybut']['atrybut_podrzedne_id'][$atrybut_id])) {
                        foreach ($rqData['towar_atrybut']['atrybut_podrzedne_id'][$atrybut_id] as $attrPod) {
                            if (empty($attrPod)) {
                                continue;
                            }
                            $k++;
                            $atrybuty[$k] = $this->TowarAtrybut->newEntity();
                            $atrybuty[$k]->atrybut_id = $atrybut_id;
                            $atrybuty[$k]->wartosc = (!empty($rqData['towar_atrybut']['wartosc'][$atrybut_id]) ? $rqData['towar_atrybut']['wartosc'][$atrybut_id] : null);
                            $atrybuty[$k]->atrybut_podrzedne_id = $attrPod;
                            if (!empty($attrWariant) && key_exists($atrybut_id, $rqData['towar_atrybut']['atrybut_typ'][$attrWariant])) {
                                $atrybuty[$k]->wariant = 1;
                            }else{
                                 $atrybuty[$k]->wariant = 0;
                            }

                            if (!empty($oldAttrsPodrzedne[$attrPod])) {
                                $atrybuty[$k]->id = $oldAttrsPodrzedneIds[$attrPod];
                                unset($oldAttrsPodrzedneIds[$attrPod]);
                            }
                        }
                    } else {
                        if (!empty($attrWariant) && key_exists($atrybut_id, $rqData['towar_atrybut']['atrybut_typ'][$attrWariant])) {
                            $atrybuty[$k]->wariant = 1;
                        }else{
                                 $atrybuty[$k]->wariant = 0;
                            }
                    }
                    $k++;
                }
                unset($rqData['towar_atrybut']);
                $towar->towar_atrybut = $atrybuty;
            }
            if(empty($rqData['towar_cena'])){
                unset($rqData['towar_cena']);
            }

            $towar = $this->Towar->patchEntity($towar, $rqData, ['translations' => $this->Towar->hasBehavior('Translate')]);
            
                if($towar->ilosc != $oldIlosc){
                    $log = date('Y-m-d H:i:s').' - Zmiana ilosci z '.$oldIlosc.' na '.$towar->ilosc;
                    $iloscLog=(!empty($towar->ilosc_log)?json_decode($towar->ilosc_log,true):[]);
                    $iloscLog[]=$log;
                    $towar->ilosc_log= json_encode($iloscLog);
                }
            if ($this->Towar->save($towar)) {
                $actCena = (!empty($towar->towar_cena)?$towar->towar_cena[0]->cena_sprzedazy:0);
                if (!empty($oldAttrsIds)) {
                    $this->TowarAtrybut->deleteAll(['id IN' => $oldAttrsIds]);
                }
                if (!empty($oldAttrsPodrzedneIds)) {
                    $this->TowarAtrybut->deleteAll(['id IN' => $oldAttrsPodrzedneIds]);
                }
                $allegroToUpdate=[];
                if($oldCena!=$actCena){
                    $allegroToUpdate['priceToUpdate']=$actCena;
                }
                if($towar->ilosc != $oldIlosc){
                    if($towar->ilosc<=0){
                        $allegroToUpdate['iloscToUpdate']=-10000;
                    }else{
                    $allegroToUpdate['iloscToUpdate']=$towar->ilosc;
                    }
                }
                if(!empty($allegroToUpdate)){
                    if(empty($this->AllegroAukcja)){
                        $this->loadModel('AllegroAukcja');
                    }
                    $this->AllegroAukcja->updateAll($allegroToUpdate,['towar_id'=>$towar->id]);
                }
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | towar'))])));

                return $this->redirect(['action' => 'index','#'=>(!empty($towar->id)?'row_'.$towar->id:null)]);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | towar'))])));
        }

        $_towar = $towar->toArray();
        if ($this->Towar->hasBehavior('Translate') && empty($_towar['_translations'])) {
            $transFields = $this->Towar->associations()->keys();
            $tFields = [];
            foreach ($transFields as $field) {
                if (strpos($field, '_translation') !== false) {
                    $field = substr($field, (strpos($field, '_') + 1));
                    $field = str_replace('_translation', '', $field);
                    $tFields[$field] = $_towar[$field];
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $towar->set('_translations', $translation);
        }


        $producent = $this->Towar->Producent->find('list', ['keyField' => 'id', 'valueField' => 'nazwa'])->toArray();
        $wersje = $this->Towar->Wersja->find('list', ['keyField' => 'id', 'valueField' => 'nazwa'])->toArray();
        $kategoria = $this->Towar->Kategoria->find('list',['keyField' => 'id', 'valueField' => 'nazwa'])->toArray();
        $kategoriaSelected = $this->Towar->TowarKategoria->find('list', ['keyField' => 'kategoria_id', 'valueField' => 'kategoria_id', 'conditions' => ['towar_id' => $id]])->toArray();
        $katArr = $this->Towar->Kategoria->getCategoryTreeAll();



        $atrybutyWyszukiwaka = [];
        $grupyAtrybutow = [];
        $typyAtrybutow = [];
        $grupyAttrIds = [];
        $typyAttrIds = [];

        $ifWyszukiwarkaAttr = \Cake\Core\Configure::read('wyszukiwarki.wyszukiwarka_atrybutow');
        if (!empty($ifWyszukiwarkaAttr)) {
            $this->loadModel('Wyszukiwarka');
            $wyszukiwarkaAtrIds = $this->Wyszukiwarka->WyszukiwarkaBox->find('list', ['keyField' => 'atrybut', 'valueField' => 'atrybut', 'group' => ['atrybut']])->toArray();
            if (!empty($wyszukiwarkaAtrIds)) {
                foreach ($wyszukiwarkaAtrIds as $attrTypId => $tmp) {
                    if (strpos($attrTypId, 'g_') !== false) {
                        $grupyAttrIds[] = str_replace('g_', '', $attrTypId);
                    } else {
                        $typyAttrIds[] = $attrTypId;
                    }
                    if (!empty($grupyAttrIds)) {
                        $grupyAtrybutow = $this->AtrybutTyp->find('all', ['conditions' => ['AtrybutTyp.atrybut_typ_parent_id IN' => $grupyAttrIds], 'contain' => ['Atrybut' => ['AtrybutPodrzedne' => ['sort' => ['AtrybutPodrzedne.nazwa' => 'asc']]], 'AtrybutTypParent']])->toArray();
                    }
                    if (!empty($typyAttrIds)) {
                        $typyAtrybutow = $this->AtrybutTyp->find('all', ['conditions' => ['AtrybutTyp.id IN' => $typyAttrIds], 'contain' => ['Atrybut' => ['AtrybutPodrzedne' => ['sort' => ['AtrybutPodrzedne.nazwa' => 'asc']]]]])->toArray();
                    }
                }
            }
        }
        $this->set('wyszukiwarkaTypyAtrybutow', $typyAtrybutow);
        $this->set('wyszukiwarkaGrupyAtrybutow', $grupyAtrybutow);
        $attrCond = [];
        if (!empty($typyAttrIds)) {
            $attrCond['id NOT IN'] = $typyAttrIds;
        }
        if (!empty($grupyAttrIds)) {
            $attrCond['OR']['atrybut_typ_parent_id NOT IN'] = $grupyAttrIds;
            $attrCond['OR']['atrybut_typ_parent_id IS'] = NULL;
        }
        $attrWariantSelected = $this->TowarAtrybut->find('all', ['contain' => ['Atrybut'], 'conditions' => ['TowarAtrybut.towar_id' => $towar->id, 'TowarAtrybut.wariant' => 1]])->first();
        $selectedWariantAttrType = null;
        if (!empty($attrWariantSelected)) {
            $selectedWariantAttrType = $attrWariantSelected->atrybut->atrybut_typ_id;
        }
        $this->set('selectedWariantAttrType', $selectedWariantAttrType);
        $atrybut_typ = $this->AtrybutTyp->find('list', ['keyField' => 'id', 'valueField' => 'nazwa', 'order' => 'kolejnosc desc', 'conditions' => $attrCond]);
        $allowAtribute = $this->Atrybut->find('list', ['keyField' => 'id', 'valueField' => 'id', 'condiitons' => ['atrybut_typ_id IN' => array_keys($atrybut_typ->toArray())]])->toArray();
        $this->set('atrybutTyp', $atrybut_typ);
        $towar_atrybut_typ = array_keys($atrybut_typ->toArray());
        $this->set('towar_atrybut_typ', $towar_atrybut_typ);
        $selected_atrybut = $this->Towar->TowarAtrybut->find('list', ['keyField' => 'atrybut_id', 'valueField' => 'wartosc', 'conditions' => ['towar_id' => $id]])->toArray();
        $this->set('selected_atrybut', $selected_atrybut);
        $selected_atrybut_podrzedne = $this->Towar->TowarAtrybut->find('list', ['keyField' => 'atrybut_podrzedne_id', 'valueField' => 'atrybut_id', 'conditions' => ['towar_id' => $id, 'atrybut_podrzedne_id IS NOT NULL']])->toArray();
        $this->set('selected_atrybut_podrzedne', $selected_atrybut_podrzedne);
        $allAttrs = $this->AtrybutTyp->find('all', ['conditions' => ['AtrybutTyp.id IN' => (empty($towar_atrybut_typ) ? [null] : $towar_atrybut_typ)], 'contain' => ['Atrybut' => ['AtrybutPodrzedne' => ['sort' => ['AtrybutPodrzedne.nazwa' => 'asc']]], 'AtrybutTypParent']]);
        $this->set('allAttrs', $allAttrs);
        $this->loadModel('Certyfikat');
        $certyfikat = $this->Certyfikat->find('list',['keyField'=>'id','valueField'=>'skrot'])->toArray();
        $this->loadModel('Wiek');
        $wiek = $this->Wiek->find('list',['keyField'=>'id','valueField'=>'nazwa'])->toArray();
        $this->loadModel('Opakowanie');
        $opakowanie = $this->Opakowanie->find('list',['keyField'=>'id','valueField'=>'nazwa'])->toArray();
       $okazje = $this->Towar->Okazje->find('list', ['keyField' => 'id', 'valueField' => 'nazwa'])->order(['kolejnosc'=>'desc','nazwa'=>'asc'])->toArray();
        
        $this->set(compact('towar', 'producent','opakowanie','wiek','wersje', 'atrybut', 'kategoria', 'katArr', 'kategoriaSelected','certyfikat','okazje'));
        $this->set('_serialize', ['towar']);

        $tPage = $this->session->read('lastPageTowar');
        if (!empty($tPage)) {
            $this->session->write('backToTowar', $tPage);
            $this->session->delete('lastPageTowar');
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Towar id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->loadComponent('Upload');
        $towar = $this->Towar->get($id, ['contain' => ['TowarZdjecie']]);
        $files = [];
        $thumbs = \Cake\Core\Configure::read('product_thumbs');
        $path = $this->filePath['towar'];
        if (!empty($towar->towar_zdjecie)) {
            foreach ($towar->towar_zdjecie as $zd) {
                $files[] = ['id' => $zd->id, 'plik' => $zd->plik];
            }
        }
        if ($this->Towar->delete($towar)) {
            if (!empty($files)) {
                foreach ($files as $file) {
                    if (!empty($file['plik'])) {
                        if (file_exists($path . DS . $this->Txt->getKatalog($file['id']) . DS . $file['plik'])) {
                            $this->Upload->deleteFile($path . DS . $this->Txt->getKatalog($file['id']) . DS . $file['plik']);
                        }
                        if (!empty($thumbs)) {
                            foreach ($thumbs as $thName => $thOption) {
                                if (file_exists($path . DS . $this->Txt->getKatalog($file['id']) . DS . $thName . '_' . $file['plik'])) {
                                    $this->Upload->deleteFile($path . DS . $this->Txt->getKatalog($file['id']) . DS . $thName . '_' . $file['plik']);
                                }
                            }
                        }
                    }
                }
            }
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.', [$this->Txt->printAdmin(__('Admin | towar'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.', [$this->Txt->printAdmin(__('Admin | towar'))])));
        }

        $tPage = $this->session->read('lastPageTowar');
        if (!empty($tPage)) {
            $this->session->write('backToTowar', $tPage);
            $this->session->delete('lastPageTowar');
        }

        return $this->redirect(['action' => 'index','#'=>'row_'.$id]);
    }

    public function findSimilar($search = null) {
        $this->autoRender = false;
        if (!empty($search)) {
            $conditions = [];
            $towarId = $this->request->query('towar_id');
            if (!empty($towarId)) {
                $conditions['Towar.id !='] = $towarId;
            }
            $keys = explode(' ', $search);
            foreach ($keys as $key) {
                $conditions['AND']['OR']['AND'][] = ['Towar.nazwa LIKE' => '%' . $key . '%'];
            }
            $conditions['AND']['OR'][] = ['Towar.kod LIKE' => '%' . $search . '%'];
            if (!empty($_GET['to_order'])) {
                $conditions['Towar.ukryty'] = 0;
//                $conditions['Towar.wariant !='] = 1;
            }
//            else if (!empty($_GET['cecha']))
//                $conditions['Towar.wariant NOT IN'] = [1, 2];
//            else
//                $conditions['Towar.wariant !='] = 2;
            $towary = $this->Towar->find('all', ['conditions' => $conditions, 'order' => ['Towar.nazwa ASC'], 'limit' => 50, 'contain' => ['TowarCena', 'TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]]]]);

            $options = '';
            if ($towary_lista = $towary->toArray()) {
                if (!empty($_GET['to_order'])) {
                    foreach ($towary_lista as $towar) {
                        if (!empty($towar->towar_cena->wartosc_minimalna)) {
                            $towar->towar_cena->wartosc_minimalna = round($towar->towar_cena->wartosc_minimalna, 3);
                            $maxCount = 1000;
                            $_options = '';
                            if (!empty($towar->towar_cena->wartosc_minimalna_count))
                                $maxCount = $towar->towar_cena->wartosc_minimalna_count * $towar->towar_cena->wartosc_minimalna;
                            for ($i_count = $towar->towar_cena->wartosc_minimalna; $i_count <= $maxCount; $i_count += $towar->towar_cena->wartosc_minimalna) {
                                $_options .= '<option value="' . $i_count . '">' . $i_count . '</option>';
                            }
                            $input_ilosc = '<select class="result_ilosc" disabled="disabled" name="zamowienie_towar[towar_id][' . $towar->id . '][ilosc]">' . $_options . '</select>';
                        } else
                            $input_ilosc = '<input class="result_ilosc" type="text" disabled="disabled" value="1" name="zamowienie_towar[towar_id][' . $towar->id . '][ilosc]"/>';
                        $options .= '<li><ul class="no-sort"><li class="name">' . (!empty($towar->towar_zdjecie) && file_exists($this->filePath['towar'] . 'thumb_' . $towar->towar_zdjecie[0]['plik']) ? '<img style="max-width: 50px; float: left;" src="' . $this->displayPath['towar'] . 'thumb_' . $towar->towar_zdjecie[0]['plik'] . '"/>' : '') . $towar->nazwa . '<input type="hidden" disabled="disabled" value="' . $towar->id . '" name="zamowienie_towar[towar_id][' . $towar->id . '][id]"/></li><li><span class="result_info_ilosc">' . $this->Txt->printAdmin(__('Admin | ilosc: {0}', $towar->ilosc)) . '</span>' . $input_ilosc . '</li><li><span class="result_info_cena">' . $this->Txt->printAdmin(__('Admin | cena: {0}', $towar->towar_cena->cena_sprzedazy)) . '</span><input data-type="float" class="result_cena" type="text" disabled="disabled" value="' . $towar->towar_cena->cena_sprzedazy . '" name="zamowienie_towar[towar_id][' . $towar->id . '][cena]"/></li><li class="result_razem"><span class="result_kwota">' . (!empty($towar->towar_cena->wartosc_minimalna) ? round($towar->towar_cena->wartosc_minimalna * $towar->towar_cena->cena_sprzedazy, 3) : $towar->towar_cena->cena_sprzedazy) . '</span></li></ul></li>';
                    }
                }
                else if (!empty($_GET['wymiana'])) {
                    foreach ($towary_lista as $towar) {
                        $options .= '<li>' . (!empty($towar->towar_zdjecie) && file_exists($this->filePath['towar'] . 'thumb_' . $towar->towar_zdjecie[0]['plik']) ? '<img style="max-width: 50px; float: left;" src="' . $this->displayPath['towar'] . 'thumb_' . $towar->towar_zdjecie[0]['plik'] . '"/>' : '') . $towar->nazwa . '<input type="hidden" disabled="disabled" value="' . $towar->id . '" name="' . $_GET['field'] . '[' . $_GET['towar_id'] . '][towar_id][]"/></li>';
                    }
                } else if (!empty($_GET['cecha'])) {
                    foreach ($towary_lista as $towar) {
                        $options .= '<li>' . (!empty($towar->towar_zdjecie) && file_exists($this->filePath['towar'] . 'thumb_' . $towar->towar_zdjecie[0]['plik']) ? '<img style="max-width: 50px; float: left;" src="' . $this->displayPath['towar'] . 'thumb_' . $towar->towar_zdjecie[0]['plik'] . '"/>' : '') . $towar->nazwa . '<input type="hidden" kod="' . $towar->kod . '" ean="' . $towar->ean . '" towar-id="' . $towar->id . '" ilosc="' . $towar->ilosc . '" disabled="disabled" value="' . $towar->id . '" name="warianty[_ids][' . $towar->id . ']"/><button type="button" class="btn btn-xs btn-primary add_to_cecha">' . $this->Txt->printAdmin(__('Admin | Dodaj')) . '</button></li>';
                    }
                } if (!empty($_GET['user_rabat'])) {
                    foreach ($towary_lista as $towar) {
                        $options .= '<li>' . (!empty($towar->towar_zdjecie) && file_exists($this->filePath['towar'] . 'thumb_' . $towar->towar_zdjecie[0]['plik']) ? '<img style="max-width: 50px; float: left;" src="' . $this->displayPath['towar'] . 'thumb_' . $towar->towar_zdjecie[0]['plik'] . '"/>' : '') . $towar->nazwa . '<button type="button" class="btn btn-xs btn-primary add_to_towar_uzytkownik_rabat" towar_id="' . $towar->id . '">' . $this->Txt->printAdmin(__('Admin | Dodaj')) . '</button></li>';
                    }
                } else {
                    foreach ($towary_lista as $towar) {
                        $options .= '<li>' . (!empty($towar->towar_zdjecie) && file_exists($this->filePath['towar'] . 'thumb_' . $towar->towar_zdjecie[0]['plik']) ? '<img style="max-width: 50px; float: left;" src="' . $this->displayPath['towar'] . 'thumb_' . $towar->towar_zdjecie[0]['plik'] . '"/>' : '') . $towar->nazwa . '<input type="hidden" disabled="disabled" value="' . $towar->id . '" name="' . $_GET['field'] . '[_ids][]"/></li>';
                    }
                }
                $respon = $options;
            } else
                $respon = '<li>' . $this->Txt->printAdmin(__('Admin | no search results')) . '</li>';
        } else {
            $respon = '<li>' . $this->Txt->printAdmin(__('Admin | enter name of product')) . '</li>';
        }
        $this->response->type('json');
        $this->response->body(json_encode(['content' => $respon]));
        return $this->response;
    }

    public function findArtykul($search = null) {
        $this->autoRender = false;
        if (!empty($search)) {
            $this->loadModel('Artykul');
            $conditions = [];
            $keys = explode(' ', $search);
            foreach ($keys as $key) {
//                ['AND']['OR']['AND'][]
                $conditions['AND']['OR']['AND'][] = ['Artykul.tytul LIKE' => '%' . $key . '%'];
            }
            $artykuly = $this->Artykul->find('all', ['conditions' => $conditions, 'order' => ['Artykul.tytul ASC'], 'limit' => 50]);

            $options = '';
            if ($artykuly_lista = $artykuly->toArray()) {

                foreach ($artykuly_lista as $artykul) {
                    $options .= '<li>' . (!empty($artykul->zdjecie_1) && file_exists($this->filePath['artykul'] . $artykul->zdjecie_1) ? '<img style="max-width: 50px; float: left;" src="/files/artykul/' . $artykul->zdjecie_1 . '"/>' : '') . $artykul->tytul . '<input type="hidden" disabled="disabled" value="' . $artykul->id . '" name="artykul[_ids][]"/></li>';
                }

                $respon = $options;
            } else
                $respon = '<li>' . $this->Txt->printAdmin(__('Admin | no search results')) . '</li>';
        } else {
            $respon = '<li>' . $this->Txt->printAdmin(__('Admin | enter name of product')) . '</li>';
        }

        $this->response->type('json');
        $this->response->body(json_encode(['content' => $respon]));
        return $this->response;
    }

    public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->Towar->find('all', ['conditions' => ['Towar.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                    $item->{$field} = $value;
                    if ($this->Towar->save($item)) {
                        $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, (!empty($value)?0:1)])), 'action' => $retAction];
                    } else {
                        $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                    }
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    public function porownywarki() {
        $lastPage = $this->session->read('backToTowar');
        $this->session->delete('backToTowar');
        $page = $this->request->query('page');
        $filter = $this->session->read('filterTowarP');
        $likeKeys = [
            'nazwa' => 'nazwa',
            'kod' => 'kod'
        ];
        $forcePage = false;
        if ($this->request->is('post')) {
            $filter = [];
            $filterData = $this->request->getData();
            if (key_exists('clear-filter', $filterData)) {
                $filter = null;
                $this->session->write('filterTowarP', $filter);
                return $this->redirect(['action' => 'porownywarki']);
            } else {
                foreach ($filterData as $keyFilter => $filterValue) {
                    if (!empty($filterValue)) {
                        $filter[$keyFilter] = $filterValue;
                    }
                }
            }
            $page = 1;
            $forcePage = true;
        }
        $conditions = [];
        if (!empty($filter)) {
            foreach ($filter as $keyFilter => $filterValue) {
                if ($keyFilter == 'ilosc') {
                    if (is_numeric($filterValue)) {
                        $conditions['Towar.' . $keyFilter] = $filterValue;
                    } else {
                        if (strpos($filterValue, '>=') !== false) {
                            $znak = '>=';
                            $val = trim(str_replace($znak, '', $filterValue));
                            $conditions['Towar.' . $keyFilter . ' ' . $znak] = (int) $val;
                        } elseif (strpos($filterValue, '<=') !== false) {
                            $znak = '<=';
                            $val = trim(str_replace($znak, '', $filterValue));
                            $conditions['Towar.' . $keyFilter . ' ' . $znak] = (int) $val;
                        } elseif (strpos($filterValue, '<>') !== false || strpos($filterValue, '!=') !== false) {
                            $znak = '!=';
                            $val = trim(str_replace(['<>', '!='], ['', ''], $filterValue));
                            $conditions['Towar.' . $keyFilter . ' ' . $znak] = (int) $val;
                        } elseif (strpos($filterValue, '>') !== false) {
                            $znak = '>';
                            $val = trim(str_replace($znak, '', $filterValue));
                            $conditions['Towar.' . $keyFilter . ' ' . $znak] = (int) $val;
                        } elseif (strpos($filterValue, '<') !== false) {
                            $znak = '<';
                            $val = trim(str_replace($znak, '', $filterValue));
                            $conditions['Towar.' . $keyFilter . ' ' . $znak] = (int) $val;
                        } else {
                            $conditions['Towar.' . $keyFilter] = (int) $filterValue;
                        }
                    }
                } else if ($keyFilter == 'kategoria') {
                    $katsIds = $this->Towar->Kategoria->getAllIds($filterValue);
                    $idsByCat = $this->Towar->TowarKategoria->find('list', ['keyField' => 'towar_id', 'valueField' => 'towar_id'])->where(['kategoria_id IN' => $katsIds])->toArray();
                    if (empty($idsByCat)) {
                        $idsByCat = [null];
                    }
                    $conditions['Towar.id IN'] = $idsByCat;
                } else if (key_exists($keyFilter, $likeKeys)) {
                    $conditions['Towar.' . $keyFilter . ' LIKE'] = '%' . $filterValue . '%';
                } else {
                    $conditions['Towar.' . $keyFilter] = $filterValue;
                }
            }
        }
        $this->session->write('filterTowarP', $filter);
        if (!$forcePage && empty($page) && !empty($lastPage)) {
            $page = $lastPage;
        }
        if (empty($page)) {
            $page = 1;
        }
        $this->session->write('lastPageTowar', $page);
        $cfgArr = [
            'contain' => [
                'Producent',
                'Platforma',
                'TowarCena' => ['Waluta', 'Vat', 'Jednostka'],
                'TowarZdjecie' => ['sort' => ['domyslne' => 'desc', 'kolejnosc' => 'desc']],
                'Kategoria'
            ],
            'limit' => 100,
            'page' => $page,
            'conditions' => $conditions,
            'order' => ['Towar.id' => 'asc']
        ];
            $this->paginate = $cfgArr;
            $towar = $this->paginate($this->Towar);
            $producenci = $this->Towar->Producent->find('list', ['keyField' => 'id', 'valueField' => 'nazwa'])->toArray();
            $platformy = $this->Towar->Platforma->find('list', ['keyField' => 'id', 'valueField' => 'nazwa'])->toArray();
            $katArr = $parentKategoria = $this->Towar->Kategoria->ParentKategoria->find('list', ['keyField' => 'id', 'valueField' => 'sciezka', 'order' => ['sciezka' => 'asc']])->toArray();
            $this->set(compact('towar', 'filter', 'producenci', 'katArr','platformy'));
            $this->set('_serialize', ['towar']);
        
    }
    public function findToSelect2(){
        $this->autoRender=false;
        $return=[];
        $query = $this->request->getQuery('q');
         $conditions = [];
            $keys = explode(' ', $query);
            foreach ($keys as $key) {
                $conditions['AND']['OR']['AND'][] = ['Towar.nazwa LIKE' => '%' . $key . '%'];
            }
            $conditions['AND']['OR'][] = ['Towar.kod LIKE' => '%' . $query . '%'];
            $towary = $this->Towar->find('all', ['conditions' => $conditions, 'order' => ['Towar.nazwa ASC'], 'limit' => 50, 'contain' => ['TowarCena', 'Platforma']]);
            $returnItems=[];
            if(!empty($towary) && $towary->count()>0){
                foreach ($towary as $towar){
                    $returnItems[]=['id'=>$towar->id,'text'=>$towar->nazwa.(!empty($towar->platforma)?' ('.$towar->platforma->nazwa.')':'')];
                }
            }
            if(!empty($returnItems)){
                $return=['items'=>$returnItems];
            }
        
        $this->response->type('ajax');
        $this->response->body(json_encode($return));
        return $this->response;
    }
}
