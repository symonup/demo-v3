<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Miasto Controller
 *
 * @property \App\Model\Table\MiastoTable $Miasto
 *
 * @method \App\Model\Entity\Miasto[] paginate($object = null, array $settings = [])
 */
class MiastoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
    $cfgArr=[];
$miasto = $this->Miasto->find('all',$cfgArr);

        $this->set(compact('miasto'));
        $this->set('_serialize', ['miasto']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $miasto = $this->Miasto->newEntity();
        if ($this->request->is('post')) {
            $miasto = $this->Miasto->patchEntity($miasto, $this->request->getData(),['translations'=>$this->Miasto->hasBehavior('Translate')]);
            if ($this->Miasto->save($miasto)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | miasto'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | miasto'))])));
        }
        $this->set(compact('miasto'));
        $this->set('_serialize', ['miasto']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Miasto id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
    
    if($this->Miasto->hasBehavior('Translate')){
        $miasto = $this->Miasto->find('translations', [
            'locales' => $this->lngIdList,
            'conditions' => ['Miasto.'.$this->Miasto->primaryKey() => $id],
            'contain' => []
        ])->first();
        }
        else{
        $miasto = $this->Miasto->find('all', [
            'conditions'=>['Miasto.'.$this->Miasto->primaryKey()=>$id],
            'contain' => []
        ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $miasto = $this->Miasto->patchEntity($miasto, $this->request->getData(),['translations'=>$this->Miasto->hasBehavior('Translate')]);
            if ($this->Miasto->save($miasto)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | miasto'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | miasto'))])));
        }
        
        $_miasto = $miasto->toArray();
        if ($this->Miasto->hasBehavior('Translate') && empty($_miasto['_translations'])) {
            $transFields=$this->Miasto->associations()->keys();
            $tFields=[];
            foreach ($transFields as $field){
                if(strpos($field, '_translation')!==false){
                    $field= substr($field, (strpos($field, '_')+1));
                    $field=str_replace('_translation','', $field);
                    $tFields[$field]=$_miasto[$field]; 
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $miasto->set('_translations',$translation);
        }
        
        
        $this->set(compact('miasto'));
        $this->set('_serialize', ['miasto']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Miasto id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $miasto = $this->Miasto->get($id);
        if ($this->Miasto->delete($miasto)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.',[$this->Txt->printAdmin(__('Admin | miasto'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.',[$this->Txt->printAdmin(__('Admin | miasto'))])));
        }

        return $this->redirect(['action' => 'index']);
    }
}
