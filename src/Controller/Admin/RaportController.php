<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Administrator Controller
 *
 * @property \App\Model\Table\AdministratorTable $Administrator
 *
 * @method \App\Model\Entity\Administrator[] paginate($object = null, array $settings = [])
 */
class RaportController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
            $this->loadModel('Uzytkownik');
        $this->loadModel('Waluta');
        if($this->request->is('post')){
            $this->loadComponent('Order');
            $this->loadModel('Zamowienie');
            $this->loadModel('ZamowienieTowar');
            $rqData=$this->request->getData();
            $from=$rqData['from'].' 00:00:00';
            $to=$rqData['to'].' 23:59:59';
            $waluta=(!empty($rqData['waluta'])?$rqData['waluta']:null);
            switch ($rqData['typ']){
                case 'wg_waluty': {
                    $zamowienia=$this->Zamowienie->find('all',['conditions'=>['Zamowienie.data >='=>$from,'Zamowienie.data <='=>$to,'Zamowienie.waluta_symbol'=>$waluta,'Zamowienie.status IN'=>$rqData['statusy'][$rqData['typ']]]]);
                    $raport=$this->Order->ordersByCurrency($zamowienia,$this->filePath['raporty'],$from,$to,$waluta);
                    if(!empty($raport)){
                        return $this->redirect(str_replace($this->basePath,'/',$this->displayPath['raporty']).$raport);
                    }
                    else{
                        $this->Flash->error($this->Txt->printAdmin(__('Brak danych do wygenerowania w raporcie')));
                    }
                } break;
                case 'towary': {
                    $zamowieniaIds=$this->Zamowienie->find('list',['keyField'=>'id','valueField'=>'id','group'=>['id'],'conditions'=>['Zamowienie.data >='=>$from,'Zamowienie.data <='=>$to,'Zamowienie.waluta_symbol'=>$waluta,'Zamowienie.status IN'=>$rqData['statusy'][$rqData['typ']]]])->toArray();
                    if(!empty($zamowieniaIds)){
                    $towary=$this->Zamowienie->ZamowienieTowar->find('all',['conditions'=>['ZamowienieTowar.zamowienie_id IN'=>$zamowieniaIds]]);
                    $raport=$this->Order->allProducts($towary,$this->filePath['raporty'],$from,$to);
                    if(!empty($raport)){
                        return $this->redirect(str_replace($this->basePath,'/',$this->displayPath['raporty']).$raport);
                    }
                    else{
                        $this->Flash->error($this->Txt->printAdmin(__('Brak danych do wygenerowania w raporcie')));
                    }
                    }
                    else{
                        $this->Flash->error($this->Txt->printAdmin(__('Brak danych do wygenerowania w raporcie')));
                    }
                    } break;
                case 'wg_klienta': {
                    $uzytkownik=$this->Uzytkownik->find('all',['conditions'=>['Uzytkownik.id'=>$rqData['uzytkownik']]])->first();
                    if(!empty($uzytkownik)){
                     $zamowienia=$this->Zamowienie->find('all',['conditions'=>['Zamowienie.data >='=>$from,'Zamowienie.data <='=>$to,'Zamowienie.uzytkownik_id'=>$rqData['uzytkownik'],'Zamowienie.status IN'=>$rqData['statusy'][$rqData['typ']]]]);
                    $raport=$this->Order->ordersByCustomer($zamowienia,$this->filePath['raporty'],$from,$to,$uzytkownik);
                    if(!empty($raport)){
                        return $this->redirect(str_replace($this->basePath,'/',$this->displayPath['raporty']).$raport);
                    }
                    else{
                        $this->Flash->error($this->Txt->printAdmin(__('Brak danych do wygenerowania w raporcie')));
                    }
                    }else{
                        $this->Flash->error($this->Txt->printAdmin(__('Brak użytkownika')));
                    }
                } break;
                case 'towary_klient': {
                    $uzytkownik=$this->Uzytkownik->find('all',['conditions'=>['Uzytkownik.id'=>$rqData['uzytkownik']]])->first();
                    if(!empty($uzytkownik)){
                        $zamowieniaIds=$this->Zamowienie->find('list',['keyField'=>'id','valueField'=>'waluta_symbol','group'=>['id'],'conditions'=>['Zamowienie.data >='=>$from,'Zamowienie.data <='=>$to,'Zamowienie.uzytkownik_id'=>$rqData['uzytkownik'],'Zamowienie.status IN'=>$rqData['statusy'][$rqData['typ']]]])->toArray();
                    if(!empty($zamowieniaIds)){
                    $towary=$this->Zamowienie->ZamowienieTowar->find('all',['conditions'=>['ZamowienieTowar.zamowienie_id IN'=> array_keys($zamowieniaIds)]]);
                    $raport=$this->Order->allProductsUser($towary,$this->filePath['raporty'],$from,$to,$uzytkownik,$zamowieniaIds);
                    if(!empty($raport)){
                        return $this->redirect(str_replace($this->basePath,'/',$this->displayPath['raporty']).$raport);
                    }
                    else{
                        $this->Flash->error($this->Txt->printAdmin(__('Brak danych do wygenerowania w raporcie')));
                    }
                    }
                    else{
                        $this->Flash->error($this->Txt->printAdmin(__('Brak danych do wygenerowania w raporcie')));
                    }
                    }else{
                        $this->Flash->error($this->Txt->printAdmin(__('Brak użytkownika')));
                    }
                } break;
            }
        }
        $waluty=$this->Waluta->find('list',['keyField'=>'symbol','valueField'=>'symbol'])->toArray();;
        $klienci=$this->Uzytkownik->find('list',['keyField'=>'id','valueField'=>'nazwa_klienta'])->select(['id','nazwa_klienta'=>"concat('[',id,']',' ',imie,' ',nazwisko,', ',firma)"])->toArray();
        $statusy=$this->statusy;
        $this->set(compact('waluty','klienci','statusy'));
    }

}
