<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use PHPImageWorkshop\ImageWorkshop;

/**
 * Banner Controller
 *
 * @property \App\Model\Table\BannerTable $Banner
 *
 * @method \App\Model\Entity\Banner[] paginate($object = null, array $settings = [])
 */
class BannerController extends AppController {

    public $components = ['Upload'];

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $slidesTypes = ['file' => $this->Txt->printAdmin(__('Admin | Grafika')), 'html' => $this->Txt->printAdmin(__('Admin | Html'))];
        $slideElementsType = ['file' => $this->Txt->printAdmin(__('Admin | Grafika')), 'html' => $this->Txt->printAdmin(__('Admin | Html'))];
        $this->set(compact('slidesTypes', 'slideElementsType'));
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $banner = $this->paginate($this->Banner);

        $this->set(compact('banner'));
        $this->set('_serialize', ['banner']);
        $tmpView=new \Cake\View\View();
        $tips=[
            'banery'=>$tmpView->element($this->tipsPath.'banery'),
                ];
        $this->set('tips',$tips);
    }

    /**
     * View method
     *
     * @param string|null $id Banner id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $banner = $this->Banner->get($id, [
            'contain' => ['BannerSlides']
        ]);

        $this->set('banner', $banner);
        $this->set('_serialize', ['banner']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $banner = $this->Banner->newEntity();
        if ($this->request->is('post')) {
            $banner = $this->Banner->patchEntity($banner, $this->request->getData());
            if ($this->Banner->save($banner)) {
                $this->Flash->success(__('The data has been saved.'));

                return $this->redirect(['action' => 'slideAdd', $banner->get('id')]);
            }
            $this->Flash->error(__('The data could not be saved. Please, try again.'));
        }
        $this->set(compact('banner'));
        $this->set('_serialize', ['banner']);
    }

    public function slides($bannerId = null) {
        if (!empty($bannerId)) {
            $banner = $this->Banner->find('all', ['conditions' => ['Banner.id' => $bannerId], 'contain' => ['BannerSlides' => ['BannerSlidesElements']]])->first();
            $this->set('banner', $banner);
        } else {
            return $this->redirect(['action' => 'index']);
        }
    }

    public function slideToImg($id = null) {
        $this->autoRender = false;
        $slide = $this->Banner->BannerSlides->find('all', ['conditions' => ['BannerSlides.id' => $id], 'contain' => ['Banner', 'BannerSlidesElements']])->first();
        if (!empty($slide)) {
            // Initialization of layers you need
            $bgLayer = ImageWorkshop::initFromPath($this->filePath['banner_slides'] . $slide->file);
            $elements = [];
            if (!empty($slide->banner_slides_elements)) {
                foreach ($slide->banner_slides_elements as $slideElement) {
                    if ($slideElement->type == 'html') {
                        $cssParams = [];
                        if (!empty($slideElement->css)) {
                            $css = $this->getCss('element{' . $slideElement->css . '}');
                            if (!empty($css)) {
                                foreach ($css['element'] as $cssItem) {
                                    foreach ($cssItem as $tagName => $value) {
                                        $cssParams[$tagName] = trim(str_replace(['#', 'px'], ['', ''], $value));
                                    }
                                }
                            }
                        }
                        $childElements = [];
                        $dom = new \DOMDocument("1.0", 'UTF-8');
                        $dom->loadHTML($slideElement->text);
                        $domx = new \DOMXPath($dom);
                        $domElements = $domx->evaluate('//*');

                        foreach ($domElements as $domElem) {
                            if ($domElem->tagName !== 'html' && $domElem->tagName !== 'body') {
                                $childElementsCssParams = [];
                                if ($domElem->attributes->length > 0) {
                                    foreach ($domElem->attributes as $attr) {
                                        if ($attr->name == 'style') {
                                            $elemCss = $this->getCss('element{' . $attr->textContent . '}');
                                            if (!empty($elemCss)) {
                                                foreach ($elemCss['element'] as $cssItem) {
                                                    foreach ($cssItem as $tagName => $value) {
                                                        $childElementsCssParams[$tagName] = trim(str_replace(['#', 'px'], ['', ''], $value));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                $childElements[] = [
                                    'text' => utf8_decode($domElem->textContent),
                                    'css' => $childElementsCssParams
                                ];
                            }
                        }
                        $text = strip_tags($slideElement->text);
                        if (!empty($childElements)) {
                            $upTo=0;
                            foreach ($childElements as $childElement) {
                                $fontPath = $this->filePath['font'] . 'Roboto-Regular.ttf';
                                if (!empty($cssParams['font-weight'])) {
                                    $fontPath = $this->getFont($cssParams['font-weight']);
                                }
                                $fontSize = 12;
                                if (!empty($cssParams['font-size'])) {
                                    $fontSize = $cssParams['font-size'];
                                }
                                $fontColor = "FFFFFF";
                                if (!empty($cssParams['color'])) {
                                    $fontColor = $cssParams['color'];
                                }
                                $textRotation = 0;
                                $backgroundColor = null; // optionnal
                                if (!empty($cssParams['background'])) {
                                    $backgroundColor = $cssParams['background'];
                                }
                                if (!empty($cssParams['background-color'])) {
                                    $backgroundColor = $cssParams['background-color'];
                                }
                                if (!empty($childElement['text'])) {
                                    if (!empty($childElement['css'])) {
                                        if (!empty($childElement['css']['font-weight'])) {
                                            $fontPath = $this->getFont($childElement['css']['font-weight']);
                                        }
                                        if (!empty($childElement['css']['font-size'])) {
                                            $fontSize = $childElement['css']['font-size'];
                                        }
                                        if (!empty($childElement['css']['color'])) {
                                            $fontColor = $childElement['css']['color'];
                                        }
                                        if (!empty($childElement['css']['background'])) {
                                            $backgroundColor = $childElement['css']['background'];
                                        }
                                        if (!empty($childElement['css']['background-color'])) {
                                            $backgroundColor = $childElement['css']['background-color'];
                                        }
                                    }
                                    $elements[] = ['left' => $slideElement->pos_left, 'top' => $slideElement->pos_top+$upTo, 'layer' => ImageWorkshop::initTextLayer($childElement['text'], $fontPath, $fontSize, $fontColor, $textRotation, $backgroundColor)];
                                $upTo += $fontSize + 15;
                                    
                                        }
                            }
                        } else {
                            $fontPath = $this->filePath['font'] . 'Roboto-Regular.ttf';
                            if (!empty($cssParams['font-weight'])) {
                                $fontPath = $this->getFont($cssParams['font-weight']);
                            }
                            $fontSize = 12;
                            if (!empty($cssParams['font-size'])) {
                                $fontSize = $cssParams['font-size'];
                            }
                            $fontColor = "FFFFFF";
                            if (!empty($cssParams['color'])) {
                                $fontColor = $cssParams['color'];
                            }
                            $textRotation = 0;
                            $backgroundColor = null; // optionnal
                            if (!empty($cssParams['background'])) {
                                $backgroundColor = $cssParams['background'];
                            }
                            if (!empty($cssParams['background-color'])) {
                                $backgroundColor = $cssParams['background-color'];
                            }
                            $elements[] = ['left' => $slideElement->pos_left, 'top' => $slideElement->pos_top, 'layer' => ImageWorkshop::initTextLayer($text, $fontPath, $fontSize, $fontColor, $textRotation, $backgroundColor)];
                            
                        }
                    } else {
                        $elements[] = ['left' => $slideElement->pos_left, 'top' => $slideElement->pos_top, 'layer' => ImageWorkshop::initFromPath($this->filePath['banner_slides_elements'] . $slideElement->file)];
                    }
                }
            }

// Add layers on bg layer
            if (!empty($elements)) {
                foreach ($elements as $element) {
                    $bgLayer->addLayerOnTop($element['layer'], $element['left'], $element['top'], 'MT');
                }
            }
// Saving the result in a folder
            $bgLayer->resizeInPixel(600, null, true);
            $image=$bgLayer->getResult();
            header('Content-type: image/png');
header('Content-Disposition: filename="butterfly.png"');
imagepng($image, null, 8); // We choose to show a PNG (quality of 8 on a scale of 0 to 9)
exit;
//            $bgLayer->save($this->filePath['banner_slides'], "prev_" . $slide->id . ".png", true, null, 95);
        }
    }

    private function getFont($weight = '400') {
        $fontPath = '';
        switch ($weight) {
            case '100' : {
                    $fontPath = $this->filePath['font'] . 'Roboto-Thin.ttf';
                } break;
            case '300' : {
                    $fontPath = $this->filePath['font'] . 'Roboto-Light.ttf';
                } break;
            case '500' : {
                    $fontPath = $this->filePath['font'] . 'Roboto-Medium.ttf';
                } break;
            case '700' : {
                    $fontPath = $this->filePath['font'] . 'Roboto-Bold.ttf';
                } break;
            case '900' : {
                    $fontPath = $this->filePath['font'] . 'Roboto-Black.ttf';
                } break;
            default : {
                    $fontPath = $this->filePath['font'] . 'Roboto-Regular.ttf';
                } break;
        }
        return $fontPath;
    }

    private function getCss($css) {
        preg_match_all('/(?ims)([a-z0-9\s\,\.\:#_\-@]+)\{([^\}]*)\}/', $css, $arr);

        $result = array();
        foreach ($arr[0] as $i => $x) {
            $selector = trim($arr[1][$i]);
            $rules = explode(';', trim($arr[2][$i]));
            $result[$selector] = array();
            foreach ($rules as $strRule) {
                if (!empty($strRule)) {
                    $rule = explode(":", $strRule);
                    $result[$selector][][trim($rule[0])] = trim($rule[1]);
                }
            }
        }
        return $result;
    }

    public function slideAdd($bannerId) {
        $this->loadModel('BannerSlides');
        $bannerSlide = $this->BannerSlides->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $path = $this->filePath['banner_slides'];
            $uploadError = false;
            if (!empty($data['file']['name'])) {
                $fileName = $this->Upload->saveFile(
                        $path, $data['file'], null);
                if (!empty($fileName)) {
                    $data['file'] = $fileName;
                    $image_info = getimagesize($path . $fileName);
                    $image_width = $image_info[0];
                    $data['width'] = $image_width;
                    $data['height'] = $image_info[1];
                } else {
                    $uploadError = true;
                }
            } else {
                $data['file'] = null;
            }
            if (!key_exists('is_linked', $data)) {
                $data['is_linked'] = 0;
            }
            if (!$uploadError) {
                $bannerSlide = $this->BannerSlides->patchEntity($bannerSlide, $data);
                if ($this->BannerSlides->save($bannerSlide)) {
                    $this->Flash->success(__('The data has been saved.'));
                    return $this->redirect(['action' => 'slideElementAdd', $bannerSlide->banner_id, $bannerSlide->id]);
                } else {
                    if (!empty($fileName) && file_exists($path . $fileName)) {
                        $this->Upload->deleteFile($path . $fileName);
                    }
                }
                $this->Flash->error(__('The data could not be saved. Please, try again.'));
            } else {
                $this->Flash->error(__('Admin | Błąd wgrywania pliku'));
            }
        }
        $this->set(compact('bannerSlide', 'bannerId'));
        $this->set('_serialize', ['bannerSlide']);
    }

    public function slideEdit($slideId) {
        $this->loadModel('BannerSlides');
        $bannerSlide = $this->BannerSlides->find('all', ['conditions' => ['BannerSlides.id' => $slideId], 'contain' => 'Banner'])->first();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $path = $this->filePath['banner_slides'];
            $uploadError = false;
            $oldFile = null;
            if (empty($data['file']['name'])) {
                $data['file'] = $bannerSlide->file;
            } else {
                $path = $this->filePath['banner_slides'];
                $fileName = $this->Upload->saveFile(
                        $path, $data['file'], null);
                if (!empty($fileName)) {
                    $data['file'] = $fileName;
                    $oldFile = $bannerSlide->file;
                    $image_info = getimagesize($path . $fileName);
                    $image_width = $image_info[0];
                    $data['width'] = $image_width;
                    $data['height'] = $image_info[1];
                } else {
                    $uploadError = true;
                }
            }
            if (!key_exists('is_linked', $data)) {
                $data['is_linked'] = 0;
            }
            if (!$uploadError) {
                $bannerSlide = $this->BannerSlides->patchEntity($bannerSlide, $data);
                if ($this->BannerSlides->save($bannerSlide)) {
                    if (!empty($oldFile) && file_exists($path . $oldFile)) {
                        $this->Upload->deleteFile($path . $oldFile);
                    }
                    $this->Flash->success(__('The data has been saved.'));
                    return $this->redirect(['action' => 'slideElementAdd', $bannerSlide->banner_id, $bannerSlide->id]);
                } else {
                    if (!empty($fileName) && file_exists($path . $fileName)) {
                        $this->Upload->deleteFile($path . $fileName);
                    }
                }
                $this->Flash->error(__('The data could not be saved. Please, try again.'));
            } else {
                $this->Flash->error(__('Admin | Błąd wgrywania pliku'));
            }
        }
        $bannerId = $bannerSlide->banner_id;
        $this->set(compact('bannerSlide', 'bannerId'));
        $this->set('_serialize', ['bannerSlide']);
    }

    public function slideElementAdd($bannerId, $bannerSlideId) {
        $this->loadModel('BannerSlidesElements');
        $bannerSlideElements = $this->BannerSlidesElements->find('all', ['condiitons' => ['BannerSlidesElements.banner_slide_id' => $bannerSlideId]])->toArray();
        $banner = $this->Banner->find('all', ['conditions' => ['Banner.id' => $bannerId], 'contain' => ['BannerSlides' => ['BannerSlidesElements', 'conditions' => ['BannerSlides.id' => $bannerSlideId]]]])->first();
        if ($this->request->is('post')) {
            $existElementIds = [];
            if (!empty($banner->banner_slides[0]->banner_slides_elements)) {
                foreach ($banner->banner_slides[0]->banner_slides_elements as $exSlideElement) {
                    $existElementIds[$exSlideElement->id] = $exSlideElement->file;
                }
            }
            $data = $this->request->getData();
            $path = $this->filePath['banner_slides_elements'];
            $fileNames = [];
            foreach ($data['banner_slides_elements'] as &$item) {
                $uploadError = false;
                $oldFile = null;
                if (empty($item['file']['name'])) {
                    if (!empty($item['id'])) {
                        if (!empty($existElementIds[$item['id']])) {
                            $item['file'] = $existElementIds[$item['id']];
                        } else {
                            $item['file'] = null;
                        }
                    } else {
                        $item['file'] = null;
                    }
                } else {
                    $fileName = $this->Upload->saveFile(
                            $path, $item['file'], null);
                    if (!empty($fileName)) {
                        $item['file'] = $fileName;
//                    $oldFiles[] = $bannerSlide->file;
                        $fileNames[] = $fileName;
                    } else {
                        $uploadError = true;
                    }
                }
                $tmpId = null;
                if (!empty($item['id'])) {
                    $tmpId = $item['id'];
                }
                $item = $this->BannerSlidesElements->newEntity($item);
                if (!empty($tmpId)) {
                    $item->id = $tmpId;
                    unset($existElementIds[$tmpId]);
                }
                $fileName = null;
            }
            $data = ['banner_slides' => [$data]];
            $banner = $this->Banner->patchEntity($banner, $data);
            if ($this->Banner->save($banner)) {
                if (!empty($existElementIds)) {
                    $this->BannerSlidesElements->deleteAll(['id IN' => array_keys($existElementIds)]);
                    foreach ($existElementIds as $exItemFile) {
                        if (!empty($exItemFile)) {
                            $this->Upload->deleteFile($path . $exItemFile);
                        }
                    }
                }
                $this->Flash->success(__('The data has been saved.'));
                return $this->redirect(['action' => 'slides', $banner->id]);
            }
            $this->Flash->error(__('The data could not be saved. Please, try again.'));
        }
        $this->set(compact('bannerSlideElements', 'bannerSlideId', 'banner'));
        $this->set('_serialize', ['bannerSlideElements']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Banner id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $banner = $this->Banner->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $banner = $this->Banner->patchEntity($banner, $data);
            if ($this->Banner->save($banner)) {
                $this->Flash->success(__('The data has been saved.'));
                if (!empty(key_exists('next-step', $data))) {
                    return $this->redirect(['action' => 'slides', $banner->get('id')]);
                } else {
                    return $this->redirect(['action' => 'index']);
                }
            }
            $this->Flash->error(__('The data could not be saved. Please, try again.'));
        }
        $this->set(compact('banner'));
        $this->set('_serialize', ['banner']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Banner id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $banner = $this->Banner->find('all', ['conditions' => ['Banner.id' => $id], 'contain' => ['BannerSlides' => ['BannerSlidesElements' => ['conditions' => ['BannerSlidesElements.type' => 'file']]]]])->first();
        if (!empty($banner)) {
            $filesElementsToDelete = [];
            $filesSlidesToDelete = [];
            if (!empty($banner->banner_slides)) {
                foreach ($banner->banner_slides as $slide) {
                    if (!empty($slide->file)) {
                        $filesSlidesToDelete[] = $slide->file;
                    }
                    if (!empty($slide->banner_slides_elements)) {
                        foreach ($slide->banner_slides_elements as $slideElement) {
                            if (!empty($slideElement->file)) {
                                $filesElementsToDelete[] = $slideElement->file;
                            }
                        }
                    }
                }
            }
            if ($this->Banner->delete($banner)) {
                if (!empty($filesSlidesToDelete)) {
                    foreach ($filesSlidesToDelete as $file) {
                        $this->Upload->deleteFile($this->filePath['banner_slides'] . $file);
                    }
                }
                if (!empty($filesElementsToDelete)) {
                    foreach ($filesElementsToDelete as $file) {
                        $this->Upload->deleteFile($this->filePath['banner_slides_elements'] . $file);
                    }
                }
                $this->Flash->success(__('The data has been deleted.'));
            } else {
                $this->Flash->error(__('The data could not be deleted. Please, try again.'));
            }
        } else {
            $this->Flash->error(__('The data could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function deleteSlide($slideId = null) {
        $bannerId = null;
        $slide = $this->Banner->BannerSlides->find('all', ['conditions' => ['BannerSlides.id' => $slideId], 'contain' => ['BannerSlidesElements' => ['conditions' => ['BannerSlidesElements.type' => 'file']]]])->first();
        if (!empty($slide)) {
            $bannerId = $slide->banner_id;
            $filesElementsToDelete = [];
            $filesToDelete = [];
            if (!empty($slide->file)) {
                $filesToDelete[] = $slide->file;
            }
            if (!empty($slide->banner_slides_elements)) {
                foreach ($slide->banner_slides_elements as $slideElement) {
                    if (!empty($slideElement->file)) {
                        $filesElementsToDelete[] = $slideElement->file;
                    }
                }
            }
            if ($this->Banner->BannerSlides->delete($slide)) {

                if (!empty($filesToDelete)) {
                    foreach ($filesToDelete as $file) {
                        $this->Upload->deleteFile($this->filePath['banner_slides'] . $file);
                    }
                }
                if (!empty($filesElementsToDelete)) {
                    foreach ($filesElementsToDelete as $file) {
                        $this->Upload->deleteFile($this->filePath['banner_slides_elements'] . $file);
                    }
                }
            } else {
                $this->Flash->error(__('The data could not be deleted. Please, try again.'));
            }
        } else {
            $this->Flash->error(__('The data could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'slides', $bannerId]);
    }

}
