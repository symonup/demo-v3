<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Wzor Controller
 *
 * @property \App\Model\Table\WzorTable $Wzor
 *
 * @method \App\Model\Entity\Wzor[] paginate($object = null, array $settings = [])
 */
class WzorController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
    $cfgArr=[];
$wzor = $this->Wzor->find('all',$cfgArr);

        $this->set(compact('wzor'));
        $this->set('_serialize', ['wzor']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $wzor = $this->Wzor->newEntity();
        if ($this->request->is('post')) {
        $rqData=$this->request->getData();
            $wzor = $this->Wzor->patchEntity($wzor, $rqData,['translations'=>$this->Wzor->hasBehavior('Translate')]);
            if ($this->Wzor->save($wzor)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | wzor'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | wzor'))])));
        }
        $this->set(compact('wzor'));
        $this->set('_serialize', ['wzor']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Wzor id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
    
    if($this->Wzor->hasBehavior('Translate')){
        $wzor = $this->Wzor->find('translations', [
            'locales' => $this->lngIdList,
            'conditions' => ['Wzor.'.$this->Wzor->primaryKey() => $id],
            'contain' => []
        ])->first();
        }
        else{
        $wzor = $this->Wzor->find('all', [
            'conditions'=>['Wzor.'.$this->Wzor->primaryKey()=>$id],
            'contain' => []
        ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData=$this->request->getData();
            $wzor = $this->Wzor->patchEntity($wzor, $rqData,['translations'=>$this->Wzor->hasBehavior('Translate')]);
            if ($this->Wzor->save($wzor)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | wzor'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | wzor'))])));
        }
        
        $_wzor = $wzor->toArray();
        if ($this->Wzor->hasBehavior('Translate') && empty($_wzor['_translations'])) {
            $transFields=$this->Wzor->associations()->keys();
            $tFields=[];
            foreach ($transFields as $field){
                if(strpos($field, '_translation')!==false){
                    $field= substr($field, (strpos($field, '_')+1));
                    $field=str_replace('_translation','', $field);
                    $tFields[$field]=$_wzor[$field]; 
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $wzor->set('_translations',$translation);
        }
        
        
        $this->set(compact('wzor'));
        $this->set('_serialize', ['wzor']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Wzor id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $wzor = $this->Wzor->get($id);
        if ($this->Wzor->delete($wzor)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.',[$this->Txt->printAdmin(__('Admin | wzor'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.',[$this->Txt->printAdmin(__('Admin | wzor'))])));
        }

        return $this->redirect(['action' => 'index']);
    }
}
