<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Wersja Controller
 *
 * @property \App\Model\Table\WersjaTable $Wersja
 *
 * @method \App\Model\Entity\Wersja[] paginate($object = null, array $settings = [])
 */
class WersjaController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
    $cfgArr=[];
$wersja = $this->Wersja->find('all',$cfgArr);

        $this->set(compact('wersja'));
        $this->set('_serialize', ['wersja']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $wersja = $this->Wersja->newEntity();
        if ($this->request->is('post')) {
        $rqData=$this->request->getData();
            $wersja = $this->Wersja->patchEntity($wersja, $rqData,['translations'=>$this->Wersja->hasBehavior('Translate')]);
            if ($this->Wersja->save($wersja)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | wersja'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | wersja'))])));
        }
        $this->set(compact('wersja'));
        $this->set('_serialize', ['wersja']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Wersja id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
    
    if($this->Wersja->hasBehavior('Translate')){
        $wersja = $this->Wersja->find('translations', [
            'locales' => $this->lngIdList,
            'conditions' => ['Wersja.'.$this->Wersja->primaryKey() => $id],
            'contain' => []
        ])->first();
        }
        else{
        $wersja = $this->Wersja->find('all', [
            'conditions'=>['Wersja.'.$this->Wersja->primaryKey()=>$id],
            'contain' => []
        ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData=$this->request->getData();
            $wersja = $this->Wersja->patchEntity($wersja, $rqData,['translations'=>$this->Wersja->hasBehavior('Translate')]);
            if ($this->Wersja->save($wersja)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | wersja'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | wersja'))])));
        }
        
        $_wersja = $wersja->toArray();
        if ($this->Wersja->hasBehavior('Translate') && empty($_wersja['_translations'])) {
            $transFields=$this->Wersja->associations()->keys();
            $tFields=[];
            foreach ($transFields as $field){
                if(strpos($field, '_translation')!==false){
                    $field= substr($field, (strpos($field, '_')+1));
                    $field=str_replace('_translation','', $field);
                    $tFields[$field]=$_wersja[$field]; 
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $wersja->set('_translations',$translation);
        }
        
        
        $this->set(compact('wersja'));
        $this->set('_serialize', ['wersja']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Wersja id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $wersja = $this->Wersja->get($id);
        if ($this->Wersja->delete($wersja)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.',[$this->Txt->printAdmin(__('Admin | wersja'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.',[$this->Txt->printAdmin(__('Admin | wersja'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

/**
     * setField method
     *
     * @param string|null $id Wersja id.
     * @param string|null $field Wersja field name.
     * @param mixed $value value to set.
     */

public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->Wersja->find('all', ['conditions' => ['Wersja.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                        $item->{$field} = $value;
                        if ($this->Wersja->save($item)) {
                            $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, (!empty($value)?0:1)])), 'action' => $retAction];
                        } else {
                            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                        }
                    
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    /**
* function sort
* 
*/
public function sort($field='kolejnosc'){
        $this->autoRender=false;
        $returnData=[];
        if($this->request->is('post')){
            $rqData=$this->request->getData();
            foreach ($rqData as $data){
                if(!empty($data['id'])){
                    $this->Wersja->updateAll($data, ['id'=>$data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    }
