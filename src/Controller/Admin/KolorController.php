<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Kolor Controller
 *
 * @property \App\Model\Table\KolorTable $Kolor
 *
 * @method \App\Model\Entity\Kolor[] paginate($object = null, array $settings = [])
 */
class KolorController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
    $cfgArr=[];
$kolor = $this->Kolor->find('all',$cfgArr);

        $this->set(compact('kolor'));
        $this->set('_serialize', ['kolor']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $kolor = $this->Kolor->newEntity();
        if ($this->request->is('post')) {
        $rqData=$this->request->getData();
            $kolor = $this->Kolor->patchEntity($kolor, $rqData,['translations'=>$this->Kolor->hasBehavior('Translate')]);
            if ($this->Kolor->save($kolor)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | kolor'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | kolor'))])));
        }
        $this->set(compact('kolor'));
        $this->set('_serialize', ['kolor']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Kolor id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
    
    if($this->Kolor->hasBehavior('Translate')){
        $kolor = $this->Kolor->find('translations', [
            'locales' => $this->lngIdList,
            'conditions' => ['Kolor.'.$this->Kolor->primaryKey() => $id],
            'contain' => []
        ])->first();
        }
        else{
        $kolor = $this->Kolor->find('all', [
            'conditions'=>['Kolor.'.$this->Kolor->primaryKey()=>$id],
            'contain' => []
        ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData=$this->request->getData();
            $kolor = $this->Kolor->patchEntity($kolor, $rqData,['translations'=>$this->Kolor->hasBehavior('Translate')]);
            if ($this->Kolor->save($kolor)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | kolor'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | kolor'))])));
        }
        
        $_kolor = $kolor->toArray();
        if ($this->Kolor->hasBehavior('Translate') && empty($_kolor['_translations'])) {
            $transFields=$this->Kolor->associations()->keys();
            $tFields=[];
            foreach ($transFields as $field){
                if(strpos($field, '_translation')!==false){
                    $field= substr($field, (strpos($field, '_')+1));
                    $field=str_replace('_translation','', $field);
                    $tFields[$field]=$_kolor[$field]; 
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $kolor->set('_translations',$translation);
        }
        
        
        $this->set(compact('kolor'));
        $this->set('_serialize', ['kolor']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Kolor id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $kolor = $this->Kolor->get($id);
        if ($this->Kolor->delete($kolor)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.',[$this->Txt->printAdmin(__('Admin | kolor'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.',[$this->Txt->printAdmin(__('Admin | kolor'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

/**
     * setField method
     *
     * @param string|null $id Kolor id.
     * @param string|null $field Kolor field name.
     * @param mixed $value value to set.
     */

public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->Kolor->find('all', ['conditions' => ['Kolor.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                        $item->{$field} = $value;
                        if ($this->Kolor->save($item)) {
                            $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, !(bool) $value])), 'action' => $retAction];
                        } else {
                            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                        }
                    
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }/**
* function sort
* 
*/
public function sort($field='kolejnosc'){
        $this->autoRender=false;
        $returnData=[];
        if($this->request->is('post')){
            $rqData=$this->request->getData();
            foreach ($rqData as $data){
                if(!empty($data['id'])){
                    $this->Kolor->updateAll($data, ['id'=>$data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }}
