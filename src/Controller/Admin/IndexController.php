<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Administrator Controller
 *
 * @property \App\Model\Table\AdministratorTable $Administrator
 *
 * @method \App\Model\Entity\Administrator[] paginate($object = null, array $settings = [])
 */
class IndexController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $this->loadModel('Uzytkownik');
        $this->loadModel('Zamowienie');
        $usersCount = $this->Uzytkownik->find('all')->count();
        $lastTimeUsersCount = $this->Uzytkownik->find('all', ['conditions' => ['Uzytkownik.data_rejestracji >=' => date('Y-m-d 00:00:00', strtotime('- 7 days'))]])->count();
        $unactiveUsersCount = $this->Uzytkownik->find('all', ['conditions' => ['Uzytkownik.aktywny IN' => [null, 0]]])->count();
        $allOrdersCount = $this->Zamowienie->find('all')->count();
        $lastOrders = $this->Zamowienie->find('all', ['conditions' => ['Zamowienie.data >=' => date('Y-m-d 00:00:00', strtotime('- 7 days'))],'order'=>['Zamowienie.id'=>'desc']]);
        $lastOrdersCount = $lastOrders->count();
        $oczekujaceOrders = $this->Zamowienie->find('all', ['conditions' => ['Zamowienie.status' => 'zlozone']]);
        $oczekujaceOrdersCount = $oczekujaceOrders->count();
        $statusy = $this->statusy;
        $ordersByDays = [];
        $dateFrom = date('Y-m-d', strtotime('- 7 days'));
        $dateTo = date('Y-m-d');
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            $dateFrom = $rqData['from'];
            $dateTo = $rqData['to'];
            $ordersToStat = $this->Zamowienie->find('all', ['conditions' => ['Zamowienie.data >=' => $dateFrom . ' 00:00:00', 'Zamowienie.data <=' => $dateTo . ' 23:59:59']]);
        } else {
            $ordersToStat = $lastOrders;
        }
        foreach ($ordersToStat as $order) {
            if (!key_exists($order->data->format('Y-m-d'), $ordersByDays)) {
                $ordersByDays[$order->data->format('Y-m-d')] = 1;
            } else
                $ordersByDays[$order->data->format('Y-m-d')] ++;
        }
        $statystykiArray = [];
        $dates = $this->Txt->date_range($dateFrom, $dateTo);
        $maxWykresCount = 0;
        foreach ($dates as $date) {
            $statystykiArray[] = [
                'y' => date('Y', strtotime($date)),
                'm' => date('n', strtotime($date)),
                'd' => date('j', strtotime($date)),
                'count' => (!empty($ordersByDays[$date]) ? $ordersByDays[$date] : 0)
            ];
            if (!empty($ordersByDays[$date]) && $ordersByDays[$date] > $maxWykresCount) {
                $maxWykresCount = $ordersByDays[$date];
            }
        }
        $statystykiArrayToJs = json_encode($statystykiArray);
        $this->set(compact('usersCount', 'lastTimeUsersCount', 'unactiveUsersCount', 'allOrdersCount', 'lastOrdersCount', 'oczekujaceOrdersCount', 'lastOrders', 'statusy', 'oczekujaceOrders', 'statystykiArrayToJs', 'maxWykresCount', 'dateFrom', 'dateTo'));
    }

    public function login() {
        if ($this->request->is('post')) {
            $administrator = $this->Auth->identify();
            if ($administrator) {
                $this->Auth->setUser($administrator);
                $aUrl = $this->Auth->redirectUrl();
                $admRd = (($aUrl == '/' || $aUrl == '/admin/' || $aUrl == '/admin') ? ['controller' => 'Index', 'action' => 'index', 'prefix' => 'admin'] : $aUrl);
                return $this->redirect($admRd);
            }
            $this->Flash->error(__('Admin | Nieprawidłowa nazwa użytkownika lub hasło'));
        }
        if ($this->Auth->isAuthorized()) {
            return $this->redirect(['action' => 'index']);
        }
        $this->viewBuilder()->layout('clear');
    }

    public function logout() {
        $this->Flash->success(__('Admin | Zostałeś wylogowany'));
        return $this->redirect($this->Auth->logout());
    }

    public function checkAlerts() {
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $warnings = [];
        $this->loadModel('Powiadomienia');
        $this->loadModel('Zapytania');
        $this->loadModel('CallBack');
        $allAlerts=0;
            $zapytaniaODostepnoscCount = $this->Powiadomienia->find('all')->where(['Powiadomienia.typ' => 'dostepnosc', 'Powiadomienia.wyslane' => 0])->count();
        $zapytaniaOPremieryCount = $this->Powiadomienia->find('all')->where(['Powiadomienia.typ' => 'dostepnosc_premiera', 'Powiadomienia.wyslane' => 0])->count();
        $naZamowienieCount = $this->Zapytanie->find('all')->where(['Zapytanie.data_wyslania IS NULL'])->count();
        $callBackCount = $this->CallBack->find('all')->where(['CallBack.status'=>0])->count();
        $allAlerts=($zapytaniaODostepnoscCount+$zapytaniaOPremieryCount+$naZamowienieCount+$callBackCount);
        
        if($zapytaniaODostepnoscCount>0){
            $warnings['dostepnosc']=$zapytaniaODostepnoscCount;
        }
        if($zapytaniaOPremieryCount>0){
            $warnings['premiery']=$zapytaniaOPremieryCount;
        }
        if($naZamowienieCount>0){
            $warnings['zapytania']=$naZamowienieCount;
        }
        if($callBackCount > 0){
            $warnings['call_back']=$callBackCount;
        }
        $view = new \Cake\View\View();
        $view->layout = 'ajax';
        $view->set(compact('warnings'));
        $html = $view->render('Admin/Index/check_alerts');

        $this->response->type('json');
        $this->response->body(json_encode(['alerts' => $allAlerts, 'html' => $html]));
        return $this->response;
    }

    public function alerts() {
        $allAlerts = [];

        $this->loadModel('Uzytkownik');
        $this->loadModel('Zamowienie');
        $this->loadModel('CallBack');
        $uzytkownicy=$this->Uzytkownik->find('all',['conditions'=>['Uzytkownik.aktywny'=>0]]);
        if($uzytkownicy->count()>0){
            foreach ($uzytkownicy as $uzytkownik){
                $allAlerts['uzytkownicy'][$uzytkownik->id]=$uzytkownik->imie.' '.$uzytkownik->nazwisko.' '.$uzytkownik->firma;
            }
        }
        $zamowieniaOczekujace=$this->Zamowienie->find('all',['conditions'=>['Zamowienie.status'=>'zlozone']]);
        if($zamowieniaOczekujace->count()>0){
            foreach ($zamowieniaOczekujace as $zamO){
                $allAlerts['zamowienia_oczekujace'][$zamO->id]=$zamO->data->format('Y-m-d');
            }
        }
        $zamowieniaNieWyslane=$this->Zamowienie->find('all',['conditions'=>['Zamowienie.status'=>'przyjete','Zamowienie.termin_wysylki <'=>date('Y-m-d')]]);
        if($zamowieniaNieWyslane->count()>0){
            foreach ($zamowieniaNieWyslane as $zamN){
                $allAlerts['zamowienia_niewyslane'][$zamN->id]=(!empty($zamN->termin_wysylki)?$zamN->termin_wysylki->format('Y-m-d'):'');
            }
        }
        $callBacks=$this->CallBack->find('all',['conditions'=>['CallBack.status'=>0]]);
        if($callBacks->count()>0){
            foreach ($callBacks as $callItem){
                $allAlerts['call_back'][$callItem->id]=(!empty($callItem->telefon)?$callItem->telefon:'');
            }
        }
        $this->set(compact('allAlerts'));
    }

    public function page($page){
        $this->autoRender=false;
        $urlData=$this->request->getData();
        $this->session->write('lastPage.'.$urlData['path'],['url'=>$urlData['url'],'page'=>$page,'filter'=>$urlData['filter']]);
    }
}
