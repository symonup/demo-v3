<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Template Controller
 *
 *
 * @method \App\Model\Entity\Template[] paginate($object = null, array $settings = [])
 */
class TemplateController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
    $cfgArr=[];
$template = $this->Template->find('all',$cfgArr);

        $this->set(compact('template'));
        $this->set('_serialize', ['template']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $template = $this->Template->newEntity();
        if ($this->request->is('post')) {
        $rqData=$this->request->getData();
            $template = $this->Template->patchEntity($template, $rqData,['translations'=>$this->Template->hasBehavior('Translate')]);
            if ($this->Template->save($template)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | template'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | template'))])));
        }
        $this->set(compact('template'));
        $this->set('_serialize', ['template']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Template id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
    
    if($this->Template->hasBehavior('Translate')){
        $template = $this->Template->find('translations', [
            'locales' => $this->lngIdList,
            'conditions' => ['Template.'.$this->Template->primaryKey() => $id],
            'contain' => []
        ])->first();
        }
        else{
        $template = $this->Template->find('all', [
            'conditions'=>['Template.'.$this->Template->primaryKey()=>$id],
            'contain' => []
        ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData=$this->request->getData();
            $template = $this->Template->patchEntity($template, $rqData,['translations'=>$this->Template->hasBehavior('Translate')]);
            if ($this->Template->save($template)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | template'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | template'))])));
        }
        
        $_template = $template->toArray();
        if ($this->Template->hasBehavior('Translate') && empty($_template['_translations'])) {
            $transFields=$this->Template->associations()->keys();
            $tFields=[];
            foreach ($transFields as $field){
                if(strpos($field, '_translation')!==false){
                    $field= substr($field, (strpos($field, '_')+1));
                    $field=str_replace('_translation','', $field);
                    $tFields[$field]=$_template[$field]; 
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $template->set('_translations',$translation);
        }
        
        
        $this->set(compact('template'));
        $this->set('_serialize', ['template']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Template id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $template = $this->Template->get($id);
        if ($this->Template->delete($template)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.',[$this->Txt->printAdmin(__('Admin | template'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.',[$this->Txt->printAdmin(__('Admin | template'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

/**
     * setField method
     *
     * @param string|null $id Template id.
     * @param string|null $field Template field name.
     * @param mixed $value value to set.
     */

public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->Template->find('all', ['conditions' => ['Template.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                        $item->{$field} = $value;
                        if ($this->Template->save($item)) {
                            $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, !(bool) $value])), 'action' => $retAction];
                        } else {
                            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                        }
                    
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    /**
* function sort
* 
*/
public function sort($field='kolejnosc'){
        $this->autoRender=false;
        $returnData=[];
        if($this->request->is('post')){
            $rqData=$this->request->getData();
            foreach ($rqData as $data){
                if(!empty($data['id'])){
                    $this->Template->updateAll($data, ['id'=>$data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    }
