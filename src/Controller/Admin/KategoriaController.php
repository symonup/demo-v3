<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Cache\Cache;

/**
 * Kategoria Controller
 *
 * @property \App\Model\Table\KategoriaTable $Kategoria
 *
 * @method \App\Model\Entity\Kategorium[] paginate($object = null, array $settings = [])
 */
class KategoriaController extends AppController {

    private $path = [];
    private $pathIds = [];

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->loadComponent('Upload');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index($id = null) {
        $cfgArr = [
            'conditions' => ['Kategoria.parent_id IS' => $id],
            'order'=>['Kategoria.kolejnosc'=>'desc']
        ];
        $kategoria = $this->Kategoria->find('all', $cfgArr);
        $paths = [];
        if (!empty($id)) {
            $paths = $this->Kategoria->find('path', ['for' => $id]);
        }
        $crumbs = [['name' => $this->Txt->printAdmin(__('Admin | Główne kategorie')), 'url' => \Cake\Routing\Router::url(['action' => 'index'])]];
        if (!empty($paths)) {
            foreach ($paths as $path) {
                $crumbs[] = ['name' => $path->nazwa, 'url' => \Cake\Routing\Router::url(['action' => 'index', $path->id])];
            }
        }
        $this->set(compact('kategoria', 'crumbs', 'id'));
        $this->set('_serialize', ['kategoria']);
    }

    public function mapowanie() {
        $allKats = $this->Kategoria->find('all', ['contain' => ['KategoriaMap']])->order(['Kategoria.sciezka' => 'ASC']);
        
        
//            'nazwa_ikonka'
//            'nazwa_centralazabawek'
//            'nazwa_gatito'
//            'nazwa_molos'
//            'nazwa_ptakmodahurt'
//            'nazwa_vmp'
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            if (!empty($rqData['kategoria_map'])) {
                foreach ($allKats as $kategoria) {
                    $allDataToSave = [];
                    $allKatsOldIds = $this->Kategoria->KategoriaMap->find('list', ['keyField' => 'id', 'valueField' => 'id'])->where(['kategoria_id' => $kategoria->id])->toArray();
                    if (key_exists($kategoria->id, $rqData['kategoria_map'])) {
                        foreach ($rqData['kategoria_map'][$kategoria->id] as $item) {
                            if (!empty($item['id']) && !empty($allKatsOldIds) && key_exists($item['id'], $allKatsOldIds)) {
                                unset($allKatsOldIds[$item['id']]);
                            }
                            $allDataToSave[] = $item;
                        }
                        $kategoria = $this->Kategoria->patchEntity($kategoria, ['kategoria_map' => $allDataToSave]);
                        if ($this->Kategoria->save($kategoria)) {
                            if (!empty($allKatsOldIds)) {
                                $this->Kategoria->KategoriaMap->deleteAll(['id IN' => $allKatsOldIds]);
                            }
                        }
                    } else {
                        if (!empty($allKatsOldIds)) {
                            $this->Kategoria->KategoriaMap->deleteAll(['id IN' => $allKatsOldIds]);
                        }
                    }
                }
            }
            $this->Flash->success($this->Txt->printAdmin(__('Admin | Dane zostały zapisane')));
            return $this->redirect(['action' => 'mapowanie']);
        }
        $hurtownie=$this->hurtownie;
        $this->set(compact('allKats', 'hurtownie'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($id = null) {
        $kategorium = $this->Kategoria->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['data'] = new \DateTime(date('Y-m-d H:i:s'));

            $deny = false;
            if (!empty($data['promuj']) && empty($data['parent_id'])) {
                $checkPromuj = $this->Kategoria->find()->where(['Kategoria.parent_id IS' => NULL, 'Kategoria.promuj' => 1])->count();
                if ($checkPromuj >= 3) {
                    $deny = $this->Txt->printAdmin(__('Admin | Nie można włączyć promowania! Na stronie głównej mogą być promowane tylko 2 kategorie główne.'));
                }
            }
            if ($deny) {
                $data['promuj'] = 0;
            }
            $path = $this->filePath['kategoria'];
            if (!empty($this->request->data['zdjecie']['name'])) {
                $fileName = $this->Upload->saveFile(
                        $path, $this->request->data['zdjecie']);
                if (!empty($fileName)) {
                    $data['zdjecie'] = $fileName;
                } else {
                    $data['zdjecie'] = null;
                }
            } else
                $data['zdjecie'] = null;

            if (empty($data['parent_id']))
                $data['sciezka'] = '/' . $data['_translations'][$this->defLngSymbol]['nazwa'];
            else {
                $sciezka = $this->getPath($data['parent_id']);
                $sciezka = array_reverse($sciezka);
                $sciezka[] = $data['_translations'][$this->defLngSymbol]['nazwa'];
                $data['sciezka'] = '/' . join('/', $sciezka);
            }
            $data['kolejnosc'] = 0;
            $kategorium = $this->Kategoria->patchEntity($kategorium, $data, ['translations' => $this->Kategoria->hasBehavior('Translate')]);
            if ($this->Kategoria->save($kategorium)) {
                $this->clear_cache();
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | kategorium'))])));
                if ($deny) {
                    $this->Flash->error($deny);
                }
                return $this->redirect(['action' => 'index', $kategorium->parent_id]);
            } else {
                if (!empty($fileName)) {
                    $this->Upload->deleteFile($path . $fileName);
                }
                $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | kategorium'))])));
            }
        }
        $parentKategoria = $this->Kategoria->ParentKategoria->find('list', ['keyField' => 'id', 'valueField' => 'sciezka', 'order' => ['sciezka' => 'asc']])->toArray();
        $this->set(compact('kategorium', 'parentKategoria', 'id'));
        $this->set('_serialize', ['kategorium']);
        $this->loadModel('GoogleKategorie');
        $googleKat = $this->GoogleKategorie->find('list', ['keyField' => 'id', 'valueField' => 'nazwa', 'order' => ['GoogleKategorie.nazwa' => 'ASC']]);
        $this->set('googleKat', $googleKat);
    }

    /**
     * Edit method
     *
     * @param string|null $id Kategorium id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {

        if ($this->Kategoria->hasBehavior('Translate')) {
            $kategorium = $this->Kategoria->find('translations', [
                        'locales' => $this->lngIdList,
                        'conditions' => ['Kategoria.' . $this->Kategoria->primaryKey() => $id],
                        'contain' => ['Promocja' => ['TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]]]]
                    ])->first();
        } else {
            $kategorium = $this->Kategoria->find('all', [
                        'conditions' => ['Kategoria.' . $this->Kategoria->primaryKey() => $id],
                        'contain' => ['Promocja' => ['TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]]]]
                    ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $oldPromuj = $kategorium->promuj;
            $deny = false;
            if (!empty($data['promuj']) && empty($oldPromuj) && empty($data['parent_id'])) {
                $checkPromuj = $this->Kategoria->find()->where(['Kategoria.parent_id IS' => NULL, 'Kategoria.id NOT IN' => [$kategorium->id], 'Kategoria.promuj' => 1])->count();
                if ($checkPromuj >= 3) {
                    $deny = $this->Txt->printAdmin(__('Admin | Nie można włączyć promowania! Na stronie głównej mogą być promowane tylko 2 kategorie główne.'));
                }
            }
            if ($deny) {
                $data['promuj'] = 0;
            }
            $oldFile = null;
            if (empty($data['zdjecie']['name'])) {
                $data['zdjecie'] = $kategorium->zdjecie;
            } else {
                $path = $this->filePath['kategoria'];
                $fileName = $this->Upload->saveFile(
                        $path, $data['zdjecie']);
                if (!empty($fileName)) {
                    $data['zdjecie'] = $fileName;
                    $oldFile = $kategorium->zdjecie;
                }
            }
            $oldFileIkona = null;
            if (empty($data['ikona']['name'])) {
                $data['ikona'] = $kategorium->ikona;
            } else {
                $path = $this->filePath['kategoria_ikony'];
                $fileNameIkona = $this->Upload->saveFile(
                        $path, $data['ikona']);
                if (!empty($fileNameIkona)) {
                    $data['ikona'] = $fileNameIkona;
                    $oldFileIkona = $kategorium->ikona;
                }
            }
            if (empty($data['parent_id']))
                $data['sciezka'] = '/' . $data['_translations'][$this->defLngSymbol]['nazwa'];
            else {
                $sciezka = $this->getPath($data['parent_id']);
                $sciezka = array_reverse($sciezka);
                $sciezka[] = $data['_translations'][$this->defLngSymbol]['nazwa'];
                $data['sciezka'] = '/' . join('/', $sciezka);
            }
            $kategorium = $this->Kategoria->patchEntity($kategorium, $data, ['translations' => $this->Kategoria->hasBehavior('Translate')]);
            if ($this->Kategoria->save($kategorium)) {
                if (!empty($oldFile))
                    $this->Upload->deleteFile($path . $oldFile);
                $this->updatePath($kategorium->id);
                $this->clear_cache();
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | kategorium'))])));
                if ($deny) {
                    $this->Flash->error($deny);
                }
                return $this->redirect(['action' => 'index', $kategorium->parent_id]);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | kategorium'))])));
        }

        $_kategorium = $kategorium->toArray();
        if ($this->Kategoria->hasBehavior('Translate') && empty($_kategorium['_translations'])) {
            $transFields = $this->Kategoria->associations()->keys();
            $tFields = [];
            foreach ($transFields as $field) {
                if (strpos($field, '_translation') !== false) {
                    $field = substr($field, (strpos($field, '_') + 1));
                    $field = str_replace('_translation', '', $field);
                    $tFields[$field] = $_kategorium[$field];
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $kategorium->set('_translations', $translation);
        }
        $notInIds = [$id];
        $children = $this->Kategoria->find('children', ['for' => $id])->toArray();
        if (!empty($children)) {
            foreach ($children as $child) {
                $notInIds[] = $child->id;
            }
        }
        $parentKategoria = $this->Kategoria->ParentKategoria->find('list', ['keyField' => 'id', 'valueField' => 'sciezka', 'order' => ['sciezka' => 'asc'], 'conditions' => ['ParentKategoria.id NOT IN' => $notInIds]])->toArray();
        $this->set(compact('kategorium', 'parentKategoria'));
        $this->set('_serialize', ['kategorium']);
        $this->loadModel('GoogleKategorie');
        $googleKat = $this->GoogleKategorie->find('list', ['keyField' => 'id', 'valueField' => 'nazwa', 'order' => ['GoogleKategorie.nazwa' => 'ASC']]);
        $this->set('googleKat', $googleKat);
    }

    /**
     * Delete method
     *
     * @param string|null $id Kategorium id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $kategorium = $this->Kategoria->get($id);
        if ($this->Kategoria->delete($kategorium)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.', [$this->Txt->printAdmin(__('Admin | kategorium'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.', [$this->Txt->printAdmin(__('Admin | kategorium'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

    private function getPath($kategoriaId = null) {
        if (empty($kategoriaId))
            return $this->path;
        $kategoria = $this->Kategoria->get($kategoriaId);
        $this->path[] = $kategoria->nazwa;
        if ($kategoria->parent_id == $kategoria->id)
            return $this->path;
        return $this->getPath($kategoria->parent_id);
    }

    private function getPathId($kategoriaId = null) {
        if (empty($kategoriaId))
            return $this->pathIds;
        $kategoria = $this->Kategoria->get($kategoriaId);
        $this->pathIds[(string) $kategoria->id]['nazwa'] = $kategoria->nazwa;
        $this->pathIds[(string) $kategoria->id]['id'] = $kategoria->id;
        return $this->getPathId($kategoria->parent_id);
    }

    private function updatePath($kategoriaId) {
        $kategorie = $this->Kategoria->find('all', ['conditions' => ['Kategoria.parent_id' => $kategoriaId]]);
        if (!$kat = $kategorie->toArray())
            return false;
        foreach ($kategorie as $kategoria) {
            $this->path = [];
            $this->getPath($kategoria->parent_id);
            $sciezka = array_reverse($this->path);
            $sciezka[] = $kategoria->nazwa;
            $kategoria->sciezka = '/' . join('/', $sciezka);
            $this->Kategoria->save($kategoria);
            $this->updatePath($kategoria->id);
        }
    }

    private function clear_cache($typ = null) {
        foreach ($this->locales as $lc => $lId) {
            if (empty($typ)) {
                Cache::delete($lc . '_kategorie', 'cfg');
                Cache::delete($lc . '_kategorie_menu', 'cfg');
                Cache::delete($lc . '_kategorie_promocja', 'cfg');
                Cache::delete($lc . '_sezon_kategoria', 'cfg');
            } else {
                switch ($typ) {
                    case 'kategorie' : Cache::delete($lc . '_kategorie', 'cfg');
                        break;
                    case 'kategorie_menu' : Cache::delete($lc . '_kategorie_menu', 'cfg');
                        break;
                    case 'kategorie_promocja' : Cache::delete($lc . '_kategorie_promocja', 'cfg');
                        break;
                    case 'sezon_kategoria' : Cache::delete($lc . '_sezon_kategoria', 'cfg');
                        break;
                }
            }
        }
    }

    /**
     * setField method
     *
     * @param string|null $id Kategoria id.
     * @param string|null $field Kategoria field name.
     * @param mixed $value value to set.
     */
    public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->Kategoria->find('all', ['conditions' => ['Kategoria.id' => $id]])->first();
            $deny = false;
            if ($field == 'promuj' && $value == 1 && empty($item->parent_id)) {
                $checkPromuj = $this->Kategoria->find()->where(['Kategoria.parent_id IS' => NULL, 'Kategoria.id NOT IN' => [$id], 'Kategoria.promuj' => 1])->count();
                if ($checkPromuj >= 3) {
                    $deny = $this->Txt->printAdmin(__('Admin | Nie można włączyć promowania! Na stronie głównej mogą być promowane tylko 2 kategorie główne.'));
                }
            }
            $retAction = '';
            if (!$deny) {
                if (!empty($item)) {
                    if (key_exists($field, $item->toArray())) {
                        $item->{$field} = $value;
                        if ($this->Kategoria->save($item)) {
                            $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, (!empty($value) ? 0 : 1)])), 'action' => $retAction];
                        } else {
                            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                        }
                    } else {
                        $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                    }
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $deny];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    /**
     * function sort
     * 
     */
    public function sort($field = 'kolejnosc') {
        $this->autoRender = false;
        $returnData = [];
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            foreach ($rqData as $data) {
                if (!empty($data['id'])) {
                    $this->Kategoria->updateAll($data, ['id' => $data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    public function findPromotion($catId, $search = null) {
        $this->viewBuilder()->layout('ajax');
        $this->loadModel('Towar');
        if (!empty($search)) {
            $conditions = [];
            $keys = explode(' ', $search);
            foreach ($keys as $key) {
                $conditions['OR']['AND']['Towar.nazwa LIKE'] = '%' . $key . '%';
            }
            $conditions['OR']['Towar.kod LIKE'] = '%' . $search . '%';
            $conditions['Towar.ukryty'] = 0;
            if (!empty($catId)) {
                $kategoria = $this->Kategoria->find('all')->where(['Kategoria.id' => $catId])->first();
                if (!empty($kategoria->preorder)) {
                    $conditions['Towar.preorder'] = 1;
                    $conditions['Towar.data_premiery >'] = date('Y-m-d');
                } else {
                    $allKatIds = $this->Kategoria->getAllIds($catId);
                    $tIds = $this->Towar->TowarKategoria->find('list', ['keyField' => 'towar_id', 'valueField' => 'towar_id', 'group' => ['towar_id'], 'conditions' => ['kategoria_id IN' => $allKatIds]])->toArray();
                    if (empty($tIds)) {
                        $tIds = [null];
                    }
                    $conditions['Towar.id IN'] = $tIds;
                }
            }
            $towary = $this->Towar->find('all', ['conditions' => $conditions, 'order' => ['Towar.nazwa ASC'], 'limit' => 50, 'contain' => ['TowarCena']]);
            $this->set('towary', $towary);
        } else {
            header("HTTP/1.0 404 Not Found");
            echo __('error no data');
            die();
        }
    }

    public function getPromotionItem($towarId) {
        $this->viewBuilder()->layout('ajax');
        $this->loadModel('Towar');
        $towar = $this->Towar->find('all', ['contain' => ['TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]]]])->where(['Towar.id' => $towarId])->first();
        $this->set('towar', $towar);
    }

}
