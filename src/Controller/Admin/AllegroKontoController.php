<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * AllegroKonto Controller
 *
 * @property \App\Model\Table\AllegroKontoTable $AllegroKonto
 *
 * @method \App\Model\Entity\AllegroKonto[] paginate($object = null, array $settings = [])
 */
class AllegroKontoController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $cfgArr = [];
        $allegroKonto = $this->AllegroKonto->find('all', $cfgArr);

        $this->set(compact('allegroKonto'));
        $this->set('_serialize', ['allegroKonto']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $allegroKonto = $this->AllegroKonto->newEntity();
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            $allegroKonto = $this->AllegroKonto->patchEntity($allegroKonto, $rqData, ['translations' => $this->AllegroKonto->hasBehavior('Translate')]);
            if ($this->AllegroKonto->save($allegroKonto)) {
                if (!empty($allegroKonto->isDefault)) {
                    $this->AllegroKonto->updateAll(['isDefault' => 0], ['id !=' => $allegroKonto->id]);
                }
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | allegro konto'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | allegro konto'))])));
        }
        $wojewodztwa=$this->wojewodztwa;
        $this->set(compact('allegroKonto','wojewodztwa'));
        $this->set('_serialize', ['allegroKonto']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Allegro Konto id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {

        if ($this->AllegroKonto->hasBehavior('Translate')) {
            $allegroKonto = $this->AllegroKonto->find('translations', [
                        'locales' => $this->lngIdList,
                        'conditions' => ['AllegroKonto.' . $this->AllegroKonto->primaryKey() => $id],
                        'contain' => []
                    ])->first();
        } else {
            $allegroKonto = $this->AllegroKonto->find('all', [
                        'conditions' => ['AllegroKonto.' . $this->AllegroKonto->primaryKey() => $id],
                        'contain' => []
                    ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData = $this->request->getData();
            $allegroKonto = $this->AllegroKonto->patchEntity($allegroKonto, $rqData, ['translations' => $this->AllegroKonto->hasBehavior('Translate')]);
            if ($this->AllegroKonto->save($allegroKonto)) {
                if (!empty($allegroKonto->isDefault)) {
                    $this->AllegroKonto->updateAll(['isDefault' => 0], ['id !=' => $allegroKonto->id]);
                }
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | allegro konto'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | allegro konto'))])));
        }

        $_allegroKonto = $allegroKonto->toArray();
        if ($this->AllegroKonto->hasBehavior('Translate') && empty($_allegroKonto['_translations'])) {
            $transFields = $this->AllegroKonto->associations()->keys();
            $tFields = [];
            foreach ($transFields as $field) {
                if (strpos($field, '_translation') !== false) {
                    $field = substr($field, (strpos($field, '_') + 1));
                    $field = str_replace('_translation', '', $field);
                    $tFields[$field] = $_allegroKonto[$field];
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $allegroKonto->set('_translations', $translation);
        }


        $wojewodztwa=$this->wojewodztwa;
        $this->set(compact('allegroKonto','wojewodztwa'));
        $this->set('_serialize', ['allegroKonto']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Allegro Konto id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $allegroKonto = $this->AllegroKonto->get($id);
        if ($this->AllegroKonto->delete($allegroKonto)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.', [$this->Txt->printAdmin(__('Admin | allegro konto'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.', [$this->Txt->printAdmin(__('Admin | allegro konto'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * setField method
     *
     * @param string|null $id Allegro Konto id.
     * @param string|null $field Allegro Konto field name.
     * @param mixed $value value to set.
     */
    public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->AllegroKonto->find('all', ['conditions' => ['AllegroKonto.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                    $item->{$field} = $value;
                    if ($this->AllegroKonto->save($item)) {
                        $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, (!empty($value) ? 0 : 1)])), 'action' => $retAction];
                    } else {
                        $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                    }
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    public function setAuthCron($kontoId, $checkToken = false) {
        $this->autoRender = false;
        $konto = $this->AllegroKonto->get($kontoId);
        $checkCronToken = \Cake\Cache\Cache::read('allegro_' . $konto->id . '_dev_token', 'allegro');
        if (!empty($checkCronToken)) {
            $this->Flash->success(__('Token jest aktywny'));
            return $this->redirect(['action' => 'index']);
        }
        if ($checkToken && !empty($konto->device_code)) {
            $url = 'https://'. \Cake\Core\Configure::read('allegroLink').'/auth/oauth/token?grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Adevice_code&device_code=' . $konto->device_code;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            $authorization = "Authorization: Basic " . base64_encode($konto->devClientId . ':' . $konto->devClientSecret);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [$authorization, 'Content-Type: application/x-www-form-urlencoded']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, 1);
            $res = curl_exec($ch);
            $results = json_decode($res, true);
            curl_close($ch);
            if (!empty($results['access_token'])) {
                \Cake\Cache\Cache::write('allegro_' . $konto->id . '_dev_token', $results['access_token'], 'allegro');
                \Cake\Cache\Cache::write('allegro_' . $konto->id . '_dev_refresh_token', $results['refresh_token'], 'allegro');
                $this->Flash->success(__('Token został aktywowany'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Token jest nieaktywny'));
                \Cake\Cache\Cache::delete('allegro_' . $konto->id . '_dev_token', 'allegro');
                $konto->set('device_code', null);
                $konto->set('user_code', null);
                $this->AllegroKonto->save($konto);
                return $this->redirect(['action' => 'index']);
            }
        } else {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, "https://". \Cake\Core\Configure::read('allegroLink')."/auth/oauth/device");
            $authorization = "Authorization: Basic " . base64_encode($konto->devClientId . ':' . $konto->devClientSecret);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [$authorization, 'Content-Type: application/x-www-form-urlencoded']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "client_id={$konto->devClientId}");
            $res = curl_exec($ch);
            $results = json_decode($res, true);
            curl_close($ch);
            if (!empty($results['device_code'])) {
                $konto->set('device_code', $results['device_code']);
                $konto->set('user_code', $results['user_code']);
                $konto->set('interval', $results['interval']);
                $this->AllegroKonto->save($konto);
                $results['expires_in'];
                $results['verification_uri'];
                return $this->redirect($results['verification_uri_complete']);
            } else {
                $this->Flash->error(__('Błąd autoryzacji, zweryfikuj poprawność danych'));
                return $this->redirect(['action' => 'index']);
            }
            die();
        }
    }

}
