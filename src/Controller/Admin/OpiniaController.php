<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Opinia Controller
 *
 * @property \App\Model\Table\OpiniaTable $Opinia
 *
 * @method \App\Model\Entity\Opinium[] paginate($object = null, array $settings = [])
 */
class OpiniaController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
    $cfgArr=[];
$opinia = $this->Opinia->find('all',$cfgArr);

        $this->set(compact('opinia'));
        $this->set('_serialize', ['opinia']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $opinium = $this->Opinia->newEntity();
        if ($this->request->is('post')) {
        $rqData=$this->request->getData();
            $opinium = $this->Opinia->patchEntity($opinium, $rqData,['translations'=>$this->Opinia->hasBehavior('Translate')]);
            if ($this->Opinia->save($opinium)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | opinium'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | opinium'))])));
        }
        $this->set(compact('opinium'));
        $this->set('_serialize', ['opinium']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Opinium id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
    
    if($this->Opinia->hasBehavior('Translate')){
        $opinium = $this->Opinia->find('translations', [
            'locales' => $this->lngIdList,
            'conditions' => ['Opinia.'.$this->Opinia->primaryKey() => $id],
            'contain' => []
        ])->first();
        }
        else{
        $opinium = $this->Opinia->find('all', [
            'conditions'=>['Opinia.'.$this->Opinia->primaryKey()=>$id],
            'contain' => []
        ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData=$this->request->getData();
            $opinium = $this->Opinia->patchEntity($opinium, $rqData,['translations'=>$this->Opinia->hasBehavior('Translate')]);
            if ($this->Opinia->save($opinium)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | opinium'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | opinium'))])));
        }
        
        $_opinium = $opinium->toArray();
        if ($this->Opinia->hasBehavior('Translate') && empty($_opinium['_translations'])) {
            $transFields=$this->Opinia->associations()->keys();
            $tFields=[];
            foreach ($transFields as $field){
                if(strpos($field, '_translation')!==false){
                    $field= substr($field, (strpos($field, '_')+1));
                    $field=str_replace('_translation','', $field);
                    $tFields[$field]=$_opinium[$field]; 
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $opinium->set('_translations',$translation);
        }
        
        
        $this->set(compact('opinium'));
        $this->set('_serialize', ['opinium']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Opinium id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $opinium = $this->Opinia->get($id);
        if ($this->Opinia->delete($opinium)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.',[$this->Txt->printAdmin(__('Admin | opinium'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.',[$this->Txt->printAdmin(__('Admin | opinium'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

/**
     * setField method
     *
     * @param string|null $id Opinium id.
     * @param string|null $field Opinium field name.
     * @param mixed $value value to set.
     */

public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->Opinia->find('all', ['conditions' => ['Opinia.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                        $item->{$field} = $value;
                        if ($this->Opinia->save($item)) {
                            $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, !(bool) $value])), 'action' => $retAction];
                        } else {
                            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                        }
                    
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    /**
* function sort
* 
*/
public function sort($field='kolejnosc'){
        $this->autoRender=false;
        $returnData=[];
        if($this->request->is('post')){
            $rqData=$this->request->getData();
            foreach ($rqData as $data){
                if(!empty($data['id'])){
                    $this->Opinia->updateAll($data, ['id'=>$data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    }
