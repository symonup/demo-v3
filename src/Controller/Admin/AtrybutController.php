<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Atrybut Controller
 *
 * @property \App\Model\Table\AtrybutTable $Atrybut
 *
 * @method \App\Model\Entity\Atrybut[] paginate($object = null, array $settings = [])
 */
class AtrybutController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index($typId=null)
    {
        if(empty($typId)){
            return $this->redirect(['controller'=>'AtrybutTyp','action'=>'index']);
        }
        $atrybutTyp=$this->Atrybut->AtrybutTyp->find('all',['conditions'=>['AtrybutTyp.id'=>$typId]])->first();
        if(empty($atrybutTyp)){
            $this->Flash->error($this->Txt->printAdmin(__('Admin | Nie znany typ atrybutu.')));
            return $this->redirect(['controller'=>'AtrybutTyp','action'=>'index']);
        }
        
        $lastPage = $this->session->read('backToAtrybut');
        $this->session->delete('backToAtrybut');
        $page = $this->request->query('page');
        $filter = $this->session->read('filterAtrybut');
        $likeKeys = [
            'nazwa' => 'nazwa'
        ];
        $forcePage = false;
        if ($this->request->is('post')) {
            $filter=[];
            $filterData = $this->request->getData();
            if (key_exists('clear-filter', $filterData)) {
                $filter = null;
                $this->session->write('filterAtrybut', $filter);
                return $this->redirect(['action'=>'index',$typId]);
            } else {
                foreach ($filterData as $keyFilter => $filterValue) {
                    if (!empty($filterValue)) {
                        $filter[$keyFilter] = $filterValue;
                    }
                }
            }
            $page = 1;
            $forcePage = true;
        }
        $conditions = ['Atrybut.atrybut_typ_id'=>$typId];
        if (!empty($filter)) {
            foreach ($filter as $keyFilter => $filterValue) {
                if (key_exists($keyFilter, $likeKeys)) {
                    $conditions['Atrybut.' . $keyFilter . ' LIKE'] = '%' . $filterValue . '%';
                } else {
                    $conditions['Atrybut.' . $keyFilter] = $filterValue;
                }
            }
        }
        $this->session->write('filterAtrybut', $filter);
        if (!$forcePage && empty($page) && !empty($lastPage)) {
            $page = $lastPage;
        }
        if (empty($page)) {
            $page = 1;
        }
        $this->session->write('lastPageAtrybut', $page);
        
        
        $cfgArr = [
            'contain' => ['AtrybutTyp'],
            'conditions' => $conditions,
            'order' => ['Atrybut.kolejnosc'=>'desc'],
            'limit' => 200,
            'page'=>$page
        ];
        $this->paginate=$cfgArr;
        $atrybut=$this->paginate($this->Atrybut);

        $this->set(compact('atrybut','typId','filter'));
        $this->set('_serialize', ['atrybut']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($typId=null)
    {
        $atrybut = $this->Atrybut->newEntity();
        if ($this->request->is('post')) {
        $rqData=$this->request->getData();
            if(!empty($rqData['atrybut_podrzedne'])){
            foreach ($rqData['atrybut_podrzedne'] as $atrPodKey => $atrPod){
                if(empty($atrPod['nazwa'])){
                    unset($rqData['atrybut_podrzedne'][$atrPodKey]);
                }
            }
            }
            $atrybut = $this->Atrybut->patchEntity($atrybut, $rqData,['translations'=>$this->Atrybut->hasBehavior('Translate')]);
            if ($this->Atrybut->save($atrybut)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | atrybut'))])));

                return $this->redirect(['action' => 'index',$atrybut->atrybut_typ_id]);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | atrybut'))])));
        }
        $atrybutTyp = $this->Atrybut->AtrybutTyp->find('list',['keyField'=>'id','valueField'=>'nazwa'])->toArray();
        $this->set(compact('atrybut', 'atrybutTyp','typId'));
        $this->set('_serialize', ['atrybut']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Atrybut id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
    
    if($this->Atrybut->hasBehavior('Translate')){
        $atrybut = $this->Atrybut->find('translations', [
            'locales' => $this->lngIdList,
            'conditions' => ['Atrybut.'.$this->Atrybut->primaryKey() => $id],
            'contain' => ['AtrybutPodrzedne']
        ])->first();
        }
        else{
        $atrybut = $this->Atrybut->find('all', [
            'conditions'=>['Atrybut.'.$this->Atrybut->primaryKey()=>$id],
            'contain' => ['AtrybutPodrzedne']
        ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData=$this->request->getData();
            if(!empty($rqData['atrybut_podrzedne'])){
            foreach ($rqData['atrybut_podrzedne'] as $atrPodKey => $atrPod){
                if(empty($atrPod['nazwa'])){
                    unset($rqData['atrybut_podrzedne'][$atrPodKey]);
                }
            }
            }
            $oldWartosci=[];
            if(!empty($atrybut->atrybut_podrzedne)){
                foreach ($atrybut->atrybut_podrzedne as $atrPod){
                    $oldWartosci[$atrPod->id]=$atrPod->id;
                }
            }
            if(!empty($oldWartosci)){
                foreach ($rqData['atrybut_podrzedne'] as $dataPod){
                    if(!empty($dataPod['id']) && !empty($oldWartosci[$dataPod['id']])){
                        unset($oldWartosci[$dataPod['id']]);
                    }
                }
            }
            $atrybut = $this->Atrybut->patchEntity($atrybut, $rqData,['translations'=>$this->Atrybut->hasBehavior('Translate')]);
            if ($this->Atrybut->save($atrybut)) {
                if(!empty($oldWartosci)){
                    $this->Atrybut->AtrybutPodrzedne->deleteAll(['id IN'=>$oldWartosci]);
                }
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | atrybut'))])));

                return $this->redirect(['action' => 'index',$atrybut->atrybut_typ_id]);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | atrybut'))])));
        }
        
        $_atrybut = $atrybut->toArray();
        if ($this->Atrybut->hasBehavior('Translate') && empty($_atrybut['_translations'])) {
            $transFields=$this->Atrybut->associations()->keys();
            $tFields=[];
            foreach ($transFields as $field){
                if(strpos($field, '_translation')!==false){
                    $field= substr($field, (strpos($field, '_')+1));
                    $field=str_replace('_translation','', $field);
                    $tFields[$field]=$_atrybut[$field]; 
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $atrybut->set('_translations',$translation);
        }
        
        
        $atrybutTyp = $this->Atrybut->AtrybutTyp->find('list',['keyField'=>'id','valueField'=>'nazwa'])->toArray();
        $this->set(compact('atrybut', 'atrybutTyp'));
        $this->set('_serialize', ['atrybut']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Atrybut id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $atrybut = $this->Atrybut->get($id);
        if ($this->Atrybut->delete($atrybut)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.',[$this->Txt->printAdmin(__('Admin | atrybut'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.',[$this->Txt->printAdmin(__('Admin | atrybut'))])));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function sort($field='kolejnosc'){
        $this->autoRender=false;
        $returnData=[];
        if($this->request->is('post')){
            $rqData=$this->request->getData();
            foreach ($rqData as $data){
                if(!empty($data['id'])){
                    $this->Atrybut->updateAll($data, ['id'=>$data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
}
