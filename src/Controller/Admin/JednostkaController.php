<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Jednostka Controller
 *
 * @property \App\Model\Table\JednostkaTable $Jednostka
 *
 * @method \App\Model\Entity\Jednostka[] paginate($object = null, array $settings = [])
 */
class JednostkaController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
    $cfgArr=[];
$jednostka = $this->Jednostka->find('all',$cfgArr);

        $this->set(compact('jednostka'));
        $this->set('_serialize', ['jednostka']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $jednostka = $this->Jednostka->newEntity();
        if ($this->request->is('post')) {
        $rqData=$this->request->getData();
            $jednostka = $this->Jednostka->patchEntity($jednostka, $rqData,['translations'=>$this->Jednostka->hasBehavior('Translate')]);
            if ($this->Jednostka->save($jednostka)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | jednostka'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | jednostka'))])));
        }
        $this->set(compact('jednostka'));
        $this->set('_serialize', ['jednostka']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Jednostka id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
    
    if($this->Jednostka->hasBehavior('Translate')){
        $jednostka = $this->Jednostka->find('translations', [
            'locales' => $this->lngIdList,
            'conditions' => ['Jednostka.'.$this->Jednostka->primaryKey() => $id],
            'contain' => []
        ])->first();
        }
        else{
        $jednostka = $this->Jednostka->find('all', [
            'conditions'=>['Jednostka.'.$this->Jednostka->primaryKey()=>$id],
            'contain' => []
        ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData=$this->request->getData();
            $jednostka = $this->Jednostka->patchEntity($jednostka, $rqData,['translations'=>$this->Jednostka->hasBehavior('Translate')]);
            if ($this->Jednostka->save($jednostka)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | jednostka'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | jednostka'))])));
        }
        
        $_jednostka = $jednostka->toArray();
        if ($this->Jednostka->hasBehavior('Translate') && empty($_jednostka['_translations'])) {
            $transFields=$this->Jednostka->associations()->keys();
            $tFields=[];
            foreach ($transFields as $field){
                if(strpos($field, '_translation')!==false){
                    $field= substr($field, (strpos($field, '_')+1));
                    $field=str_replace('_translation','', $field);
                    $tFields[$field]=$_jednostka[$field]; 
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $jednostka->set('_translations',$translation);
        }
        
        
        $this->set(compact('jednostka'));
        $this->set('_serialize', ['jednostka']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Jednostka id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $jednostka = $this->Jednostka->get($id);
        if ($this->Jednostka->delete($jednostka)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.',[$this->Txt->printAdmin(__('Admin | jednostka'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.',[$this->Txt->printAdmin(__('Admin | jednostka'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

/**
     * setField method
     *
     * @param string|null $id Jednostka id.
     * @param string|null $field Jednostka field name.
     * @param mixed $value value to set.
     */

public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->Jednostka->find('all', ['conditions' => ['Jednostka.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                        $item->{$field} = $value;
                        if ($this->Jednostka->save($item)) {
                            $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, !(bool) $value])), 'action' => $retAction];
                        } else {
                            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                        }
                    
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    /**
* function sort
* 
*/
public function sort($field='kolejnosc'){
        $this->autoRender=false;
        $returnData=[];
        if($this->request->is('post')){
            $rqData=$this->request->getData();
            foreach ($rqData as $data){
                if(!empty($data['id'])){
                    $this->Jednostka->updateAll($data, ['id'=>$data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    }
