<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Strona Controller
 *
 * @property \App\Model\Table\StronaTable $Strona
 *
 * @method \App\Model\Entity\Strona[] paginate($object = null, array $settings = [])
 */
class StronaController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->loadModel('Banner');
        $this->loadModel('Blok');
        $bloki = $this->Blok->find('all');
        $bannersAll = $this->Banner->find('all');
        $banners = [];
        $blocks = [];
        $tmpView = new \Cake\View\View();
        $blockControls = '<div class="item-controls">'
                . '<button type="button" data-toggle="tooltip" data-placment="top" title="' . $this->Txt->printAdmin(__('Admin | Usuń blok')) . '" class="btn btn-xs btn-danger item-delete"><i class="fa fa-trash"></i></button>'
                . '<span data-toggle="tooltip" data-placment="top" title="' . $this->Txt->printAdmin(__('Admin | Przesuń blok')) . '" class="btn btn-xs btn-default item-move"><i class="fa fa-arrows-alt"></i></span>'
                . '</div>';
        foreach ($bloki as $blok) {
            if (!empty($blok->ukryty)) {
                $blocks['[%-_blok_' . $blok->id . '_-%]'] = '<div class="page-element blok-item ukryty" item-id="[%-_blok_' . $blok->id . '_-%]">' . $this->Txt->printAdmin(__('Admin | BLOK')) . ' - ' . $blok->nazwa . ' (' . $this->Txt->printAdmin(__('Admin | ukryty')) . ')' . $blockControls . '</div>';
            } else {
                $blocks['[%-_blok_' . $blok->id . '_-%]'] = '<div class="page-element blok-item" item-id="[%-_blok_' . $blok->id . '_-%]">' . $this->Txt->printAdmin(__('Admin | BLOK')) . ' - ' . $blok->nazwa . $blockControls . '</div>';
            }
        }
        foreach ($bannersAll as $bannerItem) {
            $blocks['[%-_banner_' . $bannerItem->id . '_-%]'] = '<div class="page-element banner-item" item-id="[%-_banner_' . $bannerItem->id . '_-%]">' . $this->Txt->printAdmin(__('Admin | BANER')) . ' - ' . $bannerItem->nazwa . $blockControls . '</div>';
        }

        $blocks['[%-_kontakt-form_-%]'] = '<div class="page-element kontakt-item" item-id="[%-_kontakt-form_-%]">' . $this->Txt->printAdmin(__('Admin | Formularz kontaktowy')) . $blockControls . '</div>';
        $blocks['[%-_kontakt-form-2_-%]'] = '<div class="page-element kontakt-item" item-id="[%-_kontakt-form-2_-%]">' . $this->Txt->printAdmin(__('Admin | Formularz kontaktowy do popUp')) . $blockControls . '</div>';
        $blocks['[%-_newsletter-form_-%]'] = '<div class="page-element newsletter-item" item-id="[%-_newsletter-form_-%]">' . $this->Txt->printAdmin(__('Admin | Formularz zapisu do newslettera')) . $blockControls . '</div>';
        $blocks['[%-_lista-kodow_-%]'] = '<div class="page-element kody-lista-item" item-id="[%-_lista-kodow_-%]">' . $this->Txt->printAdmin(__('Admin | Lista kuponów rabatowych')) . $blockControls . '</div>';
        $blocks['[%-_kategorie_-%]'] = '<div class="page-element kategorie-item" item-id="[%-_kategorie_-%]">' . $this->Txt->printAdmin(__('Admin | Lista kategorii')) . $blockControls . '</div>';
//        $blocks['[%-_karta-kod-form_-%]']='<div class="page-element karta-kod-form" item-id="[%-_karta-kod-form_-%]">' . $this->Txt->printAdmin(__('Admin | Formularz użycia kodu karty podarunkowej')). '</div>';
//        $blocks['[%-_karty-podarunkowe-lista_-%]'] = '<div class="page-element karty-podarunkowe-lista" item-id="[%-_karty-podarunkowe-lista_-%]">' . $this->Txt->printAdmin(__('Admin | Lista dostępnych kart podarunkowych')). '</div>';
        $this->set('blocks', $blocks);

        $fGroups = [1 => $this->Txt->printAdmin(__('Admin | Grupa 1')), 2 => $this->Txt->printAdmin(__('Admin | Grupa 2'))];
        $this->set('fGroups', $fGroups);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $strona = $this->paginate($this->Strona);

        $this->set(compact('strona'));
        $this->set('_serialize', ['strona']);
        $tmpView = new \Cake\View\View();
        $tips = [
            'strony' => $tmpView->element($this->tipsPath . 'strony')
        ];
        $this->set('tips', $tips);
    }

    /**
     * View method
     *
     * @param string|null $id Strona id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $strona = $this->Strona->get($id, [
            'contain' => []
        ]);

        $this->set('strona', $strona);
        $this->set('_serialize', ['strona']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($id = null) {
        $strona = $this->Strona->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData = $this->request->getData();
            $tresc = $rqData['tresc'];
            $strona = $this->Strona->patchEntity($strona, $rqData);
            if ($this->Strona->save($strona)) {
                $path = $this->filePath['pages'];
                $this->loadComponent('Upload');
                $this->Upload->checkDir($path . (!empty($strona->locale) ? $strona->locale : $this->defPageLocale) . DS);
                $file = fopen($path . (!empty($strona->locale) ? $strona->locale : $this->defPageLocale) . DS . 'page_' . $strona->id . '.ctp', 'a+');
                fwrite($file, $tresc);
                fclose($file);
                $this->Flash->success(__('The data has been saved.'));
                if (empty($this->FooterLink)) {
                    $this->loadModel('FooterLink');
                }
                if (!empty($strona->menu_dolne)) {
                    $footerLink = $this->FooterLink->find('all')->where(['strona_id' => $strona->id])->first();
                    $fTarget = '_self';
                    if (empty($footerLink)) {
                        $footerLink = $this->FooterLink->newEntity();
                    } else {
                        $fTarget = $footerLink->target;
                    }
                    $footerLinkData = [
                        'url' => $strona->nazwa,
                        'label' => $strona->nazwa,
                        'title' => $strona->nazwa,
                        'grupa' => $strona->menu_dolne_grupa,
                        'target' => $fTarget,
                        'strona_id' => $strona->id
                    ];
                    $footerLink = $this->FooterLink->patchEntity($footerLink, $footerLinkData);
                    $this->FooterLink->save($footerLink);
                } else {
                    $this->FooterLink->deleteAll(['strona_id' => $strona->id]);
                }
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The data could not be saved. Please, try again.'));
        }
        if (!empty($id)) {
            $strona = $this->Strona->get($id, [
                'contain' => []
            ]);
            $path = $this->filePath['pages'];
            if (file_exists($path . (!empty($strona->locale) ? $strona->locale : $this->defPageLocale) . DS . 'page_' . $strona->id . '.ctp')) {
                $strona->tresc = file_get_contents($path . (!empty($strona->locale) ? $strona->locale : $this->defPageLocale) . DS . 'page_' . $strona->id . '.ctp');
            }
        }
        $this->set(compact('strona'));
        $this->set('_serialize', ['strona']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Strona id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $strona = $this->Strona->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData = $this->request->getData();
            $tresc = $rqData['tresc'];
            $strona = $this->Strona->patchEntity($strona, $rqData);
            if ($this->Strona->save($strona)) {
                $path = $this->filePath['pages'];
                $this->loadComponent('Upload');
                $this->Upload->checkDir($path . (!empty($strona->locale) ? $strona->locale : $this->defPageLocale) . DS);
                if (!file_exists($path . (!empty($strona->locale) ? $strona->locale : $this->defPageLocale) . DS . 'page_' . $strona->id . '.ctp')) {
                    $file = fopen($path . (!empty($strona->locale) ? $strona->locale : $this->defPageLocale) . DS . 'page_' . $strona->id . '.ctp', 'a+');
                } else {
                    $file = fopen($path . (!empty($strona->locale) ? $strona->locale : $this->defPageLocale) . DS . 'page_' . $strona->id . '.ctp', 'w+');
                }

                fwrite($file, $tresc);
                fclose($file);

                $this->Flash->success(__('The data has been saved.'));
                if (empty($this->FooterLink)) {
                    $this->loadModel('FooterLink');
                }
                if (!empty($strona->menu_dolne)) {
                    $footerLink = $this->FooterLink->find('all')->where(['strona_id' => $strona->id])->first();
                    $fTarget = '_self';
                    if (empty($footerLink)) {
                        $footerLink = $this->FooterLink->newEntity();
                    } else {
                        $fTarget = $footerLink->target;
                    }
                    $footerLinkData = [
                        'url' => $strona->nazwa,
                        'label' => $strona->nazwa,
                        'title' => $strona->nazwa,
                        'grupa' => $strona->menu_dolne_grupa,
                        'target' => $fTarget,
                        'strona_id' => $strona->id
                    ];
                    $footerLink = $this->FooterLink->patchEntity($footerLink, $footerLinkData);
                    $this->FooterLink->save($footerLink);
                } else {
                    $this->FooterLink->deleteAll(['strona_id' => $strona->id]);
                }
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The data could not be saved. Please, try again.'));
        }
        $path = $this->filePath['pages'];
        $rType = 'a+';
        if (file_exists($path . (!empty($strona->locale) ? $strona->locale : $this->defPageLocale) . DS . 'page_' . $strona->id . '.ctp')) {
            $rType = 'r+';
        }
        $file = fopen($path . (!empty($strona->locale) ? $strona->locale : $this->defPageLocale) . DS . 'page_' . $strona->id . '.ctp', $rType);
        $fileSize = filesize($path . (!empty($strona->locale) ? $strona->locale : $this->defPageLocale) . DS . 'page_' . $strona->id . '.ctp');
        if ($fileSize > 0) {
            $strona->tresc = fread($file, $fileSize);
        }
        fclose($file);
        $this->set(compact('strona'));
        $this->set('_serialize', ['strona']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Strona id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $strona = $this->Strona->get($id);
        if ($this->Strona->delete($strona)) {
            $path = $this->filePath['pages'];
            if (file_exists($path . $this->defPageLocale . DS . 'page_' . $id . '.ctp')) {
                rename($path . $this->defPageLocale . DS . 'page_' . $id . '.ctp', $path . $this->defPageLocale . DS . 'page_' . $id . '_removed_' . date('YmdHis') . '.ctp');
            }
            $this->Flash->success(__('The data has been deleted.'));
        } else {
            $this->Flash->error(__('The data could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getBlokList() {
        $this->viewBuilder()->layout('ajax');
    }

    public function stats() {
        $this->loadModel('Statystyki');
        $all = $this->Statystyki->find('all')->count();
        $month = $this->Statystyki->find('all', ['conditions' => ['start >=' => date('Y-m-01 00:00:00')]])->count();
        $sevenDays = $this->Statystyki->find('all', ['conditions' => ['start >=' => date('Y-m-d 00:00:00', strtotime('- 7 days'))]])->count();
        $yesterday = $this->Statystyki->find('all', ['conditions' => ['start >=' => date('Y-m-d 00:00:00', strtotime('- 1 days')), 'start <' => date('Y-m-d 00:00:00')]])->count();
        $today = $this->Statystyki->find('all', ['conditions' => ['last >=' => date('Y-m-d 00:00:00')]])->count();
        $now = $this->Statystyki->find('all', ['conditions' => ['last >=' => date('Y-m-d H:i:s', strtotime('- 1 hour'))]])->count();

        $tmpView = new \Cake\View\View();
        $tips = [
            'bloki' => $tmpView->element($this->tipsPath . 'bloki'),
            'strony' => $tmpView->element($this->tipsPath . 'strony'),
            'banery' => $tmpView->element($this->tipsPath . 'banery'),
            'menu' => $tmpView->element($this->tipsPath . 'menu'),
            'stopka' => $tmpView->element($this->tipsPath . 'stopka'),
            'newsletter_adres' => $tmpView->element($this->tipsPath . 'newsletter_adres'),
            'newsletter' => $tmpView->element($this->tipsPath . 'newsletter'),
            'send_newsletter' => $tmpView->element($this->tipsPath . 'send_newsletter'),
            'konfiguracja' => $tmpView->element($this->tipsPath . 'konfiguracja'),
            'szablon' => $tmpView->element($this->tipsPath . 'szablon'),
            'slownik' => $tmpView->element($this->tipsPath . 'slownik'),
            'admin' => $tmpView->element($this->tipsPath . 'admin')
        ];
        $this->set('tips', $tips);
        $this->set(compact('all', 'month', 'sevenDays', 'yesterday', 'today', 'now', 'tips'));
    }

}
