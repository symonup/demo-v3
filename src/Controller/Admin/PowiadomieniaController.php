<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Powiadomienia Controller
 *
 * @property \App\Model\Table\PowiadomieniaTable $Powiadomienia
 *
 * @method \App\Model\Entity\Powiadomienium[] paginate($object = null, array $settings = [])
 */
class PowiadomieniaController extends AppController {

    public $callsStat;

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);

        $this->callsStat = [
            'oczekuje', 'zrealizowane', 'brak kontaktu','błędny numer'
        ];
        $this->set('callsStat', $this->callsStat);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $this->loadModel('Zapytanie');
        $this->loadModel('Platforma');
        $this->loadModel('CallBack');
        $cfgArr = [
            'contain' => ['Uzytkownik', 'Towar', 'PunktyOdbioru']
        ];
        $zapytaniaODostepnosc = $this->Powiadomienia->find('all', $cfgArr)->where(['Powiadomienia.typ' => 'dostepnosc']);
        $zapytaniaOProdukt = $this->Powiadomienia->find('all', $cfgArr)->where(['Powiadomienia.typ' => 'zapytanie']);
        $callBack = $this->CallBack->find('all', ['contain' => ['Administrator']])->order(['CallBack.data' => 'desc']);
//        $zapytaniaOCene = $this->Powiadomienia->find('all', $cfgArr)->where(['Powiadomienia.typ' => 'cena']);
        $zapytaniaODostepnoscCount = $this->Powiadomienia->find('all', $cfgArr)->where(['Powiadomienia.typ' => 'dostepnosc', 'Powiadomienia.wyslane' => 0])->count();
        $zapytaniaOProduktCount = $this->Powiadomienia->find('all', $cfgArr)->where(['Powiadomienia.typ' => 'zapytanie', 'Powiadomienia.wyslane' => 0])->count();
        $callBackCount = $this->CallBack->find('all')->where(['CallBack.status' => 0])->count();
//        $naZamowienie = $this->Zapytanie->find('all');
//        $naZamowienieCount = $this->Zapytanie->find('all')->where(['Zapytanie.data_wyslania IS NULL'])->count();
        $this->set(compact('zapytaniaODostepnosc', 'zapytaniaOProdukt', 'zapytaniaODostepnoscCount', 'zapytaniaOProduktCount', 'callBackCount', 'callBack'));
    }

    public function quickReply($zapId) {
        $cfgArr = [
            'contain' => ['Uzytkownik', 'Towar', 'PunktyOdbioru']
        ];
        $this->loadModel('Szablon');
        $this->loadComponent('Replace');
        $zapytanie = $this->Powiadomienia->find('all', $cfgArr)->where(['Powiadomienia.id' => $zapId])->first();
        if ($zapytanie->typ == 'zapytanie') {
            $szablonNameTak = 'odpowiedz_na_zapytanie_o_produkt_tak';
            $szablonNameNie = 'odpowiedz_na_zapytanie_o_produkt_nie';
        } else {
            $szablonNameTak = 'odpowiedz_na_zapytanie_dostepny';
            $szablonNameNie = 'odpowiedz_na_zapytanie_niedostepny';
        }
        if ($this->request->is('post')) {
            $this->loadComponent('Mail');
            $rqData = $this->request->getData();
            if ($rqData['dostepny'] == 1) {
                $szablon = $this->Szablon->findByNazwa($szablonNameTak)->first();
            } else {
                $szablon = $this->Szablon->findByNazwa($szablonNameNie)->first();
            }
            $zapytanie = $this->Powiadomienia->patchEntity($zapytanie, $rqData);
            if ($this->Powiadomienia->save($zapytanie)) {
                if ($this->Mail->sendMail($zapytanie->email, $szablon->temat, $rqData['odpowiedz'])) {
                    $zapytanie = $this->Powiadomienia->patchEntity($zapytanie, ['wyslane' => 1, 'data_wyslania' => date('Y-m-d H:i:s')]);
                    $this->Powiadomienia->save($zapytanie);
                    $zapytaniaODostepnoscCount = $this->Powiadomienia->find('all', $cfgArr)->where(['Powiadomienia.typ' => 'dostepnosc', 'Powiadomienia.wyslane' => 0])->count();
                    $zapytaniaOPremieryCount = $this->Powiadomienia->find('all', $cfgArr)->where(['Powiadomienia.typ' => 'dostepnosc_premiera', 'Powiadomienia.wyslane' => 0])->count();
                    $returnArr = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Odpowiedź została wysłana')), 'countDostepnosc' => $zapytaniaODostepnoscCount, 'countPremiery' => $zapytaniaOPremieryCount, 'action' => 'powiadomienieSetWysylkaDate', 'dostepnosc-item-id' => $zapytanie->id, 'wyslane' => ((!empty($zapytanie->wyslane) && !empty($zapytanie->data_wyslania)) ? $this->Txt->printDate($zapytanie->data_wyslania, 'Y-m-d H:i', $this->Txt->printBool($zapytanie->wyslane)) : $this->Txt->printBool($zapytanie->wyslane)) . (!empty($zapytanie->wyslane) ? $this->Txt->Html->tag('span', $this->Txt->printAdmin(__('Admin | Dosępny: {0}', [$this->Txt->printBool($zapytanie->dostepny)])), ['class' => 'odpowiedz-info', 'data-toggle' => 'popover', 'data-html' => 'true', 'data-trigger' => 'click | hover', 'data-container' => 'body', 'data-placement' => 'top', 'data-content' => addslashes($zapytanie->odpowiedz)]) : '')];
                } else {
                    $returnArr = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd wysyłki maila'))];
                }
            } else {
                $returnArr = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie można zapisać odpowiedzi'))];
            }
            $this->response->type('ajax');
            $this->response->body(json_encode($returnArr));
            return $this->response;
        }
        $replace = [];
        foreach ($zapytanie->toArray() as $field => $value) {
            if (is_object($value)) {
                $replace['[%' . $field . '_data%]'] = $this->Txt->printDate($value, 'Y-m-d');
                $replace['[%' . $field . '_czas%]'] = $this->Txt->printDate($value, 'H:i');
            } else {
                if (is_array($value)) {
                    continue;
                }
                $replace['[%' . $field . '%]'] = $value;
            }
        }
        $replace['[%towar%]'] = ($zapytanie->has('towar') ? $this->Txt->Html->link($zapytanie->towar->nazwa, \Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'view', $zapytanie->towar->id, $this->Txt->friendlyUrl($zapytanie->towar->nazwa), 'prefix' => false], true)) : '');
        $replace['[%adres%]'] = ($zapytanie->has('punkty_odbioru') ? nl2br($zapytanie->punkty_odbioru->adres) : '');
        $replace['[%data-preorder%]'] = (($zapytanie->has('towar') && !empty($zapytanie->towar->preorder)) ? $this->Txt->printDate($zapytanie->towar->preorder_od, 'Y-m-d H:i') : '');
        $replace['[%data-premiery%]'] = ($zapytanie->has('towar') ? $this->Txt->printDate($zapytanie->towar->data_premiery, 'Y-m-d') : '');
        $szablonZapytanieTak = $this->Szablon->findByNazwa($szablonNameTak)->first();
        $szablonZapytanieNie = $this->Szablon->findByNazwa($szablonNameNie)->first();
        $szablonZapytanieTak = $this->Replace->getTemplate($szablonZapytanieTak->tresc, $replace, [], true);
        $szablonZapytanieNie = $this->Replace->getTemplate($szablonZapytanieNie->tresc, $replace, [], true);
        $this->viewBuilder()->setLayout('ajax');
        $this->set(compact('zapytanie', 'szablonZapytanieNie', 'szablonZapytanieTak'));
    }

    public function zapytanieReply($zapId) {
        $this->loadModel('Szablon');
        $this->loadModel('Zapytanie');
        $this->loadModel('Platforma');
        $this->loadComponent('Replace');
        $platformyNames = $this->Platforma->find('list', ['keyField' => 'id', 'valueField' => 'nazwa'])->toArray();
        $zapytanie = $this->Zapytanie->find('all')->where(['Zapytanie.id' => $zapId])->first();
        $szablonName = 'odpowiedz_na_zapytanie';
        if ($this->request->is('post')) {
            $this->loadComponent('Mail');
            $rqData = $this->request->getData();
            $zapytanie = $this->Zapytanie->patchEntity($zapytanie, $rqData);
            if ($this->Zapytanie->save($zapytanie)) {
                if ($this->Mail->sendMail($zapytanie->email, $rqData['temat'], $rqData['odpowiedz'])) {
                    $zapytanie = $this->Zapytanie->patchEntity($zapytanie, ['data_wyslania' => date('Y-m-d H:i:s')]);
                    $this->Zapytanie->save($zapytanie);
                    $naZamowienieCount = $this->Zapytanie->find('all')->where(['Zapytanie.data_wyslania IS NULL'])->count();
                    $returnArr = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Odpowiedź została wysłana')), 'countNaZamowienie' => $naZamowienieCount, 'action' => 'zapytanieSetWysylkaDate', 'zapytanie-item-id' => $zapytanie->id, 'wyslane' => (!empty($zapytanie->data_wyslania) ? $this->Txt->printDate($zapytanie->data_wyslania, 'Y-m-d H:i') : $this->Txt->printBool(0)) . (!empty($zapytanie->odpowiedz) ? $this->Txt->Html->tag('span', $this->Txt->printAdmin(__('Admin | Zobacz odpowiedź')), ['class' => 'odpowiedz-info', 'data-toggle' => 'popover', 'data-html' => 'true', 'data-trigger' => 'click | hover', 'data-container' => 'body', 'data-placement' => 'top', 'data-content' => addslashes($zapytanie->odpowiedz)]) : '')];
                } else {
                    $returnArr = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd wysyłki maila'))];
                }
            } else {
                $returnArr = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie można zapisać odpowiedzi'))];
            }
            $this->response->type('ajax');
            $this->response->body(json_encode($returnArr));
            return $this->response;
        }
        $replace = [];
        foreach ($zapytanie->toArray() as $field => $value) {
            if (is_object($value)) {
                $replace['[%' . $field . '_data%]'] = $this->Txt->printDate($value, 'Y-m-d');
                $replace['[%' . $field . '_czas%]'] = $this->Txt->printDate($value, 'H:i');
            } else {
                if (is_array($value)) {
                    continue;
                }
                if ($field == 'platforma' && key_exists($value, $platformyNames)) {
                    $replace['[%' . $field . '%]'] = $platformyNames[$value];
                } else {
                    $replace['[%' . $field . '%]'] = $value;
                }
            }
        }
        $szablon = $this->Szablon->findByNazwa($szablonName)->first();
        $temat = $this->Replace->getTemplate($szablon->temat, $replace, [], true);
        $szablon = $this->Replace->getTemplate($szablon->tresc, $replace, [], true);
        $this->viewBuilder()->setLayout('ajax');
        $this->set(compact('zapytanie', 'szablon', 'platformyNames', 'temat'));
    }

    public function callBack($id) {
        $this->loadModel('CallBack');
        $callBack = $this->CallBack->find()->where(['CallBack.id' => $id])->first();
        if ($this->request->is(['post', 'put', 'patch'])) {
            $oldStatus = $callBack->status;
            $rqData = $this->request->getData();
            if ($oldStatus != $rqData['status']) {
                $rqData['administrator_id'] = $this->Auth->user('id');
                $rqData['administrator_dane'] = $this->Auth->user('login');
                $rqData['data_status'] = new \DateTime();
            }
            $callBack = $this->CallBack->patchEntity($callBack, $rqData);
            if ($this->CallBack->save($callBack)) {
                $callBackCount = $this->CallBack->find('all')->where(['CallBack.status' => 0])->count();
                $returnArr = ['status' => 'success', 'action' => 'callBackUpdate','callId'=>$callBack->id, 'call_status' => $this->callsStat[$callBack->status], 'call_admin' => $callBack->administrator_dane,'call_data'=>$this->Txt->printDate($callBack->data_status,'Y-m-d H:i'), 'call_notatki' => nl2br($callBack->notatki), 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zapisane')), 'callBackCount' => $callBackCount];
            } else {
                $returnArr = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych'))];
            }
            $this->response->type('ajax');
            $this->response->body(json_encode($returnArr));
            return $this->response;
        }
        $this->viewBuilder()->setLayout('ajax');
        $this->set(compact('callBack'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $powiadomienium = $this->Powiadomienia->newEntity();
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            $powiadomienium = $this->Powiadomienia->patchEntity($powiadomienium, $rqData, ['translations' => $this->Powiadomienia->hasBehavior('Translate')]);
            if ($this->Powiadomienia->save($powiadomienium)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | powiadomienium'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | powiadomienium'))])));
        }
        $uzytkownik = $this->Powiadomienia->Uzytkownik->find('list')->toArray();
        $towar = $this->Powiadomienia->Towar->find('list')->toArray();
        $punktyOdbioru = $this->Powiadomienia->PunktyOdbioru->find('list')->toArray();
        $this->set(compact('powiadomienium', 'uzytkownik', 'towar', 'punktyOdbioru'));
        $this->set('_serialize', ['powiadomienium']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Powiadomienium id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {

        if ($this->Powiadomienia->hasBehavior('Translate')) {
            $powiadomienium = $this->Powiadomienia->find('translations', [
                        'locales' => $this->lngIdList,
                        'conditions' => ['Powiadomienia.' . $this->Powiadomienia->primaryKey() => $id],
                        'contain' => []
                    ])->first();
        } else {
            $powiadomienium = $this->Powiadomienia->find('all', [
                        'conditions' => ['Powiadomienia.' . $this->Powiadomienia->primaryKey() => $id],
                        'contain' => []
                    ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData = $this->request->getData();
            $powiadomienium = $this->Powiadomienia->patchEntity($powiadomienium, $rqData, ['translations' => $this->Powiadomienia->hasBehavior('Translate')]);
            if ($this->Powiadomienia->save($powiadomienium)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | powiadomienium'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | powiadomienium'))])));
        }

        $_powiadomienium = $powiadomienium->toArray();
        if ($this->Powiadomienia->hasBehavior('Translate') && empty($_powiadomienium['_translations'])) {
            $transFields = $this->Powiadomienia->associations()->keys();
            $tFields = [];
            foreach ($transFields as $field) {
                if (strpos($field, '_translation') !== false) {
                    $field = substr($field, (strpos($field, '_') + 1));
                    $field = str_replace('_translation', '', $field);
                    $tFields[$field] = $_powiadomienium[$field];
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $powiadomienium->set('_translations', $translation);
        }


        $uzytkownik = $this->Powiadomienia->Uzytkownik->find('list')->toArray();
        $towar = $this->Powiadomienia->Towar->find('list')->toArray();
        $punktyOdbioru = $this->Powiadomienia->PunktyOdbioru->find('list')->toArray();
        $this->set(compact('powiadomienium', 'uzytkownik', 'towar', 'punktyOdbioru'));
        $this->set('_serialize', ['powiadomienium']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Powiadomienium id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $powiadomienium = $this->Powiadomienia->get($id);
        if ($this->Powiadomienia->delete($powiadomienium)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.', [$this->Txt->printAdmin(__('Admin | powiadomienium'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.', [$this->Txt->printAdmin(__('Admin | powiadomienium'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * setField method
     *
     * @param string|null $id Powiadomienium id.
     * @param string|null $field Powiadomienium field name.
     * @param mixed $value value to set.
     */
    public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->Powiadomienia->find('all', ['conditions' => ['Powiadomienia.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                    $item->{$field} = $value;
                    if ($this->Powiadomienia->save($item)) {
                        $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, (!empty($value) ? 0 : 1)])), 'action' => $retAction];
                    } else {
                        $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                    }
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    /**
     * function sort
     * 
     */
    public function sort($field = 'kolejnosc') {
        $this->autoRender = false;
        $returnData = [];
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            foreach ($rqData as $data) {
                if (!empty($data['id'])) {
                    $this->Powiadomienia->updateAll($data, ['id' => $data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

}
