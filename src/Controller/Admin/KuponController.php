<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Kupon Controller
 *
 * @property \App\Model\Table\KuponTable $Kupon
 */
class KuponController extends AppController
{

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->set('kuponTyp',['text'=>__('Tekst'),'transport'=>__('Darmowy transport'),'rabat'=>__('Rabat')]);
        
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate=['contain'=>['ZamowienieKupon']];
        $kupon = $this->paginate($this->Kupon);

        $this->set(compact('kupon'));
        $this->set('_serialize', ['kupon']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $kupon = $this->Kupon->newEntity();
        if ($this->request->is('post')) {
            $rqData=$this->request->data;
            $rqData['kategoria_ids']=[];
            if(!empty($rqData['kategoria'])){
                $this->loadModel('Kategoria');
                $this->Kategoria->allIds=[];
                $allKatIds=$this->Kategoria->getAllIds2([$rqData['kategoria']]);
                if(!empty($allKatIds)){
                    $rqData['kategoria_ids']=$allKatIds;
                }
            }
            $kupon = $this->Kupon->patchEntity($kupon, $rqData);
            if ($this->Kupon->save($kupon)) {
                $this->Flash->success(__('The kupon has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The kupon could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('kupon'));
        $this->set('_serialize', ['kupon']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Kupon id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        
        $kupon = $this->Kupon->find('translations', [
            'locales' => $this->lngIdList,
            'conditions' => ['Kupon.id' => $id],
            'contain' => ['KuponRabat']
        ])->first();
        $oldKuponRabatIds=[];
        if(!empty($kupon->kupon_rabat)){
            foreach ($kupon->kupon_rabat as $pr){
                $oldKuponRabatIds[$pr->id]=$pr->id;
            }
            
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData=$this->request->data;
            $rqData['kategoria_ids']=[];
            if(!empty($rqData['kategoria_id'])){
                $this->loadModel('Kategoria');
                $this->Kategoria->allIds=[];
                $allKatIds=$this->Kategoria->getAllIds2([$rqData['kategoria_id']]);
                if(!empty($allKatIds)){
                    $rqData['kategoria']['_ids']=$allKatIds;
                }
            }
            if(!empty($rqData['kupon_rabat'])){
                foreach ($rqData['kupon_rabat'] as $rqPR){
                    if(!empty($rqPR['id'])){
                        unset($oldKuponRabatIds[$rqPR['id']]);
                    }
                }
            }
            $kupon = $this->Kupon->patchEntity($kupon, $rqData);
            if ($this->Kupon->save($kupon)) {
                if(!empty($oldKuponRabatIds)){
                    $this->Kupon->KuponRabat->deleteAll(['id IN'=>$oldKuponRabatIds]);
                }
                $this->Flash->success(__('The kupon has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The kupon could not be saved. Please, try again.'));
            }
        }
        $_kupon = $kupon->toArray();
        if (empty($_kupon['_translations'])) {
            $translation = [
                $this->defLngSymbol => [
                    'nazwa' => $_kupon['nazwa'],
                    'text' => $_kupon['text'],
                    'success' => $_kupon['success'],
                    'error' => $_kupon['error']
            ]];
            $kupon->translation($this->defLngSymbol)->set($translation[$this->defLngSymbol], ['guard' => false]);
        }
        $this->set(compact('kupon'));
        $this->set('_serialize', ['kupon']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Kupon id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $kupon = $this->Kupon->get($id);
        if ($this->Kupon->delete($kupon)) {
            $this->Flash->success(__('The kupon has been deleted.'));
        } else {
            $this->Flash->error(__('The kupon could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function status($id = null, $field = '', $value = 0) {
        $this->autoRender = false;
        if (empty($id) || empty($field)) {
            header("HTTP/1.0 404 Not Found");
            echo __('error no data');
            die();
        }
        $item = $this->Kupon->get($id);
        $item->{$field} = $value;
        if ($this->Kupon->save($item)) {
            if ($value == 0)
                echo json_encode(['message' => __('no')]);
            else
                echo json_encode(['message' => __('yes')]);
        }
        else {
            header("HTTP/1.0 404 Not Found");
            echo __('saveing data error');
            die();
        }
    }
    public function kod(){
        $this->autoRender=false;
        $kod='';
        do{
            $tmpKod=$this->getBonCode(6);
            $count=$this->Kupon->find('all',['conditions'=>['Kupon.kod'=>$tmpKod]])->count();
            if(empty($count)){
                $kod=$tmpKod;
            }
        }while ($kod=='');
        $this->response->type('ajax');
        $this->response->body($kod);
        return $this->response;
    }
    private function getBonCode($length = 6) {
        $code = [];
        for ($i = 1; $i <= $length; $i++) {
            $code[] = $this->generateRandomStringUpper(1);
        }
        shuffle($code);
        return join('', $code);
    }
        private function generateRandomString($length = 10) {
        return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
    }

    private function generateRandomStringAll($length = 10) {
        return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!#%@$*', ceil($length / strlen($x)))), 1, $length);
    }

    private function generateRandomStringUpper($length = 10) {
        return substr(str_shuffle(str_repeat($x = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
    }
    
    private function generateRandomNumber($length = 10) {
        return substr(str_shuffle(str_repeat($x = '0123456789', ceil($length / strlen($x)))), 1, $length);
    }

    private function generateRandomStringAlphaUpper($length = 10) {
        return substr(str_shuffle(str_repeat($x = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
    }

    private function generateRandomStringAlpha($length = 10) {
        return substr(str_shuffle(str_repeat($x = 'abcdefghijklmnopqrstuvwxyz', ceil($length / strlen($x)))), 1, $length);
    }
}
