<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Imper86\AllegroApi\Rest\Model\Collection\StringCollection;
use Cake\Core\Configure;
/**
 * Allegro Controller
 *
 *
 * @method \App\Model\Entity\Allegro[] paginate($object = null, array $settings = [])
 * @property \App\Model\Table\AllegroKontoTable $AllegroKonto
 */
class AllegroController extends AppController {

    private $credentials;
    private $client;
    private $token;
    private $konto;

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->loadModel('AllegroKonto');
    }

    private function setClinent($kontoId = null) {
        $conditions = [];
        if (!empty($kontoId)) {
            $conditions['AllegroKonto.id'] = $kontoId;
        } else {
            $conditions['AllegroKonto.isDefault'] = 1;
        }
        $konto = $this->AllegroKonto->find()->where($conditions)->first();
        if (!empty($konto)) {
            $this->credentials = new \Imper86\AllegroApi\Credentials([
                'restClientId' => $konto->restClientId,
                'restClientSecret' => $konto->restClientSecret,
                'restApiKey' => $konto->restApiKey,
                'restRedirectUri' => $konto->restRedirectUri,
                'soapApiKey' => $konto->soapApiKey,
            ]);
            $this->client = new \Imper86\AllegroApi\RestClient($this->credentials);
            $this->token = \Cake\Cache\Cache::read('allegro_' . $konto->id . '_token', 'allegro');
            $this->konto = $konto;
            if (empty($konto->token_expire) || (!empty($konto->token_expire) && $this->Txt->printDate($konto->token_expire,'Y-m-d H:i:s','',true) < time())) {
                if (!empty($this->token)) {
                    $authService = $this->client->getAuthService();
                    $this->token = $authService->refreshToken($this->token);
                    $this->saveToken();
                } else {
                    return true;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    private function saveToken() {
        if (empty($this->konto)) {
            return false;
        }
        $this->AllegroKonto->updateAll(['token_expire' => date('Y-m-d H:i:s', strtotime('+ 12 hours'))], ['id' => $this->konto->id]);
        \Cake\Cache\Cache::write('allegro_' . $this->konto->id . '_token', $this->token, 'allegro');
        return true;
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function restApi() {
        if ($this->setClinent()) {
            if (empty($this->token)) {
                $authService = $this->client->getAuthService();
                $code = $_GET['code'];
                if (empty($code)) {
                    header('Location: ' . urldecode($authService->getAuthUrl()));
                    die();
                }
                $this->token = $authService->getNewToken($code);
                $this->saveToken();
            } else {
                $authService = $this->client->getAuthService();
                $this->token = $authService->refreshToken($this->token);
                $this->saveToken();
            }
            $this->Flash->success($this->Txt->printAdmin(__('Admin | Wygenerowano token')));
            return $this->redirect(['action' => 'index']);
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | Uzupełnij konfiguracje konta allegro')));
            return $this->redirect(['controller' => 'AllegroKonto', 'action' => 'index']);
        }
    }

    public function getCategories($kontoId = null, $id = null) {
        $checkClinet = $this->setClinent($kontoId);
        if (!$checkClinet || empty($this->token)) {
            return $this->redirect(['action' => 'restApi']);
        }
        $allCats = $this->getCategorieTree($id);
        $options = [];
        if (!empty($allCats) && !empty($allCats['categories'])) {
            foreach ($allCats['categories'] as $category) {
                if (empty($category['leaf'])) {
                    $options[$category['id']] = $this->Txt->Html->tag('option', $category['name'], ['value' => $category['id'], 'parent-id' => $id, 'href' => \Cake\Routing\Router::url(['action' => 'getCategories', $kontoId, $category['id']])]);
                } else {
                    $options[$category['id']] = $this->Txt->Html->tag('option', $category['name'], ['value' => $category['id'], 'parent-id' => $id]);
                }
            }
        }
        $returnData = [];
        if (!empty($options)) {
            $returnModal = $this->request->getQuery('modal');
            $view = new \Cake\View\View();
            $view->set('options', $options);
            $view->set('parent', $id);
            $view->set('key', $this->request->getQuery('key'));
            if (!empty($returnModal)) {
                $html = $view->element('admin/allegro/category_modal');
            } else {
                $html = $view->element('admin/allegro/category_options');
            }
            $returnData = ['status' => 'success', 'html' => $html, 'allCats' => $allCats];
        } else {
            $returnData = ['error', 'message' => $this->Txt->printAdmin(__('Brak kategorii'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    public function getCategory($id = null, $parametrs = false) {
        $checkClinet = $this->setClinent();
        if (!$checkClinet || empty($this->token)) {
            return $this->redirect(['action' => 'restApi']);
        }
        $aukacjaId=$this->request->getQuery('itemId');
        $allegroAukcja=null;
        if(!empty($aukacjaId)){
        $this->loadModel('AllegroAukcja');
            $allegroAukcja = $this->AllegroAukcja->find()->where(['AllegroAukcja.item_id'=>$aukacjaId])->first();
        }
        if ($parametrs) {
            $parametrs = '/parameters';
        } else {
            $parametrs = '';
        }
        $request = new \Imper86\AllegroApi\Rest\Model\Request('sale/categories/' . $id . $parametrs, 'GET', null, null);
        $response = $this->client->sendRequest($this->token, $request);
        $results = json_decode((string) $response->getBody(), true);
        $returnData = [];
        if (!empty($results)) {
            $view = new \Cake\View\View();
            $view->set('params', $results);
            $view->set('allegroAukcja',$allegroAukcja);
            $html = $view->element('admin/allegro/category_params');
            $returnData = ['status' => 'success', 'html' => $html];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    private function getCategorieTree($parentId = null) {
        $params = [];
        if (!empty($parentId)) {
            $params = ['parent.id' => $parentId];
        }
        $request = new \Imper86\AllegroApi\Rest\Model\Request('sale/categories/', 'GET', null, $params);
        $response = $this->client->sendRequest($this->token, $request);
        $results = json_decode((string) $response->getBody(), true);

        return $results;
    }

    public function newAuction($itemId,$ofertaId=null,$forceEdit=false) {
        $this->loadModel('AllegroAukcja');
        $this->loadModel('Towar');
        $towar = $this->Towar->find('all', ['contain' => ['TowarCena' => ['Waluta'],'AllegroAukcja', 'TowarZdjecie', 'Platforma']])->where(['Towar.id' => $itemId])->first();
        $allegroKonto = $this->AllegroKonto->find()->where(['AllegroKonto.isDefault' => 1])->first();

        $checkClinet = $this->setClinent($allegroKonto->id);
        if (!$checkClinet || empty($this->token)) {
            return $this->redirect(['action' => 'restApi']);
        }
        if(!empty($ofertaId)){
            $allegroAukcja = $this->AllegroAukcja->find()->where(['AllegroAukcja.item_id'=>$ofertaId])->first();
            if(empty($allegroAukcja)){
            if(!$forceEdit){
                $this->Flash->error($this->Txt->printAdmin(__('Admin | Nie znaleziono pasującej aukcji')));
                return $this->redirect(['action'=>'index']);
            }else{
                $allegroAukcja = $this->AllegroAukcja->newEntity();
            }
            }
        }else{
            $allegroAukcja = $this->AllegroAukcja->newEntity();
        }
        if ($this->request->is(['post','put','patch'])) {
            $checkClinet = $this->setClinent($allegroKonto->id);
            if (!$checkClinet || empty($this->token)) {
                return $this->redirect(['action' => 'restApi']);
            }
            $auctionData = $this->request->getData();

            foreach ($auctionData as $field => &$values) {
                if ($values == '') {
                    $values = null;
                }
            }
            $allParametrs = [];
            if(!empty($ofertaId)){
                $auctionData['id']=$ofertaId;
            }
            foreach ($auctionData['parameters'] as $key => $paramValues) {
                if (key_exists('valuesIds', $paramValues) && empty($paramValues['valuesIds'])) {
                    unset($auctionData['parameters'][$key]);
                    continue;
                } elseif (key_exists('valuesIds', $paramValues)) {
                    if (!is_array($paramValues['valuesIds'])) {
                        $paramValues['valuesIds'] = [$paramValues['valuesIds']];
                    }
                    $paramValues['values'] = [];
                    $paramValues['rangeValue'] = null;
                } elseif (key_exists('values', $paramValues) && empty($paramValues['values'])) {
                    unset($auctionData['parameters'][$key]);
                    continue;
                } elseif (key_exists('values', $paramValues)) {
                    if (!is_array($paramValues['values'])) {
                        $paramValues['values'] = [$paramValues['values']];
                    }
                    $paramValues['valuesIds'] = [];
                    $paramValues['rangeValue'] = null;
                } elseif (key_exists('rangeValue', $paramValues) && empty($paramValues['rangeValue'])) {
                    unset($auctionData['parameters'][$key]);
                    continue;
                } elseif (key_exists('rangeValue', $paramValues)) {
                    $paramValues['valuesIds'] = [];
                    $paramValues['values'] = [];
                    $fromToRange = explode('-', $paramValues['rangeValue']);
                    $paramValues['rangeValue'] = ['from' => $fromToRange[0], 'to' => $fromToRange[1]];
                }
                $allParametrs[] = $paramValues;
            }
            if (!empty($allParametrs)) {
                $auctionData['parameters'] = $allParametrs;
            }
            $auctionData['external'] = ['id' => $towar->id];
            if ($auctionData['publication']['duration'] == 'null') {
                $auctionData['publication']['duration'] = null;
            }
            if (empty($auctionData['publication']['startingAt'])) {
                $auctionData['publication']['startingAt'] = null;
            }
            if (empty($auctionData['publication']['endingAt'])) {
                $auctionData['publication']['endingAt'] = null;
            }
            if (empty($auctionData['delivery']['shipmentDate'])) {
                $auctionData['delivery']['shipmentDate'] = null;
            }
            if (empty($auctionData['delivery']['additionalInfo'])) {
                $auctionData['delivery']['additionalInfo'] = null;
            }
            if (empty($auctionData['sellingMode']['startingPrice'])) {
                $auctionData['sellingMode']['startingPrice'] = null;
            }
            if (empty($auctionData['sellingMode']['minimalPrice'])) {
                $auctionData['sellingMode']['minimalPrice'] = null;
            }
            foreach ($auctionData['promotion'] as &$promoValue) {
                if ($promoValue == '0') {
                    $promoValue = false;
                } else {
                    $promoValue = true;
                }
            }
            if (empty($auctionData['ean'])) {
                unset($auctionData['ean']);
            }
//            var_dump($auctionData);
            try {
            if(!empty($ofertaId)){
                $request = new \Imper86\AllegroApi\Rest\Model\Request('sale/offers/'.$ofertaId, 'PUT', $auctionData, null);
            }else{
                $request = new \Imper86\AllegroApi\Rest\Model\Request('sale/offers', 'POST', $auctionData, null);
            }
            $response = $this->client->sendRequest($this->token, $request);
            $results = json_decode((string) $response->getBody(), true);
            \Cake\Cache\Cache::write('newAuctionResult', $results);
            $toSaveData = ['allegro_konto_id'=>$this->konto->id,'name' => $results['name'], 'category_id' => $results['category']['id'], 'validation' => (!empty($results['validation']['errors']) ? json_encode($results['validation']['errors']) : null), 'towar_id' => $towar->id, 'item_id' => $results['id'], 'item_info' => json_encode($results)];
            $errorsMessages = [];
            $returnData = ['validation' => (!empty($results['validation']['errors']) ? json_encode($results['validation']['errors']) : null), 'towar_id' => $towar->id, 'item_id' => $results['id'], 'reloadUrl' => \Cake\Routing\Router::url(['action' => 'index', 'item' => $results['id']])];
            $allegroAukcja = $this->AllegroAukcja->patchEntity($allegroAukcja, $toSaveData);
            if (!empty($results['validation']['errors'])) {
                foreach ($results['validation']['errors'] as $error) {
                    $errorsMessages[] = $error['userMessage'];
                }
            }
            if ($this->AllegroAukcja->save($allegroAukcja)) {
                $returnArr = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Aukcja została zapisana')), 'results' => $returnData, 'errors' => (!empty($errorsMessages) ? join('; ', $errorsMessages) : null)];
            } else {
                $returnArr = ['status' => 'error', 'errors' => (!empty($errorsMessages) ? join('; ', $errorsMessages) : null)];
            }
                
            } catch (\Exception $ex) {
                \Cake\Cache\Cache::write('allegro_error_'.uniqid(), $ex->getMessage());
                $returnArr = ['status' => 'error', 'message' => $ex->getMessage()];
            }
            $this->response->type('ajax');
            $this->response->body(json_encode($returnArr));
            return $this->response;
        }
        $shippingRates = $this->getShippings();
        $shippingTimes = ['PT0S' => 'natychmiast',
            'PT24H' => '24 godziny',
            'P2D' => '2 dni',
            'P3D' => '3 dni',
            'P4D' => '4 dni',
            'P5D' => '5 dni',
            'P7D' => '7 dni',
            'P10D' => '10 dni',
            'P14D' => '14 dni',
            'P21D' => '21 dni',
            'P30D' => '30 dni',
            'P60D' => '60 dni'];
        $invoiceOptions = ['VAT' => 'faktura VAT',
            'VAT_MARGIN' => 'faktura VAT marża',
            'WITHOUT_VAT' => 'faktura bez VAT',
            'NO_INVOICE' => 'nie wystawiam faktury'];
        $promotionOptions = [
            "emphasized" => 'wyróżnienie',
            "bold" => 'pogrubienie',
            "highlight" => 'podświetlenie',
            "departmentPage" => 'oferta promowana na stronie kategorii',
            "emphasizedHighlightBoldPackage" => 'pakiet promowania: wyróżnienie, podświetlenie i pogrubienie oferty'
        ];
        $wojewodztwa = $this->wojewodztwa;
        $jednostki = ['UNIT' => 'sztuki', 'SET' => 'komplety', 'PAIR' => 'pary'];
        $sellingMode = ['BUY_NOW' => 'Kup Teraz', 'AUCTION' => 'licytacja', 'ADVERTISEMENT' => 'ogłoszenie'];
        $duration = [
            'buyNow' => ['null' => 'do wyczerpania zapasów',
                'P3D' => '3 dni',
                'P5D' => '5 dni',
                'P7D' => '7 dni',
                'P10D' => '10 dni',
                'P20D' => '20 dni'],
            'auction' => ['P3D' => '3 dni',
                'P5D' => '5 dni',
                'P7D' => '7 dni',
                'P10D' => '10 dni',
                'P20D' => '20 dni'],
            'advertisment' => ['null' => 'na czas nieokreślony - opłaty naliczane co 10 dni',
                'P10D' => '10 dni',
                'P20D' => '20 dni',
                'P30D' => '30 dni']
        ];
        $this->set(compact('allegroKonto', 'allegroAukcja', 'towar', 'shippingRates', 'shippingTimes', 'invoiceOptions', 'promotionOptions', 'wojewodztwa', 'jednostki', 'sellingMode', 'duration'));
    }

    public function getOffers($kontoId, $id = null) {
        $checkClinet = $this->setClinent($kontoId);
        if (!$checkClinet || empty($this->token)) {
            return $this->redirect(['action' => 'restApi']);
        }
        $parametrs = $this->request->getQuery('parametrs');
        if ($parametrs) {
            $parametrs = '/parameters';
        } else {
            $parametrs = '';
        }
        $request = new \Imper86\AllegroApi\Rest\Model\Request('sale/offers' . (!empty($id) ? '/' . $id : ''), 'GET', null, null);
        $response = $this->client->sendRequest($this->token, $request);
        $results = json_decode((string) $response->getBody(), true);
        if (!empty($id)) {
            $this->set('oferta', $results);
        } else {
            $this->set('oferty', $results);
        }
        $this->set('kontoId', $kontoId);
    }

    public function uploadImages($kontoId) {
        $checkClinet = $this->setClinent($kontoId);
        if (!$checkClinet || empty($this->token)) {
            return $this->redirect(['action' => 'restApi']);
        }
        $this->autoRender = false;
        $returnArr = [];
        $rqData = $this->request->getData();
        $this->loadModel('TowarZdjecie');
        $allImagesResults = [];
        if (!empty($rqData)) {
            foreach ($rqData as $zdId) {
                $zdjecie = $this->TowarZdjecie->find()->where(['TowarZdjecie.id' => $zdId])->first();
                $imageStr = file_get_contents($this->filePath['towar'] . $this->Txt->getKatalog($zdId) . DS . $zdjecie->plik);
                $imageResult = $this->client->uploadImage($this->token, $imageStr);
                $allImagesResults[$zdId] = json_decode((string) $imageResult->getBody(), true);
            }
            $view = new \Cake\View\View();
            $view->set('allImagesResults', $allImagesResults);
            $html = $view->element('admin/allegro/description_images');

            $returnArr = ['status' => 'success', 'images' => $allImagesResults, 'modal' => $html, 'message' => $this->Txt->printAdmin(__('Zdjęcia zostały zaladowane, można użyć je w opisie'))];
        } else {
            $returnArr = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Brak odpowiednich danych'))];
        }
        $this->response->type('ajax');
        $this->response->body(json_encode($returnArr));
        return $this->response;
    }

    private function getShippings() {
        $params = [
            'seller.id' => $this->konto->userId
        ];
        $request = new \Imper86\AllegroApi\Rest\Model\Request('sale/shipping-rates' . (!empty($id) ? '/' . $id : ''), 'GET', null, $params);
        $response = $this->client->sendRequest($this->token, $request);
        $results = json_decode((string) $response->getBody(), true);
        if (!empty($results["shippingRates"])) {
            return $results["shippingRates"];
        } else {
            return false;
        }
        die();
    }

    public function getDeliveryMethods($kontoId) {
        $checkClinet = $this->setClinent($kontoId);
        if (!$checkClinet || empty($this->token)) {
            return $this->redirect(['action' => 'restApi']);
        }
        $params = null;
        $request = new \Imper86\AllegroApi\Rest\Model\Request('sale/delivery-methods', 'GET', null, $params);
        $response = $this->client->sendRequest($this->token, $request);
        $results = json_decode((string) $response->getBody(), true);
        echo '<pre>';
        var_dump($results);
        echo '</pre>';
        die();
    }

    public function setShippingRate($kontoId) {
        $checkClinet = $this->setClinent($kontoId);
        if (!$checkClinet || empty($this->token)) {
            return $this->redirect(['action' => 'restApi']);
        }
        $params = [
            "name" => "Nowy cennik dostawy",
            "rates" => [
                [
                    "deliveryMethod" => [
                        "id" => "059c0d58-6cdb-4955-ab79-9031518f80f3"
                    ],
                    "maxQuantityPerPackage" => 1,
                    "firstItemRate" => [
                        "amount" => "7.99",
                        "currency" => "PLN"
                    ],
                    "nextItemRate" => [
                        "amount" => "0.00",
                        "currency" => "PLN"
                    ]
                ]
            ]
        ];
        $request = new \Imper86\AllegroApi\Rest\Model\Request('sale/shipping-rates', 'POST', $params, null);
        $response = $this->client->sendRequest($this->token, $request);
        $results = json_decode((string) $response->getBody(), true);
        echo '<pre>';
        var_dump($results);
        echo '</pre>';
        die();
    }

    public function activateOffers($kontoId=null) {
        $checkClinet = $this->setClinent($kontoId);
        if (!$checkClinet || empty($this->token)) {
            return $this->redirect(['action' => 'restApi']);
        }
        $uuid = \Ramsey\Uuid\Uuid::uuid1();
        $_UUID = $uuid->getHex();
        $dane = $this->request->getData();
        if (!empty($dane) && !empty($dane['offers'])) {
            $offers = new StringCollection();
            foreach ($dane['offers'] as $key => $offer) {
                if (empty($offer['id']) || $offer['id'] == '0') {
                    unset($dane['offers'][$key]);
                } else {
                    $offers->add($offer['id']);
                }
            }
            if (!empty($offers)) {
                try {
                    $request = new \Imper86\AllegroApi\Rest\Model\Request\OfferManagement\OfferPublicationCommands\PutOfferPublicationCommandRequest($offers, $dane['action']);
                    $response = $this->client->sendRequest($this->token, $request);
                    $this->Flash->success($this->Txt->printAdmin(__('Admin | Operacja zakończona prawidłowo, proces może zająć kilka minut')));
                } catch (\GuzzleHttp\Exception\ClientException $ex) {
                    $this->Flash->error(str_replace(["\r", "\n", "'"], ['', '', ''], $ex->getMessage()));
                }
            }
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | Brak wymaganych danych')));
        }
        return $this->redirect($this->referer());
    }

    public function index($kontoId = null) {
        $checkClinet = $this->setClinent($kontoId);
        if (!$checkClinet || empty($this->token)) {
            return $this->redirect(['action' => 'restApi']);
        }
        $page = $this->request->getQuery('page');
        if (empty($page)) {
            $page = 1;
        }
        $limit = 50;
        $filters = $this->session->read('allegroOffersFilter');
        $filter = [
            'status' => 'INACTIVE,ACTIVE,ACTIVATING,ENDED'
        ];
        if ($this->request->is('POST')) {
            $filters = $this->request->getData();
            if (!empty($filters['clear'])) {
                $filters = null;
            }
        }
        $this->session->write('allegroOffersFilter', $filters);
        if (!empty($filters)) {
            if (!empty($filters['status'])) {
                $filter['status'] = $filters['status'];
            }
            if (!empty($filters['name'])) {
                $filter['name'] = $filters['name'];
            }
        }
        $params = [
            'publication.status' => $filter['status'],
            'limit' => $limit,
            'offset' => (($page - 1) * $limit)
        ];
        if (!empty($filter['name'])) {
            $params['name'] = $filter['name'];
        }
        $request = new \Imper86\AllegroApi\Rest\Model\Request\OfferManagement\Offers\GetOffersRequest($this->token, $params);
        $response = $this->client->sendRequest($this->token, $request);
        $offers = json_decode((string) $response->getBody(), true);
        $pages = ceil($offers["totalCount"] / $limit);
        $offerStates = [
            'INACTIVE' => $this->Txt->printAdmin(__('Allegro | Nieaktywne'), 'Allegro | '),
            'ACTIVE' => $this->Txt->printAdmin(__('Allegro | Aktywne'), 'Allegro | '),
            'ACTIVATING' => $this->Txt->printAdmin(__('Allegro | W trakcie aktywacji'), 'Allegro | '),
            'ENDED' => $this->Txt->printAdmin(__('Allegro | Zakończone'), 'Allegro | ')
        ];
        $allOfferIds=[];
        if(!empty($offers)){
        foreach ($offers['offers'] as $offer){
            $allOfferIds[]=$offer['id'];
        }
        }
        $existAuction=[];
        $existAuctionNames=[];
        $existAuctionIlosc=[];
        if(!empty($allOfferIds)){
            $this->loadModel('AllegroAukcja');
            $existAuction=$this->AllegroAukcja->find('list',['keyField'=>'item_id','valueField'=>'towar_id'])->where(['item_id IN'=>$allOfferIds])->toArray();
            if(!empty($existAuction)){
                $this->loadModel('Towar');
                $existAuctionNames=$this->Towar->find('list',['keyField'=>'id','valueField'=>'nazwa'])->where(['id IN'=>$existAuction])->toArray();
                $existAuctionIlosc=$this->Towar->find('list',['keyField'=>'id','valueField'=>'ilosc'])->where(['id IN'=>$existAuction])->toArray();
            }
        }
        $allegroKonto=$this->konto;
       
        $this->set(compact('offers', 'pages', 'page', 'offerStates', 'filters','existAuction','existAuctionNames','existAuctionIlosc','allegroKonto'));
    }

    public function orders($kontoId = null) {
        $this->loadModel('AllegroZamowienie');
        $this->paginate=[
            'limit'=>50,
            'order'=>['id'=>'desc']
        ];
        $zamowienia = $this->paginate($this->AllegroZamowienie);
        $this->set(compact('zamowienia'));
        $this->set('statusy', $this->statusy);
    }
    public function orderView($orderId) {
        $this->loadModel('AllegroZamowienie');
        $zamowienie = $this->AllegroZamowienie->find('all',['contain'=>['AllegroZamowienieTowar'=>['Towar'=>['Platforma']],'ZamowienieDhl']])->where(['AllegroZamowienie.id'=>$orderId])->first();
        $this->set(compact('zamowienie'));
        $this->set('statusy', $this->statusy);
    }

    private function oldOrders(){
        $checkClinet = $this->setClinent();
        if (!$checkClinet || empty($this->token)) {
            return $this->redirect(['action' => 'restApi']);
        }
        $page = $this->request->getQuery('page');
        if (empty($page)) {
            $page = 1;
        }
        $limit = 50;
        $filters = $this->session->read('allegroOrdersFilter');
        $filter = [
            'status' => 'INACTIVE,ACTIVE,ACTIVATING,ENDED'
        ];
        if ($this->request->is('POST')) {
            $filters = $this->request->getData();
            if (!empty($filters['clear'])) {
                $filters = null;
            }
        }
        $this->session->write('allegroOrdersFilter', $filters);
        if (!empty($filters)) {
            if (!empty($filters['status'])) {
                $filter['status'] = $filters['status'];
            }
            if (!empty($filters['name'])) {
                $filter['name'] = $filters['name'];
            }
        }
        $params = [
            'publication.status' => $filter['status'],
            'limit' => $limit,
            'offset' => (($page - 1) * $limit)
        ];
        if (!empty($filter['name'])) {
            $params['name'] = $filter['name'];
        }
        $request = new \Imper86\AllegroApi\Rest\Model\Request\OrderManagement\GetOrderCheckoutFormsRequest($params['offset'], $params['limit']);
        $response = $this->client->sendRequest($this->token, $request);
        $orders = json_decode((string) $response->getBody(), true);
        $pages = ceil($orders["totalCount"] / $limit);
        $offerStates = [
            'INACTIVE' => $this->Txt->printAdmin(__('Allegro | Nieaktywne'), 'Allegro | '),
            'ACTIVE' => $this->Txt->printAdmin(__('Allegro | Aktywne'), 'Allegro | '),
            'ACTIVATING' => $this->Txt->printAdmin(__('Allegro | W trakcie aktywacji'), 'Allegro | '),
            'ENDED' => $this->Txt->printAdmin(__('Allegro | Zakończone'), 'Allegro | ')
        ];
        $this->set(compact('orders', 'pages', 'page', 'offerStates', 'filters'));
    }
    
    public function orderUpdate($id = null, $type = null) {
        $this->autoRender = false;
        $this->loadModel('AllegroZamowienie');
        $returnArray = [];
        $zamowienie = $this->AllegroZamowienie->find('all', ['conditions' => ['AllegroZamowienie.id' => $id], 'contain' => ['AllegroZamowienieTowar']])->first();
        Configure::write('currencyDefault', $zamowienie->waluta);
        if (!empty($zamowienie)) {
            if ($this->request->is('post')) {
                $successMessage = $this->Txt->printAdmin(__('Admin | Dane zostały zapisane'));
                $rqData = $this->request->getData();
                $returnValues = [];
                if (!empty($rqData)) {
                    $afterSaveAction = null;
                    if (!empty($type)) {
                        switch ($type) {
                            case 'status': {
                                    $zamowienie->status = $rqData['status'];
                                    if ($rqData['status'] == 'przyjete') {
                                            $zamowienie->set('termin_wysylki',$rqData['termin_wysylki']);
                                    }
                                    if($rqData['status']=='wyslane'){
                                        if(!empty($rqData['kurier_typ'])){
                                            $zamowienie->set('kurier_typ',$rqData['kurier_typ']);
                                        }
                                        if(!empty($rqData['nr_listu_przewozowego'])){
                                            $zamowienie->set('nr_listu_przewozowego',$rqData['nr_listu_przewozowego']);
                                        }
                                    }
                                    $successMessage = $this->Txt->printAdmin(__('Admin | Status został zmieniony'));
                                    $afterSaveAction = ['action' => 'changeStatus'];
                                }break;
                        }
                    } else {
                        foreach ($rqData as $key => $value) {
                            $zamowienie->{$key} = $value;
                        }
                    }
                    if ($this->AllegroZamowienie->save($zamowienie)) {
                        if (!empty($afterSaveAction)) {
                            $this->{$afterSaveAction['action']}($zamowienie);
                        }
                        $returnArray = [
                            'status' => 'success',
                            'message' => $successMessage,
                            'values' => $returnValues
                        ];
                    } else {
                        $returnArray = [
                            'status' => 'error',
                            'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')),
                            'errors' => $zamowienie->errors()
                        ];
                    }
                } else {
                    $returnArray = [
                        'status' => 'error',
                        'message' => $this->Txt->printAdmin(__('Admin | Brak danych do zapisu'))
                    ];
                }
            } else {
                $returnArray = [
                    'status' => 'error',
                    'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowe rządanie'))
                ];
            }
        } else {
            $returnArray = [
                'status' => 'error',
                'message' => $this->Txt->printAdmin(__('Admin | Błędny identyfikator zamówienia'))
            ];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnArray));
        return $this->response;
    }
    
    private function changeStatus($zamowienie) {
        if (empty($zamowienie)) {
            return false;
        }
        $return = true;
        \Cake\I18n\I18n::locale($zamowienie->lng);
        $this->loadComponent('Mail');
        $this->loadComponent('Replace');
        $this->loadModel('Szablon');
        $replace = [
            'zamowienie-id' => $zamowienie->id,
            'zamowienie-numer' => (!empty($zamowienie->order_id) ? $zamowienie->order_id : $zamowienie->id),
            'zamowienie-imie' => $zamowienie->wysylka_imie,
            'zamowienie-nazwisko' => $zamowienie->wysylka_nazwisko,
            'zamowienie-adres-wysylki' => $this->Txt->printAddres($zamowienie->toArray(),'<br/>','wysylka_'),
            'zamowienie-adres-faktury' => $this->Txt->printAddres($zamowienie->toArray(),'<br/>','faktura_'),
            'zamowienie-email' => $zamowienie->kupujacy_email,
            'zamowienie-telefon' => $zamowienie->kupujacy_telefon,
            'zamowienie-wartosc-razem' => $this->Txt->cena($zamowienie->wartosc_razem),
            'zamowienie-wartosc-produktow' => $this->Txt->cena($zamowienie->wartosc-$zamowienie->allegro_wysylka_koszt),
            'zamowienie-waluta' => $zamowienie->waluta,
            'zamowienie-uwagi' => nl2br($zamowienie->uwagi),
            'zamowienie-status' => $this->Translation->get('statusy_zamowien.' . $zamowienie->status),
            'zamowienie-data' => (!empty($zamowienie->data_zakupu) ? $zamowienie->data_zakupu->format('Y-m-d H:i') : ''),
            'termin-wysylki' => (!empty($zamowienie->termin_wysylki) ? $zamowienie->termin_wysylki->format('Y-m-d') : ''),
            'link-sledzenia' => $this->Txt->kurierLink($zamowienie->kurier_typ,$zamowienie->nr_listu_przewozowego),
            'nr-listu-przewozowego' => $zamowienie->nr_listu_przewozowego,
        ];
        $message = '';
        $szablon = null;
        $temat = '';
        switch ($zamowienie->status) {
            case 'wyslane' : {
                    $szablon = $this->Szablon->findByNazwa('zamowienie_status_wyslane')->first();
                    $message = $this->Replace->getTemplate($szablon->tresc, $replace, $zamowienie->allegro_zamowienie_towar);
                } break;
            case 'przyjete' : {
                    $szablon = $this->Szablon->findByNazwa('zamowienie_status_przyjete')->first();
                    $message = $this->Replace->getTemplate($szablon->tresc, $replace, $zamowienie->allegro_zamowienie_towar);
                } break;
            case 'zrealizowane' : {
                    $szablon = $this->Szablon->findByNazwa('zamowienie_status_zrealizowane')->first();
                    $message = $this->Replace->getTemplate($szablon->tresc, $replace, $zamowienie->allegro_zamowienie_towar);
                } break;
            case 'anulowane' : {
                    $szablon = $this->Szablon->findByNazwa('zamowienie_status_anulowane')->first();
                    $message = $this->Replace->getTemplate($szablon->tresc, $replace, $zamowienie->allegro_zamowienie_towar);
                } break;
        }
        if (!empty($message) && !empty($szablon)) {

            $temat = $this->Replace->getTemplate($szablon->temat, $replace);
            $sendTo = [];
            $sendToPh = Configure::read('admin.ph_info');
            if (!empty($sendToPh)) {
                $phEmail = $zamowienie->administrator->email;
                if (!empty($phEmail)) {
                    $sendTo[] = $phEmail;
                }
            }
            $sendTo[] = $zamowienie->kupujacy_email;
            $this->Mail->sendMail($sendTo, $temat, $message, ['replyTo' => ((!empty($sendToPh) && !empty($phEmail)) ? $phEmail : Configure::read('mail.domyslny_email'))]);
        }
        return $return;
    }
    public function powiaz($kontoId,$aukcjaId){
        $this->autoRender=false;
        $return=[];
        $this->loadModel('AllegroAukcja');
        $checkClinet = $this->setClinent($kontoId);
        if (!$checkClinet || empty($this->token)) {
            return $this->redirect(['action' => 'restApi']);
        }
        $towarId=$this->request->getData('towar_id');
        if(!empty($aukcjaId) && !empty($towarId)){
        $request = new \Imper86\AllegroApi\Rest\Model\Request('sale/offers' . (!empty($aukcjaId) ? '/' . $aukcjaId : ''), 'GET', null, null);
        $response = $this->client->sendRequest($this->token, $request);
        $oferta = json_decode((string) $response->getBody(), true);
        if(!empty($oferta)){
            $newAuction = [
                'allegro_konto_id'=>$this->konto->id,
                'towar_id'=>$towarId,
                'item_id'=>$oferta['id'],
                'item_info'=> json_encode($oferta),
                'name'=>$oferta['name'],
                'category_id'=>$oferta['category']['id']
            ];
            $entity = $this->AllegroAukcja->newEntity($newAuction);
            if($this->AllegroAukcja->save($entity)){
                $return = ['status'=>'success','message'=>$this->Txt->printAdmin(__('Admin | Aukcja została powiązana')),'item_id'=>$oferta['id'],'towar_id'=>$towarId];
            }else{
                $return = ['status'=>'error','message'=>$this->Txt->printAdmin(__('Admin | Błąd zapisu danych')),'item_id'=>$oferta['id'],'towar_id'=>$towarId];
            }
        }else{
            $return = ['status'=>'error','message'=>$this->Txt->printAdmin(__('Admin | Nie znaleziono aukcji allegro'))];
        }
        }else{
                $return = ['status'=>'error','message'=>$this->Txt->printAdmin(__('Admin | Nieprawidłowe dane'))];
            
        }
        $this->response->type('ajax');
        $this->response->body(json_encode($return));
        return $this->response;
    }
    public function forceActualization(){
//        $this->Flash->error($this->Txt->printAdmin(__('Admin | Opcja tymczasowo zablokowana')));
//        return $this->redirect($this->request->referer());
        Log::write('info', 'Allegro full synchro start: '.date('Y-m-d H:i:s'), ['cronInfo']);
        $query = "UPDATE `allegro_aukcja` a set a.`iloscToUpdate`=(SELECT if(t.ilosc <= 0,-10000,t.ilosc) as ilosc FROM towar t WHERE t.id=a.towar_id),a.`priceToUpdate`=(SELECT tc.cena_sprzedazy as cena FROM towar_cena tc WHERE tc.towar_id = a.towar_id) WHERE 1";
        
            $connection = \Cake\Datasource\ConnectionManager::get('default');
            $results = $connection->execute($query);
            if($results){
                $this->Flash->success($this->Txt->printAdmin(__('Admin | Operacja została zlecona może zająć kilkanaście minut')));
            }
            return $this->redirect($this->request->referer());
    }
    public function unlink($towarId,$aukcjaId){
        $this->loadModel('AllegroAukcja');
        if(!empty($towarId) && !empty($aukcjaId)){
            $aukcja=$this->AllegroAukcja->find('all')->where(['item_id'=>$aukcjaId,'towar_id'=>$towarId])->first();
            if(!empty($aukcja)){
                if($this->AllegroAukcja->delete($aukcja)){
                    $this->Flash->success($this->Txt->printAdmin(__('Admin | Produkty zostały rozłączone')));
                }else{
                    $this->Flash->error($this->Txt->printAdmin(__('Admin | Nie udało się rozłączyć produktów')));
                }
            }else{
                $this->Flash->error($this->Txt->printAdmin(__('Admin | Brak aukcji o podanym identyfikatorze')));
            }
        }else{
            $this->Flash->error($this->Txt->printAdmin(__('Admin | Nieprawidłowe zapytanie')));
        }
                
        return $this->redirect($this->request->referer());
    }
}
