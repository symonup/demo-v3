<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Gatunek Controller
 *
 * @property \App\Model\Table\GatunekTable $Gatunek
 *
 * @method \App\Model\Entity\Gatunek[] paginate($object = null, array $settings = [])
 */
class GatunekController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->loadComponent('Upload');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $cfgArr = [];
        $gatunek = $this->Gatunek->find('all', $cfgArr);

        $this->set(compact('gatunek'));
        $this->set('_serialize', ['gatunek']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $gatunek = $this->Gatunek->newEntity();
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            $path = $this->filePath['gatunek'];
            if (!empty($rqData['zdjecie']['name'])) {
                $fileName = $this->Upload->saveFile(
                        $path, $rqData['zdjecie']);
                if (!empty($fileName)) {
                    $rqData['zdjecie'] = $fileName;
                } else {
                    $rqData['zdjecie'] = null;
                }
            } else
                $rqData['zdjecie'] = null;
            $gatunek = $this->Gatunek->patchEntity($gatunek, $rqData, ['translations' => $this->Gatunek->hasBehavior('Translate')]);
            if ($this->Gatunek->save($gatunek)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | gatunek'))])));

                return $this->redirect(['action' => 'index']);
            } else {
                if (!empty($fileName)) {
                    $this->Upload->deleteFile($path . $fileName);
                }
                $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | gatunek'))])));
            }
        }
        $this->set(compact('gatunek'));
        $this->set('_serialize', ['gatunek']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Gatunek id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {

        if ($this->Gatunek->hasBehavior('Translate')) {
            $gatunek = $this->Gatunek->find('translations', [
                        'locales' => $this->lngIdList,
                        'conditions' => ['Gatunek.' . $this->Gatunek->primaryKey() => $id],
                        'contain' => ['Towar']
                    ])->first();
        } else {
            $gatunek = $this->Gatunek->find('all', [
                        'conditions' => ['Gatunek.' . $this->Gatunek->primaryKey() => $id],
                        'contain' => ['Towar']
                    ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData = $this->request->getData();
            $path = $this->filePath['gatunek'];
            $oldFile = null;
            $fileName = null;
            if (empty($rqData['zdjecie']['name'])) {
                $rqData['zdjecie'] = $gatunek->zdjecie;
            } else {
                $fileName = $this->Upload->saveFile(
                        $path, $rqData['zdjecie']);
                if (!empty($fileName)) {
                    $rqData['zdjecie'] = $fileName;
                    $oldFile = $gatunek->zdjecie;
                }
            }
            $gatunek = $this->Gatunek->patchEntity($gatunek, $rqData, ['translations' => $this->Gatunek->hasBehavior('Translate')]);
            if ($this->Gatunek->save($gatunek)) {
                if (!empty($oldFile)) {
                    $this->Upload->deleteFile($path . $oldFile);
                }
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | gatunek'))])));

                return $this->redirect(['action' => 'index']);
            } else {
                if (!empty($fileName)) {
                    $this->Upload->deleteFile($path . $fileName);
                }
                $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | gatunek'))])));
            }
        }
        $_gatunek = $gatunek->toArray();
        if ($this->Gatunek->hasBehavior('Translate') && empty($_gatunek['_translations'])) {
            $transFields = $this->Gatunek->associations()->keys();
            $tFields = [];
            foreach ($transFields as $field) {
                if (strpos($field, '_translation') !== false) {
                    $field = substr($field, (strpos($field, '_') + 1));
                    $field = str_replace('_translation', '', $field);
                    $tFields[$field] = $_gatunek[$field];
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $gatunek->set('_translations', $translation);
        }
        $this->set(compact('gatunek'));
        $this->set('_serialize', ['gatunek']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Gatunek id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $gatunek = $this->Gatunek->get($id);
        $fileName=$gatunek->zdjecie;
        $path = $this->filePath['gatunek'];
        if ($this->Gatunek->delete($gatunek)) {
                if (!empty($fileName)) {
                    $this->Upload->deleteFile($path . $fileName);
                }
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.', [$this->Txt->printAdmin(__('Admin | gatunek'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.', [$this->Txt->printAdmin(__('Admin | gatunek'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * setField method
     *
     * @param string|null $id Gatunek id.
     * @param string|null $field Gatunek field name.
     * @param mixed $value value to set.
     */
    public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->Gatunek->find('all', ['conditions' => ['Gatunek.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                    $item->{$field} = $value;
                    if ($this->Gatunek->save($item)) {
                        $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, (!empty($value)?0:1)])), 'action' => $retAction];
                    } else {
                        $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                    }
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    /**
     * function sort
     * 
     */
    public function sort($field = 'kolejnosc') {
        $this->autoRender = false;
        $returnData = [];
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            foreach ($rqData as $data) {
                if (!empty($data['id'])) {
                    $this->Gatunek->updateAll($data, ['id' => $data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

}
