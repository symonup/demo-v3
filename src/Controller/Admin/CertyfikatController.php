<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Certyfikat Controller
 *
 * @property \App\Model\Table\CertyfikatTable $Certyfikat
 *
 * @method \App\Model\Entity\Certyfikat[] paginate($object = null, array $settings = [])
 */
class CertyfikatController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
    $cfgArr=[];
$certyfikat = $this->Certyfikat->find('all',$cfgArr);

        $this->set(compact('certyfikat'));
        $this->set('_serialize', ['certyfikat']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $certyfikat = $this->Certyfikat->newEntity();
        if ($this->request->is('post')) {
        $rqData=$this->request->getData();
            $certyfikat = $this->Certyfikat->patchEntity($certyfikat, $rqData,['translations'=>$this->Certyfikat->hasBehavior('Translate')]);
            if ($this->Certyfikat->save($certyfikat)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | certyfikat'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | certyfikat'))])));
        }
        $this->set(compact('certyfikat'));
        $this->set('_serialize', ['certyfikat']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Certyfikat id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
    
    if($this->Certyfikat->hasBehavior('Translate')){
        $certyfikat = $this->Certyfikat->find('translations', [
            'locales' => $this->lngIdList,
            'conditions' => ['Certyfikat.'.$this->Certyfikat->primaryKey() => $id],
            'contain' => ['Towar']
        ])->first();
        }
        else{
        $certyfikat = $this->Certyfikat->find('all', [
            'conditions'=>['Certyfikat.'.$this->Certyfikat->primaryKey()=>$id],
            'contain' => ['Towar']
        ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData=$this->request->getData();
            $certyfikat = $this->Certyfikat->patchEntity($certyfikat, $rqData,['translations'=>$this->Certyfikat->hasBehavior('Translate')]);
            if ($this->Certyfikat->save($certyfikat)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | certyfikat'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | certyfikat'))])));
        }
        
        $_certyfikat = $certyfikat->toArray();
        if ($this->Certyfikat->hasBehavior('Translate') && empty($_certyfikat['_translations'])) {
            $transFields=$this->Certyfikat->associations()->keys();
            $tFields=[];
            foreach ($transFields as $field){
                if(strpos($field, '_translation')!==false){
                    $field= substr($field, (strpos($field, '_')+1));
                    $field=str_replace('_translation','', $field);
                    $tFields[$field]=$_certyfikat[$field]; 
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $certyfikat->set('_translations',$translation);
        }
        
        
        $this->set(compact('certyfikat'));
        $this->set('_serialize', ['certyfikat']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Certyfikat id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $certyfikat = $this->Certyfikat->get($id);
        if ($this->Certyfikat->delete($certyfikat)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.',[$this->Txt->printAdmin(__('Admin | certyfikat'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.',[$this->Txt->printAdmin(__('Admin | certyfikat'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

/**
     * setField method
     *
     * @param string|null $id Certyfikat id.
     * @param string|null $field Certyfikat field name.
     * @param mixed $value value to set.
     */

public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->Certyfikat->find('all', ['conditions' => ['Certyfikat.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                        $item->{$field} = $value;
                        if ($this->Certyfikat->save($item)) {
                            $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, (!empty($value)?0:1)])), 'action' => $retAction];
                        } else {
                            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                        }
                    
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    /**
* function sort
* 
*/
public function sort($field='kolejnosc'){
        $this->autoRender=false;
        $returnData=[];
        if($this->request->is('post')){
            $rqData=$this->request->getData();
            foreach ($rqData as $data){
                if(!empty($data['id'])){
                    $this->Certyfikat->updateAll($data, ['id'=>$data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    }
