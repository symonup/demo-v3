<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * HelpInfo Controller
 *
 * @property \App\Model\Table\HelpInfoTable $HelpInfo
 *
 * @method \App\Model\Entity\HelpInfo[] paginate($object = null, array $settings = [])
 */
class HelpInfoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
    $cfgArr=[];
$helpInfo = $this->HelpInfo->find('all',$cfgArr);

        $this->set(compact('helpInfo'));
        $this->set('_serialize', ['helpInfo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $helpInfo = $this->HelpInfo->newEntity();
        if ($this->request->is('post')) {
        $rqData=$this->request->getData();
            $helpInfo = $this->HelpInfo->patchEntity($helpInfo, $rqData,['translations'=>$this->HelpInfo->hasBehavior('Translate')]);
            if ($this->HelpInfo->save($helpInfo)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | help info'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | help info'))])));
        }
        $this->set(compact('helpInfo'));
        $this->set('_serialize', ['helpInfo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Help Info id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
    
    if($this->HelpInfo->hasBehavior('Translate')){
        $helpInfo = $this->HelpInfo->find('translations', [
            'locales' => $this->lngIdList,
            'conditions' => ['HelpInfo.'.$this->HelpInfo->primaryKey() => $id],
            'contain' => []
        ])->first();
        }
        else{
        $helpInfo = $this->HelpInfo->find('all', [
            'conditions'=>['HelpInfo.'.$this->HelpInfo->primaryKey()=>$id],
            'contain' => []
        ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData=$this->request->getData();
            $helpInfo = $this->HelpInfo->patchEntity($helpInfo, $rqData,['translations'=>$this->HelpInfo->hasBehavior('Translate')]);
            if ($this->HelpInfo->save($helpInfo)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | help info'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | help info'))])));
        }
        
        $_helpInfo = $helpInfo->toArray();
        if ($this->HelpInfo->hasBehavior('Translate') && empty($_helpInfo['_translations'])) {
            $transFields=$this->HelpInfo->associations()->keys();
            $tFields=[];
            foreach ($transFields as $field){
                if(strpos($field, '_translation')!==false){
                    $field= substr($field, (strpos($field, '_')+1));
                    $field=str_replace('_translation','', $field);
                    $tFields[$field]=$_helpInfo[$field]; 
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $helpInfo->set('_translations',$translation);
        }
        
        
        $this->set(compact('helpInfo'));
        $this->set('_serialize', ['helpInfo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Help Info id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $helpInfo = $this->HelpInfo->get($id);
        if ($this->HelpInfo->delete($helpInfo)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.',[$this->Txt->printAdmin(__('Admin | help info'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.',[$this->Txt->printAdmin(__('Admin | help info'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

/**
     * setField method
     *
     * @param string|null $id Help Info id.
     * @param string|null $field Help Info field name.
     * @param mixed $value value to set.
     */

public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->HelpInfo->find('all', ['conditions' => ['HelpInfo.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                        $item->{$field} = $value;
                        if ($this->HelpInfo->save($item)) {
                            $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, (!empty($value)?0:1)])), 'action' => $retAction];
                        } else {
                            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                        }
                    
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    /**
* function sort
* 
*/
public function sort($field='kolejnosc'){
        $this->autoRender=false;
        $returnData=[];
        if($this->request->is('post')){
            $rqData=$this->request->getData();
            foreach ($rqData as $data){
                if(!empty($data['id'])){
                    $this->HelpInfo->updateAll($data, ['id'=>$data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    }
