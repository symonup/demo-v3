<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Opakowanie Controller
 *
 * @property \App\Model\Table\OpakowanieTable $Opakowanie
 *
 * @method \App\Model\Entity\Opakowanie[] paginate($object = null, array $settings = [])
 */
class OpakowanieController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
    $cfgArr=[];
$opakowanie = $this->Opakowanie->find('all',$cfgArr);

        $this->set(compact('opakowanie'));
        $this->set('_serialize', ['opakowanie']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $opakowanie = $this->Opakowanie->newEntity();
        if ($this->request->is('post')) {
        $rqData=$this->request->getData();
            $opakowanie = $this->Opakowanie->patchEntity($opakowanie, $rqData,['translations'=>$this->Opakowanie->hasBehavior('Translate')]);
            if ($this->Opakowanie->save($opakowanie)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | opakowanie'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | opakowanie'))])));
        }
        $this->set(compact('opakowanie'));
        $this->set('_serialize', ['opakowanie']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Opakowanie id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
    
    if($this->Opakowanie->hasBehavior('Translate')){
        $opakowanie = $this->Opakowanie->find('translations', [
            'locales' => $this->lngIdList,
            'conditions' => ['Opakowanie.'.$this->Opakowanie->primaryKey() => $id],
            'contain' => []
        ])->first();
        }
        else{
        $opakowanie = $this->Opakowanie->find('all', [
            'conditions'=>['Opakowanie.'.$this->Opakowanie->primaryKey()=>$id],
            'contain' => []
        ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData=$this->request->getData();
            $opakowanie = $this->Opakowanie->patchEntity($opakowanie, $rqData,['translations'=>$this->Opakowanie->hasBehavior('Translate')]);
            if ($this->Opakowanie->save($opakowanie)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | opakowanie'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | opakowanie'))])));
        }
        
        $_opakowanie = $opakowanie->toArray();
        if ($this->Opakowanie->hasBehavior('Translate') && empty($_opakowanie['_translations'])) {
            $transFields=$this->Opakowanie->associations()->keys();
            $tFields=[];
            foreach ($transFields as $field){
                if(strpos($field, '_translation')!==false){
                    $field= substr($field, (strpos($field, '_')+1));
                    $field=str_replace('_translation','', $field);
                    $tFields[$field]=$_opakowanie[$field]; 
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $opakowanie->set('_translations',$translation);
        }
        
        
        $this->set(compact('opakowanie'));
        $this->set('_serialize', ['opakowanie']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Opakowanie id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $opakowanie = $this->Opakowanie->get($id);
        if ($this->Opakowanie->delete($opakowanie)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.',[$this->Txt->printAdmin(__('Admin | opakowanie'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.',[$this->Txt->printAdmin(__('Admin | opakowanie'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

/**
     * setField method
     *
     * @param string|null $id Opakowanie id.
     * @param string|null $field Opakowanie field name.
     * @param mixed $value value to set.
     */

public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->Opakowanie->find('all', ['conditions' => ['Opakowanie.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                        $item->{$field} = $value;
                        if ($this->Opakowanie->save($item)) {
                            $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, (!empty($value)?0:1)])), 'action' => $retAction];
                        } else {
                            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                        }
                    
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    /**
* function sort
* 
*/
public function sort($field='kolejnosc'){
        $this->autoRender=false;
        $returnData=[];
        if($this->request->is('post')){
            $rqData=$this->request->getData();
            foreach ($rqData as $data){
                if(!empty($data['id'])){
                    $this->Opakowanie->updateAll($data, ['id'=>$data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    }
