<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Uzytkownik Controller
 *
 * @property \App\Model\Table\UzytkownikTable $Uzytkownik
 *
 * @method \App\Model\Entity\Uzytkownik[] paginate($object = null, array $settings = [])
 */
class UzytkownikController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index($type = 'b2c') {
        $cfgArr = [
            'conditions' => ['Uzytkownik.typ' => $type]
        ];

        if ($this->Auth->user('role_id') > 1) {
            $cfgArr['conditions']['Uzytkownik.administrator_id'] = $this->Auth->user('id');
        }
        $uzytkownik = $this->Uzytkownik->find('all', $cfgArr);

        $this->set(compact('uzytkownik', 'type'));
        $this->set('_serialize', ['uzytkownik']);
    }

    public function setApi($id) {
        $user = $this->Uzytkownik->find()->where(['id' => $id, 'typ' => 'b2b'])->first();
        if (empty($user->api_token)) {
            $user->set('api_token', sha1(uniqid(uniqid($user->token))));
        }
        return $this->redirect($this->request->referer());
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($type = null) {
        $uzytkownik = $this->Uzytkownik->newEntity();
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            $rqData['data_rejestracji'] = new \DateTime(date('Y-m-d H:i:s'));
            $rqData['token'] = uniqid();
            if($rqData['typ']=='b2b'){
                $fvAdres=[
                    'typ'=>2,
                    'imie'=>$rqData['imie'],
                    'nazwisko'=>$rqData['nazwisko'],
                    'firma'=>$rqData['firma'],
                    'nip'=>$rqData['nip'],
                    'ulica'=>$rqData['firma_ulica'],
                    'nr_domu'=>$rqData['firma_nr_domu'],
                    'nr_lokalu'=>$rqData['firma_nr_lokalu'],
                    'kod'=>$rqData['firma_kod'],
                    'miasto'=>$rqData['firma_miasto'],
                    'kraj'=>'PL'
                ];
                $rqData['uzytkownik_adres']=[$fvAdres];
            }
            if (!empty($rqData['api'])) {
                    $rqData['api_token'] = sha1(uniqid(uniqid($rqData['token'])));
            } else {
                $rqData['api_token'] = null;
            }
            $uzytkownik = $this->Uzytkownik->patchEntity($uzytkownik, $rqData);
            if ($this->Uzytkownik->save($uzytkownik)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | użytkownik'))])));

                return $this->redirect(['action' => 'index', $uzytkownik->typ]);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | użytkownik'))])));
        }
        $cond = [];
        if ($this->Auth->user('role_id') > 1) {
            $cond = ['id' => $this->Auth->user('id')];
        }
        if (!empty($type)) {
            $uzytkownik->typ = $type;
        }
        $this->set(compact('uzytkownik'));
        $this->set('_serialize', ['uzytkownik']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Uzytkownik id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {

        $uzytkownik = $this->Uzytkownik->find('all', [
                    'conditions' => ['Uzytkownik.' . $this->Uzytkownik->primaryKey() => $id]
                ])->first();
        if (empty($uzytkownik)) {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | Brak klienta o podanym identyfikatorze')));
            return $this->redirect(['action' => 'index']);
        }
        $oldActive = $uzytkownik->aktywny;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData = $this->request->getData();
            if (!empty($rqData['api'])) {
                if (empty($uzytkownik->api_token)) {
                    $rqData['api_token'] = sha1(uniqid(uniqid($uzytkownik->token)));
                }
            } elseif (!empty($uzytkownik->api_token)) {
                $rqData['api_token'] = null;
            }

            $uzytkownik = $this->Uzytkownik->patchEntity($uzytkownik, $rqData, ['translations' => $this->Uzytkownik->hasBehavior('Translate')]);
            if ($this->Uzytkownik->save($uzytkownik)) {

                if (empty($oldActive) && !empty($uzytkownik->aktywny)) {
                    $ph = $this->Uzytkownik->Administrator->find('all', ['conditions' => ['Administrator.id' => $uzytkownik->administrator_id]])->first();
                    $this->loadComponent('Mail');
                    $this->loadComponent('Replace');
                    $this->loadModel('Szablon');

                    $replace = [
                        'uzytkownik-id' => $uzytkownik->id,
                        'uzytkownik-imie' => $uzytkownik->imie,
                        'uzytkownik-nazwisko' => $uzytkownik->nazwisko,
                        'uzytkownik-email' => $uzytkownik->email,
                        'uzytkownik-firma' => $uzytkownik->firma,
                        'uzytkownik-nip' => $uzytkownik->nip,
                        'uzytkownik-telefon' => (!empty($uzytkownik->kierunkowy) ? $uzytkownik->kierunkowy . ' ' . str_replace($uzytkownik->kierunkowy, '', $uzytkownik->telefon) : $uzytkownik->telefon),
                        'uzytkownik-data-rejestracji' => (!empty($uzytkownik->data_rejestracji) ? $uzytkownik->data_rejestracji->format('Y-m-d H:i') : ''),
                    ];
                    $szablon = $this->Szablon->findByNazwa('aktywacja_konta')->first();
                    $message = $this->Replace->getTemplate($szablon->tresc, $replace);
                    $this->Mail->sendMail($uzytkownik->email, $szablon->temat, $message, ['replyTo' => (!empty($ph) ? $ph->email : Configure::read('mail.domyslny_email'))]);
                }
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | użytkownik'))])));

                return $this->redirect(['action' => 'index', $uzytkownik->typ]);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | użytkownik'))])));
        }
        $uzytkownik->set('password', '');
        $uzytkownik->set('api', (!empty($uzytkownik->api_token)));
        $this->set(compact('uzytkownik', 'waluta', 'administrator'));
        $this->set('_serialize', ['uzytkownik']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Uzytkownik id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $uzytkownik = $this->Uzytkownik->get($id);
        $type=$uzytkownik->typ;
        if ($this->Uzytkownik->delete($uzytkownik)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.', [$this->Txt->printAdmin(__('Admin | uzytkownik'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.', [$this->Txt->printAdmin(__('Admin | uzytkownik'))])));
        }
        return $this->redirect(['action' => 'index',$type]);
    }

    /**
     * setField method
     *
     * @param string|null $id Uzytkownik id.
     * @param string|null $field Uzytkownik field name.
     * @param mixed $value value to set.
     */
    public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->Uzytkownik->find('all', ['conditions' => ['Uzytkownik.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                    $item->{$field} = $value;
                    if ($this->Uzytkownik->save($item)) {
                        $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, !(bool) $value])), 'action' => $retAction];
                    } else {
                        $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                    }
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    /**
     * function sort
     * 
     */
    public function sort($field = 'kolejnosc') {
        $this->autoRender = false;
        $returnData = [];
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            foreach ($rqData as $data) {
                if (!empty($data['id'])) {
                    $this->Uzytkownik->updateAll($data, ['id' => $data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

}
