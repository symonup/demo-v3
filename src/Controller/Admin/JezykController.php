<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Jezyk Controller
 *
 * @property \App\Model\Table\JezykTable $Jezyk
 *
 * @method \App\Model\Entity\Jezyk[] paginate($object = null, array $settings = [])
 */
class JezykController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $cfgArr = [];
        $cfgArr = [
            'contain' => ['Waluta']
        ];
        $jezyk = $this->Jezyk->find('all', $cfgArr);

        $this->set(compact('jezyk'));
        $this->set('_serialize', ['jezyk']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $jezyk = $this->Jezyk->newEntity();
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            $this->loadComponent('Upload');
         $path = $this->filePath['lang'];
            if (!empty($rqData['flaga']['name'])) {
                $fileName = $this->Upload->saveFile(
                        $path, $rqData['flaga']);
                if (!empty($fileName)) {
                    $rqData['flaga'] = $fileName;
                } else {
                    $rqData['flaga'] = null;
                }
            } else
                $rqData['flaga'] = null;
            $jezyk = $this->Jezyk->patchEntity($jezyk, $rqData, ['translations' => $this->Jezyk->hasBehavior('Translate')]);
            if ($this->Jezyk->save($jezyk)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | jezyk'))])));

                return $this->redirect(['action' => 'index']);
            }else{
                if (!empty($fileName)) {
                    $this->Upload->deleteFile($path . $fileName);
                }
                }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | jezyk'))])));
        }
        $waluta = $this->Jezyk->Waluta->find('list',['keyField'=>'id','valueField'=>'symbol'])->toArray();
        $this->set(compact('jezyk', 'waluta'));
        $this->set('_serialize', ['jezyk']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Jezyk id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {

        if ($this->Jezyk->hasBehavior('Translate')) {
            $jezyk = $this->Jezyk->find('translations', [
                        'locales' => $this->lngIdList,
                        'conditions' => ['Jezyk.' . $this->Jezyk->primaryKey() => $id],
                        'contain' => []
                    ])->first();
        } else {
            $jezyk = $this->Jezyk->find('all', [
                        'conditions' => ['Jezyk.' . $this->Jezyk->primaryKey() => $id],
                        'contain' => []
                    ])->first();
        }
        $oldDefault=$jezyk->domyslny;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->loadComponent('Upload');
            $rqData = $this->request->getData();
            $setDefault=false;
            if(empty($oldDefault) && !empty($rqData['domyslny'])){
                $setDefault=true;
            }
            if(!empty($oldDefault) && empty($rqData['domyslny'])){
                $rqData['domyslny']=1;
                $this->Flash->error($this->Txt->printAdmin(__('Admin | Zmiany domyślnego języka można dokonać tylko przez ustawienie tej opcji w języku który nie jest domyślny', [$this->Txt->printAdmin(__('Admin | jezyk'))])));
            }
            if(!empty($rqData['domyslny'])){
                $rqData['aktywny']=1;
            }
            $oldFile = null;
                $path = $this->filePath['lang'];
            
            if (empty($rqData['flaga']['name'])) {
                $rqData['flaga'] = $jezyk->flaga;
            } else {
                $fileName = $this->Upload->saveFile(
                        $path, $rqData['flaga']);
                if (!empty($fileName)) {
                    $rqData['flaga'] = $fileName;
                    $oldFile = $jezyk->flaga;
                }
            }
            $jezyk = $this->Jezyk->patchEntity($jezyk, $rqData, ['translations' => $this->Jezyk->hasBehavior('Translate')]);
            if ($this->Jezyk->save($jezyk)) {
                if($setDefault){
                    $this->Jezyk->updateAll(['domyslny'=>0], ['id !='=>$jezyk->id]);
                }
                if (!empty($oldFile))
                { 
                    $this->Upload->deleteFile($path . $oldFile);
                }
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | jezyk'))])));

                return $this->redirect(['action' => 'index']);
            }else{
                if (!empty($fileName)) {
                    $this->Upload->deleteFile($path . $fileName);
                }
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | jezyk'))])));
        }

        $_jezyk = $jezyk->toArray();
        if ($this->Jezyk->hasBehavior('Translate') && empty($_jezyk['_translations'])) {
            $transFields = $this->Jezyk->associations()->keys();
            $tFields = [];
            foreach ($transFields as $field) {
                if (strpos($field, '_translation') !== false) {
                    $field = substr($field, (strpos($field, '_') + 1));
                    $field = str_replace('_translation', '', $field);
                    $tFields[$field] = $_jezyk[$field];
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $jezyk->set('_translations', $translation);
        }


        $waluta = $this->Jezyk->Waluta->find('list',['keyField'=>'id','valueField'=>'symbol'])->toArray();
        $this->set(compact('jezyk', 'waluta'));
        $this->set('_serialize', ['jezyk']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Jezyk id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $jezyk = $this->Jezyk->get($id);
        if(!empty($jezyk->domyslny)){
            $this->Flash->error($this->Txt->printAdmin(__('Admin | Nie można usunąć domyślnego języka')));
        }
        else if ($this->Jezyk->delete($jezyk)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.', [$this->Txt->printAdmin(__('Admin | jezyk'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.', [$this->Txt->printAdmin(__('Admin | jezyk'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function translations(){
        $jezyk = $this->Jezyk->find('all');

        $this->set(compact('jezyk'));
        $this->set('_serialize', ['jezyk']);
    }
    
    public function translate($id){
        $jezyk=$this->Jezyk->find('all',['conditions'=>['Jezyk.id'=>$id]])->first();
        if(empty($jezyk)){
            return $this->redirect(['action'=>'translations']);
        }
        $this->Translation->setLocale($jezyk['locale']);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if ($this->Translation->save($this->request->data)) {
                $this->Flash->success($this->Txt->printAdmin(__('Słownik został zapisany')));
                return $this->redirect(['action' => 'translations']);
            } else {
                $this->Flash->error($this->Txt->printAdmin(__('Błąd zapisu słownika.')));
            }
        }
        $this->set('lang', $this->Translation->getLang());
        $this->set('jezyk', $jezyk);
        $this->set('_serialize', ['jezyk']);
    }

        /**
     * setField method
     *
     * @param string|null $id Jezyk id.
     * @param string|null $field Jezyk field name.
     * @param mixed $value value to set.
     */
    public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->Jezyk->find('all', ['conditions' => ['Jezyk.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                    $item->{$field} = $value;
                    if ($this->Jezyk->save($item)) {
                        $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, !(bool) $value])), 'action' => $retAction];
                    } else {
                        $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                    }
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    /**
     * function sort
     * 
     */
    public function sort($field = 'kolejnosc') {
        $this->autoRender = false;
        $returnData = [];
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            foreach ($rqData as $data) {
                if (!empty($data['id'])) {
                    $this->Jezyk->updateAll($data, ['id' => $data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

}
