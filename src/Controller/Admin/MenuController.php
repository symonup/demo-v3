<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Menu Controller
 *
 * @property \App\Model\Table\MenuTable $Menu
 *
 * @method \App\Model\Entity\Menu[] paginate($object = null, array $settings = [])
 */
class MenuController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $targets=['_self'=>$this->Txt->printAdmin(__('Admin | Aktualna strona')),'_blank'=>$this->Txt->printAdmin(__('Admin | Nowa karta'))];
        $this->set('targets',$targets);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index($parentId = null) {
        $conditions = ['Menu.parent_id IS ' => NULL];
        $parentMenu = null;
        if (!empty($parentId)) {
            $conditions = ['Menu.parent_id' => $parentId];
        }
        $cfgArr = [
            'contain' => ['ChildMenu'=>['sort'=>['ChildMenu.kolejnosc'=>'desc']]],
            'conditions' => $conditions,
            'order'=>['Menu.kolejnosc'=>'desc']
        ];
        $menu = $this->Menu->find('all', $cfgArr);
        if (!empty($parentId)) {
            $parentMenu = $this->Menu->find('all', ['conditions' => ['Menu.id' => $parentId]])->first();
            if (empty($parentMenu)) {
                $this->Flash->error($this->Txt->printAdmin(__('Admin | Brak elementu nadrzędnego')));
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set(compact('menu', 'parentMenu','parentId'));
        $this->set('_serialize', ['menu']);$tmpView=new \Cake\View\View();
        $tips=[
            'menu'=>$tmpView->element($this->tipsPath.'menu')
                ];
        $this->set('tips',$tips);
    }

    /**
     * View method
     *
     * @param string|null $id Menu id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $menu = $this->Menu->get($id, [
            'contain' => ['ParentMenu', 'ChildMenu']
        ]);

        $this->set('menu', $menu);
        $this->set('_serialize', ['menu']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($parentId = null) {
        $menu = $this->Menu->newEntity();
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            if(empty($rqData['jezyk_id']) || $rqData['jezyk_id']=='0'){
                $rqData['jezyk_id']=null;
            }
            $menu = $this->Menu->patchEntity($menu, $rqData, ['translations' => $this->Menu->hasBehavior('Translate')]);
            if ($this->Menu->save($menu)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The data has been saved.')));
                return $this->redirect(['action' => 'index', $parentId]);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The data could not be saved. Please, try again.')));
        }
        $parentMenu = $this->Menu->find('list', ['keyField' => 'id', 'valueField' => 'nazwa', 'conditions' => ['parent_id IS ' => NULL]])->toArray();
        $this->set(compact('menu', 'parentMenu', 'parentId'));
        $this->set('_serialize', ['menu']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Menu id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {

        if ($this->Menu->hasBehavior('Translate')) {
            $menu = $this->Menu->find('translations', [
                        'locales' => $this->lngIdList,
                        'conditions' => ['Menu.' . $this->Menu->primaryKey() => $id],
                        'contain' => []
                    ])->first();
        } else {
            $menu = $this->Menu->find('all', [
                        'conditions' => ['Menu.' . $this->Menu->primaryKey() => $id],
                        'contain' => []
                    ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData = $this->request->getData();
            if(empty($rqData['jezyk_id']) || $rqData['jezyk_id']=='0'){
                $rqData['jezyk_id']=null;
            }
            
            $menu = $this->Menu->patchEntity($menu, $rqData, ['translations' => $this->Menu->hasBehavior('Translate')]);
            if ($this->Menu->save($menu)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The data has been saved.')));

                return $this->redirect(['action' => 'index',$menu->parent_id]);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The data could not be saved. Please, try again.')));
        }

        $_menu = $menu->toArray();
        if ($this->Menu->hasBehavior('Translate') && empty($_menu['_translations'])) {
            $transFields = $this->Menu->associations()->keys();
            $tFields = [];
            foreach ($transFields as $field) {
                if (strpos($field, '_translation') !== false) {
                    $field = substr($field, (strpos($field, '_') + 1));
                    $field = str_replace('_translation', '', $field);
                    $tFields[$field] = $_menu[$field];
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $menu->set('_translations', $translation);
        }


        $parentMenu = $this->Menu->find('list', ['keyField' => 'id', 'valueField' => 'nazwa', 'conditions' => ['parent_id IS ' => NULL]])->toArray();
        $this->set(compact('menu', 'parentMenu'));
        $this->set('_serialize', ['menu']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Menu id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $menu = $this->Menu->get($id);
        if ($this->Menu->delete($menu)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The data has been deleted.')));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The data could not be deleted. Please, try again.')));
        }

        return $this->redirect(['action' => 'index']);
    }
public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            if (!empty($rqData['id'])) {
                $entity = $this->Menu->find('all', ['conditions' => ['Menu.id' => $rqData['id']]])->first();
                if (!empty($entity)) {
                    $entity = $this->Menu->patchEntity($entity, $rqData);
                    if ($this->Menu->save($entity)) {
                        $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | The data has been saved.'))];
                    } else {
                        $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | The data could not be saved. Please, try again.'))];
                    }
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowy identyfikator'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak identyfikatora'))];
            }
        } else if (!empty($id) && !empty($field)) {
            $item = $this->Menu->find('all', ['conditions' => ['Menu.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                    if ($field == 'domyslne' && empty($value) && !empty($item->domyslne)) {
                        $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Przynajmniej jedno zdjęcie musi być domyślne.')), 'errors' => $item->errors()];
                    } else {
                        if ($field == 'domyslne' && !empty($value)) {
                            if (empty($item->domyslne)) {
                                $this->Menu->updateAll(['domyslne' => 0], ['towar_id' => $item->towar_id]);
                                $retAction = 'reload';
                            }
                        }
                        $item->{$field} = $value;
                        if ($this->Menu->save($item)) {
                            $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | The data has been saved.')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, !(bool) $value])), 'action' => $retAction];
                        } else {
                            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | The data could not be saved. Please, try again.')), 'errors' => $item->errors()];
                        }
                    }
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    public function sort($field='kolejnosc'){
        $this->autoRender=false;
        $returnData=[];
        if($this->request->is('post')){
            $rqData=$this->request->getData();
            foreach ($rqData as $data){
                if(!empty($data['id'])){
                    $this->Menu->updateAll($data, ['id'=>$data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
}
