<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Zestaw Controller
 *
 * @property \App\Model\Table\ZestawTable $Zestaw
 */
class ZestawController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->loadModel('Zestaw');
        $this->loadModel('ZestawTowar');
        $this->loadModel('Towar');
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index($id = null) {
        $conditions = [];
        if (!empty($id))
            $conditions = ['Zestaw.towar_id' => $id];
        $zestaw = $this->Zestaw->find('all', [
            'conditions' => $conditions,
            'contain' => ['Towar' => ['TowarCenaDefault', 'TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]]], 'ZestawTowar' => ['Towar' => ['TowarCena', 'TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]]]]],
            'order' => ['Zestaw.kolejnosc DESC']
        ]);
        $towarIds = [];
        foreach ($zestaw as $_zestaw) {
            $towarIds[$_zestaw->towar_id] = $_zestaw->towar_id;
        }
        if (empty($towarIds)) {
            $towarIds = [null];
        }
        $towary = $this->Towar->find('list', ['keyField' => 'id', 'valueField' => 'nazwa', 'conditions' => ['id IN' => $towarIds]])->toArray();
        if (!empty($id)) {
            $towar = $this->Towar->find('all', ['conditions' => ['Towar.id' => $id]])->first();
        } else {
            $towar = null;
        }
        if (empty($id)) {
            $allTowary = $this->Towar->find('list', ['keyField' => 'id', 'valueField' => 'nazwa'])->toArray();
            $this->set('allTowary', $allTowary);
        } else {
            $tPage = $this->session->read('lastPageTowar');
            if (!empty($tPage)) {
                $this->session->write('backToTowar', $tPage);
                $this->session->delete('lastPageTowar');
            }
        }
        $this->set('towary', $towary);
        $this->set('towar', $towar);
        $this->set('zestaw', $zestaw);
        $this->set('id', $id);
        $this->set('_serialize', ['zestaw']);
    }

    /**
     * View method
     *
     * @param string|null $id Zestaw id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $zestaw = $this->Zestaw->get($id, [
            'contain' => ['Towar']
        ]);
        $this->set('zestaw', $zestaw);
        $this->set('_serialize', ['zestaw']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add($id = null) {
        $zestaw = $this->Zestaw->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->data;
            unset($data['search']);
            $data['towar_id'] = $id;
            $zestaw_towar = [];
            $zt_i = 0;
            $data['token'] = uniqid();
            $zestawCombine = [$data['towar_id'] => $data];
            $baseId = $data['towar_id'];
            foreach ($data['zestaw_towar'] as $itemKey => $item) {
                $tmp = $data;
                $tmp['towar_id'] = $item['towar_id'];
                $tmp['zestaw_towar'][$itemKey]['towar_id'] = $baseId;
                $zestawCombine[$item['towar_id']] = $tmp;
                $zt_i++;
            }
//            $zestaw->zestaw_towar = $zestaw_towar;
//            unset($data['zestaw_towar'], $data['zestaw_towar_wymiana']);
            $zestawy = $this->Zestaw->newEntities($zestawCombine);
            $errors = [];
            foreach ($zestawy as $zestaw) {
                if (!$this->Zestaw->save($zestaw)) {
                    $errors[] = $zestaw->errors();
                }
            }
            if (empty($errors)) {
                $this->Flash->success(__('The zestaw has been saved.'));
                return $this->redirect(['action' => 'index', $id, 'iframe' => (!empty($this->iframe) ? $this->iframe : null)]);
            } else {
                $this->Flash->error(__('The zestaw could not be saved. Please, try again.'));
            }
        }
        if (empty($id))
            $this->redirect(['action' => 'index']);
        $towar = $this->Zestaw->Towar->findById($id)->toArray();
        $this->set(compact('zestaw', 'towar'));
        $this->set('_serialize', ['zestaw']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Zestaw id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $zestaw = $this->Zestaw->get($id, [
            'contain' => ['Towar' => ['TowarCena', 'TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]]], 'ZestawTowar' => ['Towar' => ['TowarCena', 'TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]]]]]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            unset($data['zestaw_towar'], $data['zestaw_towar_wymiana'], $data['search']);
            $zestaw = $this->Zestaw->patchEntity($zestaw, $data);
            $success = false;
            if (!empty($zestaw->token)) {
                if ($this->Zestaw->updateAll($data, ['token' => $zestaw->token])) {
                    $success = true;
                }
            } else {
                $zestaw = $this->Zestaw->patchEntity($zestaw, $data);
                if ($this->Zestaw->save($zestaw)) {
                    $success = true;
                }
            }
            if ($success) {
                $this->Flash->success(__('The zestaw has been saved.'));
                return $this->redirect(['action' => 'index', $zestaw->towar_id, 'iframe' => (!empty($this->iframe) ? $this->iframe : null)]);
            } else {
                $this->Flash->error(__('The zestaw could not be saved. Please, try again.'));
            }
        }
        $kategoria = $this->Towar->Kategoria->find('list', ['keyField' => 'id', 'valueField' => 'sciezka', 'order' => 'sciezka ASC']);
        $this->set('kategoria', $kategoria);
        $this->set(compact('zestaw'));
        $this->set('_serialize', ['zestaw']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Zestaw id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null) {
        $zestaw = $this->Zestaw->get($id);
        $towar_id = $zestaw->towar_id;
        $token = $zestaw->token;
        $success = false;
        if (!empty($token)) {
            if ($this->Zestaw->deleteAll(['token' => $token])) {
                $success = true;
            }
        } else {
            if ($this->Zestaw->delete($zestaw)) {
                $success = true;
            }
        }
        if ($success) {
            $this->Flash->success(__('The zestaw has been deleted.'));
        } else {
            $this->Flash->error(__('The zestaw could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index', $towar_id, 'iframe' => (!empty($this->iframe) ? $this->iframe : null)]);
    }

    public function addElement($towar_id) {
        $this->viewBuilder()->layout('ajax');
        $towar = $this->Towar->find('all', ['conditions' => ['Towar.id' => $towar_id], 'contain' => ['TowarCena', 'TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]]]])->toArray();
        $kategoria = $this->Towar->Kategoria->find('list', ['keyField' => 'id', 'valueField' => 'sciezka', 'order' => 'sciezka ASC']);
        $this->set('kategoria', $kategoria);
        $this->set('towar', $towar[0]);
    }

    public function findElement($search = null) {
        $this->viewBuilder()->layout('ajax');
        if (!empty($search)) {
            $conditions = [];
            $keys = explode(' ', $search);
            foreach ($keys as $key) {
                $conditions['OR']['AND']['Towar.nazwa LIKE'] = '%' . $key . '%';
            }
            $conditions['OR']['Towar.kod LIKE'] = '%' . $search . '%';
            $conditions['Towar.ukryty'] = 0;
            $towary = $this->Towar->find('all', ['conditions' => $conditions, 'order' => ['Towar.nazwa ASC'], 'limit' => 50, 'contain' => ['TowarCena','Platforma']]);
            $this->set('towary', $towary);
        } else {
            header("HTTP/1.0 404 Not Found");
            echo __('error no data');
            die();
        }
    }

    public function ajaxSort() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = json_decode($this->request->data['data']);
            $enities = $this->Zestaw->find('all', ['conditions' => ['Zestaw.id IN' => $data->ids]]);
            foreach ($enities->toArray() as $modelEnity) {
                $modelEnity->kolejnosc = $data->values->{$modelEnity->id};
                $this->Zestaw->save($modelEnity);
            }
        }
    }
    public function checkAll(){
        set_time_limit(0);
        $zestawy = $this->Zestaw->find('all',['contain'=>['ZestawTowar'],'conditions'=>['Zestaw.id <='=>113]]);
        $all=[];
        $errors=0;
        $err=[];
        foreach ($zestawy as $zestaw){
            $zIds=[$zestaw->towar_id];
            $zestaw->set('token', uniqid($zestaw->id));
            $dane = [
                'rabat'=>$zestaw->rabat,
                'typ'=>$zestaw->typ,
                'token'=>$zestaw->token,
                'kolejnosc'=>$zestaw->kolejnosc,
                'nazwa'=>$zestaw->nazwa
            ];
            $newZestawy = [];
            $allZIds=[];
            foreach ($zestaw->zestaw_towar as $zItem){
                $tmp=$dane;
                $tmp['towar_id']=$zItem->towar_id;
                $tmp['zestaw_towar']=[['towar_id'=>$zestaw->towar_id,'wymiana'=>0]];
                $newZestawy[]=$tmp;
                $allZIds[$zItem->towar_id]=$zItem->towar_id;
            }
            foreach ($newZestawy as &$newZestaw){
                foreach ($allZIds as $izId){
                    if($izId!=$newZestaw['towar_id']){
                        $newZestaw['zestaw_towar'][]=['towar_id'=>$izId,'wymiana'=>0];
                    }
                }
            }
            if($this->Zestaw->save($zestaw)){
                $entNew=$this->Zestaw->newEntities($newZestawy);
                foreach ($entNew as $entity){
                    if(!$this->Zestaw->save($entity)){
                        $errors++;
                        $err[]=$entity->errors();
                    }
                }
            }else{
                $err[]=$zestaw->errors();
                $errors++;
            }
        }
        $this->response->type('ajax');
        $this->response->body(json_encode(['errors'=>$errors,'all'=>$err]));
        return $this->response;
    }

}
