<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Przeznaczenie Controller
 *
 * @property \App\Model\Table\PrzeznaczenieTable $Przeznaczenie
 *
 * @method \App\Model\Entity\Przeznaczenie[] paginate($object = null, array $settings = [])
 */
class PrzeznaczenieController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
    $cfgArr=[];
        $cfgArr = [
            'contain' => ['Ikona']
        ];
$przeznaczenie = $this->Przeznaczenie->find('all',$cfgArr);

        $this->set(compact('przeznaczenie'));
        $this->set('_serialize', ['przeznaczenie']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $przeznaczenie = $this->Przeznaczenie->newEntity();
        if ($this->request->is('post')) {
        $rqData=$this->request->getData();
            $przeznaczenie = $this->Przeznaczenie->patchEntity($przeznaczenie, $rqData,['translations'=>$this->Przeznaczenie->hasBehavior('Translate')]);
            if ($this->Przeznaczenie->save($przeznaczenie)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | przeznaczenie'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | przeznaczenie'))])));
        }
        $ikona = $this->Przeznaczenie->Ikona->find('list')->toArray();
        $towar = $this->Przeznaczenie->Towar->find('list')->toArray();
        $this->set(compact('przeznaczenie', 'ikona', 'towar'));
        $this->set('_serialize', ['przeznaczenie']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Przeznaczenie id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
    
    if($this->Przeznaczenie->hasBehavior('Translate')){
        $przeznaczenie = $this->Przeznaczenie->find('translations', [
            'locales' => $this->lngIdList,
            'conditions' => ['Przeznaczenie.'.$this->Przeznaczenie->primaryKey() => $id],
            'contain' => ['Towar']
        ])->first();
        }
        else{
        $przeznaczenie = $this->Przeznaczenie->find('all', [
            'conditions'=>['Przeznaczenie.'.$this->Przeznaczenie->primaryKey()=>$id],
            'contain' => ['Towar']
        ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData=$this->request->getData();
            $przeznaczenie = $this->Przeznaczenie->patchEntity($przeznaczenie, $rqData,['translations'=>$this->Przeznaczenie->hasBehavior('Translate')]);
            if ($this->Przeznaczenie->save($przeznaczenie)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | przeznaczenie'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | przeznaczenie'))])));
        }
        
        $_przeznaczenie = $przeznaczenie->toArray();
        if ($this->Przeznaczenie->hasBehavior('Translate') && empty($_przeznaczenie['_translations'])) {
            $transFields=$this->Przeznaczenie->associations()->keys();
            $tFields=[];
            foreach ($transFields as $field){
                if(strpos($field, '_translation')!==false){
                    $field= substr($field, (strpos($field, '_')+1));
                    $field=str_replace('_translation','', $field);
                    $tFields[$field]=$_przeznaczenie[$field]; 
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $przeznaczenie->set('_translations',$translation);
        }
        
        
        $ikona = $this->Przeznaczenie->Ikona->find('list')->toArray();
        $towar = $this->Przeznaczenie->Towar->find('list')->toArray();
        $this->set(compact('przeznaczenie', 'ikona', 'towar'));
        $this->set('_serialize', ['przeznaczenie']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Przeznaczenie id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $przeznaczenie = $this->Przeznaczenie->get($id);
        if ($this->Przeznaczenie->delete($przeznaczenie)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.',[$this->Txt->printAdmin(__('Admin | przeznaczenie'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.',[$this->Txt->printAdmin(__('Admin | przeznaczenie'))])));
        }

        return $this->redirect(['action' => 'index']);
    }
}
