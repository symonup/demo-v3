<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Okazje Controller
 *
 * @property \App\Model\Table\OkazjeTable $Okazje
 *
 * @method \App\Model\Entity\Okazje[] paginate($object = null, array $settings = [])
 */
class OkazjeController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
    $cfgArr=[];
$okazje = $this->Okazje->find('all',$cfgArr);

        $this->set(compact('okazje'));
        $this->set('_serialize', ['okazje']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $okazje = $this->Okazje->newEntity();
        if ($this->request->is('post')) {
        $rqData=$this->request->getData();
            $okazje = $this->Okazje->patchEntity($okazje, $rqData,['translations'=>$this->Okazje->hasBehavior('Translate')]);
            if ($this->Okazje->save($okazje)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | okazje'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | okazje'))])));
        }
        $this->set(compact('okazje'));
        $this->set('_serialize', ['okazje']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Okazje id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
    
    if($this->Okazje->hasBehavior('Translate')){
        $okazje = $this->Okazje->find('translations', [
            'locales' => $this->lngIdList,
            'conditions' => ['Okazje.'.$this->Okazje->primaryKey() => $id]
        ])->first();
        }
        else{
        $okazje = $this->Okazje->find('all', [
            'conditions'=>['Okazje.'.$this->Okazje->primaryKey()=>$id]
        ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData=$this->request->getData();
            $okazje = $this->Okazje->patchEntity($okazje, $rqData,['translations'=>$this->Okazje->hasBehavior('Translate')]);
            if ($this->Okazje->save($okazje)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | okazje'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | okazje'))])));
        }
        
        $_okazje = $okazje->toArray();
        if ($this->Okazje->hasBehavior('Translate') && empty($_okazje['_translations'])) {
            $transFields=$this->Okazje->associations()->keys();
            $tFields=[];
            foreach ($transFields as $field){
                if(strpos($field, '_translation')!==false){
                    $field= substr($field, (strpos($field, '_')+1));
                    $field=str_replace('_translation','', $field);
                    $tFields[$field]=$_okazje[$field]; 
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $okazje->set('_translations',$translation);
        }
        
        
        $this->set(compact('okazje'));
        $this->set('_serialize', ['okazje']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Okazje id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $okazje = $this->Okazje->get($id);
        if ($this->Okazje->delete($okazje)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.',[$this->Txt->printAdmin(__('Admin | okazje'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.',[$this->Txt->printAdmin(__('Admin | okazje'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

/**
     * setField method
     *
     * @param string|null $id Okazje id.
     * @param string|null $field Okazje field name.
     * @param mixed $value value to set.
     */

public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->Okazje->find('all', ['conditions' => ['Okazje.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                        $item->{$field} = $value;
                        if ($this->Okazje->save($item)) {
                            $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, (!empty($value)?0:1)])), 'action' => $retAction];
                        } else {
                            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                        }
                    
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    /**
* function sort
* 
*/
public function sort($field='kolejnosc'){
        $this->autoRender=false;
        $returnData=[];
        if($this->request->is('post')){
            $rqData=$this->request->getData();
            foreach ($rqData as $data){
                if(!empty($data['id'])){
                    $this->Okazje->updateAll($data, ['id'=>$data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    }
