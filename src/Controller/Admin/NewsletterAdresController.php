<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * NewsletterAdres Controller
 *
 * @property \App\Model\Table\NewsletterAdresTable $NewsletterAdres
 *
 * @method \App\Model\Entity\NewsletterAdre[] paginate($object = null, array $settings = [])
 */
class NewsletterAdresController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $cfgArr = [];
        $newsletterAdres = $this->NewsletterAdres->find('all', $cfgArr);

        $this->set(compact('newsletterAdres'));
        $this->set('_serialize', ['newsletterAdres']);$tmpView=new \Cake\View\View();
        $tips=[
            'newsletter_adres'=>$tmpView->element($this->tipsPath.'newsletter_adres')
                ];
        $this->set('tips',$tips);
    }

    /**
     * Delete method
     *
     * @param string|null $id Newsletter Adre id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $newsletterAdre = $this->NewsletterAdres->get($id);
        if ($this->NewsletterAdres->delete($newsletterAdre)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.', [$this->Txt->printAdmin(__('Admin | newsletter adre'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.', [$this->Txt->printAdmin(__('Admin | newsletter adre'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

    
    public function import() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $dane = $this->request->getData();
            $allEmails = [];
            $notValid = [];
            $existMails=[];
            if (!empty($dane['plik']) && empty($dane['plik']['errors'])) {
                $row = 1;
                if (($handle = fopen($dane['plik']['tmp_name'], "r")) !== FALSE) {

                    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                        if ($this->Txt->validateEmail($data[0])) {
                            $exist = $this->NewsletterAdres->find('all')->where(['NewsletterAdres.email' => $data[0]])->first();
                            if (empty($exist)) {
                                $allEmails[$data[0]] = [
                                    'email' => $data[0],
                                    'token' => uniqid(uniqid()),
                                    'data_dodania'=>date('Y-m-d H:i:s'),
                                    'potwierdzony'=>1
                                ];
                            }else{
                                $existMails[$row]=$data[0];
                            }
                        }else{
                            $notValid[$row]=$data[0];
                        }
                        $row++;
                    }
                    fclose($handle);
                }
                if (!empty($allEmails)) {
                    $all = count($allEmails);
                    $allEmails = $this->NewsletterAdres->newEntities($allEmails);
                    $errors=[];
                    foreach ($allEmails as $newEmail){
                        if(!$this->NewsletterAdres->save($newEmail)){
                            $errors[]=$newEmail->errors();
                        }
                    }
                    if(!empty($errors)){
                        \Cake\Cache\Cache::write('importEmailErrors', $errors);
                        $this->Flash->error($this->Txt->printAdmin(__('Admin | Błąd zapisu danych')));
                    }else{
                         $this->Flash->success($this->Txt->printAdmin(__('Admin | Dane zostały zapisane. Zapisano {0} pozycji.', [$all])));
                    }
                }else{
                    $this->Flash->success($this->Txt->printAdmin(__('Admin | Brak danych do importu.')));
                }
            }
            if(!empty($notValid)){
                        $this->Flash->error($this->Txt->printAdmin(__('Admin | Wykryto {0} nieprawidłowych adresów email. Nie zostały one zapisane do bazy danych',[count($notValid)])));
            }
        }
        return $this->redirect(['action' => 'index']);
    }
}
