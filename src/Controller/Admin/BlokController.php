<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Blok Controller
 *
 * @property \App\Model\Table\BlokTable $Blok
 *
 * @method \App\Model\Entity\Blok[] paginate($object = null, array $settings = [])
 */
class BlokController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->loadModel('Banner');
        $bannersAll = $this->Banner->find('all');

        $blocks['[%-_kontakt-form_-%]'] = $this->Txt->printAdmin(__('Admin | Formularz kontaktowy'));
        $blocks['[%-_kontakt-form-2_-%]'] = $this->Txt->printAdmin(__('Admin | Formularz kontaktowy do popUp'));
        $blocks['[%-_newsletter-form_-%]'] = $this->Txt->printAdmin(__('Admin | Formularz zapisu do newslettera'));
        $blocks['[%-_karta-kod-form_-%]']=$this->Txt->printAdmin(__('Admin | Formularz użycia kodu karty podarunkowej'));
        $blocks['[%-_karty-podarunkowe-lista_-%]'] = $this->Txt->printAdmin(__('Admin | Lista dostępnych kart podarunkowych'));
                foreach ($bannersAll as $bannerItem) {
            $blocks['[%-_banner_' . $bannerItem->id . '_-%]'] = 'BANER - ' . $bannerItem->nazwa;
        }

        $this->set('blocks', $blocks);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $cfgArr = [];
        $blok = $this->Blok->find('all', $cfgArr);

        $this->set(compact('blok'));
        $this->set('_serialize', ['blok']);
        $tmpView = new \Cake\View\View();
        $tips = [
            'bloki' => $tmpView->element($this->tipsPath . 'bloki')
        ];
        $this->set('tips', $tips);
    }

    /**
     * View method
     *
     * @param string|null $id Blok id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $blok = $this->Blok->get($id, [
            'contain' => []
        ]);

        $this->set('blok', $blok);
        $this->set('_serialize', ['blok']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($id = null) {
        $blok = $this->Blok->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData = $this->request->getData();
            $tresc = $rqData['tresc'];
            $blok = $this->Blok->patchEntity($blok, $rqData, ['translations' => $this->Blok->hasBehavior('Translate')]);
            if ($this->Blok->save($blok)) {
                $path = $this->filePath['bloki'];
                $this->loadComponent('Upload');
                $this->Upload->checkDir($path . (!empty($blok->locale) ? $blok->locale : $this->defPageLocale) . DS);
                $file = fopen($path . (!empty($blok->locale) ? $blok->locale : $this->defPageLocale) . DS . 'blok_' . $blok->id . '.ctp', 'a+');
                fwrite($file, $tresc);
                fclose($file);
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The data has been saved.')));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The data could not be saved. Please, try again.')));
        }
        if (!empty($id)) {
            if ($this->Blok->hasBehavior('Translate')) {
                $blok = $this->Blok->find('translations', [
                            'locales' => $this->lngIdList,
                            'conditions' => ['Blok.' . $this->Blok->primaryKey() => $id],
                            'contain' => []
                        ])->first();
            } else {
                $blok = $this->Blok->find('all', [
                            'conditions' => ['Blok.' . $this->Blok->primaryKey() => $id],
                            'contain' => []
                        ])->first();
            }
            $path = $this->filePath['bloki'];
            if (file_exists($path . (!empty($blok->locale) ? $blok->locale : $this->defPageLocale) . DS . 'blok_' . $blok->id . '.ctp')) {
                $blok->tresc = file_get_contents($path . (!empty($blok->locale) ? $blok->locale : $this->defPageLocale) . DS . 'blok_' . $blok->id . '.ctp');
            }
        }
        $this->set(compact('blok'));
        $this->set('_serialize', ['blok']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Blok id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {

        if ($this->Blok->hasBehavior('Translate')) {
            $blok = $this->Blok->find('translations', [
                        'locales' => $this->lngIdList,
                        'conditions' => ['Blok.' . $this->Blok->primaryKey() => $id],
                        'contain' => []
                    ])->first();
        } else {
            $blok = $this->Blok->find('all', [
                        'conditions' => ['Blok.' . $this->Blok->primaryKey() => $id],
                        'contain' => []
                    ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData = $this->request->getData();
            $tresc = $rqData['tresc'];
            $blok = $this->Blok->patchEntity($blok, $rqData, ['translations' => $this->Blok->hasBehavior('Translate')]);
            if ($this->Blok->save($blok)) {
                $path = $this->filePath['bloki'];
                $this->loadComponent('Upload');
                $this->Upload->checkDir($path . (!empty($blok->locale) ? $blok->locale : $this->defPageLocale) . DS);
                if (!file_exists($path . (!empty($blok->locale) ? $blok->locale : $this->defPageLocale) . DS . 'blok_' . $blok->id . '.ctp')) {
                    $file = fopen($path . (!empty($blok->locale) ? $blok->locale : $this->defPageLocale) . DS . 'blok_' . $blok->id . '.ctp', 'a+');
                } else {
                    $file = fopen($path . (!empty($blok->locale) ? $blok->locale : $this->defPageLocale) . DS . 'blok_' . $blok->id . '.ctp', 'w+');
                }
                fwrite($file, $tresc);
                fclose($file);
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The data has been saved.')));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The data could not be saved. Please, try again.')));
        }

        $path = $this->filePath['bloki'];
        $rType = 'a+';
        if (file_exists($path . (!empty($blok->locale) ? $blok->locale : $this->defPageLocale) . DS . 'blok_' . $blok->id . '.ctp')) {
            $rType = 'r+';
        }
        $file = fopen($path . (!empty($blok->locale) ? $blok->locale : $this->defPageLocale) . DS . 'blok_' . $blok->id . '.ctp', $rType);
        $fileSize = filesize($path . (!empty($blok->locale) ? $blok->locale : $this->defPageLocale) . DS . 'blok_' . $blok->id . '.ctp');
        if ($fileSize > 0) {
            $blok->tresc = fread($file, $fileSize);
        }
        fclose($file);


        $this->set(compact('blok'));
        $this->set('_serialize', ['blok']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Blok id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $blok = $this->Blok->get($id);
        if ($this->Blok->delete($blok)) {
            $path = $this->filePath['bloki'];
            if (file_exists($path . $this->defPageLocale . DS . 'blok_' . $id . '.ctp')) {
                rename($path . $this->defPageLocale . DS . 'blok_' . $id . '.ctp', $path . $this->defPageLocale . DS . 'blok_' . $id . '_removed_' . date('YmdHis') . '.ctp');
            }
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The data has been deleted.')));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The data could not be deleted. Please, try again.')));
        }

        return $this->redirect(['action' => 'index']);
    }

}
