<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * KartaPodarunkowa Controller
 *
 * @property \App\Model\Table\KartaPodarunkowaTable $KartaPodarunkowa
 *
 * @method \App\Model\Entity\KartaPodarunkowa[] paginate($object = null, array $settings = [])
 */
class KartaPodarunkowaController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->loadComponent('Upload');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $cfgArr = [];
        $kartaPodarunkowa = $this->KartaPodarunkowa->find('all', $cfgArr);

        $this->set(compact('kartaPodarunkowa'));
        $this->set('_serialize', ['kartaPodarunkowa']);
    }
    
    public function kody($id) {
        if(empty($id)){
            $this->Flash->error($this->Txt->printAdmin(__('Admin | Wybierz kartę')));
            return $this->redirect(['action'=>'index']);
        }
        $this->loadModel('KartaPodarunkowaKod');
        $cfgArr = ['contain'=>['Zamowienie'],'conditions'=>['KartaPodarunkowaKod.karta_podarunkowa_id'=>$id]];
        if ($this->KartaPodarunkowa->hasBehavior('Translate')) {
            $kartaPodarunkowa = $this->KartaPodarunkowa->find('translations', [
                        'locales' => $this->lngIdList,
                        'conditions' => ['KartaPodarunkowa.' . $this->KartaPodarunkowa->primaryKey() => $id],
                        'contain' => []
                    ])->first();
        } else {
            $kartaPodarunkowa = $this->KartaPodarunkowa->find('all', [
                        'conditions' => ['KartaPodarunkowa.' . $this->KartaPodarunkowa->primaryKey() => $id],
                        'contain' => []
                    ])->first();
        }
        $kody = $this->KartaPodarunkowaKod->find('all', $cfgArr);

        $this->set('zamowienieStatusy', $this->statusy);
        $this->set(compact('kartaPodarunkowa','kody'));
        $this->set('_serialize', ['kartaPodarunkowa']);
    }
    public function kodyAll() {
        $this->loadModel('KartaPodarunkowaKod');
        $cfgArr = ['contain'=>['Zamowienie','KartaPodarunkowa']];
        $kody = $this->KartaPodarunkowaKod->find('all', $cfgArr);

        $this->set('zamowienieStatusy', $this->statusy);
        $this->set(compact('kody'));
        $this->set('_serialize', ['kody']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $kartaPodarunkowa = $this->KartaPodarunkowa->newEntity();
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            $rqData['data_dodania'] = date('Y-m-d H:i:s');
            $path = $this->filePath['karta_podarunkowa'];
            $fileName = null;
            if (!empty($this->request->data['zdjecie']['name'])) {
                $fileName = $this->Upload->saveFile(
                        $path, $this->request->data['zdjecie']);
                if (!empty($fileName)) {
                    $rqData['zdjecie'] = $fileName;
                } else {
                    $rqData['zdjecie'] = null;
                }
            } else
                $rqData['zdjecie'] = null;
            $kartaPodarunkowa = $this->KartaPodarunkowa->patchEntity($kartaPodarunkowa, $rqData, ['translations' => $this->KartaPodarunkowa->hasBehavior('Translate')]);
            if ($this->KartaPodarunkowa->save($kartaPodarunkowa)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | karta podarunkowa'))])));

                return $this->redirect(['action' => 'index']);
            } else {
                if (!empty($fileName)) {
                    $this->Upload->deleteFile($path . $fileName);
                }
                $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | karta podarunkowa'))])));
            }
        }
        $this->set(compact('kartaPodarunkowa'));
        $this->set('_serialize', ['kartaPodarunkowa']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Karta Podarunkowa id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {

        if ($this->KartaPodarunkowa->hasBehavior('Translate')) {
            $kartaPodarunkowa = $this->KartaPodarunkowa->find('translations', [
                        'locales' => $this->lngIdList,
                        'conditions' => ['KartaPodarunkowa.' . $this->KartaPodarunkowa->primaryKey() => $id],
                        'contain' => []
                    ])->first();
        } else {
            $kartaPodarunkowa = $this->KartaPodarunkowa->find('all', [
                        'conditions' => ['KartaPodarunkowa.' . $this->KartaPodarunkowa->primaryKey() => $id],
                        'contain' => []
                    ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData = $this->request->getData();
            $oldFile = null;
            $fileName = null;
            $path = $this->filePath['karta_podarunkowa'];
            if (empty($rqData['zdjecie']['name'])) {
                $rqData['zdjecie'] = $kartaPodarunkowa->zdjecie;
            } else {
                $fileName = $this->Upload->saveFile(
                        $path, $rqData['zdjecie']);
                if (!empty($fileName)) {
                    $rqData['zdjecie'] = $fileName;
                    $oldFile = $kartaPodarunkowa->zdjecie;
                }
            }
            $kartaPodarunkowa = $this->KartaPodarunkowa->patchEntity($kartaPodarunkowa, $rqData, ['translations' => $this->KartaPodarunkowa->hasBehavior('Translate')]);
            if ($this->KartaPodarunkowa->save($kartaPodarunkowa)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | karta podarunkowa'))])));
                if (!empty($oldFile))
                {
                    $this->Upload->deleteFile($path . $oldFile);
                }
                return $this->redirect(['action' => 'index']);
            }else {
                $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | karta podarunkowa'))])));
                if (!empty($fileName))
                {
                    $this->Upload->deleteFile($path . $fileName);
                }
            }
        }

        $_kartaPodarunkowa = $kartaPodarunkowa->toArray();
        if ($this->KartaPodarunkowa->hasBehavior('Translate') && empty($_kartaPodarunkowa['_translations'])) {
            $transFields = $this->KartaPodarunkowa->associations()->keys();
            $tFields = [];
            foreach ($transFields as $field) {
                if (strpos($field, '_translation') !== false) {
                    $field = substr($field, (strpos($field, '_') + 1));
                    $field = str_replace('_translation', '', $field);
                    $tFields[$field] = $_kartaPodarunkowa[$field];
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $kartaPodarunkowa->set('_translations', $translation);
        }


        $this->set(compact('kartaPodarunkowa'));
        $this->set('_serialize', ['kartaPodarunkowa']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Karta Podarunkowa id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $kartaPodarunkowa = $this->KartaPodarunkowa->get($id);
        if ($this->KartaPodarunkowa->delete($kartaPodarunkowa)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.', [$this->Txt->printAdmin(__('Admin | karta podarunkowa'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.', [$this->Txt->printAdmin(__('Admin | karta podarunkowa'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * setField method
     *
     * @param string|null $id Karta Podarunkowa id.
     * @param string|null $field Karta Podarunkowa field name.
     * @param mixed $value value to set.
     */
    public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->KartaPodarunkowa->find('all', ['conditions' => ['KartaPodarunkowa.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                    $item->{$field} = $value;
                    if ($this->KartaPodarunkowa->save($item)) {
                        $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, (!empty($value) ? 0 : 1)])), 'action' => $retAction];
                    } else {
                        $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                    }
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    public function setFieldKod($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->KartaPodarunkowa->KartaPodarunkowaKod->find('all', ['conditions' => ['KartaPodarunkowaKod.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                    $item->{$field} = $value;
                    if ($this->KartaPodarunkowa->KartaPodarunkowaKod->save($item)) {
                        $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setFieldKod', $id, $field, (!empty($value) ? 0 : 1)])), 'action' => $retAction];
                    } else {
                        $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                    }
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    /**
     * function sort
     * 
     */
    public function sort($field = 'kolejnosc') {
        $this->autoRender = false;
        $returnData = [];
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            foreach ($rqData as $data) {
                if (!empty($data['id'])) {
                    $this->KartaPodarunkowa->updateAll($data, ['id' => $data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

}
