<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Administrator Controller
 *
 * @property \App\Model\Table\AdministratorTable $Administrator
 *
 * @method \App\Model\Entity\Administrator[] paginate($object = null, array $settings = [])
 */
class AdministratorController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
    $cfgArr=[];
        $cfgArr = [
            'contain' => ['Roles']
        ];
$administrator = $this->Administrator->find('all',$cfgArr);

        $this->set(compact('administrator'));
        $this->set('_serialize', ['administrator']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $administrator = $this->Administrator->newEntity();
        if ($this->request->is('post')) {
        $rqData=$this->request->getData();
            $administrator = $this->Administrator->patchEntity($administrator, $rqData,['translations'=>$this->Administrator->hasBehavior('Translate')]);
            if ($this->Administrator->save($administrator)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | administrator'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | administrator'))])));
        }
        $roles = $this->Administrator->Roles->find('list',['keyField'=>'id','valueField'=>'alias'])->toArray();
        $this->set(compact('administrator', 'roles'));
        $this->set('_serialize', ['administrator']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Administrator id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
    
    if($this->Administrator->hasBehavior('Translate')){
        $administrator = $this->Administrator->find('translations', [
            'locales' => $this->lngIdList,
            'conditions' => ['Administrator.'.$this->Administrator->primaryKey() => $id],
            'contain' => []
        ])->first();
        }
        else{
        $administrator = $this->Administrator->find('all', [
            'conditions'=>['Administrator.'.$this->Administrator->primaryKey()=>$id],
            'contain' => []
        ])->first();
        }
        $oldDane=$administrator->toArray();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData=$this->request->getData();
            if(empty($rqData['password'])){
                unset($rqData['password']);
            }
            $administrator = $this->Administrator->patchEntity($administrator, $rqData,['translations'=>$this->Administrator->hasBehavior('Translate')]);
            if ($this->Administrator->save($administrator)) {
                
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | administrator'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | administrator'))])));
        }
        
        $_administrator = $administrator->toArray();
        if ($this->Administrator->hasBehavior('Translate') && empty($_administrator['_translations'])) {
            $transFields=$this->Administrator->associations()->keys();
            $tFields=[];
            foreach ($transFields as $field){
                if(strpos($field, '_translation')!==false){
                    $field= substr($field, (strpos($field, '_')+1));
                    $field=str_replace('_translation','', $field);
                    $tFields[$field]=$_administrator[$field]; 
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $administrator->set('_translations',$translation);
        }
        
        
        $roles = $this->Administrator->Roles->find('list',['keyField'=>'id','valueField'=>'alias'])->toArray();
        $this->set(compact('administrator', 'roles'));
        $this->set('_serialize', ['administrator']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Administrator id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $administrator = $this->Administrator->get($id);
        if ($this->Administrator->delete($administrator)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.',[$this->Txt->printAdmin(__('Admin | administrator'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.',[$this->Txt->printAdmin(__('Admin | administrator'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

/**
     * setField method
     *
     * @param string|null $id Administrator id.
     * @param string|null $field Administrator field name.
     * @param mixed $value value to set.
     */

public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->Administrator->find('all', ['conditions' => ['Administrator.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                        $item->{$field} = $value;
                        if ($this->Administrator->save($item)) {
                            $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, !(bool) $value])), 'action' => $retAction];
                        } else {
                            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                        }
                    
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    /**
* function sort
* 
*/
public function sort($field='kolejnosc'){
        $this->autoRender=false;
        $returnData=[];
        if($this->request->is('post')){
            $rqData=$this->request->getData();
            foreach ($rqData as $data){
                if(!empty($data['id'])){
                    $this->Administrator->updateAll($data, ['id'=>$data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    }
