<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Platforma Controller
 *
 * @property \App\Model\Table\PlatformaTable $Platforma
 *
 * @method \App\Model\Entity\Platforma[] paginate($object = null, array $settings = [])
 */
class PlatformaController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->loadComponent('Upload');
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $cfgArr = [];
        $platforma = $this->Platforma->find('all', $cfgArr);

        $this->set(compact('platforma'));
        $this->set('_serialize', ['platforma']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $platforma = $this->Platforma->newEntity();
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            $path = $this->filePath['platforma'];
            if (!empty($rqData['logo']['name'])) {
                $fileName = $this->Upload->saveFile(
                        $path, $rqData['logo']);
                if (!empty($fileName)) {
                    $rqData['logo'] = $fileName;
                } else {
                    $rqData['logo'] = null;
                }
            } else
                $rqData['logo'] = null;
            
            if (!empty($rqData['logo_2']['name'])) {
                $fileName2 = $this->Upload->saveFile(
                        $path, $rqData['logo_2']);
                if (!empty($fileName2)) {
                    $rqData['logo_2'] = $fileName2;
                } else {
                    $rqData['logo_2'] = null;
                }
            } else
                $rqData['logo_2'] = null;
            $platforma = $this->Platforma->patchEntity($platforma, $rqData, ['translations' => $this->Platforma->hasBehavior('Translate')]);
            if ($this->Platforma->save($platforma)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | platforma'))])));

                return $this->redirect(['action' => 'index']);
            } else {
                if (!empty($fileName)) {
                    $this->Upload->deleteFile($path . $fileName);
                }
                if (!empty($fileName2)) {
                    $this->Upload->deleteFile($path . $fileName2);
                }
                $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | platforma'))])));
            }
        }
        $this->set(compact('platforma'));
        $this->set('_serialize', ['platforma']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Platforma id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {

        if ($this->Platforma->hasBehavior('Translate')) {
            $platforma = $this->Platforma->find('translations', [
                        'locales' => $this->lngIdList,
                        'conditions' => ['Platforma.' . $this->Platforma->primaryKey() => $id],
                        'contain' => []
                    ])->first();
        } else {
            $platforma = $this->Platforma->find('all', [
                        'conditions' => ['Platforma.' . $this->Platforma->primaryKey() => $id],
                        'contain' => []
                    ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData = $this->request->getData();
            $path = $this->filePath['platforma'];
            $oldFile = null;
            $fileName = null;
            if (empty($rqData['logo']['name'])) {
                $rqData['logo'] = $platforma->logo;
            } else {
                $fileName = $this->Upload->saveFile(
                        $path, $rqData['logo']);
                if (!empty($fileName)) {
                    $rqData['logo'] = $fileName;
                    $oldFile = $platforma->logo;
                }
            }
            
            $oldFile2 = null;
            $fileName2 = null;
            if (empty($rqData['logo_2']['name'])) {
                $rqData['logo_2'] = $platforma->logo_2;
            } else {
                $fileName2 = $this->Upload->saveFile(
                        $path, $rqData['logo_2']);
                if (!empty($fileName2)) {
                    $rqData['logo_2'] = $fileName2;
                    $oldFile2 = $platforma->logo_2;
                }
            }
            $platforma = $this->Platforma->patchEntity($platforma, $rqData, ['translations' => $this->Platforma->hasBehavior('Translate')]);
            if ($this->Platforma->save($platforma)) {
                if (!empty($oldFile)) {
                    $this->Upload->deleteFile($path . $oldFile);
                }
                if (!empty($oldFile2)) {
                    $this->Upload->deleteFile($path . $oldFile2);
                }
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | platforma'))])));

                return $this->redirect(['action' => 'index']);
            } else {
                if (!empty($fileName)) {
                    $this->Upload->deleteFile($path . $fileName);
                }
                if (!empty($fileName2)) {
                    $this->Upload->deleteFile($path . $fileName2);
                }
                $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | platforma'))])));
            }
        }

        $_platforma = $platforma->toArray();
        if ($this->Platforma->hasBehavior('Translate') && empty($_platforma['_translations'])) {
            $transFields = $this->Platforma->associations()->keys();
            $tFields = [];
            foreach ($transFields as $field) {
                if (strpos($field, '_translation') !== false) {
                    $field = substr($field, (strpos($field, '_') + 1));
                    $field = str_replace('_translation', '', $field);
                    $tFields[$field] = $_platforma[$field];
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $platforma->set('_translations', $translation);
        }


        $this->set(compact('platforma'));
        $this->set('_serialize', ['platforma']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Platforma id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $platforma = $this->Platforma->get($id);
        $fileName = $platforma->logo;
        $fileName2 = $platforma->logo_2;
        $path = $this->filePath['platforma'];
        if ($this->Platforma->delete($platforma)) {
            if (!empty($fileName)) {
                $this->Upload->deleteFile($path . $fileName);
            }
            if (!empty($fileName2)) {
                $this->Upload->deleteFile($path . $fileName2);
            }
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.', [$this->Txt->printAdmin(__('Admin | platforma'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.', [$this->Txt->printAdmin(__('Admin | platforma'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * setField method
     *
     * @param string|null $id Platforma id.
     * @param string|null $field Platforma field name.
     * @param mixed $value value to set.
     */
    public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->Platforma->find('all', ['conditions' => ['Platforma.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                    $item->{$field} = $value;
                    if ($this->Platforma->save($item)) {
                        $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, (!empty($value) ? 0 : 1)])), 'action' => $retAction];
                    } else {
                        $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                    }
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    /**
     * function sort
     * 
     */
    public function sort($field = 'kolejnosc') {
        $this->autoRender = false;
        $returnData = [];
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            foreach ($rqData as $data) {
                if (!empty($data['id'])) {
                    $this->Platforma->updateAll($data, ['id' => $data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

}
