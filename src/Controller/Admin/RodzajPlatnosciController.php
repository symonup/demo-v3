<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * RodzajPlatnosci Controller
 *
 * @property \App\Model\Table\RodzajPlatnosciTable $RodzajPlatnosci
 *
 * @method \App\Model\Entity\RodzajPlatnosci[] paginate($object = null, array $settings = [])
 */
class RodzajPlatnosciController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $cfgArr = [
            'order'=>['RodzajPlatnosci.kolejnosc'=>'desc'],
            'conditions'=>['RodzajPlatnosci.id IN'=>[1,2,5,7]]
        ];
        $rodzajPlatnosci = $this->RodzajPlatnosci->find('all', $cfgArr);

        $this->set(compact('rodzajPlatnosci'));
        $this->set('_serialize', ['rodzajPlatnosci']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $rodzajPlatnosci = $this->RodzajPlatnosci->newEntity();
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            $rodzajPlatnosci = $this->RodzajPlatnosci->patchEntity($rodzajPlatnosci, $rqData, ['translations' => $this->RodzajPlatnosci->hasBehavior('Translate')]);
            if ($this->RodzajPlatnosci->save($rodzajPlatnosci)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | rodzaj platnosci'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | rodzaj platnosci'))])));
        }
        $this->set(compact('rodzajPlatnosci'));
        $this->set('_serialize', ['rodzajPlatnosci']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Rodzaj Platnosci id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {

        if ($this->RodzajPlatnosci->hasBehavior('Translate')) {
            $rodzajPlatnosci = $this->RodzajPlatnosci->find('translations', [
                        'locales' => $this->lngIdList,
                        'conditions' => ['RodzajPlatnosci.' . $this->RodzajPlatnosci->primaryKey() => $id],
                        'contain' => []
                    ])->first();
        } else {
            $rodzajPlatnosci = $this->RodzajPlatnosci->find('all', [
                        'conditions' => ['RodzajPlatnosci.' . $this->RodzajPlatnosci->primaryKey() => $id],
                        'contain' => []
                    ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData = $this->request->getData();
            $rodzajPlatnosci = $this->RodzajPlatnosci->patchEntity($rodzajPlatnosci, $rqData, ['translations' => $this->RodzajPlatnosci->hasBehavior('Translate')]);
            if ($this->RodzajPlatnosci->save($rodzajPlatnosci)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | rodzaj platnosci'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | rodzaj platnosci'))])));
        }

        $_rodzajPlatnosci = $rodzajPlatnosci->toArray();
        if ($this->RodzajPlatnosci->hasBehavior('Translate') && empty($_rodzajPlatnosci['_translations'])) {
            $transFields = $this->RodzajPlatnosci->associations()->keys();
            $tFields = [];
            foreach ($transFields as $field) {
                if (strpos($field, '_translation') !== false) {
                    $field = substr($field, (strpos($field, '_') + 1));
                    $field = str_replace('_translation', '', $field);
                    $tFields[$field] = $_rodzajPlatnosci[$field];
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $rodzajPlatnosci->set('_translations', $translation);
        }


        $this->set(compact('rodzajPlatnosci'));
        $this->set('_serialize', ['rodzajPlatnosci']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Rodzaj Platnosci id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $rodzajPlatnosci = $this->RodzajPlatnosci->get($id);
        if ($this->RodzajPlatnosci->delete($rodzajPlatnosci)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.', [$this->Txt->printAdmin(__('Admin | rodzaj platnosci'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.', [$this->Txt->printAdmin(__('Admin | rodzaj platnosci'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * setField method
     *
     * @param string|null $id Rodzaj Platnosci id.
     * @param string|null $field Rodzaj Platnosci field name.
     * @param mixed $value value to set.
     */
    public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->RodzajPlatnosci->find('all', ['conditions' => ['RodzajPlatnosci.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                    $item->{$field} = $value;
                    if ($this->RodzajPlatnosci->save($item)) {
                        $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, !(bool) $value])), 'action' => $retAction];
                    } else {
                        $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                    }
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    /**
     * function sort
     * 
     */
    public function sort($field = 'kolejnosc') {
        $this->autoRender = false;
        $returnData = [];
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            foreach ($rqData as $data) {
                if (!empty($data['id'])) {
                    $this->RodzajPlatnosci->updateAll($data, ['id' => $data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
}
