<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Newsletter Controller
 *
 * @property \App\Model\Table\NewsletterTable $Newsletter
 *
 * @method \App\Model\Entity\Newsletter[] paginate($object = null, array $settings = [])
 */
class NewsletterController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->set('newsletterTypes',[0=>$this->Txt->printAdmin(__('Admin | Osób zapisanych przez stronę'))]);
        $this->set('newsletterTargetGroups',[0=>$this->Txt->printAdmin(__('Admin | Użytkownicy strony'))]);
    }
    public function index()
    {
    $cfgArr=[];
$newsletter = $this->Newsletter->find('all',$cfgArr);

        $this->set(compact('newsletter'));
        $this->set('_serialize', ['newsletter']);
//        $tmpView=new \Cake\View\View();
//        $tips=['send_newsletter'=>$tmpView->element($this->tipsPath.'send_newsletter')];
//        $this->set('tips',$tips);
    }

    /**
     * View method
     *
     * @param string|null $id Newsletter id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
       
        
        $newsletter = $this->Newsletter->find('all', [
            'conditions'=>['Newsletter.'.$this->Newsletter->primaryKey()=>$id],
            'contain' => []
        ])->first();
        
        
        $_newsletter = $newsletter->toArray();
        if ($this->Newsletter->hasBehavior('Translate') && empty($_newsletter['_translations'])) {
            $transFields=$this->Newsletter->associations()->keys();
            $tFields=[];
            foreach ($transFields as $field){
                if(strpos($field, '_translation')!==false){
                    $field= substr($field, (strpos($field, '_')+1));
                    $field=str_replace('_translation','', $field);
                    $tFields[$field]=$_newsletter[$field]; 
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $newsletter->set('_translations',$translation);
        }
        
        
        $this->set(compact('newsletter'));
        $this->set('_serialize', ['newsletter']);
//        $tmpView=new \Cake\View\View();
//        $tips=['newsletter_edit'=>$tmpView->element($this->tipsPath.'newsletter_edit')];
//        $this->set('tips',$tips);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $newsletter = $this->Newsletter->newEntity();
        if ($this->request->is('post')) {
        $rqData=$this->request->getData();
            $newsletter = $this->Newsletter->patchEntity($newsletter, $rqData);
            if ($this->Newsletter->save($newsletter)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | newsletter'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | newsletter'))])));
        }
        $this->set(compact('newsletter'));
        $this->set('_serialize', ['newsletter']);
//        $tmpView=new \Cake\View\View();
//        $tips=['newsletter_edit'=>$tmpView->element($this->tipsPath.'newsletter_edit')];
//        $this->set('tips',$tips);
    }

    /**
     * Edit method
     *
     * @param string|null $id Newsletter id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
    
        
        $newsletter = $this->Newsletter->find('all', [
            'conditions'=>['Newsletter.'.$this->Newsletter->primaryKey()=>$id],
            'contain' => []
        ])->first();
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData=$this->request->getData();
            $newsletter = $this->Newsletter->patchEntity($newsletter, $rqData,['translations'=>$this->Newsletter->hasBehavior('Translate')]);
            if ($this->Newsletter->save($newsletter)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | newsletter'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | newsletter'))])));
        }
        
        $_newsletter = $newsletter->toArray();
        if ($this->Newsletter->hasBehavior('Translate') && empty($_newsletter['_translations'])) {
            $transFields=$this->Newsletter->associations()->keys();
            $tFields=[];
            foreach ($transFields as $field){
                if(strpos($field, '_translation')!==false){
                    $field= substr($field, (strpos($field, '_')+1));
                    $field=str_replace('_translation','', $field);
                    $tFields[$field]=$_newsletter[$field]; 
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $newsletter->set('_translations',$translation);
        }
        
        
        $this->set(compact('newsletter'));
        $this->set('_serialize', ['newsletter']);
//        $tmpView=new \Cake\View\View();
//        $tips=['newsletter_edit'=>$tmpView->element($this->tipsPath.'newsletter_edit')];
//        $this->set('tips',$tips);
    }

    /**
     * Delete method
     *
     * @param string|null $id Newsletter id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $newsletter = $this->Newsletter->get($id);
        if ($this->Newsletter->delete($newsletter)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.',[$this->Txt->printAdmin(__('Admin | newsletter'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.',[$this->Txt->printAdmin(__('Admin | newsletter'))])));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function send($id = null) {
        $this->autoRender=false;
        $count = $this->Newsletter->find('all',['conditions'=>['Newsletter.status'=>1]])->count();
        if ($count > 0) {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | Newsletter nie może zostać wysłany ponieważ inny newsletter jest w trakcie wysyłki. Poczekaj aż wysyłka zostanie zakończona.')));
        } else {
            $newsletter = $this->Newsletter->find('all',['conditions'=>['Newsletter.id'=>$id]])->first();
            $newsletter->status = 1;
            $newsletter->data = new \DateTime(date('Y-m-d H:i:s'));
            if ($this->Newsletter->save($newsletter)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | Newsletter został ustawiony do wysyłki')));
            } else {
                $this->Flash->error($this->Txt->printAdmin(__('Admin | The data could not be saved. Please, try again.')));
            }
        }
        return $this->redirect(['action' => 'index']);
    }
}
