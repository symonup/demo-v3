<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Bonus Controller
 *
 * @property \App\Model\Table\BonusTable $Bonus
 *
 * @method \App\Model\Entity\Bonus[] paginate($object = null, array $settings = [])
 */
class BonusController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
    $cfgArr=[];
$bonus = $this->Bonus->find('all',$cfgArr);

        $this->set(compact('bonus'));
        $this->set('_serialize', ['bonus']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $bonus = $this->Bonus->newEntity();
        if ($this->request->is('post')) {
        $rqData=$this->request->getData();
            $bonus = $this->Bonus->patchEntity($bonus, $rqData,['translations'=>$this->Bonus->hasBehavior('Translate')]);
            if ($this->Bonus->save($bonus)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | bonus'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | bonus'))])));
        }
        $this->set(compact('bonus'));
        $this->set('_serialize', ['bonus']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Bonus id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
    
    if($this->Bonus->hasBehavior('Translate')){
        $bonus = $this->Bonus->find('translations', [
            'locales' => $this->lngIdList,
            'conditions' => ['Bonus.'.$this->Bonus->primaryKey() => $id],
            'contain' => []
        ])->first();
        }
        else{
        $bonus = $this->Bonus->find('all', [
            'conditions'=>['Bonus.'.$this->Bonus->primaryKey()=>$id],
            'contain' => []
        ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData=$this->request->getData();
            $bonus = $this->Bonus->patchEntity($bonus, $rqData,['translations'=>$this->Bonus->hasBehavior('Translate')]);
            if ($this->Bonus->save($bonus)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | bonus'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | bonus'))])));
        }
        
        $_bonus = $bonus->toArray();
        if ($this->Bonus->hasBehavior('Translate') && empty($_bonus['_translations'])) {
            $transFields=$this->Bonus->associations()->keys();
            $tFields=[];
            foreach ($transFields as $field){
                if(strpos($field, '_translation')!==false){
                    $field= substr($field, (strpos($field, '_')+1));
                    $field=str_replace('_translation','', $field);
                    $tFields[$field]=$_bonus[$field]; 
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $bonus->set('_translations',$translation);
        }
        
        
        $this->set(compact('bonus'));
        $this->set('_serialize', ['bonus']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Bonus id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $bonus = $this->Bonus->get($id);
        if ($this->Bonus->delete($bonus)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.',[$this->Txt->printAdmin(__('Admin | bonus'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.',[$this->Txt->printAdmin(__('Admin | bonus'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

/**
     * setField method
     *
     * @param string|null $id Bonus id.
     * @param string|null $field Bonus field name.
     * @param mixed $value value to set.
     */

public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->Bonus->find('all', ['conditions' => ['Bonus.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                        $item->{$field} = $value;
                        if ($this->Bonus->save($item)) {
                            $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, (!empty($value)?0:1)])), 'action' => $retAction];
                        } else {
                            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                        }
                    
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    /**
* function sort
* 
*/
public function sort($field='kolejnosc'){
        $this->autoRender=false;
        $returnData=[];
        if($this->request->is('post')){
            $rqData=$this->request->getData();
            foreach ($rqData as $data){
                if(!empty($data['id'])){
                    $this->Bonus->updateAll($data, ['id'=>$data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    }
