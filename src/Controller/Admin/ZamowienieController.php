<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Core\Configure;
use mPDF;

/**
 * Zamowienie Controller
 *
 * @property \App\Model\Table\ZamowienieTable $Zamowienie
 *
 * @method \App\Model\Entity\Zamowienie[] paginate($object = null, array $settings = [])
 */
class ZamowienieController extends AppController {

    private $subiektFvFields = [
        'TYP DOKUMENTU' => 'typ',
        'Status dokumentu' => 'status',
        'Status rejestracji fiskalnej dokumentu' => 'status_fiskalny',
        'Numer dokumentu' => 'id_dokumentu',
        'Numer dokumentu dostawcy' => 'numer_dostawcy',
        'Rozszerzenie numeru wpisane przez użytkownika' => 'rozszerzenie_numeru',
        'Pełny numer dokumentu' => 'numer_dokumentu',
        'Numer dokumentu korygowanego' => 'numer_do_korekty',
        'Data wystawienia dokumentu korygowanego' => 'data_wystawienia_do_korekty',
        'Numer zamówienia' => 'numer_zamowienia',
        'Magazyn docelowy dla MM' => 'magazyn_docelowy_mm',
        'Kod identyfikacyjny kontrahenta' => 'kod_kontrahenta',
        'Nazwa skrócona kontrahenta' => 'nazwa_skrocona',
        'Nazwa pełna kontrahenta' => 'nazwa_pelna',
        'Miasto kontrahenta' => 'miasto',
        'Kod pocztowy kontrahenta ' => 'kod_pocztowy',
        'Ulica i numer kontrahenta' => 'adres',
        'NIP kontrahenta' => 'nip',
        'Kategoria' => 'kategoria_dokumentu',
        'Podtytuł kategorii' => 'podtytul_kategorii',
        'Miejsce wystawienia' => 'miejsce_wystawienia',
        'Data wystawienia' => 'data_wystawienia',
        'Data sprzedaży' => 'data_sprzedazy',
        'Data otrzymania' => 'data_otrzymania',
        'Liczba pozycji' => 'ilosc_pozycji',
        'Czy dokument wystawiany wg cen netto' => 'wg_cen_netto',
        'Aktywna cena' => 'aktywna_cena',
        'Wartość netto' => 'wartosc_netto',
        'Wartość VAT' => 'wartosc_vat',
        'Wartość brutto' => 'wartosc_brutto',
        'Koszt' => 'koszt',
        'Rabat nazwa' => 'rabat_nazwa',
        'Rabat procent' => 'rabat_procent',
        'Forma płatności' => 'forma_platnosci',
        'Termin płatności' => 'termin_platnosci',
        'Kwota zapłacona przy odbiorze dokumentu' => 'kwota_zaplacona_przy_odbiorze',
        'Wartość do zapłaty' => 'wartosc_do_zaplaty',
        'Zaokrąglenie wartości do zapłaty' => 'zaokraglenie_do_zaplaty',
        'Zaokrąglenie wartości VAT' => 'zaokraglenie_vat',
        'Automatycznie przeliczana tabela VAT i wartość dokumentu' => 'automatyczne_przeliczanie',
        'Statusy rozszerzone i specjalne dokumentów' => 'statusy_specjalne',
        'Nazwisko i imię osoby wystawiającej dokument' => 'wystawiajacy',
        'Nazwisko i imię osoby odbierającej dokument' => 'odbiorca',
        'Podstawa wydania dokumentu' => 'podstawa_wydania',
        'Wartość wydanych opakowań' => 'wartosc_opakowan',
        'Wartość zwróconych opakowań' => 'wartosc_zwroconych_opakowan',
        'Waluta' => 'waluta',
        'Kurs waluty' => 'kurs_waluty',
        'Uwagi' => 'uwagi',
        'Komentarz' => 'komentarz',
        'Podtytuł dokumentu' => 'podtytul',
        'Nie używane' => 'empty_data',
        'Przeprowadzony import dokumentu' => 'import_dokumentu',
        'Dokument eksportowy' => 'eksportowy',
        'Rodzaj transakcji' => 'rodzaj_transakcji',
        'Płatność kartą nazwa' => 'platnosc_karta_nazwa',
        'Płatność kartą kwota' => 'platnosc_karta_kwota',
        'Płatność kredytowa nazwa' => 'platnosc_kredyt_nazwa',
        'Płatność kredytowa kwota' => 'platnosc_kredyt_kwota',
        'Państwo kontrahenta' => 'panstwo',
        'Prefiks państwa UE kontrahenta' => 'prefix_ue',
        'Czy kontrahent jest unijny' => 'czy_ue'
    ];
    private $subiektFvPozycje = [
        'Liczba porządkowa' => 'lp',
        'Typ towaru' => 'typ_towaru',
        'Kod identyfikacyjny towaru' => 'symbol',
        'Rabat procentowy' => 'rabat_procentowy',
        'Rabat od ceny' => 'rabat_od_ceny',
        'Rabat na pozycji kumulowany z rabatem od całego dokumentu ' => 'rabat_pozycja',
        'Rabat zablokowany dla tej pozycji ' => 'rabat_blokada',
        'Wartość udzielonego na pozycji rabatu' => 'rabat_wartosc',
        'Wysokość rabatu udzielonego na pozycji w procentach ' => 'rabat_procent',
        'Jednostka miary ' => 'jednostka',
        'Ilość towaru w jednostce miary ' => 'ilosc_jednostka',
        'Ilość towaru w jednostce magazynowej ' => 'ilosc_magazyn',
        'Cena magazynowa towaru' => 'cena_magazyn',
        'Cena netto towaru ' => 'cena_netto',
        'Cena brutto towaru' => 'cena_brutto',
        'Wysokość stawki podatku VAT w procentach ' => 'stawka_vat',
        'Wartość netto pozycji' => 'wartosc_netto',
        'Wartość VAT' => 'wartosc_vat',
        'Wartość brutto' => 'wartosc_brutto',
        'Koszt pozycji' => 'koszt',
        'Opis usługi jednorazowej ' => 'opis_uslugi',
        'Nazwa usługi jednorazowej ' => 'nazwa_uslugi'
    ];
    private $subiektProduktFields = [
        'Typ towaru' => 'typ',
        'Kod identyfikacyjny' => 'symbol',
        'Kod towaru u producenta' => 'kod_producenta',
        'Kod paskowy' => 'kod_paskowy',
        'Nazwa' => 'nazwa',
        'Opis' => 'opis',
        'Nazwa towaru dla urządzeń fiskalnych' => 'nazwa_fiskalna',
        'Symbol SWW lub KU ' => 'symbol_sww_ku',
        'Symbol PKWiU ' => 'symbol_pkwiu',
        'Podstawowa jednostka miary ' => 'jednostka',
        'Symbol stawki podatku VAT ' => 'vat_stawka',
        'Wysokość stawki podatku VAT w procentach ' => 'vat_procent',
        'Symbol stawki podatku VAT przy zakupie ' => 'vat_stawka_zakup',
        'Wysokość stawki podatku VAT przy zakupie w procentach ' => 'vat_procent_zakup',
        'Ostatnia cena zakupu netto' => 'cena_zakupu',
        'Cena zakupu walutowa ' => 'cena_zakupu_waluta',
        'Jednostka miary przy zakupie' => 'jednostka_zakup',
        'Kurs waluty służący do kalkulacji ceny zakupu' => 'kurs_walut',
        'Symbol waluty' => 'waluta',
        'Kod opakowania związanego z towarem' => 'kod_opakowania',
        'Jednostka miary dla stanu minimalnego' => 'jednostka_stan_minimalny',
        'Stan minimalny' => 'stan_minimalny',
        'Średni czas dostawy' => 'stan_sredni',
        'Kod identyfikacyjny producenta / dostawcy' => 'identyfikator_producenta',
        'Data ważności jako konkretny dzień' => 'data_waznosci',
        'Data ważności jako ilość dni od ostatniej dostawy' => 'data_waznosci_dni',
        'Jednostka miary dla objętości ' => 'jednostka_objetosc',
        'Objętość towaru (wybrana jednostka)' => 'objetosc',
        'Ilość roboczo-godzin w jednostce podstawowej usługi' => 'usluga_ilosc_godzin',
        'Rodzaj roboczo-godzin usługi ' => 'usluga_rodzaj_godzin',
        'Cena otwarta ' => 'cena_otwarcia',
        'Uwagi' => 'uwagi',
        'Podstawa kalkulacji cen' => 'kalkulacja_cen',
        'Towar ważony na wadze etykietującej' => 'wazony_na_wadze',
        'Pole użytkownika 1' => 'pole_1',
        'Pole użytkownika 2' => 'pole_2',
        'Pole użytkownika 3' => 'pole_3',
        'Pole użytkownika 4' => 'pole_4',
        'Pole użytkownika 5' => 'pole_5',
        'Pole użytkownika 6' => 'pole_6',
        'Pole użytkownika 7' => 'pole_7',
        'Pole użytkownika 8' => 'pole_8'
    ];

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->set('statusy', $this->statusy);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $cfgArr = [
            'contain' => ['Wysylka', 'ZamowienieTowar', 'FakturaSubiekt']
        ];
        $filter = $this->session->read('zamowieniaFilter');
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            if (key_exists('clear_filter', $rqData)) {
                $filter = [];
            } else if (key_exists('filter', $rqData)) {
                $filter = $rqData;
            }
        }
        if (!empty($filter)) {
            if (!empty($filter['status'])) {
                $cfgArr['conditions']['Zamowienie.status'] = $filter['status'];
            }
            if (!empty($filter['wysylka_id'])) {
                $cfgArr['conditions']['Zamowienie.wysylka_id'] = $filter['wysylka_id'];
            }
            if (!empty($filter['bonusy'])) {
                switch ($filter['bonusy']) {
                    case 1 : {
                            $cfgArr['conditions']['OR']['Zamowienie.bonus_suma IS'] = NULL;
                            $cfgArr['conditions']['OR']['Zamowienie.bonus_suma'] = 0;
                        } break;
                    case 2 : {
                            $cfgArr['conditions']['Zamowienie.uzytkownik_id IS NOT'] = NULL;
                            $cfgArr['conditions']['Zamowienie.bonus_suma >'] = 0;
                        } break;
                    case 3 : {
                            $cfgArr['conditions']['Zamowienie.uzytkownik_id IS NOT'] = NULL;
                            $cfgArr['conditions']['Zamowienie.bonus_suma >'] = 0;
                            $cfgArr['conditions']['OR']['Zamowienie.bonus_przyznany IS'] = NULL;
                            $cfgArr['conditions']['OR']['Zamowienie.bonus_przyznany'] = 0;
                        } break;
                    case 4 : {
                            $cfgArr['conditions']['Zamowienie.uzytkownik_id IS NOT'] = NULL;
                            $cfgArr['conditions']['Zamowienie.bonus_suma >'] = 0;
                            $cfgArr['conditions']['Zamowienie.bonus_przyznany >'] = 0;
                        } break;
                }
            }
            if (!empty($filter['preorder'])) {
                if ((int) $filter['preorder'] > 0) {
                    $preorderIds = $this->Zamowienie->ZamowienieTowar->find('list', ['keyField' => 'zamowienie_id', 'valueField' => 'zamowienie_id', 'group' => 'zamowienie_id'])->where(['preorder' => 1])->toArray();
                    if (empty($preorderIds)) {
                        $preorderIds = [null];
                    }
                    if ($filter['preorder'] == 1) {
                        $cfgArr['conditions']['Zamowienie.id IN'] = $preorderIds;
                    } else {
                        $cfgArr['conditions']['Zamowienie.id NOT IN'] = $preorderIds;
                    }
                }
            }
        }
        $cfgArr['order'] = ['Zamowienie.id' => 'desc'];
        $zamowienie = $this->Zamowienie->find('all', $cfgArr);
        $this->session->write('zamowieniaFilter', $filter);
        $this->loadModel('Wysylka');
        $wysylki = $this->Wysylka->find('list', ['keyField' => 'id', 'valueField' => 'nazwa'])->toArray();
        $filterBonusy = [
            1 => $this->Txt->printAdmin(__('Admin | Bez bonusów')),
            2 => $this->Txt->printAdmin(__('Admin | Z bonusami')),
            3 => $this->Txt->printAdmin(__('Admin | Bonusy nie przyznane')),
            4 => $this->Txt->printAdmin(__('Admin | Bonusy przyznane'))
        ];
        $this->set(compact('zamowienie', 'filter', 'wysylki', 'filterBonusy'));
        $this->set('_serialize', ['zamowienie']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($userId = null) {
        $zamowienie = $this->Zamowienie->newEntity();
        if ($this->request->is('post')) {
            $klient = $this->Zamowienie->Uzytkownik->find('all', ['conditions' => ['Uzytkownik.id' => $userId], 'contain' => ['Waluta']])->first();
            Configure::write('currencyDefault', $klient->walutum->symbol);
            $rqData = $this->request->getData();
            if (!empty($rqData['zamowienie_towar']) && !empty($klient)) {
                $token = uniqid();
                $wartoscRazemNetto = 0;
                $wartoscRazemBrutton = 0;
                foreach ($rqData['zamowienie_towar'] as &$item) {
                    $itemRazemNetto = ($item['ilosc'] * $item['ilosc_w_kartonie']) * $item['cena_za_sztuke_netto'];
                    $itemRazemBrutto = ($item['ilosc'] * $item['ilosc_w_kartonie']) * $item['cena_za_sztuke_brutto'];
                    $wartoscRazemNetto += $itemRazemNetto;
                    $wartoscRazemBrutton += $itemRazemBrutto;
                    $item['cena_razem_netto'] = $itemRazemNetto;
                    $item['cena_razem_brutto'] = $itemRazemBrutto;
                }
                $rqData['wartosc_razem'] = $wartoscRazemBrutton;
                $rqData['wartosc_razem_netto'] = $wartoscRazemNetto;
                $rqData['wartosc_vat'] = $wartoscRazemBrutton - $wartoscRazemNetto;
                $rqData['wartosc_produktow_netto'] = $wartoscRazemNetto;
                $rqData['wartosc_produktow'] = $wartoscRazemBrutton;
                $rqData['uzytkownik_id'] = $klient->id;
                $rqData['lng'] = \Cake\I18n\I18n::locale();
                $rqData['wysylka_telefon'] = (!empty($rqData['kierunkowy']) ? $rqData['kierunkowy'] . ' ' . (str_replace($rqData['kierunkowy'], $rqData['kierunkowy'], $rqData['wysylka_telefon'])) : $rqData['wysylka_telefon']);
                $rqData['waluta_id'] = $klient->waluta_id;
                $rqData['waluta_symbol'] = $klient->walutum->symbol;
                $rqData['imie'] = $klient->imie;
                $rqData['nazwisko'] = $klient->nazwisko;
                $rqData['email'] = $klient->email;
                $rqData['firma'] = $klient->firma;
                $rqData['nip'] = $klient->nip;
                $rqData['telefon'] = (!empty($klient->kierunkowy) ? $klient->kierunkowy . ' ' . str_replace($klient->kierunkowy, $klient->kierunkowy, $klient->telefon) : $klient->telefon);
                $rqData['uwagi'] = $rqData['uwagi'];
                $rqData['status'] = 'zlozone';
                $rqData['data'] = new \DateTime(date('Y-m-d H:i:s'));
                $rqData['rabat'] = $klient->rabat_typ;
                $rqData['telefoniczne'] = 1;
                $rqData['domena'] = $_SERVER['SERVER_NAME'];
                $rqData['token'] = $token;
                $rqData['adres_faktury'] = $this->Txt->printAddres($rqData, '<br/>', 'faktura_');
                $rqData['adres_wysylki'] = $this->Txt->printAddres($rqData, '<br/>', 'wysylka_');
                $rqData['administrator_dane'] = $this->Auth->user('imie') . ' ' . $this->Auth->user('nazwisko');

                $zamowienie = $this->Zamowienie->patchEntity($zamowienie, $rqData);
                if ($this->Zamowienie->save($zamowienie)) {
                    $zamowienie->numer = $this->Zamowienie->newOrderNumber();
                    $this->Zamowienie->save($zamowienie);
                    $zamowienie = $this->Zamowienie->find('all', ['conditions' => ['Zamowienie.id' => $zamowienie->id], 'contain' => ['ZamowienieTowar' => ['sort' => ['ZamowienieTowar.id' => 'asc']]]])->first();
                    $this->loadComponent('Mail');
                    $this->loadComponent('Replace');
                    $this->loadModel('Szablon');
                    $replace = [
                        'zamowienie-id' => $zamowienie->id,
                        'zamowienie-numer' => (!empty($zamowienie->numer) ? $zamowienie->numer : $zamowienie->id),
                        'zamowienie-imie' => $zamowienie->imie,
                        'zamowienie-nazwisko' => $zamowienie->nazwisko,
                        'zamowienie-adres-wysylki' => $zamowienie->adres_wysylki,
                        'zamowienie-adres-faktury' => $zamowienie->adres_faktury,
                        'zamowienie-email' => $zamowienie->email,
                        'zamowienie-firma' => $zamowienie->firma,
                        'zamowienie-nip' => $zamowienie->nip,
                        'zamowienie-telefon' => $zamowienie->telefon,
                        'zamowienie-wartosc-razem' => $this->Txt->cena($zamowienie->wartosc_razem),
                        'zamowienie-wartosc-produktow' => $this->Txt->cena($zamowienie->wartosc_produktow),
                        'zamowienie-waluta' => $zamowienie->waluta_symbol,
                        'zamowienie-uwagi' => nl2br($zamowienie->uwagi),
                        'zamowienie-status' => $this->Translation->get('statusy_zamowien.' . $zamowienie->status),
                        'zamowienie-data' => (!empty($zamowienie->data) ? $this->Txt->printDate($zamowienie->data, 'Y-m-d H:i') : ''),
                        'zamowienie-token' => $zamowienie->token,
                        'zamowienie-rabat' => $zamowienie->rabat_wartosc,
                        'zamowienie-opiekun' => $zamowienie->administrator_dane,
                        'adres-ip' => $this->request->clientIp()
                    ];
                    $this->loadComponent('Order');
                    $sendTo = [];
                    $sendToPh = Configure::read('admin.ph_info');
                    if (!empty($sendToPh)) {
                        $phEmail = $this->Auth->user('email');
                        if (!empty($phEmail)) {
                            $sendTo[] = $phEmail;
                        }
                    }
                    $orderFileName = $this->Order->excel($zamowienie, $this->filePath['orders']);
                    $szablon = $this->Szablon->findByNazwa('zamowienie_zlozone_telefoniczne')->first();
                    $message = $this->Replace->getTemplate($szablon->tresc, $replace, $zamowienie->zamowienie_towar);
                    $temat = $this->Replace->getTemplate($szablon->temat, $replace);
                    $this->Mail->sendMail($zamowienie->email, $temat, $message, ['replyTo' => ((!empty($sendToPh) && !empty($phEmail)) ? $phEmail : Configure::read('mail.domyslny_email'))]);
                    $szablonInfo = $this->Szablon->findByNazwa('admin_info_zamowienie_telefoniczne')->first();
                    $tematInfo = $this->Replace->getTemplate($szablonInfo->temat, $replace);
                    $messageInfo = $this->Replace->getTemplate($szablonInfo->tresc, $replace, $zamowienie->zamowienie_towar);

                    $sendTo[] = Configure::read('mail.domyslny_email');
                    $this->Mail->sendInfo($sendTo, $tematInfo, $messageInfo, ['attachment' => [$orderFileName => $this->filePath['orders'] . $orderFileName]]);


                    $this->Flash->success($this->Txt->printAdmin(__('Admin | Zamówienie zostało dodane')));

                    return $this->redirect(['action' => 'view', $zamowienie->id]);
                } else {
                    $this->Flash->error($this->Txt->printAdmin(__('Admin | Błąd zapisu zamówienia')));
                }
            } else {
                $this->Flash->error($this->Txt->printAdmin(__('Admin | Brak produktów w zamówieniu')));
            }
        }
        $klient = null;
        if (!empty($userId)) {
            $klienci = null;
            $klient = $this->Zamowienie->Uzytkownik->find('all', ['conditions' => ['Uzytkownik.id' => $userId], 'contain' => ['UzytkownikWysylka', 'UzytkownikFaktura', 'Waluta']])->first();
            if (!empty($klient)) {
                Configure::write('currencyDefault', $klient->walutum->symbol);
                $towarCenaCond = [];
                if (!empty($klient->walutum)) {
                    $towarCenaCond = ['TowarCena.waluta_id' => $klient->walutum->id, 'TowarCena.uzytkownik_id IS' => null];
                } else {
                    $towarCenaCond = ['TowarCena.waluta_id' => $this->defCurrency['id'], 'TowarCena.uzytkownik_id IS' => null];
                }
                $towary = $this->Zamowienie->Towar->find('all', ['contain' => ['TowarZdjecie' => ['sort' => ['TowarZdjecie.domyslne' => 'desc', 'TowarZdjecie.kolejnosc' => 'desc']], 'TowarCena' => ['conditions' => $towarCenaCond, 'Vat', 'Jednostka'], 'TowarAtrybut' => ['TowarZdjecie' => ['sort' => ['TowarZdjecie.domyslne' => 'desc', 'TowarZdjecie.kolejnosc' => 'desc']], 'AtrybutPodrzedne', 'Atrybut', 'conditions' => ['wariant' => 1]]]])->toArray();
                $this->setUserRabat($klient->toArray());
            } else {
                $this->Flash->error($this->Txt->printAdmin(__('Admin | Nie znaleziono klienta o podanym identyfikatorze')));
                $klienci = $this->Zamowienie->Uzytkownik->find('all')->toArray();
            }
        } else {
            $klienci = $this->Zamowienie->Uzytkownik->find('all')->toArray();
        }

        $this->set(compact('zamowienie', 'klienci', 'towary', 'userId', 'klient'));
        $this->set('_serialize', ['zamowienie']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Zamowienie id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $conditions = ['Zamowienie.' . $this->Zamowienie->primaryKey() => $id];
        $zamowienie = $this->Zamowienie->find('all', [
                    'conditions' => $conditions,
                    'contain' => ['ZamowienieDhl', 'ZamowienieTowar', 'KartaPodarunkowaKod' => ['KartaPodarunkowa']]
                ])->first();
        if (empty($zamowienie)) {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | Brak zamówienia lub zamówienie nie jest przypisane do Ciebie')));
            return $this->redirect(['action' => 'index']);
        }
        $this->set(compact('zamowienie'));
        $this->set('_serialize', ['zamowienie']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Zamowienie id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $conditions = ['Zamowienie.' . $this->Zamowienie->primaryKey() => $id];
        $zamowienie = $this->Zamowienie->find('all', [
                    'conditions' => $conditions
                ])->first();
        if (empty($zamowienie)) {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | Brak zamówienia lub zamówienie nie jest przypisane do Ciebie')));
            return $this->redirect(['action' => 'index']);
        }
        if ($this->Zamowienie->delete($zamowienie)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.', [$this->Txt->printAdmin(__('Admin | zamowienie'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.', [$this->Txt->printAdmin(__('Admin | zamowienie'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * setField method
     *
     * @param string|null $id Zamowienie id.
     * @param string|null $field Zamowienie field name.
     * @param mixed $value value to set.
     */
    public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->Zamowienie->find('all', ['conditions' => ['Zamowienie.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                    $item->{$field} = $value;
                    if ($this->Zamowienie->save($item)) {
                        $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, !(bool) $value])), 'action' => $retAction];
                    } else {
                        $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                    }
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    /**
     * function sort
     * 
     */
    public function sort($field = 'kolejnosc') {
        $this->autoRender = false;
        $returnData = [];
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            foreach ($rqData as $data) {
                if (!empty($data['id'])) {
                    $this->Zamowienie->updateAll($data, ['id' => $data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    public function update($id = null, $type = null) {
        $this->autoRender = false;
        $returnArray = [];
        $zamowienie = $this->Zamowienie->find('all', ['conditions' => ['Zamowienie.id' => $id], 'contain' => ['ZamowienieTowar', 'Uzytkownik']])->first();
        Configure::write('currencyDefault', $zamowienie->waluta_symbol);
        if (!empty($zamowienie)) {
            if ($this->request->is('post')) {
                $successMessage = $this->Txt->printAdmin(__('Admin | Dane zostały zapisane'));
                $rqData = $this->request->getData();
                $returnValues = [];
                if (!empty($rqData)) {
                    $afterSaveAction = null;
                    if (!empty($type)) {
                        switch ($type) {
                            case 'bonus' : {
                                    if (!empty($zamowienie->uzytkownik_id)) {
                                        $bonusPrzyznany = 0;
                                        if (!empty($zamowienie->bonus_przyznany)) {
                                            $bonusPrzyznany = $zamowienie->bonus_przyznany;
                                        }
                                        $saveBonus = $bonusPrzyznany + $rqData['bonus_przyznany'];
                                        if ($saveBonus < 0) {
                                            $saveBonus = 0;
                                        }
                                        $zamowienie->set('bonus_przyznany', ($saveBonus));
                                        $this->Zamowienie->Uzytkownik->updateAll(['bonusy_suma' => ($zamowienie->uzytkownik->bonusy_suma + $rqData['bonus_przyznany'])], ['id' => $zamowienie->uzytkownik_id]);
                                        $returnValues = ['bonus_przyznany' => $this->Txt->cena($zamowienie->bonus_przyznany, $zamowienie->waluta_symbol, true)];
                                    }
                                }break;
                            case 'status': {
                                    $zamowienie->status = $rqData['status'];
                                    if ($rqData['status'] == 'przyjete') {
                                        $zamowienie->set('termin_wysylki', $rqData['termin_wysylki']);
                                    }
                                    if ($rqData['status'] == 'wyslane') {
                                        if (!empty($rqData['kurier_typ'])) {
                                            $zamowienie->set('kurier_typ', $rqData['kurier_typ']);
                                        }
                                        if (!empty($rqData['nr_listu_przewozowego'])) {
                                            $zamowienie->set('nr_listu_przewozowego', $rqData['nr_listu_przewozowego']);
                                        }
                                    }
                                    $successMessage = $this->Txt->printAdmin(__('Admin | Status został zmieniony'));
                                    $afterSaveAction = ['action' => 'changeStatus'];
                                }break;
                        }
                    } else {
                        foreach ($rqData as $key => $value) {
                            $zamowienie->{$key} = $value;
                        }
                    }
                    if ($this->Zamowienie->save($zamowienie)) {
                        if (!empty($afterSaveAction)) {
                            $this->{$afterSaveAction['action']}($zamowienie);
                        }
                        $returnArray = [
                            'status' => 'success',
                            'message' => $successMessage,
                            'values' => $returnValues
                        ];
                    } else {
                        $returnArray = [
                            'status' => 'error',
                            'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')),
                            'errors' => $zamowienie->errors()
                        ];
                    }
                } else {
                    $returnArray = [
                        'status' => 'error',
                        'message' => $this->Txt->printAdmin(__('Admin | Brak danych do zapisu'))
                    ];
                }
            } else {
                $returnArray = [
                    'status' => 'error',
                    'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowe rządanie'))
                ];
            }
        } else {
            $returnArray = [
                'status' => 'error',
                'message' => $this->Txt->printAdmin(__('Admin | Błędny identyfikator zamówienia'))
            ];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnArray));
        return $this->response;
    }

    private function changeStatus($zamowienie) {
        if (empty($zamowienie)) {
            return false;
        }
        $return = true;
        \Cake\I18n\I18n::locale($zamowienie->lng);
        $this->loadComponent('Mail');
        $this->loadComponent('Replace');
        $this->loadModel('Szablon');
        $replace = [
            'zamowienie-id' => $zamowienie->id,
            'zamowienie-numer' => (!empty($zamowienie->numer) ? $zamowienie->numer : $zamowienie->id),
            'zamowienie-imie' => $zamowienie->imie,
            'zamowienie-nazwisko' => $zamowienie->nazwisko,
            'zamowienie-adres-wysylki' => $zamowienie->adres_wysylki,
            'zamowienie-adres-faktury' => $zamowienie->adres_faktury,
            'zamowienie-email' => $zamowienie->email,
            'zamowienie-firma' => $zamowienie->firma,
            'zamowienie-nip' => $zamowienie->nip,
            'zamowienie-telefon' => $zamowienie->telefon,
            'zamowienie-wartosc-razem' => $this->Txt->cena($zamowienie->wartosc_razem),
            'zamowienie-wartosc-produktow' => $this->Txt->cena($zamowienie->wartosc_produktow),
            'zamowienie-waluta' => $zamowienie->waluta_symbol,
            'zamowienie-uwagi' => nl2br($zamowienie->uwagi),
            'zamowienie-status' => $this->Translation->get('statusy_zamowien.' . $zamowienie->status),
            'zamowienie-data' => (!empty($zamowienie->data) ? $this->Txt->printDate($zamowienie->data, 'Y-m-d H:i') : ''),
            'termin-wysylki' => (!empty($zamowienie->termin_wysylki) ? $this->Txt->printDate($zamowienie->termin_wysylki, 'Y-m-d') : ''),
            'zamowienie-token' => $zamowienie->token,
            'zamowienie-rabat' => $zamowienie->rabat_wartosc,
            'zamowienie-opiekun' => $zamowienie->administrator_dane,
            'link-sledzenia' => $this->Txt->kurierLink($zamowienie->kurier_typ, $zamowienie->nr_listu_przewozowego),
            'nr-listu-przewozowego' => $zamowienie->nr_listu_przewozowego,
            'adres-ip' => $this->request->clientIp()
        ];
        $message = '';
        $szablon = null;
        $temat = '';
        switch ($zamowienie->status) {
            case 'wyslane' : {
                    $szablon = $this->Szablon->findByNazwa('zamowienie_status_wyslane')->first();
                    $message = $this->Replace->getTemplate($szablon->tresc, $replace, $zamowienie->zamowienie_towar);
                } break;
            case 'przyjete' : {
                    $szablon = $this->Szablon->findByNazwa('zamowienie_status_przyjete')->first();
                    $message = $this->Replace->getTemplate($szablon->tresc, $replace, $zamowienie->zamowienie_towar);
                } break;
            case 'zrealizowane' : {
                    $szablon = $this->Szablon->findByNazwa('zamowienie_status_zrealizowane')->first();
                    $message = $this->Replace->getTemplate($szablon->tresc, $replace, $zamowienie->zamowienie_towar);
                } break;
            case 'anulowane' : {
                    $szablon = $this->Szablon->findByNazwa('zamowienie_status_anulowane')->first();
                    $message = $this->Replace->getTemplate($szablon->tresc, $replace, $zamowienie->zamowienie_towar);
                } break;
        }
        if (!empty($message) && !empty($szablon)) {

            $temat = $this->Replace->getTemplate($szablon->temat, $replace);
            $sendTo = [];
            $sendToPh = Configure::read('admin.ph_info');
            if (!empty($sendToPh)) {
                $phEmail = $zamowienie->administrator->email;
                if (!empty($phEmail)) {
                    $sendTo[] = $phEmail;
                }
            }
            $sendTo[] = $zamowienie->email;
            $this->Mail->sendMail($sendTo, $temat, $message, ['replyTo' => ((!empty($sendToPh) && !empty($phEmail)) ? $phEmail : Configure::read('mail.domyslny_email'))]);
        }
        return $return;
    }

    public function getExcel($id = null) {
        $this->autoRender = false;
        $zamowienie = $this->Zamowienie->find('all', ['conditions' => ['Zamowienie.id' => $id], 'contain' => ['ZamowienieTowar' => ['sort' => ['ZamowienieTowar.mix' => 'asc', 'ZamowienieTowar.id' => 'asc']]]])->first();
        Configure::write('currencyDefault', $zamowienie->waluta_symbol);
        if (!empty($zamowienie)) {
            $this->loadComponent('Order');
            $fileName = $this->Order->excel($zamowienie, $this->filePath['orders']);
            if (!empty($fileName)) {
                return $this->redirect(str_replace($this->basePath, '/', $this->displayPath['orders']) . $fileName);
            } else {
                $this->Flash->error($this->Txt->printAdmin(__('Admin | Błąd generowania pliku')));
                return $this->redirect(['action' => 'index']);
            }
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | Błędny identyfikator zamówienia')));
            return $this->redirect(['action' => 'index']);
        }
    }

    public function getPdf($id, $download = false) {
        $this->autoRender = false;
        $this->loadModel('Szablon');
        $this->loadComponent('Replace');
        $szablon = $this->Szablon->findByNazwa('zamowienie_pdf')->first();

        if (empty($szablon)) {
            return false;
        }
        if (empty($this->Zamowienie)) {
            $this->loadModel('Zamowienie');
        }
        $zamowienie = $this->Zamowienie->find('all', ['conditions' => ['Zamowienie.id' => $id], 'contain' => ['Uzytkownik', 'ZamowienieTowar']])->first();
        Configure::write('currencyDefault', $zamowienie->waluta_symbol);
        if (empty($zamowienie)) {
            return false;
        }
        $tresc = $szablon->tresc;
        $replace = [
            'zamowienie-id' => $zamowienie->id,
            'zamowienie-numer' => (!empty($zamowienie->numer) ? $zamowienie->numer : $zamowienie->id),
            'zamowienie-imie' => $zamowienie->imie,
            'zamowienie-nazwisko' => $zamowienie->nazwisko,
            'zamowienie-adres-wysylki' => $this->Txt->printUserInfo($zamowienie->toArray(), '<br/>', 'wysylka_') . '<br/>' . $this->Txt->printAddres($zamowienie->toArray(), '<br/>', 'wysylka_'),
            'zamowienie-adres-faktury' => $this->Txt->printUserInfo($zamowienie->toArray(), '<br/>', 'faktura_') . '<br/>' . $this->Txt->printAddres($zamowienie->toArray(), '<br/>', 'faktura_'),
            'zamowienie-dane-klienta' => $this->Txt->printUserInfo($zamowienie->uzytkownik->toArray(), '<br/>') . '<br/>' . $this->Txt->printAddres($zamowienie->uzytkownik->toArray(), '<br/>'),
            'zamowienie-email' => $zamowienie->email,
            'zamowienie-firma' => $zamowienie->firma,
            'zamowienie-nip' => $zamowienie->nip,
            'zamowienie-telefon' => $zamowienie->telefon,
            'zamowienie-wartosc-produktow' => $this->Txt->cena($zamowienie->wartosc_produktow),
            'zamowienie-wartosc-razem' => $this->Txt->cena($zamowienie->wartosc_razem),
            'zamowienie-waluta' => $zamowienie->waluta_symbol,
            'zamowienie-uwagi' => nl2br($zamowienie->uwagi),
            'zamowienie-status' => $this->Translation->get('statusy_zamowien.' . $zamowienie->status),
            'zamowienie-data' => (!empty($zamowienie->data) ? $this->Txt->printDate($zamowienie->data, 'Y-m-d H:i') : ''),
            'termin-wysylki' => (!empty($zamowienie->termin_wysylki) ? $this->Txt->printDate($zamowienie->termin_wysylki, 'Y-m-d') : ''),
            'zamowienie-token' => $zamowienie->token,
            'zamowienie-rabat' => $zamowienie->rabat_wartosc,
            'zamowienie-opiekun' => $zamowienie->administrator_dane,
            'adres-ip' => $this->request->clientIp()
        ];
        $html = $this->Replace->getTemplate($tresc, $replace, $zamowienie->zamowienie_towar);
        $name = 'order_' . $zamowienie->id . '.pdf';
        $orientacja = 'A4-P';
        $mpdf = new \mPDF('utf-8', $orientacja);
        $mpdf->WriteHTML($html);
        if ($download) {
            $mpdf->Output($name, 'D');
        } else {
            header("Content-type:application/pdf");
            echo $mpdf->Output($name, 'S');
        }
//        return $dst . $name;
    }

    public function wyslijKarte($kartaId, $download = false, $forceSend = false) {
        if (empty($this->KartaPodarunkowaKod)) {
            $this->loadModel('KartaPodarunkowaKod');
        }
        $conditions = ['KartaPodarunkowaKod.id' => $kartaId, 'KartaPodarunkowaKod.aktywny' => 1, 'KartaPodarunkowaKod.wyslany' => 0];
        if ($forceSend) {
            unset($conditions['KartaPodarunkowaKod.wyslany']);
        }
        $karta = $this->KartaPodarunkowaKod->find('all', ['contain' => ['KartaPodarunkowa']])->where($conditions)->first();
        $zamowienie = $this->Zamowienie->find('all', ['contain' => ['PunktyOdbioru', 'Platnosc' => ['RodzajPlatnosci'], 'RodzajPlatnosci', 'Wysylka', 'ZamowienieTowar' => ['sort' => ['ZamowienieTowar.id' => 'asc']], 'KodyRabatowe']])->where(['Zamowienie.id' => $karta->zamowienie_id])->first();
        $kartyIds = [];
        if (!empty($karta)) {
            $pages = [];
            $kartyIds[$karta->id] = $karta->id;
            $pages[] = $this->prepareKartaPodarunkowaPdf($karta);
            $style = '<style type="text/css">'
                    . '</style>';
            $name = 'karta_podarunkowa_' . (!empty($karta->kod) ? str_replace(["/"], ['_'], $karta->kod) : $karta->id) . '.pdf';
            $orientacja = 'A6-L';
            $default_font_size = 0;
            $default_font = '';
            $mgf = 4;
            $orientation = 'L';
            $mpdf = new \mPDF('utf-8', $orientacja, $default_font_size, $default_font, 0, 0, 5, 0, 0, $mgf, $orientation);
            $mpdf->WriteHTML($style);
            $i = 1;
            foreach ($pages as $page) {
                if (empty($page)) {
                    continue;
                }
                if ($i > 1) {
                    $mpdf->AddPage();
                }
                $mpdf->WriteHTML($page);
                $i++;
            }
            if (empty($this->Mail)) {
                $this->loadComponent('Mail');
            }
            if (empty($this->Replace)) {
                $this->loadComponent('Replace');
            }

            if ($download) {
                $mpdf->Output($name, 'D');
            } else {
                $dstPath = ROOT . DS . 'tmp' . DS . 'files_karty' . DS;
                $mpdf->Output($dstPath . $name, 'F');
                $mailCfg = ['attachment' => [$name => $dstPath . $name]];
                $szablon = $this->Szablon->findByNazwa('karta_podarunkowa_mail')->first();
                $replace = $this->Replace->getReplaceOrderData($zamowienie);
                $message = $this->Replace->getTemplate($szablon->tresc, $replace, $zamowienie->zamowienie_towar);
                $temat = $this->Replace->getTemplate($szablon->temat, $replace);
                if ($this->Mail->sendMail($zamowienie->email, $temat, $message, $mailCfg)) {
                    $this->Flash->success($this->Txt->printAdmin(__('Admin | Karty zostały wysłane')));
                    if (!empty($kartyIds)) {
                        $this->KartaPodarunkowaKod->updateAll(['wyslany' => 1, 'data_wyslania' => date('Y-m-d H:i:s')], ['id IN' => $kartyIds]);
                    }
                } else {
                    $this->Flash->error($this->Txt->printAdmin(__('Admin | Błąd wysyłki kart')));
                }
//                header("Content-type:application/pdf");
//                echo $mpdf->Output($name, 'S');
//                die();
            }
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | Nie znaleziono karty do wysłania, sprawdĹş czy kod jest aktywny lub czy nie była już wysłana')));
        }
        return $this->redirect($this->request->referer());
    }

    public function wyslijKarty($zamowienieId, $download = false, $forceSend = false) {
        if (empty($this->KartaPodarunkowaKod)) {
            $this->loadModel('KartaPodarunkowaKod');
        }
        $zamowienie = $this->Zamowienie->find('all', ['contain' => ['PunktyOdbioru', 'Platnosc' => ['RodzajPlatnosci'], 'RodzajPlatnosci', 'Wysylka', 'ZamowienieTowar' => ['sort' => ['ZamowienieTowar.id' => 'asc']], 'KodyRabatowe']])->where(['Zamowienie.id' => $zamowienieId])->first();
        $kartyIds = [];
        $conditions = ['KartaPodarunkowaKod.zamowienie_id' => $zamowienieId, 'KartaPodarunkowaKod.aktywny' => 1, 'KartaPodarunkowaKod.wyslany' => 0];
        if ($forceSend) {
            unset($conditions['KartaPodarunkowaKod.wyslany']);
        }
        $karty = $this->KartaPodarunkowaKod->find('all', ['contain' => ['KartaPodarunkowa']])->where($conditions);
        if (!empty($karty) && $karty->count() > 0) {
            $attachments = [];
            $pages = [];

            $style = '<style type="text/css">'
                    . '</style>';
            $name = 'karty_podarunkowe_' . (!empty($zamowienie->numer) ? str_replace(["/"], ['_'], $zamowienie->numer) : $zamowienie->id) . '.pdf';
            $orientacja = 'A6-L';
            $default_font_size = 0;
            $default_font = '';
            $mgf = 4;
            $orientation = 'L';

            foreach ($karty as $karta) {
                $kartyIds[$karta->id] = $karta->id;
                $html = $this->prepareKartaPodarunkowaPdf($karta);
                $mpdf = new \mPDF('utf-8', $orientacja, $default_font_size, $default_font, 0, 0, 5, 0, 0, $mgf, $orientation);
                $mpdf->WriteHTML($style);
                $mpdf->WriteHTML($html);
                $name = 'karta_podarunkowa_' . (!empty($karta->kod) ? str_replace(["/"], ['_'], $karta->kod) : $karta->id) . '.pdf';
                $dstPath = ROOT . DS . 'tmp' . DS . 'files_karty' . DS;
                $mpdf->Output($dstPath . $name, 'F');
                $attachments[$name] = $dstPath . $name;
            }
            if (empty($this->Mail)) {
                $this->loadComponent('Mail');
            }
            if (empty($this->Replace)) {
                $this->loadComponent('Replace');
            }
            $mailCfg = ['attachment' => $attachments];
            $szablon = $this->Szablon->findByNazwa('karta_podarunkowa_mail')->first();
            $replace = $this->Replace->getReplaceOrderData($zamowienie);
            $message = $this->Replace->getTemplate($szablon->tresc, $replace, $zamowienie->zamowienie_towar);
            $temat = $this->Replace->getTemplate($szablon->temat, $replace);
            if ($this->Mail->sendMail($zamowienie->email, $temat, $message, $mailCfg)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | Karty zostały wysłane')));
                if (!empty($kartyIds)) {
                    $this->KartaPodarunkowaKod->updateAll(['wyslany' => 1, 'data_wyslania' => date('Y-m-d H:i:s')], ['id IN' => $kartyIds]);
                }
            } else {
                $this->Flash->error($this->Txt->printAdmin(__('Admin | Błąd wysyłki kart')));
            }
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | Nie znaleziono kart do wysłania, sprawdĹş czy kody są aktywne lub czy nie były już wysłane')));
        }

        return $this->redirect(['action' => 'view', $zamowienieId]);
    }

    private function prepareKartaPodarunkowaPdf($karta, $template = null) {
        if (empty($this->Replace)) {
            $this->loadComponent('Replace');
        }
        if (empty($template)) {
            if (empty($this->Szablon)) {
                $this->loadModel('Szablon');
            }
            $template = $this->Szablon->findByNazwa('karta_podarunkowa_pdf')->first();
        }
        $replaceArr = $this->getReplaceField($karta->toArray());
        return $this->Replace->getTemplate($template->tresc, $replaceArr, null, true);
    }

    private function getReplaceField($dane, $parentField = '', $returnArr = []) {
        foreach ($dane as $field => $value) {
            if (is_array($value)) {
                $returnArr = $this->getReplaceField($value, (!empty($parentField) ? $parentField . '-' : '') . $field, $returnArr);
            } elseif (is_object($value)) {
                $returnArr['[%' . (!empty($parentField) ? $parentField . '-' : '') . $field . '%]'] = (!empty($value) ? $value->format('Y-m-d') : '');
            } else {
                $returnArr['[%' . (!empty($parentField) ? $parentField . '-' : '') . $field . '%]'] = (!empty($value) ? $value : '');
            }
        }
        return $returnArr;
    }

    public function kurierXml($name, $type = 'poczta') {
        $this->autoRender = false;
        $encoding = 'UTF-8';
        $data = $this->request->data;
        switch ($type) {
            case 'poczta': {
                    $increment = \Cake\Cache\Cache::read('xml', 'poczta');
                    if (empty($increment))
                        $increment = array(date('Y-m-d') => '1');
                    else if (!empty($increment[date('Y-m-d')]))
                        $increment[date('Y-m-d')] = $increment[date('Y-m-d')] + 1;
                    else
                        $increment[date('Y-m-d')] = 1;
                    \Cake\Cache\Cache::write('xml', $increment, 'poczta');
                    $data['Nadawca']['Zbior']['attr']['Nazwa'] = date('Y-m-d') . '/' . $increment[date('Y-m-d')];
                } break;
            case 'dhl' : {
                    $_tmp = ['LIST' => $data];
                    $data = $_tmp;
                } break;
            case 'ups' : {
                    if (empty($data['OpenShipments']['OpenShipment']['ShipmentInformation']['AdditionalHandling']['NumberOfAdditionalHandling']) || $data['OpenShipments']['OpenShipment']['ShipmentInformation']['AdditionalHandling']['NumberOfAdditionalHandling'] == 0) {
                        unset($data['OpenShipments']['OpenShipment']['ShipmentInformation']['AdditionalHandling']['NumberOfAdditionalHandling']);
                    }
                    if (empty($data['OpenShipments']['OpenShipment']['ShipmentInformation']['AdditionalHandling'])) {
                        unset($data['OpenShipments']['OpenShipment']['ShipmentInformation']['AdditionalHandling']);
                    }
                    foreach ($data['OpenShipments'] as $_key => &$_data) {
                        if ($_key == 'attr')
                            continue;
                        if (is_array($_data)) {
                            foreach ($_data as $_itemKey => &$_item) {
                                if ($_itemKey == 'attr')
                                    continue;
                                if (is_array($_item)) {
                                    foreach ($_item as $_subItemKey => &$_subItem) {
                                        if ($_subItemKey == 'attr')
                                            continue;
                                        if (empty($_subItem))
                                            unset($_item[$_subItemKey]);
                                    }
                                } else
                                if (empty($_item))
                                    unset($_data[$_itemKey]);
                            }
                        } else if (empty($_data))
                            unset($data['OpenShipments'][$_key]);
                    }
                    $encoding = 'windows-1250';
//                    $convartData = $this->convert($data, '', 'windows-1250//TRANSLIT');
//                    $data = $convartData;
                } break;
        }

        $key = array_keys($data);
        $key = $key[0];
        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="' . $encoding . '"?><' . $key . '/>');
        $this->array_to_xml($data[$key], $xml);
        header('Content-type: text/xml');
        header("Content-disposition:attachment;filename=" . $name);
        echo $xml->asXML();
    }

    public function kurierCsv($id, $type = '', $allegro = null) {
        $this->viewBuilder()->autoLayout(false);
        $this->autoRender = false;
        if ($this->request->is('post')) {
            if (!empty($this->request->data['csv'])) {
                $filename = $type . '_' . (!empty($allegro) ? 'allegro_' : '') . $id . '_' . date("Y-m-d");

                $delimiter = '^';
                $enclausure = '"';
                if (strpos($type, 'geis') !== false) {
                    $delimiter = \Cake\Core\Configure::read('geis.delimiter');
                    $enclausure = \Cake\Core\Configure::read('geis.enclausure');
                }
                if (strpos($type, 'inpost') !== false) {
                    $delimiter = ';';
                    $enclausure = '"';
                }
                header("Content-type: text/csv");
                header("Content-Disposition: attachment; filename={$filename}.csv");
                header("Pragma: no-cache");
                header("Expires: 0");
                $this->outputCSV($this->request->data, $delimiter, $enclausure);
            }
        }
    }

    private function outputCSV($data, $delimiter = '^', $enclausure = '"') {
        $outputBuffer = fopen("php://output", 'w');
        if (!empty($data['labels'])) {
            fputcsv($outputBuffer, $data['labels'], $delimiter, $enclausure);
        }
        if (!empty($data['csv'])) {
            fputcsv($outputBuffer, $data['csv'], $delimiter, $enclausure);
        }
        if (!empty($data['csv_multi'])) {
            foreach ($data['csv_multi'] as $line) {
                fputcsv($outputBuffer, $line, $delimiter, $enclausure);
            }
        }
//        foreach ($data as $val) {
//            fputcsv($outputBuffer, $val, $delimiter, $enclausure);
//        }
        fclose($outputBuffer);
    }

    public function kurierXls($id, $type = 'siodemka', $allegro = null) {
        $this->viewBuilder()->autoLayout(false);
        $filename = $type . '_' . (!empty($allegro) ? 'allegro_' : '') . $id . '_' . date("Y-m-d") . '.xls';
        $this->set('filename', $filename);
        $this->set('data', $this->request->data);
        $this->set('type', $type);
        $data = $this->request->data;
    }

    private function array_to_xml($array, &$xml, $parent = null, $tag = null) {
        $count = count($array) - 1;
        if (!empty($array['attr'])) {
            $this->add_attr_to_xml($array['attr'], $xml);
            unset($array['attr']);
        }
        foreach ($array as $key => $value) {
            if (is_array($value) && !isset($value['val'])) {
                if (is_integer($key)) {
                    $this->array_to_xml($value, $xml);
                } else {
                    $subnode = $xml->addChild($key);
                    $this->array_to_xml($value, $subnode, $xml, $key);
                }
            } else {
                if (!empty($value) && is_array($value) && is_array($value['val'])) {
                    $value['val'] = implode('', $value['val']);
                }
                if (!isset($value['val']) && is_array($value))
                    $value = implode('', $value);
                $child = $xml->addChild($key, is_array($value) ? $value['val'] : $value);
                if (is_array($value) && !empty($value['attr'])) {
                    $this->add_attr_to_xml($value['attr'], $child);
                }
            }
        }
    }

    private function add_attr_to_xml($attr, &$node) {
        if (!empty($attr)) {
            foreach ($attr as $key => $value) {
                $node->addAttribute($key, $value);
            }
        }
    }

    private function convert($array, $z = '', $do = 'UTF-8') {
        $temp = array();
        foreach ($array as $k => $v)
            if (is_array($v))
                $temp[$k] = $this->convert($v, $z, $do);
            else
                $temp[$k] = iconv($z, $do, $v);
        return $temp;
    }

    private function nextWorkDay($date = null) {
        if (empty($date)) {
            $date = date('Y-m-d');
        }
        $testDate = date('N', strtotime($date . ' + 1 day'));
        if ($testDate == 6) {
            return date('Y-m-d', strtotime($date . ' + 3 days'));
        }
        if ($testDate == 7) {
            return date('Y-m-d', strtotime($date . ' + 2 days'));
        }
        return date('Y-m-d', strtotime($date . ' + 1 day'));
    }

    public function dhlEdit($shipmentId) {
        $this->loadModel('ZamowienieDhl');
        $ship = $this->ZamowienieDhl->find('all', ['conditions' => ['ZamowienieDhl.shipmentId' => $shipmentId]])->first();
        if (!empty($ship) && !empty($ship->requestData) && empty($ship->orderId)) {
            $requestOldData = json_decode($ship->requestData, true);
            $rqData = $this->request->data;
            foreach ($rqData as $field => $data) {
                $requestOldData[$field] = $data;
            }
            $dhlConfig = \Cake\Core\Configure::read('dhl');
            $dhl = new \dhl($dhlConfig['username'], $dhlConfig['password'], (bool) $dhlConfig['test']);
            $dhlShipper = [];
            foreach ($dhlConfig as $dhlField => $dhlValue) {
                if (strpos($dhlField, 'shipper') !== false && !empty($dhlValue)) {
                    $filedName = lcfirst(str_replace('shipper', '', $dhlField));
                    $dhlShipper[$filedName] = $dhlValue;
                }
            }
            $requestOldData['payment']['accountNumber'] = $dhlConfig['accountNumber'];
            $requestOldData['shipper'] = $dhlShipper;
            if (empty($requestOldData['shipmentDate'])) {
                $requestOldData['shipmentDate'] = $this->nextWorkDay();
            }
            $requestOldData['skipRestrictionCheck'] = true;
            $requestOldData['shipmentId'] = $ship->shipmentId;
            $type = $requestOldData['pieceList'][0]['type'];
            if ($type == 'PALLET') {
                $type = 'DR';
            } else {
                $type = 'EX';
            }
            $toSaveData = [
                'id' => $ship->id,
                'zamowienie_id' => $ship->zamowienie_id,
                'allegro_zamowienie_id' => $ship->allegro_zamowienie_id,
                'opis' => $requestOldData['content'],
                'shipmentDate' => $requestOldData['shipmentDate'],
                'data_dodania' => date('Y-m-d H:i:s'),
                'type' => $type,
                'requestData' => json_encode($requestOldData),
                'labelType' => null,
                'labelName' => null,
                'labelData' => null,
                'labelMimeType' => null
            ];
            $newShipment = $dhl->newShipment([$requestOldData]);
            $addResult = false;
            if (!is_string($newShipment)) {
                if (property_exists($newShipment, 'createShipmentsResult')) {
                    if (property_exists($newShipment->createShipmentsResult, 'item')) {
                        if (is_array($newShipment->createShipmentsResult->item)) {
                            foreach ($newShipment->createShipmentsResult->item as $item) {
                                $addResult = $this->dhlResultItemAction($item, $toSaveData);
                            }
                        } else {
                            $addResult = $this->dhlResultItemAction($newShipment->createShipmentsResult->item, $toSaveData);
                        }
                    }
                }
                if (!$addResult) {
                    $this->Flash->error(__('Nie udało się wygenerować paczki'), ['escape' => false]);
                } else {
                    $this->Flash->success(__('Paczka została zapisana, możesz pobrać etykietę'), ['escape' => false]);
                    $deleteResult = $dhl->deleteShipment(['shipments' => [$shipmentId]]);
                }
            } else {
                $errors = explode(',', $newShipment);
                $this->Flash->error(join('<br/>', $errors), ['escape' => false]);
            }
        } else {
            $this->Flash->error(__('Nie można edytować przesyłki'), ['escape' => false]);
        }
        return $this->redirect($this->request->referer());
    }

    public function dhlAdd($zamowienieId = null, $allegro = false) {
        $rqData = $this->request->data;
        if (!empty($rqData)) {
            $dhlConfig = \Cake\Core\Configure::read('dhl');
            if (empty($dhlConfig)) {
                $this->Flash->error($this->Txt->printAdmin(__('Admin | Brak danych konfiguracyjnych')));
                return $this->redirect($this->request->referer());
            }
            $dhl = new \dhl($dhlConfig['username'], $dhlConfig['password'], (bool) $dhlConfig['test']);
            $dhlShipper = [];
            foreach ($dhlConfig as $dhlField => $dhlValue) {
                if (strpos($dhlField, 'shipper') !== false && !empty($dhlValue)) {
                    $filedName = lcfirst(str_replace('shipper', '', $dhlField));
                    $dhlShipper[$filedName] = $dhlValue;
                }
            }
            $rqData['payment']['accountNumber'] = $dhlConfig['accountNumber'];
            $rqData['shipper'] = $dhlShipper;
            if (empty($rqData['shipmentDate'])) {
                $rqData['shipmentDate'] = $this->nextWorkDay();
            }
            $rqData['skipRestrictionCheck'] = true;
            $paczka = [
                [
                    'shipmentDate' => date('Y-m-d'),
                    'service' => [
                        'product' => 'AH'
                    ],
//                'dropOffType' => 'REGULAR_PICKUP',
//                'labelType' => 'LP',
                    'content' => 'Jakaś zawartość',
                    'skipRestrictionCheck' => true,
                    'payment' => [
                        'payerType' => 'SHIPPER', //SHIPPER - nadawca,RECEIVER - odbiorca,USER - strona trzecia
                        'accountNumber' => $dhlConfig['accountNumber'], //numer klienta
                        'paymentMethod' => 'BANK_TRANSFER', //CASH - płatność gotówką, BANK_TRANSFER - przelew (tylko dla klientów z umową i numerem SAP)
                    ],
//                'shipmentTime' => [
//                    'shipmentDate' => date('Y-m-d'),
//                    'shipmentStartHour' => '08:00',
//                    'shipmentEndHour' => '14:00',
//                ],
                    'receiver' => [
                        'country' => 'PL',
                        'name' => 'Jan Kowalski',
                        'postalCode' => '75423',
                        'city' => 'Koszalin',
                        'street' => 'Młyńska',
                        'houseNumber' => '52',
                        'apartmentNumber' => '3',
                    ],
                    'shipper' => $dhlShipper,
                    'pieceList' => [
                        [
                            'type' => 'PACKAGE',
                            'weight' => 5,
                            'width' => 30,
                            'height' => 30,
                            'length' => 30,
                            'quantity' => 1,
                        ]
                    ]
                ]
            ];
            $type = $rqData['pieceList'][0]['type'];
            if ($type == 'PALLET') {
                $type = 'DR';
            } else {
                $type = 'EX';
            }
            $toSaveData = [
                (($allegro == 'a') ? 'allegro_' : '') . 'zamowienie_id' => $zamowienieId,
                'opis' => $rqData['content'],
                'shipmentDate' => $rqData['shipmentDate'],
                'data_dodania' => date('Y-m-d H:i:s'),
                'type' => $type,
                'requestData' => json_encode($rqData)
            ];
            $newShipment = $dhl->newShipment([$rqData]);
            $addResult = false;
            if (!is_string($newShipment)) {
                if (property_exists($newShipment, 'createShipmentsResult')) {
                    if (property_exists($newShipment->createShipmentsResult, 'item')) {
                        if (is_array($newShipment->createShipmentsResult->item)) {
                            foreach ($newShipment->createShipmentsResult->item as $item) {
                                $addResult = $this->dhlResultItemAction($item, $toSaveData);
                            }
                        } else {
                            $addResult = $this->dhlResultItemAction($newShipment->createShipmentsResult->item, $toSaveData);
                        }
                    }
                }
                if (!$addResult) {
                    $this->Flash->error(__('Nie udało się wygenerować paczki'), ['escape' => false]);
                } else {
                    $this->Flash->success(__('Paczka została dodana, możesz pobrać etykietę'), ['escape' => false]);
                }
            } else {
                $errors = explode(',', $newShipment);
                $this->Flash->error(join('<br/>', $errors), ['escape' => false]);
            }
        } else {
            $this->Flash->error(__('Nieprawidłowe wywołanie'));
        }
        $referer = $this->request->referer();
        if (strpos($referer, '?') !== false) {
            $referer .= '&k_tab=dhlWebApi';
        } else {
            $referer .= '?k_tab=dhlWebApi';
        }
        return $this->redirect($referer);
    }

    public function dhlGetLabel($shipmentId) {
        if (empty($this->ZamowienieDhl)) {
            $this->loadModel('ZamowienieDhl');
        }
        $shipment = $this->ZamowienieDhl->find('all', ['conditions' => ['ZamowienieDhl.shipmentId' => $shipmentId]])->first();
        if (empty($shipment->labelData)) {
            $dhlConfig = \Cake\Core\Configure::read('dhl');
            $dhl = new \dhl($dhlConfig['username'], $dhlConfig['password'], (bool) $dhlConfig['test']);
            $label = $dhl->getLabel([['labelType' => (!empty($dhlConfig['labelType']) ? $dhlConfig['labelType'] : 'ZBLP'), 'shipmentId' => $shipment->shipmentId]]);
            if (property_exists($label, 'getLabelsResult') && property_exists($label->getLabelsResult, 'item')) {
                if (!is_array($label->getLabelsResult->item)) {
                    $patchData = [
                        'labelType' => $label->getLabelsResult->item->labelType,
                        'labelName' => $label->getLabelsResult->item->labelName,
                        'labelData' => $label->getLabelsResult->item->labelData,
                        'labelMimeType' => $label->getLabelsResult->item->labelMimeType
                    ];
                    $shipment = $this->ZamowienieDhl->patchEntity($shipment, $patchData);
                    $this->ZamowienieDhl->save($shipment);
                }
            } else {
                $this->Flash->error($label);
                return $this->redirect($this->request->referer());
            }
        }
        $ext = mb_strtolower(pathinfo($shipment->labelName, PATHINFO_EXTENSION));

        if ($ext == 'pdf') {
//        header('Content-Disposition: attachment; filename=' . $shipment->labelName); 
            header('Content-type: ' . $shipment->labelMimeType);
            echo base64_decode($shipment->labelData);
        } else {
            header('Content-type: text/html');
            echo '<!doctype html>
<html lang="en">
<head>
  <title>Print ZPL from browser</title>
</head>
<body>
<textarea id="printData" style="display:none;">' . base64_decode($shipment->labelData) . '</textarea>
<script type="text/javascript">
    function printZpl() {
      var printWindow = window.open();
      printWindow.document.open(\'' . $shipment->labelMimeType . '\')
      printWindow.document.write(document.getElementById(\'printData\').value);
      printWindow.document.close();
      printWindow.focus();
      printWindow.print();
      printWindow.close();
    }
    printZpl();
  </script></body></html>';
        }
        die();
    }

    private function dhlResultItemAction($item, $toSaveData = []) {
        if (empty($this->ZamowienieDhl)) {
            $this->loadModel('ZamowienieDhl');
        }
        $toSaveData['shipmentId'] = $item->shipmentId;
        if (!empty($toSaveData['id'])) {
            $newItem = $this->ZamowienieDhl->find('all', ['conditions' => ['ZamowienieDhl.id' => $toSaveData['id']]])->first();
            if (!empty($newItem)) {
                $newItem = $this->ZamowienieDhl->patchEntity($newItem, $toSaveData);
            } else {
                $newItem = $this->ZamowienieDhl->newEntity($toSaveData);
            }
        } else {
            $newItem = $this->ZamowienieDhl->newEntity($toSaveData);
        }
        if (!$this->ZamowienieDhl->save($newItem)) {
            return false;
        } else {
            return $newItem->id;
        }
    }

    public function dhl() {
        if (empty($this->ZamowienieDhl)) {
            $this->loadModel('ZamowienieDhl');
        }
        if ($this->request->is('post')) {
            if (isset($this->request->data['wyczysc_filtr'])) {
                $this->session->delete('Dhl.filter');
                $this->session->delete('Dhl.filterRequest');
            } else {
                $filtr = $this->request->data;

                $this->session->write('Dhl.filterRequest', $this->request->data);
                if (!empty($filtr['status'])) {
                    if ($filtr['status'] == 'nowe') {
                        $filter['ZamowienieDhl.orderId IS'] = NULL;
                    } else {
                        $filter['ZamowienieDhl.orderId IS NOT'] = NULL;
                    }
                }
                if (!empty($filtr['data'])) {
                    $filter['ZamowienieDhl.shipmentDate'] = $filtr['data'];
                }
                if (!empty($filtr['id'])) {
                    $filter['ZamowienieDhl.shipmentId'] = $filtr['id'];
                }
                if (!empty($filtr['order_id'])) {
                    $filter['ZamowienieDhl.orderId'] = $filtr['order_id'];
                }
                if (!empty($filtr['zamowienie_id'])) {
                    $filter['Zamowienie.numer'] = $filtr['zamowienie_id'];
                }
                if (empty($filter))
                    $filter = [];
                $this->session->write('Dhl.filter', $filter);
            }
        }
        $filter = $this->session->read('Dhl.filter');
        if (empty($filter)) {
            $filter = [];
            if (!$this->request->is('post')) {
                $filter['ZamowienieDhl.orderId IS'] = NULL;
                if (time() < strtotime(date('Y-m-d 13:00:00'))) {
                    $filter['ZamowienieDhl.shipmentDate'] = date('Y-m-d');
                } else {
                    $filter['ZamowienieDhl.shipmentDate'] = $this->nextWorkDay();
                }
                $this->session->write('Dhl.filterRequest', ['status' => 'nowe', 'data' => $filter['ZamowienieDhl.shipmentDate']]);
            }
        }
        $this->paginate = [
            'limit' => 50,
            'conditions' => $filter,
            'contain' => ['Zamowienie']
        ];
        $zlecenia = $this->paginate($this->ZamowienieDhl);
        $this->set('zlecenia', $zlecenia);
        $this->set('filter', $this->session->read('Dhl.filterRequest'));
        $this->set('allStatus', ['nowe' => 'Nie zlecone', 'zlecone' => 'Zlecone']);

        $dhlConfig = \Cake\Core\Configure::read('dhl');
        $dhl = new \dhl($dhlConfig['username'], $dhlConfig['password'], (bool) $dhlConfig['test']);
        $postCodeDate = (!empty($filter['ZamowienieDhl.shipmentDate']) ? $filter['ZamowienieDhl.shipmentDate'] : date('Y-m-d'));
        $dhlServices = $dhl->getPostCodes($dhlConfig['shipperPostalCode'], $postCodeDate);
        $this->set('dhlServices', $dhlServices);
        $this->set('dhlServicesPostCode', $dhlConfig['shipperPostalCode']);
        $this->set('dhlServicesDate', $postCodeDate);
        $this->set('courierTypes', ['EX' => 'Paczki, Listy (EX)', 'DR' => 'Palety (DR)']);
    }

    public function dhlBook() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $dhlConfig = \Cake\Core\Configure::read('dhl');
            $dhl = new \dhl($dhlConfig['username'], $dhlConfig['password'], (bool) $dhlConfig['test']);
            $rqData = $this->request->data;
            if (empty($this->ZamowienieDhl)) {
                $this->loadModel('ZamowienieDhl');
            }
            $uwagi = (key_exists('additionalInfo', $rqData) ? $rqData['additionalInfo'] : '');
            $paczki = $this->ZamowienieDhl->find('list', ['groupField' => 'type', 'keyField' => 'id', 'valueField' => 'shipmentId', 'conditions' => ['shipmentId IN' => $rqData['shipmentIdList']]])->toArray();
            if (!empty($paczki)) {
                $errors = [];
                foreach ($paczki as $type => $listIds) {
                    $rqData['shipmentIdList'] = array_values($listIds);
                    $orderId = $dhl->book($rqData);
                    if (is_object($orderId)) {
                        if (property_exists($orderId, 'bookCourierResult')) {
                            $this->ZamowienieDhl->updateAll(['orderId' => $orderId->bookCourierResult->item, 'godzina_od' => $rqData['pickupTimeFrom'], 'godzina_do' => $rqData['pickupTimeTo'], 'uwagi' => $uwagi], ['shipmentId IN' => $rqData['shipmentIdList']]);
                        } else {
                            $errors[] = __('Nieoczekiwany błąd');
                        }
                    } else {
                        $errors[] = $orderId;
                    }
                }
                if (!empty($errors)) {
                    $returnData = ['status' => 'error', 'message' => join('; ', $errors)];
                } else {
                    $this->Flash->success(__('Kurier został zamówiony'));
                    $returnData = ['status' => 'success'];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => __('Nie znaleziono przesyłek')];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => __('Nieoczekiwany błąd')];
        }
        $this->response->type('ajax');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    public function dhlCancelBook($orderId) {
        $this->autoRender = false;
        $dhlConfig = \Cake\Core\Configure::read('dhl');
        $dhl = new \dhl($dhlConfig['username'], $dhlConfig['password'], (bool) $dhlConfig['test']);
        $cancelResult = $dhl->cancel_book(['orders' => [$orderId]]);
        if (is_object($cancelResult)) {
            if (property_exists($cancelResult, 'cancelCourierBookingResult')) {
                if (empty($this->ZamowienieDhl)) {
                    $this->loadModel('ZamowienieDhl');
                }
                if ($cancelResult->cancelCourierBookingResult->item->result) {
                    $this->ZamowienieDhl->updateAll(['orderId' => null], ['orderId' => $cancelResult->cancelCourierBookingResult->item->id]);
                    $this->Flash->success(__('Kurier został odwołany'));
                } else {
                    $this->Flash->error($cancelResult->cancelCourierBookingResult->item->error);
                }
            } else {
                $this->Flash->error(__('Nieoczekiwany błąd'));
            }
        } else {
            $this->Flash->error($cancelResult);
        }
        return $this->redirect($this->request->referer());
    }

    public function dhlDeleteShipment($shipmentId) {
        $this->autoRender = false;
        $dhlConfig = \Cake\Core\Configure::read('dhl');
        $dhl = new \dhl($dhlConfig['username'], $dhlConfig['password'], (bool) $dhlConfig['test']);
        $deleteResult = $dhl->deleteShipment(['shipments' => [$shipmentId]]);
        if (is_object($deleteResult)) {
            if (property_exists($deleteResult, 'deleteShipmentsResult')) {
                if (empty($this->ZamowienieDhl)) {
                    $this->loadModel('ZamowienieDhl');
                }
                if ($deleteResult->deleteShipmentsResult->item->result) {
                    $this->ZamowienieDhl->deleteAll(['shipmentId' => $deleteResult->deleteShipmentsResult->item->id]);
                    $this->Flash->success(__('Przesyłka została usunięta.'));
                } else {
                    $this->Flash->error($deleteResult->deleteShipmentsResult->item->error);
                }
            } else {
                $this->Flash->error(__('Nieoczekiwany błąd'));
            }
        } else {
            $this->Flash->error($deleteResult);
        }
        return $this->redirect($this->request->referer());
    }

    public function orderListAction() {
        $rqData = $this->request->getData();
        if (!empty($rqData) && (!empty($rqData['ids']) || key_exists('subiekt_epp', $rqData))) {
            if (key_exists('set_status', $rqData)) {
                if (!empty($rqData['status'])) {
                    $allOrders = $this->Zamowienie->find('all', ['contain' => ['ZamowienieTowar', 'Uzytkownik']])->where(['Zamowienie.id IN' => $rqData['ids'], 'Zamowienie.status NOT LIKE' => $rqData['status']])->toArray();
                    $this->Zamowienie->updateAll(['status' => $rqData['status']], ['id IN' => $rqData['ids'], 'status !=' => $rqData['status']]);
                    if (!empty($allOrders)) {
                        foreach ($allOrders as $zamowienie) {
                            $zamowienie->set('status', $rqData['status']);
                            if ($rqData['status'] == 'przyjete' && !empty($rqData['termin_wysylki'])) {
                                $zamowienie->set('termin_wysylki', $rqData['termin_wysylki']);
                                $this->Zamowienie->save($zamowienie);
                            }
                            if (!empty($rqData['send_status_mails']) || $rqData['status'] == 'oplacone') {
                                $this->changeStatus($zamowienie);
                            }
                        }
                    }
                    $this->Flash->success($this->Txt->printAdmin(__('Admin | Statusy zostały zmienione')));
                } else {
                    $this->Flash->error($this->Txt->printAdmin(__('Admin | Wybierz status')));
                }
            } elseif (key_exists('bonus', $rqData)) {

                $allOrders = $this->Zamowienie->find('all', ['contain' => ['ZamowienieTowar', 'Uzytkownik']])->where(['Zamowienie.id IN' => $rqData['ids'], 'Zamowienie.bonus_suma >' => 0, 'OR' => ['Zamowienie.bonus_przyznany IS' => NULL, 'Zamowienie.bonus_przyznany' => 0]]);
                if ($allOrders->count() > 0) {
                    foreach ($allOrders as $zamowienie) {
                        if (!empty($zamowienie->uzytkownik_id)) {
                            $saveBonus = $zamowienie->bonus_suma;
                            if ($saveBonus < 0) {
                                $saveBonus = 0;
                            }
                            $zamowienie->set('bonus_przyznany', ($saveBonus));
                            if ($this->Zamowienie->save($zamowienie)) {
                                $this->Zamowienie->Uzytkownik->updateAll(['bonusy_suma' => ($zamowienie->uzytkownik->bonusy_suma + $saveBonus)], ['id' => $zamowienie->uzytkownik_id]);
                            }
                        }
                    }
                    $this->Flash->success($this->Txt->printAdmin(__('Admin | Bonusy zostały przyznane')));
                }
            } elseif (key_exists('pdf', $rqData)) {
                Configure::write('debug', false);
                $this->autoRender = false;
                $conditions = ['Zamowienie.id IN' => $rqData['ids']];
                $conditionsTowar = [];
                if (!empty($rqData['pdf_status'])) {
                    $conditions['Zamowienie.status'] = $rqData['pdf_status'];
                }
                $allOrders = $this->Zamowienie->find('all', ['contain' => ['ZamowienieTowar' => ['conditions' => $conditionsTowar]]])->where($conditions);
                if ($allOrders->count() > 0) {
                    $view = new \Cake\View\View();
                    $view->layout = 'ajax';
                    $view->viewBuilder()->setLayout('ajax');
                    $view->set('zamowienie', $allOrders);
                    $html = $view->render('Admin/Zamowienie/list_pdf');
                    $mpdf = new mPDF();
                    $mpdf->WriteHTML($html);
                    $name = 'zamowienia_' . date('Y_m_d') . '.pdf';
//            header("Content-type:application/pdf");
                    echo $mpdf->Output($name, 'D');
                    die();
                }
            } elseif (key_exists('geis', $rqData)) {
                $allOrders = $this->Zamowienie->find('all', ['contain' => ['ZamowienieTowar']])->where(['Zamowienie.id IN' => $rqData['ids']]);
                if ($allOrders->count() > 0) {
                    $csvLabels = [
                        'nr_zamowienia Wymagane',
                        'Odbiorca - nazwa Wymagane',
                        'Odbiorca - kraj Wymagane',
                        'Odbiorca - miasto Wymagane',
                        'Odbiorca - adres Wymagane',
                        'Odbiorca - kod pocztowy Wymagane',
                        'Odbiorca - osoba kontaktowa Wymagane',
                        'Odbiorca - email',
                        'Odbiorca - telefon Wymagane',
                        'Data nadania',
                        'Numer referencyjny (nr_zamowienia)',
                        'Ex works (0)',
                        'Pobranie (0/1)',
                        'Wartość pobrania  Wymagane jeżeli pobranie 1',
                        'Variable symbol (id_zamowienia) Wymagane jeżeli pobranie 1',
                        'Waga w kg Wymagane',
                        'Objętość (wymagane dla Cargo)',
                        'ilosc paczek Wymagane',
                        'Opis zawartości max (100 znaków)',
                        'Jednostka obsługi (wymagane dla Cargo)', //EP - 1WAY PALLET, FP - EUR PALLET, GP - GITTERBOX, BU - BUNDLE, CC - COLLI, DO - CAN, EI - BUCKET, FA - BARREL, GS  - STAND, HP  - HALF PALLET, KN  - CANISTER, KT  - CARDBOARD, RO - ROLL, SA - SACK, UV - UNWRAPPED, VP - QUARTER PALLET
                        'Transport type (0/1) Wymagane', //0 - cargo, 1 - parcel
                        'Notatka dla odbiorcy (max 103 znaki)',
                        'Notatka dla kierowcy (max 103 znaki)',
                        'Ubezpieczenie (0/1)',
                        'Wartość produktów do ubezpieczenia',
                        'Potwierdzenie dostarczenia (0/1)',
                        'Email lub telefon do potwierdzenia',
                        'Powiadomienie o uszkodzeniu',
                        'Email lub telefon do powiadomienia',
                        'APZ - powiadomienie o problemach',
                        'APS email lub telefon',
                        'Usługa dla b2c (adres prywatny) (0/1)',
                        'Dostawa do 12 0/1',
                        'Gwarantowana dostawa 0/1',
                        'Service - POD 0/1',
                        'Service - email for POD',
                        'Service - SMS advice of delivery 0/1',
                        'Service - Phone advice of delivery 0/1',
                        'Recipient - evidence number (puste)',
                        'Recipient - street number (puste)',
                        'CrossDock - Name (for CD only) (puste)',
                        'CrossDock - type of CD collection (puste)',
                        'CrossDock - car ID (own delivery) (puste)',
                        'CrossDock - Dispatch date (for CD only) (puste)',
                        'Service - Email to recipient (puste)',
                        'Service - Card pament (puste)',
                        'Shipment to pickup point - PP code (puste)',
                        'PP shipment - without recipient address (puste)',
                        'Service - B2C assisted delivery (B2P) (puste)',
                        'Service - B2C appliance collection (B2S) (puste)',
                        'Frigo shipment 0/1',
                        'Frigo mode',
                        'Delivery notes back (VDL) 0/1',
                        'Contracts back (VSM) 0/1',
                        'Premium delivery (VIP) 0/1',
                        'Premium delivery (VIP) telephone number',
                        'Length',
                        'Width',
                        'Height',
                        'Fixtermin (FIX) 0/1',
                        'Fixtermin (FIX) - date',
                        'IBAN account number',
                        'COD express 0/1',
                        'Individual bank account for COD 0/1',
                        'Number of pallet positions',
                        'Non standard packaging 0/1',
                        'Service - SMS aviso of shipment collection (SAS) 0/1',
                        'Service - phone aviso of shipment collection (SAT) 0/1',
                        'Phone number for service SAS / SAT',
                        'ROD service 0/1',
                        'ROD service - return address code',
                        'ROD service - document 1 type',
                        'ROD service - document 1 description',
                        'ROD service - document 2 type',
                        'ROD service - document 2 description',
                        'ROD service - document 3 type',
                        'ROD service - document 3 description',
                        'ROD service - document 4 type',
                        'ROD service - document 4 description',
                        'ROD service - document 5 type',
                        'ROD service - document 5 description',
                        'ROD service - document 6 type',
                        'ROD service - document 6 description',
                    ];
                    $csv = [];
                    foreach ($allOrders as $zamowienie) {
                        $csv[] = [
                        iconv('UTF-8', 'Windows-1250//TRANSLIT',$zamowienie->numer),
                        iconv('UTF-8', 'Windows-1250//TRANSLIT',(!empty($zamowienie->wysylka_firma)?$zamowienie->wysylka_firma:$zamowienie->wysylka_imie . ' ' . $zamowienie->wysylka_nazwisko)),
                        iconv('UTF-8', 'Windows-1250//TRANSLIT',$zamowienie->wysylka_kraj),
                        iconv('UTF-8', 'Windows-1250//TRANSLIT',$zamowienie->wysylka_miasto),
                        iconv('UTF-8', 'Windows-1250//TRANSLIT',$zamowienie->wysylka_ulica.(!empty($zamowienie->wysylka_nr_domu)?' '.$zamowienie->wysylka_nr_domu.(!empty($zamowienie->wysylka_nr_lokalu)?'/'.$zamowienie->wysylka_nr_lokalu:''):'')),
                        iconv('UTF-8', 'Windows-1250//TRANSLIT',$zamowienie->wysylka_kod),
                        iconv('UTF-8', 'Windows-1250//TRANSLIT',$zamowienie->wysylka_imie . ' ' . $zamowienie->wysylka_nazwisko),
                        iconv('UTF-8', 'Windows-1250//TRANSLIT',(!empty($zamowienie->wysylka_email)?$zamowienie->wysylka_email:$zamowienie->email)),
                        iconv('UTF-8', 'Windows-1250//TRANSLIT',(!empty($zamowienie->wysylka_telefon)?$zamowienie->wysylka_telefon:$zamowienie->telefon)),
                        '',
                        iconv('UTF-8', 'Windows-1250//TRANSLIT',$zamowienie->numer),
                        '0',
                        (($zamowienie->rodzaj_platnosci_id == 1)?'1':'0'),
                        (($zamowienie->rodzaj_platnosci_id == 1)?$zamowienie->wartosc_razem:''),
                        (($zamowienie->rodzaj_platnosci_id == 1)?$zamowienie->id:''),
                        (($zamowienie->waga == 1)?$zamowienie->waga:'2'),
                        '',//Objętość (wymagane dla Cargo)',
                        1,//'ilosc paczek Wymagane',
                        '',//'Opis zawartości max (100 znaków)',
                        '',//'Jednostka obsługi (wymagane dla Cargo)', //EP - 1WAY PALLET, FP - EUR PALLET, GP - GITTERBOX, BU - BUNDLE, CC - COLLI, DO - CAN, EI - BUCKET, FA - BARREL, GS  - STAND, HP  - HALF PALLET, KN  - CANISTER, KT  - CARDBOARD, RO - ROLL, SA - SACK, UV - UNWRAPPED, VP - QUARTER PALLET
                        '1',//'Transport type (0/1) Wymagane', //0 - cargo, 1 - parcel
                        '',//'Notatka dla odbiorcy (max 103 znaki)',
                        '',//'Notatka dla kierowcy (max 103 znaki)',
                        '',//'Ubezpieczenie (0/1)',
                        '',//'Wartość produktów do ubezpieczenia',
                       '',// 'Potwierdzenie dostarczenia (0/1)',
                        '',//'Email lub telefon do potwierdzenia',
                        '',//'Powiadomienie o uszkodzeniu',
                        '',//'Email lub telefon do powiadomienia',
                        '',//'APZ - powiadomienie o problemach',
                        '',//'APS email lub telefon',
                        '',//'Usługa dla b2c (adres prywatny) (0/1)',
                        '',//'Dostawa do 12 0/1',
                        '',//'Gwarantowana dostawa 0/1',
                        '',//'Service - POD 0/1',
                        '',//'Service - email for POD',
                        '',//'Service - SMS advice of delivery 0/1',
                        '',//'Service - Phone advice of delivery 0/1',
                        '',//'Recipient - evidence number (puste)',
                        '',//'Recipient - street number (puste)',
                        '',//'CrossDock - Name (for CD only) (puste)',
                        '',//'CrossDock - type of CD collection (puste)',
                        '',//'CrossDock - car ID (own delivery) (puste)',
                        '',//'CrossDock - Dispatch date (for CD only) (puste)',
                        '',//'Service - Email to recipient (puste)',
                        '',//'Service - Card pament (puste)',
                        '',//'Shipment to pickup point - PP code (puste)',
                        '',//'PP shipment - without recipient address (puste)',
                        '',//'Service - B2C assisted delivery (B2P) (puste)',
                        '',//'Service - B2C appliance collection (B2S) (puste)',
                        '',//'Frigo shipment 0/1',
                        '',//'Frigo mode',
                        '',//'Delivery notes back (VDL) 0/1',
                        '',//'Contracts back (VSM) 0/1',
                        '',//'Premium delivery (VIP) 0/1',
                       '',// 'Premium delivery (VIP) telephone number',
                        '',//'Length',
                        '',//'Width',
                        '',//'Height',
                        '',//'Fixtermin (FIX) 0/1',
                        '',//'Fixtermin (FIX) - date',
                        '',//'IBAN account number',
                        '',//'COD express 0/1',
                       '',// 'Individual bank account for COD 0/1',
                        '',//'Number of pallet positions',
                        '',//'Non standard packaging 0/1',
                        '',//'Service - SMS aviso of shipment collection (SAS) 0/1',
                       '',// 'Service - phone aviso of shipment collection (SAT) 0/1',
                        '',//'Phone number for service SAS / SAT',
                       '',// 'ROD service 0/1',
                      '',//  'ROD service - return address code',
                      '',//  'ROD service - document 1 type',
                       '',// 'ROD service - document 1 description',
                       '',// 'ROD service - document 2 type',
                       '',// 'ROD service - document 2 description',
                       '',// 'ROD service - document 3 type',
                       '',// 'ROD service - document 3 description',
                       '',// 'ROD service - document 4 type',
                       '',// 'ROD service - document 4 description',
                       '',// 'ROD service - document 5 type',
                       '',// 'ROD service - document 5 description',
                       '',// 'ROD service - document 6 type',
                       ''// 'ROD service - document 6 description',
                        ];
                    }
                    $csvData = ['labels' => $csvLabels, 'csv_multi' => $csv];
                    $filename = 'geis_' . date("Y-m-d");

                    $delimiter = ';';
                    $enclausure = '"';
                    header("Content-type: text/csv");
                    header("Content-Disposition: attachment; filename={$filename}.csv");
                    header("Pragma: no-cache");
                    header("Expires: 0");
                    $this->outputCSV($csvData, $delimiter, $enclausure);
                    die();
                }
            } elseif (key_exists('inpost', $rqData)) {
                $allOrders = $this->Zamowienie->find('all', ['contain' => ['ZamowienieTowar']])->where(['Zamowienie.id IN' => $rqData['ids']]);
                if ($allOrders->count() > 0) {
                    $csvLabels = ['e-mail', 'telefon', 'rozmiar', 'paczkomat', 'numer_referencyjny', 'ubezpieczenie', 'za_pobraniem', 'imie_i_nazwisko', 'nazwa_firmy', 'ulica', 'kod_pocztowy', 'miejscowosc', 'typ_przesylki'];
                    $csv = [];
                    foreach ($allOrders as $zamowienie) {
                        $csv[] = [
                            $zamowienie->email,
                            $zamowienie->telefon,
                            'A',
                            $zamowienie->paczkomat,
                            $zamowienie->numer,
                            round($zamowienie->wartosc_produktow, 2),
                            (($zamowienie->rodzaj_platnosci_id == 1) ? round($zamowienie->wartosc_razem, 2) : ''),
                            $zamowienie->imie . ' ' . $zamowienie->nazwisko,
                            (!empty($zamowienie->szkola) ? $zamowienie->szkola : (!empty($zamowienie->firma) ? $zamowienie->firma : '')),
                            $zamowienie->wysylka_ulica . (!empty($zamowienie->wysylka_nr_domu) ? ' ' . $zamowienie->wysylka_nr_domu . (!empty($zamowienie->wysylka_nr_lokalu) ? '/' . $zamowienie->wysylka_nr_lokalu : '') : ''),
                            $zamowienie->wysylka_kod,
                            $zamowienie->wysylka_miasto,
                            'paczkomaty'
                        ];
                    }
                    $csvData = ['labels' => $csvLabels, 'csv_multi' => $csv];
                    $filename = 'inpost_' . date("Y-m-d");

                    $delimiter = ';';
                    $enclausure = '"';
                    header("Content-type: text/csv");
                    header("Content-Disposition: attachment; filename={$filename}.csv");
                    header("Pragma: no-cache");
                    header("Expires: 0");
                    $this->outputCSV($csvData, $delimiter, $enclausure);
                    die();
                }
            } elseif (key_exists('subiekt', $rqData)) {
                $this->autoRender = false;
                $dostawcy = [];
                $allOrders = $this->Zamowienie->find('all', ['contain' => ['ZamowienieTowar', 'RodzajPlatnosci']])->where(['Zamowienie.id IN' => $rqData['ids']]);
                if ($allOrders->count() > 0) {
                    $csvHeader = [
                        'Data sprzedaży',
                        'Numer zamówienia',
                        'ID klienta',
                        'Email',
                        'NIP',
                        'Nazwa',
                        'Ulica',
                        'Kod',
                        'Miasto',
                        'Magazyn',
                        'ID produktu',
                        'Kod produktu',
                        'Produkt',
                        'Ilość',
                        'VAT',
                        'Cena jednostkowa netto ',
                        'Wartość netto',
                        'Cena jednostkowa brutto',
                        'Wartość brutto',
                        'Cena jednostkowa netto po rabacie',
                        'Wartość netto po rabacie',
                        'Cena jednostkowa brutto po rabacie',
                        'Wartość brutto po rabacie',
                        'Jednostka',
                        'Rabat',
                        'Uwagi zamówienia'
                    ];
                    $csvLines = [];
                    foreach ($allOrders as $order) {
                        $rabat = 0;
                        $w100 = $order->wartosc_produktow + $order->wysylka_koszt + $order->wysylka_koszt_long;
                        $wr = $order->wartosc_razem;
                        if ($w100 != $wr) {
                            $rabat = (100 - (($wr * 100) / $w100)) / 100;
                        }

                        foreach ($order->zamowienie_towar as $orderTowar) {
                            $csvLines[] = [
                                $this->Txt->printDate($order->data, 'Y-m-d'),
                                $order->numer,
                                (!empty($order->uzytkownik_id) ? 'u' . $order->uzytkownik_id : 'z' . $order->id),
                                $order->email,
                                (!empty($order->faktura_nip) ? $order->faktura_nip : $order->nip),
                                (!empty($order->faktura_gmina) ? $order->faktura_gmina : (!empty($order->faktura_firma) ? $order->faktura_firma : (!empty($order->faktura_nazwisko) ? $order->faktura_imie . ' ' . $order->faktura_nazwisko : $order->imie . ' ' . $order->nazwisko))),
                                (!empty($order->faktura_ulica) ? $order->faktura_ulica : $order->wysylka_ulica),
                                (!empty($order->faktura_kod) ? $order->faktura_kod : $order->wysylka_kod),
                                (!empty($order->faktura_miasto) ? $order->faktura_miasto : $order->wysylka_miasto),
                                '',
                                $orderTowar->towar_id,
                                $orderTowar->kod,
                                $orderTowar->nazwa,
                                $orderTowar->ilosc,
                                $orderTowar->vat_stawka,
                                $orderTowar->cena_za_sztuke_netto,
                                $orderTowar->cena_razem_netto,
                                $orderTowar->cena_za_sztuke_brutto,
                                $orderTowar->cena_razem_brutto,
                                $orderTowar->cena_za_sztuke_netto - ($orderTowar->cena_za_sztuke_netto * $rabat),
                                $orderTowar->cena_razem_netto - ($orderTowar->cena_razem_netto * $rabat),
                                $orderTowar->cena_za_sztuke_brutto - ($orderTowar->cena_za_sztuke_brutto * $rabat),
                                $orderTowar->cena_razem_brutto - ($orderTowar->cena_razem_brutto * $rabat),
                                $orderTowar->jednostka_str,
                                $rabat,
                                $order->uwagi
                            ];
                        }
                        if (!empty($order->wysylka_koszt)) {
                            $csvLines[] = [
                                $this->Txt->printDate($order->data, 'Y-m-d'),
                                $order->numer,
                                (!empty($order->uzytkownik_id) ? 'u' . $order->uzytkownik_id : 'z' . $order->id),
                                $order->email,
                                (!empty($order->faktura_nip) ? $order->faktura_nip : $order->nip),
                                (!empty($order->faktura_gmina) ? $order->faktura_gmina : (!empty($order->faktura_firma) ? $order->faktura_firma : (!empty($order->faktura_nazwisko) ? $order->faktura_imie . ' ' . $order->faktura_nazwisko : $order->imie . ' ' . $order->nazwisko))),
                                (!empty($order->faktura_ulica) ? $order->faktura_ulica : $order->wysylka_ulica),
                                (!empty($order->faktura_kod) ? $order->faktura_kod : $order->wysylka_kod),
                                (!empty($order->faktura_miasto) ? $order->faktura_miasto : $order->wysylka_miasto),
                                '',
                                $orderTowar->towar_id,
                                'Transport',
                                'Transport',
                                1,
                                c('wysylka.stawkaVat'),
                                '',
                                '',
                                $order->wysylka_koszt,
                                $order->wysylka_koszt,
                                '',
                                '',
                                round($order->wysylka_koszt - ($order->wysylka_koszt * $rabat), 2),
                                round($order->wysylka_koszt - ($order->wysylka_koszt * $rabat), 2),
                                'szt.',
                                $rabat,
                                $order->uwagi
                            ];
                        }
                    }
                    $name = 'zamowienia_subiekt.csv';

                    header('Content-Type: text/csv');
                    header('Content-Disposition: attachment; filename="' . $name . '"');

                    $fp = fopen('php://output', 'wb');
                    fputcsv($fp, $csvHeader);
                    foreach ($csvLines as $line) {
                        fputcsv($fp, $line);
                    }
                    fclose($fp);
                    die();
//
//                    $view = new \Cake\View\View();
//                    $view->layout = 'ajax';
//                    $view->viewBuilder()->setLayout('ajax');
//                    $view->set('zamowienie', $allOrders);
//                    $xml = $view->render('Admin/Zamowienie/subiekt');
//                    header('Content-Disposition: attachment; filename="' . $name . '"');
//                    header('Content-Type: application/xml; charset=utf-8');
//                    echo $xml;
                    die();
                }
            } elseif (key_exists('subiekt_epp', $rqData)) {
                $this->autoRender = false;
                $eppType = 'ZK'; //ZK - Zamówienie klienta FS - Faktura sprzedazy
                if ($rqData['subiekt_epp'] == 'FS') {
                    $eppType = 'FS';
                }
                $dostawcy = [];
                $conditions = [];
                if (!empty($rqData['ids'])) {
                    $conditions['Zamowienie.id IN'] = $rqData['ids'];
                } else {
                    $conditions['Zamowienie.subiekt'] = 0;
                    $conditions['Zamowienie.data >='] = $this->Txt->printDate(new \DateTime('-4 days'), 'Y-m-d 00:00:00');
                    if (!empty($rqData['pdf_status'])) {
                        $conditions['Zamowienie.status'] = $rqData['pdf_status'];
                    } else {
                        $conditions['Zamowienie.status'] = 'oplacone';
                    }
                }
                $allOrders = $this->Zamowienie->find('all', ['contain' => ['ZamowienieTowar', 'RodzajPlatnosci', 'Wysylka' => ['Vat']]])->where($conditions);
                if ($allOrders->count() > 0) {
//"1.05",3,1250,"Nexo","PRZYKLAD","Firma prezentacyjna","PHU Prezentacja sp. z o.o.","Wrocław","54-519","Jerzmanowska 2","0000000000","MAG","Magazyn główny",,,0,,,,20191014000000,"Polska","PL","PL0000000000",1
                    $fileLines = [];
                    $breakLine = "\r\n";
                    $nadawca = join(',', [
                        'Wersja formatu pliku EDI++' => '"1.05"',
                        'Cel komunikacji' => '3',
                        'Strona kodowa zapisu' => '1250',
                        'Informacja o programie' => '"' . c('subiekt.id_programu') . '"',
                        'Kod identyfikacyjny nadawcy pliku komunikacji' => '"' . c('subiekt.id_nadawcy') . '"',
                        'Nazwa skrócona nadawcy' => '"' . c('subiekt.nazwa_krotka') . '"',
                        'Nazwa długa nadawcy' => '"' . c('subiekt.nazwa_pelna') . '"',
                        'Miasto' => '"' . c('subiekt.miasto') . '"',
                        'Kod pocztowy nadawcy' => '"' . c('subiekt.kod_pocztowy') . '"',
                        'Ulica i nr nadawcy' => '"' . c('subiekt.adres') . '"',
                        'NIP nadawcy' => '"' . c('subiekt.nip') . '"',
                        'Kod magazynu, z którego pochodzą zapisane informacje' => '',
                        'Nazwa magazynu' => '',
                        'Opis magazynu' => '',
                        'Analityka magazynu' => '',
                        'Dane z okresu (następujące daty są istotne)' => '0',
                        'Początek okresu, z którego pochodzą dane' => $this->Txt->printDate(new \DateTime(), 'Ymd') . '000000',
                        'Koniec okresu, z którego pochodzą dane' => $this->Txt->printDate(new \DateTime(), 'Ymd') . '000000',
                        'Kto wykonał komunikację' => '',
                        'Kiedy została wykonana komunikacja' => $this->Txt->printDate(new \DateTime(), 'Ymd') . '000000',
                        'Państwo' => '"Polska"',
                        'Prefiks państwa UE' => '"PL"',
                        'NIP unijny nadawcy' => (!empty(c('subiekt.nip_ue')) ? '"' . c('subiekt.nip_ue') . '"' : NULL),
                        'Czy nadawca jest unijny' => (!c('subiekt.vat_ue') ? 0 : 1)
                    ]);
//                    var_dump(mb_convert_encoding($nadawca,'CP1250')); die();
                    $fileLines[] = '[INFO]';
                    $fileLines[] = $nadawca; // mb_convert_encoding($nadawca,'Windows-1250');
                    $fileLines[] = $breakLine;
                    $kontrahenci = [];
                    $towary = [];
                    $subiektGetsId = [];
                    foreach ($allOrders as $order) {
                        $subiektGetsId[$order->id] = $order->id;
                        $eppLines = [];
                        $rabat = 0;
                        $w100 = $order->wartosc_produktow + $order->wysylka_koszt;
                        $wr = $order->wartosc_razem;
                        if ($w100 != $wr) {
                            $rabat = round((100 - (($wr * 100) / $w100)) / 100, 4);
                        }
                        if ($rabat < 0) {
                            $rabat = 0;
                        }
                        $countPozycje = 0;
                        $wartoscNetto = 0;
                        $wartoscBrutto = 0;
                        $lp = 1;
                        foreach ($order->zamowienie_towar as $orderTowar) {
                            $prCenaNetto = round($orderTowar->cena_za_sztuke_netto - ($orderTowar->cena_za_sztuke_netto * $rabat), 2);
                            $prCenaBrutto = round($orderTowar->cena_za_sztuke_brutto - ($orderTowar->cena_za_sztuke_brutto * $rabat), 2);
                            $prWartoscNetto = round($orderTowar->cena_razem_netto - ($orderTowar->cena_razem_netto * $rabat), 2);
                            $prWartoscBrutto = round($orderTowar->cena_razem_brutto - ($orderTowar->cena_razem_brutto * $rabat), 2);
                            $itemSymbol = (!empty($orderTowar->kod) ? $orderTowar->kod : (!empty($orderTowar->towar_id) ? 't_' . $orderTowar->towar_id : 'zt_' . $orderTowar->id)) . (!empty($orderTowar->wariant_id) ? '_' . $orderTowar->wariant_id : '');
                            $eppLines[] = join(',', [
                                'Liczba porządkowa' => $lp++,
                                'Typ towaru' => 1,
                                'Kod identyfikacyjny towaru' => '"' . $itemSymbol . '"',
                                'Rabat procentowy' => 1,
                                'Rabat od ceny' => 1,
                                'Rabat na pozycji kumulowany z rabatem od całego dokumentu ' => '0',
                                'Rabat zablokowany dla tej pozycji ' => '0',
                                'Wartość udzielonego na pozycji rabatu' => '0',
                                'Wysokość rabatu udzielonego na pozycji w procentach ' => '0',
                                'Jednostka miary ' => '"' . $orderTowar->jednostka_str . '"',
                                'Ilość towaru w jednostce miary ' => $orderTowar->ilosc,
                                'Ilość towaru w jednostce magazynowej ' => $orderTowar->ilosc,
                                'Cena magazynowa towaru' => $orderTowar->cena_podstawowa,
                                'Cena netto towaru ' => $prCenaNetto,
                                'Cena brutto towaru' => $prCenaBrutto,
                                'Wysokość stawki podatku VAT w procentach ' => $orderTowar->vat_stawka,
                                'Wartość netto pozycji' => $prWartoscNetto,
                                'Wartość VAT' => $prWartoscBrutto - $prWartoscNetto,
                                'Wartość brutto' => $prWartoscBrutto,
                                'Koszt pozycji' => '0', //$prWartoscBrutto,
                                'Opis usługi jednorazowej ' => NULL,
                                'Nazwa usługi jednorazowej ' => NULL
                            ]);
                            $towary[$itemSymbol] = join(',', [
                                'Typ towaru' => '1',
                                'Kod identyfikacyjny' => '"' . $itemSymbol . '"',
                                'Kod towaru u producenta' => '"' . $orderTowar->ean . '"',
                                'Kod paskowy' => NULL,
                                'Nazwa' => '"' . $orderTowar->nazwa . '"',
                                'Opis' => NULL,
                                'Nazwa towaru dla urządzeń fiskalnych' => NULL,
                                'Symbol SWW lub KU ' => NULL,
                                'Symbol PKWiU ' => NULL,
                                'Podstawowa jednostka miary ' => '"' . $orderTowar->jednostka_str . '"',
                                'Symbol stawki podatku VAT ' => '"' . $orderTowar->vat_stawka . '"',
                                'Wysokość stawki podatku VAT w procentach ' => $orderTowar->vat_stawka / 100,
                                'Symbol stawki podatku VAT przy zakupie ' => '"' . $orderTowar->vat_stawka . '"',
                                'Wysokość stawki podatku VAT przy zakupie w procentach ' => $orderTowar->vat_stawka / 100,
                                'Ostatnia cena zakupu netto' => 0,
                                'Cena zakupu walutowa ' => 0,
                                'Jednostka miary przy zakupie' => '"' . $orderTowar->jednostka_str . '"',
                                'Kurs waluty służący do kalkulacji ceny zakupu' => '0',
                                'Symbol waluty' => 'PLN',
                                'Kod opakowania związanego z towarem' => NULL,
                                'Jednostka miary dla stanu minimalnego' => '"' . $orderTowar->jednostka_str . '"',
                                'Stan minimalny' => 0,
                                'Średni czas dostawy' => 0,
                                'Kod identyfikacyjny producenta / dostawcy' => NULL,
                                'Data ważności jako konkretny dzień' => NULL,
                                'Data ważności jako ilość dni od ostatniej dostawy' => 0,
                                'Jednostka miary dla objętości ' => NULL,
                                'Objętość towaru (wybrana jednostka)' => 0,
                                'Ilość roboczo-godzin w jednostce podstawowej usługi' => 0,
                                'Rodzaj roboczo-godzin usługi ' => NULL,
                                'Cena otwarta ' => 0,
                                'Uwagi' => NULL,
                                'Podstawa kalkulacji cen' => 0,
                                'Towar ważony na wadze etykietującej' => 0,
                                'Pole użytkownika 1' => NULL,
                                'Pole użytkownika 2' => NULL,
                                'Pole użytkownika 3' => NULL,
                                'Pole użytkownika 4' => NULL,
                                'Pole użytkownika 5' => NULL,
                                'Pole użytkownika 6' => NULL,
                                'Pole użytkownika 7' => NULL,
                                'Pole użytkownika 8' => NULL
                            ]);
                            $wartoscBrutto += round($orderTowar->cena_razem_brutto - ($orderTowar->cena_razem_brutto * $rabat), 2);
                            $wartoscNetto += round($orderTowar->cena_razem_netto - ($orderTowar->cena_razem_netto * $rabat), 2);
                            $countPozycje++;
                        }
                        if (!empty($order->wysylka_koszt)) {
                            $wysylkaSymbol = ((!empty($order->wysylka) && !empty($order->wysylka->subiekt_kod)) ? $order->wysylka->subiekt_kod : 'Transport');
                            $wysylkaNazwa = ((!empty($order->wysylka) && !empty($order->wysylka->nazwa)) ? $order->wysylka->nazwa : 'Transport');
                            $wysylkaVat = ((!empty($order->wysylka) && !empty($order->wysylka->vat)) ? $order->wysylka->vat->stawka : c('wysylka.stawkaVat'));
                            $vatStawka = (1 + ($wysylkaVat / 100));
                            $prCenaNetto = round(($order->wysylka_koszt - ($order->wysylka_koszt * $rabat)) / $vatStawka, 2);
                            $prCenaBrutto = round($order->wysylka_koszt - ($order->wysylka_koszt * $rabat), 2);
                            $prWartoscNetto = $prCenaNetto;
                            $prWartoscBrutto = $prCenaBrutto;
                            $eppLines[] = join(',', [
                                'Liczba porządkowa' => $lp++,
                                'Typ towaru' => 2,
                                'Kod identyfikacyjny towaru' => '"' . $wysylkaSymbol . '"',
                                'Rabat procentowy' => 1,
                                'Rabat od ceny' => 1,
                                'Rabat na pozycji kumulowany z rabatem od całego dokumentu ' => '0',
                                'Rabat zablokowany dla tej pozycji ' => '0',
                                'Wartość udzielonego na pozycji rabatu' => '0',
                                'Wysokość rabatu udzielonego na pozycji w procentach ' => '0',
                                'Jednostka miary ' => '"szt."',
                                'Ilość towaru w jednostce miary ' => 1,
                                'Ilość towaru w jednostce magazynowej ' => 1,
                                'Cena magazynowa towaru' => $prCenaNetto,
                                'Cena netto towaru ' => $prCenaNetto,
                                'Cena brutto towaru' => $prCenaBrutto,
                                'Wysokość stawki podatku VAT w procentach ' => $wysylkaVat,
                                'Wartość netto pozycji' => $prWartoscNetto,
                                'Wartość VAT' => $prWartoscBrutto - $prWartoscNetto,
                                'Wartość brutto' => $prWartoscBrutto,
                                'Koszt pozycji' => 0, //$prWartoscBrutto,
                                'Opis usługi jednorazowej ' => NULL,
                                'Nazwa usługi jednorazowej ' => NULL
                            ]);
                            $towary[$wysylkaSymbol] = join(',', [
                                'Typ towaru' => '2',
                                'Kod identyfikacyjny' => '"' . $wysylkaSymbol . '"',
                                'Kod towaru u producenta' => '',
                                'Kod paskowy' => NULL,
                                'Nazwa' => '"' . $wysylkaNazwa . '"',
                                'Opis' => NULL,
                                'Nazwa towaru dla urządzeń fiskalnych' => NULL,
                                'Symbol SWW lub KU ' => NULL,
                                'Symbol PKWiU ' => NULL,
                                'Podstawowa jednostka miary ' => '"szt."',
                                'Symbol stawki podatku VAT ' => '"' . $wysylkaVat . '"',
                                'Wysokość stawki podatku VAT w procentach ' => $wysylkaVat / 100,
                                'Symbol stawki podatku VAT przy zakupie ' => '"' . $wysylkaVat . '"',
                                'Wysokość stawki podatku VAT przy zakupie w procentach ' => $wysylkaVat / 100,
                                'Ostatnia cena zakupu netto' => 0,
                                'Cena zakupu walutowa ' => 0,
                                'Jednostka miary przy zakupie' => '"szt."',
                                'Kurs waluty służący do kalkulacji ceny zakupu' => '0',
                                'Symbol waluty' => 'PLN',
                                'Kod opakowania związanego z towarem' => NULL,
                                'Jednostka miary dla stanu minimalnego' => '"szt."',
                                'Stan minimalny' => 0,
                                'Średni czas dostawy' => 0,
                                'Kod identyfikacyjny producenta / dostawcy' => NULL,
                                'Data ważności jako konkretny dzień' => NULL,
                                'Data ważności jako ilość dni od ostatniej dostawy' => 0,
                                'Jednostka miary dla objętości ' => NULL,
                                'Objętość towaru (wybrana jednostka)' => 0,
                                'Ilość roboczo-godzin w jednostce podstawowej usługi' => 0,
                                'Rodzaj roboczo-godzin usługi ' => NULL,
                                'Cena otwarta ' => 0,
                                'Uwagi' => NULL,
                                'Podstawa kalkulacji cen' => 0,
                                'Towar ważony na wadze etykietującej' => 0,
                                'Pole użytkownika 1' => NULL,
                                'Pole użytkownika 2' => NULL,
                                'Pole użytkownika 3' => NULL,
                                'Pole użytkownika 4' => NULL,
                                'Pole użytkownika 5' => NULL,
                                'Pole użytkownika 6' => NULL,
                                'Pole użytkownika 7' => NULL,
                                'Pole użytkownika 8' => NULL
                            ]);
                            $wartoscBrutto += $prWartoscBrutto;
                            $wartoscNetto += $prWartoscNetto;
                            $countPozycje++;
                        }
                        $fvHeader = join(',', [
                            'TYP DOKUMENTU' => '"' . $eppType . '"',
                            'Status dokumentu (0)' => '0',
                            'Status rejestracji fiskalnej dokumentu (0)' => '0',
                            'Numer dokumentu (id)' => '1',
                            'Numer dokumentu dostawcy (puste)' => NULL,
                            'Rozszerzenie numeru wpisane przez użytkownika (puste)' => NULL,
                            'Pełny numer dokumentu (pierwsze maks. cztery znaki to mnemonik typu dokumentu i spacja)  (puste)' => '"' . $order->numer . '"',
                            'Numer dokumentu korygowanego (puste)' => NULL,
                            'Data wystawienia dokumentu korygowanego(puste)' => NULL,
                            'Numer zamówienia' => '"' . $order->numer . '"',
                            'Magazyn docelowy dla MM (symbol) (puste)' => NULL,
                            'Kod identyfikacyjny kontrahenta' => '"' . (!empty($order->uzytkownik_id) ? 'u' . $order->uzytkownik_id : 'z' . $order->id) . '"',
                            'Nazwa skrócona kontrahenta' => '"' . $this->Txt->cutText((!empty($order->faktura_gmina) ? $order->faktura_gmina : (!empty($order->faktura_firma) ? $order->faktura_firma : (!empty($order->faktura_nazwisko) ? $order->faktura_imie . ' ' . $order->faktura_nazwisko : $order->imie . ' ' . $order->nazwisko))), 40) . '"',
                            'Nazwa pełna kontrahenta' => '"' . (!empty($order->faktura_gmina) ? $order->faktura_gmina : (!empty($order->faktura_firma) ? $order->faktura_firma : (!empty($order->faktura_nazwisko) ? $order->faktura_imie . ' ' . $order->faktura_nazwisko : $order->imie . ' ' . $order->nazwisko))) . '"',
                            'Miasto kontrahenta' => '"' . (!empty($order->faktura_miasto) ? $order->faktura_miasto : $order->wysylka_miasto) . '"',
                            'Kod pocztowy kontrahenta ' => '"' . (!empty($order->faktura_kod) ? $order->faktura_kod : $order->wysylka_kod) . '"',
                            'Ulica i numer kontrahenta (adres)' => '"' . (!empty($order->faktura_ulica) ? $order->faktura_ulica : $order->wysylka_ulica) . '"',
                            'NIP kontrahenta (unijny lub krajowy)' => (!empty($order->faktura_nip) ? '"' . $order->faktura_nip . '"' : (!empty($order->nip) ? '"' . $order->nip . '"' : NULL)),
                            'Kategoria (nazwa) (Sprzedaż)' => '"Sprzedaż"',
                            'Podtytuł kategorii (puste)' => NULL,
                            'Miejsce wystawienia' => '"' . c('subiekt.miesjce_wystawienia') . '"', // tu
                            'Data wystawienia ' => $this->Txt->printDate($order->data, 'Ymd') . '000000',
                            'Data sprzedaży' => $this->Txt->printDate($order->data, 'Ymd') . '000000',
                            'Data otrzymania ' => NULL,
                            'Liczba pozycji ' => $countPozycje,
                            'Czy dokument wystawiany wg cen netto (0/1)' => '0',
                            'Aktywna cena (nazwa) ' => '"Podstawowy"',
                            'Wartość netto ' => $wartoscNetto, //tu
                            'Wartość VAT' => ($wartoscBrutto - $wartoscNetto),
                            'Wartość brutto ' => $wartoscBrutto,
                            'Koszt' => 0, //$wartoscBrutto,
                            'Rabat (nazwa) ' => NULL,
                            'Rabat (procent) ' => $rabat,
                            'Forma płatności (nazwa) ' => NULL,
                            'Termin płatności ' => $this->Txt->printDate($order->data, 'Ymd') . '000000',
                            'Kwota zapłacona przy odbiorze dokumentu ' => $wartoscBrutto,
                            'Wartość do zapłaty ' => $wartoscBrutto,
                            'Zaokrąglenie wartości do zapłaty (0)' => '0',
                            'Zaokrąglenie wartości VAT (0)' => '0',
                            'Automatycznie przeliczana tabela VAT i wartość dokumentu  (0/1)' => '1',
                            'Statusy rozszerzone i specjalne dokumentów (0)' => '5',
                            'Nazwisko i imię osoby wystawiającej dokument ' => NULL,
                            'Nazwisko i imię osoby odbierającej dokument' => NULL,
                            'Podstawa wydania dokumentu ' => NULL,
                            'Wartość wydanych opakowań ' => '0',
                            'Wartość zwróconych opakowań ' => '0',
                            'Waluta (symbol)' => '"PLN"',
                            'Kurs waluty' => '1',
                            'Uwagi' => NULL,
                            'Komentarz' => NULL,
                            'Podtytuł dokumentu ' => __(c('subiekt.podtytul_fv'), ['numer' => $order->numer]),
                            'Nie używane ' => NULL,
                            'Przeprowadzony import dokumentu (0)' => '0',
                            'Dokument eksportowy (0/1)' => '0',
                            'Rodzaj transakcji (0)' => '0',
                            'Płatność kartą (nazwa) ' => NULL,
                            'Płatność kartą (kwota) ' => NULL,
                            'Płatność kredytowa (nazwa) ' => NULL,
                            'Płatność kredytowa (kwota) ' => NULL,
                            'Państwo kontrahenta ' => '"Polska"',
                            'Prefiks państwa UE kontrahenta' => '"PL"',
                            'Czy kontrahent jest unijny(0/1)' => '0'
                        ]);
                        $fileLines[] = '[NAGLOWEK]';
                        $fileLines[] = $fvHeader;
                        $fileLines[] = $breakLine;
                        $fileLines[] = '[ZAWARTOSC]';
                        $fileLines[] = join($breakLine, $eppLines);
                        $kontrahenci[(!empty($order->uzytkownik_id) ? 'u' . $order->uzytkownik_id : 'z' . $order->id)] = join(',', [
                            'Typ kontrahenta' => '0',
                            'Kod identyfikacyjny' => '"' . (!empty($order->uzytkownik_id) ? 'u' . $order->uzytkownik_id : 'z' . $order->id) . '"',
                            'Nazwa skrócona' => '"' . $this->Txt->cutText((!empty($order->faktura_gmina) ? $order->faktura_gmina : (!empty($order->faktura_firma) ? $order->faktura_firma : (!empty($order->faktura_nazwisko) ? $order->faktura_imie . ' ' . $order->faktura_nazwisko : $order->imie . ' ' . $order->nazwisko))), 40) . '"',
                            'Nazwa pełna' => '"' . (!empty($order->faktura_gmina) ? $order->faktura_gmina : (!empty($order->faktura_firma) ? $order->faktura_firma : (!empty($order->faktura_nazwisko) ? $order->faktura_imie . ' ' . $order->faktura_nazwisko : $order->imie . ' ' . $order->nazwisko))) . '"',
                            'Miasto' => '"' . (!empty($order->faktura_miasto) ? $order->faktura_miasto : $order->wysylka_miasto) . '"',
                            'Kod pocztowy' => '"' . (!empty($order->faktura_kod) ? $order->faktura_kod : $order->wysylka_kod) . '"',
                            'Ulica i numer' => '"' . (!empty($order->faktura_ulica) ? $order->faktura_ulica : $order->wysylka_ulica) . '"',
                            'NIP' => '"' . (!empty($order->faktura_nip) ? $order->faktura_nip : $order->nip) . '"',
                            'REGON' => (!empty($order->faktura_regon) ? '"' . $order->faktura_regon . '"' : (!empty($order->regon) ? '"' . $order->regon . '"' : NULL)),
                            'Telefon' => (!empty($order->telefon) ? '"' . $order->telefon . '"' : NULL),
                            'Faks' => NULL,
                            'Teleks' => NULL,
                            'e-mail' => (!empty($order->email) ? '"' . $order->email . '"' : NULL),
                            'Adres stron WWW' => NULL,
                            'Nazwisko i imię osoby kontaktowej' => '"' . $order->imie . ' ' . $order->nazwisko . '"',
                            'Analityka dostawcy' => NULL,
                            'Analityka odbiorcy' => NULL,
                            'Pole użytkownika 1' => NULL,
                            'Pole użytkownika 2' => NULL,
                            'Pole użytkownika 3' => NULL,
                            'Pole użytkownika 4' => NULL,
                            'Pole użytkownika 5' => NULL,
                            'Pole użytkownika 6' => NULL,
                            'Pole użytkownika 7' => NULL,
                            'Pole użytkownika 8' => NULL,
                            'Nazwa banku' => NULL,
                            'Numer konta w banku' => NULL,
                            'Państwo kontrahenta' => 'Polska',
                            'Prefiks państwa UE kontrahenta ' => 'PL',
                            'Czy kontrahent jest unijny ' => '0'
                        ]);
                        $fileLines[] = $breakLine;
                    }
                    $fileLines[] = '[NAGLOWEK]';
                    $fileLines[] = '"KONTRAHENCI"';
                    $fileLines[] = $breakLine;
                    $fileLines[] = '[ZAWARTOSC]';
                    $fileLines[] = join($breakLine, $kontrahenci);
                    $fileLines[] = $breakLine;
                    $fileLines[] = '[NAGLOWEK]';
                    $fileLines[] = '"TOWARY"';
                    $fileLines[] = $breakLine;
                    $fileLines[] = '[ZAWARTOSC]';
                    $fileLines[] = join($breakLine, $towary);
                    $name = 'zamowienia_subiekt.epp';


                    $content = join($breakLine, $fileLines) . "\r\n\r\n";
//                    var_dump(iconv('UTF-8', 'Windows-1250//TRANSLIT', $content)); die();
                    $this->Zamowienie->updateAll(['subiekt' => 1], ['id IN' => $subiektGetsId]);
                    header('Content-Type: text/plain');
                    header('Content-Disposition: attachment; filename="' . $name . '"');
                    $fp = fopen('php://output', 'wb');
                    fputs($fp, iconv('UTF-8', 'Windows-1250//TRANSLIT', $content));
                    fclose($fp);
                    die();
//
//                    $view = new \Cake\View\View();
//                    $view->layout = 'ajax';
//                    $view->viewBuilder()->setLayout('ajax');
//                    $view->set('zamowienie', $allOrders);
//                    $xml = $view->render('Admin/Zamowienie/subiekt');
//                    header('Content-Disposition: attachment; filename="' . $name . '"');
//                    header('Content-Type: application/xml; charset=utf-8');
//                    echo $xml;
                    die();
                } else {
                    $this->Flash->error($this->Txt->printAdmin(__('Admin | Brak danych do pobrania')));
                }
            }
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | Zaznacz pozycje na liście')));
        }
        return $this->redirect($this->request->referer());
    }

    public function importSubiektFv() {
        $rqData = $this->request->getData();
        if (!empty($rqData['file']) && empty($rqData['file']['error'])) {
            $file = file_get_contents($rqData['file']['tmp_name']);
            $fileItems = [];
            $produkty = [];
            $firmaNazwa = '';
            $firmaMiasto = '';
            $firmaKodPocztowy = '';
            $firmaAdres = '';
            $firmaNip = '';
            if (!empty($file)) {
                $file = iconv('windows-1250', 'utf-8', $file);
                $version = '';
                $file = explode("\r\n", $file);
                $header = '';
                $itemKey = null;
                $subHeader = null;
                for ($i = 0; $i <= count($file); $i++) {
                    if (!empty($file[$i])) {
                        $value = str_getcsv($file[$i], ',', '"');
                        if (!empty($value) && strpos($value[0], '[') === 0) {
                            $header = trim(mb_strtolower(str_replace(['[', ']'], ['', ''], $value[0])));
                            if ($header == 'naglowek') {
                                $itemKey = null;
                            }
                            if ($header != 'zawartosc') {
                                $subHeader = null;
                            }
                            continue;
                        } elseif (count($value) == 1) {
                            $subHeader = trim(mb_strtolower($value[0]));
                            $itemKey = null;
                            continue;
                        } else {
                            if (!empty($header)) {
                                if ($header == 'info') {
                                    $firmaNazwa = $value[6];
                                    $firmaMiasto = $value[7];
                                    $firmaKodPocztowy = $value[8];
                                    $firmaAdres = $value[9];
                                    $firmaNip = $value[10];
                                } elseif ($header == 'naglowek') {
                                    if ($value[0] == 'FS') {
                                        $itemKey = $value[6];
                                        $fileItems[$itemKey] = array_combine(array_values($this->subiektFvFields), $value);
                                        continue;
                                    }
                                } elseif ($header == 'zawartosc' && !empty($itemKey) && count($value) > 1) {
                                    $fileItems[$itemKey]['faktura_pozycja'][] = array_combine(array_values($this->subiektFvPozycje), $value);
                                } elseif ($subHeader == 'towary' && count($value) > 1) {
                                    $produkty[$value[1]] = array_combine(array_values($this->subiektProduktFields), $value);
                                }
                            }
                        }
                    }
                }
            }
            $orderNumberSchema = explode('{numer}', c('subiekt.podtytul_fv'));
            $errors = [];
            $newItemsCount = 0;
            $updateItemsCount = 0;
            if (!empty($fileItems)) {
                $this->loadModel('Faktura');
                foreach ($fileItems as &$faktura) {
                    $orderNumber = $faktura['podtytul'];
                    if (!empty($orderNumber)) {
                        $numer = str_replace($orderNumberSchema, '', $orderNumber);
                        if (!empty($numer)) {
                            $extOrder = $this->Zamowienie->find()->where(['Zamowienie.numer' => $numer])->first();
                            if (!empty($extOrder)) {
                                $faktura['zamowienie_id'] = $extOrder->id;
                            }
                        }
                    }
                    $faktura['sklep_nazwa'] = $firmaNazwa;
                    $faktura['sklep_miasto'] = $firmaMiasto;
                    $faktura['sklep_kod_pocztowy'] = $firmaKodPocztowy;
                    $faktura['sklep_adres'] = $firmaAdres;
                    $faktura['sklep_nip'] = $firmaNip;
                    if (!empty($faktura['termin_platnosci'])) {
                        $faktura['termin_platnosci'] = $this->Txt->subiektDate($faktura['termin_platnosci']);
                    }
                    if (!empty($faktura['data_otrzymania'])) {
                        $faktura['data_otrzymania'] = $this->Txt->subiektDate($faktura['data_otrzymania']);
                    }
                    if (!empty($faktura['data_sprzedazy'])) {
                        $faktura['data_sprzedazy'] = $this->Txt->subiektDate($faktura['data_sprzedazy']);
                    }
                    if (!empty($faktura['data_wystawienia'])) {
                        $faktura['data_wystawienia'] = $this->Txt->subiektDate($faktura['data_wystawienia']);
                    }
                    if (!empty($faktura['data_wystawienia_do_korekty'])) {
                        $faktura['data_wystawienia_do_korekty'] = $this->Txt->subiektDate($faktura['data_wystawienia_do_korekty']);
                    }
                    foreach ($faktura['faktura_pozycja'] as &$fvItem) {
                        if (key_exists($fvItem['symbol'], $produkty)) {
                            $fvItem['nazwa'] = $produkty[$fvItem['symbol']]['nazwa'];
                        }
                    }
                    $oldPozycje = [];
                    $new = false;
                    $fakturaEntity = $this->Faktura->find('all', ['contain' => ['FakturaPozycja']])->where(['Faktura.numer_dokumentu' => $faktura['numer_dokumentu']])->first();
                    if (!empty($fakturaEntity)) {
                        if (!empty($fakturaEntity->faktura_pozycja)) {
                            foreach ($fakturaEntity->faktura_pozycja as $pozycja) {
                                $oldPozycje[$pozycja->lp . '_' . $pozycja->symbol] = $pozycja->id;
                            }
                        }
                        if (!empty($oldPozycje) && !empty($faktura['faktura_pozycja'])) {
                            foreach ($faktura['faktura_pozycja'] as &$actPozycja) {
                                if (key_exists($actPozycja['lp'] . '_' . $actPozycja['symbol'], $oldPozycje)) {
                                    $actPozycja['id'] = $oldPozycje[$actPozycja['lp'] . '_' . $actPozycja['symbol']];
                                    unset($oldPozycje[$actPozycja['lp'] . '_' . $actPozycja['symbol']]);
                                }
                            }
                        }
                        if (empty($fakturaEntity->token)) {
                            $faktura['token'] = sha1(uniqid($faktura['numer_dokumentu']));
                        }
                        $fakturaEntity = $this->Faktura->patchEntity($fakturaEntity, $faktura);
                    } else {
                        $new = true;
                        $faktura['token'] = sha1(uniqid($faktura['numer_dokumentu']));
                        $fakturaEntity = $this->Faktura->newEntity($faktura);
                    }
                    if ($this->Faktura->save($fakturaEntity)) {
                        if ($new) {
                            $newItemsCount++;
                        } else {
                            $updateItemsCount++;
                        }
                        if (!empty($oldPozycje)) {
                            $this->Fkatura->FakturaPozycja->deleteAll(['id IN' => $oldPozycje]);
                        }
                    } else {
                        \Cake\Log\Log::write('info', 'Save FV error: ' . json_encode($fakturaEntity->getErrors()));
                        $errors[] = $faktura['numer_dokumentu'];
                    }
                }
            }
        }
        if (!empty($errors)) {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | Błąd zapisu faktur: {0}', [join(", ", $errors)])));
        }
        if (!empty($newItemsCount)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | Dodano {0} nowych faktur', [$newItemsCount])));
        }
        if (!empty($updateItemsCount)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | Zaktualizowano {0} faktury', [$updateItemsCount])));
        }
        if (empty($newItemsCount) && empty($updateItemsCount)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | Brak faktur do wgrania i aktualizacji')));
        }
        return $this->redirect(['action' => 'index']);
    }

}
