<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * AtrybutTyp Controller
 *
 * @property \App\Model\Table\AtrybutTypTable $AtrybutTyp
 *
 * @method \App\Model\Entity\AtrybutTyp[] paginate($object = null, array $settings = [])
 */
class AtrybutTypController extends AppController
{

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        
        $this->loadModel('AtrybutTypParent');
                $atrybutGroups=$this->AtrybutTypParent->find('list',['keyField'=>'id','valueField'=>'nazwa','order'=>['nazwa'=>'asc']])->toArray();
                $this->set('atrybutGroups',$atrybutGroups);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        
        $lastPage = $this->session->read('backToAtrybutTyp');
        $this->session->delete('backToAtrybutTyp');
        $page = $this->request->query('page');
        $filter = $this->session->read('filterAtrybutTyp');
        $likeKeys = [
            'nazwa' => 'nazwa'
        ];
        $forcePage = false;
        if ($this->request->is('post')) {
            $filter=[];
            $filterData = $this->request->getData();
            if (key_exists('clear-filter', $filterData)) {
                $filter = null;
                $this->session->write('filterAtrybutTyp', $filter);
                return $this->redirect(['action'=>'index']);
            } else {
                foreach ($filterData as $keyFilter => $filterValue) {
                    if (!empty($filterValue)) {
                        $filter[$keyFilter] = $filterValue;
                    }
                }
            }
            $page = 1;
            $forcePage = true;
        }
        $conditions = [];
        if (!empty($filter)) {
            foreach ($filter as $keyFilter => $filterValue) {
                if (key_exists($keyFilter, $likeKeys)) {
                    $conditions['AtrybutTyp.' . $keyFilter . ' LIKE'] = '%' . $filterValue . '%';
                } else {
                    $conditions['AtrybutTyp.' . $keyFilter] = $filterValue;
                }
            }
        }
        $this->session->write('filterAtrybutTyp', $filter);
        if (!$forcePage && empty($page) && !empty($lastPage)) {
            $page = $lastPage;
        }
        if (empty($page)) {
            $page = 1;
        }
        $this->session->write('lastPageAtrybutTyp', $page);
        
        
        
        $this->paginate = [
            'contain' => ['AtrybutTypParent'],
            'conditions'=>$conditions,
            'order'=>['AtrybutTyp.kolejnosc'=>'desc'],
            'limit'=>200,
            'page'=>$page
        ];
$atrybutTyp = $this->paginate($this->AtrybutTyp);

        $this->set(compact('atrybutTyp','filter'));
        $this->set('_serialize', ['atrybutTyp']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $atrybutTyp = $this->AtrybutTyp->newEntity();
        if ($this->request->is('post')) {
        $rqData=$this->request->getData();
            $atrybutTyp = $this->AtrybutTyp->patchEntity($atrybutTyp, $rqData,['translations'=>$this->AtrybutTyp->hasBehavior('Translate')]);
            if ($this->AtrybutTyp->save($atrybutTyp)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | atrybut typ'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | atrybut typ'))])));
        }
        $atrybutTypParent = $this->AtrybutTyp->AtrybutTypParent->find('list',['keyField'=>'id','valueField'=>'nazwa'])->toArray();
        $this->set(compact('atrybutTyp', 'atrybutTypParent'));
        $this->set('_serialize', ['atrybutTyp']);
    }

    
    public function addGroup()
    {
        $atrybutTypParent = $this->AtrybutTypParent->newEntity();
        if ($this->request->is('post')) {
        $rqData=$this->request->getData();
            $atrybutTypParent = $this->AtrybutTypParent->patchEntity($atrybutTypParent, $rqData,['translations'=>$this->AtrybutTypParent->hasBehavior('Translate')]);
            if ($this->AtrybutTypParent->save($atrybutTypParent)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | atrybut group'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | atrybut group'))])));
        }
        $this->set(compact('atrybutTypParent'));
        $this->set('_serialize', ['atrybutTypParent']);
    }
    
    /**
     * Edit method
     *
     * @param string|null $id Atrybut Typ id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
    
    if($this->AtrybutTyp->hasBehavior('Translate')){
        $atrybutTyp = $this->AtrybutTyp->find('translations', [
            'locales' => $this->lngIdList,
            'conditions' => ['AtrybutTyp.'.$this->AtrybutTyp->primaryKey() => $id],
            'contain' => []
        ])->first();
        }
        else{
        $atrybutTyp = $this->AtrybutTyp->find('all', [
            'conditions'=>['AtrybutTyp.'.$this->AtrybutTyp->primaryKey()=>$id],
            'contain' => []
        ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData=$this->request->getData();
            $atrybutTyp = $this->AtrybutTyp->patchEntity($atrybutTyp, $rqData,['translations'=>$this->AtrybutTyp->hasBehavior('Translate')]);
            if ($this->AtrybutTyp->save($atrybutTyp)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | atrybut typ'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | atrybut typ'))])));
        }
        
        $_atrybutTyp = $atrybutTyp->toArray();
        if ($this->AtrybutTyp->hasBehavior('Translate') && empty($_atrybutTyp['_translations'])) {
            $transFields=$this->AtrybutTyp->associations()->keys();
            $tFields=[];
            foreach ($transFields as $field){
                if(strpos($field, '_translation')!==false){
                    $field= substr($field, (strpos($field, '_')+1));
                    $field=str_replace('_translation','', $field);
                    $tFields[$field]=$_atrybutTyp[$field]; 
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $atrybutTyp->set('_translations',$translation);
        }
        
        
        $atrybutTypParent = $this->AtrybutTyp->AtrybutTypParent->find('list',['keyField'=>'id','valueField'=>'nazwa'])->toArray();
        $this->set(compact('atrybutTyp', 'atrybutTypParent'));
        $this->set('_serialize', ['atrybutTyp']);
    }

    public function editGroup($id = null)
    {
    
    if($this->AtrybutTypParent->hasBehavior('Translate')){
        $atrybutTypParent = $this->AtrybutTypParent->find('translations', [
            'locales' => $this->lngIdList,
            'conditions' => ['AtrybutTypParent.'.$this->AtrybutTypParent->primaryKey() => $id],
            'contain' => []
        ])->first();
        }
        else{
        $atrybutTypParent = $this->AtrybutTypParent->find('all', [
            'conditions'=>['AtrybutTypParent.'.$this->AtrybutTypParent->primaryKey()=>$id],
            'contain' => []
        ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData=$this->request->getData();
            $atrybutTypParent = $this->AtrybutTypParent->patchEntity($atrybutTypParent, $rqData,['translations'=>$this->AtrybutTypParent->hasBehavior('Translate')]);
            if ($this->AtrybutTypParent->save($atrybutTypParent)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | atrybut group'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | atrybut group'))])));
        }
        
        $_atrybutTypParent = $atrybutTypParent->toArray();
        if ($this->AtrybutTypParent->hasBehavior('Translate') && empty($_atrybutTypParent['_translations'])) {
            $transFields=$this->AtrybutTypParent->associations()->keys();
            $tFields=[];
            foreach ($transFields as $field){
                if(strpos($field, '_translation')!==false){
                    $field= substr($field, (strpos($field, '_')+1));
                    $field=str_replace('_translation','', $field);
                    $tFields[$field]=$_atrybutTypParent[$field]; 
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $atrybutTypParent->set('_translations',$translation);
        }
        
        
        $this->set(compact('atrybutTypParent'));
        $this->set('_serialize', ['atrybutTypParent']);
    }
    /**
     * Delete method
     *
     * @param string|null $id Atrybut Typ id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $atrybutTyp = $this->AtrybutTyp->get($id);
        if ($this->AtrybutTyp->delete($atrybutTyp)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.',[$this->Txt->printAdmin(__('Admin | atrybut typ'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.',[$this->Txt->printAdmin(__('Admin | atrybut typ'))])));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function groupDelete($id = null)
    {
        $atrybutTypParent = $this->AtrybutTypParent->get($id);
        if ($this->AtrybutTypParent->delete($atrybutTypParent)) {
            $this->Flash->success(__('Grupa została usunięta'));
        } else {
            $this->Flash->error(__('Błąd usuwania grupy'));
        }
        return $this->redirect(['action' => 'index']);
    }
    public function sort($field='kolejnosc'){
        $this->autoRender=false;
        $returnData=[];
        if($this->request->is('post')){
            $rqData=$this->request->getData();
            foreach ($rqData as $data){
                if(!empty($data['id'])){
                    $this->AtrybutTyp->updateAll($data, ['id'=>$data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
}
