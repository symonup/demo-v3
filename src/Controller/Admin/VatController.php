<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Vat Controller
 *
 * @property \App\Model\Table\VatTable $Vat
 *
 * @method \App\Model\Entity\Vat[] paginate($object = null, array $settings = [])
 */
class VatController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
    $cfgArr=[];
$vat = $this->Vat->find('all',$cfgArr);

        $this->set(compact('vat'));
        $this->set('_serialize', ['vat']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $vat = $this->Vat->newEntity();
        if ($this->request->is('post')) {
        $rqData=$this->request->getData();
            $vat = $this->Vat->patchEntity($vat, $rqData,['translations'=>$this->Vat->hasBehavior('Translate')]);
            if ($this->Vat->save($vat)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | vat'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | vat'))])));
        }
        $this->set(compact('vat'));
        $this->set('_serialize', ['vat']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Vat id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
    
    if($this->Vat->hasBehavior('Translate')){
        $vat = $this->Vat->find('translations', [
            'locales' => $this->lngIdList,
            'conditions' => ['Vat.'.$this->Vat->primaryKey() => $id],
            'contain' => []
        ])->first();
        }
        else{
        $vat = $this->Vat->find('all', [
            'conditions'=>['Vat.'.$this->Vat->primaryKey()=>$id],
            'contain' => []
        ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData=$this->request->getData();
            $vat = $this->Vat->patchEntity($vat, $rqData,['translations'=>$this->Vat->hasBehavior('Translate')]);
            if ($this->Vat->save($vat)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | vat'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | vat'))])));
        }
        
        $_vat = $vat->toArray();
        if ($this->Vat->hasBehavior('Translate') && empty($_vat['_translations'])) {
            $transFields=$this->Vat->associations()->keys();
            $tFields=[];
            foreach ($transFields as $field){
                if(strpos($field, '_translation')!==false){
                    $field= substr($field, (strpos($field, '_')+1));
                    $field=str_replace('_translation','', $field);
                    $tFields[$field]=$_vat[$field]; 
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $vat->set('_translations',$translation);
        }
        
        
        $this->set(compact('vat'));
        $this->set('_serialize', ['vat']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Vat id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $vat = $this->Vat->get($id);
        if ($this->Vat->delete($vat)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.',[$this->Txt->printAdmin(__('Admin | vat'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.',[$this->Txt->printAdmin(__('Admin | vat'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

/**
     * setField method
     *
     * @param string|null $id Vat id.
     * @param string|null $field Vat field name.
     * @param mixed $value value to set.
     */

public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->Vat->find('all', ['conditions' => ['Vat.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                        $item->{$field} = $value;
                        if ($this->Vat->save($item)) {
                            $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, !(bool) $value])), 'action' => $retAction];
                        } else {
                            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                        }
                    
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    /**
* function sort
* 
*/
public function sort($field='kolejnosc'){
        $this->autoRender=false;
        $returnData=[];
        if($this->request->is('post')){
            $rqData=$this->request->getData();
            foreach ($rqData as $data){
                if(!empty($data['id'])){
                    $this->Vat->updateAll($data, ['id'=>$data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    }
