<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * FooterLink Controller
 *
 * @property \App\Model\Table\FooterLinkTable $FooterLink
 *
 * @method \App\Model\Entity\FooterLink[] paginate($object = null, array $settings = [])
 */
class FooterLinkController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $fGroups = [1 => $this->Txt->printAdmin(__('Admin | Grupa 1')), 2 => $this->Txt->printAdmin(__('Admin | Grupa 2'))];
        $this->set('fGroups', $fGroups);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Strona'],
            'order' => ['FooterLink.kolejnosc' => 'desc'],
            'limit' => 1000
        ];
        $footerLink = $this->paginate($this->FooterLink);

        $this->set(compact('footerLink'));
        $this->set('_serialize', ['footerLink']);
        $tmpView = new \Cake\View\View();
        $tips = [
            'stopka' => $tmpView->element($this->tipsPath . 'stopka')
        ];
        $this->set('tips', $tips);
    }

    /**
     * View method
     *
     * @param string|null $id Footer Link id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $footerLink = $this->FooterLink->get($id, [
            'contain' => []
        ]);

        $this->set('footerLink', $footerLink);
        $this->set('_serialize', ['footerLink']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $footerLink = $this->FooterLink->newEntity();
        if ($this->request->is('post')) {
            $footerLink = $this->FooterLink->patchEntity($footerLink, $this->request->getData());
            if ($this->FooterLink->save($footerLink)) {
                $this->Flash->success(__('The data link has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The data link could not be saved. Please, try again.'));
        }
        $this->set(compact('footerLink'));
        $this->set('_serialize', ['footerLink']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Footer Link id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $footerLink = $this->FooterLink->get($id, [
            'contain' => ['Strona']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $footerLink = $this->FooterLink->patchEntity($footerLink, $this->request->getData());
            if ($this->FooterLink->save($footerLink)) {
                $this->Flash->success(__('The data link has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The data link could not be saved. Please, try again.'));
        }
        $this->set(compact('footerLink'));
        $this->set('_serialize', ['footerLink']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Footer Link id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $footerLink = $this->FooterLink->get($id);
        $strona = null;
        if (!empty($footerLink->strona_id)) {
            $this->loadModel('Strona');
            $strona = $this->Strona->find('all')->where(['id' => $footerLink->strona_id])->first();
        }
        if ($this->FooterLink->delete($footerLink)) {
            $this->Flash->success(__('The data link has been deleted.'));
            if (!empty($strona)) {
                $strona->set('menu_dolne', 0);
                $this->Strona->save($strona);
            }
        } else {
            $this->Flash->error(__('The data link could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function sort($field = 'kolejnosc') {
        $this->autoRender = false;
        $returnData = [];
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            foreach ($rqData as $data) {
                if (!empty($data['id'])) {
                    $this->FooterLink->updateAll($data, ['id' => $data['id']]);
}
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

}
