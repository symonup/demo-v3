<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Rodo Controller
 *
 * @property \App\Model\Table\RodoTable $Rodo
 *
 * @method \App\Model\Entity\Rodo[] paginate($object = null, array $settings = [])
 */
class RodoController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $this->paginate = [
            'limit' => 100
        ];
        $rodo = $this->paginate($this->Rodo);

        $this->set(compact('rodo'));
        $this->set('_serialize', ['rodo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $rodo = $this->Rodo->newEntity();
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            $rqData['token'] = uniqid();
            $rodo = $this->Rodo->patchEntity($rodo, $rqData, ['translations' => $this->Rodo->hasBehavior('Translate')]);
            if ($this->Rodo->save($rodo)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | rodo'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | rodo'))])));
        }
        $this->set(compact('rodo'));
        $this->set('_serialize', ['rodo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Rodo id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {

        if ($this->Rodo->hasBehavior('Translate')) {
            $rodo = $this->Rodo->find('translations', [
                        'locales' => $this->lngIdList,
                        'conditions' => ['Rodo.' . $this->Rodo->primaryKey() => $id],
                        'contain' => []
                    ])->first();
        } else {
            $rodo = $this->Rodo->find('all', [
                        'conditions' => ['Rodo.' . $this->Rodo->primaryKey() => $id],
                        'contain' => []
                    ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData = $this->request->getData();
            $rodo = $this->Rodo->patchEntity($rodo, $rqData, ['translations' => $this->Rodo->hasBehavior('Translate')]);
            if ($this->Rodo->save($rodo)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | rodo'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | rodo'))])));
        }

        $_rodo = $rodo->toArray();
        if ($this->Rodo->hasBehavior('Translate') && empty($_rodo['_translations'])) {
            $transFields = $this->Rodo->associations()->keys();
            $tFields = [];
            foreach ($transFields as $field) {
                if (strpos($field, '_translation') !== false) {
                    $field = substr($field, (strpos($field, '_') + 1));
                    $field = str_replace('_translation', '', $field);
                    $tFields[$field] = $_rodo[$field];
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $rodo->set('_translations', $translation);
        }


        $this->set(compact('rodo'));
        $this->set('_serialize', ['rodo']);
    }

    public function import() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $dane = $this->request->getData();
            $allEmails = [];
            $notValid = [];
            $existMails=[];
            if (!empty($dane['plik']) && empty($dane['plik']['errors'])) {
                $row = 1;
                if (($handle = fopen($dane['plik']['tmp_name'], "r")) !== FALSE) {

                    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                        if ($this->Txt->validateEmail($data[0])) {
                            $exist = $this->Rodo->find('all')->where(['Rodo.email' => $data[0]])->first();
                            if (empty($exist)) {
                                $allEmails[$data[0]] = [
                                    'email' => $data[0],
                                    'token' => uniqid()
                                ];
                            }else{
                                $existMails[$row]=$data[0];
                            }
                        }else{
                            $notValid[$row]=$data[0];
                        }
                        $row++;
                    }
                    fclose($handle);
                }
                if (!empty($allEmails)) {
                    $all = count($allEmails);
                    $allEmails = $this->Rodo->newEntities($allEmails);
                    if ($this->Rodo->saveMany($allEmails)) {
                        $this->Flash->success($this->Txt->printAdmin(__('Admin | Dane zostały zapisane. Zapisano {0} pozycji.', [$all])));
                    } else {
                        $this->Flash->error($this->Txt->printAdmin(__('Admin | Błąd zapisu danych')));
                    }
                }else{
                    $this->Flash->success($this->Txt->printAdmin(__('Admin | Brak danych do importu.')));
                }
            }
            if(!empty($notValid)){
                        $this->Flash->error($this->Txt->printAdmin(__('Admin | Wykryto {0} nieprawidłowych adresów email. Nie zostały one zapisane do bazy danych',[count($notValid)])));
            }
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Rodo id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $rodo = $this->Rodo->get($id);
        if ($this->Rodo->delete($rodo)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.', [$this->Txt->printAdmin(__('Admin | rodo'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.', [$this->Txt->printAdmin(__('Admin | rodo'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * setField method
     *
     * @param string|null $id Rodo id.
     * @param string|null $field Rodo field name.
     * @param mixed $value value to set.
     */
    public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->Rodo->find('all', ['conditions' => ['Rodo.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                    $item->{$field} = $value;
                    if ($this->Rodo->save($item)) {
                        $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, !(bool) $value])), 'action' => $retAction];
                    } else {
                        $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                    }
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    /**
     * function sort
     * 
     */
    public function sort($field = 'kolejnosc') {
        $this->autoRender = false;
        $returnData = [];
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            foreach ($rqData as $data) {
                if (!empty($data['id'])) {
                    $this->Rodo->updateAll($data, ['id' => $data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

}
