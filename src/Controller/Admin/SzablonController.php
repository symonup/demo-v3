<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Szablon Controller
 *
 * @property \App\Model\Table\SzablonTable $Szablon
 *
 * @method \App\Model\Entity\Szablon[] paginate($object = null, array $settings = [])
 */
class SzablonController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
    $cfgArr=[];
$szablon = $this->Szablon->find('all',$cfgArr);

        $this->set(compact('szablon'));
        $this->set('_serialize', ['szablon']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $szablon = $this->Szablon->newEntity();
        if ($this->request->is('post')) {
        $rqData=$this->request->getData();
            $szablon = $this->Szablon->patchEntity($szablon, $rqData,['translations'=>$this->Szablon->hasBehavior('Translate')]);
            if ($this->Szablon->save($szablon)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | szablon'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | szablon'))])));
        }
        $this->set(compact('szablon'));
        $this->set('_serialize', ['szablon']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Szablon id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
    
    if($this->Szablon->hasBehavior('Translate')){
        $szablon = $this->Szablon->find('translations', [
            'locales' => $this->lngIdList,
            'conditions' => ['Szablon.'.$this->Szablon->primaryKey() => $id],
            'contain' => []
        ])->first();
        }
        else{
        $szablon = $this->Szablon->find('all', [
            'conditions'=>['Szablon.'.$this->Szablon->primaryKey()=>$id],
            'contain' => []
        ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData=$this->request->getData();
            $szablon = $this->Szablon->patchEntity($szablon, $rqData,['translations'=>$this->Szablon->hasBehavior('Translate')]);
            if ($this->Szablon->save($szablon)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | szablon'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | szablon'))])));
        }
        
        $_szablon = $szablon->toArray();
        if ($this->Szablon->hasBehavior('Translate') && empty($_szablon['_translations'])) {
            $transFields=$this->Szablon->associations()->keys();
            $tFields=[];
            foreach ($transFields as $field){
                if(strpos($field, '_translation')!==false){
                    $field= substr($field, (strpos($field, '_')+1));
                    $field=str_replace('_translation','', $field);
                    $tFields[$field]=$_szablon[$field]; 
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $szablon->set('_translations',$translation);
        }
        
        
        $this->set(compact('szablon'));
        $this->set('_serialize', ['szablon']);
    }

/**
     * setField method
     *
     * @param string|null $id Szablon id.
     * @param string|null $field Szablon field name.
     * @param mixed $value value to set.
     */

public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->Szablon->find('all', ['conditions' => ['Szablon.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                        $item->{$field} = $value;
                        if ($this->Szablon->save($item)) {
                            $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, !(bool) $value])), 'action' => $retAction];
                        } else {
                            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                        }
                    
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    /**
* function sort
* 
*/
public function sort($field='kolejnosc'){
        $this->autoRender=false;
        $returnData=[];
        if($this->request->is('post')){
            $rqData=$this->request->getData();
            foreach ($rqData as $data){
                if(!empty($data['id'])){
                    $this->Szablon->updateAll($data, ['id'=>$data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    }
