<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Waluta Controller
 *
 * @property \App\Model\Table\WalutaTable $Waluta
 *
 * @method \App\Model\Entity\Walutum[] paginate($object = null, array $settings = [])
 */
class WalutaController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
    $cfgArr=[];
$waluta = $this->Waluta->find('all',$cfgArr);

        $this->set(compact('waluta'));
        $this->set('_serialize', ['waluta']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $walutum = $this->Waluta->newEntity();
        if ($this->request->is('post')) {
        $rqData=$this->request->getData();
        $setDefault=false;
        if(!empty($rqData['domyslna'])){
            $setDefault=true;
        }
            $walutum = $this->Waluta->patchEntity($walutum, $rqData,['translations'=>$this->Waluta->hasBehavior('Translate')]);
            if ($this->Waluta->save($walutum)) {
                if($setDefault){
                    $this->Waluta->updateAll(['domyslna'=>0], ['id !='=>$walutum->id]);
                }
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | walutum'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | walutum'))])));
        }
        $symbole= \Cake\Core\Configure::read('waluty');
        
        $this->set(compact('walutum','symbole'));
        $this->set('_serialize', ['walutum']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Walutum id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
    
    if($this->Waluta->hasBehavior('Translate')){
        $walutum = $this->Waluta->find('translations', [
            'locales' => $this->lngIdList,
            'conditions' => ['Waluta.'.$this->Waluta->primaryKey() => $id],
            'contain' => []
        ])->first();
        }
        else{
        $walutum = $this->Waluta->find('all', [
            'conditions'=>['Waluta.'.$this->Waluta->primaryKey()=>$id],
            'contain' => []
        ])->first();
        }
        $oldDefault=$walutum->domyslna;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData=$this->request->getData();
         
        $setDefault=false;
        if(empty($oldDefault) && !empty($rqData['domyslna'])){
            $setDefault=true;
        }
                $walutum = $this->Waluta->patchEntity($walutum, $rqData,['translations'=>$this->Waluta->hasBehavior('Translate')]);
            if ($this->Waluta->save($walutum)) {
                if($setDefault){
                    $this->Waluta->updateAll(['domyslna'=>0], ['id !='=>$walutum->id]);
                }
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.',[$this->Txt->printAdmin(__('Admin | walutum'))])));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.',[$this->Txt->printAdmin(__('Admin | walutum'))])));
        }
        
        $_walutum = $walutum->toArray();
        if ($this->Waluta->hasBehavior('Translate') && empty($_walutum['_translations'])) {
            $transFields=$this->Waluta->associations()->keys();
            $tFields=[];
            foreach ($transFields as $field){
                if(strpos($field, '_translation')!==false){
                    $field= substr($field, (strpos($field, '_')+1));
                    $field=str_replace('_translation','', $field);
                    $tFields[$field]=$_walutum[$field]; 
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $walutum->set('_translations',$translation);
        }
        $symbole= \Cake\Core\Configure::read('waluty');
        
        $this->set(compact('walutum','symbole'));
        $this->set('_serialize', ['walutum']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Walutum id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $walutum = $this->Waluta->get($id);
        if ($this->Waluta->delete($walutum)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.',[$this->Txt->printAdmin(__('Admin | walutum'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.',[$this->Txt->printAdmin(__('Admin | walutum'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

/**
     * setField method
     *
     * @param string|null $id Walutum id.
     * @param string|null $field Walutum field name.
     * @param mixed $value value to set.
     */

public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->Waluta->find('all', ['conditions' => ['Waluta.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                        $item->{$field} = $value;
                        if ($this->Waluta->save($item)) {
                            $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool($value, \Cake\Routing\Router::url(['action' => 'setField', $id, $field, !(bool) $value])), 'action' => $retAction];
                        } else {
                            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                        }
                    
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    /**
* function sort
* 
*/
public function sort($field='kolejnosc'){
        $this->autoRender=false;
        $returnData=[];
        if($this->request->is('post')){
            $rqData=$this->request->getData();
            foreach ($rqData as $data){
                if(!empty($data['id'])){
                    $this->Waluta->updateAll($data, ['id'=>$data['id']]);
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    
    }
