<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Cache\Cache;
/**
 * Producent Controller
 *
 * @property \App\Model\Table\ProducentTable $Producent
 *
 * @method \App\Model\Entity\Producent[] paginate($object = null, array $settings = [])
 */
class ProducentController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->loadComponent('Upload');
        $this->set('hurtownie',$this->hurtownie);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $cfgArr = [];
        $producent = $this->Producent->find('all', $cfgArr);

        $this->set(compact('producent'));
        $this->set('_serialize', ['producent']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $producent = $this->Producent->newEntity();
        if ($this->request->is('post')) {
            $path = $this->filePath['producent'];
            $data = $this->request->getData();
            $fileName = null;
            if (!empty($data['logo']['name'])) {
                $fileName = $this->Upload->saveFile(
                        $path, $data['logo']);
            }
            $data['logo'] = $fileName;
            $data['kolejnosc'] = 0;
            $producent = $this->Producent->patchEntity($producent, $data, ['translations' => $this->Producent->hasBehavior('Translate')]);
            if ($this->Producent->save($producent)) {
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | producent'))])));
                return $this->redirect(['action' => 'index']);
            } else {
                if (!empty($fileName)) {
                    $this->Upload->deleteFile($path . $fileName);
                }
                $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | producent'))])));
            }
        }
        $this->set(compact('producent'));
        $this->set('_serialize', ['producent']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Producent id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        if ($this->Producent->hasBehavior('Translate')) {
            $producent = $this->Producent->find('translations', [
                        'locales' => $this->lngIdList,
                        'conditions' => ['Producent.' . $this->Producent->primaryKey() => $id],
                        'contain' => []
                    ])->first();
        } else {
            $producent = $this->Producent->find('all', [
                        'conditions' => ['Producent.' . $this->Producent->primaryKey() => $id],
                        'contain' => []
                    ])->first();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $oldFile = null;
            $data = $this->request->getData();
            if (empty($data['logo']['name'])) {
                $data['logo'] = $producent->logo;
            } else {
                $path = $this->filePath['producent'];
                $fileName = $this->Upload->saveFile(
                        $path, $data['logo']);
                if (!empty($fileName)) {
                    $data['logo'] = $fileName;
                    $oldFile = $producent->logo;
                }
            }
            $producent = $this->Producent->patchEntity($producent, $data, ['translations' => $this->Producent->hasBehavior('Translate')]);
            if ($this->Producent->save($producent)) {
                $this->clear_cache();
                if (!empty($oldFile)) {
                    $this->Upload->deleteFile($path . $oldFile);
                }
                $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been saved.', [$this->Txt->printAdmin(__('Admin | producent'))])));

                return $this->redirect(['action' => 'index']);
            } else {
                if (!empty($fileName) && ($oldFile != $fileName)) {
                    $this->Upload->deleteFile($path . $fileName);
                }
                $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be saved. Please, try again.', [$this->Txt->printAdmin(__('Admin | producent'))])));
            }
        }
        $_producent = $producent->toArray();
        if ($this->Producent->hasBehavior('Translate') && empty($_producent['_translations'])) {
            $transFields = $this->Producent->associations()->keys();
            $tFields = [];
            foreach ($transFields as $field) {
                if (strpos($field, '_translation') !== false) {
                    $field = substr($field, (strpos($field, '_') + 1));
                    $field = str_replace('_translation', '', $field);
                    $tFields[$field] = $_producent[$field];
                }
            }
            $translation = [$this->defLngSymbol => $tFields];
            $producent->set('_translations', $translation);
        }


        $this->set(compact('producent'));
        $this->set('_serialize', ['producent']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Producent id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $producent = $this->Producent->get($id);
        if ($this->Producent->delete($producent)) {
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.', [$this->Txt->printAdmin(__('Admin | producent'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.', [$this->Txt->printAdmin(__('Admin | producent'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

    private function clear_cache($typ = null) {
        foreach ($this->locales as $lc => $lId) {
            if (empty($typ)) {
                Cache::delete($lc . '_producenci_main', 'cfg');
                Cache::delete($lc . '_producenci_na_zamowienie', 'cfg');
                Cache::delete($lc . '_producenci_all', 'cfg');
            } else {
                switch ($typ) {
                    case 'producenci_main' : Cache::delete($lc . '_producenci_main', 'cfg');
                        break;
                    case 'producenci_na_zamowienie' : Cache::delete($lc . '_producenci_na_zamowienie', 'cfg');
                        break;
                    case 'producenci_all' : Cache::delete($lc . '_producenci_all', 'cfg');
                        break;
                }
            }
        }
    }

}
