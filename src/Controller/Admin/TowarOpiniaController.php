<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * TowarOpinia Controller
 *
 * @property \App\Model\Table\TowarOpiniaTable $TowarOpinia
 *
 * @method \App\Model\Entity\TowarOpinium[] paginate($object = null, array $settings = [])
 */
class TowarOpiniaController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $cfgArr = [];
        $cfgArr = [
            'contain' => ['Towar']
        ];
        $towarOpinia = $this->TowarOpinia->find('all', $cfgArr);

        $this->set(compact('towarOpinia'));
        $this->set('_serialize', ['towarOpinia']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Towar Opinium id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $towarOpinium = $this->TowarOpinia->get($id);
        $towarId=$towarOpinium->towar_id;
        if ($this->TowarOpinia->delete($towarOpinium)) {
            $towarOpinie = $this->TowarOpinia->find('all', ['conditions' => ['TowarOpinia.towar_id' => $towarId, 'TowarOpinia.status' => 2]]);
                        $opinie = $towarOpinie->select(['TowarOpinia.ocena', 'sum' => $towarOpinie->func()->sum('TowarOpinia.ocena'), 'count' => $towarOpinie->func()->count('TowarOpinia.ocena')])->toArray();
                        $suma = (int) $opinie[0]->sum;
                        $count = (int) $opinie[0]->count;
                        if ($count <= 0) {
                            $avg = 0;
                        } else {
                            $avg = ceil($suma / $count);
                        }
                        $towar = $this->TowarOpinia->Towar->find('all', ['conditions' => ['Towar.id' => $towarId]])->first();
                        $towar->opinia_avg = $avg;
                        $this->TowarOpinia->Towar->save($towar);
            $this->Flash->success($this->Txt->printAdmin(__('Admin | The {0} has been deleted.', [$this->Txt->printAdmin(__('Admin | towar opinium'))])));
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | The {0} could not be deleted. Please, try again.', [$this->Txt->printAdmin(__('Admin | towar opinium'))])));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * setField method
     *
     * @param string|null $id Towar Opinium id.
     * @param string|null $field Towar Opinium field name.
     * @param mixed $value value to set.
     */
    public function setField($id = null, $field = null, $value = null) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($id) && !empty($field)) {
            $item = $this->TowarOpinia->find('all', ['conditions' => ['TowarOpinia.id' => $id]])->first();
            $retAction = '';
            if (!empty($item)) {
                if (key_exists($field, $item->toArray())) {
                    $item->{$field} = $value;
                    if ($this->TowarOpinia->save($item)) {
                        $towarOpinie = $this->TowarOpinia->find('all', ['conditions' => ['TowarOpinia.towar_id' => $item->towar_id, 'TowarOpinia.status' => 2]]);
                        $opinie = $towarOpinie->select(['TowarOpinia.ocena', 'sum' => $towarOpinie->func()->sum('TowarOpinia.ocena'), 'count' => $towarOpinie->func()->count('TowarOpinia.ocena')])->toArray();
                        $suma = (int) $opinie[0]->sum;
                        $count = (int) $opinie[0]->count;
                        if ($count <= 0) {
                            $avg = 0;
                        } else {
                            $avg = ceil($suma / $count);
                        }
                        $towar = $this->TowarOpinia->Towar->find('all', ['conditions' => ['Towar.id' => $item->towar_id]])->first();
                        $towar->opinia_avg = $avg;
                        $this->TowarOpinia->Towar->save($towar);
                        $returnData = ['status' => 'success', 'message' => $this->Txt->printAdmin(__('Admin | Dane zostały zaktualizowane')), 'field' => $field, 'value' => $value, 'link' => $this->Txt->printBool((($value == 2) ? 1 : 0), \Cake\Routing\Router::url(['action' => 'setField', $id, $field, (($value == 2) ? 1 : 2)])), 'action' => $retAction];
                    } else {
                        $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Błąd zapisu danych')), 'errors' => $item->errors()];
                    }
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Brak komórki do aktualizacji'))];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nie znaleziono elementu do aktualizacji'))];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Txt->printAdmin(__('Admin | Nieprawidłowa wartość parametru'))];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    public function przelicz() {
        $this->autoRender = false;
        $towarOpinie = $this->TowarOpinia->find('all', ['conditions' => ['TowarOpinia.status' => 2]]);
        $opinie = $towarOpinie->select(['TowarOpinia.towar_id', 'sum' => $towarOpinie->func()->sum('TowarOpinia.ocena'), 'count' => $towarOpinie->func()->count('TowarOpinia.ocena')])->group(['TowarOpinia.towar_id'])->toArray();
        foreach ($opinie as $opinia) {
            $suma = (int) $opinia->sum;
            $count = (int) $opinia->count;
            $avg = round($suma / $count);
            $towar = $this->TowarOpinia->Towar->find('all', ['conditions' => ['Towar.id' => $opinia->towar_id]])->first();
            $towar->opinia_avg = $avg;
            $this->TowarOpinia->Towar->save($towar);
        }
    }

}
