<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Controller\Component\AuthComponent;
use Cake\I18n\I18n;
use Cake\Core\Configure;
use Cake\I18n\Number;
use GeoIP;
use geoiprecord;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $styleShets = [];
    public $scripts = [
    ];
    public $headScripts = [
        '/layout-admin/vendors/jquery/dist/jquery.min.js',
    ];
    public $blockedIp = [];
    public $session;
    public $components = ['Translation'];
    public $helpers = ['Translation'];
    public $Txt;
    public $fullMonths = [];
    public $active_jezyk;
    public $statusy = [];
    public $locales = [];
    public $defPageLocale = [];
    public $defLngSymbol;
    public $filePath = [];
    public $displayPath = [];
    public $currencyOptions = [];
    public $userRabat = [];
    public $userId = null;
    public $allCountry = [];
    public $defCurrency = [];
    public $basePath = '/';
    public $cuntryPhones = [];
    public $seoData = [];
    public $off = true;
    public $iframe;
    public $tipsPath = 'admin/tips';
    public $footerOsiagniecia = true;
    public $menuTypes = [];
    public $singleSite = false;
    public $forceHideLeft = false;
    public $cenyRodzaj = 'brutto';
    public $requireAllFunctions = true;
    public $mobile;
    public $moreStyle;
    public $hurtownie = [
        'stary_sklep' => 'Stary sklep'
    ];
    public $allCats = [];
    public $wojewodztwa = [
        'DOLNOSLASKIE' => 'DOLNOŚLĄSKIE',
        'KUJAWSKO_POMORSKIE' => 'KUJAWSKO POMORSKIE',
        'LUBELSKIE' => 'LUBELSKIE',
        'LUBUSKIE' => 'LUBUSKIE',
        'LODZKIE' => 'ŁÓDZKIE',
        'MALOPOLSKIE' => 'MAŁOPOLSKIE',
        'MAZOWIECKIE' => 'MAZOWIECKIE',
        'OPOLSKIE' => 'OPOLSKIE',
        'PODKARPACKIE' => 'PODKARPACKIE',
        'PODLASKIE' => 'PODLASKIE',
        'POMORSKIE' => 'POMORSKIE',
        'SLASKIE' => 'ŚLASKIE',
        'SWIETOKRZYSKIE' => 'ŚWIĘTOKRZYSKIE',
        'WARMINSKO_MAZURSKIE' => 'WARMINSKO MAZURSKIE',
        'WIELKOPOLSKIE' => 'WIELKOPOLSKIE',
        'ZACHODNIOPOMORSKIE' => 'ZACHODNIOPOMORSKIE'
    ];
    public $stats = true;

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();
        if (!empty($this->blockedIp) && in_array($this->request->clientIp(), $this->blockedIp)) {
            die('IP ' . $this->request->clientIp() . ' is blocked due to illegal activities, if you are real person send email to administrator');
        }
        define('NO_PHOTO', '<i class="fa fa-camera fa-2x"></i>');
        $this->Txt = new \App\View\Helper\TxtHelper(new \Cake\View\View());
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Translation');

        $this->loadModel('Jezyk');
        $this->loadModel('Waluta');
        $this->defPageLocale = $this->Jezyk->find('all', ['conditions' => ['Jezyk.domyslny' => 1]])->first()->locale;
        $this->loadModel('Konfiguracja');
        if (empty($this->UzytkownikKoszyk)) {
            $this->loadModel('UzytkownikKoszyk');
        }
        $timeZ = Configure::read('data.strefa_czasowa');
        if (!empty($timeZ)) {
            date_default_timezone_set($timeZ);
        } else {
            date_default_timezone_set('Europe/Warsaw');
        }
        $waluty = $this->Waluta->find('all');
        foreach ($waluty as $walutaItem) {
            if ($walutaItem->domyslna == 1) {
                $this->defCurrency = ['id' => $walutaItem->id, 'symbol' => $walutaItem->symbol];
            }
            $this->currencyOptions[$walutaItem->symbol] = [
                'symbol' => $walutaItem->symbol,
                'after' => !$walutaItem->symbol_przed,
                'space' => $walutaItem->space,
                'decimal' => $walutaItem->separator_d,
                'thousend' => $walutaItem->separator_t
            ];
        }
        Configure::write('currencyOptions', $this->currencyOptions);
        $this->fullMonths = [
            1 => $this->Txt->printAdmin(__('Admin | Styczen')),
            2 => $this->Txt->printAdmin(__('Admin | Luty')),
            3 => $this->Txt->printAdmin(__('Admin | Marzec')),
            4 => $this->Txt->printAdmin(__('Admin | Kwiecien')),
            5 => $this->Txt->printAdmin(__('Admin | Maj')),
            6 => $this->Txt->printAdmin(__('Admin | Czerwiec')),
            7 => $this->Txt->printAdmin(__('Admin | Lipiec')),
            8 => $this->Txt->printAdmin(__('Admin | Sierpien')),
            9 => $this->Txt->printAdmin(__('Admin | Wrzesien')),
            10 => $this->Txt->printAdmin(__('Admin | Pazdziernik')),
            11 => $this->Txt->printAdmin(__('Admin | Listopad')),
            12 => $this->Txt->printAdmin(__('Admin | Grudzien')),
        ];
        if (!empty($this->request->params['prefix']) && $this->request->params['prefix'] == 'admin') {
            $this->loadComponent('Auth', [
                'authenticate' => [
                    AuthComponent::ALL => ['userModel' => 'Administrator'],
                    'Basic' => ['userModel' => 'Administrator'],
                    'Form' => [
                        'fields' => ['userModel' => 'Administrator', 'username' => 'login', 'password' => 'password']
                    ]
                ],
                'authorize' => [
                    'Controller'
                ],
                'loginAction' => [
                    'plugin' => false,
                    'controller' => 'Index',
                    'action' => 'login'
                ],
                'loginRedirect' => [
                    'plugin' => false,
                    'controller' => 'Index',
                    'action' => 'index'
                ],
                'logoutRedirect' => [
                    'plugin' => false,
                    'controller' => 'Index',
                    'action' => 'login'
                ],
                'unauthorizedRedirect' => $this->request->referer(),
                'authError' => $this->Txt->printAdmin(__('Admin | Brak uprawnień')),
                'flash' => [
                    'element' => 'error'
                ]
            ]);
            $this->Auth->sessionKey = 'Auth.Admin';
        } else {
//            $this->loadComponent('Cookie', ['path' => '/', 'expires' => '+10 days']);
            $this->loadComponent('Auth', [
                'authorize' => 'Controller',
                'authenticate' => [
                    'Form' => [
                        'userModel' => 'Uzytkownik',
                        'fields' => [
                            'username' => 'email',
                            'password' => 'password'
                        ]
                    ]
                ],
                'loginAction' => [
                    'controller' => 'Home',
                    'action' => 'login'
                ],
                'unauthorizedRedirect' => $this->referer()
            ]);
            $this->Auth->sessionKey = 'Auth.User';
        }
        $this->session = $this->request->session();
        $this->userId = $this->Auth->user('id');
        if (!empty($this->userId)) {
            $this->userTyp = $this->Auth->user('typ');
        }

        $this->cenyRodzaj = Configure::read('ceny.rodzaj');
        Configure::write('cenyRodzaj', $this->cenyRodzaj);
        $this->allCountry = Configure::read('kraje');
        $this->mobile = $this->request->is('mobile');
        if (!empty($this->session)) {
            $checkForceDesktop = $this->session->read('forceDesktop');
            if (!empty($checkForceDesktop)) {
                $this->mobile = false;
            } elseif (!$this->mobile) {
                $checkMobile = $this->session->read('forceMobile');
                if (!empty($checkMobile)) {
                    $this->mobile = true;
                }
            }
        }
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        ini_set('default_charset', 'utf-8');
        $this->statusy = [
            'zlozone' => $this->Txt->printAdmin(__('Statusy | Oczekuje'), 'Statusy | '),
            'oplacone' => $this->Txt->printAdmin(__('Statusy | Opłacone'), 'Statusy | '),
            'przyjete' => $this->Txt->printAdmin(__('Statusy | Przyjęte do realizacji'), 'Statusy | '),
            'wyslane' => $this->Txt->printAdmin(__('Statusy | Zlecone do wysyłki'), 'Statusy | '),
            'zrealizowane' => $this->Txt->printAdmin(__('Statusy | Zrealizowane'), 'Statusy | '),
            'anulowane' => $this->Txt->printAdmin(__('Statusy | Anulowane'), 'Statusy | '),
        ];

        $this->filePath = [
            'ikona' => ROOT . DS . 'webroot' . DS . 'files' . DS . 'ikony' . DS,
            'kategoria' => ROOT . DS . 'webroot' . DS . 'files' . DS . 'categories' . DS,
            'kategoria_ikony' => ROOT . DS . 'webroot' . DS . 'files' . DS . 'categories_icons' . DS,
            'gatunek' => ROOT . DS . 'webroot' . DS . 'files' . DS . 'gatunek' . DS,
            'kolor' => ROOT . DS . 'webroot' . DS . 'files' . DS . 'kolor' . DS,
            'producent' => ROOT . DS . 'webroot' . DS . 'files' . DS . 'producent' . DS,
            'platforma' => ROOT . DS . 'webroot' . DS . 'files' . DS . 'platforma' . DS,
            'towar' => ROOT . DS . 'webroot' . DS . 'files' . DS . 'images' . DS,
            'faktura' => ROOT . DS . 'webroot' . DS . 'files' . DS . 'fv' . DS,
            'baner' => ROOT . DS . 'webroot' . DS . 'files' . DS . 'baner' . DS,
            'pages' => ROOT . DS . 'src' . DS . 'Template' . DS . 'Element' . DS . 'pages' . DS,
            'bloki' => ROOT . DS . 'src' . DS . 'Template' . DS . 'Element' . DS . 'bloki' . DS,
            'banner_slides' => ROOT . DS . 'webroot' . DS . 'files' . DS . 'banner_slides' . DS,
            'banner_slides_elements' => ROOT . DS . 'webroot' . DS . 'files' . DS . 'banner_slides_elements' . DS,
            'porownywarki' => ROOT . DS . 'webroot' . DS . 'files' . DS . 'xml' . DS,
            'lang' => ROOT . DS . 'webroot' . DS . 'files' . DS . 'lng' . DS,
            'xls' => ROOT . DS . 'webroot' . DS . 'files' . DS . 'xls' . DS,
            'webroot' => ROOT . DS . 'webroot' . DS,
            'artykul' => ROOT . DS . 'webroot' . DS . 'files' . DS . 'artykul' . DS,
            'film' => ROOT . DS . 'webroot' . DS . 'files' . DS . 'film' . DS,
            'orders' => ROOT . DS . 'webroot' . DS . 'files' . DS . 'orders' . DS,
            'raporty' => ROOT . DS . 'webroot' . DS . 'files' . DS . 'raporty' . DS,
            'img' => ROOT . DS . 'webroot' . DS . 'img' . DS,
            'blog' => ROOT . DS . 'webroot' . DS . 'files' . DS . 'blog' . DS,
            'karta_podarunkowa' => ROOT . DS . 'webroot' . DS . 'files' . DS . 'karta_podarunkowa' . DS,
            'my_files' => ROOT . DS . 'webroot' . DS . 'files' . DS . 'my_files' . DS,
        ];
        $this->displayPath = [
            'ikona' => $this->basePath . 'files/ikony/',
            'kategoria' => $this->basePath . 'files/categories/',
            'kategoria_ikony' => $this->basePath . 'files/categories_icons/',
            'gatunek' => $this->basePath . 'files/gatunek/',
            'kolor' => $this->basePath . 'files/kolor/',
            'producent' => $this->basePath . 'files/producent/',
            'platforma' => $this->basePath . 'files/platforma/',
            'towar' => $this->basePath . 'files/images/',
            'faktura' => $this->basePath . 'files/fv/',
            'baner' => $this->basePath . 'files/baner/',
            'banner_slides' => $this->basePath . 'files/banner_slides/',
            'banner_slides_elements' => $this->basePath . 'files/banner_slides_elements/',
            'porownywarki' => $this->basePath . 'files/xml/',
            'lang' => $this->basePath . 'files/lng/',
            'xls' => $this->basePath . 'files/xls/',
            'webroot' => $this->basePath,
            'artykul' => $this->basePath . 'files/artykul/',
            'film' => $this->basePath . 'files/film/',
            'orders' => $this->basePath . 'files/orders/',
            'raporty' => $this->basePath . 'files/raporty/',
            'img' => $this->basePath . 'img/',
            'blog' => $this->basePath . 'files/blog/',
            'karta_podarunkowa' => $this->basePath . 'files/karta_podarunkowa/',
        ];
        if (!empty($this->request->params['language'])) {
            $this->basePath .= $this->request->params['language'] . '/';
        }

        Configure::write('filePath', $this->filePath);
        $this->set('filePath', $this->filePath);
        Configure::write('displayPath', $this->displayPath);
        $this->set('displayPath', $this->displayPath);
        if (!empty($this->request->params['prefix']) && $this->request->params['prefix'] === 'api') {
            
        } elseif (!empty($this->request->params['prefix']) && $this->request->params['prefix'] === 'admin') {

            I18n::locale('pl_PL');
            $this->tipsPath = 'admin/tips/' . I18n::locale() . '/';

            $this->iframe = $this->request->query('iframe');
            if (!empty($this->iframe)) {
                $this->viewBuilder()->layout('iframe');
            } else {
                $this->viewBuilder()->layout('admin');
            }
            $this->set('iframe', $this->iframe);
            $this->set('linkTarget', ['_blank' => $this->Txt->printAdmin(__('Admin | Nowa karta')), '_self' => $this->Txt->printAdmin(__('Admin | Aktualna strona'))]);
            $this->set('pageTypes', ['type_default' => $this->Txt->printAdmin(__('Admin | Domyślny')), 'type_home' => $this->Txt->printAdmin(__('Admin | Strona główna')), 'type_popup' => $this->Txt->printAdmin(__('Admin | PopUp'))]);

            $this->locales = $this->Jezyk->find('list', ['keyField' => 'locale', 'valueField' => 'nazwa', 'order' => ['domyslny' => 'desc']])->toArray();
            $this->defLngSymbol = $this->Jezyk->find('all', ['contain' => ['Waluta'], 'conditions' => ['Jezyk.domyslny' => 1]])->first()->locale;
            $this->set('languages', $this->locales);
            $this->set('_languages', $this->Jezyk->find('list', ['keyField' => 'id', 'valueField' => 'nazwa', 'order' => ['domyslny' => 'desc']])->toArray());
            $this->menuTypes = ['top' => $this->Txt->printAdmin(__('Admin | Menu w nagłówku')), 'page' => $this->Txt->printAdmin(__('Admin | Menu na stronach informacyjnych'))];
            $this->set('menuTypes', $this->menuTypes);
            $this->loadModel('Powiadomienia');
            $this->loadModel('Zapytanie');
            $this->loadModel('CallBack');
            $zapytaniaODostepnoscCount = $this->Powiadomienia->find('all')->where(['Powiadomienia.typ' => 'dostepnosc', 'Powiadomienia.wyslane' => 0])->count();
            $zapytaniaOPremieryCount = $this->Powiadomienia->find('all')->where(['Powiadomienia.typ' => 'zapytanie', 'Powiadomienia.wyslane' => 0])->count();
            $callBackCount = $this->CallBack->find('all')->where(['CallBack.status' => 0])->count();
//            $naZamowienieCount = $this->Zapytanie->find('all')->where(['Zapytanie.data_wyslania IS NULL'])->count();
            $zapytaniaCountAll = ($zapytaniaODostepnoscCount + $zapytaniaOPremieryCount + $callBackCount);
            $this->set(compact('zapytaniaODostepnoscCount', 'zapytaniaOPremieryCount', 'zapytaniaCountAll'));
            if (empty($this->HelpInfo)) {
                $this->loadModel('HelpInfo');
            }
            $helpInfoText = $this->HelpInfo->find('list', ['groupField' => 'type', 'keyField' => 'field', 'valueField' => 'info'])->toArray();
            Configure::write('helpInfoText', $helpInfoText);
        } else {
            $crtlAct = mb_strtolower($this->request->getParam('controller') . '_' . $this->request->getParam('action'));
            if ($crtlAct != 'cron_tests' && $crtlAct != 'home_index' && $crtlAct != 'cart_add' && $crtlAct != 'home_newsletter' && $crtlAct != 'home_currency' && $crtlAct != 'home_confirmcookie' && $crtlAct != 'towar_view' && $crtlAct != 'towar_index' && $crtlAct != 'towar_image') {
                //   die('Strona w przygotowaniu');
            }
            if ($crtlAct == 'towar_searchrq' || $crtlAct == 'sitemap_xml') {
                $this->requireAllFunctions = false;
            }
            $this->setLocale();
            if ($this->requireAllFunctions) {
                if (empty($this->Kategoria)) {
                    $this->loadModel('Kategoria');
                }
                $this->allCats = $this->Kategoria->getCategoryTree();
                $this->set('allCats', $this->allCats);
                $userAccount = $this->Auth->user();
                if (empty($userAccount)) {
                    if ($this->request->getCookie('rememberMe')) {
                        $rememberMeToken = $this->request->getCookie('rememberMe');
                        if (!empty($rememberMeToken)) {
                            if (empty($this->Uzytkownik)) {
                                $this->loadModel('Uzytkownik');
                            }
                            $userRemember = $this->Uzytkownik->find('all', [])->where(['Uzytkownik.remember_token' => $rememberMeToken])->first();
                            if (!empty($userRemember)) {
                                $userAccount = $userRemember->toArray();
                                $this->Auth->setUser($userAccount);
                                $this->writeCookie('rememberMe', $userRemember->remember_token);
                            } else {
                                $this->writeCookie('rememberMe', null, '- 1 day');
                            }
                        }
                    }
                }
                $this->loadModel('Menu');
                $menu = $this->Menu->find('all', ['conditions' => ['Menu.parent_id IS' => NULL, 'Menu.typ' => 'top', '(Menu.jezyk_id = ' . $this->active_jezyk->id . ' OR Menu.jezyk_id IS NULL)', 'OR' => ['Menu.ukryty' => 0, 'Menu.ukryty IS' => NULL]], 'order' => ['Menu.kolejnosc' => 'desc'], 'contain' => ['ChildMenu' => ['conditions' => ['ChildMenu.ukryty' => 0], 'sort' => ['ChildMenu.kolejnosc' => 'desc']]]]);
                $this->set('menu', $menu);
                $this->loadModel('Strona');
                $pagesFooterLinks = $this->Strona->find('all', ['fields' => ['id', 'nazwa', 'type', 'seo_tytul'], 'conditions' => ['Strona.ukryta' => 0, 'Strona.menu_dolne' => 1]]);
                $this->set('pagesFooterLinks', $pagesFooterLinks);
                $this->loadModel('FooterLink');
                $footerLinks = $this->FooterLink->find('all', ['order' => ['FooterLink.kolejnosc' => 'desc', 'FooterLink.id' => 'asc'], 'contain' => ['Strona']]);
                $this->set('footerLinks', $footerLinks);
                $this->loadModel('Banner');
                $this->loadModel('Blok');
                $bloki = $this->Blok->find('all');
                $bannersAll = $this->Banner->find('all', ['contain' => ['BannerSlides' => ['BannerSlidesElements']]]);
                $banners = [];
                $blocks = [];
                $kontaktFormData = $this->session->read('kontaktForm');
                $this->set('kontaktFormData', $kontaktFormData);
                $this->session->delete('kontaktForm');
                $tmpView = new \Cake\View\View();
                $tmpView->Translation->setLocale(\Cake\I18n\I18n::locale());
                $tmpView->set('kontaktFormData', $kontaktFormData);
                $tmpView->set('mobile', $this->mobile);
                $aktywneKody = $this->session->read('cartCode');
                $tmpView->set('aktywneKody', $aktywneKody);
                $this->loadModel('KartaPodarunkowa');
                $kartyPodarunkowe = $this->KartaPodarunkowa->find()->where(['KartaPodarunkowa.aktywna' => 1])->order(['KartaPodarunkowa.wartosc' => 'asc']);
                $this->loadModel('KodyRabatowe');
                $kodyRabatowe = $this->KodyRabatowe->find()->where(['KodyRabatowe.zablokuj' => 0, 'KodyRabatowe.data_end >=' => $this->Txt->printDate(new \DateTime(), 'Y-m-d H:i:s'), 'KodyRabatowe.data_start <=' => $this->Txt->printDate(new \DateTime(), 'Y-m-d H:i:s')])->order(['KodyRabatowe.data_end' => 'asc']);
                $tmpView->set('displayPath', $this->displayPath);
//        $tmpView->set('kartyPodarunkowe', $kartyPodarunkowe);
                foreach ($bloki as $blok) {
                    if (!empty($blok->ukryty)) {
                        $blocks['[%-_blok_' . $blok->id . '_-%]'] = '';
                    } else {
                        $blocks['[%-_blok_' . $blok->id . '_-%]'] = $tmpView->element('bloki/' . $blok->locale . '/blok_' . $blok->id, ['referer' => $this->request->referer()]);
                    }
                }
                foreach ($bannersAll as $bannerItem) {
                    $banners[$bannerItem->id] = $bannerItem;
                    $blocks['[%-_banner_' . $bannerItem->id . '_-%]'] = $tmpView->element('default/banner', ['banner' => $bannerItem, 'referer' => $this->request->referer()]);
                }
                $blocks['[%-_karta-kod-form_-%]'] = $tmpView->element('default/karta_kod_form');
                $blocks['[%-_karty-podarunkowe-lista_-%]'] = $tmpView->element('default/karty_podarunkowe_lista', ['kartyPodarunkowe' => $kartyPodarunkowe]);
                $blocks['[%-_kontakt-form_-%]'] = $tmpView->element('default/kontakt_form', ['referer' => $this->request->referer()]);
                $blocks['[%-_lista-kodow_-%]'] = $tmpView->element('default/lista_kodow', ['referer' => $this->request->referer(), 'kodyRabatowe' => $kodyRabatowe]);
                $blocks['[%-_kontakt-form-2_-%]'] = $tmpView->element('default/kontakt_form_2', ['referer' => $this->request->referer()]);
                $blocks['[%-_newsletter-form_-%]'] = $tmpView->element('default/newsletter_form', ['referer' => $this->request->referer()]);
                $blocks['[%-_kategorie_-%]'] = $tmpView->element('default/kategorie', ['allCats' => $this->allCats, 'referer' => $this->request->referer()]);
                $tmpView->Translation->afterLayout();
                $this->set('banners', $banners);
                $this->set('blocks', $blocks);
                if (empty($this->Jezyk)) {
                    $this->loadModel('Jezyk');
                }
                $languages = $this->Jezyk->find('list', ['keyField' => 'symbol', 'valueField' => 'nazwa', 'order' => ['domyslny' => 'desc']])->where(['aktywny' => 1])->toArray();
                $this->set('languages', $languages);
                if (empty($this->Platforma)) {
                    $this->loadModel('Platforma');
                }
                $preorderPlatforms = $this->Platforma->find('list', ['keyField' => 'id', 'valueField' => 'nazwa'])->where(['preorder' => 1])->toArray();
                Configure::write('preorderPlatforms', $preorderPlatforms);
                if (empty($this->Towar)) {
                    $this->loadModel('Towar');
                }
                $hotDeal = $this->Towar->HotDeal->find('all', ['contain' => ['Towar']])->where(['Towar.ukryty' => 0, 'HotDeal.ilosc >' => 0, 'HotDeal.aktywna' => 1, 'HotDeal.data_do >=' => date('Y-m-d H:i:s'), '(HotDeal.data_od IS NULL OR HotDeal.data_od <= \'' . date('Y-m-d H:i:s') . '\')'])->order(['HotDeal.data_do' => 'asc'])->first();
                $hotDealLink = null;
                if (!empty($hotDeal)) {
                    $hotDealLink = $this->Txt->towarViewUrl($hotDeal->towar->toArray());
                }
                $this->set('hotDealLink', $hotDealLink);
                $this->loadModel('Wiek');
                $wiek = $this->Wiek->find('list', ['keyField' => 'id', 'valueField' => 'nazwa'])->order(['kolejnosc' => 'desc', 'id' => 'asc'])->toArray();

                $this->set('wiek', $wiek);
                $aktywneKody = $this->session->read('cartCode');
                Configure::write('aktywneKody', $aktywneKody);
                $this->set('aktywneKody', $aktywneKody);
            }
        }
    }

    protected function checkBrowser() {
        $browser = '';
        if (!empty($_SERVER) && key_exists('HTTP_USER_AGENT', $_SERVER)) {
            if (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident') !== false) {
                $browser = 'explorer';
            } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') !== false) {
                $browser = 'firefox';
            } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') !== false) {
                $browser = 'chrome';
            } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Safari') !== false) {
                $browser = 'safari';
            }
        }
        return $browser;
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Network\Response|null|void
     */
    public function beforeRender(Event $event) {
        if ($this->requireAllFunctions) {
            $browser = $this->checkBrowser();
            if (!empty($this->request->params['prefix']) && $this->request->params['prefix'] === 'api') {
                
            } elseif (!empty($this->request->params['prefix']) && $this->request->params['prefix'] === 'admin') {
                $this->styleShets[] = '/layout-admin/vendors/bootstrap/dist/css/bootstrap.min';
                $this->styleShets[] = '/layout-admin/vendors/font-awesome/css/font-awesome.min';
                $this->styleShets[] = '/layout-admin/vendors/jqvmap/dist/jqvmap.min.css';
                $this->styleShets[] = '/plugins/datetimepicker/jquery.datetimepicker.min.css';

                $this->styleShets[] = '/plugins/pnotify/dist/pnotify.css';
                $this->styleShets[] = '/plugins/pnotify/dist/pnotify.buttons.css';
                $this->styleShets[] = '/plugins/pnotify/dist/pnotify.nonblock.css';


                $this->styleShets[] = '/plugins/jquery-ui-1.12.1/jquery-ui.min.css';
                $this->styleShets[] = '/plugins/fullcalendar-3.4.0/fullcalendar.css';
                $this->styleShets[] = '/plugins/swetalert2/sweetalert2.min.css';
                $this->styleShets[] = '/plugins/phone/phone.css';
                $this->styleShets[] = '/layout-admin/vendors/select2/dist/css/select2.css';
                $this->styleShets[] = '/layout-admin/vendors/elFinder-2.1.28/css/elfinder.min.css';
                $this->styleShets[] = '/layout-admin/vendors/summernote/dist/summernote.css';
                $this->styleShets[] = '/layout-admin/vendors/kategorie/kategorie.css';
                $this->styleShets[] = '/layout-admin/vendors/atrybuty/atrybuty.css';
                $this->styleShets[] = '/layout-admin/vendors/custom/similar.css';

                $this->styleShets[] = '/layout-admin/build/css/custom.min.css';
                $this->styleShets[] = '/layout-admin/build/css/style.css?' . uniqid();

                $this->headScripts[] = '/plugins/jquery-ui-1.12.1/jquery-ui.min.js';
                $this->headScripts[] = '/layout-admin/vendors/bootstrap/dist/js/bootstrap.min.js';

                $this->headScripts[] = '/plugins/cropper-master/dist/cropper.min.js';
                $this->styleShets[] = '/plugins/cropper-master/dist/cropper.min.css';

                $this->headScripts[] = '/layout-admin/vendors/jqueryJcrop/jquery.Jcrop.min.js';

                $this->styleShets[] = '/layout-admin/vendors/CodeMirror-master/lib/codemirror.css';
                $this->scripts[] = '/layout-admin/vendors/CodeMirror-master/lib/codemirror.js';
                $this->scripts[] = '/layout-admin/vendors/CodeMirror-master/mode/xml/xml.js';
                $this->scripts[] = '/layout-admin/vendors/CodeMirror-master/mode/css/css.js';
                $this->scripts[] = '/layout-admin/vendors/CodeMirror-master/mode/htmlmixed/htmlmixed.js';
                $this->scripts[] = '/layout-admin/vendors/CodeMirror-master/mode/javascript/javascript.js';

                $this->styleShets[] = '/layout-admin/vendors/Jcrop/jquery.Jcrop.min.css';
                $this->scripts[] = '/layout-admin/vendors/Jcrop/jquery.Jcrop.min.js';

                $this->scripts[] = '/layout-admin/vendors/fastclick/lib/fastclick.js';
                $this->scripts[] = '/layout-admin/vendors/nprogress/nprogress.js';
                $this->scripts[] = '/layout-admin/vendors/Chart.js/dist/Chart.min.js';
                $this->scripts[] = '/layout-admin/vendors/gauge.js/dist/gauge.min.js';
                $this->scripts[] = '/layout-admin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js';
                $this->scripts[] = '/layout-admin/vendors/iCheck/icheck.min.js';
                $this->scripts[] = '/layout-admin/vendors/skycons/skycons.js';
                $this->scripts[] = '/layout-admin/vendors/Flot/jquery.flot.js';
                $this->scripts[] = '/layout-admin/vendors/Flot/jquery.flot.pie.js';
                $this->scripts[] = '/layout-admin/vendors/Flot/jquery.flot.time.js';
                $this->scripts[] = '/layout-admin/vendors/Flot/jquery.flot.stack.js';
                $this->scripts[] = '/layout-admin/vendors/Flot/jquery.flot.resize.js';
                $this->scripts[] = '/layout-admin/vendors/flot.orderbars/js/jquery.flot.orderBars.js';
                $this->scripts[] = '/layout-admin/vendors/flot-spline/js/jquery.flot.spline.min.js';
                $this->scripts[] = '/layout-admin/vendors/flot.curvedlines/curvedLines.js';
                $this->scripts[] = '/layout-admin/vendors/DateJS/build/date.js';
                $this->scripts[] = '/layout-admin/vendors/jqvmap/dist/jquery.vmap.js';
                $this->scripts[] = '/layout-admin/vendors/jqvmap/dist/maps/jquery.vmap.world.js';
                $this->scripts[] = '/layout-admin/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js';
                $this->scripts[] = '/layout-admin/vendors/moment/min/moment.min.js';
//            $this->scripts[] = '/layout-admin/vendors/moment/locale/pl.js';
                $this->scripts[] = '/plugins/datetimepicker/jquery.datetimepicker.full.min.js';
                $this->scripts[] = '/layout-admin/vendors/select2/dist/js/select2.full.min.js';

                $this->scripts[] = '/plugins/pnotify/dist/pnotify.js';
                $this->scripts[] = '/plugins/pnotify/dist/pnotify.buttons.js';
                $this->scripts[] = '/plugins/pnotify/dist/pnotify.nonblock.js';

                $this->scripts[] = '/plugins/tagsinput/bootstrap-tagsinput.min.js';
                $this->styleShets[] = '/plugins/tagsinput/bootstrap-tagsinput.css';

                $this->scripts[] = '/layout-admin/vendors/datatables.net/js/jquery.dataTables.min.js';
                $this->scripts[] = '/layout-admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js';
                $this->scripts[] = '/layout-admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js';
                $this->scripts[] = '/layout-admin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js';
                $this->scripts[] = '/layout-admin/vendors/datatables.net-buttons/js/buttons.flash.min.js';
                $this->scripts[] = '/layout-admin/vendors/datatables.net-buttons/js/buttons.html5.min.js';
                $this->scripts[] = '/layout-admin/vendors/datatables.net-buttons/js/buttons.print.min.js';
                $this->scripts[] = '/layout-admin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js';
                $this->scripts[] = '/layout-admin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js';
                $this->scripts[] = '/layout-admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js';
                $this->scripts[] = '/layout-admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js';
                $this->scripts[] = '/layout-admin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js';

                $this->scripts[] = '/plugins/fullcalendar-3.4.0/fullcalendar.js';
                $this->scripts[] = '/plugins/fullcalendar-3.4.0/locale/pl.js';
                $this->scripts[] = '/plugins/swetalert2/sweetalert2.min.js';
                $this->scripts[] = '/layout-admin/vendors/elFinder-2.1.28/js/elfinder.min.js';
                $this->scripts[] = '/layout-admin/vendors/elFinder-2.1.28/js/i18n/elfinder.pl.js';
                $this->scripts[] = '/layout-admin/vendors/summernote/dist/summernote.min.js';
                $this->scripts[] = '/layout-admin/vendors/summernote-ext-elfinder/summernote-ext-elfinder.js';
                $this->scripts[] = '/layout-admin/build/js/custom.js';
                $this->scripts[] = '/layout-admin/vendors/kategorie/kategorie.js';
                $this->scripts[] = '/layout-admin/vendors/atrybuty/atrybuty.js';
                $this->scripts[] = '/layout-admin/vendors/custom/similar.js';
                $this->scripts[] = '/plugins/phone/phone.js';
                $this->scripts[] = '/layout-admin/build/js/global.js?' . uniqid();
                $this->scripts[] = '/layout-admin/build/js/colorSchema.js?' . uniqid();
                if (strpos(mb_strtolower($this->request->params['controller']), 'allegro') !== false) {
                    $this->scripts[] = '/layout-admin/build/js/allegro.js?' . uniqid();
                    $this->styleShets[] = '/layout-admin/build/css/allegro.css?' . uniqid();
                }
                if (mb_strtolower($this->request->params['controller']) == 'zestaw') {
                    $this->scripts[] = '/layout-admin/build/js/zestaw.js?' . uniqid();
                    $this->styleShets[] = '/layout-admin/build/css/zestaw.css?' . uniqid();
                }
                if (mb_strtolower($this->request->params['controller']) == 'kategoria') {
                    $this->scripts[] = '/layout-admin/build/js/kategoria_promocja.js?' . uniqid();
                    $this->styleShets[] = '/layout-admin/build/css/kategoria_promocja.css?' . uniqid();
                }
                if (mb_strtolower($this->request->params['controller']) == 'banner') {
                    $this->scripts[] = '/layout-admin/build/js/banner.js?' . uniqid();
                    $this->styleShets[] = '/layout-admin/build/css/banner.css?' . uniqid();
                }
                if (mb_strtolower($this->request->params['controller']) == 'strona') {
                    $this->scripts[] = '/layout-admin/build/js/page-edit.js?' . uniqid();
                }
            } else {
                $this->styleShets[] = '/plugins/jquery-ui-1.12.1/jquery-ui.min.css';
                $this->styleShets[] = '/plugins/bootstrap/css/bootstrap.min.css';

                $this->styleShets[] = '/plugins/fullcalendar-3.4.0/fullcalendar.css';
//            $this->styleShets[] = '/plugins/fontawesome/web-fonts-with-css/css/fontawesome.min.css';
//                $this->styleShets[] = 'https://use.fontawesome.com/releases/v5.0.10/css/all.css';
                $this->styleShets[] = '/plugins/fontawsome5/css/all.min.css';
                $this->styleShets[] = '/plugins/flaticons/flaticon.css?5';
                $this->styleShets[] = '/plugins/flaticons-kategorie/flaticon.css';
                $this->styleShets[] = '/plugins/pnotify/dist/pnotify.css';
                $this->styleShets[] = '/plugins/pnotify/dist/pnotify.buttons.css';
                $this->styleShets[] = '/plugins/pnotify/dist/pnotify.nonblock.css';
                $this->styleShets[] = '/plugins/ion.rangeSlider/css/ion.rangeSlider.css';
                $this->styleShets[] = '/plugins/ion.rangeSlider/css/ion.rangeSlider.skinHTML5.css';
                $this->styleShets[] = '/plugins/swetalert2/sweetalert2.min.css';
                $this->styleShets[] = '/plugins/opinie/opinie.css';
                $this->styleShets[] = '/plugins/phone/phone.css';
                $this->styleShets[] = '/css/banner.css?' . uniqid();
                $this->styleShets[] = '/css/style.css?' . uniqid();
                $this->styleShets[] = '/css/custom.css?' . uniqid();

                $this->scripts[] = '/plugins/bootstrap/js/vendor/popper.min.js';
                $this->scripts[] = '/plugins/jquery-ui-1.12.1/jquery-ui.min.js';
                $this->scripts[] = '/plugins/bootstrap/js/bootstrap.min.js';
                $this->scripts[] = '/layout-admin/vendors/moment/min/moment-with-locales.min.js';
//                $this->scripts[] = '/plugins/fontawsome5/js/all.min.js';
                $this->scripts[] = '/plugins/pnotify/dist/pnotify.js';
                $this->scripts[] = '/plugins/pnotify/dist/pnotify.buttons.js';
                $this->scripts[] = '/plugins/pnotify/dist/pnotify.nonblock.js';
                $this->scripts[] = '/plugins/fullcalendar-3.4.0/fullcalendar.js';
                $this->scripts[] = '/plugins/fullcalendar-3.4.0/locale/pl.js';
                $this->scripts[] = '/plugins/ion.rangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js';
                $this->scripts[] = '/plugins/swetalert2/sweetalert2.min.js';
                $this->scripts[] = '/plugins/opinie/opinie.js';
                if (mb_strtolower($this->request->params['controller']) == 'towar' && (mb_strtolower($this->request->params['action']) == 'view' || mb_strtolower($this->request->params['action']) == 'zestawy')) {
                    $this->scripts[] = '/plugins/jquery-mousewheel-3.1.13/jquery.mousewheel.min';
                    $this->styleShets[] = '/css/zestaw.css?' . uniqid();
                }
                $this->scripts[] = '/plugins/lightbox2/dist/js/lightbox.min.js';
                $this->styleShets[] = '/plugins/lightbox2/dist/css/lightbox.min.css';

                $this->scripts[] = '/plugins/jquery-validation/jquery.validate.min.js';
                $this->scripts[] = '/plugins/jquery-validation/additional-methods.js?' . uniqid();
                $this->scripts[] = '/plugins/jquery-validation/localization/messages_pl.js';

//$this->scripts[] = '/plugins/mapa-polski/snap.svg-min.js';
//$this->scripts[] = '/plugins/mapa-polski/map.js';
//                $this->styleShets[] = '/plugins/mapa-polski/map.css';


                $this->styleShets[] = '/plugins/multiple-select-master/dist/multiple-select.min.css';
                $this->scripts[] = '/plugins/multiple-select-master/dist/multiple-select.min.js';

                $this->scripts[] = \Cake\Routing\Router::url(['controller' => 'Home', 'action' => 'currency', 'u' => 1]);
                if (mb_strtolower($this->request->params['controller']) == 'home' && mb_strtolower($this->request->params['action']) == 'currency') {
                    $this->stats = false;
                }

                if (mb_strtolower($this->request->params['controller']) == 'towar' && mb_strtolower($this->request->params['action']) == 'view') {
                    $this->scripts[] = '/js/hammer.min.js';
                    $this->styleShets[] = '/css/galeria.css?' . uniqid();
                    $this->scripts[] = '/js/galeria.js?' . uniqid();
                }
                $this->scripts[] = '/plugins/phone/phone.js';
                $this->scripts[] = '/js/global.js?' . uniqid();
                $this->scripts[] = '/js/cart.js?' . uniqid();
                if (mb_strtolower($this->request->params['controller']) == 'landingpage') {
                    $this->styleShets[] = '/css/landingpage.css?' . uniqid();
                }

                if (mb_strtolower($this->request->params['controller']) == 'cart' && mb_strtolower($this->request->params['action']) == 'index') {
                    $this->styleShets[] = 'https://geowidget.easypack24.net/css/easypack.css';
                    $this->scripts[] = 'https://geowidget.easypack24.net/js/sdk-for-javascript.js';
                }
            }

            if (!empty($this->request->params['prefix']) && $this->request->params['prefix'] === 'api') {
                
            } else if (!empty($this->request->params['prefix']) && $this->request->params['prefix'] === 'admin') {
                
            } else {
                if (empty($this->Kategoria)) {
                    $this->loadModel('Kategoria');
                }
                $towarCenaDefaultParams = ['Vat', 'Waluta', 'Jednostka', 'conditions' => ['TowarCenaDefault.waluta_id' => (!empty($this->active_jezyk) ? $this->active_jezyk->waluta_id : 1)]];
                $towarContain = ['HotDeal', 'Bonus', 'Wersja', 'Platforma', 'TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]], 'TowarCenaDefault' => $towarCenaDefaultParams];

                $mainCats = $this->Kategoria->find('all', ['contain' => ['Promocja' => $towarContain], 'conditions' => ['Kategoria.parent_id IS NULL', 'Kategoria.ukryta' => 0], 'order' => ['Kategoria.kolejnosc' => 'desc']]);
                $mainCatsDef = $this->Kategoria->find('all', ['conditions' => ['Kategoria.parent_id IS NULL', 'Kategoria.ukryta' => 0], 'order' => ['Kategoria.kolejnosc' => 'desc']]);
                $this->set('mainCats', $mainCats);
                $this->set('mainCatsDef', $mainCatsDef);
                if (empty($this->Producent)) {
                    $this->loadModel('Producent');
                }
                $allProducenci = $this->Producent->find('list', ['keyField' => 'nazwa', 'valueField' => 'logo', 'groupField' => 'id', 'conditions' => ['ukryty' => 0], 'order' => ['kolejnosc' => 'asc']])->toArray();
                $this->set('allProducenci', $allProducenci);
//            $this->session->write('cartItems',null);
                $cartItems = (!empty($this->session) ? $this->session->read('cartItems') : null);
                if (empty($cartItems) && !empty($this->userId)) {
                    if (empty($this->UzytkownikKoszyk)) {
                        $this->loadModel('UzytkownikKoszyk');
                    }
                    $savedCart = $this->UzytkownikKoszyk->find('all', ['conditions' => ['UzytkownikKoszyk.uzytkownik_id' => $this->userId, 'UzytkownikKoszyk.active' => 1]])->first();
                    if (!empty($savedCart)) {
                        if (!empty($savedCart['items'])) {
                            $savedItems = explode(',', $savedCart['items']);
                            $cartItemsById = [];
                            foreach ($savedItems as $savedItem) {
                                $item = explode(':', $savedItem);
                                if (empty($item[0]) || $item[0] == '0') {
                                    continue;
                                }
                                if (strpos($item[0], 'card') !== false) {
                                    $warianty[$item[0]] = $item[1];
                                    $itemTmp = explode('_', $item[0]);
                                    $itemId = $itemTmp[0];
                                    $cartItemsById[$itemId][$itemTmp[1]] = $item[1];
                                } else {
                                    $warianty[$item[0]] = $item[1];
                                    $itemTmp = explode('_', $item[0]);
                                    $itemId = $itemTmp[0];
                                    $cartItemsById[$itemId][$item[0]] = $item[1];
                                }
                            }
                            if (!empty($cartItemsById)) {
                                $itemsToAdd = [];
                                foreach ($cartItemsById as $itemId => $warianty) {
                                    if ($itemId == 'card') {
                                        if (empty($this->KartaPodarunkowa)) {
                                            $this->loadModel('KartaPodarunkowa');
                                        }
                                        foreach ($warianty as $kartaId => $kartaIlosc) {
                                            $item = $this->KartaPodarunkowa->find()->where(['KartaPodarunkowa.id' => $kartaId])->first();
                                            $itemsToAdd = $this->UzytkownikKoszyk->prepareCartBonus($item, $kartaIlosc);
                                            if (!empty($itemsToAdd)) {
                                                foreach ($itemsToAdd as $addKey => $addItem) {
                                                    $cartItems['items'][$addKey] = $addItem;
                                                }
                                            }
                                        }
                                    } else {
                                        $wariantyIds = [];
                                        if (!empty($warianty)) {
                                            foreach ($warianty as $key => $ilosc) {
                                                if (strpos($key, $itemId . '_') !== false) {
                                                    $wariantyIds[] = str_replace($itemId . '_', '', $key);
                                                }
                                            }
                                        }
                                        if (empty($wariantyIds)) {
                                            $wariantyIds = [null];
                                        }
                                        if (empty($this->Towar)) {
                                            $this->loadModel('Towar');
                                        }
                                        $item = $this->Towar->getWariantsToCart($this->Auth->user(), $itemId, $wariantyIds);
                                        if (!empty($itemsToAdd)) {
                                            $itemsToAdd = $itemsToAdd + $this->UzytkownikKoszyk->prepareCart($item, $warianty, $this->userRabat);
                                        } else {
                                            $itemsToAdd = $this->UzytkownikKoszyk->prepareCart($item, $warianty, $this->userRabat);
                                        }
                                        //array_merge($itemsToAdd, $this->UzytkownikKoszyk->prepareCart($item, $warianty, $this->userRabat));
                                        if (!empty($itemsToAdd)) {
                                            foreach ($itemsToAdd as $addKey => $addItem) {
                                                $cartItems['items'][$addKey] = $addItem;
                                            }
                                        }
                                    }
                                    $cartItems = $this->cartPrzelicz($cartItems);
                                    $this->session->write('cartItems', $cartItems);
                                }
                            }
                        }
                    }
                }
                $this->set('cartItems', $cartItems);
                Configure::write('cartItems', $cartItems);
                $count_star = Configure::read('opinie.ilosc_gwiazdek');
                $this->set('count_star', (!empty($count_star) ? $count_star : 5));
                $this->seoData['noIndex'] = (int) Configure::read('seo.noindex');
                $seoPrefix = Configure::read('seo.title_prefix');
                $seoSufiks = Configure::read('seo.title_sufix');
                if (empty($this->seoData['title'])) {
                    $this->seoData['title'] = Configure::read('seo.title');
                }
                if (!empty($seoPrefix)) {
                    $this->seoData['title'] = $seoPrefix . $this->seoData['title'];
                }
                if (!empty($seoSufiks)) {
                    $this->seoData['title'] = $this->seoData['title'] . $seoSufiks;
                }
                $this->set('seoData', $this->seoData);
                $this->set('footerOsiagniecia', $this->footerOsiagniecia);
                if (!empty($this->session)) {
                    $schowek = $this->session->read('schowek');
                    if (empty($schowek) && !empty($this->userId)) {
                        $schowek = $this->getSchowek();
                    }
                    $this->set('favorite', $schowek);
                }
                if ($this->getParams() != 'towar_index' && $this->getParams() != 'towar_view' && $this->getParams() != 'home_currency' && !$this->request->is('ajax')) {
                    if (!empty($this->session)) {
                        $this->session->delete('lastCategory');
                    }
                }
                $this->set('singleSite', $this->singleSite);
                $this->set('forceHideLeft', $this->forceHideLeft);

                $this->set('mobile', $this->mobile);
                if ($browser == 'safari') {
                    $this->styleShets[] = '/css/safari.css?' . uniqid();
                }
                if ($this->mobile) {
                    $this->styleShets[] = '/css/mobile.css?' . uniqid();
                }
                $hideCookieBar = $this->request->getCookie('hideCookieBar');
                $this->set('showCookieBar', empty($hideCookieBar));
                $usr = null;
                if (!empty($this->Auth)) {
                    $usr = $this->Auth->user();
                }
                $this->set('b2b', (!empty($usr) && $usr['typ'] == 'b2b'));
                $this->updateStat();
            }



            if (!empty($this->request->params['prefix']) && $this->request->params['prefix'] === 'api') {

                if (!array_key_exists('_serialize', $this->viewVars) && in_array($this->response->type(), ['application/json'])) {
                    $this->set('_serialize', true);
                }
            } else {
                if (!array_key_exists('_serialize', $this->viewVars) && in_array($this->response->type(), ['application/json', 'application/xml'])) {
                    $this->set('_serialize', true);
                }
                if (!empty($this->moreStyle)) {
                    foreach ($this->moreStyle as $styleUrl) {
                        $this->styleShets[] = $styleUrl;
                    }
                }
                $this->set('styleShets', $this->styleShets);
                $this->set('scripts', $this->scripts);
                $this->set('headScripts', $this->headScripts);
                if (!empty($this->Auth)) {
                    $this->set('user', $this->Auth->user());
                }
                $this->set('allCountry', $this->allCountry);
                $this->set('basePath', $this->basePath);
                $this->set('browser', $browser);
                $this->set('devMobile', $this->request->is('mobile'));
            }
        }
    }

    public function getParams() {
        return mb_strtolower($this->request->params['controller'] . '_' . $this->request->params['action']);
    }

    protected function getSchowek() {
        $this->loadModel('Schowek');
        $items = $this->Schowek->find('list', ['keyField' => 'towar_id', 'valueField' => 'towar_id'])->where(['uzytkownik_id' => $this->userId])->toArray();
        if (!empty($items)) {
            return $items;
        } else {
            return null;
        }
    }

    public function isAuthorized($user) {
        if (empty($user)) {
            return false;
        }
        if (!empty($this->request->params['prefix']) && $this->request->params['prefix'] == 'admin') {
            $controller = strtolower($this->request->params['controller']);
            $action = strtolower($this->request->params['action']);
            $deny = [];
            $allow = [];
            switch ($user['role_id']) {
                case 1 : {
                        
                    } break;
                case 2 : {
                        $allow = [
                            'zamowienie' => '*',
                            'raport' => '*',
                            'index' => '*',
//                            'uzytkownik' => '*',
                            'towar' => '*',
                            'towarzdjecie' => '*',
                            'kategoria' => '*',
                            'producent' => '*',
                            'atrybuttyp' => '*',
                            'atrybut' => '*',
                            'zestaw' => '*',
                            'index' => '*',
                            'newsletter' => '*',
                            'towaropinia' => '*',
                            'strona' => '*',
                            'menu' => '*',
                            'banner' => '*',
                            'blok' => '*',
                            'footerlink' => '*',
                            'waluta' => '*',
                            'vat' => '*',
                            'jednostka' => '*',
                            'rodzajplatnosci' => '*',
                            'wysylka' => '*',
                            'jezyk' => '*',
                            'szablon' => '*'
                        ];
                        $deny = '*';
                    } break;
                default : $deny = '*';
            }
            if (!empty($allow)) {
                if ($allow == '*') {
                    return true;
                } else {
                    if (key_exists($controller, $allow)) {
                        if ($allow[$controller] == '*') {
                            return true;
                        } elseif (key_exists($action, $allow[$controller])) {
                            return true;
                        }
                    } else {
                        return false;
                    }
                }
            }
            if ($deny == '*')
                return false;
            if (key_exists($controller, $deny)) {
                if ($deny[$controller] == '*')
                    return false;
                else if (!is_array($deny[$controller]))
                    return false;
                else {
                    if (key_exists($action, $deny[$controller]))
                        return false;
                    else
                        return true;
                }
            } else
                return true;
        } else {
            return !empty($user);
        }
    }

    private function setLocale() {
        if (strpos($_SERVER['SERVER_NAME'], 'www.') !== false && strpos($_SERVER['SERVER_NAME'], 'www.') == 0) {
            $domena = substr($_SERVER['SERVER_NAME'], (strpos($_SERVER['SERVER_NAME'], '.') + 1));
        } else {
            $domena = $_SERVER['SERVER_NAME'];
        }
        $this->domena = $domena;
        $this->cfg_domena = str_replace('.', '_', $this->domena);
        if (empty($this->Jezyk)) {
            $this->loadModel('Jezyk');
        }
        if (!empty($this->request->params['language'])) {
            $active_jezyk = $this->Jezyk->find('all', ['contain' => ['Waluta'], 'conditions' => ['Jezyk.symbol' => $this->request->params['language']]])->first();
        } else
            $active_jezyk = $this->Jezyk->find('all', ['contain' => ['Waluta'], 'conditions' => ['Jezyk.domena' => $domena]])->first();
        if (empty($active_jezyk->aktywny))
            $active_jezyk = $this->Jezyk->find('all', ['contain' => ['Waluta'], 'conditions' => ['Jezyk.domyslny' => 1]])->first();
        if (!empty($active_jezyk)) {
            $locale = $active_jezyk->locale;
            $this->active_jezyk = $active_jezyk;
            $this->set('active_jezyk', $active_jezyk);
            Configure::write('App.language', $locale);
            Configure::write('active_jezyk', $active_jezyk->toArray());
            I18n::locale($locale);
            $this->Translation->setLocale($locale);
            $this->set('locale', $locale);
        } else {
            I18n::locale('pl_PL');
        }
        $walutaId = $active_jezyk->waluta_id;
        if (!empty($walutaId)) {
            if (empty($this->Waluta)) {
                $this->loadModel('Waluta');
            }
            $waluta = $this->Waluta->find('all', ['conditions' => ['Waluta.id' => $walutaId]])->first();
            Configure::write('currencyDefault', $waluta->symbol);
        }
        $this->set('allLanguages', $this->Jezyk->find('all', ['order' => ['domyslny' => 'desc']])->where(['Jezyk.aktywny' => 1]));
    }

    public function cartPrzelicz($cartItems, $onlyReturn = false, $view = null) {
        $aktywneKody = $this->session->read('cartCode');
        if (!empty($cartItems['items'])) {
            $allPrice = 0;
            $allItems = 0;
            $totalItems = 0;
            $totalNetto = 0;
            $totalBrutto = 0;
            $toDistValue = 0;
            $defValue = 0;
            $toDistValueNetto = 0;
            $defValueNetto = 0;
            $allKg = 0;
            $allM3 = 0;
            $allCartItems = [];
            $inCartIds = [];
            $cartPreviewItems = [];
            $saveCartItems = [];
            $preorder = false;
            $onlyEWysylka = true;
            $cartIlosci = [];
            foreach ($cartItems['items'] as $itemKey => $item) {
                if (!empty($item['zestaw_bonus'])) {
                    continue;
                }
                if (!empty($item['gratis'])) {
                    continue;
                }
                if (!key_exists('karta_podarunkowa', $item)) {
                    $inCartIds[$item['towar_id']] = $item['towar_id'];
                    $onlyEWysylka = false;
                }
                $allCartItems[] = $itemKey . ':' . $item['ilosc'];
                $allPrice += $item['cena_razem'];
                $totalNetto += $item['cena_razem_netto'];
                $totalBrutto += $item['cena_razem_brutto'];
                if (!key_exists('karta_podarunkowa', $item)) {
                    $allKg += $item['waga'] * $item['ilosc'];
                }
                if (!$item['w_promocji']) {
                    $toDistValue += $item['cena_razem_brutto'];
                    $toDistValueNetto += $item['cena_razem_netto'];
                } else {
                    $defValue += $item['cena_razem_brutto'];
                    $defValueNetto += $item['cena_razem_netto'];
                }
                $allItems++;
                $totalItems += $item['ilosc'];
                if (!empty($item['preorder'])) {
                    $preorder = true;
                }
                if (!key_exists('karta_podarunkowa', $item)) {
                    $saveCartItems[$itemKey] = [
                        'towar_id' => $item['towar_id'],
                        'koszyk_key' => $itemKey,
                        'dodany' => 1,
                        'usuniety' => 0,
                        'data_dodania' => new \DateTime(date('Y-m-d H:i:s')),
                        'data_usuniecia' => null,
                        'ilosc' => $item['ilosc'],
                        'cena' => $item['cena_za_sztuke_brutto'],
                    ];
                    if (key_exists($item['towar_id'], $cartIlosci)) {
                        $cartIlosci[$item['towar_id']]['ilosc'] += $item['ilosc'];
                    } else {
                        $cartIlosci[$item['towar_id']] = ['max_ilosc' => $item['towar_ilosc'], 'ilosc' => $item['ilosc']];
                    }
                } else {
                    $saveCartItems[$itemKey] = [
                        'karta_podarunkowa_id' => $item['karta_podarunkowa_id'],
                        'koszyk_key' => $itemKey,
                        'dodany' => 1,
                        'usuniety' => 0,
                        'data_dodania' => new \DateTime(date('Y-m-d H:i:s')),
                        'data_usuniecia' => null,
                        'ilosc' => $item['ilosc'],
                        'cena' => $item['cena_za_sztuke_brutto'],
                    ];
                }
            }
            unset($cartItems['remove_gratis']);
            if (key_exists('gratis', $cartItems['items'])) {
                if ($totalBrutto < $cartItems['items']['gratis']['gratis_wartosc']) {
                    unset($cartItems['items']['gratis']);
                    $cartItems['remove_gratis'] = true;
                }
            }
            $cartItems['cartIlosci'] = $cartIlosci;
            $cartItems['onlyEWysylka'] = $onlyEWysylka;
            $cartItems['preorder'] = $preorder;
            $cartItems['in_cart_ids'] = $inCartIds;
            $cartItems['total_value'] = $allPrice;
            $cartItems['total_netto'] = $totalNetto;
            $cartItems['total_brutto'] = $totalBrutto;
            $cartItems['total_items'] = $this->Txt->printItems($totalItems);
            $cartItems['total_weight'] = $allKg;
            $cartItems['total_size'] = $allM3;
            unset($cartItems['rabat_value'], $cartItems['rabat'], $cartItems['next_rabat'], $cartItems['next_kwota'], $cartItems['total_netto_rabat'], $cartItems['total_brutto_rabat']);
            $allCartItems = join(',', $allCartItems);
            $saveArray = ['items' => $allCartItems, 'last_update' => new \DateTime(date('Y-m-d h:i:s'))];
            $koszykSessionId = $this->session->read('Koszyk.session_id');
            if (empty($koszykSessionId)) {
                $koszykSessionId = session_id();
            }
            $this->session->write('Koszyk.session_id', $koszykSessionId);
            $this->loadModel('Koszyk');
            $cartSession = $this->Koszyk->find('all', ['conditions' => ['Koszyk.session_id' => $koszykSessionId, 'Koszyk.aktywny' => 1], 'contain' => ['KoszykTowar']])->first();
            $newCart = false;
            if (empty($cartSession)) {
                $this->session->write('Koszyk.session_id', session_id());
                $cartNewData = [
                    'session_id' => $koszykSessionId,
                    'data' => new \DateTime(date('Y-m-d H:i:s')),
                    'last_update' => new \DateTime(date('Y-m-d H:i:s')),
                    'zamowienie_id' => null,
                    'aktywny' => 1,
                    'domena' => $_SERVER['SERVER_NAME']
                ];
                $cartSession = $this->Koszyk->newEntity();
                $newCart = true;
            } else {
                $cartSession->last_update = new \DateTime(date('Y-m-d H:i:s'));
            }

            if (!empty($cartSession->koszyk_towar)) {
                foreach ($cartSession->koszyk_towar as &$cartItem) {
                    if (key_exists($cartItem->koszyk_key, $saveCartItems)) {
                        if ($cartItem->ilosc != $saveCartItems[$cartItem->koszyk_key]['ilosc']) {
                            $cartItem->notatki .= (!empty($cartItem->notatki) ? "\n" : '') . 'Zmiana ilości z ' . $cartItem->ilosc . ' na ' . $saveCartItems[$cartItem->koszyk_key]['ilosc'];
                        }
                        $cartItem->set('usuniety', 0);
                        $cartItem->set('data_usuniecia', null);
                        $cartItem->ilosc = $saveCartItems[$cartItem->koszyk_key]['ilosc'];
                        unset($saveCartItems[$cartItem->koszyk_key]);
                    } else {
                        if (empty($cartItem->usuniety)) {
                            $cartItem->set('usuniety', 1);
                            $cartItem->set('data_usuniecia', date('Y-m-d H:i:s'));
                        }
                    }
                }
            }
            if ($newCart) {
                $cartToSave = $cartNewData;
                $cartToSave['koszyk_towar'] = $saveCartItems;
            } else {
                $cartToSave = $cartSession->toArray();
                if (!empty($cartToSave['koszyk_towar'])) {
                    $cartToSave['koszyk_towar'] = array_merge($cartToSave['koszyk_towar'], $saveCartItems);
                } else {
                    $cartToSave['koszyk_towar'] = $saveCartItems;
                }
            }
            $cartSession = $this->Koszyk->patchEntity($cartSession, $cartToSave);
            $this->Koszyk->save($cartSession);
            if (!empty($this->userId)) {
                $cartEntity = $this->UzytkownikKoszyk->find('all', ['conditions' => ['uzytkownik_id' => $this->userId, 'active' => 1]])->first();
            } elseif (!empty($cartItems['cart_id'])) {
                $cartEntity = $this->UzytkownikKoszyk->find('all', ['conditions' => ['id' => $cartItems['cart_id'], 'active' => 1]])->first();
            } else {
                $cartEntity = null;
            }
            if (empty($cartEntity)) {
                $saveArray['uzytkownik_id'] = $this->userId;
                $saveArray['create_date'] = $saveArray['last_update'];
                $saveArray['active'] = 1;
                $cartEntity = $entity = $this->UzytkownikKoszyk->newEntity();
            }
            $cartEntity = $this->UzytkownikKoszyk->patchEntity($cartEntity, $saveArray);
            $this->UzytkownikKoszyk->save($cartEntity);
            $cartItems['cart_id'] = $cartEntity->id;
        } else {
            if (!empty($this->userId)) {
                $cartEntity = $this->UzytkownikKoszyk->find('all', ['conditions' => ['uzytkownik_id' => $this->userId, 'active' => 1]])->first();
            } elseif (!empty($cartItems['cart_id'])) {
                $cartEntity = $this->UzytkownikKoszyk->find('all', ['conditions' => ['id' => $cartItems['cart_id'], 'active' => 1]])->first();
            } else {
                $cartEntity = null;
            }
            if (!empty($cartEntity)) {
                $this->UzytkownikKoszyk->delete($cartEntity);
            }
            $cartItems = null;
        }
        $totalAfterRabat = 0;
        $totalAfterRabatNetto = 0;
        if (!empty($cartItems)) {
            foreach ($cartItems['items'] as $itemKey => &$item) {
                if (!empty($item['zestaw_bonus'])) {
                    continue;
                }
$item['rabat']=0;
                if (!empty($aktywneKody)) {
                    foreach ($aktywneKody as $kodKey => $kodData) {
                        if (empty($kodData['min_order']) || $cartItems['total_brutto'] > $kodData['min_order']) {
                            if ($kodData['typ'] == 'procent' && ($kodData['rodzaj'] == 'ogolny' || ($kodData['rodzaj'] == 'producent' && !empty($item['producent_id']) && key_exists($item['producent_id'], $kodData['producent'])) || ($kodData['rodzaj'] == 'kategoria' && !empty($item['kategoria_id']) && key_exists($item['kategoria_id'], $kodData['kategoria'])))) {
                                if (!empty($item['rabat'])) {
                                    if ($kodData['rabat'] > $item['rabat']) {
                                        $item['rabat'] = $kodData['rabat'];
                                    }
                                } else {
                                    $item['rabat'] = $kodData['rabat'];
                                }
                            }
                        }
                    }
                }
                if (!empty($item['rabat'])) {
                    $item['cena_z_rabatem'] = round($item['cena_za_sztuke'] - ($item['cena_za_sztuke'] * $item['rabat'] / 100), 2);
                }else{
                    $item['cena_z_rabatem']=$item['cena_za_sztuke'];
                }
                $item['cena_razem_z_rabatem'] = round($item['cena_z_rabatem'] * $item['ilosc'], 2);
                $totalAfterRabat += $item['cena_razem_z_rabatem'];
                $totalAfterRabatNetto += round($item['cena_razem_z_rabatem'] / (1 + ($item['vat_stawka'] / 100)), 2);
            }

            $cartItems['total_z_rabatem'] = $totalAfterRabat;
            $cartItems['total_z_rabatem_netto'] = $totalAfterRabatNetto;
            
                    $darmowaLimit = c('wysylka.limit');
                    if(!empty($darmowaLimit)){
                        $brakuje = $darmowaLimit;
                        if(!empty($cartItems['total_z_rabatem'])){
                            $brakuje = $brakuje - $cartItems['total_z_rabatem'];
                        }
                        if($brakuje < 0){
                            $brakuje = 0;
                        }
                        $cartItems['to_free_delivery']=$this->Txt->cena($brakuje);
                    }
            $cartItems['cart-info'] = t('header.Ilosc{0}Kwota{1}', ['<span id="cartItemsCount">' . $totalItems . '</span>', '<span id="cartItemsValue">' . $cartItems['total_value'] . '</span>']);
            $this->loadModel('RodzajPlatnosci');
            $wysylkiPrice = $this->RodzajPlatnosci->Wysylka->getPrice($cartItems['total_weight'],$cartItems['total_z_rabatem']);
            $platnoscChecked = $this->session->read('platnosc');
            if (empty($this->Zamowienie)) {
                $this->loadModel('Zamowienie');
            }
            $this->loadModel('Zamowienie');
            $orderData = $this->session->read('orderFormData');
            $zamowienie = $this->Zamowienie->newEntity($orderData, ['validate' => false]);
            if (empty($view)) {
                $view = new \Cake\View\View();
                $view->Translation->setLocale(\Cake\I18n\I18n::locale());
            }
            $view->set('mobile', $this->mobile);
            $view->set('items', $cartItems['items']);
            $view->set('displayPath', $this->displayPath);
            $view->set('filePath', $this->filePath);
            $cartItems['preview'] = (!empty($cartItems['items']) ? $view->element('default/cart_preview') : '');
            $view->layout = false;
            $displayPath = $this->displayPath;
            $getPlatnosci = $this->getPlatnosci($cartItems, $onlyEWysylka);
            $view->set('rodzaje_platnosci', $getPlatnosci['platnosci']);
            $view->set('wysylki', $getPlatnosci['wysylki']);
            $view->set('totalBrutto', $totalAfterRabat);
            $view->set('user', $this->Auth->user());
            $rabatKodHtml = $this->getRabatKodHtml($cartItems);
            $view->set('rabatKodHtml', $rabatKodHtml);
            $view->set(compact('wysylki', 'platnoscChecked', 'displayPath', 'wysylkiPrice', 'zamowienie'));
            $cartItems['platnosci_html'] = $view->element('default/order/rodzaje_platnosci');
            $view->Translation->afterLayout();
        } else {
            $cartItems['preview'] = '';
        }
        Configure::write('cartItems', $cartItems);
        return $cartItems;
    }

    public function getIpData($ip) {
        if ((strpos($ip, ":") === false)) {
            //ipv4
            $gi = geoip_open(ROOT . DS . "plugins" . DS . "GeoLiteCity.dat", GEOIP_STANDARD);
        } else {
            //ipv6
            $gi = geoip_open(ROOT . DS . "plugins" . DS . "GeoIPv6.dat", GEOIP_STANDARD);
        }
        $record = GeoIP_record_by_addr($gi, $ip);
        $return = [];
        $regions = new \geoIpRegionVars();
        if (!empty($record)) {
            foreach ($record as $key => $value) {
                if ($key == 'region') {
                    $return[$key] = $regions->getRegionName($record->country_code, $record->region);
                } else {
                    $return[$key] = $value;
                }
            }
        }
        geoip_close($gi);
        return $return;
    }

    public function userPricesStart() {
        $userId = $this->Auth->user('id');
        $aktualizacjaStart = false;
        $user = $this->Uzytkownik->find('all')->where(['Uzytkownik.id' => $userId])->first();
        if ($user->typ == 'b2b') {
            if ($user->aktualizacja_cen > 2) {
                $time = time() - strtotime($user->aktualizacja_cen_start->format('Y-m-d H:i:s'));
                if ($time > 14400) {
                    $user->aktualizacja_cen = 1;
                    if ($this->Uzytkownik->save($user)) {
                        $aktualizacjaStart = true;
                    }
                }
            } elseif ($user->aktualizacja_cen == 0) {
                $user->aktualizacja_cen = 1;
                if ($this->Uzytkownik->save($user)) {
                    $aktualizacjaStart = true;
                }
            }
        }
        return $aktualizacjaStart;
    }

    protected function getRabatKodHtml($cartItems) {
        $rabatKodHtml = '';
        $kodRabatowy = $this->session->read('rabat_kod');
        if (!empty($kodRabatowy)) {
            $this->loadModel('KodyRabatowe');
            $checkCode = $this->KodyRabatowe->find('all', ['conditions' => ['kod' => $kodRabatowy, 'zablokuj' => 0, 'data_start <=' => date('Y-m-d H:i:s'), 'data_end >=' => date('Y-m-d H:i:s')]])->first();
            if (!empty($checkCode)) {
                if (!empty($checkCode)) {
                    $rabatValue = round($checkCode->rabat, 2);
                } else {
                    $rabatValue = 0;
                }
                $razem = $cartItems['total_brutto'];
                $oldRazem = $razem;
                if ($checkCode->typ == 'kwota') {
                    $razem = $razem - $rabatValue;
                } else {
                    $razem = $razem - ($razem * ($rabatValue / 100));
                }
                if (!empty($kodRabatowy) && empty($checkCode)) {
                    
                } else {
                    if ($razem == $oldRazem) {
                        
                    } else
                        $rabatKodHtml = $this->Txt->kodRabatowyHtml($checkCode, $kodRabatowy, $rabatValue); //$this->Txt->Html->tag('div', $this->Txt->Form->control('promocja_kod', ['type' => 'hidden', 'value' => $kodRabatowy]).$this->Txt->Form->control('promocja_kod_value', ['type' => 'hidden', 'value' => $rabatValue]).$this->Txt->Form->control('promocja_kod_typ', ['type' => 'hidden', 'value' => $checkCode->typ]) . $this->Translation->get('koszyk.KodRabatowy{kod}{rabat}', ['kod'=>$this->Txt->Html->tag('span',$kodRabatowy),'rabat'=>(($checkCode->typ=='kwota')?$this->Txt->Html->tag('span',$this->Txt->cena($rabatValue)):$this->Txt->Html->tag('span',$rabatValue.'%'))], true), ['class' => 'rabatKodVal text-right']);
                }
            }
        }
        return $rabatKodHtml;
    }

    public function writeCookie($key, $value, $expire = '+1 year') {
        $this->response = $this->response->withCookie($key, ['value' => $value, 'expire' => strtotime($expire)]);
    }

    protected function getPlatnosci($koszykWartosc, $onlyEWysylka = false) {
        if (empty($this->Wysylka)) {
            $this->loadModel('Wysylka');
        }
        if (empty($this->RodzajPlatnosci)) {
            $this->loadModel('RodzajPlatnosci');
        }
        $rodzaj_platnosci = $this->RodzajPlatnosci->find('all', ['conditions' => ['RodzajPlatnosci.aktywna' => 1], 'contain' => ['WysylkaPlatnosc']]);
        $wsIds = [];
        foreach ($rodzaj_platnosci as $rodzajPlatnosci) {
            foreach ($rodzajPlatnosci->wysylka_platnosc as $wysylkaPlatnosc) {
                $wsIds[$wysylkaPlatnosc->wysylka_id] = $wysylkaPlatnosc->wysylka_id;
            }
        }
        if (empty($wsIds))
            $wsIds = [null];

        $conditions = ['Wysylka.aktywna' => 1, 'Wysylka.id IN' => $wsIds, 'Wysylka.elektroniczna' => 0];
        if ($onlyEWysylka) {
            $conditions['Wysylka.elektroniczna'] = 1;
        }
        $wysylka = $this->Wysylka->find('all', ['conditions' => $conditions, 'contain' => ['WysylkaPlatnosc', 'PunktyOdbioru']]);
        $allWysylki = [];
        $allWsIds = [];
        foreach ($wysylka as $tmpWs) {
            $allWysylki[] = $tmpWs;
            $allWsIds[$tmpWs->id] = $tmpWs->id;
        }
        $allPlatnosci = [];
        foreach ($rodzaj_platnosci as $tmpRodzajP) {
            foreach ($tmpRodzajP->wysylka_platnosc as $wysylkaPlatnosc) {
                if (key_exists($wysylkaPlatnosc->wysylka_id, $allWsIds)) {
                    $allPlatnosci[$tmpRodzajP->id] = $tmpRodzajP;
                    break;
                }
            }
        }
        return ['platnosci' => $allPlatnosci, 'wysylki' => $allWysylki];
    }

    private function updateStat() {
//        return false;
        if (!$this->stats || empty($_SERVER) || !key_exists('HTTP_USER_AGENT', $_SERVER) || strpos(mb_strtolower($_SERVER['HTTP_USER_AGENT']), 'bot') !== false || empty($this->session)) {
            return false;
        }
        $this->loadModel('Statystyki');
        $urls = $this->session->read('visited');
        if (empty($urls)) {
            $urls = [];
        }
        $urls[$this->request->getRequestTarget()] = time();
        $this->session->write('visited', $urls);
        $sId = $this->session->id();
        if (!$this->Statystyki->updateAll(['last' => new \DateTime(date('Y-m-d H:i:s')), 'pages_count' => count($urls), 'pages' => join(" | ", array_keys($urls))], ['session_id' => $sId])) {
            if (!($this->Statystyki->find()->where(['session_id' => $sId])->count() > 0)) {
                $stat = $this->Statystyki->newEntity(['session_id' => $sId, 'start' => new \DateTime(date('Y-m-d H:i:s')), 'last' => new \DateTime(date('Y-m-d H:i:s')), 'agent' => $_SERVER['HTTP_USER_AGENT'], 'pages_count' => count($urls), 'pages' => join(" | ", array_keys($urls)), 'ip' => $_SERVER['REMOTE_ADDR']]);
                $this->Statystyki->save($stat);
            }
        }
    }

    protected function checkActiveCodes() {
        $aktywneKody = $this->session->read('cartCode');
        if (!empty($aktywneKody)) {
            $user = $this->Auth->user();
            foreach ($aktywneKody as $kodKey => $kod) {
                if ($kod['klient_typ'] == 'b2b' && (empty($user) || $user['typ'] == 'b2c')) {
                    unset($aktywneKody[$kodKey]);
                }
                if ($kod['klient_typ'] == 'b2c' && (!empty($user) && $user['typ'] == 'b2b')) {
                    unset($aktywneKody[$kodKey]);
                }
            }
            $this->session->write('cartCode', $aktywneKody);
        }
    }

}
