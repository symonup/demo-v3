<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Cache\Cache;
use Cake\Controller\Component\CookieComponent;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class BlogController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow();
        $this->loadModel('Blog');
    }
    public function index() {
        $this->paginate=[
            'conditions'=>['Blog.ukryty'=>0],
            'order'=>['Blog.data'=>'desc'],
            'limit'=>10
        ];
        $artykuly=$this->paginate($this->Blog);
        $this->loadModel('Menu');
            $menuPages = $this->Menu->find('all', ['conditions' => ['Menu.parent_id IS' => NULL,'Menu.typ'=>'page','(Menu.jezyk_id = '.$this->active_jezyk->id.' OR Menu.jezyk_id IS NULL)', 'OR' => ['Menu.ukryty' => 0, 'Menu.ukryty IS' => NULL]], 'order' => ['Menu.kolejnosc' => 'desc'], 'contain' => ['ChildMenu' => ['conditions' => ['ChildMenu.ukryty' => 0], 'sort' => ['ChildMenu.kolejnosc' => 'desc']]]]);
            $this->set('menuPages', $menuPages);
        $this->set(compact('artykuly'));
        $crumbs = [$this->Translation->get('breadcrumb.Blog') => null];
                $this->set('crumbs', $crumbs);
                $this->forceHideLeft=true;
    }
    public function artykul($id=null){
        $artykul=$this->Blog->find('all')->where(['Blog.id'=>$id,'Blog.ukryty'=>0])->first();
        if(!empty($artykul)){   $this->loadModel('Menu');
            $menuPages = $this->Menu->find('all', ['conditions' => ['Menu.parent_id IS' => NULL,'(Menu.jezyk_id = '.$this->active_jezyk->id.' OR Menu.jezyk_id IS NULL)','Menu.typ'=>'page', 'OR' => ['Menu.ukryty' => 0, 'Menu.ukryty IS' => NULL]], 'order' => ['Menu.kolejnosc' => 'desc'], 'contain' => ['ChildMenu' => ['conditions' => ['ChildMenu.ukryty' => 0], 'sort' => ['ChildMenu.kolejnosc' => 'desc']]]]);
            $this->set('menuPages', $menuPages);
        $this->set(compact('artykuly'));
        $crumbs = [
            $this->Translation->get('breadcrumb.Blog') => \Cake\Routing\Router::url(['action'=>'index']),
            $artykul->tytul=>null
                ];
                $this->set('crumbs', $crumbs);
            $this->set(compact('artykul'));
        }else{
            $this->Flash->error($this->Translation->get('blog.BrakArtykuluLubArtykulJestNiedostepny'));
            return $this->redirect(['action'=>'index']);
        }
                $this->forceHideLeft=true;
    }

    
}
