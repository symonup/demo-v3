<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class HomeController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow();
    }

    public function index() {
        if (empty($this->Banner)) {
            $this->loadModel('Banner');
        }
        if (empty($this->Blog)) {
            $this->loadModel('Blog');
        }
        $banner = $this->Banner->find('all', ['conditions' => ['Banner.glowny' => 1, 'Banner.ukryty' => 0], 'contain' => ['BannerSlides' => ['BannerSlidesElements']]])->first();
        $this->loadModel('Towar');
        $towarCenaDefaultParams = ['Vat', 'Waluta', 'Jednostka', 'conditions' => ['TowarCenaDefault.waluta_id' => $this->active_jezyk->waluta_id]];
        $towarContain = ['HotDeal', 'Wersja', 'TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]], 'TowarCenaDefault' => $towarCenaDefaultParams];
        $towarHotDealContain = $towarContain;
        unset($towarHotDealContain[array_search('HotDeal', $towarContain)]);
        $bestsellers = $this->Towar->find('all', ['order' => ['rand()'], 'contain' => $towarContain])->where(['Towar.wyrozniony' => 1, 'Towar.ukryty' => 0])->limit(30);
        $promocje = $this->Towar->find('all', ['order' => ['rand()'], 'contain' => $towarContain])->where(['Towar.promocja' => 1, 'Towar.ukryty' => 0])->limit(30);
        $nowosci = $this->Towar->find('all', ['order' => ['rand()'], 'contain' => $towarContain])->where(['Towar.nowosc' => 1, 'Towar.ukryty' => 0])->limit(30);
        $hotDeal = $this->Towar->HotDeal->find('all', ['contain' => ['Towar' => $towarHotDealContain]])->where(['Towar.ukryty' => 0, 'HotDeal.ilosc >' => 0, 'HotDeal.aktywna' => 1, 'HotDeal.data_do >=' => date('Y-m-d H:i:s'), '(HotDeal.data_od IS NULL OR HotDeal.data_od <= \'' . date('Y-m-d H:i:s') . '\')'])->order(['HotDeal.data_do' => 'asc'])->first();
        $artykuly = $this->Blog->find()->where(['Blog.ukryty' => 0])->order(['Blog.data' => 'desc'])->limit(4);
        $this->set(compact('ostatnie', 'promocje', 'nowosci', 'banner', 'bestsellers', 'hotDeal', 'artykuly'));
        $seo = Configure::read('seoHome');
        if (!empty($seo) && !empty($seo['title'])) {
            $this->seoData['title'] = $seo['title'];
        }
        if (!empty($seo) && !empty($seo['keywords'])) {
            $this->seoData['keywords'] = $seo['keywords'];
        }
        if (!empty($seo) && !empty($seo['description'])) {
            $this->seoData['description'] = $seo['description'];
        }
    }

    public function register($type = 'b2c') {

        $this->session->delete('registerErrors');
        if (empty($this->Uzytkownik)) {
            $this->loadModel('Uzytkownik');
        }
        $uzytkownik = $this->Uzytkownik->newEntity();
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            $rqData['data_rejestracji'] = new \DateTime(date('Y-m-d H:i:s'));
            $rqData['aktywny'] = 0;
            $rqData['typ'] = $type;
            $rqData['token'] = uniqid();
            if ($type == 'b2b') {
                $fvAdres = [
                    'typ' => 2,
                    'imie' => $rqData['imie'],
                    'nazwisko' => $rqData['nazwisko'],
                    'firma' => $rqData['firma'],
                    'nip' => $rqData['nip'],
                    'ulica' => $rqData['firma_ulica'],
                    'nr_domu' => $rqData['firma_nr_domu'],
                    'nr_lokalu' => $rqData['firma_nr_lokalu'],
                    'kod' => $rqData['firma_kod'],
                    'miasto' => $rqData['firma_miasto'],
                    'kraj' => 'PL'
                ];
                $rqData['uzytkownik_adres'] = [$fvAdres];
            }
            $uzytkownik = $this->Uzytkownik->patchEntity($uzytkownik, $rqData, ['validate' => 'registration' . $type]);
            if ($this->Uzytkownik->save($uzytkownik)) {
                $this->loadComponent('Mail');
                $this->loadComponent('Replace');
                $this->loadModel('Szablon');

                $replace = [
                    'uzytkownik-id' => $uzytkownik->id,
                    'uzytkownik-imie' => $uzytkownik->imie,
                    'uzytkownik-nazwisko' => $uzytkownik->nazwisko,
                    'uzytkownik-email' => $uzytkownik->email,
                    'uzytkownik-firma' => $uzytkownik->firma,
                    'uzytkownik-nip' => $uzytkownik->nip,
                    'uzytkownik-telefon' => (!empty($uzytkownik->kierunkowy) ? $uzytkownik->kierunkowy . ' ' . str_replace($uzytkownik->kierunkowy, '', $uzytkownik->telefon) : $uzytkownik->telefon),
                    'uzytkownik-data-rejestracji' => (!empty($uzytkownik->data_rejestracji) ? $uzytkownik->data_rejestracji->format('Y-m-d H:i') : ''),
                    'uzytkownik-potwierdz-url' => \Cake\Routing\Router::url(['controller' => 'Home', 'action' => 'userPotwierdz', $uzytkownik->token], true),
                    'adres-ip' => $this->request->clientIp()
                ];
                $szablonInfo = $this->Szablon->findByNazwa('admin_info_nowy_klient')->first();
                $messageInfo = $this->Replace->getTemplate($szablonInfo->tresc, $replace);
                $szablon = $this->Szablon->findByNazwa('rejestracja')->first();
                $message = $this->Replace->getTemplate($szablon->tresc, $replace);
                $this->Mail->sendMail($uzytkownik->email, $szablon->temat, $message);
                $sendTo[] = Configure::read('mail.domyslny_email');
                $this->Mail->sendInfo($sendTo, $szablonInfo->temat, $messageInfo);

                $redirect = ['controller' => 'Home', 'action' => 'registerSuccess'];
                $returnData = ['status' => 'success', 'message' => t('success.rejestracjaPrawidlowa'), 'redirect' => r($redirect)];
            } else {
                $errors = $uzytkownik->getErrors();
                $returnData = ['status' => 'error', 'message' => t('errors.regisrerErrorMessage'), 'errors' => $errors];
            }
            $this->response->type('ajax');
            $this->response->body(json_encode($returnData));
            return $this->response;
        }
        $this->viewBuilder()->setLayout('ajax');
        $this->set(compact('type', 'uzytkownik'));
    }

    public function login($fromOrder = null) {
        if ($this->Auth->isAuthorized()) {
            return $this->redirect(['controller' => 'Towar', 'action' => 'index']);
        }
        $this->session->delete('registerErrors');
        if (empty($this->Uzytkownik)) {
            $this->loadModel('Uzytkownik');
        }
        $uzytkownik = $this->Uzytkownik->newEntity();
        $logUzytkownik = $this->Uzytkownik->newEntity();
        if ($this->request->is('post')) {

            $rqData = $this->request->getData();
            if (key_exists('login', $rqData)) {
                $logUzytkownik = $this->Uzytkownik->patchEntity($logUzytkownik, $rqData, ['validate' => 'login']);
                $user = $this->Auth->identify();
                if ($user) {
                    if (!$user['aktywny']) {
                        $this->Flash->error($this->Translation->get('errors.accountNotAccepted'));
                        if (!empty($fromOrder) && $fromOrder == 'order') {
                            return $this->redirect(['controller' => 'Cart', 'action' => 'index', 'order_open' => 1]);
                        }
                    } else {
                        if (!empty($rqData['remember_me'])) {
                            if (empty($user['remember_token'])) {
                                $user['remember_token'] = uniqid();
                                $this->Uzytkownik->updateAll(['remember_token' => $user['remember_token']], ['id' => $user['id']]);
                            }
                            $this->writeCookie('rememberMe', $user['remember_token']);
                        }
                        $this->Auth->setUser($user);
                        $this->checkActiveCodes();
                        if (!empty($fromOrder) && $fromOrder == 'order') {
                            return $this->redirect(['controller' => 'Cart', 'action' => 'index', 'order_open' => 1]);
                        }
                        $aUrl = $this->Auth->redirectUrl();
                        $admRd = $this->request->referer(); //(($aUrl == '/') ? ['controller' => 'Uzytkownik', 'action' => 'index'] : $aUrl);
                        return $this->redirect($admRd);
                    }
                } else {
                    $this->Flash->error($this->Translation->get('errors.loginErrorMessage'));
                    if (!empty($fromOrder) && $fromOrder == 'order') {
                        return $this->redirect(['controller' => 'Cart', 'action' => 'index', 'order_open' => 1]);
                    }
                }
            }
        }

        $this->set('logUzytkownik', $logUzytkownik);
        $this->set('uzytkownik', $uzytkownik);
        $this->set('_serialize', ['uzytkownik']);
        $this->set('hideNavbar', true);
        $seo = Configure::read('seoLogin');
        if (!empty($seo) && !empty($seo['title'])) {
            $this->seoData['title'] = $seo['title'];
        }
        if (!empty($seo) && !empty($seo['keywords'])) {
            $this->seoData['keywords'] = $seo['keywords'];
        }
        if (!empty($seo) && !empty($seo['description'])) {
            $this->seoData['description'] = $seo['description'];
        }
    }

    public function userPotwierdz($token) {
        $this->autoRender = false;
        if (!empty($token)) {
            $this->loadModel('Uzytkownik');
            $uzytkownik = $this->Uzytkownik->find('all')->where(['Uzytkownik.token' => $token])->first();
            if (!empty($uzytkownik)) {
                if (empty($uzytkownik->aktywny)) {
                    $uzytkownik->aktywny = 1;
                    if ($this->Uzytkownik->save($uzytkownik)) {
                        $this->Flash->success($this->Translation->get('success.TwojeKontoZostaloPotwierdzone'));
                        return $this->redirect(['action' => 'login']);
                    } else {
                        $this->Flash->error($this->Translation->get('errors.BladPotwierdzeniaKonta'));
                    }
                } else {
                    $this->Flash->success($this->Translation->get('success.TwojeKontoJestJuzPotwierdzone'));
                    return $this->redirect(['action' => 'login']);
                }
            } else {
                $this->Flash->error($this->Translation->get('errors.NieprawidlowyAdres'));
            }
        } else {
            $this->Flash->error($this->Translation->get('errors.NieprawidlowyAdres'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function logout() {
        $this->Flash->success($this->Translation->get('success.logoutSuccessMessage'));
        $this->session->delete('cartItems');
        $this->session->delete('orderFormData');
        $this->writeCookie('rememberMe', null, '- 1 day');
        $this->Auth->logout();
        $this->checkActiveCodes();
        return $this->redirect(['action' => 'login']);
    }

    public function registerSuccess() {
        if ($this->request->is('mobile')) {
            $this->forceHideLeft = true;
        }
    }

    public function resetPassword($token = null) {
        $urlToken = $token;
        $this->loadComponent('Mail');
        $this->loadComponent('Replace');
        $this->loadModel('Uzytkownik');
        if (!empty($this->userId)) {
            $uzytkownik = $this->Uzytkownik->find('all', ['conditions' => ['Uzytkownik.id' => $this->userId]])->first();
        } else {
            $uzytkownik = $this->Uzytkownik->newEntity();
        }
        if (!empty($token)) {
            $uzytkownik = $this->Uzytkownik->find('all', [
                        'fields' => ['Uzytkownik.id'],
                        'conditions' => [
                            'Uzytkownik.token' => $token,
                            'Uzytkownik.token_data >=' => new \DateTime('30 minutes ago')
                        ]
                    ])->first();
            if (!empty($uzytkownik)) {
                if ($this->request->is(['patch', 'post', 'put'])) {
                    $uzytkownik = $this->Uzytkownik->patchEntity($uzytkownik, $this->request->getData(), ['validate' => 'passwords']);
                    if ($this->Uzytkownik->save($uzytkownik)) {
                        $this->Flash->success($this->Translation->get('success.yourPasswordHasBeenChanged'));
                        return $this->redirect(['action' => 'login']);
                    }
                }
            } else {
                $this->Flash->error($this->Translation->get('errors.unactiveToken'));
                return $this->redirect(['action' => 'resetPassword']);
            }
            $this->set('token', $token);
        } elseif ($this->request->is(['patch', 'post', 'put'])) {
            $uzytkownik = $this->Uzytkownik->findByEmail($this->request->data['email'])->first();
            if (!empty($uzytkownik)) {
                $token = md5(uniqid('', true));
                $url = \Cake\Routing\Router::url([$token], true);
                $replace = [
                    'odzyskiwanie-hasla-link' => __('<a href="{0}">{1}</a>', [$url, $this->Translation->get('labels.resetPasswordLink')]),
                    'odzyskiwanie-hasla-url' => $url,
                    'adres-ip' => $this->request->clientIp(),
                ];
                $this->Uzytkownik->connection()->transactional(function() use ($uzytkownik, $replace, $token) {
                    $uzytkownik->token = $token;
                    $uzytkownik->set('token_data', date('Y-m-d H:i:s'));
                    if ($this->Uzytkownik->save($uzytkownik)) {
                        $this->loadModel('Szablon');
                        $szablon = $this->Szablon->findByNazwa('odzyskiwanie_hasla')->first();
                        $message = $this->Replace->getTemplate($szablon->tresc, $replace);
                        $this->Flash->success($this->Translation->get('success.resetPasswordMailSent'));
                        return $this->Mail->sendMail($uzytkownik->email, $szablon->temat, $message);
                    }
                    $this->Flash->error($this->Translation->get('errors.dataCouldNotBeSaved'));
                    return false;
                });
            } else {
                $this->Flash->error($this->Translation->get('errors.userNotExist'));
            }
        }
        $this->set('uzytkownik', $uzytkownik);
        $this->set('token', $urlToken);
        $this->set('_serialize', ['uzytkownik']);
        if ($this->request->is('mobile')) {
            $this->forceHideLeft = true;
        }
        $this->set('hideNavbar', true);
        $seo = Configure::read('seoPassword');
        if (!empty($seo) && !empty($seo['title'])) {
            $this->seoData['title'] = $seo['title'];
        }
        if (!empty($seo) && !empty($seo['keywords'])) {
            $this->seoData['keywords'] = $seo['keywords'];
        }
        if (!empty($seo) && !empty($seo['description'])) {
            $this->seoData['description'] = $seo['description'];
        }
    }

    public function currency() {
        $this->autoRender = false;
        $this->stats = false;
        $this->viewBuilder()->layout('ajax');
        $currencyOptions = $this->Txt->currencyJsOptions(Configure::read('currencyDefault'));
        $this->set('currencyOptions', $currencyOptions);
        $this->render('currency.js');
    }

    public function bannerPreview($bannerId = null, $slideId = null) {
        $this->loadModel('Banner');
        $this->viewBuilder()->layout('banner');
        $bsc = [];
        if (!empty($slideId)) {
            $bsc['BannerSlides.id'] = $slideId;
        }
        $banner = $this->Banner->find('all', ['conditions' => ['Banner.id' => $bannerId], 'contain' => ['BannerSlides' => ['conditions' => $bsc, 'BannerSlidesElements']]])->first();
        $this->set('banner', $banner);
    }

    public function getTranslate() {
        $this->autoRender = false;
        $message = $this->request->query('message');
        if (!empty($message)) {
            $message = $this->Translation->get($message);
        }
        $this->response->type('ajax');
        $this->response->body($message);
        return $this->response;
    }

    public function newsletter($token = null) {
        $this->autoRender = false;
        $this->loadModel('NewsletterAdres');
        $this->loadComponent('Mail');
        $this->loadComponent('Replace');
        if (!empty($token)) {
            $newsletterAdres = $this->NewsletterAdres->findByToken($token)->first();
            if (!empty($newsletterAdres)) {
                $newsletterAdres->potwierdzony = 1;
                if ($this->NewsletterAdres->save($newsletterAdres)) {
                    $this->Flash->success($this->Translation->get('newsletter.TwojEmailZostalPotwierdzony'));
                }
            }
        } else {
            if ($this->request->is('post')) {
                $newsletterAdres = $this->NewsletterAdres->newEntity();
                $data = $this->request->data;
                $data['jezyk_id'] = $this->active_jezyk->id;
                $data['data'] = date('Y-m-d H:i:s');
                $data['token'] = md5(uniqid('', true));
                $data['potwierdzony'] = 0;
                $data['locale'] = \Cake\I18n\I18n::locale();
                $newsletterAdres = $this->NewsletterAdres->patchEntity($newsletterAdres, $data);
                if ($this->NewsletterAdres->save($newsletterAdres)) {
                    $url = \Cake\Routing\Router::url(['controller' => 'Home', 'action' => 'newsletter', $newsletterAdres->token], true);
                    $replace = [
                        'newsletter-potwierdzenie-link' => __('<a href="{0}">{1}</a>', [$url, $this->Translation->get('newsletter.potwierdzZapisNaNewsletter')]),
                        'newsletter-potwierdzenie-url' => $url,
                        'newsletter-token' => $newsletterAdres->token,
                        'adres-ip' => $this->request->clientIp(),
                    ];
                    $this->loadModel('Szablon');
                    $szablon = $this->Szablon->findByNazwa('newsletter_potwierdzenie')->first();
                    $message = $this->Replace->getTemplate($szablon->tresc, $replace);
                    $this->Mail->sendMail($newsletterAdres->email, $szablon->temat, $message);
                    $this->Flash->success($this->Translation->get('newsletter.emailZostalZapisany'));
                } else {
                    $flashMessage = [];
                    foreach ($newsletterAdres->errors() as $type => $message) {
                        if (!empty($message['_isUnique'])) {
                            $newsletterAdres = $this->NewsletterAdres->findByEmail($data['email'])->first();
                            if (!empty($newsletterAdres->potwierdzony)) {
                                $this->Flash->success($this->Translation->get('newsletter.TwojEmailJestJuzZapisany'));
                                $flashMessage = [];
                                break;
                            } else {
                                $url = \Cake\Routing\Router::url(['controller' => 'Home', 'action' => 'newsletter', $newsletterAdres->token], true);
                                $replace = [
                                    'newsletter-potwierdzenie-link' => __('<a href="{0}">{1}</a>', [$url, $this->Translation->get('newsletter.potwierdzZapisNaNewsletter')]),
                                    'newsletter-potwierdzenie-url' => $url,
                                    'newsletter-token' => $newsletterAdres->token,
                                    'adres-ip' => $this->request->clientIp(),
                                ];
                                $this->loadModel('Szablon');
                                $szablon = $this->Szablon->findByNazwa('newsletter_potwierdzenie')->first();
                                $message = $this->Replace->getTemplate($szablon->tresc, $replace);
                                $this->Mail->sendMail($newsletterAdres->email, $szablon->temat, $message);
                                $this->Flash->success($this->Translation->get('newsletter.emailZostalZapisany'));
                                $flashMessage = [];
                                break;
                            }
                        } else
                            foreach ($message as $text)
                                $flashMessage[] = $text;
                    }
                    if (!empty($flashMessage)) {
                        foreach ($flashMessage as $text)
                            $this->Flash->error($text);
                    }
                }
            }
        }
        $referer = $this->request->referer();
        if (empty($referer))
            $referer = ['controller' => 'Home', 'action' => 'index'];
        return $this->redirect($referer);
    }

    public function newsletterWypisz($token = null) {
        $this->autoRender = false;
        $this->loadModel('NewsletterAdres');
        if (!empty($token)) {
            $newsletterAdres = $this->NewsletterAdres->findByToken($token)->first();
            if (!empty($newsletterAdres)) {
                if ($this->NewsletterAdres->delete($newsletterAdres)) {
                    $this->Flash->success($this->Translation->get('newsletter.TwojEmailZostalUsunietyZNewslettera'));
                }
            }
        }
        $referer = $this->request->referer();
        if (empty($referer))
            $referer = ['controller' => 'Home', 'action' => 'index'];
        return $this->redirect($referer);
    }

    public function changeLocale() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $route = [];
            if (empty($this->request->getData('redirect'))) {
                $route = ['controller' => 'Home', 'action' => 'index'];
            } else {
                foreach (json_decode($this->request->getData('redirect')) as $key => $value) {
                    $route[$key] = $value;
                }
            }
            $route['language'] = $this->request->getData('locale');
            return $this->redirect($route);
        } else {
            return $this->redirect($this->request->referer());
        }
    }

    public function powiadomODostepnosci($towarId) {
        $this->viewBuilder()->setLayout('ajax');
        if (empty($this->Towar)) {
            $this->loadModel('Towar');
        }
        $this->loadModel('Powiadomienia');
        $towarCenaDefaultParams = ['Vat', 'Waluta', 'Jednostka', 'conditions' => ['TowarCenaDefault.waluta_id' => $this->active_jezyk->waluta_id]];
        $towarContain = ['HotDeal', 'TowarCenaDefault' => $towarCenaDefaultParams];
        $towar = $this->Towar->find('all', ['contain' => $towarContain])->where(['Towar.id' => $towarId])->first();
        $powiadomienie = $this->Powiadomienia->newEntity();
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
//            if (!empty($rqData['zgoda'])) {
            $rqData['typ'] = (!empty($_GET['t']) ? 'dostepnosc' : 'zapytanie');
            $rqData['uzytkownik_id'] = $this->Auth->user('id');
            $rqData['towar_id'] = $towar->id;
            $rqData['data_dodania'] = date('Y-m-d H:i:s');
            $rqData['cena'] = $this->Txt->itemPrice($towar);
            $powiadomienie = $this->Powiadomienia->patchEntity($powiadomienie, $rqData);
            if ($this->Powiadomienia->save($powiadomienie)) {
                $this->Flash->success($this->Translation->get('success.zgloszenieZostaloZapisane'));
            } else {
                $this->Flash->error($this->Translation->get('errors.bladZapisuZgloszenia'));
            }
//            } else {
//                $this->Flash->error($this->Translation->get('errors.zgodaNaPrzetwarzanieDanychJestWymagana'));
//            }
        } else {
            if (!empty($this->userId)) {
                $powiadomienie->set('email', $this->Auth->user('email'));
                $powiadomienie->set('imie', $this->Auth->user('imie'));
            }
            $this->loadModel('PunktyOdbioru');
            $punktyOdbioru = $this->PunktyOdbioru->find('list', ['keyField' => 'id', 'valueField' => 'adres'])->where(['aktywny' => 1])->toArray();
            if (!empty($punktyOdbioru)) {
                foreach ($punktyOdbioru as &$punkt) {
                    $punkt = nl2br($punkt);
                }
            }
            $this->set('punktyOdbioru', $punktyOdbioru);
        }
        if (!$this->request->is('ajax')) {
            return $this->redirect($this->request->referer());
        }
        $typ = (!empty($_GET['t']) ? 'dostepnosc' : 'zapytanie');
        $this->set(compact('towar', 'powiadomienie', 'typ'));
    }

    public function zapytanie() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $this->loadModel('Zapytanie');
            $rqData = $this->request->getData();
//            if (!empty($rqData['zgoda_dane'])) {
            $zapytanie = $this->Zapytanie->newEntity();
            $rqData['nr_zgloszenia'] = $this->zapytanieNumer();
            $rqData['uzytkownik_id'] = $this->Auth->user('id');
            $rqData['data_dodania'] = date('Y-m-d H:i:s');
            $zapytanie = $this->Zapytanie->patchEntity($zapytanie, $rqData);
            if ($this->Zapytanie->save($zapytanie)) {
                $this->loadModel('Szablon');
                $this->loadComponent('Replace');
                $this->loadComponent('Mail');
                $replace = [];
                foreach ($zapytanie->toArray() as $field => $value) {
                    if ($field == 'data_dodania' || $field == 'data_wyslania') {
                        $replace['[%' . $field . '%]'] = $this->Txt->printDate($value);
                    } else
                        $replace['[%' . $field . '%]'] = (!empty($value) ? $value : '');
                }
                $replace['adres-ip'] = $this->request->clientIp();
                $szablon = $this->Szablon->findByNazwa('potwierdzenie_zapytania')->first();
                $message = $this->Replace->getTemplate($szablon->tresc, $replace, [], true);
                $temat = $this->Replace->getTemplate($szablon->temat, $replace, [], true);
                $this->Mail->sendMail($zapytanie->email, $temat, $message);

                $this->Flash->success($this->Translation->get('success.zapytanieZostaloWyslane'));
            } else {
                $this->Flash->error($this->Translation->get('errors.bladZapisuZapytania'));
            }
//            } else {
//                $this->Flash->error($this->Translation->get('errors.zgodaNaPrzetwarzanieDanychJestWymagana'));
//            }
        }
        if (!$this->request->is('ajax')) {
            return $this->redirect($this->request->referer());
        }
    }

    private function zapytanieNumer() {
        if (empty($this->Zapytanie)) {
            $this->loadModel('Zapytanie');
        }
        $numer = $this->Txt->randomOrderNumber(8);
        if ($this->Zapytanie->find()->where(['nr_zgloszenia' => $numer])->count() == 0) {
            return $numer;
        } else {
            return $this->zapytanieNumer();
        }
    }

    public function setDesktop($desktop = true) {
        $this->autorender = false;
        if (empty($desktop)) {
            $this->session->delete('forceDesktop');
            $this->session->write('forceMobile', true);
        } else {
            $this->session->write('forceDesktop', true);
            $this->session->delete('forceMobile');
        }
        $this->response->type('ajax');
        $this->response->body('');
        return $this->response;
    }

    function confirmCookie() {
        $this->autoRender = false;
        $this->writeCookie('hideCookieBar', true);
        $this->response->type('ajax');
        $this->response->body('');
        return $this->response;
    }

    public function unUseCode($kod) {
        $aktywneKody = $this->session->read('cartCode');
        unset($aktywneKody[$kod]);
        $this->session->write('cartCode', $aktywneKody);
        $cartItems = $this->session->read('cartItems');
        $cartItems = $this->cartPrzelicz($cartItems);
        $this->session->write('cartItems', $cartItems);
        $this->Flash->success('success.kodZostalWylaczony');
        return $this->redirect($this->request->referer());
    }

    public function useCode($kod) {
        $this->autoRender = false;
        $this->loadModel('KodyRabatowe');
        $kodRabatowy = $this->KodyRabatowe->find('all', ['contain' => ['Kategoria', 'Producent']])->where(['KodyRabatowe.kod' => $kod, 'KodyRabatowe.zablokuj' => 0])->first();
        $error = '';
        $rqQuery = [];
        if (!empty($kodRabatowy)) {
            $now = $this->Txt->printDate(new \DateTime(), 'Y-m-d H:i:s', '', true);
            if ($now >= $this->Txt->printDate($kodRabatowy->data_start, 'Y-m-d H:i:s', '', true) && $now <= $this->Txt->printDate($kodRabatowy->data_end, 'Y-m-d H:i:s', '', true)) {
                if ($kodRabatowy->klient_typ == 'b2b' && $this->Auth->user('typ') != 'b2b') {
                    $error = t('errors.kodDostepnyTylkoDlaKlientowHurtowych');
                } elseif ($kodRabatowy->klient_typ == 'b2c' && $this->Auth->user('typ') == 'b2b') {
                    $error = t('errors.kodDostepnyTylkoDlaKlientowDetalicznych');
                }
                if (empty($error)) {
                    $prepareCode = $kodRabatowy->toArray();
                    if ($kodRabatowy->rodzaj == 'producent') {
                        $prodArr = [];
                        foreach ($kodRabatowy->producent as $kodProd) {
                            $prodArr[$kodProd->id] = $kodProd->id;
                        }
                        $prepareCode['producent'] = $prodArr;
                        $rqQuery['producent'] = join(',', $prodArr);
                    }
                    if ($kodRabatowy->rodzaj == 'kategoria') {
                        $katArr = [];
                        foreach ($kodRabatowy->kategoria as $kodKat) {
                            $katArr[$kodKat->id] = $kodKat->id;
                        }
                        $prepareCode['kategoria'] = $katArr;
                        $rqQuery['cat'] = join(',', $katArr);
                    }
                    $aktywneKody = $this->session->read('cartCode');
                    if (!empty($aktywneKody)) {
                        if (!empty($prepareCode['multi'])) {
                            foreach ($aktywneKody as $aKod => $aKodData) {
                                if (empty($aKodData['multi'])) {
                                    $error = t('errors.maszAktywnyKodKtoryNieMozeLaczycSieZWybranymKodem', ['kodAktywny' => $aKod]);
                                    break;
                                }
                            }
                        } elseif (key_exists($prepareCode['kod'], $aktywneKody)) {
                            $aktywneKody[$prepareCode['kod']] = $prepareCode;
                        } else {
                            $error = t('errors.wybranyKodNieMozeLaczycSieZInnymi');
                        }
                    } else {
                        $aktywneKody = [$prepareCode['kod'] => $prepareCode];
                    }
                    if (empty($error)) {
                        $this->session->write('cartCode', $aktywneKody);
                    }
                }
            } else {
                $error = t('errors.kodJestNieaktywny');
            }
        } else {
            $error = t('errors.podanyKodJestNieprawidlowy');
        }
        if (empty($error)) {
            $rqQuery['code'] = $kodRabatowy->kod;
            $urlArr = ['controller' => 'Towar', 'action' => 'index'];
            foreach ($rqQuery as $qKey => $qVal) {
                $urlArr[$qKey] = $qVal;
            }
            $cartItems = $this->session->read('cartItems');
            $cartItems = $this->cartPrzelicz($cartItems);
            $this->session->write('cartItems', $cartItems);
            $this->Flash->success(t('success.kodZostalAktywowany'));
            return $this->redirect($urlArr);
        }
        $this->Flash->error($error);
        return $this->redirect($this->request->referer());
    }

    public function getTalk() {
        $this->viewBuilder()->setLayout('ajax');
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            $saveData = [
                'telefon' => $rqData['telefon'],
                'zgoda' => $rqData['zgoda'],
                'data' => new \DateTime(),
                'status' => 0
            ];
            $this->loadModel('CallBack');
            $callBack = $this->CallBack->newEntity($saveData);
            if ($this->CallBack->save($callBack)) {
                $this->Flash->success(t('success.telefonZostalPrzekazanyDoOddzwonienia'));
            }
            return $this->redirect($this->request->referer());
        }
    }

    public function getFv($token = null, $download = false) {
        if (!empty($token)) {
            $this->loadModel('Faktura');
            $faktura = $this->Faktura->find('all', ['contain' => ['FakturaPozycja']])->where(['Faktura.token' => $token])->first();
            if (!empty($faktura)) {
                $this->loadModel('Szablon');
                $this->loadComponent('Replace');
                $szablon = $this->Szablon->findByNazwa('faktura')->first();
                $tresc = $szablon->tresc;
                $rodzajPlatnosci = '';
                if(!empty($faktura->kwota_zaplacona_przy_odbiorze) && empty($faktura->platnosc_karta_nazwa)){
                    //gotowka
                    $rodzajPlatnosci=c('faktura.platnoscGotowka');
                }elseif(!empty($faktura->platnosc_karta_nazwa)){
                    $rodzajPlatnosci=c('faktura.platnoscKarta');
                }else{
                    $rodzajPlatnosci=c('faktura.platnoscPrzelew');
                }
                $replace = [
                    '[%numer%]' => $faktura->numer_dokumentu,
                    '[%podtytul%]' => $faktura->podtytul,
                    '[%dane-klienta%]' => $faktura->nazwa_pelna.'<br>'.$faktura->adres.'<br>'.$faktura->kod_pocztowy.' '.$faktura->miasto.'<br>'.__('NIP: {0}',$faktura->nip),
                    '[%dane-sklepu%]'=>$faktura->sklep_nazwa.'<br>'.$faktura->sklep_adres.'<br>'.$faktura->sklep_kod_pocztowy.' '.$faktura->sklep_miasto.'<br>'.__('NIP: {0}',$faktura->sklep_nip),
                    '[%rodaj-platnosci%]'=>$rodzajPlatnosci,
                    '[%miejsce-wystawienia%]'=>$faktura->miejsce_wystawienia,
                    '[%data-wystawienia%]'=>$this->Txt->printDate($faktura->data_wystawienia),
                    '[%data-sprzedazy%]'=>$this->Txt->printDate($faktura->data_sprzedazy),
                    '[%termin-platnosci%]'=>$this->Txt->printDate($faktura->termin_platnosci),
                    '[%wartosc-faktury%]'=>$this->Txt->cena($faktura->wartosc_do_zaplaty)
                    ];
                $html = $this->Replace->getTemplateFaktura($tresc, $replace, $faktura->faktura_pozycja);
                $name = str_replace('/', '_', $faktura->numer_dokumentu) . '.pdf';
                $orientacja = 'A4-P';
                $mpdf = new \mPDF('utf-8', $orientacja);
                $mpdf->WriteHTML($html);
                if ($download) {
                    $mpdf->Output($name, 'D');
                } else {
                    header("Content-type:application/pdf");
                    echo $mpdf->Output($name, 'S');
                }
                die();
            }
        }
        return $this->redirect(['action' => 'index']);
    }

}
