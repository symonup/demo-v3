<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Contact Controller
 *
 *
 * @method \App\Model\Entity\Contact[] paginate($object = null, array $settings = [])
 */
class ContactController extends AppController
{
    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow();
        if($this->request->is('mobile')){
                $this->forceHideLeft=true;
            }
    }
    public function initialize() {
        parent::initialize();
        $this->loadComponent('Captcha');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        if ($this->request->is('post')) {
            if ($this->Captcha->validateCaptcha($this->request)) {
                $this->loadComponent('Mail');
                $template = '';
                foreach ($this->request->data as $key => $value) {
                    if ($key == 'cakecaptcha')
                        continue;
                    $template.='<b>' . $key . ':</b>' . '<br/>' . nl2br($value) . '<br/><br/>';
                }
                $this->Mail->sendKontakt($this->request->data['email'], $template);
                $this->Flash->success($this->Translation->get('success.contactFormHasBeenSent'));
                return $this->redirect(['action' => 'index']);
            }else{
                $this->session->write('kontaktForm',$this->request->getData());
            }
            return $this->redirect('/kontakt');
        }else{
            $this->session->delete('kontaktForm');
            return $this->redirect('/kontakt');
        }
    }

    public function captcha() {
        $this->autoRender = false;
        $this->Captcha->configCaptcha(array('pathType' => 2,'background'=>[255,0,233]));
        $this->Captcha->getCaptcha();
    }

    }
