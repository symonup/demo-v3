<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Cache\Cache;
use Cake\Log\Log;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;

/**
 * Cron Controller
 *
 * @property \App\Model\Table\CronTable $Cron
 *
 * @method \App\Model\Entity\Cron[] paginate($object = null, array $settings = [])
 */
class CronController extends AppController {

    private $allowsIP = false;
    private $maxTimeRun = 285; //Maksymalny czas (w sekundach) wykonania crona jeżeli wykonywana jest kolejna pętla a czas od uruchomienia przekroczył ten czas to cron kończy swoje działanie.
    private $cronItem = null;
    private $countLoop = 0;
    private $trimAddress = [" " => "", "\t" => "", "\n" => "", "\r" => "", "-" => "", "," => "", "/" => ""];
    private $allStates = [];
    private $timeStart;
    private $blockLog = true;
    private $fieldMap = [];
    private $remoteKatMap = [];
    private $remoteColorMap = [];
    private $remoteSizeMap = [];
    private $katPath = [];

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['run', 'porownywarki', 'tests']);
        date_default_timezone_set('Europe/Warsaw');
        ignore_user_abort(true);
        set_time_limit(0);
        ini_set('max_execution_time', 900);
        ini_set('max_input_time', 900);
        ini_set('mysql.connect_timeout', 900);
        ini_set('max_input_vars', 10000);
        ini_set('default_socket_timeout', 900);
        ini_set('memory_limit', '-1');
//        phpinfo();
//        die();
    }

    public function run($id) {
        $this->autoRender = false;
        if (empty($id)) {
            die('Brak ID crona');
        }
        if (!empty($this->allowsIP)) {
            if (array_search($this->request->clientIp(), $this->allowsIP) === false) {
                die('Access denied');
            }
        }
        $this->cronItem = $this->Cron->find('all')->where(['Cron.id' => $id, 'OR' => ['Cron.blokada' => 0, 'Cron.blokada IS' => NULL]])->first();
        if (empty($this->cronItem)) {
            die();
        }
        if (!empty($this->cronItem->log)) {
            $this->blockLog = false;
        }
        if ($this->cronItem->status > 0) {
            if ((!empty($this->cronItem->last_run) && (time() - strtotime($this->cronItem->last_run->format('Y-m-d H:i:s'))) >= 3600)) {
                if (!$this->blockLog) {
                    Log::write('info', __('Cron {0} reset. Nie dziala od {1} sec.', [$id, (time() - strtotime($this->cronItem->last_run->format('Y-m-d H:i:s')))]), ['cronInfo']);
                }
                $this->cronItem->set('status', 0);
            } else {
                //'Cron jest w trakcie dzialania'
                die();
            }
        }
        if (!empty($this->cronItem->max_time)) {
            $this->maxTimeRun = $this->cronItem->max_time;
        }
        $this->timeStart = time();
        $timeEnd = $this->timeStart;
        $token = uniqid();
        $up = ['token' => $token, 'last_run' => date('Y-m-d H:i:s'), 'status' => 1];
        $this->Cron->updateAll($up, ['id' => $id]);
        $message = '';
        $brake = false;
        $finished = false;
        $pause = false;
        $con = null;
        do {
            $this->countLoop++;
            if (empty($this->cronItem)) {
                break;
            }
            if ($this->cronItem->status != 1) {
                $this->cronItem->set('status', 1);
                $this->Cron->save($this->cronItem);
            }
            $skip = $this->cronItem->skip;
            $take = $this->cronItem->take;
            if ($this->cronItem->page > 0) {
                $skip = ($this->cronItem->take * $this->cronItem->page) + 1;
                $take = $take - 1;
            }
            $modifiedTime = (!empty($this->cronItem->modifiedTime) ? $this->cronItem->modifiedTime->format('Y-m-d') : null);
            switch ($this->cronItem->typ) {
                case 'priceMessage' : {
                        $result = $this->priceMessage();
                    }break;
                case 'getAllegroOrders' : {
                        $result = $this->getAllegroOrders();
                    }break;
                case 'allegroUpdateAvil' : {
                        $result = $this->allegroUpdateAvil();
                    } break;
                case 'allegroUpdatePrice' : {
                        $result = $this->allegroUpdatePrice();
                    } break;
                case 'allegroCheckRenew' : {
                        $result = $this->allegroCheckRenew();
                    } break;
                case 'allegroCheckStatus': {
                        $type = 'avil';
                        switch ($this->cronItem->take) {
                            case 2 : {
                                    $type = 'price';
                                }break;
                            case 3 : {
                                    $type = 'renew';
                                }break;
                        }
                        $result = $this->allegroCheckStatus($type);
                    }break;
                case 'ceneoXml' : {
                        $result = $this->ceneoXml();
                    } break;
                case 'newsletter' : {
                        $this->loadModel('Newsletter');
                        $newsletter = $this->Newsletter->find('all')->where(['Newsletter.status' => 1])->first();
                        if (!empty($newsletter)) {
                            switch ($newsletter->typ) {
                                case 0: {
                                        $result = $this->sendNewsletter($newsletter, (!empty($this->cronItem->take) ? $this->cronItem->take : 50));
                                    }break;
                                default : {
                                        $result = ['status' => 'error', 'message' => __('Niezidentyfikowany typ newslettera')];
                                    } break;
                            }
                        } else {
                            $result = ['status' => 'stop', 'message' => __('Brak newsletterów do wysłania')];
                        }
                    }break;
                case 'integracjaIkonka' : {
                        $this->setFieldMapIkonka();
                        $result = $this->integracjaIkonka((!empty($this->cronItem->take) ? $this->cronItem->take : 1));
                    }break;
                case 'integracjaCentralazabawek' : {
                        $this->setFieldMapCentralazabawek();
                        $result = $this->integracjaCentralazabawek((!empty($this->cronItem->take) ? $this->cronItem->take : 1));
                    }break;
                case 'integracjaGatito' : {
                        $this->setFieldMapGatito();
                        $result = $this->integracjaGatito((!empty($this->cronItem->take) ? $this->cronItem->take : 1));
                    }break;
                case 'integracjaMolos' : {
                        $this->setFieldMapMolos();
                        $result = $this->integracjaMolos((!empty($this->cronItem->take) ? $this->cronItem->take : 1));
                    }break;
                case 'integracjaPtakmodahurt' : {
                        $this->setFieldMapPtakmodahurt();
                        $result = $this->integracjaPtakmodahurt((!empty($this->cronItem->take) ? $this->cronItem->take : 1));
                    }break;
                case 'integracjaVmp' : {
                        $this->setFieldMapVmp();
                        $result = $this->integracjaVmp((!empty($this->cronItem->take) ? $this->cronItem->take : 1));
                    }break;
                case 'downloadImages' : {
                        $result = $this->downloadImages((!empty($this->cronItem->take) ? $this->cronItem->take : 20));
                    } break;
                default : {
                        $result = ['status' => 'error', 'message' => 'Brak poprawnej akcji.'];
                    }break;
            }
            $timeEnd = time();

            if ($result['status'] == 'pause') {
                $brake = true;
                if ((!key_exists('log', $result) || $result['log'] == true) && !$this->blockLog) {
                    Log::write('info', __('Cron {0} zakończono w {1} sec. {2}', [$id, ($timeEnd - $this->timeStart), $result['message']]), ['cronInfo']);
                }
            } elseif ($result['status'] == 'stop') {
                $brake = true;
                $finished = true;
                if ((!key_exists('log', $result) || $result['log'] == true) && !$this->blockLog) {
                    Log::write('info', __('Cron {0} zakończono w {1} sec. {2}', [$id, ($timeEnd - $this->timeStart), $result['message']]), ['cronInfo']);
                }
            } elseif ($result['status'] == 'success') {
                if (!$this->blockLog) {
                    Log::write('info', __('Cron {0} success. {1}', [$id, $result['message']]), ['cronInfo']);
                }
                sleep(3);
            } elseif ($result['status'] == 'continue') {
                if ((!key_exists('log', $result) || $result['log'] == true) && !$this->blockLog) {
                    Log::write('info', $result['message'], ['cronInfo']);
                }
                unset($result);
                sleep(3);
            } else {
                $brake = true;
                Log::write('error', __('Cron {0} error. {1}', [$id, $result['message']]), ['cronError']);
            }
            if ($brake) {
                break;
            }
        } while (($timeEnd - $this->timeStart) < $this->maxTimeRun);
        $this->dbReconnect();
        $endTime = ($timeEnd - $this->timeStart);
        $message = null; //__('{3} Cron job {0} time: {1} sec. {2}', [$id, $endTime, 'Memory usage: ' . (memory_get_usage(true) / 1024 / 1024) . ' MB', date('Y-m-d H:i:s')]);
        $this->cronItem->set('status', 0);
        if (($finished || ($this->countLoop == 1 && $endTime < $this->maxTimeRun)) && !$pause) {
            $this->cronItem->set('page', 0);
            if (!empty($this->cronItem->nextModifiedTime)) {
                $this->cronItem->set('modifiedTime', $this->cronItem->nextModifiedTime);
            }
        }
        if ($this->newRouteRequierd) {
            $this->generateRouting();
        }
        $this->Cron->save($this->cronItem);
        $this->response->type('json');
        $this->response->body($message);
        return $this->response;
    }

    private function priceMessage() {
        $this->loadModel('Powiadomienia');
        $this->loadModel('Towar');
        $this->loadModel('TowarCena');
        $wyslane = 0;
        $towaryToCheckIds = $this->Powiadomienia->find('list', ['keyField' => 'towar_id', 'valueField' => 'towar_id', 'group' => 'towar_id'])->where(['Powiadomienia.wyslane' => 0, 'Powiadomienia.typ' => 'cena', 'Powiadomienia.towar_id IS NOT NULL'])->toArray();
        if (!empty($towaryToCheckIds)) {
            $this->loadModel('Szablon');
            $this->loadComponent('Replace');
            $this->loadComponent('Mail');
            $szablon = $this->Szablon->findByNazwa('powiadomienie_o_cenie')->first();
            $towaryToCheck = $this->Powiadomienia->find('all', ['contain' => ['Towar']])->where(['Powiadomienia.wyslane' => 0, 'Powiadomienia.typ' => 'cena']);
            $con = \Cake\Datasource\ConnectionManager::get('default');
            $dataNow = date('Y-m-d H:i:s');
            $stmt = $con->execute("SELECT tc.towar_id, if((hd.cena IS NOT NULL AND hd.aktywna = 1 AND hd.ilosc > 0 AND hd.data_do >= '$dataNow' AND (hd.data_od <= '$dataNow' OR hd.data_od IS NULL)),hd.cena,if(((t.promocja = 1 OR t.wyprzedaz=1 OR t.produkt_tygodnia=1) AND tc.cena_promocja > 0),tc.cena_promocja,tc.cena_sprzedazy)) as 'cena' 
FROM towar t 
INNER JOIN towar_cena tc ON t.id=tc.towar_id 
LEFT JOIN hot_deal hd ON t.id=hd.towar_id 
WHERE tc.towar_id IN (" . join(',', $towaryToCheckIds) . ")");
            $results = $stmt->fetchAll('assoc');
            foreach ($results as $result) {
                $allPrices[$result['towar_id']] = $result['cena'];
            }
            foreach ($towaryToCheck as $powiadomienieItem) {
                if (!empty($powiadomienieItem->cena) && key_exists($powiadomienieItem->towar_id, $allPrices) && $powiadomienieItem->cena > $allPrices[$powiadomienieItem->towar_id]) {
                    $replaceArr = [
                        '[%stara_cena%]' => $this->Txt->cena($powiadomienieItem->cena),
                        '[%nowa_cena%]' => $this->Txt->cena($allPrices[$powiadomienieItem->towar_id]),
                        '[%produkt%]' => ($powiadomienieItem->has('towar') ? $this->Txt->Html->link($powiadomienieItem->towar->nazwa, \Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'view', $powiadomienieItem->towar->id, $this->Txt->friendlyUrl($powiadomienieItem->towar->nazwa), 'prefix' => false], true)) : '')
                    ];

                    foreach ($powiadomienieItem->toArray() as $field => $value) {
                        if (is_object($value)) {
                            $replaceArr['[%' . $field . '_data%]'] = $this->Txt->printDate($value, 'Y-m-d');
                            $replaceArr['[%' . $field . '_czas%]'] = $this->Txt->printDate($value, 'H:i');
                        } else {
                            if (is_array($value)) {
                                continue;
                            }
                            $replaceArr['[%' . $field . '%]'] = $value;
                        }
                    }

                    $temat = $this->Replace->getTemplate($szablon->temat, $replaceArr, [], true);
                    $message = $this->Replace->getTemplate($szablon->tresc, $replaceArr, [], true);
                    if ($this->Mail->sendMail($powiadomienieItem->email, $temat, $message)) {
                        $powiadomienieItem = $this->Powiadomienia->patchEntity($powiadomienieItem, ['wyslane' => 1, 'data_wyslania' => date('Y-m-d H:i:s'), 'odpowiedz' => $message]);
                        $this->Powiadomienia->save($powiadomienieItem);
                        $wyslane++;
                        sleep(2);
                    }
                }
            }
            if ($wyslane > 0) {
                return ['status' => 'stop', 'message' => __('Wysłano {0} powiadomień o cenie', [$wyslane])];
            } else {
                return ['status' => 'stop', 'message' => __('Brak powiadomień do wysłania')];
            }
        } else {
            return ['status' => 'stop', 'message' => __('Brak powiadomień do wysłania')];
        }
    }

    private function sendNewsletter($nesletter, $limit = 50) {
        $this->autoRender = false;
        $wyslano = 0;
        $this->loadModel('NewsletterAdres');
        $emails = $this->NewsletterAdres->find('all')->where(['NewsletterAdres.wyslany' => 0, 'NewsletterAdres.email IS NOT' => NULL, 'NewsletterAdres.potwierdzony' => 1])->limit($limit);
        if (!empty($emails) && $emails->count() > 0) {
            $this->loadComponent('Replace');
            $this->loadComponent('Mail');
            foreach ($emails as $mail) {
                $replaceArr = [
                    '[%data%]' => date('Y-m-d'),
                    '[%token%]' => $mail->token
                ];
                $template = $this->Replace->getNewsletterTemplate($nesletter->tresc, $replaceArr);
                $temat = $this->Replace->getNewsletterTemplate($nesletter->temat, $replaceArr);
                if (!empty($mail->email)) {
                    $this->Mail->sendMail($mail->email, $temat, $template);
                    $up = ['wyslany' => 1];
                    $this->NewsletterAdres->updateAll($up, ['id' => $mail->id]);
                    $wyslano++;
                    sleep(2);
                }
            }
        } else {
            if (empty($this->Newsletter)) {
                $this->loadModel('Newsletter');
            }
            $up = [
                'status' => 2
            ];
            $nesletter = $this->Newsletter->patchEntity($nesletter, $up);
            if ($this->Newsletter->save($nesletter)) {
                $this->NewsletterAdres->updateAll(['wyslany' => 0], ['wyslany' => 1]);
            }
            return ['status' => 'stop', 'message' => __('Wysyłka neslettera została zakończona {0}', [date('Y-m-d H:i:s')])];
        }
        return ['status' => 'stop', 'message' => __('Wysłano {0} maili', [$wyslano])];
    }

    private function dbReconnect() {
        $connection = \Cake\Datasource\ConnectionManager::get('default');
        $connection->disconnect();
        $connection->connect();
    }

    private function allegroCheckStatus($type = 'avil') {
        $this->loadModel('AllegroKonto');
        $this->loadModel('AllegroAukcja');
        $itemsToCheck = null;
        $action = '';
        $field = '';
        $fieldStatus = '';
        $allCount = 0;
        switch ($type) {
            case 'avil' : {
                    $action = 'allegroCheckAvilStatus';
                    $field = 'iloscQueryId';
                    $fieldStatus = 'iloscQueryStatus';
                }break;
            case 'price' : {
                    $action = 'allegroCheckPriceStatus';
                    $field = 'cenaQueryId';
                    $fieldStatus = 'cenaQueryStatus';
                }break;
            case 'renew' : {
                    $action = 'allegroCheckRenewStatus';
                    $field = 'statusQueryId';
                    $fieldStatus = 'statusQueryStatus';
                }break;
        }
        if (!empty($field)) {
            $itemsToCheck = $this->AllegroAukcja->find('all', ['contain' => 'AllegroKonto'])->where([$field . ' IS NOT NULL']);
            if (!empty($itemsToCheck) && $itemsToCheck->count() > 0 && !empty($action)) {
                foreach ($itemsToCheck as $item) {
                    if (!empty($item->allegro_konto)) {
                        $this->setAllegroClinent($item->allegro_konto);

//                         Log::write('info', 'Update ilosc status start: '.date('Y-m-d H:i:s'), ['cronInfo']);
                        $result = $this->{$action}($item->{$field});
//                         Log::write('info', 'Update ilosc status end: '.date('Y-m-d H:i:s'), ['cronInfo']);
                        if (!empty($result) && !empty($result['status'])) {
                            $statusLog = (!empty($item->iloscQueryStatusLog) ? json_decode($item->iloscQueryStatusLog, true) : []);
                            $lastLog = null;
                            if (!empty($statusLog)) {
                                $logKeys = array_keys($statusLog);
                                $lastLog = json_encode($statusLog[$logKeys[count($logKeys) - 1]]);
                            }
                            if ($lastLog != json_encode($result)) {
                                $statusLog[] = $result;
                            }
                            if (mb_strtolower($result['status']) == 'success') {
                                $this->AllegroAukcja->updateAll([$field => null, $fieldStatus => 'success', 'iloscQueryStatusLog' => json_encode($statusLog)], ['id' => $item->id]);
                            } else {
                                if (mb_strtolower($result['status']) == 'fail' && !empty($result['code']) && mb_strtolower($result['code']) == 'err_item_not_found') {
                                    $this->AllegroAukcja->updateAll([$fieldStatus => $result['status'] . ': ' . $result['message'], 'iloscQueryStatusLog' => json_encode($statusLog), 'removed' => 1], ['id' => $item->id]);
                                } else
                                    $this->AllegroAukcja->updateAll([$fieldStatus => $result['status'] . ': ' . $result['message'], 'iloscQueryStatusLog' => json_encode($statusLog)], ['id' => $item->id]);
                            }
                        }
                        $allCount++;
                    }
                }
            }
        }
        return ['status' => 'stop', 'message' => __('Sprawdzono zapytania z allegro {0}', [$allCount])];
    }

    private function allegroCheckRenew() {
        $this->loadModel('AllegroKonto');
        $this->loadModel('AllegroAukcja');
        $allCount = 0;
        $itemsToCheck = $this->AllegroAukcja->find('all', ['contain' => 'AllegroKonto'])->where(['renew' => 1, 'AllegroAukcja.removed' => 0]);
        if (!empty($itemsToCheck) && $itemsToCheck->count() > 0) {
            foreach ($itemsToCheck as $item) {
                if (!empty($item->allegro_konto)) {
                    $this->setAllegroClinent($item->allegro_konto);
                    $queryId = $this->allegroRenew($item->item_id);
                    if (!empty($queryId)) {
                        $this->AllegroAukcja->updateAll(['renew' => 0, 'statusQueryId' => $queryId], ['id' => $item->id]);
                        $allCount++;
                    }
                }
            }
        }
        return ['status' => 'stop', 'message' => __('Wznowiono {0} aukcji allegro', [$allCount])];
    }

    private function getAllegroOrders() {
        $this->loadModel('AllegroKonto');
        $this->loadModel('AllegroZamowienie');
        $konta = $this->AllegroKonto->find('all');
        $allCount = 0;
        foreach ($konta as $konto) {
            $this->setAllegroClinent($konto);
            $lastRun = date('Y-m-d', strtotime('- 3 days'));
            $nowRun = date('Y-m-d H:i:s');
            $getOrders = $this->allegroOrders(1, 100, 'READY_FOR_PROCESSING', $lastRun);
            $allCount += $getOrders;
        }
        return ['status' => 'stop', 'message' => __('Sprawdzono zamówienia z allegro {0}', [$allCount])];
    }

    private function allegroUpdateAvil() {
        $this->loadModel('AllegroKonto');
        $this->loadModel('AllegroAukcja');
        $konta = $this->AllegroKonto->find('all');
        $allCount = 0;
        foreach ($konta as $konto) {
            $this->setAllegroClinent($konto);
            $allItemsToChange = $this->AllegroAukcja->find('all', ['limit' => 10])->where(['AllegroAukcja.allegro_konto_id' => $konto->id, 'AllegroAukcja.removed' => 0, 'OR' => ['AllegroAukcja.iloscToUpdate <' => 0, 'AllegroAukcja.iloscToUpdate >' => 0]]);
            foreach ($allItemsToChange as $itemToChange) {
                Log::write('info', 'Update ilosc start ' . $itemToChange->item_id . ': ' . date('Y-m-d H:i:s') . ' ilosc: ' . $itemToChange->iloscToUpdate, ['cronInfo']);
                $result = $this->allegroChangeAvil($itemToChange->item_id, $itemToChange->iloscToUpdate);
                Log::write('info', 'Update ilosc end ' . $itemToChange->item_id . ': ' . date('Y-m-d H:i:s'), ['cronInfo']);
                Log::write('info', $result, ['cronInfo']);
                if ($result) {
                    if (is_array($result)) {
                        $upData = ['iloscToUpdate' => 0, 'iloscQueryId' => $result['queryId'], 'renew' => $result['renew']];
                        sleep(3);
                    } else {
                        $upData = ['iloscToUpdate' => 0, 'iloscQueryId' => $result];
                    }
                    $this->AllegroAukcja->updateAll($upData, ['id' => $itemToChange->id]);
                    $allCount++;
                } else {
//                    if (mb_strtolower($result['status']) == 'fail' && !empty($result['code']) && mb_strtolower($result['code']) == 'err_item_not_found') {
//                        $this->AllegroAukcja->updateAll(['removed' => 1], ['id' => $itemToChange->id]);
//                    }
                }
            }
        }
        sleep(3);
        return ['status' => 'continue', 'message' => __('Zaktualizowano stany magazynowe na allegro {0}', [$allCount])];
    }

    private function allegroUpdatePrice() {
        $this->loadModel('AllegroKonto');
        $this->loadModel('AllegroAukcja');
        $konta = $this->AllegroKonto->find('all');
        $allCount = 0;
        foreach ($konta as $konto) {
            $this->setAllegroClinent($konto);
            $allItemsToChange = $this->AllegroAukcja->find('all')->where(['AllegroAukcja.allegro_konto_id' => $konto->id, 'AllegroAukcja.removed' => 0, 'AllegroAukcja.priceToUpdate >' => 0]);
            foreach ($allItemsToChange as $itemToChange) {
                $queryId = $this->allegroChangePrice($itemToChange->item_id, $itemToChange->priceToUpdate);
                if ($queryId) {
//                    $itemToChange->set('priceToUpdate', 0);
                    $this->AllegroAukcja->updateAll(['priceToUpdate' => 0, 'cenaQueryId' => $queryId], ['id' => $itemToChange->id]);
//                    $this->AllegroAukcja->save($itemToChange);
                    $allCount++;
                }
            }
        }
        return ['status' => 'stop', 'message' => __('Zaktualizowano ceny na allegro {0}', [$allCount])];
    }

    private function allegroChangeAvil($itemId, $ilosc = 0) {
        if (!empty($itemId)) {
            $offer = $this->allegroGetOffer($itemId);
            if (!empty($offer)) {
                $typ = 'GAIN';
                $actIlosc = $offer['stock']['available'];
                $offerStatus = $offer['publication']['status'];
                $tmpIlosc = $actIlosc + $ilosc;
                if ($tmpIlosc <= 0) {
                    $endData = [
                        'publication' => [
                            'action' => 'END'
                        ],
                        'offerCriteria' => [[
                        'type' => 'CONTAINS_OFFERS',
                        'offers' => [['id' => $itemId]]
                            ]]
                    ];
                    $ilosc = 0;
                    $typ = 'FIXED';
                    return $this->allegroPublicationRequest($endData);
                } else {
                    if ($ilosc > 0) {
                        $typ = 'FIXED';
                    }
                    $dataToChange = [
                        'modification' => [
                            'changeType' => $typ,
                            'value' => $ilosc
                        ],
                        'offerCriteria' => [[
                        'type' => 'CONTAINS_OFFERS',
                        'offers' => [['id' => $itemId]]
                            ]]
                    ];

                    Log::write('info', $dataToChange, ['cronInfo']);
                    if ($typ == 'FIXED' && $ilosc > 0 && $offerStatus == 'ENDED' && !empty($this->konto->automaticRenew)) {
                        $queryId = $this->allegroUpdateRequest($dataToChange);
                        if ($queryId) {
                            return ['queryId' => $queryId, 'renew' => true];
                        } else {
                            return false;
                        }
                    } else {
                        return $this->allegroUpdateRequest($dataToChange);
                    }
                }
            } else {
                Log::write('info', 'Avil update offer get EMPTY ' . $itemId, ['cronInfo']);
                return false;
            }
        } else {
            return false;
        }
    }

    private function allegroRenew($itemId) {
        $startData = [
            'publication' => [
                'action' => 'ACTIVATE'
            ],
            'offerCriteria' => [[
            'type' => 'CONTAINS_OFFERS',
            'offers' => [['id' => $itemId]]
                ]]
        ];
        return $this->allegroPublicationRequest($startData);
    }

    private function allegroChangePrice($itemId, $price = 0, $currency = 'PLN') {
        if (!empty($itemId) && $price > 0) {
            $offer = $this->allegroGetOffer($itemId);
            if (!empty($offer)) {
                $typ = 'FIXED_PRICE';
                $actPrice = $offer['sellingMode']['price']['amount'];
                if ($actPrice != $price) {
                    $dataToChange = [
                        'modification' => [
                            'type' => $typ,
                            'price' => [
                                'amount' => $price,
                                'currency' => $currency
                            ]
                        ],
                        'offerCriteria' => [[
                        'type' => 'CONTAINS_OFFERS',
                        'offers' => [['id' => $itemId]]
                            ]]
                    ];
                    return $this->allegroUpdatePriceRequest($dataToChange);
                } else {
                    return true;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function allegroOrders($page = 1, $limit = 100, $status = null, $lastRun = null, $nowRun = null) {
        $params = [
            'offset' => (($page - 1) * $limit),
            'imit' => $limit,
            'status' => $status
        ];
        if (!empty($lastRun)) {
            $lastRun = date('Y-m-d', strtotime($lastRun));
            $params['lineItems.boughtAt.gte'] = $lastRun . 'T00:00:00.000Z';
        }
        $orders = $this->allegroOrderRequest($params);
//        Log::write('info', $orders, ['cronInfo']);
        $newOrdersCount = 0;
        if (!empty($orders) && !empty($orders['checkoutForms'])) {
            $prepareOrders = [];
            $iloscUpdate = [];
            foreach ($orders['checkoutForms'] as $order) {
                $iloscUpdate[$order['id']] = [];
                $orderStatus = (!empty($order['payment']['finishedAt']) ? 'oplacone' : 'zlozone');
                $prepareOrders[$order['id']] = [
                    'allegro_konto_id' => $this->konto->id,
                    'order_id' => $order['id'],
                    'status' => $orderStatus,
                    'uwagi' => (key_exists('messageToSeller', $order) ? $order['messageToSeller'] : ''),
                    'kupujacy_id' => $order['buyer']['id'],
                    'kupujacy_email' => $order['buyer']['email'],
                    'kupujacy_login' => $order['buyer']['login'],
                    'kupujacy_guest' => $order['buyer']['guest'],
                    'kupujacy_telefon' => $order['buyer']['phoneNumber'],
                    'payment_id' => (key_exists('payment', $order) ? (key_exists('id', $order['payment']) ? $order['payment']['id'] : null) : null),
                    'payment_type' => (key_exists('payment', $order) ? (key_exists('type', $order['payment']) ? $order['payment']['type'] : null) : null),
                    'payment_provider' => (key_exists('payment', $order) ? (key_exists('provider', $order['payment']) ? $order['payment']['provider'] : null) : null),
                    'payment_finished_at' => (key_exists('payment', $order) ? (!empty($order['payment']['finishedAt']) ? date('Y-m-d H:i:s', strtotime($order['payment']['finishedAt'])) : null) : null),
                    'payment_wplata_kwota' => (key_exists('payment', $order) ? (!empty($order['payment']['paidAmount']) ? $order['payment']['paidAmount']['amount'] : null) : null),
                    'payment_wplata_waluta' => (key_exists('payment', $order) ? (!empty($order['payment']['paidAmount']) ? $order['payment']['paidAmount']['currency'] : null) : null),
                    'allegro_status' => $order['status'],
                    'allegro_wysylka_id' => $order['delivery']['method']['id'],
                    'allegro_wysylka_nazwa' => $order['delivery']['method']['name'],
                    'allegro_wysylka_koszt' => $order['delivery']['cost']['amount'],
                    'allegro_wysylka_waluta' => $order['delivery']['cost']['currency'],
                    'allegro_wysylka_smart' => $order['delivery']['smart'],
                    'faktura' => $order['invoice']['required'],
                    'wartosc_razem' => $order['summary']['totalToPay']['amount'],
                    'waluta' => $order['summary']['totalToPay']['currency'],
                    'get_date' => date('Y-m-d H:i:s')
                ];

                if (!empty($order['delivery']['address'])) {
                    $prepareOrders[$order['id']]['wysylka_imie'] = $order['delivery']['address']['firstName'];
                    $prepareOrders[$order['id']]['wysylka_nazwisko'] = $order['delivery']['address']['lastName'];
                    $prepareOrders[$order['id']]['wysylka_ulica'] = $order['delivery']['address']['street'];
                    $prepareOrders[$order['id']]['wysylka_miasto'] = $order['delivery']['address']['city'];
                    $prepareOrders[$order['id']]['wysylka_kod'] = $order['delivery']['address']['zipCode'];
                    $prepareOrders[$order['id']]['wysylka_kraj'] = $order['delivery']['address']['countryCode'];
                    $prepareOrders[$order['id']]['wysylka_telefon'] = $order['delivery']['address']['phoneNumber'];
                }
                if (!empty($order['delivery']['pickupPoint'])) {
                    $prepareOrders[$order['id']]['paczkomat_id'] = $order['delivery']['pickupPoint']['id'];
                    $prepareOrders[$order['id']]['paczkomat_nazwa'] = $order['delivery']['pickupPoint']['name'];
                    $prepareOrders[$order['id']]['paczkomat_opis'] = $order['delivery']['pickupPoint']['description'];
                    $prepareOrders[$order['id']]['paczkomat_adres'] = (!empty($order['delivery']['pickupPoint']['address']) ? $order['delivery']['pickupPoint']['address']['street'] . "\r\n" . $order['delivery']['pickupPoint']['address']['zipCode'] . ' ' . $order['delivery']['pickupPoint']['address']['city'] : null);
                }
                if (!empty($order['invoice']['address'])) {
                    $prepareOrders[$order['id']]['faktura_ulica'] = $order['invoice']['address']['street'];
                    $prepareOrders[$order['id']]['faktura_miasto'] = $order['invoice']['address']['city'];
                    $prepareOrders[$order['id']]['faktura_kod'] = $order['invoice']['address']['zipCode'];
                    $prepareOrders[$order['id']]['faktura_kraj'] = $order['invoice']['address']['countryCode'];
                    $prepareOrders[$order['id']]['faktura_firma'] = (!empty($order['invoice']['address']['company']) ? $order['invoice']['address']['company']['name'] : null);
                    $prepareOrders[$order['id']]['faktura_nip'] = (!empty($order['invoice']['address']['company']) ? $order['invoice']['address']['company']['taxId'] : null);
                }
                $dataZakupu = null;
                foreach ($order['lineItems'] as $orderItem) {
                    $towarId = (!empty($orderItem['offer']['external']) ? $orderItem['offer']['external']['id'] : null);
                    $ilosc = $orderItem['quantity'];
                    if (!key_exists($orderItem['offer']['id'], $iloscUpdate[$order['id']])) {
                        $iloscUpdate[$order['id']][$orderItem['offer']['id']] = $ilosc;
                    } else {
                        $iloscUpdate[$order['id']][$orderItem['offer']['id']] += $ilosc;
                    }

                    $dataZakupu = date('Y-m-d H:i:s', strtotime($orderItem['boughtAt']));
                    $prepareOrders[$order['id']]['allegro_zamowienie_towar'][] = [
                        'towar_id' => $towarId,
                        'item_id' => $orderItem['offer']['id'],
                        'buy_id' => $orderItem['id'],
                        'nazwa' => $orderItem['offer']['name'],
                        'cena_podstawowa_za_sztuke' => $orderItem['originalPrice']['amount'],
                        'waluta' => $orderItem['price']['currency'],
                        'cena_za_sztuke' => $orderItem['price']['amount'],
                        'ilosc' => $ilosc,
                        'data_zakupu' => $dataZakupu,
                    ];
                }
                $prepareOrders[$order['id']]['data_zakupu'] = $dataZakupu;
            }
            if (!empty($prepareOrders)) {
                if (empty($this->AllegroZamowienie)) {
                    $this->loadModel('AllegroZamowienie');
                }
                $orderIds = array_keys($prepareOrders);
                $extOrders = $this->AllegroZamowienie->find('list', ['keyField' => 'id', 'valueField' => 'order_id'])->where(['order_id IN' => $orderIds])->toArray();
                if (!empty($extOrders)) {
                    foreach ($extOrders as $extOrderId) {
                        unset($prepareOrders[$extOrderId]);
                        unset($iloscUpdate[$extOrderId]);
                    }
                }
            }
            if (!empty($prepareOrders)) {
                foreach ($prepareOrders as $newOrder) {
                    $orderEntity = $this->AllegroZamowienie->newEntity($newOrder);
                    if ($this->AllegroZamowienie->save($orderEntity)) {
                        $newOrdersCount++;
                    } else {
                        Log::write(LOG_ERR, __('Allegro order error: {0}', $newOrder['id']));
                        Log::write(LOG_ERR, $orderEntity->errors());
                    }
                }
            }
            if (!empty($iloscUpdate)) {
                $allegroIloscToCahnge = [];
                if (empty($this->AllegroAukcja)) {
                    $this->loadModel('AllegroAukcja');
                }
                foreach ($iloscUpdate as $allegroOrderId => $itemsToChange) {
                    foreach ($itemsToChange as $itemId => $changeIlosc) {
                        $towar = $this->AllegroAukcja->find('all')->where(['AllegroAukcja.item_id' => $itemId])->first();
                        if (!empty($towar)) {
                            $towar_id = $towar->towar_id;
                            if (!key_exists($towar_id, $allegroIloscToCahnge)) {
                                $allegroIloscToCahnge[$towar_id] = 0;
                            }
                            $allegroIloscToCahnge[$towar_id] += $changeIlosc;
//        Log::write('info', ['changeAllegroOrder'=>[$allegroOrderId=>[$itemId=>$changeIlosc]]], ['cronInfo']);
                            if (!empty($towar->iloscToUpdate) && $towar->iloscToUpdate > 0) {
                                $towar->iloscToUpdate = ($towar->iloscToUpdate - $changeIlosc);
                                $this->AllegroAukcja->save($towar);
                            }
                        }
                    }
                }
                if (!empty($allegroIloscToCahnge)) {
                    if (empty($this->Towar)) {
                        $this->loadModel('Towar');
                    }
                    $actIlosci = $this->Towar->find('list', ['keyField' => 'id', 'valueField' => 'ilosc'])->where(['id IN' => array_keys($allegroIloscToCahnge)])->toArray();
                    $actPreorderIlosci = $this->Towar->find('list', ['keyField' => 'id', 'valueField' => 'preorder_limit'])->where(['preorder_limit IS NOT NULL', 'id IN' => array_keys($allegroIloscToCahnge)])->toArray();

                    foreach ($allegroIloscToCahnge as $towarId => $iloscToChange) {
                        $newIlosc = ($actIlosci[$towarId] - $iloscToChange);
                        if ($newIlosc < 0) {
                            $newIlosc = 0;
                        }
                        $upData = ['ilosc' => $newIlosc];
                        if (!empty($actPreorderIlosci) && !empty($actPreorderIlosci[$towarId])) {
                            $upData['preorder_limit'] = ($actPreorderIlosci[$towarId] - $iloscToChange);
                            if ($upData['preorder_limit'] < 0) {
                                $upData['preorder_limit'] = 0;
                            }
                        }
                        Log::write('info', __('AllegroOrder: {0} - Zmiana ilości w sklepie {3} z {1} na {2}', [date('Y-m-d H:i:s'), $actIlosci[$towarId], $newIlosc, $towarId]), ['cronInfo']);
                        $this->Towar->updateAll($upData, ['id' => $towarId]);
                    }
                }
            }
            if ($orders['count'] > $limit) {
                return $this->allegroOrders(($page + 1), $limit, $status, $lastRun, $nowRun);
            }
            return $newOrdersCount;
        }
        return false;
    }

    private $credentials;
    private $client;
    private $token;
    private $konto;

    private function setAllegroClinent($konto) {
        if (!empty($konto)) {
            $this->credentials = new \Imper86\AllegroApi\Credentials([
                'restClientId' => $konto->devClientId,
                'restClientSecret' => $konto->devClientSecret,
                'restApiKey' => $konto->devClientId,
                'restRedirectUri' => $konto->restRedirectUri,
                'soapApiKey' => $konto->soapApiKey,
            ]);
            $this->client = new \Imper86\AllegroApi\RestClient($this->credentials);
            $this->token = \Cake\Cache\Cache::read('allegro_' . $konto->id . '_dev_token', 'allegro');
            $this->konto = $konto;
            if (empty($this->token)) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    private function parseData($data) {
        if (empty($data) || !is_array($data)) {
            return $data;
        }
        $tmp = [];
        foreach ($data as $key => $value) {
            $tmp[] = $key . '=' . $value;
        }
        return join('&', $tmp);
    }

    private function allegroOrderRequest($params = null) {

        $ch = curl_init();
        $url = 'https://api.' . \Cake\Core\Configure::read('allegroLink') . '/order/checkout-forms';
        if (!empty($params)) {
            $queryValue = $this->parseData($params);
            if (!empty($queryValue)) {
                $url .= '?' . $queryValue;
            }
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        $authorization = "Authorization: Bearer " . $this->token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, [$authorization, 'Accept: application/vnd.allegro.beta.v1+json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($ch);
        $results = json_decode($res, true);
        curl_close($ch);

        if (!empty($results['errors']) || !empty($results['error'])) {
            if (!empty($results['error']) && $results['error'] == 'invalid_token') {
                $this->allegroToken();
            }
            return false;
        }
        return $results;
    }

    private function allegroUpdatePriceRequest($data = null) {
        $commandId = $this->allegroGetCommandId();
        $ch = curl_init();
        $url = 'https://api.' . \Cake\Core\Configure::read('allegroLink') . '/sale/offer-price-change-commands/' . $commandId;
        curl_setopt($ch, CURLOPT_URL, $url);
        $authorization = "Authorization: Bearer " . $this->token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, [$authorization, 'Content-Type: application/vnd.allegro.public.v1+json', 'accept: application/vnd.allegro.public.v1+json']);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($ch);
        $results = json_decode($res, true);
        curl_close($ch);
        if (!empty($results['errors']) || !empty($results['error'])) {
            if (!empty($results['error']) && $results['error'] == 'invalid_token') {
                $this->allegroToken();
            }
            return false;
        }
        if (!empty($results['id'])) {
            return $commandId;
        } else {
            return false;
        }
    }

    private function allegroCheckPriceStatus($commandId) {
        $ch = curl_init();
        $url = 'https://api.' . \Cake\Core\Configure::read('allegroLink') . '/sale/offer-price-change-commands/' . $commandId . '/tasks';
        curl_setopt($ch, CURLOPT_URL, $url);
        $authorization = "Authorization: Bearer " . $this->token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, [$authorization, 'Content-Type: application/vnd.allegro.public.v1+json', 'accept: application/vnd.allegro.public.v1+json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($ch);
        $results = json_decode($res, true);
        curl_close($ch);
        $return = [];
        if (!empty($results['tasks'])) {
            $return = [
                'status' => $results['tasks'][0]['status'],
                'message' => $results['tasks'][0]['message']
            ];
        }
        if (empty($return)) {
            return FALSE;
        }
        return $return;
    }

    private function allegroCheckRenewStatus($commandId) {
        $ch = curl_init();
        $url = 'https://api.' . \Cake\Core\Configure::read('allegroLink') . '/sale/offer-publication-commands/' . $commandId . '/tasks';
        curl_setopt($ch, CURLOPT_URL, $url);
        $authorization = "Authorization: Bearer " . $this->token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, [$authorization, 'Content-Type: application/vnd.allegro.public.v1+json', 'accept: application/vnd.allegro.public.v1+json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($ch);
        $results = json_decode($res, true);
        curl_close($ch);
        $return = [];
        if (!empty($results['tasks'])) {
            $return = [
                'status' => $results['tasks'][0]['status'],
                'message' => $results['tasks'][0]['message']
            ];
        }
        if (empty($return)) {
            return FALSE;
        }
        return $return;
    }

    private function allegroCheckAvilStatus($commandId) {
        $ch = curl_init();
        $url = 'https://api.' . \Cake\Core\Configure::read('allegroLink') . '/sale/offer-quantity-change-commands/' . $commandId . '/tasks';
        curl_setopt($ch, CURLOPT_URL, $url);
        $authorization = "Authorization: Bearer " . $this->token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, [$authorization, 'Content-Type: application/vnd.allegro.public.v1+json', 'accept: application/vnd.allegro.public.v1+json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($ch);
        $results = json_decode($res, true);
        curl_close($ch);

        Log::write('info', 'Update ilosc status results:', ['cronInfo']);
        Log::write('info', $results, ['cronInfo']);
        $return = [];
        if (!empty($results['tasks'])) {
            $return = [
                'status' => $results['tasks'][0]['status'],
                'message' => $results['tasks'][0]['message'],
                'scheduledAt' => $results['tasks'][0]['scheduledAt'],
                'finishedAt' => $results['tasks'][0]['finishedAt'],
                'code' => (!empty($results['tasks'][0]['errors']) ? $results['tasks'][0]['errors'][0]['code'] : null),
            ];
        }
        if (empty($return)) {
            return FALSE;
        }
        return $return;
    }

    private function allegroUpdateRequest($data = null) {
        $commandId = $this->allegroGetCommandId();
        $ch = curl_init();
        $url = 'https://api.' . \Cake\Core\Configure::read('allegroLink') . '/sale/offer-quantity-change-commands/' . $commandId;
        curl_setopt($ch, CURLOPT_URL, $url);
        $authorization = "Authorization: Bearer " . $this->token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, [$authorization, 'Content-Type: application/vnd.allegro.public.v1+json', 'accept: application/vnd.allegro.public.v1+json']);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($ch);
        $results = json_decode($res, true);
        curl_close($ch);
        if (!empty($results['errors']) || !empty($results['error'])) {
            if (!empty($results['error']) && $results['error'] == 'invalid_token') {
                $this->allegroToken();
            }
            return false;
        }
        if (!empty($results['id'])) {
            return $commandId;
        } else {
            return false;
        }
    }

    private function allegroPublicationRequest($data = null) {
        $commandId = $this->allegroGetCommandId();
        $ch = curl_init();
        $url = 'https://api.' . \Cake\Core\Configure::read('allegroLink') . '/sale/offer-publication-commands/' . $commandId;
        curl_setopt($ch, CURLOPT_URL, $url);
        $authorization = "Authorization: Bearer " . $this->token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, [$authorization, 'Content-Type: application/vnd.allegro.public.v1+json', 'accept: application/vnd.allegro.public.v1+json']);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($ch);
        $results = json_decode($res, true);
        curl_close($ch);
        if (!empty($results['errors']) || !empty($results['error'])) {
            if (!empty($results['error']) && $results['error'] == 'invalid_token') {
                $this->allegroToken();
            }
            return false;
        }
        if (!empty($results['id'])) {
            return $commandId;
        } else {
            return false;
        }
    }

    private function allegroGetOffer($itemId) {
        $ch = curl_init();
        $url = 'https://api.' . \Cake\Core\Configure::read('allegroLink') . '/sale/offers/' . $itemId;
        curl_setopt($ch, CURLOPT_URL, $url);
        $authorization = "Authorization: Bearer " . $this->token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, [$authorization, 'Content-Type: application/vnd.allegro.public.v1+json', 'accept: application/vnd.allegro.public.v1+json']);
//        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($ch);
        $results = json_decode($res, true);
        curl_close($ch);
        if (!empty($results['errors']) || !empty($results['error'])) {
            if (!empty($results['error']) && $results['error'] == 'invalid_token') {
                $this->allegroToken();
            }
            if (!empty($results['errors'][0]['code']) && mb_strtolower($results['errors'][0]['code']) == 'not_found') {
                if (empty($this->AllegroAukcja)) {
                    $this->loadModel('AllegroAukcja');
                }
                $this->AllegroAukcja->updateAll(['removed' => 1], ['item_id' => $itemId]);
                Log::write('info', 'GetOffer ERROR Results ' . $itemId . ': ', ['cronInfo']);
                Log::write('info', $results, ['cronInfo']);
            }
            return false;
        }

        if (!empty($results['id'])) {
            return $results;
        } else {
            return false;
        }
    }

    private function allegroGetCommandId() {
        $uId = \Ramsey\Uuid\Uuid::uuid4();
        return $uId->toString();
    }

    private function allegroToken() {
        $oldToken = \Cake\Cache\Cache::read('allegro_' . $this->konto->id . '_dev_refresh_token', 'allegro');
        $url = 'https://' . \Cake\Core\Configure::read('allegroLink') . '/auth/oauth/token?grant_type=refresh_token&refresh_token=' . $oldToken . '&redirect_uri=' . $this->konto->restRedirectUri;
//        $url = 'https://' . \Cake\Core\Configure::read('allegroLink') . '/auth/oauth/token?grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Adevice_code&device_code=' . $konto->device_code
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        $authorization = "Authorization: Basic " . base64_encode($this->konto->devClientId . ':' . $this->konto->devClientSecret);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [$authorization]); //, 'Content-Type: application/x-www-form-urlencoded'
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
//            curl_setopt($ch, CURLOPT_POSTFIELDS, "client_id={$this->konto->devClientId}");
        $res = curl_exec($ch);
        $results = json_decode($res, true);
        curl_close($ch);
        if (!empty($results['access_token'])) {
            \Cake\Cache\Cache::write('allegro_' . $this->konto->id . '_dev_token', $results['access_token'], 'allegro');
            \Cake\Cache\Cache::write('allegro_' . $this->konto->id . '_dev_refresh_token', $results['refresh_token'], 'allegro');
            return true;
        } else {
            return false;
        }
    }

    public function porownywarki($type = 'ceneo') {
        $this->autoRender = false;
        $results = '';
        switch ($type) {
            case 'ceneo' : {
                    $results = $this->ceneoXml();
                } break;
            case 'google' : {
                    $results = $this->googleMerchantXml();
                } break;
            case 'skapiec' : {
                    $results = $this->skapiecXml();
                } break;
        }
        if (!empty($results)) {
            $this->Flash->success($results['message']);
        } else {
            $this->Flash->error($this->Txt->printAdmin(__('Admin | Nieprawidłowe polecenie')));
        }
        return $this->redirect($this->request->referer());
    }

    private function ceneoXml() {
        set_time_limit(0);
        $wynenerowano = 0;
        $limit = 1000;
        $this->autoRender = false;
        if (strpos($_SERVER['SERVER_NAME'], 'www.') === 0) {
            $filename = str_replace('.', '_', str_replace('www.', '', $_SERVER['SERVER_NAME'])) . '_ceneo.xml';
        } else {
            $filename = str_replace('.', '_', $_SERVER['SERVER_NAME']) . '_ceneo.xml';
        }
        $nextPage = 0;
        $xmlstr = '';
        $page = 1;

        if ($page == 1) {
            $i = 1;
            $xmlstr = '<?xml version="1.0" encoding="utf-8"?>
<offers xmlns:xsi="www.w3.org/2001/XMLSchema-instance" version="1">';
            if (file_exists($this->filePath['porownywarki'] . $filename)) {
                unlink($this->filePath['porownywarki'] . $filename);
            }
            file_put_contents($this->filePath['porownywarki'] . $filename, $xmlstr);
            $xmlstr = '';
        } else {
            if (!empty($nextPage) && $nextPage != $page) {
                $page = $nextPage;
            }
        }
        $end = false;
        $dataNow = date('Y-m-d H:i:s');
        while (!$end) {
            $pagin = (($page - 1) * $limit) . ',' . $limit;
            $fields = "t.id, t.nazwa, t.ilosc, t.opis, k.nazwa as 'kategoria',k.sciezka, tz.plik, tz.id as 'zdjecie_id', t.promocja, tc.cena_promocja,tc.cena_sprzedazy, tc.rodzaj as 'cena_rodzaj',"
                    . "if((hd.cena IS NOT NULL AND hd.aktywna = 1 AND hd.ilosc > 0 AND hd.data_do >= '$dataNow' AND (hd.data_od <= '$dataNow' OR hd.data_od IS NULL)),hd.cena,if(((t.promocja = 1 OR t.wyprzedaz=1 OR t.produkt_tygodnia=1) AND tc.cena_promocja > 0),tc.cena_promocja,tc.cena_sprzedazy)) as 'cena', "
                    . "p.nazwa as 'producent_nazwa',t.ean,t.kod,t.waga, t.wyprzedaz, vat.stawka as 'stawka_vat', jednostka.jednostka as 'jednostka' ";
            $query = "SELECT $fields FROM `towar` t 
INNER JOIN towar_kategoria tk on tk.towar_id = t.id 
INNER JOIN kategoria k ON tk.kategoria_id = k.id 
INNER JOIN towar_cena tc ON tc.towar_id=t.id
LEFT JOIN hot_deal hd ON t.id=hd.towar_id 
LEFT JOIN producent p ON p.id = t.producent_id 
LEFT JOIN towar_zdjecie tz ON tz.towar_id=t.id AND tz.domyslne=1 
LEFT JOIN vat ON tc.vat_id = vat.id 
LEFT JOIN jednostka ON tc.jednostka_id = jednostka.id 
WHERE t.ukryty = 0 AND t.ilosc > 0 AND t.p_ceneo = 1 GROUP BY t.id ORDER BY t.id ASC 
LIMIT $pagin";

            $connection = ConnectionManager::get('default');
            $towary = $connection->execute($query)->fetchAll('assoc');
            if (!empty($towary)) {
                foreach ($towary as $item) {
                    $wynenerowano++;
                    if ($item['ilosc'] > 0)
                        $avail = 1;
                    else
                        $avail = 99;
                    $addUrl = htmlspecialchars('?utm_source=Ceneo&utm_medium=feed&utm_campaign=Ceneo&utm_term=' . $item['kod']);
                    $cena_sp = round($this->Txt->cenaVat(round($item['cena'], 2), $item['stawka_vat'], $item['cena_rodzaj'], 'brutto', false, false), 2);
                    $xmlstr .= "\r\n" . '<o id="' . $item['id'] . '" url="' . str_replace('&', '&amp;', $this->Txt->towarViewUrl($item, true)) . $addUrl . '" price="' . $cena_sp . '" avail="' . $avail . '"  weight="' . (!empty($item['waga']) ? trim($item['waga']) : '0.01') . '">';
                    $xmlstr .= "\r\n" . '<name><![CDATA[' . htmlspecialchars(substr($item['nazwa'], 0, 150)) . ']]></name>';
                    $xmlstr .= "\r\n" . '<cat><![CDATA[' . htmlspecialchars(substr((!empty($item['kategoria']) ? $item['sciezka'] : ''), 0, 750)) . ']]></cat>';
                    if (!empty($item['plik'])) {
                        $xmlstr .= "\r\n" . '<imgs>';
                        $xmlstr .= "\r\n" . '<main url="' . (!empty($item['plik']) ? ('https://' . $_SERVER['SERVER_NAME'] . '/towar/image/' . $item['zdjecie_id'] . '/thumb_3_' . $item['plik'] . '/0') : 'https://' . $_SERVER['SERVER_NAME'] . '/img/noPhoto.png') . '"/>';
                        $xmlstr .= "\r\n" . '</imgs>';
                    }
                    $xmlstr .= "\r\n" . '<desc><![CDATA[' . htmlspecialchars(strip_tags($item['opis'], '<br />')) . ']]></desc>';
                    $xmlstr .= "\r\n" . '<attrs>';
                    if (!empty($item['producent_nazwa'])) {
                        $xmlstr .= "\r\n" . '<a name="Producent"><![CDATA[' . htmlspecialchars($item['producent_nazwa']) . ']]></a>';
                    }
                    if (!empty(trim($item['ean']))) {
                        $xmlstr .= "\r\n" . '<a name="EAN"><![CDATA[' . $item['ean'] . ']]></a>';
                    }
                    if (!empty(trim($item['kod']))) {
                        $xmlstr .= "\r\n" . '<a name="Kod_producenta"><![CDATA[' . $item['kod'] . ']]></a>';
                    }
                    $xmlstr .= "\r\n" . '</attrs>';
                    $xmlstr .= "\r\n" . '</o>';
                }
                file_put_contents($this->filePath['porownywarki'] . $filename, $xmlstr, FILE_APPEND);
                $xmlstr = '';
                $page++;
            } else {
                $end = true;
            }
        }
        $xmlstr = "\r\n" . '</offers>';
        file_put_contents($this->filePath['porownywarki'] . $filename, $xmlstr, FILE_APPEND);
        $xmlstr = '';
        return ['status' => 'stop', 'message' => __('{0} - Integracja Ceneo XML wygenerowano {1} pozycji do pliku {2}', [date('Y-m-d H:i:s'), $wynenerowano, $filename])];
    }

    private function googleMerchantXml() {
        $wynenerowano = 0;
        $limit = 1000;
        $this->autoRender = false;
        if (strpos($_SERVER['SERVER_NAME'], 'www.') === 0) {
            $filename = str_replace('.', '_', str_replace('www.', '', $_SERVER['SERVER_NAME'])) . '_google.xml';
        } else {
            $filename = str_replace('.', '_', $_SERVER['SERVER_NAME']) . '_google.xml';
        }
        $nextPage = 0;
        $xmlstr = '';
        $page = 1;
        if ($page == 1) {
            $i = 1;
            $xmlstr = '<?xml version="1.0"?' . '>
<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">
    <channel>
<title>' . Configure::read('dane.nazwa_firmy') . '</title>
		<link>' . \Cake\Routing\Router::url(['controller' => 'Home', 'action' => 'index'], true) . '</link>
		<description><![CDATA[' . Configure::read('seo.title') . ']]></description>';
            if (file_exists($this->filePath['porownywarki'] . $filename)) {
                unlink($this->filePath['porownywarki'] . $filename);
            }
            file_put_contents($this->filePath['porownywarki'] . $filename, $xmlstr);
            $xmlstr = '';
        } else {
            if (!empty($nextPage) && $nextPage != $page) {
                $page = $nextPage;
            }
        }
        $end = false;
        $dataNow = date('Y-m-d H:i:s');
        while (!$end) {
            $pagin = (($page - 1) * $limit) . ',' . $limit;
            $fields = "t.id, t.nazwa, t.ilosc, t.opis, k.google_kategoria_id,k.nazwa as 'kategoria',k.sciezka, tz.plik, tz.id as 'zdjecie_id', t.promocja, tc.cena_promocja,tc.cena_sprzedazy, tc.rodzaj as 'cena_rodzaj', "
                    . "if((hd.cena IS NOT NULL AND hd.aktywna = 1 AND hd.ilosc > 0 AND hd.data_do >= '$dataNow' AND (hd.data_od <= '$dataNow' OR hd.data_od IS NULL)),hd.cena,if(((t.promocja = 1 OR t.wyprzedaz=1 OR t.produkt_tygodnia=1) AND tc.cena_promocja > 0),tc.cena_promocja,tc.cena_sprzedazy)) as 'cena', "
                    . "p.nazwa as 'producent_nazwa',t.ean,t.kod,t.waga, t.wyprzedaz, vat.stawka as 'stawka_vat', jednostka.jednostka as 'jednostka' ";
            $query = "SELECT $fields FROM `towar` t 
INNER JOIN towar_kategoria tk on tk.towar_id = t.id 
INNER JOIN kategoria k ON tk.kategoria_id = k.id 
INNER JOIN towar_cena tc ON tc.towar_id=t.id 
LEFT JOIN hot_deal hd ON t.id=hd.towar_id 
LEFT JOIN producent p ON p.id = t.producent_id 
LEFT JOIN towar_zdjecie tz ON tz.towar_id=t.id AND tz.domyslne=1 
LEFT JOIN vat ON tc.vat_id = vat.id 
LEFT JOIN jednostka ON tc.jednostka_id = jednostka.id 
WHERE t.ukryty = 0 AND t.ilosc > 0 AND t.p_google = 1 GROUP BY t.id ORDER BY t.id ASC 
LIMIT $pagin";

            $connection = ConnectionManager::get('default');
            $towary = $connection->execute($query)->fetchAll('assoc');
            if (!empty($towary)) {
                foreach ($towary as $item) {
                    $wynenerowano++;
                    $xmlstr .= "\r\n" . ' <item>';
                    $xmlstr .= "\r\n" . '     <g:id>' . $item['id'] . '</g:id>';
                    $xmlstr .= "\r\n" . '     <g:title><![CDATA[' . $item['nazwa'] . ']]></g:title>';
                    $xmlstr .= "\r\n" . '     <g:description><![CDATA[' . $item['opis'] . ']]></g:description>';
                    $xmlstr .= "\r\n" . '     <g:google_product_category>' . (!empty($item['google_kategoria_id']) ? $item['google_kategoria_id'] : '1279') . '</g:google_product_category>';
                    $xmlstr .= "\r\n" . '     <g:product_type><![CDATA[' . $item['sciezka'] . ']]></g:product_type>';
                    $xmlstr .= "\r\n" . '     <g:link><![CDATA[' . $this->Txt->towarViewUrl($item, true) . ']]></g:link>';
                    $xmlstr .= "\r\n" . '     <g:image_link><![CDATA[' . (!empty($item['plik']) ? (\Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'image', $item['zdjecie_id'], 'thumb_3_' . $item['plik'], 0], true)) : 'https://' . $_SERVER['SERVER_NAME'] . '/img/noPhoto.png') . ']]></g:image_link>';
                    $xmlstr .= "\r\n" . '     <g:condition>new</g:condition>';
                    $xmlstr .= "\r\n" . '     <g:availability>in stock</g:availability>'; //(($item['ilosc'] > 0) ? 'in stock' : 'out of stock')
                    $xmlstr .= "\r\n" . '     <g:price>' . round($this->Txt->cenaVat(round($item['cena'], 2), $item['stawka_vat'], $item['cena_rodzaj'], 'brutto', false, false), 2) . ' PLN' . '</g:price>';
                    $xmlstr .= "\r\n" . '     <g:brand><![CDATA[' . (!empty($item['producent_nazwa']) ? $item['producent_nazwa'] : '') . ']]></g:brand>';
                    $xmlstr .= "\r\n" . '     <g:gtin><![CDATA[' . (!empty($item['ean']) ? $item['ean'] : '') . ']]></g:gtin>';
                    $xmlstr .= "\r\n" . '     <g:mpn><![CDATA[' . (!empty($item['kod']) ? substr(trim($item['kod']), 0, 70) : '') . ']]></g:mpn>';
//                    $xmlstr .= "\r\n" . '     <g:shipping_weight><![CDATA[' . (!empty($item['waga']) ? trim($item['waga']) : '0.01') . ' kg' . ']]></g:shipping_weight>';

                    $xmlstr .= "\r\n" . ' </item>';
                }
                file_put_contents($this->filePath['porownywarki'] . $filename, $xmlstr, FILE_APPEND);
                $xmlstr = '';
                $page++;
            } else {
                $end = true;
            }
        }
        $xmlstr = "\r\n" . '    </channel>';
        $xmlstr .= "\r\n" . '</rss>';
        file_put_contents($this->filePath['porownywarki'] . $filename, $xmlstr, FILE_APPEND);
        $xmlstr = '';
        return ['status' => 'stop', 'message' => __('{0} - Integracja Google XML wygenerowano {1} pozycji do pliku {2}', [date('Y-m-d H:i:s'), $wynenerowano, $filename])];
    }

    private function skapiecXml() {
        set_time_limit(0);
        $limit = 1000;
        $this->autoRender = false;
        $wynenerowano = 0;
        if (strpos($_SERVER['SERVER_NAME'], 'www.') === 0) {
            $filename = str_replace('.', '_', str_replace('www.', '', $_SERVER['SERVER_NAME'])) . '_skapiec.xml';
        } else {
            $filename = str_replace('.', '_', $_SERVER['SERVER_NAME']) . '_skapiec.xml';
        }
        $nextPage = 0;
        $xmlstr = '';
        $page = 1;

        if ($page == 1) {
            $i = 1;
            $xmlstr = '<?xml version="1.0" encoding="utf-8"?' . '>
				<xmldata>
					<version>12.0</version>
					<header>
						<name>' . Configure::read('dane.nazwa_firmy') . '</name>
						<www>https://' . $_SERVER['SERVER_NAME'] . '</www>
						<time>' . date('Y-m-d') . '</time>
					</header>';
            if (file_exists($this->filePath['porownywarki'] . $filename)) {
                unlink($this->filePath['porownywarki'] . $filename);
            }
            $query = 'SELECT * FROM kategoria WHERE kategoria.ukryta = 0;';
            $connection = ConnectionManager::get('default');
            $kategorie = $connection->execute($query)->fetchAll('assoc');
            if (count($kategorie) > 0) {
                $xmlstr .= '<category>';
                foreach ($kategorie as $kategoria) {
                    $xmlstr .= '<catitem>';
                    $xmlstr .= '<catid>' . $kategoria['id'] . '</catid>';
                    $xmlstr .= '<catname><![CDATA[' . htmlspecialchars($kategoria['nazwa']) . ']]></catname>';
                    $xmlstr .= '</catitem>';
                }
                $xmlstr .= '</category>';
            }

            $xmlstr .= '<data>';
            file_put_contents($this->filePath['porownywarki'] . $filename, $xmlstr);
            $xmlstr = '';
        } else {
            if (!empty($nextPage) && $nextPage != $page) {
                $page = $nextPage;
            }
        }
        $end = false;
        $dataNow = date('Y-m-d H:i:s');
        while (!$end) {
            $pagin = (($page - 1) * $limit) . ',' . $limit;
            $fields = "t.id, t.nazwa, t.ilosc, t.opis, k.id as 'kategoria_id', k.nazwa as 'kategoria',k.sciezka, tz.plik, tz.id as 'zdjecie_id', t.promocja, tc.cena_promocja,tc.cena_sprzedazy, tc.rodzaj as 'cena_rodzaj', "
                    . "if((hd.cena IS NOT NULL AND hd.aktywna = 1 AND hd.ilosc > 0 AND hd.data_do >= '$dataNow' AND (hd.data_od <= '$dataNow' OR hd.data_od IS NULL)),hd.cena,if(((t.promocja = 1 OR t.wyprzedaz=1 OR t.produkt_tygodnia=1) AND tc.cena_promocja > 0),tc.cena_promocja,tc.cena_sprzedazy)) as 'cena', "
                    . "p.nazwa as 'producent_nazwa',t.ean,t.kod,t.waga, t.wyprzedaz, vat.stawka as 'stawka_vat', jednostka.jednostka as 'jednostka' ";
            $query = "SELECT $fields FROM `towar` t 
INNER JOIN towar_kategoria tk on tk.towar_id = t.id 
INNER JOIN kategoria k ON tk.kategoria_id = k.id 
INNER JOIN towar_cena tc ON tc.towar_id=t.id 
LEFT JOIN hot_deal hd ON t.id=hd.towar_id 
LEFT JOIN producent p ON p.id = t.producent_id 
LEFT JOIN towar_zdjecie tz ON tz.towar_id=t.id AND tz.domyslne=1 
LEFT JOIN vat ON tc.vat_id = vat.id 
LEFT JOIN jednostka ON tc.jednostka_id = jednostka.id 
WHERE t.ukryty = 0 AND t.ilosc > 0 AND t.p_skapiec = 1 GROUP BY t.id ORDER BY t.id ASC 
LIMIT $pagin";

            $connection = ConnectionManager::get('default');
            $towary = $connection->execute($query)->fetchAll('assoc');
            if (!empty($towary)) {
                foreach ($towary as $item) {
                    $wynenerowano++;
                    if ($item['ilosc'] > 0)
                        $avail = 2;
                    else
                        $avail = -1;
                    $addUrl = '?utm_source=Skapiec&utm_medium=feed&utm_campaign=Skapiec&utm_term=' . $item['kod'];
                    $cena_sp = round($this->Txt->cenaVat(round($item['cena'], 2), $item['stawka_vat'], $item['cena_rodzaj'], 'brutto', false, false), 2);

                    $xmlstr .= "\r\n" . '<item> <compid>' . $item['id'] . '</compid>';
                    $xmlstr .= "\r\n" . '<vendor><![CDATA[' . $item['producent_nazwa'] . ']]></vendor>';
                    $xmlstr .= "\r\n" . '<name><![CDATA[' . $item['nazwa'] . ']]></name>';
                    $xmlstr .= "\r\n" . '<price>' . $cena_sp . '</price>';
                    $xmlstr .= "\r\n" . '<partnr>' . $item['kod'] . '</partnr>';
                    $xmlstr .= "\r\n" . '<catid>' . $item['kategoria_id'] . '</catid>';
                    $xmlstr .= "\r\n" . '<foto><![CDATA[';
                    if (!empty($item['plik'])) {
                        $xmlstr .= "\r\n" . (!empty($item['plik']) ? ('https://' . $_SERVER['SERVER_NAME'] . '/towar/image/' . $item['zdjecie_id'] . '/thumb_3_' . $item['plik'] . '/0') : 'https://' . $_SERVER['SERVER_NAME'] . '/img/noPhoto.png');
                    }
                    $xmlstr .= "\r\n" . ']]></foto>';
                    $xmlstr .= "\r\n" . '<desclong><![CDATA[' . $item['opis'] . ']]></desclong>';
                    $xmlstr .= "\r\n" . '<availability>' . $avail . '</availability>';
                    if (!empty($item['ean'])) {
                        $xmlstr .= "\r\n" . '<ean>' . $item['ean'] . '</ean>';
                    }
                    $xmlstr .= "\r\n" . '<category><![CDATA[' . $item['sciezka'] . ']]></category>';
                    $xmlstr .= "\r\n" . '<producer><![CDATA[' . $item['producent_nazwa'] . ']]></producer>';
                    if (!empty($item->ean)) {
                        $xmlstr .= "\r\n" . '<property name = "EAN">' . $item['ean'] . '</property>';
                    }
                    $xmlstr .= "\r\n" . '<url><![CDATA[' . $this->Txt->towarViewUrl($item, true) . $addUrl . ']]></url>';
                    $xmlstr .= "\r\n" . '</item>';
                }
                file_put_contents($this->filePath['porownywarki'] . $filename, $xmlstr, FILE_APPEND);
                $xmlstr = '';
                $page++;
            } else {
                $end = true;
            }
        }
        $xmlstr = "\r\n" . '</data>' . "\r\n" . '</xmldata>';
        file_put_contents($this->filePath['porownywarki'] . $filename, $xmlstr, FILE_APPEND);
        $xmlstr = '';
        return ['status' => 'stop', 'message' => __('{0} - Integracja Skąpiec XML wygenerowano {1} pozycji do pliku {2}', [date('Y-m-d H:i:s'), $wynenerowano, $filename])];
    }

    private function integracjaPtakmodahurt($type = 1) {
        $xmlAddr = 'https://ptakmodahurt.pl/xmlfiles/products.xml';
        $xml = new \SimpleXMLElement($xmlAddr, LIBXML_NOCDATA, true);
        $byArr = $this->xmlToArray($xml);
        if (!empty($byArr['products']['product']['name'])) {
            $byArr['products']['product'] = [$byArr['products']['product']];
        }
        switch ($type) {
            case 1: {
                    $allItems = array_chunk($byArr['products']['product'], 1000);
                    foreach ($allItems as $items) {
                        $this->addNewItemsFromPtakmodahurt($items);
                    }
                }break;
            case 2: {
                    
                }break;
        }

        return ['status' => 'stop', 'message' => __('Integriacja IKONKA {0}: wariant {1}', [date('Y-m-d H:i:s'), $type])];
    }

    private function integracjaCustomXml($type = 1) {
$allItems = Cache::read('import');
if(empty($allItems)){
    $fileName='product_part_0_1.yml';
    if(!empty($_GET['file'])){
        $fileName=$_GET['file'];
    }
        $path = $this->filePath['webroot'] . $fileName;
        $yaml = new \Symfony\Component\Yaml\Yaml();
        $allItems=$yaml->parseFile($path);
                    $allItems = array_chunk($allItems, 1000);
            Cache::write('import', $allItems);
}


        switch ($type) {
            case 1: {
                    var_dump(count($allItems));
                    foreach ($allItems as $key => $items) {
                        var_dump(count($items));
                        $this->addNewItemsFromCustomXml($items);
                        unset($allItems[$key]);
                        Cache::write('import', $allItems);
                        $this->dbReconnect();
                    }
                    Cache::delete('import');
                }break;
            case 2: {
                    
                }break;
        }

        return ['status' => 'stop', 'message' => __('Integriacja IKONKA {0}: wariant {1}', [date('Y-m-d H:i:s'), $type])];
    }

    private function prepareKategoryTreeFormArray($kategorie = [], $parentId = null, $tree = [], $beforeParents = []) {
        if (!empty($kategorie)) {
            foreach ($kategorie as $katKey => $katVal) {
                if (empty($parentId) && empty($katVal['parent_id']) && empty($tree[$katVal['id']])) {
                    $tree[$katVal['id']] = [
                        'id' => $katVal['id'],
                        'nazwa' => $katVal['nazwa']
                    ];
                    $tree[$katVal['id']]['sub'] = $this->prepareKategoryTreeFormArray($kategorie, $katVal['id'], $tree);
                } else {
                    if (!empty($parentId)) {
                        $beforeParents[$parentId] = $parentId;
                        if (!empty($katVal['parent_id']) && $katVal['parent_id'] == $parentId) {
                            $tree[$parentId]['sub'][$katVal['id']] = [
                                'id' => $katVal['id'],
                                'nazwa' => $katVal['nazwa']
                            ];
                            $tree[$parentId]['sub'][$katVal['id']]['sub'] = $this->prepareKategoryTreeFormArray($kategorie, $katVal['id'], $tree, $beforeParents);
                        }
                    }
                }
            }
        }
        return $tree;
    }

    private function buildTree(array &$elements, $parentId = 0) {

        $branch = array();
        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[$element['id']] = $element;
            }
        }
        return $branch;
    }

    private function katsPath($kats) {
        foreach ($kats as $kat) {
            if (!empty($kat['parent_id'])) {
                $this->katPath[$kat['id']] = array_merge($this->katPath[$kat['parent_id']], [$kat['nazwa']]);
            } else {
                $this->katPath[$kat['id']][] = $kat['nazwa'];
            }
            if (!empty($kat['children'])) {
                $this->katsPath($kat['children']);
            }
        }
        return $this->katPath;
    }

    private function integracjaVmp($type = 1) {
        $xmlAddr = 'https://www.vmp.com.pl/hurt/HVMP_full.xml';
        $xml = new \SimpleXMLElement($xmlAddr, LIBXML_NOCDATA, true);
        $byArr = $this->xmlToArray($xml);
        $kategoryMappTmp = [];
        foreach ($byArr['ROOT']['CATEGORIES']['CATEGORY'] as $kat) {
            $kategoryMappTmp[$kat['@category_id']] = ['id' => $kat['@category_id'], 'nazwa' => $kat['$'], 'parent_id' => (!empty($kat['@parent_id']) ? $kat['@parent_id'] : 0)];
        }
        $fullCatTree = $this->buildTree($kategoryMappTmp);
        $this->katPath = [];
        $catPath = $this->katsPath($fullCatTree);
        foreach ($catPath as $katId => $katNames) {
            $catPath[$katId] = join('/', $katNames);
        }
        $this->remoteKatMap = $catPath;
        $colors = [];
        foreach ($byArr['ROOT']['COLORS']['COLOR'] as $colorData) {
            $colors[$colorData['@color_id']] = $colorData['$'];
        }
        $this->remoteColorMap = $colors;
        $sizes = [];
        foreach ($byArr['ROOT']['SIZES']['SIZE'] as $sizeData) {
            $sizes[$sizeData['@size_id']] = $sizeData['$'];
        }
        $this->remoteSizeMap = $sizes;
        if (!empty($byArr['ROOT']['PRODUCTS']['PRODUCT']['@product_id'])) {
            $byArr['ROOT']['PRODUCTS']['PRODUCT'] = [$byArr['ROOT']['PRODUCTS']['PRODUCT']];
        }
        switch ($type) {
            case 1: {
                    $allItems = array_chunk($byArr['ROOT']['PRODUCTS']['PRODUCT'], 1000);
                    foreach ($allItems as $items) {
                        $this->addNewItemsFromVmp($items);
                    }
                }break;
            case 2: {
                    
                }break;
        }

        return ['status' => 'stop', 'message' => __('Integriacja IKONKA {0}: wariant {1}', [date('Y-m-d H:i:s'), $type])];
    }

    private function integracjaMolos($type = 1) {
        $xmlAddr = 'http://molos.com.pl/data/UserFiles/File/przyklad.xml';
        $xml = new \SimpleXMLElement($xmlAddr, LIBXML_NOCDATA, true);
        $byArr = $this->xmlToArray($xml);
        if (!empty($byArr['PRODUCTS']['PRODUCT']['ID'])) {
            $byArr['PRODUCTS']['PRODUCT'] = [$byArr['PRODUCTS']['PRODUCT']];
        }
        switch ($type) {
            case 1: {
                    $allItems = array_chunk($byArr['PRODUCTS']['PRODUCT'], 1000);
                    foreach ($allItems as $items) {
                        $this->addNewItemsFromMolos($items);
                    }
                }break;
            case 2: {
                    
                }break;
        }

        return ['status' => 'stop', 'message' => __('Integriacja IKONKA {0}: wariant {1}', [date('Y-m-d H:i:s'), $type])];
    }

    private function integracjaGatito($type = 1) {
        $xmlAddr = 'https://gatito.pl/offer/products.xml';
        $xml = new \SimpleXMLElement($xmlAddr, LIBXML_NOCDATA, true);
        $arrayData = $this->xmlToArray($xml);
        if (!empty($arrayData['offers']['o']['@id'])) {
            $arrayData['offers']['o'] = [$arrayData['offers']['o']];
        }
        switch ($type) {
            case 1: {
                    $allItems = array_chunk($arrayData['offers']['o'], 1000);
                    foreach ($allItems as $items) {
                        $this->addNewItemsFromGatito($items);
                    }
                }break;
            case 2: {
                    
                }break;
        }

        return ['status' => 'stop', 'message' => __('Integriacja IKONKA {0}: wariant {1}', [date('Y-m-d H:i:s'), $type])];
    }

    private function integracjaCentralazabawek($type = 1) {
        $xmlAddr = r('/cz_4685508379_products.xml', true);
        $xml = new \SimpleXMLElement($xmlAddr, LIBXML_NOCDATA, true);
        $byArr = json_decode(json_encode($xml), true);
        if (!empty($byArr['product']['id'])) {
            $byArr['product'] = [$byArr['product']];
        }
        switch ($type) {
            case 1: {
                    $allItems = array_chunk($byArr['product'], 1000);
                    foreach ($allItems as $items) {
                        $this->addNewItemsFromCentralazabawek($items);
                    }
                }break;
            case 2: {
                    
                }break;
        }

        return ['status' => 'stop', 'message' => __('Integriacja IKONKA {0}: wariant {1}', [date('Y-m-d H:i:s'), $type])];
    }

    private function integracjaIkonka($variant = 1) {
        $url = 'https://api.ikonka.com.pl/api2/index.php/request/?format=json&hash=377a3fd53a2bdf10eefa0ac3c71f3d33352559d2&variant=' . $variant;

        $allItems = json_decode(file_get_contents($url), true);
        if (!empty($allItems)) {
            switch ($variant) {
                case 1 : {
                        $allItems = array_chunk($allItems, 1000);
                        foreach ($allItems as $items) {
                            $this->addNewItemsFromIkonka($items);
                        }
                    } break;
                case 2 : {
                        $this->updateItemsFromIkonka($allItems);
                    } break;
            }
        }

        return ['status' => 'stop', 'message' => __('Integriacja IKONKA {0}: wariant {1}', [date('Y-m-d H:i:s'), $variant])];
    }

    private function addNewItemsFromIkonka($allItems) {
        $this->loadModel('KategoriaMap');
        $this->loadModel('Producent');
        $this->loadModel('Vat');
        $vatByStawka = $this->Vat->find('list', ['keyField' => 'stawka', 'valueField' => 'id'])->toArray();
        $this->loadModel('Jednostka');
        $jednostkiByName = $this->Jednostka->find('list', ['keyField' => 'jednostka', 'valueField' => 'id'])->toArray();
        $kategoryMapp = $this->KategoriaMap->find('list', ['keyField' => 'l_nazwa', 'valueField' => 'kategoria_id'])->select(['kategoria_id', 'l_nazwa' => 'LOWER(nazwa)'])->where(['hurtownia' => 'ikonka'])->toArray();
        $producentMapp = $this->Producent->find('list', ['keyField' => 'l_nazwa', 'valueField' => 'id'])->select(['id', 'l_nazwa' => 'LOWER(nazwa_ikonka)'])->where(['nazwa_ikonka IS NOT' => null])->toArray();
        $prepareData = [];
        foreach ($allItems as $item) {
            $key = $item[array_search('kod', $this->fieldMap)];
            if (empty($key)) {
                continue;
            }
            $prepareData[$key]['hurtownia'] = 'ikonka';
            foreach ($this->fieldMap as $remoteField => $localField) {
                if (!empty($localField)) {
                    if ($localField == 'jednostka') {
                        if (!key_exists($item[$remoteField], $jednostkiByName)) {
                            $newJ = $this->Jednostka->newEntity(['jednostka' => $item[$remoteField]]);
                            if ($this->Jednostka->save($newJ)) {
                                $jednostkiByName[$newJ->jednostka] = $newJ->id;
                            }
                        }
                        $prepareData[$key]['towar_cena_default']['jednostka_id'] = $jednostkiByName[$item[$remoteField]];
                    } elseif ($localField == 'vat_stawka') {
                        if (!key_exists($item[$remoteField], $vatByStawka)) {
                            $newVat = $this->Vat->newEntity(['stawka' => $item[$remoteField], 'nazwa' => $item[$remoteField] . '%']);
                            if ($this->Vat->save($newVat)) {
                                $vatByStawka[$newVat->stawka] = $newVat->id;
                            }
                        }
                        $prepareData[$key]['towar_cena_default']['vat_id'] = $vatByStawka[$item[$remoteField]];
                    } elseif ($localField == 'cena_katalogowa' || $localField == 'cena_sprzedazy') {
                        $prepareData[$key]['towar_cena_default'][$localField] = $item[$remoteField];
                    } elseif ($localField == 'zdjecia' && !empty($item[$remoteField])) {
                        foreach ($item[$remoteField] as $image) {
                            $prepareData[$key]['towar_zdjecie'][] = [
                                'remote_url' => $image['value']
                            ];
                        }
                    } elseif ($localField == 'kategoria_nazwa') {
                        $katVal = mb_strtolower($item[$remoteField]);
                        if (!empty($kategoryMapp) && key_exists($katVal, $kategoryMapp)) {
                            $prepareData[$key]['kategoria']['_ids'][$kategoryMapp[$katVal]] = $kategoryMapp[$katVal];
                        }
                    } else {
                        $prepareData[$key][$localField] = $item[$remoteField];
                    }
                }
            }
            if (empty($prepareData[$key]['towar_cena_default']['jednostka_id'])) {
                $prepareData[$key]['towar_cena_default']['jednostka_id'] = 1;
            }
            $prepareData[$key]['towar_cena_default']['waluta_id'] = 1;
        }
        if (!empty($prepareData)) {
            $this->saveTowar($prepareData, 'ikonka');
        }
    }

    private function addNewItemsFromCentralazabawek($allItems) {
        $this->loadModel('KategoriaMap');
        $this->loadModel('Producent');
        $this->loadModel('Vat');
        $vatByStawka = $this->Vat->find('list', ['keyField' => 'stawka', 'valueField' => 'id'])->toArray();
        $this->loadModel('Jednostka');
        $jednostkiByName = $this->Jednostka->find('list', ['keyField' => 'jednostka', 'valueField' => 'id'])->toArray();
        $kategoryMapp = $this->KategoriaMap->find('list', ['keyField' => 'l_nazwa', 'valueField' => 'kategoria_id'])->select(['kategoria_id', 'l_nazwa' => 'LOWER(nazwa)'])->where(['hurtownia' => 'centralazabawek'])->toArray();
        $producentMapp = $this->Producent->find('list', ['keyField' => 'l_nazwa', 'valueField' => 'id'])->select(['id', 'l_nazwa' => 'LOWER(nazwa_centralazabawek)'])->where(['nazwa_centralazabawek IS NOT' => null])->toArray();
        $prepareData = [];
        foreach ($allItems as $item) {
            $key = $item[array_search('kod', $this->fieldMap)];
            if (empty($key)) {
                continue;
            }
            $prepareData[$key]['hurtownia'] = 'centralazabawek';
            foreach ($this->fieldMap as $remoteField => $localField) {
                if (!empty($localField)) {
                    if ($localField == 'waga') {
                        if (!empty($item[$remoteField])) {
                            $prepareData[$key][$localField] = ((float) trim($item[$remoteField]) / 1000);
                        }
                    } elseif ($localField == 'jednostka') {
                        if (!key_exists($item[$remoteField], $jednostkiByName)) {
                            $newJ = $this->Jednostka->newEntity(['jednostka' => $item[$remoteField]]);
                            if ($this->Jednostka->save($newJ)) {
                                $jednostkiByName[$newJ->jednostka] = $newJ->id;
                            }
                        }
                        $prepareData[$key]['towar_cena_default']['jednostka_id'] = $jednostkiByName[$item[$remoteField]];
                    } elseif ($localField == 'vat_stawka') {
                        $tmpStawki = [
                            'A' => '23',
                            'B' => '8',
                            'C' => '0',
                            'D' => '5',
                            'E' => 'zw'
                        ];
                        if (!key_exists($tmpStawki[$item[$remoteField]], $vatByStawka)) {
                            $newVat = $this->Vat->newEntity(['stawka' => $tmpStawki[$item[$remoteField]], 'nazwa' => $tmpStawki[$item[$remoteField]] . '%']);
                            if ($this->Vat->save($newVat)) {
                                $vatByStawka[$newVat->stawka] = $newVat->id;
                            }
                        }
                        $prepareData[$key]['towar_cena_default']['vat_id'] = $vatByStawka[$tmpStawki[$item[$remoteField]]];
                    } elseif ($localField == 'cena_katalogowa' || $localField == 'cena_sprzedazy') {
                        $prepareData[$key]['towar_cena_default'][$localField] = trim($item[$remoteField]);
                    } elseif ($localField == 'zdjecia' && !empty($item[$remoteField])) {
                        foreach ($item[$remoteField] as $image) {
                            if (is_array($image)) {
                                foreach ($image as $imgSrc) {
                                    $prepareData[$key]['towar_zdjecie'][] = [
                                        'remote_url' => trim($imgSrc)
                                    ];
                                }
                            } elseif (!empty($image)) {
                                $prepareData[$key]['towar_zdjecie'][] = [
                                    'remote_url' => trim($image)
                                ];
                            }
                        }
                    } elseif ($localField == 'kategoria_nazwa') {
                        $katVal = (is_string($item[$remoteField]) ? mb_strtolower($item[$remoteField]) : null);
                        if (!empty($kategoryMapp) && key_exists($katVal, $kategoryMapp)) {
                            $prepareData[$key]['kategoria']['_ids'][$kategoryMapp[$katVal]] = $kategoryMapp[$katVal];
                        }
                    } elseif ($localField == 'producent_nazwa') {
                        $prodVal = (is_string($item[$remoteField]) ? mb_strtolower($item[$remoteField]) : null);
                        if (!empty($producentMapp) && key_exists($prodVal, $producentMapp)) {
                            $prepareData[$key]['producent_id'] = $producentMapp[$prodVal];
                        }
                    } else {
                        $prepareData[$key][$localField] = (is_string($item[$remoteField]) ? trim($item[$remoteField]) : $item[$remoteField]);
                    }
                }
            }
            if (empty($prepareData[$key]['towar_cena_default']['jednostka_id'])) {
                $prepareData[$key]['towar_cena_default']['jednostka_id'] = 1;
            }
            if (!((float) $prepareData[$key]['towar_cena_default']['cena_sprzedazy']) > 0) {
                $prepareData[$key]['towar_cena_default']['cena_sprzedazy'] = $prepareData[$key]['towar_cena_default']['cena_katalogowa'];
            }
            $prepareData[$key]['towar_cena_default']['waluta_id'] = 1;
        }
        if (!empty($prepareData)) {
            $this->saveTowar($prepareData, 'centralazabawek');
        }
    }

    private function addNewItemsFromGatito($allItems) {
        $this->loadModel('KategoriaMap');
        $this->loadModel('Producent');
        $this->loadModel('Vat');
        $vatByStawka = $this->Vat->find('list', ['keyField' => 'stawka', 'valueField' => 'id'])->toArray();
        $this->loadModel('Jednostka');
        $jednostkiByName = $this->Jednostka->find('list', ['keyField' => 'jednostka', 'valueField' => 'id'])->toArray();
        $kategoryMapp = $this->KategoriaMap->find('list', ['keyField' => 'l_nazwa', 'valueField' => 'kategoria_id'])->select(['kategoria_id', 'l_nazwa' => 'LOWER(nazwa)'])->where(['hurtownia' => 'gatito'])->toArray();
        $producentMapp = $this->Producent->find('list', ['keyField' => 'l_nazwa', 'valueField' => 'id'])->select(['id', 'l_nazwa' => 'LOWER(nazwa_gatito)'])->where(['nazwa_gatito IS NOT' => null])->toArray();
        $prepareData = [];
        foreach ($allItems as $item) {
            $attrMap = [];
            if (!empty($item['attrs'])) {
                foreach ($item['attrs']['a'] as $attr) {
                    $attrMap[$attr['@name']] = $attr['$'];
                }
            }
            if (!empty($attrMap)) {
                foreach ($attrMap as $attrName => $attrValue) {
                    if (key_exists('attr_' . $attrName, $this->fieldMap)) {
                        $item['attr_' . $attrName] = $attrValue;
                        unset($attrMap[$attrName]);
                    }
                }
            }
            $key = $item[array_search('kod', $this->fieldMap)];
            if (empty($key)) {
                continue;
            }
            $prepareData[$key]['hurtownia'] = 'gatito';
            foreach ($this->fieldMap as $remoteField => $localField) {
                if (!empty($localField)) {
                    if ($localField == 'atrybuty') {
                        
                    } elseif ($localField == 'cena_katalogowa' || $localField == 'cena_sprzedazy') {
                        $prepareData[$key]['towar_cena_default'][$localField] = trim($item[$remoteField]);
                    } elseif ($localField == 'zdjecia' && !empty($item[$remoteField])) {
                        if (!empty($item[$remoteField]['main'])) {
                            $imgUrl = trim($item[$remoteField]['main']);
                            if (strpos($imgUrl, 'https://') === false) {
                                $imgUrl = 'https://' . $imgUrl;
                            }
                            $prepareData[$key]['towar_zdjecie'][] = ['remote_url' => $imgUrl];
                        }
                        if (!empty($item[$remoteField]['o'])) {
                            foreach ($item[$remoteField]['o'] as $image) {
                                if (is_array($image)) {
                                    foreach ($image as $imgSrc) {
                                        $imgUrl = trim($imgSrc);
                                        if (strpos($imgUrl, 'https://') === false) {
                                            $imgUrl = 'https://' . $imgUrl;
                                        }
                                        $prepareData[$key]['towar_zdjecie'][] = [
                                            'remote_url' => $imgUrl
                                        ];
                                    }
                                } elseif (!empty($image)) {
                                    $imgUrl = trim($image);
                                    if (strpos($imgUrl, 'https://') === false) {
                                        $imgUrl = 'https://' . $imgUrl;
                                    }
                                    $prepareData[$key]['towar_zdjecie'][] = [
                                        'remote_url' => $imgUrl
                                    ];
                                }
                            }
                        }
                    } elseif ($localField == 'kategoria_nazwa') {
                        $katVal = (is_string($item[$remoteField]) ? mb_strtolower($item[$remoteField]) : null);
                        if (!empty($kategoryMapp) && key_exists($katVal, $kategoryMapp)) {
                            $prepareData[$key]['kategoria']['_ids'][$kategoryMapp[$katVal]] = $kategoryMapp[$katVal];
                        }
                    } elseif ($localField == 'producent_nazwa') {
                        $prodVal = (is_string($item[$remoteField]) ? mb_strtolower($item[$remoteField]) : null);
                        if (!empty($prodVal) && !empty($producentMapp) && key_exists($prodVal, $producentMapp)) {
                            $prepareData[$key]['producent_id'] = $producentMapp[$prodVal];
                        }
                    } else {
                        $prepareData[$key][$localField] = (is_string($item[$remoteField]) ? trim($item[$remoteField]) : $item[$remoteField]);
                    }
                }
            }
            if (empty($prepareData[$key]['towar_cena_default']['jednostka_id'])) {
                $prepareData[$key]['towar_cena_default']['jednostka_id'] = 1;
            }
            if (empty($prepareData[$key]['towar_cena_default']['vat_id'])) {
                $prepareData[$key]['towar_cena_default']['vat_id'] = 1;
            }
            if (!((float) $prepareData[$key]['towar_cena_default']['cena_sprzedazy']) > 0) {
                $prepareData[$key]['towar_cena_default']['cena_sprzedazy'] = $prepareData[$key]['towar_cena_default']['cena_katalogowa'];
            }
            $prepareData[$key]['towar_cena_default']['waluta_id'] = 1;
//            if(!empty($attrMap) && empty($prepareData[$key]['opis'])){
//                $tmpOpis=[];
//                foreach ($attrMap as $attrName => $attrVal){
//                    $tmpOpis[]=$attrName.': '.$attrVal;
//                }
//                $prepareData[$key]['opis']=join('<br>',$tmpOpis);
//            }
        }
        if (!empty($prepareData)) {
            $this->saveTowar($prepareData, 'gatito');
        }
    }

    private function addNewItemsFromVmp($allItems) {
        $this->loadModel('KategoriaMap');
        $this->loadModel('Producent');
        $this->loadModel('Vat');
        $this->loadModel('Rozmiar');
        $this->loadModel('Kolor');
        $vatByStawka = $this->Vat->find('list', ['keyField' => 'stawka', 'valueField' => 'id'])->toArray();
        $this->loadModel('Jednostka');
        $jednostkiByName = $this->Jednostka->find('list', ['keyField' => 'jednostka', 'valueField' => 'id'])->toArray();
        $kategoryMapp = $this->KategoriaMap->find('list', ['keyField' => 'l_nazwa', 'valueField' => 'kategoria_id'])->select(['kategoria_id', 'l_nazwa' => 'LOWER(nazwa)'])->where(['hurtownia' => 'vmp'])->toArray();
        $producentMapp = $this->Producent->find('list', ['keyField' => 'l_nazwa', 'valueField' => 'id'])->select(['id', 'l_nazwa' => 'LOWER(nazwa_vmp)'])->where(['nazwa_vmp IS NOT' => null])->toArray();
        $rozmiarMap = $this->Rozmiar->find('list', ['keyField' => 'l_nazwa', 'valueField' => 'id'])->select(['id', 'l_nazwa' => 'LOWER(nazwa)'])->toArray();
        $kolorMap = $this->Kolor->find('list', ['keyField' => 'l_nazwa', 'valueField' => 'id'])->select(['id', 'l_nazwa' => 'LOWER(nazwa)'])->toArray();
        $prepareData = [];
        foreach ($allItems as $item) {
            $key = $item[array_search('kod', $this->fieldMap)];
            if (empty($key)) {
                continue;
            }
            $prepareData[$key]['hurtownia'] = 'vmp';
            foreach ($this->fieldMap as $remoteField => $localField) {
                if (!empty($localField)) {
                    if ($localField == 'waga') {
                        if (!empty($item[$remoteField])) {
                            $prepareData[$key][$localField] = ((float) trim($item[$remoteField]) / 1000);
                        }
                    } elseif ($localField == 'jednostka') {
                        if (!key_exists($item[$remoteField], $jednostkiByName)) {
                            $newJ = $this->Jednostka->newEntity(['jednostka' => $item[$remoteField]]);
                            if ($this->Jednostka->save($newJ)) {
                                $jednostkiByName[$newJ->jednostka] = $newJ->id;
                            }
                        }
                        $prepareData[$key]['towar_cena_default']['jednostka_id'] = $jednostkiByName[$item[$remoteField]];
                    } elseif ($localField == 'vat_stawka') {
                        if (!key_exists($item[$remoteField], $vatByStawka)) {
                            $newVat = $this->Vat->newEntity(['stawka' => $item[$remoteField], 'nazwa' => $item[$remoteField] . '%']);
                            if ($this->Vat->save($newVat)) {
                                $vatByStawka[$newVat->stawka] = $newVat->id;
                            }
                        }
                        $prepareData[$key]['towar_cena_default']['vat_id'] = $vatByStawka[$item[$remoteField]];
                    } elseif ($localField == 'cena_katalogowa' || $localField == 'cena_sprzedazy') {
                        $prepareData[$key]['towar_cena_default'][$localField] = trim($item[$remoteField]);
                    } elseif ($localField == 'zdjecia' && !empty($item[$remoteField])) {
                        foreach ($item[$remoteField] as $image) {
                            if (!empty($image['@no'])) {
                                $image = [$image];
                            }
                            foreach ($image as $imgSrc) {
                                $prepareData[$key]['towar_zdjecie'][] = [
                                    'remote_url' => trim($imgSrc['LARGE'])
                                ];
                            }
                        }
                    } elseif ($localField == 'kategoria_nazwa') {
                        $catName = $this->remoteKatMap[$item[$remoteField]['CATEGORY'][0]];
                        $katVal = (is_string($catName) ? mb_strtolower($catName) : null);
                        if (!empty($kategoryMapp) && key_exists($katVal, $kategoryMapp)) {
                            $prepareData[$key]['kategoria']['_ids'][$kategoryMapp[$katVal]] = $kategoryMapp[$katVal];
                        }
                    } elseif ($localField == 'producent_nazwa') {
                        $prodVal = (is_string($item[$remoteField]) ? mb_strtolower($item[$remoteField]) : null);
                        if (!empty($producentMapp) && key_exists($prodVal, $producentMapp)) {
                            $prepareData[$key]['producent_id'] = $producentMapp[$prodVal];
                        }
                    } elseif ($localField == 'warianty') {
                        $remoteWariants = $item[$remoteField];
                        if (!empty($remoteWariants['COLOR']['@color_id'])) {
                            $remoteWariants['COLOR'] = [$remoteWariants['COLOR']];
                        }
                        $warianty = [];
                        $sumIlosc = 0;
                        foreach ($remoteWariants['COLOR'] as $wariantByColor) {
                            $ilosc = 0;
                            $magazyn = null;
                            $kolorVal = $this->remoteColorMap[$wariantByColor['@color_id']];
                            $kolorId = null;
                            if (key_exists(mb_strtolower($kolorVal), $kolorMap)) {
                                $kolorId = $kolorMap[mb_strtolower($kolorVal)];
                            } else {
                                $newKolor = $this->Kolor->newEntity(['nazwa' => $kolorVal]);
                                if ($this->Kolor->save($newKolor)) {
                                    $kolorId = $newKolor->id;
                                    $kolorMap[mb_strtolower($kolorVal)] = $newKolor->id;
                                }
                            }
                            $sizeId = null;
                            if (!empty($wariantByColor['SIZE'])) {
                                if (!empty($wariantByColor['SIZE']['@size_id'])) {
                                    $wariantByColor['SIZE'] = [$wariantByColor['SIZE']];
                                }
                                foreach ($wariantByColor['SIZE'] as $sizeItem) {
                                    $sizeVal = $this->remoteSizeMap[$sizeItem['@size_id']];
                                    if (!key_exists(mb_strtolower($sizeVal), $rozmiarMap)) {
                                        $newRozmiar = $this->Rozmiar->newEntity(['nazwa' => $sizeVal]);
                                        if ($this->Rozmiar->save($newRozmiar)) {
                                            $rozmiarMap[mb_strtolower($sizeVal)] = $newRozmiar->id;
                                        }
                                    }
                                    $sizeId = $rozmiarMap[mb_strtolower($sizeVal)];
                                    $ilosc = $sizeItem['@quantity'];
                                    $sumIlosc += $ilosc;
                                    $magazyn = $sizeItem['@stock'];
                                    $ean = (!empty($sizeItem['@ean']) ? $sizeItem['@ean'] : null);
                                    if (key_exists($kolorId . '_' . $sizeId, $warianty)) {
                                        $warianty[$kolorId . '_' . $sizeId]['ilosc'] += $ilosc;
                                    } else {
                                        $warianty[$kolorId . '_' . $sizeId] = ['kolor_id' => $kolorId, 'rozmiar_id' => $sizeId, 'ilosc' => $ilosc, 'ean' => $ean];
                                    }
                                }
                            }
                        }
                        $prepareData[$key]['ilosc'] = $sumIlosc;
                        $prepareData[$key]['towar_wariant'] = $warianty;
                    } else {
                        $prepareData[$key][$localField] = (is_string($item[$remoteField]) ? trim($item[$remoteField]) : $item[$remoteField]);
                    }
                }
            }
            if (empty($prepareData[$key]['towar_cena_default']['jednostka_id'])) {
                $prepareData[$key]['towar_cena_default']['jednostka_id'] = 1;
            }
            if (empty($prepareData[$key]['towar_cena_default']['vat_id'])) {
                $prepareData[$key]['towar_cena_default']['vat_id'] = 1;
            }
            if (!((float) $prepareData[$key]['towar_cena_default']['cena_sprzedazy']) > 0) {
                $prepareData[$key]['towar_cena_default']['cena_sprzedazy'] = $prepareData[$key]['towar_cena_default']['cena_katalogowa'];
            }
            $prepareData[$key]['towar_cena_default']['waluta_id'] = 1;
            if (empty($prepareData[$key]['hurt_id'])) {
                $prepareData[$key]['hurt_id'] = $key;
            }
            if (empty($prepareData[$key]['ilosc'])) {
                $prepareData[$key]['ilosc'] = 0;
            }
        }
        if (!empty($prepareData)) {
            $this->saveTowar($prepareData, 'vmp');
        }
    }

    private function addNewItemsFromCustomXml($allItems) {
        $this->loadModel('KategoriaMap');
        $this->loadModel('Producent');
        $this->loadModel('Vat');
        $this->loadModel('Jednostka');
        $this->loadModel('Kolor');
        $this->loadModel('Rozmiar');
        $vatByStawka = $this->Vat->find('list', ['keyField' => 'stawka', 'valueField' => 'id'])->toArray();
        $jednostkiByName = $this->Jednostka->find('list', ['keyField' => 'jednostka', 'valueField' => 'id'])->toArray();
        $kategoryMapp = $this->KategoriaMap->find('list', ['keyField' => 'l_nazwa', 'valueField' => 'kategoria_id'])->select(['kategoria_id', 'l_nazwa' => 'LOWER(nazwa)'])->where(['hurtownia' => 'stary_sklep'])->toArray();
        $rozmiarMap = $this->Rozmiar->find('list', ['keyField' => 'l_nazwa', 'valueField' => 'id'])->select(['id', 'l_nazwa' => 'LOWER(nazwa)'])->toArray();
        $kolorMap = $this->Kolor->find('list', ['keyField' => 'l_nazwa', 'valueField' => 'id'])->select(['id', 'l_nazwa' => 'LOWER(nazwa)'])->toArray();
        $prepareData = [];
        foreach ($allItems as $item) {
            if(!empty($item['images'])){
                $item['images'] = explode(',', $item['images']);
            }
            $key = $item[array_search('kod', $this->fieldMap)];
            if (empty($key)) {
            $key = $item[array_search('hurt_id', $this->fieldMap)];
            }
            if (empty($key)) {
                
                continue;
            }
            $prepareData[$key]['hurtownia'] = 'stary_sklep';
            $kolorName = $rozmiarName = '';
            foreach ($this->fieldMap as $remoteField => $localField) {
                if (!empty($localField)) {
                    if ($localField == 'waga') {
                        if (!empty($item[$remoteField])) {
                            $prepareData[$key][$localField] = ((float) trim($item[$remoteField]) / 1000);
                        }
                    } elseif ($localField == 'jednostka') {
                        if (!key_exists($item[$remoteField], $jednostkiByName)) {
                            $newJ = $this->Jednostka->newEntity(['jednostka' => $item[$remoteField]]);
                            if ($this->Jednostka->save($newJ)) {
                                $jednostkiByName[$newJ->jednostka] = $newJ->id;
                            }
                        }
                        $prepareData[$key]['towar_cena_default']['jednostka_id'] = $jednostkiByName[$item[$remoteField]];
                    } elseif ($localField == 'vat_stawka') {
                        if (!key_exists($item[$remoteField], $vatByStawka)) {
                            $newVat = $this->Vat->newEntity(['stawka' => $item[$remoteField], 'nazwa' => $item[$remoteField] . '%']);
                            if ($this->Vat->save($newVat)) {
                                $vatByStawka[$newVat->stawka] = $newVat->id;
                            }
                        }
                        $prepareData[$key]['towar_cena_default']['vat_id'] = $vatByStawka[$item[$remoteField]];
                    } elseif ($localField == 'cena_katalogowa' || $localField == 'cena_sprzedazy' || $localField=='cena_promocja' || $localField=='cena_netto') {
                        $prepareData[$key]['towar_cena_default'][$localField] = trim($item[$remoteField]);
                        $prepareData[$key]['towar_cena_default']['typ']='brutto';
                    } elseif ($localField == 'zdjecia' && !empty($item[$remoteField])) {
                        foreach ($item[$remoteField] as $image) {
                            if (is_array($image)) {
                                foreach ($image as $imgSrc) {
                                    $prepareData[$key]['towar_zdjecie'][] = [
                                        'remote_url' => trim($imgSrc)
                                    ];
                                }
                            } elseif (!empty($image)) {
                                $prepareData[$key]['towar_zdjecie'][] = [
                                    'remote_url' => trim($image)
                                ];
                            }
                        }
                    } elseif ($localField == 'kategoria_nazwa') {
                        if (is_array($item[$remoteField])) {
                            foreach ($item[$remoteField] as $katName) {

                                $katVal = (is_string($katName) ? mb_strtolower($katName) : null);
                                if (!empty($kategoryMapp) && key_exists($katVal, $kategoryMapp)) {
                                    $prepareData[$key]['kategoria']['_ids'][$kategoryMapp[$katVal]] = $kategoryMapp[$katVal];
                                }
                            }
                        } else {
                            $katVal = (is_string($item[$remoteField]) ? mb_strtolower($item[$remoteField]) : null);
                            if (!empty($kategoryMapp) && key_exists($katVal, $kategoryMapp)) {
                                $prepareData[$key]['kategoria']['_ids'][$kategoryMapp[$katVal]] = $kategoryMapp[$katVal];
                            }
                        }
                    } elseif ($localField == 'kolor_nazwa') {
                        $kolorName = trim($item[$remoteField]);
                    } elseif ($localField == 'rozmiar_nazwa') {
                        $rozmiarName = trim($item[$remoteField]);
                    } elseif ($localField == 'aktywny') {
                        if($item[$remoteField]==0){
                            $prepareData[$key]['ukryty']=1;
                        }else{
                            $prepareData[$key]['ukryty']=0;
                        }
                    }  elseif ($localField == 'opis' || $localField == 'opis2') {
                        $prepareData[$key][$localField]= html_entity_decode($item[$remoteField]);
                    }else {
                        $prepareData[$key][$localField] = (is_string($item[$remoteField]) ? trim($item[$remoteField]) : $item[$remoteField]);
                    }
                }
            }
            if (!empty($kolorName) || !empty($rozmiarName)) {
                $kolorId = null;
                $rozmiarId = null;
                if (!empty($kolorName)) {
                    if (!key_exists(mb_strtolower($kolorName), $kolorMap)) {
                        $newKolor = $this->Kolor->newEntity(['nazwa' => $kolorName]);
                        if ($this->Kolor->save($newKolor)) {
                            $kolorMap[mb_strtolower($kolorVal)] = $newKolor->id;
                        }
                    }
                    $kolorId = $kolorMap[mb_strtolower($kolorName)];
                }
                if (!empty($rozmiarName)) {
                    if (!key_exists(mb_strtolower($rozmiarName), $rozmiarMap)) {
                        $newRozmiar = $this->Rozmiar->newEntity(['nazwa' => $rozmiarName]);
                        if ($this->Rozmiar->save($newRozmiar)) {
                            $rozmiarMap[mb_strtolower($rozmiarName)] = $newRozmiar->id;
                        }
                    }
                    $rozmiarId = $rozmiarMap[mb_strtolower($rozmiarName)];
                }
                $prepareData[$key]['towar_wariant'][] = ['kolor_id' => $kolorId, 'rozmiar_id' => $rozmiarId, 'ilosc' => (!empty($prepareData[$key]['ilosc']) ? $prepareData[$key]['ilosc'] : 0)];
            }
            if (empty($prepareData[$key]['towar_cena_default']['jednostka_id'])) {
                $prepareData[$key]['towar_cena_default']['jednostka_id'] = 1;
            }
            if (empty($prepareData[$key]['towar_cena_default']['vat_id'])) {
                $prepareData[$key]['towar_cena_default']['vat_id'] = 1;
            }
            if (!((float) $prepareData[$key]['towar_cena_default']['cena_sprzedazy']) > 0) {
                $prepareData[$key]['towar_cena_default']['cena_sprzedazy'] = $prepareData[$key]['towar_cena_default']['cena_katalogowa'];
            }
            $prepareData[$key]['towar_cena_default']['waluta_id'] = 1;
            if (empty($prepareData[$key]['hurt_id'])) {
                $prepareData[$key]['hurt_id'] = $key;
            }
            if (empty($prepareData[$key]['ilosc'])) {
                $prepareData[$key]['ilosc'] = 0;
            }
        }
        if (!empty($prepareData)) {
            $this->saveTowar($prepareData, 'stary_sklep');
        }
    }

    private function addNewItemsFromPtakmodahurt($allItems) {
        $this->loadModel('KategoriaMap');
        $this->loadModel('Producent');
        $this->loadModel('Vat');
        $vatByStawka = $this->Vat->find('list', ['keyField' => 'stawka', 'valueField' => 'id'])->toArray();
        $this->loadModel('Jednostka');
        $jednostkiByName = $this->Jednostka->find('list', ['keyField' => 'jednostka', 'valueField' => 'id'])->toArray();
        $kategoryMapp = $this->KategoriaMap->find('list', ['keyField' => 'l_nazwa', 'valueField' => 'kategoria_id'])->select(['kategoria_id', 'l_nazwa' => 'LOWER(nazwa)'])->where(['hurtownia' => 'ptakmodahurt'])->toArray();
        $producentMapp = $this->Producent->find('list', ['keyField' => 'l_nazwa', 'valueField' => 'id'])->select(['id', 'l_nazwa' => 'LOWER(nazwa_ptakmodahurt)'])->where(['nazwa_ptakmodahurt IS NOT' => null])->toArray();
        $prepareData = [];
        foreach ($allItems as $item) {
            $item['images'] = explode(',', $item['images']);
            $key = $item[array_search('kod', $this->fieldMap)];
            if (empty($key)) {
                continue;
            }
            $prepareData[$key]['hurtownia'] = 'ptakmodahurt';
            $kolorName = $rozmiarName = '';
            foreach ($this->fieldMap as $remoteField => $localField) {
                if (!empty($localField)) {
                    if ($localField == 'waga') {
                        if (!empty($item[$remoteField])) {
                            $prepareData[$key][$localField] = ((float) trim($item[$remoteField]) / 1000);
                        }
                    } elseif ($localField == 'jednostka') {
                        if (!key_exists($item[$remoteField], $jednostkiByName)) {
                            $newJ = $this->Jednostka->newEntity(['jednostka' => $item[$remoteField]]);
                            if ($this->Jednostka->save($newJ)) {
                                $jednostkiByName[$newJ->jednostka] = $newJ->id;
                            }
                        }
                        $prepareData[$key]['towar_cena_default']['jednostka_id'] = $jednostkiByName[$item[$remoteField]];
                    } elseif ($localField == 'vat_stawka') {
                        if (!key_exists($item[$remoteField], $vatByStawka)) {
                            $newVat = $this->Vat->newEntity(['stawka' => $item[$remoteField], 'nazwa' => $item[$remoteField] . '%']);
                            if ($this->Vat->save($newVat)) {
                                $vatByStawka[$newVat->stawka] = $newVat->id;
                            }
                        }
                        $prepareData[$key]['towar_cena_default']['vat_id'] = $vatByStawka[$item[$remoteField]];
                    } elseif ($localField == 'cena_katalogowa' || $localField == 'cena_sprzedazy') {
                        $prepareData[$key]['towar_cena_default'][$localField] = trim($item[$remoteField]);
                    } elseif ($localField == 'zdjecia' && !empty($item[$remoteField])) {
                        foreach ($item[$remoteField] as $image) {
                            if (is_array($image)) {
                                foreach ($image as $imgSrc) {
                                    $prepareData[$key]['towar_zdjecie'][] = [
                                        'remote_url' => trim($imgSrc)
                                    ];
                                }
                            } elseif (!empty($image)) {
                                $prepareData[$key]['towar_zdjecie'][] = [
                                    'remote_url' => trim($image)
                                ];
                            }
                        }
                    } elseif ($localField == 'kategoria_nazwa') {
                        if (is_array($item[$remoteField])) {
                            foreach ($item[$remoteField] as $katName) {

                                $katVal = (is_string($katName) ? mb_strtolower($katName) : null);
                                if (!empty($kategoryMapp) && key_exists($katVal, $kategoryMapp)) {
                                    $prepareData[$key]['kategoria']['_ids'][$kategoryMapp[$katVal]] = $kategoryMapp[$katVal];
                                }
                            }
                        } else {
                            $katVal = (is_string($item[$remoteField]) ? mb_strtolower($item[$remoteField]) : null);
                            if (!empty($kategoryMapp) && key_exists($katVal, $kategoryMapp)) {
                                $prepareData[$key]['kategoria']['_ids'][$kategoryMapp[$katVal]] = $kategoryMapp[$katVal];
                            }
                        }
                    } elseif ($localField == 'producent_nazwa') {
                        $prodVal = (is_string($item[$remoteField]) ? mb_strtolower($item[$remoteField]) : null);
                        if (!empty($producentMapp) && key_exists($prodVal, $producentMapp)) {
                            $prepareData[$key]['producent_id'] = $producentMapp[$prodVal];
                        }
                    } elseif ($localField == 'kolor_nazwa') {
                        $kolorName = trim($item[$remoteField]);
                    } elseif ($localField == 'rozmiar_nazwa') {
                        $rozmiarName = trim($item[$remoteField]);
                    } else {
                        $prepareData[$key][$localField] = (is_string($item[$remoteField]) ? trim($item[$remoteField]) : $item[$remoteField]);
                    }
                }
            }
            if (!empty($kolorName) || !empty($rozmiarName)) {
                $kolorId = null;
                $rozmiarId = null;
                if (!empty($kolorName)) {
                    if (!key_exists(mb_strtolower($kolorName), $kolorMap)) {
                        $newKolor = $this->Kolor->newEntity(['nazwa' => $kolorName]);
                        if ($this->Kolor->save($newKolor)) {
                            $kolorMap[mb_strtolower($kolorVal)] = $newKolor->id;
                        }
                    }
                    $kolorId = $kolorMap[mb_strtolower($kolorName)];
                }
                if (!empty($rozmiarName)) {
                    if (!key_exists(mb_strtolower($rozmiarName), $rozmiarMap)) {
                        $newRozmiar = $this->Rozmiar->newEntity(['nazwa' => $rozmiarName]);
                        if ($this->Rozmiar->save($newRozmiar)) {
                            $rozmiarMap[mb_strtolower($rozmiarName)] = $newRozmiar->id;
                        }
                    }
                    $rozmiarId = $rozmiarMap[mb_strtolower($rozmiarName)];
                }
                $prepareData[$key]['towar_wariant'][] = ['kolor_id' => $kolorId, 'rozmiar_id' => $rozmiarId, 'ilosc' => (!empty($prepareData[$key]['ilosc']) ? $prepareData[$key]['ilosc'] : 0)];
            }
            if (empty($prepareData[$key]['towar_cena_default']['jednostka_id'])) {
                $prepareData[$key]['towar_cena_default']['jednostka_id'] = 1;
            }
            if (empty($prepareData[$key]['towar_cena_default']['vat_id'])) {
                $prepareData[$key]['towar_cena_default']['vat_id'] = 1;
            }
            if (!((float) $prepareData[$key]['towar_cena_default']['cena_sprzedazy']) > 0) {
                $prepareData[$key]['towar_cena_default']['cena_sprzedazy'] = $prepareData[$key]['towar_cena_default']['cena_katalogowa'];
            }
            $prepareData[$key]['towar_cena_default']['waluta_id'] = 1;
            if (empty($prepareData[$key]['hurt_id'])) {
                $prepareData[$key]['hurt_id'] = $key;
            }
            if (empty($prepareData[$key]['ilosc'])) {
                $prepareData[$key]['ilosc'] = 0;
            }
        }
        if (!empty($prepareData)) {
            $this->saveTowar($prepareData, 'ptakmodahurt');
        }
    }

    private function addNewItemsFromMolos($allItems) {
        $this->loadModel('KategoriaMap');
        $this->loadModel('Producent');
        $this->loadModel('Vat');
        $vatByStawka = $this->Vat->find('list', ['keyField' => 'stawka', 'valueField' => 'id'])->toArray();
        $this->loadModel('Jednostka');
        $jednostkiByName = $this->Jednostka->find('list', ['keyField' => 'jednostka', 'valueField' => 'id'])->toArray();
        $kategoryMapp = $this->KategoriaMap->find('list', ['keyField' => 'l_nazwa', 'valueField' => 'kategoria_id'])->select(['kategoria_id', 'l_nazwa' => 'LOWER(nazwa)'])->where(['hurtownia' => 'molos'])->toArray();
        $producentMapp = $this->Producent->find('list', ['keyField' => 'l_nazwa', 'valueField' => 'id'])->select(['id', 'l_nazwa' => 'LOWER(nazwa_molos)'])->where(['nazwa_molos IS NOT' => null])->toArray();
        $prepareData = [];
        foreach ($allItems as $item) {
            $item['PRICE'] = $item['PRICE']['$'];
            $item['MARKET_PRICE'] = $item['MARKET_PRICE']['$'];
            if (!empty($item['FIRMS']) && !empty($item['FIRMS']['FIRM']['@role']) && $item['FIRMS']['FIRM']['@role'] == 'producent') {
                $item['FIRMS'] = $item['FIRMS']['FIRM']['$'];
            } elseif (!empty($item['FIRMS']) && !empty($item['FIRMS']['FIRM'])) {
                foreach ($item['FIRMS']['FIRM'] as $firm) {
                    if ($firm['@role'] == 'producent') {
                        $item['FIRMS'] = $firm['$'];
                        break;
                    }
                }
            }
            if (!empty($item['CATEGORIES'])) {
                $tmpKats = [];
                if (!empty($item['CATEGORIES']['CATEGORY']['@id'])) {
                    $tmpKats[$item['CATEGORIES']['CATEGORY']['@id']] = $item['CATEGORIES']['CATEGORY']['$'];
                } else {
                    foreach ($item['CATEGORIES']['CATEGORY'] as $cat) {
                        $tmpKats[$cat['@id']] = $cat['$'];
                    }
                }
                $item['CATEGORIES'] = $tmpKats;
            }
            $key = $item[array_search('kod', $this->fieldMap)];
            if (empty($key)) {
                continue;
            }
            $prepareData[$key]['hurtownia'] = 'molos';
            foreach ($this->fieldMap as $remoteField => $localField) {
                if (!empty($localField)) {
                    if ($localField == 'waga') {
                        if (!empty($item[$remoteField])) {
                            $prepareData[$key][$localField] = ((float) trim($item[$remoteField]) / 1000);
                        }
                    } elseif ($localField == 'jednostka') {
                        if (!key_exists($item[$remoteField], $jednostkiByName)) {
                            $newJ = $this->Jednostka->newEntity(['jednostka' => $item[$remoteField]]);
                            if ($this->Jednostka->save($newJ)) {
                                $jednostkiByName[$newJ->jednostka] = $newJ->id;
                            }
                        }
                        $prepareData[$key]['towar_cena_default']['jednostka_id'] = $jednostkiByName[$item[$remoteField]];
                    } elseif ($localField == 'vat_stawka') {
                        if (!key_exists($item[$remoteField], $vatByStawka)) {
                            $newVat = $this->Vat->newEntity(['stawka' => $item[$remoteField], 'nazwa' => $item[$remoteField] . '%']);
                            if ($this->Vat->save($newVat)) {
                                $vatByStawka[$newVat->stawka] = $newVat->id;
                            }
                        }
                        $prepareData[$key]['towar_cena_default']['vat_id'] = $vatByStawka[$item[$remoteField]];
                    } elseif ($localField == 'cena_katalogowa' || $localField == 'cena_sprzedazy') {
                        $prepareData[$key]['towar_cena_default'][$localField] = trim($item[$remoteField]);
                    } elseif ($localField == 'zdjecia' && !empty($item[$remoteField])) {
                        foreach ($item[$remoteField] as $image) {
                            if (is_array($image)) {
                                foreach ($image as $imgSrc) {
                                    $prepareData[$key]['towar_zdjecie'][] = [
                                        'remote_url' => trim($imgSrc)
                                    ];
                                }
                            } elseif (!empty($image)) {
                                $prepareData[$key]['towar_zdjecie'][] = [
                                    'remote_url' => trim($image)
                                ];
                            }
                        }
                    } elseif ($localField == 'kategoria_nazwa') {
                        if (is_array($item[$remoteField])) {
                            foreach ($item[$remoteField] as $katName) {

                                $katVal = (is_string($katName) ? mb_strtolower($katName) : null);
                                if (!empty($kategoryMapp) && key_exists($katVal, $kategoryMapp)) {
                                    $prepareData[$key]['kategoria']['_ids'][$kategoryMapp[$katVal]] = $kategoryMapp[$katVal];
                                }
                            }
                        } else {
                            $katVal = (is_string($item[$remoteField]) ? mb_strtolower($item[$remoteField]) : null);
                            if (!empty($kategoryMapp) && key_exists($katVal, $kategoryMapp)) {
                                $prepareData[$key]['kategoria']['_ids'][$kategoryMapp[$katVal]] = $kategoryMapp[$katVal];
                            }
                        }
                    } elseif ($localField == 'producent_nazwa') {
                        $prodVal = (is_string($item[$remoteField]) ? mb_strtolower($item[$remoteField]) : null);
                        if (!empty($producentMapp) && key_exists($prodVal, $producentMapp)) {
                            $prepareData[$key]['producent_id'] = $producentMapp[$prodVal];
                        }
                    } else {
                        $prepareData[$key][$localField] = (is_string($item[$remoteField]) ? trim($item[$remoteField]) : $item[$remoteField]);
                    }
                }
            }
            if (empty($prepareData[$key]['towar_cena_default']['jednostka_id'])) {
                $prepareData[$key]['towar_cena_default']['jednostka_id'] = 1;
            }
            if (empty($prepareData[$key]['towar_cena_default']['vat_id'])) {
                $prepareData[$key]['towar_cena_default']['vat_id'] = 1;
            }
            if (!((float) $prepareData[$key]['towar_cena_default']['cena_sprzedazy']) > 0) {
                $prepareData[$key]['towar_cena_default']['cena_sprzedazy'] = $prepareData[$key]['towar_cena_default']['cena_katalogowa'];
            }
            $prepareData[$key]['towar_cena_default']['waluta_id'] = 1;
            if (empty($prepareData[$key]['hurt_id'])) {
                $prepareData[$key]['hurt_id'] = $key;
            }
            if (empty($prepareData[$key]['ilosc'])) {
                $prepareData[$key]['ilosc'] = 0;
            }
        }
        if (!empty($prepareData)) {
            $this->saveTowar($prepareData, 'molos');
        }
    }

    private function saveTowar($items, $hurtownia) {
        $errors = [];
        if (!empty($items) && !empty($hurtownia)) {
            if (empty($this->Towar)) {
                $this->loadModel('Towar');
            }
            foreach ($items as $item) {
                if (!empty($item['kod'])) {
                    $towar = $this->Towar->find('all', ['contain' => ['Kategoria', 'TowarCenaDefault', 'TowarZdjecie', 'TowarWariant']])->where(['Towar.hurt_id' => $item['hurt_id'], 'Towar.hurtownia' => $hurtownia])->first();
                    if (empty($towar)) {
                        $towar = $this->Towar->newEntity();
                    }
                    if (!empty($towar->towar_zdjecie)) {
                        foreach ($towar->towar_zdjecie as $towarImg) {
                            if (!empty($towarImg['remote_url'])) {
                                foreach ($item['towar_zdjecie'] as $key => $rmImage) {
                                    if ($towarImg->remote_url == $rmImage['remote_url']) {
                                        unset($item['towar_zdjecie'][$key]);
                                    }
                                }
                            }
                        }
                    }
                    $oldWariants = [];
                    if (!empty($towar->towar_wariant)) {
                        foreach ($towar->towar_wariant as $twoarWariant) {
                            $oldWariants[$twoarWariant->kolor_id . '_' . $twoarWariant->rozmiar_id] = $twoarWariant->id;
                        }
                    }
                    if (!empty($item['towar_wariant'])) {
                        foreach ($item['towar_wariant'] as &$itemWariant) {
                            if (!empty($oldWariants) && key_exists($itemWariant['kolor_id'] . '_' . $itemWariant['rozmiar_id'], $oldWariants)) {
                                $itemWariant['id'] = $oldWariants[$itemWariant['kolor_id'] . '_' . $itemWariant['rozmiar_id']];
                                unset($oldWariants[$itemWariant['kolor_id'] . '_' . $itemWariant['rozmiar_id']]);
                            }
                        }
                    }
                    $item['get_date'] = new \DateTime();
                    $towar = $this->Towar->patchEntity($towar, $item);
                    if (!$this->Towar->save($towar)) {
                        $errors[] = $towar->errors();
                    } else {
                        if (!empty($oldWariants)) {
                            $this->Towar->TowarWariant->deleteAll(['id IN' => $oldWariants]);
                        }
                    }
                    unset($towar);
                }
            }
            if (!empty($errors)) {
                Log::write('error', $errors, ['cronError']);
            }
        } else {
            return false;
        }
    }

    private function updateItemsFromIkonka($allItems) {
        
    }

    private function setFieldMapIkonka() {
        $this->fieldMap = [
            'nazwa' => 'nazwa',
            'kod' => 'kod',
            'kod_kreskowy' => 'ean',
            'stan' => 'ilosc',
            'grupa_rabatowa' => null,
            'cena' => 'cena_katalogowa',
            'sugerowana_cena_detaliczna' => 'cena_sprzedazy',
            'vat' => 'vat_stawka',
            'zdjecia' => 'zdjecia',
            'kategoria' => 'kategoria_nazwa',
            'opis_krotki' => 'opis2',
            'opis' => 'opis',
            'czas_dostawy' => 'wysylka_info_dostepny',
            'waga' => 'waga',
            'sztuk_w_kartonie' => null,
            'objetosc' => null,
            'wysokosc' => null,
            'dlugosc' => null,
            'najblizsza_dostawa' => null,
            'link_do_instrukcji' => null
        ];
    }

    private function setFieldMapCentralazabawek() {
        $this->fieldMap = [
            'id' => 'hurt_id',
            'name' => 'nazwa',
            'manufacturer_code' => 'kod',
            'ean' => 'ean',
            'stock' => 'ilosc',
            'price' => 'cena_katalogowa',
            'suggested_price' => 'cena_sprzedazy',
            'vat' => 'vat_stawka',
            'images' => 'zdjecia',
            'category' => 'kategoria_nazwa',
            'description' => 'opis',
            'available' => 'wysylka_info_dostepny',
            'weight' => 'waga',
            'ean1' => null,
            'ean2' => null,
            'sex' => null,
            'manufacturer' => 'producent_nazwa',
            'guarantee' => 'gwarancja'
        ];
    }

    private function setFieldMapGatito() {
        $this->fieldMap = [
            '@id' => 'hurt_id',
            'attr_Kod_produktu' => 'kod',
            '@url' => null,
            '@avail' => null,
            'name' => 'nazwa',
            'product_code' => 'ean',
            '@stock' => 'ilosc',
            'price' => null,
            'price_brutto' => 'cena_sprzedazy',
            'imgs' => 'zdjecia',
            'cat' => 'kategoria_nazwa',
            'attrs' => 'atrybuty',
            'attr_Supplier' => 'producent_nazwa',
            'attr_Price_netto_single_unit' => 'cena_netto_za_sztuke'
        ];
    }

    private function setFieldMapMolos() {
        $this->fieldMap = [
            '@availability' => 'wysylka_info_dostepny',
            'ID' => 'kod',
            'URL_PRODUCT' => null,
            '@avail' => null,
            'NAME' => 'nazwa',
            'EAN' => 'ean',
            '@stock' => 'ilosc',
            'PRICE' => 'cena_katalogowa',
            'MARKET_PRICE' => 'cena_sprzedazy',
            'IMAGES' => 'zdjecia',
            'CATEGORIES' => 'kategoria_nazwa',
            'VAT' => 'vat_stawka',
            'DESCRIPTION' => 'opis',
            'FIRMS' => 'producent_nazwa'
        ];
    }

    private function setFieldMapPtakmodahurt() {
        $this->fieldMap = [
            'reference' => 'kod',
            'product_link' => null,
            'name' => 'nazwa',
            'ean13' => 'ean',
            'price' => 'cena_sprzedazy',
            'images' => 'zdjecia',
            'categories_names' => 'kategoria_nazwa',
            'description' => 'opis',
            'description_short' => 'opis2',
            'quantity' => 'ilosc',
            'Attribute_Color' => 'kolor_nazwa',
            'Attribute_Size' => 'rozmiar_nazwa',
            'combinations_name' => null,
            'combinations_reference' => null
        ];
    }

    private function setFieldMapVmp() {
        $this->fieldMap = [
            '@product_id' => 'kod',
            'MODEL' => 'nazwa',
//            'ean13' => 'ean',
            'NET_PRICE' => 'cena_katalogowa',
            'RECOMMENDED_PRICE' => 'cena_sprzedazy',
            'IMAGES' => 'zdjecia',
            'CATEGORIES' => 'kategoria_nazwa',
            'DESCRIPTION' => 'opis',
            'PRODUCER' => 'producent_nazwa',
            'COLORS' => 'warianty'
        ];
    }

    private function setFieldMapCustomXml() {
        $this->fieldMap = [
            'id' => 'hurt_id',
            'ean' => 'ean',
            'code' => 'kod',
            'path'=>null,
            'barcode'=>null,
            'typecode'=>null,
            'name' => 'nazwa',
            'description' => 'opis',
            'short_description' => 'opis2',
            'tags'=>'tagi',
            'producer_id'=>'producent_id',
            'typeID' => 'kategoria_nazwa',
            'vat' => 'vat_stawka',
            'price_netto' => 'cena_netto',
            'price_brutto' => 'cena_sprzedazy',
            'promo_price' => 'cena_promocja',
            'available' => 'ilosc',
            'is_active' => 'aktywny',
            'promoted' => 'promocja',
            'new' => 'nowosc',
            'best_buy' => 'wyrozniony',
            'marked' => 'polecany',
            'date_added'=>'data_utworzenia',
            'is_active'=>'aktywny',
        ];
    }

    private function setFieldCeneo() {
        $this->fieldMap = [
            '@id' => 'kod',
            'name' => 'nazwa',
            '@ean' => 'ean',
            '@price' => 'cena_sprzedazy',
            'imgs' => 'zdjecia',
            'cat' => 'kategoria_nazwa',
            'desc' => 'opis',
            '@producent' => 'producent_nazwa',
        ];
    }

    private function downloadImages($getLen = 20) {
        $this->loadModel('TowarZdjecie');
        $images = $this->TowarZdjecie->find('all')->where(['TowarZdjecie.remote_url IS NOT' => NULL, 'TowarZdjecie.plik IS' => NULL])->limit($getLen);
        $dCount = 0;
        if (!empty($images) && $images->count() > 0) {
            $this->loadComponent('Upload');
            $checkDefaults = [];
            foreach ($images as $image) {
                if (!empty($image['remote_url'])) {
                    $fileContent = file_get_contents($image['remote_url']);

                    if (!empty($fileContent)) {
                        $destination = $this->filePath['towar'] . $this->Txt->getKatalog($image->id) . DS;
                        $fileName = $this->Upload->saveFile($destination, ['name' => pathinfo($image['remote_url'], PATHINFO_BASENAME), 'image' => $fileContent], null, Configure::read('product_thumbs'), false, true);
                        if (!empty($fileName)) {
                            if (!key_exists($image->towar_id, $checkDefaults)) {
                                $checkDefaults[$image->towar_id] = $this->TowarZdjecie->find()->where(['TowarZdjecie.domyslne' => 1, 'TowarZdjecie.towar_id' => $image->towar_id])->count();
                            }
                            if (empty($checkDefaults[$image->towar_id])) {
                                $image->set('domyslne', true);
                            }
                            $image->set('plik', $fileName);
                            $this->TowarZdjecie->save($image);
                            $dCount++;
                        } else {
                            $this->TowarZdjecie->delete($image);
                        }
                    } else {
                        $this->TowarZdjecie->delete($image);
                    }
                } else {
                    $this->TowarZdjecie->delete($image);
                }
            }
        }

        return ['status' => (($dCount > 0) ? 'continue' : 'stop'), 'message' => __('Pobrano {0} zdjęć', [$dCount])];
    }

    public function tests() {
        $this->autoRender = false;
//        $this->setFieldMapCentralazabawek();
        $this->setFieldMapCustomXml();
        $this->integracjaCustomXml();

        die();
    }

    private function xmlToArray($xml, $options = array()) {
        $defaults = array(
            'namespaceSeparator' => ':', //you may want this to be something other than a colon
            'attributePrefix' => '@', //to distinguish between attributes and nodes with the same name
            'alwaysArray' => array(), //array of xml tag names which should always become arrays
            'autoArray' => true, //only create arrays for tags which appear more than once
            'textContent' => '$', //key used for the text content of elements
            'autoText' => true, //skip textContent key if node has no attributes or child nodes
            'keySearch' => false, //optional search and replace on tag and attribute names
            'keyReplace' => false       //replace values for above search values (as passed to str_replace())
        );
        $options = array_merge($defaults, $options);
        $namespaces = $xml->getDocNamespaces();
        $namespaces[''] = null; //add base (empty) namespace
        //get attributes from all namespaces
        $attributesArray = array();
        foreach ($namespaces as $prefix => $namespace) {
            foreach ($xml->attributes($namespace) as $attributeName => $attribute) {
                //replace characters in attribute name
                if ($options['keySearch'])
                    $attributeName = str_replace($options['keySearch'], $options['keyReplace'], $attributeName);
                $attributeKey = $options['attributePrefix']
                        . ($prefix ? $prefix . $options['namespaceSeparator'] : '')
                        . $attributeName;
                $attributesArray[$attributeKey] = (string) $attribute;
            }
        }

        //get child nodes from all namespaces
        $tagsArray = array();
        foreach ($namespaces as $prefix => $namespace) {
            foreach ($xml->children($namespace) as $childXml) {
                //recurse into child nodes
                $childArray = $this->xmlToArray($childXml, $options);
//                list($childTagName, $childProperties) = each($childArray);
                $childTagName = key($childArray);
                $childProperties = current($childArray);

                //replace characters in tag name
                if ($options['keySearch'])
                    $childTagName = str_replace($options['keySearch'], $options['keyReplace'], $childTagName);
                //add namespace prefix, if any
                if ($prefix)
                    $childTagName = $prefix . $options['namespaceSeparator'] . $childTagName;

                if (!isset($tagsArray[$childTagName])) {
                    //only entry with this key
                    //test if tags of this type should always be arrays, no matter the element count
                    $tagsArray[$childTagName] = in_array($childTagName, $options['alwaysArray']) || !$options['autoArray'] ? array($childProperties) : $childProperties;
                } elseif (
                        is_array($tagsArray[$childTagName]) && array_keys($tagsArray[$childTagName]) === range(0, count($tagsArray[$childTagName]) - 1)
                ) {
                    //key already exists and is integer indexed array
                    $tagsArray[$childTagName][] = $childProperties;
                } else {
                    //key exists so convert to integer indexed array with previous value in position 0
                    $tagsArray[$childTagName] = array($tagsArray[$childTagName], $childProperties);
                }
            }
        }

        //get text content of node
        $textContentArray = array();
        $plainText = trim((string) $xml);
        if ($plainText !== '')
            $textContentArray[$options['textContent']] = $plainText;

        //stick it all together
        $propertiesArray = !$options['autoText'] || $attributesArray || $tagsArray || ($plainText === '') ? array_merge($attributesArray, $tagsArray, $textContentArray) : $plainText;

        //return node as array
        return array(
            $xml->getName() => $propertiesArray
        );
    }

}
