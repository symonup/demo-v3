<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use PHPExcel;

/**
 * Static content controller
 *
 * This controller will control cart actions
 *
 */
class CartController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow();
        if ($this->request->is('mobile')) {
            $this->forceHideLeft = true;
        }
    }

    public function add($itemId = null, $returnTr = false) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($itemId)) {
            if (empty($this->Towar)) {
                $this->loadModel('Towar');
            }
            $cartItems = (!empty($this->session) ? $this->session->read('cartItems') : null);
            $cartIlosci = ((!empty($cartItems) && !empty($cartItems['cartIlosci'])) ? $cartItems['cartIlosci'] : []);
            $warianty = $this->request->getData();
            $gratis=false;
            if(!empty($warianty['gratis'])){
                $gratis=true;
            }
            unset($warianty['gratis']);
            $wariantyIds = [];
            if (!empty($warianty)) {
                foreach ($warianty as $key => $ilosc) {
                    if (strpos($key, $itemId . '_') !== false) {
                        $wariantyIds[] = str_replace($itemId . '_', '', $key);
                    }
                }
            }
            if (empty($wariantyIds)) {
                $wariantyIds = [null];
            }
            $addAllow = true;


            $item = $this->Towar->getWariantsToCart($this->Auth->user(), $itemId, $wariantyIds);
            if (!empty($item)) {
                if($gratis){
                    if(empty($item->gratis) || empty($cartItems) || $cartItems['total_brutto'] < $item->gratis_wartosc){
                        $addError = $this->Translation->get('errors.nieMozeszWybracTegoGratisu');
                        $addAllow=false;
                    }
                }
                if (empty($this->UzytkownikKoszyk)) {
                    $this->loadModel('UzytkownikKoszyk');
                }
                $itemsToAdd = $this->UzytkownikKoszyk->prepareCart($item, $warianty, $this->userRabat,$gratis);
                if ($addAllow && !empty($itemsToAdd)) {
                    $addError = false;
                    foreach ($itemsToAdd as $addKey => $addItem) {
                        if (key_exists($addItem['towar_id'], $cartIlosci)) {
                            if (($cartIlosci[$addItem['towar_id']]['ilosc'] + $addItem['ilosc']) > $cartIlosci[$addItem['towar_id']]['max_ilosc']) {
                                $addAllow = false;
                                $addError = $this->Translation->get('errors.przekroczonoDostepnaIlosc');
                            }
                        }
                        if (!empty($cartItems['items'][$addKey]) && $addKey!='gratis') {
                            $cartItems['items'][$addKey]['ilosc'] = $cartItems['items'][$addKey]['ilosc'] + $addItem['ilosc'];
                            $priceRazem = round($cartItems['items'][$addKey]['cena_za_sztuke'] * $cartItems['items'][$addKey]['ilosc'], 2);
                            $cartItems['items'][$addKey]['cena_razem'] = $priceRazem;
                            $cartItems['items'][$addKey]['cena_razem_netto'] = $this->Txt->cenaVat($priceRazem, $cartItems['items'][$addKey]['vat_stawka'], \Cake\Core\Configure::read('ceny.rodzaj'), 'netto', false);
                            $cartItems['items'][$addKey]['cena_razem_brutto'] = $this->Txt->cenaVat($priceRazem, $cartItems['items'][$addKey]['vat_stawka'], \Cake\Core\Configure::read('ceny.rodzaj'), 'brutto', false);
                        } else {
                            $cartItems['items'][$addKey] = $addItem;
                        }
                        if (($cartItems['items'][$addKey]['ilosc'] > $cartItems['items'][$addKey]['towar_ilosc']) || (!empty($cartItems['items'][$addKey]['hot_deal']) && $cartItems['items'][$addKey]['ilosc'] > $cartItems['items'][$addKey]['hot_deal'])) {
                            $addError = $this->Translation->get('errors.przekroczonoDostepnaIlosc');
                        } else {
                            $sklepMaxIlosc = (int) Configure::read('limity.limit_ilosci');
                            if (!empty($sklepMaxIlosc) && $sklepMaxIlosc > 0 && $cartItems['items'][$addKey]['ilosc'] > $sklepMaxIlosc) {
                                $addError = $this->Translation->get('errors.przekroczonoLimitIlosci');
                            }
                        }
                    }
                    if (!$addError) {
                        $view = new \Cake\View\View();
                        $view->Translation->setLocale(\Cake\I18n\I18n::locale());
                        $cartItems = $this->cartPrzelicz($cartItems, false, $view);
                        $this->session->write('cartItems', $cartItems);
                        Configure::write('cartItems', $cartItems);

                        $view->layout = false;
                        $view->set('mobile', $this->mobile);
                        $view->set('cartItems', $cartItems);
                        $view->set('filePath', $this->filePath);
                        $view->set('displayPath', $this->displayPath);
                        if (!empty($returnTr)) {
                            $html = $view->element('default/order/cart_item', ['itemKey' => $addKey, 'mobile' => $this->mobile, 'item' => $cartItems['items'][$addKey]]);
                            $returnData = ['status' => 'success', 'cartItems' => $cartItems, 'html' => $html, 'returnTr' => $returnTr];
                        } else {
                            $html = $view->render('Cart/add');
                            $returnData = ['status' => 'success', 'cartItems' => $cartItems, 'html' => $html];
                        }

                        $view->Translation->afterLayout();
                    } else {
                        $returnData = ['status' => 'error', 'message' => $addError];
                    }
                }else{
                    $returnData = ['status' => 'error', 'message' => $addError];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Translation->get('errors.itemIsUnavailable')];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Translation->get('errors.itemIdIsUndefined')];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    public function cardAdd($itemId = null, $returnTr = false) {
        $this->autoRender = false;
        $returnData = [];
        if (!empty($itemId)) {
            if (empty($this->KartaPodarunkowa)) {
                $this->loadModel('KartaPodarunkowa');
            }
            $cartItems = (!empty($this->session) ? $this->session->read('cartItems') : null);
            $kartaId = $this->request->getData('cardId');

            $item = $this->KartaPodarunkowa->find()->where(['KartaPodarunkowa.id' => $kartaId])->first();
            if (!empty($item)) {
                if (empty($this->UzytkownikKoszyk)) {
                    $this->loadModel('UzytkownikKoszyk');
                }
                $itemsToAdd = $this->UzytkownikKoszyk->prepareCartBonus($item);
                if (!empty($itemsToAdd)) {
                    $addError = false;
                    foreach ($itemsToAdd as $addKey => $addItem) {
                        if (!empty($cartItems['items'][$addKey])) {
                            $cartItems['items'][$addKey]['ilosc'] = $cartItems['items'][$addKey]['ilosc'] + $addItem['ilosc'];
                            $priceRazem = round($cartItems['items'][$addKey]['cena_za_sztuke'] * $cartItems['items'][$addKey]['ilosc'], 2);
                            $cartItems['items'][$addKey]['cena_razem'] = $priceRazem;
                            $cartItems['items'][$addKey]['cena_razem_netto'] = $this->Txt->cenaVat($priceRazem, $cartItems['items'][$addKey]['vat_stawka'], \Cake\Core\Configure::read('ceny.rodzaj'), 'netto', false);
                            $cartItems['items'][$addKey]['cena_razem_brutto'] = $this->Txt->cenaVat($priceRazem, $cartItems['items'][$addKey]['vat_stawka'], \Cake\Core\Configure::read('ceny.rodzaj'), 'brutto', false);
                        } else {
                            $cartItems['items'][$addKey] = $addItem;
                        }
                    }
                    if (!$addError) {
                        $view = new \Cake\View\View();
                        $view->Translation->setLocale(\Cake\I18n\I18n::locale());
                        $cartItems = $this->cartPrzelicz($cartItems, false, $view);
                        $this->session->write('cartItems', $cartItems);
                        Configure::write('cartItems', $cartItems);

                        $view->layout = false;
                        $view->set('mobile', $this->mobile);
                        $view->set('cartItems', $cartItems);
                        $view->set('filePath', $this->filePath);
                        $view->set('displayPath', $this->displayPath);
                        if (!empty($returnTr)) {
                            $html = $view->element('default/order/cart_item', ['itemKey' => $addKey, 'item' => $cartItems['items'][$addKey]]);
                            $returnData = ['status' => 'success', 'cartItems' => $cartItems, 'html' => $html, 'returnTr' => $returnTr];
                        } else {
                            $html = $view->render('Cart/add');
                            $returnData = ['status' => 'success', 'cartItems' => $cartItems, 'html' => $html];
                        }

                        $view->Translation->afterLayout();
                    } else {
                        $returnData = ['status' => 'error', 'message' => $addError];
                    }
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Translation->get('errors.itemIsUnavailable')];
            }
        } else {
            $returnData = ['status' => 'error', 'message' => $this->Translation->get('errors.itemIdIsUndefined')];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    public function addZestaw($zestaw_id) {
        $this->autoRender = false;
        $this->loadModel('Zestaw');
        $towarCenaCond = ['TowarCenaDefault.uzytkownik_id IS' => null, 'TowarCenaDefault.waluta_id' => $this->active_jezyk->waluta_id];

        $zestaw = $this->Zestaw->get($zestaw_id, ['contain' =>
            [
                'ZestawTowar' => [
                    'Towar' => ['TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]],
                        'TowarCenaDefault' => ['Vat', 'Waluta', 'Jednostka', 'conditions' => $towarCenaCond],
                        'Bonus',
                        'Platforma'
                    ]
                ],
                'Towar' => [
                    'TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]],
                    'TowarCenaDefault' => ['Vat', 'Waluta', 'Jednostka', 'conditions' => $towarCenaCond],
                    'Bonus',
                    'Platforma'
                ]
        ]]);
        $zestawBonus = [];
        switch ($zestaw->typ) {
            case 1 : {
                    $zestawBonus = [
                        'nazwa' => $this->Translation->get('zestaw.Rabat{0}', [$zestaw->rabat . '%']),
                        'wartosc' => $zestaw->rabat,
                        'typ' => 'procent',
                        'zestaw_bonus' => true
                    ];
                } break;
            case 2 : {
                    $zestawBonus = [
                        'nazwa' => $this->Translation->get('zestaw.RabatNaZakupy{0}', [$this->Txt->cena($zestaw->rabat)]),
                        'wartosc' => $zestaw->rabat,
                        'typ' => 'kwota',
                        'zestaw_bonus' => true
                    ];
                } break;
            case 3 : {
                    $zestawBonus = [
                        'nazwa' => $this->Translation->get('zestaw.Bon{0}', [$this->Txt->cena($zestaw->rabat)]),
                        'wartosc' => $zestaw->rabat,
                        'typ' => 'bon',
                        'zestaw_bonus' => true
                    ];
                } break;
        }
        $errors = '';
        $zestawRazem = 0;
        $maxIlosc = 0;
        $addAllow = true;
        $ilosc = $this->request->getData('ilosc');
        if(empty($ilosc)){
            $ilosc = 1;
        }
        if (!empty($zestaw)) {
            $cartItems = (!empty($this->session) ? $this->session->read('cartItems') : null);
            $cartIlosci = (!empty($cartItems) ? $cartItems['cartIlosci'] : []);
            $rabatTyp = $zestaw->typ;
            $rabat = $zestaw->rabat;
            $zestawKey = 'z_' . $zestaw->token . '_t_' . $zestaw->towar->id;
            if (key_exists($zestaw->towar->id, $cartIlosci)) {
                if (($cartIlosci[$zestaw->towar->id]['ilosc'] + $ilosc) > $cartIlosci[$zestaw->towar->id]['max_ilosc']) {
                    $addAllow = false;
                }
            }
            if (!empty($cartItems['items'][$zestawKey])) {
                $cartItems['items'][$zestawKey]['ilosc']+=$ilosc;
                $maxIlosc = $cartItems['items'][$zestawKey]['max_ilosc'];
                if ($cartItems['items'][$zestawKey]['ilosc'] > $maxIlosc) {
                    $cartItems['items'][$zestawKey]['ilosc'] = $maxIlosc;
                    $errors = $this->Translation->get('errors.przekroczonoDostepnaIlosc');
                    $addAllow = false;
                } else {
                    $sklepMaxIlosc = (int) Configure::read('limity.limit_ilosci');
                    if (!empty($sklepMaxIlosc) && $sklepMaxIlosc > 0 && $cartItems['items'][$zestawKey]['ilosc'] > $sklepMaxIlosc) {
                        $errors = $this->Translation->get('errors.przekroczonoLimitIlosci');
                        $addAllow = false;
                    }
                }

                $zestawRazem += $cartItems['items'][$zestawKey]['cena_za_sztuke'];
                $priceRazem = round($cartItems['items'][$zestawKey]['cena_za_sztuke'] * $cartItems['items'][$zestawKey]['ilosc'], 2);
                $cartItems['items'][$zestawKey]['cena_razem'] = $priceRazem;
                $cartItems['items'][$zestawKey]['cena_razem_netto'] = $this->Txt->cenaVat($priceRazem, $cartItems['items'][$zestawKey]['vat_stawka'], \Cake\Core\Configure::read('ceny.rodzaj'), 'netto', false);
                $cartItems['items'][$zestawKey]['cena_razem_brutto'] = $this->Txt->cenaVat($priceRazem, $cartItems['items'][$zestawKey]['vat_stawka'], \Cake\Core\Configure::read('ceny.rodzaj'), 'brutto', false);
                $zestawItems = [];
                $zestawIlosc = $cartItems['items'][$zestawKey]['ilosc'];
            } else {
                $zestawItems = $this->UzytkownikKoszyk->prepareCart($zestaw->towar, [$zestaw->towar->id => $ilosc], (($zestawBonus['typ'] == 'procent') ? $zestawBonus['wartosc'] : null));
            }
            foreach ($zestaw->zestaw_towar as $zItems) {
                $zestawKey .= '_' . $zItems->towar->id;
                $zestawItemKey = 'z_' . $zestaw->token . '_t_' . $zItems->towar->id;
                if (key_exists($zItems->towar->id, $cartIlosci)) {
                    if (($cartIlosci[$zItems->towar->id]['ilosc'] + $ilosc) > $cartIlosci[$zItems->towar->id]['max_ilosc']) {
                        $addAllow = false;
                    }
                }
                if (!empty($cartItems['items'][$zestawItemKey])) {
                    $cartItems['items'][$zestawItemKey]['ilosc']+=$ilosc;
                    $maxIlosc = $cartItems['items'][$zestawItemKey]['max_ilosc'];
                    if ($cartItems['items'][$zestawItemKey]['ilosc'] > $maxIlosc) {
                        $cartItems['items'][$zestawItemKey]['ilosc'] = $maxIlosc;
                        $errors = $this->Translation->get('errors.przekroczonoDostepnaIlosc');
                    }

                    $zestawRazem += $cartItems['items'][$zestawItemKey]['cena_za_sztuke'];
                    $priceRazem = round($cartItems['items'][$zestawItemKey]['cena_za_sztuke'] * $cartItems['items'][$zestawItemKey]['ilosc'], 2);
                    $cartItems['items'][$zestawItemKey]['cena_razem'] = $priceRazem;
                    $cartItems['items'][$zestawItemKey]['cena_razem_netto'] = $this->Txt->cenaVat($priceRazem, $cartItems['items'][$zestawItemKey]['vat_stawka'], \Cake\Core\Configure::read('ceny.rodzaj'), 'netto', false);
                    $cartItems['items'][$zestawItemKey]['cena_razem_brutto'] = $this->Txt->cenaVat($priceRazem, $cartItems['items'][$zestawItemKey]['vat_stawka'], \Cake\Core\Configure::read('ceny.rodzaj'), 'brutto', false);
                } else {
                    $zItem = $this->UzytkownikKoszyk->prepareCart($zItems->towar, [$zItems->towar->id => $ilosc], (($zestawBonus['typ'] == 'procent') ? $zestawBonus['wartosc'] : null));
                    if (!empty($zItem) && is_array($zItem)) {
                        $zestawItems = $zestawItems + $zItem;
                    }
                }
            }
            if (!empty($zestawItems)) {
                $allInZestaw = [];
                $zestawIlosc = $ilosc;
                foreach ($zestawItems as $key => &$item) {
                    if (empty($maxIlosc) || $item['towar_ilosc'] < $maxIlosc) {
                        $maxIlosc = $item['towar_ilosc'];
                    }
                }

                foreach ($zestawItems as $key => &$item) {
                    if ($item['ilosc'] > $maxIlosc) {
                        $item['ilosc'] = $maxIlosc;
                        $errors = $this->Translation->get('errors.przekroczonoDostepnaIlosc');
                        $addAllow = false;
                    }
                    $sklepMaxIlosc = (int) Configure::read('limity.limit_ilosci');
                    if (!empty($sklepMaxIlosc) && $sklepMaxIlosc > 0 && $item['ilosc'] > $sklepMaxIlosc) {
                        $errors = $this->Translation->get('errors.przekroczonoLimitIlosci');
                        $addAllow = false;
                    }
                    $item['max_ilosc'] = $maxIlosc;
                    $item['zestaw_key'] = $zestawKey;
                    $item['zestaw_id'] = $zestaw->id;
                    $zestawIlosc = $item['ilosc'];
                    $cartItems['items']['z_' . $zestaw->token . '_t_' . $key] = $item;
                    $zestawRazem += $item['cena_za_sztuke'];
                }
            }
            $zestawBonus['zestaw_key'] = $zestawKey;
            $zestawBonus['zestaw_id'] = $zestaw->id;
            $zestawBonus['zestaw_ilosc'] = $zestawIlosc;
            $zestawBonus['max_ilosc'] = $maxIlosc;
            $zestawBonus['cena_za_sztuke'] = $zestawRazem;
            $zestawBonus['cena_razem_z_rabatem'] = $zestawRazem * $zestawIlosc;
            if ($addAllow) {
                $cartItems['items']['z_' . $zestaw->token . '_bonus'] = $zestawBonus;
                $cartItems = $this->cartPrzelicz($cartItems);
                $this->session->write('cartItems', $cartItems);
                Configure::write('cartItems', $cartItems);
                $view = new \Cake\View\View();
                $view->Translation->setLocale(\Cake\I18n\I18n::locale());
                $view->layout = false;
                $view->set('mobile', $this->mobile);
                $view->set('cartItems', $cartItems);
                $view->set('filePath', $this->filePath);
                $view->set('displayPath', $this->displayPath);
                $html = $view->render('Cart/add');
                $view->Translation->afterLayout();
                $returnData = ['status' => 'success', 'cartItems' => $cartItems, 'html' => $html, 'error-message' => $errors];
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Translation->get('errors.przekroczonoDostepnaIlosc')];
            }
            $this->response->type('ajax');
            $this->response->body(json_encode($returnData));
            return $this->response;
        } else {
            $returnData = ['status' => 'error'];
            $this->response->type('ajax');
            $this->response->body(json_encode($returnData));
            return $this->response;
        }
    }

    public function index() {
        if (empty($this->Uzytkownik)) {
            $this->loadModel('Uzytkownik');
        }
        $this->session->delete('orderData');
        $this->footerOsiagniecia = false;
        $cartItems = $this->session->read('cartItems');
        if (empty($cartItems['items'])) {
            $kodRabatowy = null;
            $this->session->delete('rabat_kod');
        }
        $rabatKodHtml = $this->getRabatKodHtml($cartItems);
        $this->set('rabatKodHtml', $rabatKodHtml);
        $this->set('cartItems', $cartItems);
        $uzytkownik = $this->Uzytkownik->find('all', ['conditions' => ['Uzytkownik.id' => $this->userId], 'contain' => ['UzytkownikAdres' => ['conditions' => ['UzytkownikAdres.typ' => 2]]]])->first();
        if (!empty($uzytkownik) && $uzytkownik->bonusy_suma != $this->Auth->user('bonusy_suma')) {
            $this->session->write('Auth.User.bonusy_suma', $uzytkownik->bonusy_suma);
        }
        $deliveryAdresses = $this->Uzytkownik->UzytkownikAdres->find('all', ['conditions' => ['UzytkownikAdres.uzytkownik_id' => $this->userId, 'UzytkownikAdres.typ' => 1], 'order' => ['UzytkownikAdres.domyslny_wysylka' => 'desc']]);
        $deliveryAdressOpitons = [];
        $deliveryAdressLabels = [];
        if (!empty($deliveryAdresses)) {
            foreach ($deliveryAdresses as $dvAdress) {
                $deliveryAdressOpitons[$dvAdress->id] = $this->Txt->printAddres($dvAdress->toArray(), ', ');

                $deliveryAdressLabels[$dvAdress->id] = $this->Txt->printUserInfo($dvAdress->toArray(), '<br/>');
                $deliveryAdressLabels[$dvAdress->id] .= (!empty($deliveryAdressLabels[$dvAdress->id]) ? '<br/>' : '') . $this->Txt->printAddres($dvAdress->toArray(), '<br/>');
            }
        }
        $this->loadModel('Zamowienie');
        $orderData = $this->session->read('orderFormData');
        if ((empty($orderData) && !empty($this->userId)) || (!empty($this->userId) && (empty($orderData['email']) || $this->Auth->user('email') != $orderData['email']))) {
            if (empty($this->Uzytkownik)) {
                $this->loadModel('Uzytkownik');
            }
            $user = $this->Uzytkownik->find('all', ['contain' => ['UzytkownikAdresWysylka' => ['sort' => ['UzytkownikAdresWysylka.domyslny_wysylka' => 'desc']], 'UzytkownikAdresFaktura']])->where(['Uzytkownik.id' => $this->userId])->first();
            if (!empty($user)) {
                $orderData = [];
                if (!empty($user->uzytkownik_adres_wysylka)) {
                    $orderData['imie'] = $user->uzytkownik_adres_wysylka[0]->imie;
                    $orderData['nazwisko'] = $user->uzytkownik_adres_wysylka[0]->nazwisko;
                    $orderData['telefon'] = $user->telefon;
                    $orderData['wysylka_kod'] = $user->uzytkownik_adres_wysylka[0]->kod;
                    $orderData['wysylka_miasto'] = $user->uzytkownik_adres_wysylka[0]->miasto;
                    $orderData['wysylka_ulica'] = $user->uzytkownik_adres_wysylka[0]->ulica . (!empty($user->uzytkownik_adres_wysylka[0]->nr_domu) ? ' ' . $user->uzytkownik_adres_wysylka[0]->nr_domu . (!empty($user->uzytkownik_adres_wysylka[0]->nr_lokalu) ? '/' . $user->uzytkownik_adres_wysylka[0]->nr_lokalu : '') : '');
                    $orderData['email'] = $user->email;
                    $orderData['wysylka_kraj'] = $user->uzytkownik_adres_wysylka[0]->kraj;
                } else {
                    $orderData['imie'] = $user->imie;
                    $orderData['nazwisko'] = $user->nazwisko;
                    $orderData['telefon'] = $user->telefon;
                    $orderData['email'] = $user->email;
                }
                if (!empty($user->uzytkownik_adres_faktura)) {
                    $orderData['faktura_imie'] = $user->uzytkownik_adres_faktura->imie;
                    $orderData['faktura_nazwisko'] = $user->uzytkownik_adres_faktura->nazwisko;
                    $orderData['faktura_firma'] = $user->uzytkownik_adres_faktura->firma;
                    $orderData['faktura_nip'] = $user->uzytkownik_adres_faktura->nip;
                    $orderData['faktura_kod'] = $user->uzytkownik_adres_faktura->kod;
                    $orderData['faktura_miasto'] = $user->uzytkownik_adres_faktura->miasto;
                    $orderData['faktura_ulica'] = $user->uzytkownik_adres_faktura->ulica . (!empty($user->uzytkownik_adres_faktura->nr_domu) ? ' ' . $user->uzytkownik_adres_faktura->nr_domu . (!empty($user->uzytkownik_adres_faktura->nr_lokalu) ? '/' . $user->uzytkownik_adres_faktura->nr_lokalu : '') : '');
                    $orderData['faktura_kraj'] = $user->uzytkownik_adres_faktura->kraj;
                    $orderData['dane_do_fv'] = 'inne';
                }
                $orderData['paczkomat'] = $user->paczkomat;
                $orderData['paczkomat_info'] = $user->paczkomat_info;
                $this->session->write('platnosc', ['rodzaj' => $user['rodzaj_platnosci_id'], 'wysylka' => $user['wysylka_id']]);
                $this->session->write('orderFormData', $orderData);
            }
        }
        $zamowienie = $this->Zamowienie->newEntity($orderData, ['validate' => false]);
        $registerErrors = $this->session->read('registerErrors');
        $this->session->delete('registerErrors');
        if (!empty($registerErrors) && empty($uzytkownik)) {
            $uzytkownik = $registerErrors;
        }
        $crumbs = [$this->Translation->get('breadcrumb.Koszyk') => null];
        $this->loadModel('RodzajPlatnosci');
        if (!empty($cartItems['items'])) {
            $wysylkiPrice = $this->RodzajPlatnosci->Wysylka->getPrice($cartItems['total_weight'],$cartItems['total_z_rabatem']);
            $getPlatnosci = $this->getPlatnosci($cartItems, $cartItems['onlyEWysylka']);
            $this->set('rodzaje_platnosci', $getPlatnosci['platnosci']);
            $this->set('wysylki', $getPlatnosci['wysylki']);
            $platnoscChecked = $this->session->read('platnosc');
        }
        if (empty($this->Towar)) {
            $this->loadModel('Towar');
        }
        $towarCenaDefaultParams = ['Vat', 'Waluta', 'Jednostka', 'conditions' => ['TowarCenaDefault.waluta_id' => $this->active_jezyk->waluta_id]];
        $towarContain = ['HotDeal', 'Bonus', 'Wersja', 'Platforma', 'TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]], 'TowarCenaDefault' => $towarCenaDefaultParams];
        $towarHotDealContain = $towarContain;
        unset($towarHotDealContain[array_search('HotDeal', $towarContain)]);
        $idsInCart = ((!empty($cartItems) && key_exists('in_cart_ids', $cartItems)) ? $cartItems['in_cart_ids'] : null);
        if (empty($idsInCart)) {
            $idsInCart = [null];
        }
//        $polecaneIds = $this->Towar->TowarCena->find('list',['keyField'=>'towar_id','valueField'=>'towar_id'])->where([''])
        $polecane = $this->Towar->find('all', ['limit' => 5, 'order' => ['rand()'], 'contain' => $towarContain])->where(['Towar.polecany' => 1, 'Towar.ilosc >' => 0, 'TowarCenaDefault.cena_sprzedazy <=' => 70, 'Towar.ukryty' => 0, 'Towar.id NOT IN' => $idsInCart]);
        $this->loadModel('WysylkaKrajeKoszty');
        $kosztyKraje = $this->WysylkaKrajeKoszty->find('list', ['groupField' => 'kraj', 'keyField' => 'wysylka_id', 'valueField' => 'koszt'])->toArray();
        $this->set(compact('uzytkownik', 'kosztyKraje', 'deliveryAdressOpitons', 'deliveryAdressLabels', 'wysylkiPrice', 'zamowienie', 'crumbs', 'platnoscChecked', 'polecane'));
        $this->singleSite = true;
    }

    public function remove() {
        $this->autoRender = FALSE;
        $this->viewBuilder()->layout('ajax');
        $cartItems = (!empty($this->session) ? $this->session->read('cartItems') : null);
        $item = $this->request->query('i');
        $zestaw = null;
        $returnData = [];
        if (empty($item)) {
            $zestaw = $this->request->query('z');
            if (!empty($zestaw)) {
                foreach ($cartItems['items'] as $cartItemKey => $cartItem) {
                    if (key_exists('zestaw_key', $cartItem) && $cartItem['zestaw_key'] == $zestaw) {
                        unset($cartItems['items'][$cartItemKey]);
                    }
                }
                $cartItems = $this->cartPrzelicz($cartItems);
                $returnData = ['action' => 'remove', 'zestawKey' => $zestaw, 'cartItems' => $cartItems];
            }
        } else {
            unset($cartItems['items'][$item]);
            $cartItems = $this->cartPrzelicz($cartItems);
            $returnData = ['action' => 'remove', 'itemKey' => $item, 'cartItems' => $cartItems];
        }
        $this->session->write('cartItems', $cartItems);
        Configure::write('cartItems', $cartItems);
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    public function removeAll() {
        $this->autoRender = FALSE;
        $this->viewBuilder()->layout('ajax');
        $cartItems = null;
        $cartItems = $this->cartPrzelicz($cartItems);
        $returnData = ['action' => 'remove', 'cartItems' => null];
        $this->session->write('cartItems', $cartItems);
        Configure::write('cartItems', $cartItems);
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    public function update() {
        $this->autoRender = FALSE;
        $this->viewBuilder()->layout('ajax');
        $cartItems = (!empty($this->session) ? $this->session->read('cartItems') : null);
        $item = $this->request->query('i');
        $ilosc = $this->request->query('v');
        $zestaw = null;
        if (empty($item)) {
            $zestaw = $this->request->query('z');
        }
        $returnData = [];

        $errors = $this->Translation->get('errors.przekroczonoDostepnaIlosc');
        $maxIlosc = 0;
        if (!empty($zestaw)) {
            if ($ilosc <= 0) {
                foreach ($cartItems['items'] as $cartItemKey => $cartItem) {
                    if (key_exists('zestaw_key', $cartItem) && $cartItem['zestaw_key'] == $zestaw) {
                        unset($cartItems['items'][$cartItemKey]);
                    }
                }
                $cartItems = $this->cartPrzelicz($cartItems);
                $returnData = ['action' => 'remove', 'zestawKey' => $zestaw, 'cartItems' => $cartItems];
            } else {
                $sklepMaxIlosc = (int) Configure::read('limity.limit_ilosci');
                if (!empty($sklepMaxIlosc) && $sklepMaxIlosc > 0 && $ilosc > $sklepMaxIlosc) {
                    $errors = $this->Translation->get('errors.przekroczonoLimitIlosci');
                    $returnData = ['action' => 'updateError', 'message' => $errors, 'zestawKey' => $zestaw, 'maxIlosc' => $sklepMaxIlosc, 'cartItems' => $cartItems, 'error-message' => $errors];
                } else {
                    foreach ($cartItems['items'] as $cartItemKey => $cartItem) {
                        if (key_exists('zestaw_key', $cartItem) && $cartItem['zestaw_key'] == $zestaw) {
                            if (!empty($cartItems['items'][$cartItemKey]['zestaw_bonus'])) {
                                if ($ilosc > $cartItems['items'][$cartItemKey]['max_ilosc']) {
                                    $ilosc = $cartItems['items'][$cartItemKey]['max_ilosc'];
                                    $errors = $this->Translation->get('errors.przekroczonoDostepnaIlosc');
                                    $maxIlosc = $ilosc;
                                }
                                $cartItems['items'][$cartItemKey]['zestaw_ilosc'] = $ilosc;
                                $cartItems['items'][$cartItemKey]['cena_razem_z_rabatem'] = $cartItems['items'][$cartItemKey]['cena_za_sztuke'] * $ilosc;
                                continue;
                            }
                            if ($ilosc > $cartItems['items'][$cartItemKey]['max_ilosc']) {
                                $ilosc = $cartItems['items'][$cartItemKey]['max_ilosc'];
                                $errors = $this->Translation->get('errors.przekroczonoDostepnaIlosc');
                                $maxIlosc = $ilosc;
                            }
                            $cartItems['items'][$cartItemKey]['ilosc'] = $ilosc;
                            $cartItems['items'][$cartItemKey]['cena_razem'] = round($cartItems['items'][$cartItemKey]['cena_za_sztuke'] * $ilosc, 2);
                            $cartItems['items'][$cartItemKey]['cena_razem_netto'] = $this->Txt->cenaVat($cartItems['items'][$cartItemKey]['cena_razem'], $cartItems['items'][$cartItemKey]['vat_stawka'], \Cake\Core\Configure::read('ceny.rodzaj'), 'netto', false);
                            $cartItems['items'][$cartItemKey]['cena_razem_brutto'] = $this->Txt->cenaVat($cartItems['items'][$cartItemKey]['cena_razem'], $cartItems['items'][$cartItemKey]['vat_stawka'], \Cake\Core\Configure::read('ceny.rodzaj'), 'brutto', false);
                        }
                    }
                    $cartItems = $this->cartPrzelicz($cartItems);
                    $returnData = ['action' => 'update', 'zestawKey' => $zestaw, 'maxIlosc' => $maxIlosc, 'cartItems' => $cartItems, 'error-message' => $errors];
                }
            }
        } else if (!empty($item)) {
            if ($ilosc <= 0) {
                unset($cartItems['items'][$item]);
                $cartItems = $this->cartPrzelicz($cartItems);
                $returnData = ['action' => 'remove', 'itemKey' => $item, 'cartItems' => $cartItems];
            } else {
                $cartItems['items'][$item]['ilosc'] = $ilosc;
                $cartItems['items'][$item]['cena_razem'] = round($cartItems['items'][$item]['cena_za_sztuke'] * $ilosc, 2);
                $cartItems['items'][$item]['cena_razem_netto'] = $this->Txt->cenaVat($cartItems['items'][$item]['cena_razem'], $cartItems['items'][$item]['vat_stawka'], \Cake\Core\Configure::read('ceny.rodzaj'), 'netto', false);
                $cartItems['items'][$item]['cena_razem_brutto'] = $this->Txt->cenaVat($cartItems['items'][$item]['cena_razem'], $cartItems['items'][$item]['vat_stawka'], \Cake\Core\Configure::read('ceny.rodzaj'), 'brutto', false);
                $addError = false;
                if (!key_exists('karta_podarunkowa', $cartItems['items'][$item]) && (($cartItems['items'][$item]['ilosc'] > $cartItems['items'][$item]['towar_ilosc']) || (!empty($cartItems['items'][$item]['preorder']) && $cartItems['items'][$item]['ilosc'] > $cartItems['items'][$item]['preorder_limit']) || (!empty($cartItems['items'][$item]['hot_deal']) && $cartItems['items'][$item]['ilosc'] > $cartItems['items'][$item]['hot_deal']))) {
                    $addError = $this->Translation->get('errors.przekroczonoDostepnaIlosc');
                } elseif(!key_exists('karta_podarunkowa', $cartItems['items'][$item])) {
                    $sklepMaxIlosc = (int) Configure::read('limity.limit_ilosci');
                    if (!empty($sklepMaxIlosc) && $sklepMaxIlosc > 0 && $cartItems['items'][$item]['ilosc'] > $sklepMaxIlosc) {
                        $addError = $this->Translation->get('errors.przekroczonoLimitIlosci');
                    }
                }
                if (!$addError) {
                    $cartItems = $this->cartPrzelicz($cartItems);
                    $returnData = ['action' => 'update', 'itemKey' => $item, 'cartItems' => $cartItems];
                } else {
                    $cartItems = (!empty($this->session) ? $this->session->read('cartItems') : null);
                    $returnData = ['action' => 'updateError', 'message' => $addError, 'itemKey' => $item, 'cartItems' => $cartItems];
                }
            }
        }
        $this->session->write('cartItems', $cartItems);
        Configure::write('cartItems', $cartItems);
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    public function orderSummary() {
        $this->footerOsiagniecia = false;
        if ($this->request->is('post')) {
            $cartItems = $this->session->read('cartItems');
            $platnoscChecked = $this->session->read('platnosc');
            $kodRabatowy = $this->session->read('rabat_kod');
            $rabatKodHtml = '';
            $kodRabatowyValue = 0;
            $kodRabatowyTyp = 0;
            $razem = $cartItems['total_z_rabatem'];
            if (!empty($kodRabatowy)) {
                $this->loadModel('KodyRabatowe');
                $checkCode = $this->KodyRabatowe->find('all', ['conditions' => ['kod' => $kodRabatowy, 'zablokuj' => 0, 'data_start <=' => date('Y-m-d H:i:s'), 'data_end >=' => date('Y-m-d H:i:s')]])->first();
                if (!empty($checkCode)) {
                    if (!empty($checkCode)) {
                        $rabatValue = round($checkCode->rabat, 2);
                    } else {
                        $rabatValue = 0;
                    }
                    $oldRazem = $razem;
                    if ($checkCode->typ == 'kwota') {
                        $razem = $razem - $rabatValue;
                    } else {
                        $razem = $razem - ($razem * ($rabatValue / 100));
                    }
                    if (!empty($kodRabatowy) && empty($checkCode)) {
                        
                    } else {
                        if ($razem == $oldRazem) {
                            
                        } else {
                            $rabatKodHtml = $this->Txt->kodRabatowyHtml($checkCode, $kodRabatowy, $rabatValue); //$this->Txt->Html->tag('div', $this->Txt->Form->control('promocja_kod', ['type' => 'hidden', 'value' => $kodRabatowy]).$this->Txt->Form->control('promocja_kod_value', ['type' => 'hidden', 'value' => $rabatValue]).$this->Txt->Form->control('promocja_kod_typ', ['type' => 'hidden', 'value' => $checkCode->typ]) . $this->Translation->get('koszyk.KodRabatowy{kod}{rabat}', ['kod'=>$this->Txt->Html->tag('span',$kodRabatowy),'rabat'=>(($checkCode->typ=='kwota')?$this->Txt->Html->tag('span',$this->Txt->cena($rabatValue)):$this->Txt->Html->tag('span',$rabatValue.'%'))], true), ['class' => 'rabatKodVal text-right']);
                            $kodRabatowyValue = $rabatValue;
                            $kodRabatowyTyp = $checkCode->typ;
                        }
                    }
                }
            }
            $this->set('rabatKodHtml', $rabatKodHtml);
            if (!empty($cartItems)) {
                $this->loadModel('UzytkownikAdres');
                $this->loadModel('Uzytkownik');
                $this->loadModel('RodzajPlatnosci');
                $this->loadModel('Wysylka');
                $platnosc = $this->RodzajPlatnosci->find('all')->where(['RodzajPlatnosci.id' => $platnoscChecked['rodzaj']])->first();
                $rodzajWysylki = $this->Wysylka->find('all', ['contain' => ['Vat']])->where(['Wysylka.id' => $platnoscChecked['wysylka']])->first();
                $wysylkaKoszt = 0;
                $kosztyWysylki = $this->RodzajPlatnosci->Wysylka->getPrice($cartItems['total_weight'],$cartItems['total_z_rabatem']);
                if (key_exists($rodzajWysylki->id, $kosztyWysylki)) {
                    $wysylkaKoszt = $kosztyWysylki[$rodzajWysylki->id];
                    if (!empty($platnosc->koszt_dodatkowy) && !empty($rodzajWysylki->koszt_dodatkowy)) {
                        $wysylkaKoszt += $rodzajWysylki->koszt_dodatkowy;
                    }
                }
                $krajDef = \Cake\Core\Configure::read('wysylka.krajPodstawowy');
                $rqData = $this->request->getData();
                if (!empty($rqData['wysylka_kraj']) && $rqData['wysylka_kraj'] != $krajDef && empty($rodzajWysylki->elektroniczna)) {
                    $this->loadModel('WysylkaKrajeKoszty');
                    $kosztyKraje = $this->WysylkaKrajeKoszty->find('list', ['groupField' => 'kraj', 'keyField' => 'wysylka_id', 'valueField' => 'koszt'])->toArray();
                    $_kd = \Cake\Core\Configure::read('wysylka.krajKosztDodatkowy');
                    $kosztyKrajeDef = (!empty($_kd) ? $_kd : 0);
                    if (!empty($kosztyKraje) && key_exists($rqData['wysylka_kraj'], $kosztyKraje) && key_exists($rodzajWysylki->id, $kosztyKraje[$rqData['wysylka_kraj']])) {
                        $wysylkaKoszt += $kosztyKraje[$rqData['wysylka_kraj']][$rodzajWysylki->id];
                    } else {
                        $wysylkaKoszt += $kosztyKrajeDef;
                    }
                }
                $wysylkaKosztNetto = $wysylkaKoszt / (1 + ($rodzajWysylki->vat->stawka / 100));
                if (!empty($rodzajWysylki->paczkomat)) {
                    if (empty($rqData['paczkomat'])) {
                        $this->Flash->error($this->Translation->get('errors.wybierzPaczkomat'));
                        return $this->redirect(['action' => 'index']);
                    }
                } else {
                    $rqData['paczkomat'] = null;
                    $rqData['paczkomat_info'] = null;
                }
                $wysylka = [];
                $bonusUzyty = 0;
                if (!empty($this->userId) && !empty($rqData['bonusowe_zlotowki'])) {
                    $bonusUzyty = $this->Auth->user('bonusy_suma');
                }

                if (empty($wysylka)) {
                    $wysylka = [
                        'ulica' => (!empty($rqData['wysylka_ulica']) ? $rqData['wysylka_ulica'] : null),
                        'kod' => (!empty($rqData['wysylka_kod']) ? $rqData['wysylka_kod'] : null),
                        'miasto' => (!empty($rqData['wysylka_miasto']) ? $rqData['wysylka_miasto'] : null),
                        'imie' => (!empty($rqData['imie']) ? $rqData['imie'] : null),
                        'nazwisko' => (!empty($rqData['nazwisko']) ? $rqData['nazwisko'] : null),
                        'telefon' => (!empty($rqData['telefon']) ? $rqData['telefon'] : null),
                        'email' => (!empty($rqData['email']) ? $rqData['email'] : null),
                        'kraj' => (!empty($rqData['wysylka_kraj']) ? $rqData['wysylka_kraj'] : null)
                    ];
                }
                $daneFaktura = [];

                $uzytkownik = [
                    'imie' => $rqData['imie'],
                    'nazwisko' => $rqData['nazwisko'],
                    'email' => $rqData['email'],
                    'firma' => null,
                    'nip' => null,
                    'telefon' => $rqData['telefon']
                ];
                if (!empty($rqData['faktura']) && $rqData['dane_do_fv'] == 'inne') {
                    foreach ($rqData as $key => $value) {
                        if (strpos($key, 'faktura_') !== false) {
                            $daneFaktura[str_replace('faktura_', '', $key)] = $value;
                        }
                    }
                }
                if (!empty($this->userId)) {
                    if (empty($this->Uzytkownik)) {
                        $this->loadModel('Uzytkownik');
                    }
                    $tmpUser = $this->Uzytkownik->find('all', ['contain' => ['UzytkownikAdresWysylka', 'UzytkownikAdresFaktura']])->where(['Uzytkownik.id' => $this->userId])->first();
                    $uzytkownik['email'] = $tmpUser->email;
                    $saveUser = [];
                    if (empty($tmpUser->telefon)) {
                        $tmpUser->set('telefon', $uzytkownik['telefon']);
                        $saveUser['telefon'] = $uzytkownik['telefon'];
                    }
                    if (empty($tmpUser->uzytkownik_adres_wysylka) && !empty($wysylka)) {
                        $saveUser['uzytkownik_adres_wysylka'] = [array_merge($wysylka, ['typ' => 1, 'domyslna_wysylka' => 1])];
                    }
                    if (empty($tmpUser->uzytkownik_adres_faktura) && !empty($daneFaktura)) {
                        $saveUser['uzytkownik_adres_faktura'] = [array_merge($wysylka, ['typ' => 2])];
                    }
                    $saveUser['wysylka_id'] = $rodzajWysylki->id;
                    $saveUser['rodzaj_platnosci_id'] = $platnosc->id;
                    if (!empty($rqData['paczkomat'])) {
                        $saveUser['paczkomat'] = $rqData['paczkomat'];
                        $saveUser['paczkomat_info'] = $rqData['paczkomat_info'];
                    }
                    if (!empty($saveUser)) {
                        $tmpUser = $this->Uzytkownik->patchEntity($tmpUser, $saveUser);
                        $this->Uzytkownik->save($tmpUser);
                    }
                }
                if (empty($daneFaktura)) {
                    $daneFaktura = $wysylka;
                }
                if (!empty($wysylka)) {
                    $wartoscProduktow = $razem;
                    $wartoscProduktowNetto = $wartoscProduktow;
                    $wartoscRazem = $wartoscProduktow + $wysylkaKoszt;
                    if (!empty($bonusUzyty)) {
                        if ($bonusUzyty > $wartoscRazem) {
                            $bonusUzyty = $wartoscRazem;
                            $wartoscRazem = 0;
                        } else {
                            $wartoscRazem = $wartoscRazem - $bonusUzyty;
                        }
                    }
                    $wartoscRazemNetto = $cartItems['total_z_rabatem_netto'] + $wysylkaKosztNetto;
                    $wartoscVat = $wartoscRazem - $wartoscRazemNetto;
                    $token = uniqid();
                    $orderData = [
                        'uzytkownik_id' => $this->userId,
                        'lng' => \Cake\I18n\I18n::locale(),
                        'waluta_id' => $this->defCurrency['id'],
                        'waluta_symbol' => $this->defCurrency['symbol'],
                        'imie' => $uzytkownik['imie'],
                        'nazwisko' => $uzytkownik['nazwisko'],
                        'email' => $uzytkownik['email'],
                        'firma' => $uzytkownik['firma'],
                        'nip' => $uzytkownik['nip'],
                        'faktura' => $rqData['faktura'],
                        'adres_faktury' => $this->Txt->printAddres($daneFaktura, '<br/>'),
                        'adres_wysylki' => $this->Txt->printAddres($wysylka, '<br/>'),
                        'wysylka_ulica' => $wysylka['ulica'],
                        'wysylka_kod' => $wysylka['kod'],
                        'wysylka_miasto' => $wysylka['miasto'],
                        'wysylka_imie' => $wysylka['imie'],
                        'wysylka_nazwisko' => $wysylka['nazwisko'],
                        'wysylka_telefon' => $wysylka['telefon'],
                        'wysylka_email' => $wysylka['email'],
                        'wysylka_kraj' => $wysylka['kraj'],
                        'faktura_imie' => $daneFaktura['imie'],
                        'faktura_nazwisko' => $daneFaktura['nazwisko'],
                        'faktura_firma' => (!empty($daneFaktura['firma']) ? $daneFaktura['firma'] : null),
                        'faktura_nip' => (!empty($daneFaktura['nip']) ? $daneFaktura['nip'] : null),
                        'faktura_ulica' => $daneFaktura['ulica'],
                        'faktura_kod' => $daneFaktura['kod'],
                        'faktura_miasto' => $daneFaktura['miasto'],
                        'faktura_kraj' => $daneFaktura['kraj'],
                        'telefon' => $uzytkownik['telefon'],
                        'wartosc_produktow' => $wartoscProduktow,
                        'wartosc_produktow_netto' => $wartoscProduktowNetto,
                        'wartosc_vat' => $wartoscVat,
                        'wartosc_razem_netto' => $wartoscRazemNetto,
                        'wartosc_razem' => $wartoscRazem,
                        'uwagi' => trim(strip_tags($rqData['uwagi'])),
                        'status' => 'zlozone',
                        'data' => new \DateTime(date('Y-m-d H:i:s')),
                        'rabat' => null,
                        'token' => $token,
                        'rodzaj_platnosci_id' => $platnosc->id,
                        'wysylka_id' => $rodzajWysylki->id,
                        'platnosc_wysylka' => $platnosc->nazwa . ' - ' . $rodzajWysylki->nazwa,
                        'wysylka_koszt' => $wysylkaKoszt,
                        'bonus_uzyty' => $bonusUzyty,
                        'paczkomat' => $rqData['paczkomat'],
                        'paczkomat_info' => $rqData['paczkomat_info'],
                        'regulamin' => $rqData['regulamin'],
                        'zgoda' => (!empty($rqData['zgoda']) ? 1 : 0),
                        'zgoda2' => (!empty($rqData['zgoda2']) ? 1 : 0)
                    ];
                    if (!empty($rqData['punkty_odbioru_id'])) {
                        $this->loadModel('PunktyOdbioru');
                        $punktOdbioru = $this->PunktyOdbioru->find()->where(['PunktyOdbioru.id' => $rqData['punkty_odbioru_id']])->first();
                        if (!empty($punktOdbioru)) {
                            $orderData['punkty_odbioru_id'] = $punktOdbioru->id;
                            $orderData['punkty_odbioru_info'] = $punktOdbioru->adres;
                            $orderData['adres_wysylki'] = null;
                        }
                    }
                    if (!empty($kodRabatowyValue)) {
                        $orderData['kod_rabatowy'] = $kodRabatowy;
                        $orderData['kod_rabatowy_typ'] = $kodRabatowyTyp;
                        $orderData['kod_rabatowy_wartosc'] = $kodRabatowyValue;
                    }
                    $rabatKwotaZestaw = 0;
                    $zestawBonus = [];
                    $bonusoweZlotowki = 0;
                    $kartyPodarunkowe = [];
                    $kartyPodarunkoweIds = [];
                    foreach ($cartItems['items'] as $itemKey => &$zamItem) {
                        if (!empty($zamItem['zestaw_bonus'])) {
                            //Przydzielanie bonusow za zestaw
                            $zestawBonus[$zamItem['zestaw_key']] = $zamItem;
                            if ($zamItem['typ'] == 'kwota') {
                                $rabatKwotaZestaw += ($zamItem['wartosc'] * $zamItem['zestaw_ilosc']);
                            }
                            unset($cartItems['items'][$itemKey]);
                            continue;
                        }
                        $zamItem['rabat'] = (!empty($zamItem['rabat']) ? $zamItem['rabat'] : null); //$orderData['rabat_wartosc'];
                       
                    }
                    $orderData['bonus_suma'] = $bonusoweZlotowki;
                    $orderData['rabat_zestaw'] = $rabatKwotaZestaw;
                    $orderData['zamowienie_towar'] = $cartItems['items'];
                    if (!empty($kartyPodarunkowe)) {
                        $orderData['karta_podarunkowa_kod'] = $kartyPodarunkowe;
                    }
                    $bony = [];
                    if (!empty($zestawBonus)) {
                        foreach ($zestawBonus as $bonusItem) {
                            if ($bonusItem['typ'] == 'bon') {
                                for ($i = 1; $i <= $bonusItem['zestaw_ilosc']; $i++) {
                                    $bony[] = [
                                        'nazwa' => Configure::read('zestaw.nazwa_bonu'),
                                        'kod' => $this->Txt->getBonCode(6),
                                        'rabat' => $bonusItem['wartosc'],
                                        'zablokuj' => 1,
                                        'data_dodania' => date('Y-m-d H:i:s'),
                                        'typ' => 'kwota'
                                    ];
                                }
                            }
                        }
                    }
                    if (!empty($bony)) {
                        $orderData['bony'] = $bony;
                    } else {
                        unset($orderData['bony']);
                    }
                    $this->session->write('orderData', $orderData);
                    $this->loadModel('Zamowienie');
                    $zamowienie = $this->Zamowienie->newEntity($orderData);
                    $this->set('zamowienie', $orderData);
                    $this->set('zestawBonusy', $zestawBonus);
                    $crumbs = [$this->Translation->get('breadcrumb.Koszyk') => \Cake\Routing\Router::url(['action' => 'index']), $this->Translation->get('breadcrumb.PodsumowanieZamowienia') => null];
                    $this->set('crumbs', $crumbs);
                    return $this->redirect(['action' => 'submitOrder']);
                } else {
                    $this->Flash->error($this->Translation->get('errors.InvalidShippingAddress'));
                    return $this->redirect(['action' => 'index']);
                }
            } else {
                return $this->redirect(['action' => 'index']);
            }
        } else {
            return $this->redirect(['action' => 'index']);
        }
    }

    private function kodKarta() {
        $kod = $this->Txt->getBonCode(8);
        if (empty($this->KartaPodarunkowaKod)) {
            $this->loadModel('KartaPodarunkowaKod');
        }
        $check = $this->KartaPodarunkowaKod->find()->where(['KartaPodarunkowaKod.kod' => $kod])->count();
        if (!empty($check)) {
            return $this->kodKarta();
        } else {
            return $kod;
        }
    }

    private function addBon($bon) {
        if (empty($bon)) {
            return false;
        }
        if (empty($this->KodyRabatowe)) {
            $this->loadModel('KodyRabatowe');
        }
        $bonEntity = $this->KodyRabatowe->newEntity($bon);
        if (!$this->KodyRabatowe->save($bonEntity)) {
            $errors = $bonEntity->errors();
            if (key_exists('kod', $errors)) {
                $bon['kod'] = $this->Txt->getBonCode(6);
                return $this->addBon($bon);
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    private function newOrderNumber() {
        $orderNumber = $this->Txt->randomOrderNumber();
        if (empty($this->Zamowienie)) {
            $this->loadModel('Zamowienie');
        }
        $checkOrder = $this->Zamowienie->find()->where(['Zamowienie.numer' => $orderNumber])->count();
        if (empty($checkOrder)) {
            return $orderNumber;
        } else {
            return $this->newOrderNumber();
        }
    }

    public function submitOrder() {
        $orderData = $this->session->read('orderData');
        $this->session->delete('orderData');
        if (!empty($orderData)) {
            $this->loadModel('Zamowienie');
            $bony = [];
            if (!empty($orderData['bony'])) {
                $bony = $orderData['bony'];
                unset($orderData['bony']);
            }

            $orderData['numer'] = $this->newOrderNumber();
            $zamowienie = $this->Zamowienie->newEntity($orderData);
            if ($this->Zamowienie->save($zamowienie, ['validate' => 'order'])) {
                if (!empty($zamowienie->bonus_uzyty)) {
                    if (empty($this->Uzytkownik)) {
                        $this->loadModel('Uzytkownik');
                    }
                    $user = $this->Uzytkownik->find()->where(['Uzytkownik.id' => $this->Auth->user('id')])->first();
                    if (!empty($user)) {
                        $tmpBonus = $user->bonusy_suma;
                        $tmpBonus = $tmpBonus - $zamowienie->bonus_uzyty;
                        if ($tmpBonus < 0) {
                            $tmpBonus = 0;
                        }
                        $user->set('bonusy_suma', $tmpBonus);
                        $this->Uzytkownik->save($user);
                        $this->session->write('Auth.User.bonusy_suma', $tmpBonus);
                    }
                }
                if (!empty($bony)) {
                    foreach ($bony as $bon) {
                        $bon['zamowienie_id'] = $zamowienie->id;
                        $this->addBon($bon);
                    }
                }
                $cartItems = $this->session->read('cartItems');
                if (!empty($cartItems['cart_id'])) {
                    $this->loadModel('UzytkownikKoszyk');
                    $this->UzytkownikKoszyk->deleteAll(['id' => $cartItems['cart_id']]);
                }
                $this->session->delete('cartItems');
                $this->session->delete('rabat_kod');
                $this->session->delete('orderFormData');
                $zamowienie = $this->Zamowienie->find('all', ['conditions' => ['Zamowienie.id' => $zamowienie->id], 'contain' => ['PunktyOdbioru', 'Platnosc' => ['RodzajPlatnosci'], 'RodzajPlatnosci', 'Wysylka', 'ZamowienieTowar' => ['sort' => ['ZamowienieTowar.id' => 'asc']], 'KodyRabatowe']])->first();
                $changeIlosc = [];
                $changeHotDealIlosc = [];
                $preorderIds = [];
                if (!empty($zamowienie->zamowienie_towar)) {
                    foreach ($zamowienie->zamowienie_towar as $itemTowar) {
                        if (!empty($itemTowar->preorder)) {
                            $preorderIds[$itemTowar->towar_id] = $itemTowar->towar_id;
                        }
                        if (!key_exists($itemTowar->towar_id, $changeIlosc)) {
                            $changeIlosc[$itemTowar->towar_id] = 0;
                        }
                        $changeIlosc[$itemTowar->towar_id] += $itemTowar->ilosc;
                        if (!empty($itemTowar->hot_deal)) {
                            if (!key_exists($itemTowar->towar_id, $changeHotDealIlosc)) {
                                $changeHotDealIlosc[$itemTowar->towar_id] = 0;
                            }
                            $changeHotDealIlosc[$itemTowar->towar_id] += $itemTowar->ilosc;
                        }
                    }
                }
                if (!empty($changeIlosc)) {
                    if (empty($this->Towar)) {
                        $this->loadModel('Towar');
                    }
                    if (empty($this->AllegroAukcja)) {
                        $this->loadModel('AllegroAukcja');
                    }
                    $actIlosci = $this->Towar->find('list', ['keyField' => 'id', 'valueField' => 'ilosc'])->where(['id IN' => array_keys($changeIlosc)])->toArray();
                    $allegroIlosci = $this->AllegroAukcja->find('list', ['groupField' => 'towar_id', 'keyField' => 'item_id', 'valueField' => 'iloscToUpdate'])->where(['towar_id IN' => array_keys($changeIlosc)])->toArray();
                    $actPreorderIlosci = [];
                    if (!empty($preorderIds)) {
                        $actPreorderIlosci = $this->Towar->find('list', ['keyField' => 'id', 'valueField' => 'preorder_limit'])->where(['id IN' => $preorderIds])->toArray();
                    }
                    $allegroItemsToChange = [];
                    foreach ($changeIlosc as $towarId => $iloscToChange) {
                        if (!empty($allegroIlosci[$towarId])) {
                            foreach ($allegroIlosci[$towarId] as $allegroItemId => $allegroOfferIlosc) {
                                $allegroItemsToChange[$allegroItemId] = ($allegroOfferIlosc - $iloscToChange);
                            }
                        }
                        $newIlosc = ($actIlosci[$towarId] - $iloscToChange);
                        if ($newIlosc < 0) {
                            $newIlosc = 0;
                        }
                        $upData = ['ilosc' => $newIlosc];
                        if (!empty($actPreorderIlosci) && !empty($actPreorderIlosci[$towarId])) {
                            $upData['preorder_limit'] = ($actPreorderIlosci[$towarId] - $iloscToChange);
                        }
                        $this->Towar->updateAll($upData, ['id' => $towarId]);
                    }
                    if (!empty($allegroItemsToChange)) {
                        foreach ($allegroItemsToChange as $allegroItemId => $allegroIloscToSave) {
                            $this->AllegroAukcja->updateAll(['iloscToUpdate' => $allegroIloscToSave], ['item_id' => $allegroItemId]);
                        }
                    }
                }
                if (!empty($changeHotDealIlosc)) {
                    if (empty($this->HotDeal)) {
                        $this->loadModel('HotDeal');
                    }
                    $actIlosci = $this->HotDeal->find('list', ['keyField' => 'towar_id', 'valueField' => 'ilosc'])->where(['towar_id IN' => array_keys($changeHotDealIlosc)])->toArray();
                    $actSprzedano = $this->HotDeal->find('list', ['keyField' => 'towar_id', 'valueField' => 'sprzedano'])->where(['towar_id IN' => array_keys($changeHotDealIlosc)])->toArray();
                    foreach ($changeHotDealIlosc as $towarId => $iloscToChange) {
                        $newSprzedano = $actSprzedano[$towarId] + $iloscToChange;
                        $newIlosc = ($actIlosci[$towarId] - $iloscToChange);
                        if ($newIlosc < 0) {
                            $newIlosc = 0;
                        }
                        $this->HotDeal->updateAll(['ilosc' => $newIlosc, 'sprzedano' => $newSprzedano], ['towar_id' => $towarId]);
                    }
                }
                $platnosc = null;
                if (!empty($zamowienie->rodzaj_platnosci) && !empty($zamowienie->rodzaj_platnosci->platnosc_elektroniczna)) {
                    $token = uniqid();
                    $platnosc = [
                        'zamowienie_id' => $zamowienie->id,
                        'uzytkownik_id' => $zamowienie->uzytkownik_id,
                        'rodzaj_platnosci_id' => $zamowienie->rodzaj_platnosci_id,
                        'waluta_id' => $zamowienie->waluta_id,
                        'kwota' => $zamowienie->wartosc_razem,
                        'opis' => __('Platnosc za zamowienie {0}', [$zamowienie->id . (!empty($zamowienie->zamowienie_niedostepne) ? ' / ' . $zamowienie->zamowienie_niedostepne->id : '')]),
                        'opis_lng' => $this->Translation->get('platnosc.platnoscZaZamowienie{0}', [(!empty($zamowienie->numer_cdn) ? $zamowienie->numer_cdn : $zamowienie->id) . (!empty($zamowienie->zamowienie_niedostepne) ? ' / ' . (!empty($zamowienie->zamowienie_niedostepne->numer_cdn) ? $zamowienie->zamowienie_niedostepne->numer_cdn : $zamowienie->zamowienie_niedostepne->id) : '')]),
                        'token' => $token,
                        'token2' => $zamowienie->id . '_' . $token,
                        'status' => 'oczekuje',
                        'data' => date('Y-m-d H:i:s')
                    ];
                    $this->loadModel('Platnosc');
                    $platnoscEntity = $this->Platnosc->newEntity($platnosc);
                    if ($this->Platnosc->save($platnoscEntity)) {
                        $zamowienie->set('platnosc', $platnoscEntity);
                    }
                }

                $koszykSessionId = $this->session->read('Koszyk.session_id');
                if (empty($koszykSessionId)) {
                    $koszykSessionId = session_id();
                }
                $this->loadModel('Koszyk');
                $cart = $this->Koszyk->find('all', ['conditions' => ['Koszyk.session_id' => $koszykSessionId, 'Koszyk.aktywny' => 1]])->first();
                if (!empty($cart)) {
                    $cart->zamowienie_id = $zamowienie->id;
                    $cart->aktywny = 0;
                    $this->Koszyk->save($cart);
                }

                $this->loadComponent('Mail');
                $this->loadComponent('Replace');
                $this->loadModel('Szablon');
                $razem = $zamowienie->wartosc_razem;
                $replace = $this->Replace->getReplaceOrderData($zamowienie);
                $this->loadComponent('Order');
                $sendTo = [];
                $sendToPh = Configure::read('admin.ph_info');
                $phEmail = null;


                $szablon = $this->Szablon->findByNazwa('zamowienie_zlozone')->first();
                $message = $this->Replace->getTemplate($szablon->tresc, $replace, $zamowienie->zamowienie_towar);
//                $f= fopen(ROOT.DS.'email.html', 'a+');
//                fwrite($f, $message);
//                fclose($f);
                $temat = $this->Replace->getTemplate($szablon->temat, $replace);
                $attachment = [];
                if (file_exists($this->filePath['my_files'] . 'regulamin.pdf')) {
                    $attachment['regulamin.pdf'] = $this->filePath['my_files'] . 'regulamin.pdf';
                }
                if (file_exists($this->filePath['my_files'] . 'formularz_reklamacji.pdf')) {
                    $attachment['formularz_reklamacji.pdf'] = $this->filePath['my_files'] . 'formularz_reklamacji.pdf';
                }
                $this->Mail->sendMail($zamowienie->email, $temat, $message, ['replyTo' => ((!empty($sendToPh) && !empty($phEmail)) ? $phEmail : Configure::read('mail.domyslny_email')), 'attachment' => $attachment]);
                $szablonInfo = $this->Szablon->findByNazwa('admin_info_nowe_zamowienie')->first();
                $messageInfo = $this->Replace->getTemplate($szablonInfo->tresc, $replace, $zamowienie->zamowienie_towar);
                $tematInfo = $this->Replace->getTemplate($szablonInfo->temat, $replace);
                $sendTo[] = Configure::read('mail.domyslny_email');
                //, ['attachment' => [$orderFileName => $this->filePath['orders'] . $orderFileName]]
                $this->Mail->sendInfo($sendTo, $tematInfo, $messageInfo);
                return $this->redirect(['action' => 'orderCompleted', $zamowienie->token]);
            } else {
                $this->Flash->error($this->Translation->get('errors.saveOrderError'));
                return $this->redirect(['action' => 'index']);
            }
        } else {
            $this->Flash->error($this->Translation->get('errors.BrakDanychZamowowieniaSesjaWygasla'));
            return $this->redirect(['action' => 'index']);
        }
    }

    public function orderCompleted($token = null) {
        if (empty($token)) {
            return $this->redirect('/');
        }
        $this->loadModel('Zamowienie');
        $zamowienie = $this->Zamowienie->find('all', ['conditions' => ['Zamowienie.token' => $token], 'contain' => ['ZamowienieTowar', 'RodzajPlatnosci', 'Wysylka', 'Platnosc' => ['RodzajPlatnosci']]])->first();
        $this->set(compact('zamowienie'));

        $dataLayerProducts = [];
        $this->loadModel('Towar');
        $ceneoIds = [];
        foreach ($zamowienie->zamowienie_towar as $zamItem) {
            $dataLayerProducts[] = [
                'sku' => $zamItem->kod,
                'name' => $zamItem->nazwa,
                'category' => $this->Towar->getCategoryNameByTowarId($zamItem->towar_id),
                'price' => $zamItem->cena_za_sztuke_brutto,
                'quantity' => $zamItem->ilosc
            ];
            for ($i = 1; $i <= $zamItem->ilosc; $i++) {
                $ceneoIds[] = $zamItem->towar_id;
            }
        }
        $orderDataLayer = json_encode([
            'transactionId' => (!empty($zamowienie->numer) ? $zamowienie->numer : $zamowienie->id),
            'email' => $zamowienie->email,
            'transactionAffiliation' => Configure::read('dane.nazwa_firmy'),
            'transactionTotal' => $zamowienie->wartosc_razem,
            'transactionShipping' => (!empty($zamowienie->wysylka_koszt) ? $zamowienie->wysylka_koszt : 0),
            'transactionProducts' => $dataLayerProducts,
            'ceneoIds' => join('#', $ceneoIds)
        ]);
        $this->set('orderDataLayer', $orderDataLayer);
    }

    public function setPlatnosc() {
        $this->autoRender = false;
        $returnData = [];
        if ($this->request->is('ajax')) {
            $dane = $this->request->getData();
            $this->session->write('platnosc', ['rodzaj' => $dane['rodzaj'], 'wysylka' => $dane['wysylka']]);
            $returnData = ['status' => 'success'];
        } else {
            $returnData = ['status' => 'error'];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    public function checkCode() {
        $this->autoRender = false;
        $this->viewBuilder()->layout('ajax');
        $returnData = [];
        if ($this->request->is(['ajax', 'post'])) {
            $kod = $this->request->getData('kod');
            if (!empty($kod)) {
                $this->loadModel('KodyRabatowe');
                $checkCode = $this->KodyRabatowe->find('all', ['conditions' => ['kod' => $kod, 'zablokuj' => 0, 'data_start <=' => date('Y-m-d H:i:s'), 'data_end >=' => date('Y-m-d H:i:s')]])->first();
                if (!empty($checkCode)) {
                    if (!empty($checkCode)) {
                        $rabatValue = round($checkCode->rabat, 2);
                    } else {
                        $rabatValue = 0;
                    }
                    $cartItems = $this->session->read('cartItems');

                    $razem = $cartItems['total_brutto'];
                    $oldRazem = $razem;
                    if ($checkCode->typ == 'kwota') {
                        $razem = $razem - $rabatValue;
                    } else {
                        $razem = $razem - ($razem * ($rabatValue / 100));
                    }
                    if (!empty($kod) && empty($checkCode)) {
                        $returnData = ['status' => 'error', 'message' => $this->Translation->get('errors.KodRabatowyJestNieprawidlowy')];
                    } else {
                        $this->session->write('rabat_kod', $kod);
                        if ($razem == $oldRazem) {
                            $returnData = ['status' => 'success', 'value' => $this->Txt->cena($razem), 'valueHtml' => ''];
                        } else
                            $returnData = ['status' => 'success', 'message' => $this->Translation->get('koszyk.KodRabatowy{0}ZostalDodanyDoKoszyka', [$kod]), 'value' => $this->Txt->cena($razem), 'valueHtml' => $this->Txt->kodRabatowyHtml($checkCode, $kod, $rabatValue)];
                    }
                } else {
                    $returnData = ['status' => 'error', 'message' => $this->Translation->get('errors.KodRabatowyJestNieprawidlowy')];
                }
            } else {
                $returnData = ['status' => 'error', 'message' => $this->Translation->get('errors.KodRabatowyJestNieprawidlowy')];
            }
        } else {
            $returnData = ['status' => 'error'];
        }
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    public function removeCode() {
        $this->autoRender = false;
        $this->session->delete('rabat_kod');
        $returnData = ['status' => 'success'];
        $this->response->type('json');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }

    public function setOrderForm() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            $orderData = $this->session->read('orderFormData');
            if (empty($orderData)) {
                $orderData = [];
            }
            if (!empty($rqData)) {
                foreach ($rqData as $field => $value) {
                    if (is_array($value)) {
                        foreach ($value as $childField => $childValue) {
                            $rqData[$field][$childField] = $childValue;
                        }
                    } else {
                        $orderData[$field] = $value;
                    }
                }
            }
            $this->session->write('orderFormData', $orderData);
        }
        $this->response->type('ajax');
        return $this->response;
    }

    public function payment($type = 3, $token = null) {
        $this->autoRender = false;
        $this->loadModel('RodzajPlatnosci');
        $rodzajPlatnosci = $this->RodzajPlatnosci->find('all')->where(['id' => $type])->first();
        if (!empty($rodzajPlatnosci)) {
            if ($type == 5) {
                $this->checkPayuPayment($rodzajPlatnosci, $token);
            }
        } else {
            \Cake\Log\Log::write('error', __('Peyment return: Type of payment not found'));
            die('ERROR 5');
        }
    }

    public function orderPay($token) {
        $this->loadModel('Platnosc');
        $this->loadModel('Zamowienie');
        $platnosc = $this->Platnosc->find('all', ['conditions' => ['Platnosc.token' => $token]])->first();
        $zamowienie = $this->Zamowienie->find('all', ['contain' => ['ZamowienieTowar', 'Platnosc' => ['conditions' => ['Platnosc.id' => $platnosc->id]]]])->where(['Zamowienie.id' => $platnosc->zamowienie_id])->first();

        $status = $this->request->query('status');
        $this->set(compact('zamowienie', 'status'));
    }

    private function checkDotpayPayment($rodzajPlatnosci) {
        $statusArray = ['new' => 'oczekuje',
            'processing' => 'rozpoczeta',
            'completed' => 'wykonana',
            'rejected' => 'odrzucona',
            'processing_realization_waiting' => 'rozpoczeta',
            'processing_realization' => 'rozpoczeta'];
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            $sign = $rodzajPlatnosci->konfig1;
            $fromSign = $rqData['signature'];
            unset($rqData['signature']);
            foreach ($rqData as $valueKey => $value) {
                $sign .= $value;
            }
            $sign = hash('sha256', $sign);

            if ($fromSign == $sign) {
                $this->loadModel('Platnosc');
                $platnosc = $this->Platnosc->find('all', ['conditions' => ['Platnosc.token' => $rqData['control']], 'contain' => ['Zamowienie']])->first();
                if (!empty($platnosc)) {
                    if ($platnosc->status == 'wykonana') {
                        die('ok');
                    }
                    $platnosc->status = $statusArray[$rqData['operation_status']];
                    $platnosc->platnosc_kwota = $rqData['operation_amount'];
                    $platnosc->platnosc_numer = $rqData['operation_number'];
                    if ($platnosc->status == 'wykonana') {
                        $platnosc->zamowienie->status = 'oplacone';
                        if ($this->Platnosc->save($platnosc)) {
                            $wyslijEmail = Configure::read('zamowienie.wyslij_potwierdzenie_wplaty');
                            if (!empty($wyslijEmail)) {
                                if (empty($this->Zamowienie)) {
                                    $this->loadModel('Zamowienie');
                                }
                                $zamowienie = $this->Zamowienie->find('all', ['contain' => ['ZamowienieTowar', 'RodzajPlatnosci', 'Platnosc' => ['RodzajPlatnosci', 'conditions' => ['Platnosc.id' => $platnosc->id]]]])->where(['Zamowienie.id' => $platnosc->zamowienie->id])->first();
                                $replace = [
                                    'zamowienie-id' => $zamowienie->id,
                                    'zamowienie-numer' => (!empty($zamowienie->numer_cdn) ? $zamowienie->numer_cdn : $zamowienie->id),
                                    'zamowienie-imie' => $zamowienie->imie,
                                    'zamowienie-nazwisko' => $zamowienie->nazwisko,
                                    'zamowienie-adres-wysylki' => $zamowienie->adres_wysylki,
                                    'zamowienie-adres-faktury' => $zamowienie->adres_faktury,
                                    'zamowienie-email' => $zamowienie->email,
                                    'zamowienie-nazwa' => $zamowienie->nazwa,
                                    'zamowienie-nip' => $zamowienie->nip,
                                    'zamowienie-telefon' => $zamowienie->telefon,
                                    'zamowienie-wartosc-razem' => $this->Txt->cena($razem),
                                    'zamowienie-wartosc-produktow' => $this->Txt->cena($zamowienie->wartosc_produktow),
                                    'zamowienie-koszt-wysylki' => $this->Txt->cena($zamowienie->wysylka_koszt),
                                    'zamowienie-platnosc-wysylka' => $zamowienie->platnosc_wysylka,
                                    'zamowienie-waluta' => $zamowienie->waluta_symbol,
                                    'zamowienie-uwagi' => nl2br($zamowienie->uwagi),
                                    'zamowienie-status' => $this->Translation->get('statusy_zamowien.' . $zamowienie->status),
                                    'zamowienie-data' => (!empty($zamowienie->data) ? $zamowienie->data->format('Y-m-d H:i') : ''),
                                    'zamowienie-token' => $zamowienie->token,
                                    'zamowienie-rabat' => $zamowienie->rabat_wartosc,
                                    'zamowienie-opiekun' => $zamowienie->administrator_dane,
                                    'zamowienie-opiekun-id' => $zamowienie->administrator_id,
                                    'zamowienie-rabat-zestaw' => $zamowienie->rabat_zestaw,
                                    'zamowienie-kod-rabatowy' => $zamowienie->kod_rabatowy,
                                    'zamowienie-kod-rabatowy-typ' => $zamowienie->kod_rabatowy_typ,
                                    'zamowienie-kod-rabatowy-wartosc' => $zamowienie->kod_rabatowy_wartosc,
                                    'zamowienie-rabaty' => (!empty($rabatyInfo) ? join('', $rabatyInfo) : ''),
                                    'zamowienie-bony' => (!empty($bonyInfo) ? $this->Txt->Html->tag('div', $this->Txt->Html->tag('div', $this->Translation->get('zamowienie.bonyInfo', [], true)) . join('', $bonyInfo)) : ''),
                                    'adres-ip' => $this->request->clientIp()
                                ];
                                $this->loadComponent('Replace');
                                $this->loadComponent('Mail');
                                $this->loadModel('Szablon');
                                $szablon = $this->Szablon->findByNazwa('zamowienie_oplacone')->first();
                                $message = $this->Replace->getTemplate($szablon->tresc, $replace, $zamowienie->zamowienie_towar);
                                $temat = $this->Replace->getTemplate($szablon->temat, $replace);
                                $this->Mail->sendMail($zamowienie->email, $temat, $message, ['replyTo' => Configure::read('mail.domyslny_email')]);
                            }
                            die('OK');
                        } else {
                            \Cake\Log\Log::write('error', __('Dotpay return: Cant save a payment') . $platnosc->errors());
                            die('ERROR 1');
                        }
                    } else {
                        $platnosc->zamowienie->status = 'nieoplacone';
                        $this->Platnosc->save($platnosc);
                        \Cake\Log\Log::write('error', __('Dotpay return: Payment uncomplited'));
                        die('ERROR 2');
                    }
                } else {
                    \Cake\Log\Log::write('error', __('Dotpay return: No payment found'));
                    die('ERROR 3');
                }
            } else {
                \Cake\Log\Log::write('error', __('Dotpay return: difrent sign'));
                die('ERROR 4');
            }
        } else {
            \Cake\Log\Log::write('error', __('Dotpay return: request is not post'));
            die('ERROR 5');
        }
    }

    private function checkPayuPayment($rodzajPlatnosci, $token) {
        $statusArray = ['PENDING' => 'rozpoczeta',
            'WAITING_FOR_CONFIRMATION' => 'rozpoczeta',
            'COMPLETED' => 'wykonana',
            'CANCELED' => 'odrzucona',
            'REJECTED' => 'odrzucona'];
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            $this->loadModel('Platnosc');
            $pToken = explode('|', $rqData['order']['extOrderId']);
            $pToken = $pToken[0];
            $platnosc = $this->Platnosc->find('all', ['conditions' => ['Platnosc.token' => $pToken], 'contain' => ['Zamowienie']])->first();
            if (!empty($platnosc) && $platnosc->status != 'wykonana') {

                $platnosc->status = $statusArray[$rqData['order']['status']];
                $platnosc->platnosc_kwota = $rqData['order']['totalAmount'];
                if (!empty($rqData['properties'])) {
                    foreach ($rqData['properties'] as $propArr) {
                        if ($propArr['name'] == 'PAYMENT_ID') {
                            $platnosc->platnosc_numer = $propArr['value'];
                            break;
                        }
                    }
                }
                if ($platnosc->status == 'wykonana') {
                    $platnosc->zamowienie->status = 'oplacone';
                    if (empty($this->Zamowienie)) {
                        $this->loadModel('Zamowienie');
                    }
                    $this->Zamowienie->updateAll(['status' => 'oplacone'], ['id' => $platnosc->zamowienie->id, 'status' => 'zlozone']);
                    if ($this->Platnosc->save($platnosc)) {
                        $wyslijEmail = Configure::read('zamowienie.wyslij_potwierdzenie_wplaty');
                        if (!empty($wyslijEmail)) {
                            if (empty($this->Zamowienie)) {
                                $this->loadModel('Zamowienie');
                            }
                            $zamowienie = $this->Zamowienie->find('all', ['contain' => ['ZamowienieTowar', 'Platnosc']])->where(['Zamowienie.id' => $platnosc->zamowienie->id])->first();
                            $replace = [
                                'zamowienie-id' => $zamowienie->id,
                                'zamowienie-numer' => (!empty($zamowienie->numer_cdn) ? $zamowienie->numer_cdn : $zamowienie->id),
                                'zamowienie-imie' => $zamowienie->imie,
                                'zamowienie-nazwisko' => $zamowienie->nazwisko,
                                'zamowienie-adres-wysylki' => $zamowienie->adres_wysylki,
                                'zamowienie-adres-faktury' => $zamowienie->adres_faktury,
                                'zamowienie-email' => $zamowienie->email,
                                'zamowienie-nazwa' => $zamowienie->nazwa,
                                'zamowienie-nip' => $zamowienie->nip,
                                'zamowienie-telefon' => $zamowienie->telefon,
                                'zamowienie-wartosc-razem' => $this->Txt->cena($razem),
                                'zamowienie-wartosc-produktow' => $this->Txt->cena($zamowienie->wartosc_produktow),
                                'zamowienie-koszt-wysylki' => $this->Txt->cena($zamowienie->wysylka_koszt),
                                'zamowienie-platnosc-wysylka' => $zamowienie->platnosc_wysylka,
                                'zamowienie-waluta' => $zamowienie->waluta_symbol,
                                'zamowienie-uwagi' => nl2br($zamowienie->uwagi),
                                'zamowienie-status' => $this->Translation->get('statusy_zamowien.' . $zamowienie->status),
                                'zamowienie-data' => (!empty($zamowienie->data) ? $zamowienie->data->format('Y-m-d H:i') : ''),
                                'zamowienie-token' => $zamowienie->token,
                                'zamowienie-rabat' => $zamowienie->rabat_wartosc,
                                'zamowienie-opiekun' => $zamowienie->administrator_dane,
                                'zamowienie-opiekun-id' => $zamowienie->administrator_id,
                                'zamowienie-rabat-zestaw' => $zamowienie->rabat_zestaw,
                                'zamowienie-kod-rabatowy' => $zamowienie->kod_rabatowy,
                                'zamowienie-kod-rabatowy-typ' => $zamowienie->kod_rabatowy_typ,
                                'zamowienie-kod-rabatowy-wartosc' => $zamowienie->kod_rabatowy_wartosc,
                                'zamowienie-rabaty' => (!empty($rabatyInfo) ? join('', $rabatyInfo) : ''),
                                'zamowienie-bony' => (!empty($bonyInfo) ? $this->Txt->Html->tag('div', $this->Txt->Html->tag('div', $this->Translation->get('zamowienie.bonyInfo', [], true)) . join('', $bonyInfo)) : ''),
                                'adres-ip' => $this->request->clientIp()
                            ];
                            $szablon = $this->Szablon->findByNazwa('zamowienie_oplacone')->first();
                            if (!empty($szablon)) {
                                $message = $this->Replace->getTemplate($szablon->tresc, $replace, $zamowienie->zamowienie_towar);
                                $temat = $this->Replace->getTemplate($szablon->temat, $replace);
                                $this->Mail->sendMail($zamowienie->email, $temat, $message, ['replyTo' => Configure::read('mail.domyslny_email')]);
                            }
                        }
                        die('OK');
                    } else {
                        \Cake\Log\Log::write('error', __('PayU return: Cant save a payment') . $platnosc->errors());
                        die('ERROR 1');
                    }
                } else {
                    $platnosc->zamowienie->status = 'nieoplacone';
                    $this->Platnosc->save($platnosc);
                    \Cake\Log\Log::write('error', __('PayU return: Payment uncomplited'));
                    die('ERROR 2');
                }
            } else {
                if (!empty($platnosc)) {
                    die('SUCCESS');
                } else {
                    \Cake\Log\Log::write('error', __('PayU return: No payment found'));
                    die('ERROR 3');
                }
            }
        } else {
            \Cake\Log\Log::write('error', __('PayU return: request is not post'));
            die('ERROR 5');
        }
    }

    private function checkPrzelewy24Payment($rodzajPlatnosci) {
        $statusArray = ['new' => 'oczekuje',
            'processing' => 'rozpoczeta',
            'completed' => 'wykonana',
            'rejected' => 'odrzucona',
            'processing_realization_waiting' => 'rozpoczeta',
            'processing_realization' => 'rozpoczeta'];
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            /**
             * p24_merchant_id - ID Sprzedawcy
              p24_pos_id - ID Sklepu (domyślnie ID Sprzedawcy)
              p24_session_id - Unikalny identyfikator z systemu sprzedawcy
              p24_amount - Kwota transakcji wyrażona w WALUTA/100 (1.23 PLN = 123)
              p24_currency - PLN, EUR, GBP, CZK
              p24_order_id - numer transakcji nadany przez przelewy24
              p24_method - Metoda wybrana przez klienta
              p24_statement - Tytuł przelewu
              p24_sign - Suma kontrolna wyliczana wg opisu poniżej (patrz pkt. 7.1) z pól: p24_session_id, p24_order_id, p24_amount ,p24_currency i pola „Klucz CRC”

              Weryfikacja odpowiedzi pod adresem przez POST
             *  https://secure.przelewy24.pl/trnVerify
             * https://sandbox.przelewy24.pl/trnVerify - testowy
             * p24_merchant_id - ID Sprzedawcy
              p24_pos_id - ID Sklepu (domyślnie ID Sprzedawcy)
              p24_session_id - Unikalny identyfikator z systemu sprzedawcy
              p24_amount - Kwota transakcji wyrażona w WALUTA/100 (1.23 PLN = 123)
              p24_currency - PLN, EUR, GBP, CZK
              p24_order_id - numer transakcji nadany przez przelewy24
             * p24_sign - Suma kontrolna wyliczana wg opisu poniżej (patrz pkt. 8.1) z pól: p24_session_id, p24_order_id, p24_amount, p24_currency i pola „Klucz CRC”.
             *              */
            $sign = hash('md5', $rqData['p24_session_id'] . '|' . $rqData['p24_order_id'] . '|' . $rqData['p24_amount'] . '|' . $rqData['p24_currency'] . '|' . $rodzajPlatnosci->konfig1);
            $fromSign = $rqData['p24_sign'];

            if ($fromSign == $sign) {
                $this->loadModel('Platnosc');
                $platnosc = $this->Platnosc->find('all', ['conditions' => ['Platnosc.token' => $rqData['p24_session_id']], 'contain' => ['Zamowienie']])->first();
                if (!empty($platnosc)) {

                    $platnosc->status = 'rozpoczeta';
                    $platnosc->platnosc_kwota = $rqData['p24_amount'];
                    $platnosc->platnosc_numer = $rqData['p24_order_id'];
                    $platnosc->set('data_rozpoczecia', date('Y-m-d H:i:s'));
                    $this->Platnosc->save($platnosc);


                    $REQ = array();

                    $REQ[] = "p24_merchant_id=" . urlencode($rodzajPlatnosci->konfig2);
                    $REQ[] = "p24_pos_id=" . urlencode($rodzajPlatnosci->konfig3);
                    $REQ[] = "p24_session_id=" . urlencode($rqData['p24_session_id']);
                    $REQ[] = "p24_amount=" . urlencode($rqData['p24_amount']);
                    $REQ[] = "p24_currency=" . urlencode($rqData['p24_currency']);
                    $REQ[] = "p24_order_id=" . urlencode($rqData['p24_order_id']);
                    $REQ[] = "p24_sign=" . urlencode($sign);

                    $url = 'https://secure.przelewy24.pl/trnVerify';
                    if (!empty($rodzajPlatnosci->test)) {
                        $url = 'https://sandbox.przelewy24.pl/trnVerify';
                    }
                    $user_agent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)";
                    if ($ch = curl_init()) {

                        if (count($REQ)) {
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, join("&", $REQ));
                        }

                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                        curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                        if ($result = curl_exec($ch)) {
                            $INFO = curl_getinfo($ch);
                            curl_close($ch);

                            if ($INFO["http_code"] != 200) {

                                \Cake\Log\Log::write('error', __('Przelewy24 return: call:Page load error (' . $INFO["http_code"] . ')'));
                            } else {

                                $RES = array();
                                $X = explode("&", $result);

                                foreach ($X as $val) {

                                    $Y = explode("=", $val);
                                    $RES[trim($Y[0])] = urldecode(trim($Y[1]));
                                }
                                if (!isset($RES["error"])) {
                                    \Cake\Log\Log::write('error', __('Przelewy24 return: call:Unknown error'));
                                } else {
                                    $platnosc->status = 'wykonana';
                                }
                            }
                        } else {
                            curl_close($ch);
                            \Cake\Log\Log::write('error', __('Przelewy24 return: call:Curl exec error'));
                        }
                    } else {
                        \Cake\Log\Log::write('error', __('Przelewy24 return: call:Curl init error'));
                    }


                    if ($platnosc->status == 'wykonana') {
                        $platnosc->zamowienie->status = 'oplacone';
                        if ($this->Platnosc->save($platnosc)) {
                            $wyslijEmail = Configure::read('zamowienie.wyslij_potwierdzenie_wplaty');
                            if (!empty($wyslijEmail)) {
                                if (empty($this->Zamowienie)) {
                                    $this->loadModel('Zamowienie');
                                }
                                $zamowienie = $this->Zamowienie->find('all', ['contain' => ['ZamowienieTowar', 'Platnosc']])->where(['Zamowienie.id' => $platnosc->zamowienie->id])->first();
                                $replace = [
                                    'zamowienie-id' => $zamowienie->id,
                                    'zamowienie-numer' => (!empty($zamowienie->numer_cdn) ? $zamowienie->numer_cdn : $zamowienie->id),
                                    'zamowienie-imie' => $zamowienie->imie,
                                    'zamowienie-nazwisko' => $zamowienie->nazwisko,
                                    'zamowienie-adres-wysylki' => $zamowienie->adres_wysylki,
                                    'zamowienie-adres-faktury' => $zamowienie->adres_faktury,
                                    'zamowienie-email' => $zamowienie->email,
                                    'zamowienie-nazwa' => $zamowienie->nazwa,
                                    'zamowienie-nip' => $zamowienie->nip,
                                    'zamowienie-telefon' => $zamowienie->telefon,
                                    'zamowienie-wartosc-razem' => $this->Txt->cena($razem),
                                    'zamowienie-wartosc-produktow' => $this->Txt->cena($zamowienie->wartosc_produktow),
                                    'zamowienie-koszt-wysylki' => $this->Txt->cena($zamowienie->wysylka_koszt),
                                    'zamowienie-platnosc-wysylka' => $zamowienie->platnosc_wysylka,
                                    'zamowienie-waluta' => $zamowienie->waluta_symbol,
                                    'zamowienie-uwagi' => nl2br($zamowienie->uwagi),
                                    'zamowienie-status' => $this->Translation->get('statusy_zamowien.' . $zamowienie->status),
                                    'zamowienie-data' => (!empty($zamowienie->data) ? $zamowienie->data->format('Y-m-d H:i') : ''),
                                    'zamowienie-token' => $zamowienie->token,
                                    'zamowienie-rabat' => $zamowienie->rabat_wartosc,
                                    'zamowienie-opiekun' => $zamowienie->administrator_dane,
                                    'zamowienie-opiekun-id' => $zamowienie->administrator_id,
                                    'zamowienie-rabat-zestaw' => $zamowienie->rabat_zestaw,
                                    'zamowienie-kod-rabatowy' => $zamowienie->kod_rabatowy,
                                    'zamowienie-kod-rabatowy-typ' => $zamowienie->kod_rabatowy_typ,
                                    'zamowienie-kod-rabatowy-wartosc' => $zamowienie->kod_rabatowy_wartosc,
                                    'zamowienie-rabaty' => (!empty($rabatyInfo) ? join('', $rabatyInfo) : ''),
                                    'zamowienie-bony' => (!empty($bonyInfo) ? $this->Txt->Html->tag('div', $this->Txt->Html->tag('div', $this->Translation->get('zamowienie.bonyInfo', [], true)) . join('', $bonyInfo)) : ''),
                                    'adres-ip' => $this->request->clientIp()
                                ];
                                $szablon = $this->Szablon->findByNazwa('zamowienie_oplacone')->first();
                                $message = $this->Replace->getTemplate($szablon->tresc, $replace, $zamowienie->zamowienie_towar);
                                $temat = $this->Replace->getTemplate($szablon->temat, $replace);
                                $this->Mail->sendMail($zamowienie->email, $temat, $message, ['replyTo' => Configure::read('mail.domyslny_email')]);
                            }
                            die('OK');
                        } else {
                            \Cake\Log\Log::write('error', __('Przelewy24 return: Cant save a payment') . $platnosc->errors());
                            die('ERROR 1');
                        }
                    } else {
                        $platnosc->zamowienie->status = 'nieoplacone';
                        $this->Platnosc->save($platnosc);
                        \Cake\Log\Log::write('error', __('Przelewy24 return: Payment uncomplited'));
                        die('ERROR 2');
                    }
                } else {
                    \Cake\Log\Log::write('error', __('Przelewy24 return: No payment found'));
                    die('ERROR 3');
                }
            } else {
                \Cake\Log\Log::write('error', __('Przelewy24 return: difrent sign'));
                die('ERROR 4');
            }
        } else {
            \Cake\Log\Log::write('error', __('Przelewy24 return: request is not post'));
            die('ERROR 5');
        }
    }

    public function getPaczkomaty() {
        $this->viewBuilder()->setLayout('ajax');
    }

}
