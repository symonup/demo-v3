<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{
    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow();
        if($this->request->is('mobile')){
                $this->forceHideLeft=true;
            }
    }

    public function view($id=null){
        $this->loadModel('Strona');
        if(empty($id) && !$this->request->is('ajax')){
            return $this->redirect(['controller'=>'Home','action'=>'index']);
        }
        $strona=$this->Strona->find('all',['conditions'=>['Strona.id'=>$id,'Strona.ukryta'=>0]])->first();
        if(empty($strona) && !$this->request->is('ajax')){
            return $this->redirect(['controller'=>'Home','action'=>'index']);
        }
        if($strona->locale!=$this->active_jezyk->locale && !$this->request->is('ajax')){
            return $this->redirect(['controller'=>'Home','action'=>'index']);
        }
        if($strona->type=='type_popup' || $this->request->is('ajax')){
            $this->viewBuilder()->layout('ajax');
        }else{
            $this->loadModel('Menu');
            $menuPages = $this->Menu->find('all', ['conditions' => ['Menu.parent_id IS' => NULL,'Menu.typ'=>'page','(Menu.jezyk_id = '.$this->active_jezyk->id.' OR Menu.jezyk_id IS NULL)', 'OR' => ['Menu.ukryty' => 0, 'Menu.ukryty IS' => NULL]], 'order' => ['Menu.kolejnosc' => 'desc'], 'contain' => ['ChildMenu' => ['conditions' => ['ChildMenu.ukryty' => 0], 'sort' => ['ChildMenu.kolejnosc' => 'desc']]]]);
            $this->set('menuPages', $menuPages);
        }
        if(empty($this->Towar)){
        $this->loadModel('Towar');
        } 
        
        $nowosci = $this->Towar->find('all', ['limit' => 2, 'order' => ['rand()'], 'contain' => ['TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]], 'TowarCenaDefault' => ['Vat', 'Waluta', 'Jednostka']]])->where(['Towar.nowosc' => 1, 'Towar.ukryty' => 0]);
        
        $this->set('nowosci',$nowosci);
        $this->set('strona',$strona);
        $this->set('referer',$this->request->referer());
        $this->seoData['title']=(!empty($strona->seo_tytul)?$strona->seo_tytul:$strona->nazwa);
        if(!empty($strona->seo_opis)){
            $this->seoData['desctiption']=$strona->seo_opis;
        }
        if(!empty($strona->seo_slowa)){
            $this->seoData['keywords']=$strona->seo_slowa;
        }
                $crumbs = [$strona->nazwa => null];
                $this->set('crumbs', $crumbs);
                $this->forceHideLeft=true;
    }
    public function preview(){
        if(!$this->request->is('post')){
            return $this->redirect(['controller'=>'Home','action'=>'index']);
        }
        $this->seoData['title']='Podgląd strony';
        $this->set('referer',$this->request->referer());
        $this->set('tresc',$this->request->getData('html'));
    }

    /**
     * Displays a view
     *
     * @param string ...$path Path segments.
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function display(...$path)
    {
        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        if (in_array('..', $path, true) || in_array('.', $path, true)) {
            throw new ForbiddenException();
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        $this->set(compact('page', 'subpage'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }
}
