<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Uzytkownik Controller
 *
 * @property \App\Model\Table\UzytkownikTable $Uzytkownik
 *
 * @method \App\Model\Entity\Uzytkownik[] paginate($object = null, array $settings = [])
 */
class UzytkownikController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->viewBuilder()->layout('user');
        $this->footerOsiagniecia = false;
        if($this->request->is('mobile')){
                $this->forceHideLeft=true;
            }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $cfgArr = [
            'contain' => ['UzytkownikAdresWysylka'=>['sort'=>['UzytkownikAdresWysylka.domyslny_wysylka'=>'desc']], 'UzytkownikFaktura'],
            'conditions' => ['Uzytkownik.id' => $this->Auth->user('id')]
        ];
        $uzytkownik = $this->Uzytkownik->find('all', $cfgArr)->first();
        $this->loadModel('Zamowienie');
        $this->loadModel('NewsletterAdres');
        $lastOrders = $this->Zamowienie->find('all', ['limit' => 3])->where(['Zamowienie.uzytkownik_id' => $this->userId]);
        $newsletter = $this->NewsletterAdres->find('all')->where(['NewsletterAdres.email' => $uzytkownik->email, 'NewsletterAdres.potwierdzony' => 1])->first();
        if (!empty($newsletter)) {
            $uzytkownik->set('newsletter', 1);
        }
        $this->set(compact('uzytkownik', 'lastOrders'));
        $this->set('_serialize', ['uzytkownik']);
    }

    public function addressList() {
        $this->loadModel('UzytkownikAdres');
        $adresy = $this->UzytkownikAdres->find('all', ['conditions' => ['UzytkownikAdres.uzytkownik_id' => $this->userId, 'UzytkownikAdres.typ' => 1]]);
        $this->set('adresy', $adresy);
    }

    public function addAddress() {
        $this->loadModel('UzytkownikAdres');
        $uzytkownikAdres = $this->UzytkownikAdres->newEntity();
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
            $rqData['uzytkownik_id'] = $this->userId;
            $invoiceData = [];
            if (!empty($rqData['as_invoice'])) {
                $invoiceData = $rqData;
                $invoiceData['typ'] = 2;
            }
            $uzytkownikAdres = $this->UzytkownikAdres->patchEntity($uzytkownikAdres, $rqData, ['translations' => $this->UzytkownikAdres->hasBehavior('Translate')]);
            if ($this->UzytkownikAdres->save($uzytkownikAdres)) {
                if (!empty($invoiceData)) {
                    $checkInvoice = $this->UzytkownikAdres->find('all', ['conditions' => ['UzytkownikAdres.uzytkownik_id' => $this->Auth->user('id'), 'UzytkownikAdres.typ' => 2]])->first();
                    if (empty($checkInvoice)) {
                        $checkInvoice = $this->UzytkownikAdres->newEntity();
                    }
                    $checkInvoice = $this->UzytkownikAdres->patchEntity($checkInvoice, $invoiceData);
                    $this->UzytkownikAdres->save($checkInvoice);
                }
                if (!empty($uzytkownikAdres->domyslny_wysylka)) {
                    $this->UzytkownikAdres->updateAll(['domyslny_wysylka' => 0], ['UzytkownikAdres.id !=' => $uzytkownikAdres->id]);
                }
                $this->Flash->success($this->Translation->get('success.dataHasBeenSaved'));
                return $this->redirect(['action' => 'addressList']);
            }
            $this->Flash->error($this->Translation->get('errors.dataCouldNotBeSaved'));
        }
        $this->set(compact('uzytkownikAdres'));
        $this->set('_serialize', ['uzytkownikAdres']);
    }

    public function editAddress($id = null) {
        $this->loadModel('UzytkownikAdres');
        $uzytkownikAdres = $this->UzytkownikAdres->find('all', ['conditions' => ['UzytkownikAdres.id' => $id, 'UzytkownikAdres.uzytkownik_id' => $this->userId]])->first();
        if (empty($uzytkownikAdres)) {
            $this->Flash->error($this->Translation->get('errors.invalidAddressIdentifier'));
            return $this->redirect(['action' => 'addressList']);
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData = $this->request->getData();
            $invoiceData = [];
            if (!empty($rqData['as_invoice'])) {
                $invoiceData = $rqData;
                $invoiceData['typ'] = 2;
            }
            $uzytkownikAdres = $this->UzytkownikAdres->patchEntity($uzytkownikAdres, $rqData, ['translations' => $this->UzytkownikAdres->hasBehavior('Translate')]);
            if ($this->UzytkownikAdres->save($uzytkownikAdres)) {
                if (!empty($invoiceData)) {
                    $checkInvoice = $this->UzytkownikAdres->find('all', ['conditions' => ['UzytkownikAdres.uzytkownik_id' => $this->Auth->user('id'), 'UzytkownikAdres.typ' => 2]])->first();
                    if (empty($checkInvoice)) {
                        $checkInvoice = $this->UzytkownikAdres->newEntity();
                    }
                    $checkInvoice = $this->UzytkownikAdres->patchEntity($checkInvoice, $invoiceData);
                    $this->UzytkownikAdres->save($checkInvoice);
                }
                if (!empty($uzytkownikAdres->domyslny_wysylka)) {
                    $this->UzytkownikAdres->updateAll(['domyslny_wysylka' => 0], ['UzytkownikAdres.id !=' => $uzytkownikAdres->id]);
                }
                $this->Flash->success($this->Translation->get('success.dataHasBeenSaved'));
                return $this->redirect(['action' => 'addressList']);
            }
            $this->Flash->error($this->Translation->get('errors.dataCouldNotBeSaved'));
        }
        $this->set(compact('uzytkownikAdres'));
        $this->set('_serialize', ['uzytkownikAdres']);
    }

    public function deleteAddress($id = null) {
        $this->loadModel('UzytkownikAdres');
        $uzytkownikAdres = $this->UzytkownikAdres->find('all', ['conditions' => ['UzytkownikAdres.id' => $id, 'UzytkownikAdres.uzytkownik_id' => $this->userId]])->first();
        if (empty($uzytkownikAdres)) {
            $this->Flash->error($this->Translation->get('errors.invalidAddressIdentifier'));
            return $this->redirect(['action' => 'addressList']);
        }
        if ($this->UzytkownikAdres->delete($uzytkownikAdres)) {
            $this->Flash->success($this->Translation->get('success.dataHasBeenDeleted'));
        } else {
            $this->Flash->error($this->Translation->get('success.dataCouldNotBeDeleted'));
        }
        return $this->redirect(['action' => 'addressList']);
    }

    public function invoiceAddress() {
        $this->loadModel('UzytkownikAdres');
        $new = false;
        $uzytkownikAdres = $this->UzytkownikAdres->find('all', ['conditions' => ['UzytkownikAdres.typ' => 2, 'UzytkownikAdres.uzytkownik_id' => $this->userId]])->first();
        if (empty($uzytkownikAdres)) {
            $uzytkownikAdres = $this->UzytkownikAdres->newEntity();
            $new = true;
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData = $this->request->getData();
            if (!empty($rqData['redirect'])) {
                $redirect = $rqData['redirect'];
            } else {
                $redirect = ['action' => 'info'];
            }
            unset($rqData['redirect']);
            if ($new) {
                $rqData['uzytkownik_id'] = $this->userId;
            }
            $newDelivery = [];
            if (!empty($rqData['as_new_delivery'])) {
                $newDelivery = $rqData;
                $newDelivery['uzytkownik_id'] = $this->userId;
                $newDelivery['typ'] = 1;
            }
            $uzytkownikAdres = $this->UzytkownikAdres->patchEntity($uzytkownikAdres, $rqData, ['translations' => $this->UzytkownikAdres->hasBehavior('Translate')]);
            if ($this->UzytkownikAdres->save($uzytkownikAdres)) {
                if (!empty($newDelivery)) {
                    $newDvAddress = $this->UzytkownikAdres->newEntity($newDelivery);
                    $this->UzytkownikAdres->save($newDvAddress);
                }
                $this->Flash->success($this->Translation->get('success.dataHasBeenSaved'));
                return $this->redirect($redirect);
            }
            $this->Flash->error($this->Translation->get('errors.dataCouldNotBeSaved'));
        }
        $redirect = $this->request->query('redirect');
        if (!empty($redirect)) {
            if ($redirect == 'toCart') {
                $redirect = \Cake\Routing\Router::url(['controller' => 'Cart', 'action' => 'index']);
            }
        }
        $this->set(compact('uzytkownikAdres', 'redirect'));
        $this->set('_serialize', ['uzytkownikAdres']);
    }

    public function carts() {
        if (empty($this->UzytkownikKoszyk)) {
            $this->loadModel('UzytkownikKoszyk');
        }
        $carts = $this->UzytkownikKoszyk->find('all', ['conditions' => ['UzytkownikKoszyk.uzytkownik_id' => $this->userId], 'order' => ['UzytkownikKoszyk.create_date' => 'desc']]);
        $allCarts = [];
        if ($carts->count() > 0) {
            foreach ($carts as $savedCart) {
                $cartItems = [];
                if (!empty($savedCart['items'])) {
                    $savedItems = explode(',', $savedCart['items']);
                    $cartItemsById = [];
                    foreach ($savedItems as $savedItem) {
                        $item = explode(':', $savedItem);
                        $warianty[$item[0]] = $item[1];
                        $itemTmp = explode('_', $item[0]);
                        $itemId = $itemTmp[0];
                        $cartItemsById[$itemId][$item[0]] = $item[1];
                    }
                    if (!empty($cartItemsById)) {
                        $itemsToAdd = [];
                        foreach ($cartItemsById as $itemId => $warianty) {
                            $wariantyIds = [];
                            if (!empty($warianty)) {
                                foreach ($warianty as $key => $ilosc) {
                                    if (strpos($key, $itemId . '_') !== false) {
                                        $wariantyIds[] = str_replace($itemId . '_', '', $key);
                                    }
                                }
                            }
                            if (empty($wariantyIds)) {
                                $wariantyIds = [null];
                            }

                            if (empty($this->Towar)) {
                                $this->loadModel('Towar');
                            }
                            $item = $this->Towar->getWariantsToCart($this->Auth->user(), $itemId, $wariantyIds);
                            $itemsToAdd = array_merge($itemsToAdd, $this->UzytkownikKoszyk->prepareCart($item, $warianty, $this->userRabat));
                            if (!empty($itemsToAdd)) {
                                foreach ($itemsToAdd as $addKey => $addItem) {
                                    $cartItems['items'][$addKey] = $addItem;
                                }
                            }
                            $cartItems = $this->cartPrzelicz($cartItems, true);
                        }
                    }
                }
                $allCarts[$savedCart['id']] = [
                    'id' => $savedCart['id'],
                    'created' => $savedCart['create_date']->format('Y-m-d H:i:s'),
                    'modified' => $savedCart['last_update']->format('Y-m-d H:i:s'),
                    'items' => $cartItems,
                    'active' => $savedCart['active']
                ];
            }
        }
        $this->set(compact('allCarts'));
    }

    public function orders() {
        if (empty($this->Zamowienie)) {
            $this->loadModel('Zamowienie');
        }
        $this->paginate = [
            'conditions' => ['Zamowienie.uzytkownik_id' => $this->userId],
            'order' => ['Zamowienie.data' => 'desc'],
            'contain' => ['Waluta'],
            'limit' => 20
        ];
        $zamowienia = $this->paginate($this->Zamowienie);


        $cfgArr = [
            'contain' => ['UzytkownikWysylka', 'UzytkownikFaktura'],
            'conditions' => ['Uzytkownik.id' => $this->Auth->user('id')]
        ];
        $this->loadModel('Uzytkownik');
        $uzytkownik = $this->Uzytkownik->find('all', $cfgArr)->first();
        $this->loadModel('NewsletterAdres');
        $newsletter = $this->NewsletterAdres->find('all')->where(['NewsletterAdres.email' => $uzytkownik->email, 'NewsletterAdres.potwierdzony' => 1])->first();
        if (!empty($newsletter)) {
            $uzytkownik->set('newsletter', 1);
        }



        $this->set(compact('zamowienia', 'uzytkownik'));
    }

    public function orderDetalis($id = null) {
        if (empty($this->Zamowienie)) {
            $this->loadModel('Zamowienie');
        }
        $zamowienie = $this->Zamowienie->find('all', ['conditions' => ['Zamowienie.uzytkownik_id' => $this->userId, 'Zamowienie.token' => $id], 'contain' => ['Waluta', 'ZamowienieTowar']])->first();
        if (empty($zamowienie)) {
            $this->Flash->error($this->Translation->get('errors.invalidOrderIdentifier'));
            return $this->redirect(['action' => 'orders']);
        }
        $this->set(compact('zamowienie'));
    }

    public function info() {
        $cfgArr = [
            'conditions' => ['Uzytkownik.id' => $this->userId]
        ];
        $uzytkownik = $this->Uzytkownik->find('all', $cfgArr)->first();
        $this->loadModel('NewsletterAdres');
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rqData = $this->request->getData();
            unset($rqData['email']);
            $uzytkownik = $this->Uzytkownik->patchEntity($uzytkownik, $rqData);
            if ($this->Uzytkownik->save($uzytkownik)) {
                if (!empty($rqData['newsletter'])) {
                    $isNewsletter = $this->NewsletterAdres->find('all')->where(['NewsletterAdres.email' => $uzytkownik->email])->first();

                    if (empty($isNewsletter)) {
                        $newsletterData = ['email' => $uzytkownik->email];
                        $newsletterData['data'] = date('Y-m-d H:i:s');
                        $newsletterData['token'] = md5(uniqid('', true));
                        $newsletterData['potwierdzony'] = 1;
                        $newsletterData['locale'] = \Cake\I18n\I18n::locale();
                        $isNewsletter = $this->NewsletterAdres->newEntity($newsletterData);
                        $this->NewsletterAdres->save($isNewsletter);
                    } elseif (empty($isNewsletter->potwierdzony)) {
                        $isNewsletter->potwierdzony = 1;
                        $this->NewsletterAdres->save($isNewsletter);
                    }
                } else {
                    $this->NewsletterAdres->deleteAll(['NewsletterAdres.email' => $uzytkownik->email]);
                }
                $this->Flash->success($this->Translation->get('success.dataHasBeenSaved'));
                return $this->redirect(['action' => 'info']);
            } else {
                $this->Flash->error($this->Translation->get('errors.dataCouldNotBeSaved'));
            }
        }
        $newsletter = $this->NewsletterAdres->find('all')->where(['NewsletterAdres.email' => $uzytkownik->email, 'NewsletterAdres.potwierdzony' => 1])->first();
        if (!empty($newsletter)) {
            $uzytkownik->set('newsletter', 1);
        }
        $this->set(compact('uzytkownik'));
        $this->set('_serialize', ['uzytkownik']);
    }

    public function activateCart($cartId = null) {
        if (empty($this->UzytkownikKoszyk)) {
            $this->loadModel('UzytkownikKoszyk');
        }
        $koszyk = $this->UzytkownikKoszyk->find('all', ['conditions' => ['UzytkownikKoszyk.id' => $cartId, 'UzytkownikKoszyk.uzytkownik_id' => $this->userId]])->first();
        if (!empty($koszyk)) {
            $koszyk->active = 1;
            if ($this->UzytkownikKoszyk->save($koszyk)) {
                $this->UzytkownikKoszyk->updateAll(['active' => 0], ['uzytkownik_id' => $this->userId, 'id !=' => $koszyk->id, 'active' => 1]);
                $this->session->delete('cartItems');
                return $this->redirect(['controller' => 'Cart', 'action' => 'index']);
            } else {
                $this->Flash->error($this->Translation->get('errors.activateCartError'));
                return $this->redirect(['action' => 'carts']);
            }
        } else {
            $this->Flash->error($this->Translation->get('errors.invalidCartIdentifier'));
            return $this->redirect(['action' => 'carts']);
        }
    }

    public function deleteCart($cartId = null) {
        if (empty($this->UzytkownikKoszyk)) {
            $this->loadModel('UzytkownikKoszyk');
        }
        $koszyk = $this->UzytkownikKoszyk->find('all', ['conditions' => ['UzytkownikKoszyk.id' => $cartId, 'UzytkownikKoszyk.uzytkownik_id' => $this->userId]])->first();
        if (!empty($koszyk)) {
            $isActive = $koszyk->active;
            if ($this->UzytkownikKoszyk->delete($koszyk)) {
                if (!empty($isActive)) {
                    $this->session->delete('cartItems');
                }
                $this->Flash->success($this->Translation->get('success.dataHasBeenDeleted'));
                return $this->redirect(['action' => 'carts']);
            } else {
                $this->Flash->error($this->Translation->get('errors.dataCouldNotBeDeleted'));
                return $this->redirect(['action' => 'carts']);
            }
        } else {
            $this->Flash->error($this->Translation->get('errors.invalidCartIdentifier'));
            return $this->redirect(['action' => 'carts']);
        }
    }
    
    public function kartaKod(){
        $this->autoRender=false;
        $returnData=[];
        if($this->request->isAll(['post','ajax'])){
            $kod=$this->request->getData('kod');
                    $pin=$this->request->getData('pin');
                    $this->loadModel('KartaPodarunkowaKod');
            if(!empty($kod) && !empty($pin)){
                $karta=$this->KartaPodarunkowaKod->find()->where(['KartaPodarunkowaKod.kod'=>$kod,'KartaPodarunkowaKod.pin'=>$pin,'KartaPodarunkowaKod.aktywny'=>1,'KartaPodarunkowaKod.uzyty'=>0,'(KartaPodarunkowaKod.data_waznosci IS NULL OR KartaPodarunkowaKod.data_waznosci <= \''.date('Y-m-d').'\')'])->first();
                if(!empty($karta)){
                    $uzytkownik=$this->Uzytkownik->find()->where(['Uzytkownik.id'=>$this->Auth->user('id')])->first();
                    $actZlotowki=$uzytkownik->bonusy_suma;
                    if($actZlotowki<0){
                        $actZlotowki=0;
                    }
                    $actZlotowki+=$karta->wartosc;
                    $uzytkownik->set('bonusy_suma',$actZlotowki);
                    if($this->Uzytkownik->save($uzytkownik)){
                        $karta->set('uzyty',1);
                        $karta->set('data_uzycia',date('Y-m-d H:i:s'));
                        $karta->set('uzytkownik_id',$uzytkownik->id);
                        $this->session->write('Auth.User.bonusy_suma', $uzytkownik->bonusy_suma);
                        $this->KartaPodarunkowaKod->save($karta);
                        $returnData=['status'=>'success','message'=>$this->Translation->get('success.kartaZostalaUzyta'),'bonusy'=>$uzytkownik->bonusy_suma];
                    }else{
                        $returnData=['status'=>'error','message'=>$this->Translation->get('errors.bladZapisuDanych')];
                    }
                }else{
                    $returnData=['status'=>'error','message'=>$this->Translation->get('errors.blednyKodLubKartaNieaktywna')];
                }
            }else{
                $returnData=['status'=>'error','message'=>$this->Translation->get('errors.invalidRequest')];
            }
        }else{
            $returnData=['status'=>'error','message'=>$this->Translation->get('errors.invalidRequest')];
        }
        $this->response->type('ajax');
        $this->response->body(json_encode($returnData));
        return $this->response;
    }
    public function integracja() {
        if(empty($this->userId) || $this->Auth->user('typ')!='b2b'){
            return $this->redirect(['action'=>'index']);
        }
        $uzytkownik = $this->Uzytkownik->find()->where(['Uzytkownik.id'=>$this->Auth->user('id')])->first();
        $this->set('uzytkownik',$uzytkownik);
    }
    public function faktury() {
        if(empty($this->userId) || $this->Auth->user('typ')!='b2b'){
            return $this->redirect(['action'=>'index']);
        }
        $this->loadModel('Faktura');
        $uzytkownik = $this->Uzytkownik->find()->where(['Uzytkownik.id'=>$this->Auth->user('id')])->first();
        $faktury = $this->Faktura->find('all')->where(['Faktura.uzytkownik_id'=>$uzytkownik->id]);
        $this->set('uzytkownik',$uzytkownik);
        $this->set('faktury',$faktury);
    }

}
