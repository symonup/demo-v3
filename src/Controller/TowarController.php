<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * Towar Controller
 *
 * @property \App\Model\Table\TowarTable $Towar
 *
 * @method \App\Model\Entity\Towar[] paginate($object = null, array $settings = [])
 */
class TowarController extends AppController {

    private $towarCenaDefaultParams = [];

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow();
        $this->towarCenaDefaultParams = ['Vat', 'Waluta', 'Jednostka', 'conditions' => ['TowarCenaDefault.waluta_id' => $this->active_jezyk->waluta_id]];
    }

    public $orders = [
        'id_asc' => ['Towar.id' => 'asc'],
        'id_desc' => ['Towar.id' => 'desc'],
        'kod_asc' => ['Towar.kod' => 'asc'],
        'kod_desc' => ['Towar.kod' => 'desc'],
        'nazwa_asc' => ['Towar.nazwa' => 'asc'],
        'nazwa_desc' => ['Towar.nazwa' => 'desc'],
        'cena_asc' => ['price' => 'asc'],
        'cena_desc' => ['price' => 'desc']
    ];

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index($kategoriaId = null, $preorderId = null) {
        if (empty($this->AtrybutTyp)) {
            $this->loadModel('AtrybutTyp');
        }
        $kategoria = null;
        $this->footerOsiagniecia = false;
        $gatunekId = $this->request->getParam('type');
        $queryS = $this->request->query('search');
        if (!empty($kategoriaId) && is_numeric($kategoriaId)) {
            $kategoria = $this->Towar->Kategoria->find('all', ['contain' => ['ParentKategoria' => ['ParentKategoria']]])->where(['Kategoria.id' => $kategoriaId])->first();
            if (!empty($kategoria)) {
                $crumbs = $this->Towar->Kategoria->getCrumb($kategoria->id);
                $crumbs = array_reverse($crumbs);
                $crumbsKey = array_keys($crumbs);
                if (empty($queryS))
                    $crumbs[$crumbsKey[(count($crumbsKey) - 1)]] = null;
                if (!empty($queryS)) {
                    $crumbs[$this->Translation->get('lista_szukaj.WynikiWyszukiwania')] = null;
                    $this->set('fraza', $queryS);
                }
                $this->set('crumbs', $crumbs);

                $this->set('kategoria', $kategoria);
                $allKatIds = $this->Towar->Kategoria->getAllIds($kategoriaId);

                $this->session->write('lastCategory', $kategoria->id);
            }
        }
        if (!empty($kategoria) && !empty($kategoria->ukryta)) {
            $this->Flash->error('errors.WybranaKategoriaJestNiedostepna');
            return $this->redirect(['controller' => 'Home', 'action' => 'index']);
        } elseif (!empty($kategoria) && !empty($kategoria->wyswietl_miniatury)) {
            $podkategorie = $this->Towar->Kategoria->find('all', ['contain' => ['ChildKategoria']])->where(['Kategoria.parent_id' => $kategoria->id, 'Kategoria.ukryta' => 0])->order(['Kategoria.kolejnosc' => 'desc', 'Kategoria.id' => 'asc']);

            $kategorieAll = $this->allCats;
            $this->set(compact('podkategorie', 'polecane', 'kategorieAll'));
            $this->render('podkategorie');
        } else {
            $limit = $this->session->read('towarListaLimit');
            $sortKey = $this->session->read('towarListaOrder');
            if ($sortKey == 'id_asc' || $sortKey == 'id_desc') {
                $sortKey = 'kod_asc';
            }
            if (empty($limit)) {
                $limit = 32;
            }
            if (empty($sortKey)) {
                $sortKey = 'kod_asc';
            }
            if ($this->request->is('post')) {
                $querySearch = $this->request->query('search');
                $filters = $this->request->getData();
                $producers = $this->request->getQuery('producent');
                if (!empty($producers)) {
                    $filters['producent'] = $producers;
                }
                $query = [];
                foreach ($filters as $filterKey => $filterData) {
                    if ($filterKey == 'sort') {
                        $sortKey = $filterData;
                        continue;
                    }
                    if ($filterKey == 'kategoria') {
                        if (!empty($filterData['_ids'])) {
                            $query['cat'] = join(',', $filterData['_ids']);
                        }
                        continue;
                    } elseif ($filterKey == 'okazje') {
                        if (!empty($filterData)) {
                            $okazjeSelected = [];
                            foreach ($filterData as $okazjaId) {
                                if (!empty($okazjaId)) {
                                    $okazjeSelected[] = $okazjaId;
                                }
                            }
                            if (!empty($okazjeSelected)) {
                                $query['okazje'] = join(',', $okazjeSelected);
                            }
                        }
                        continue;
                    } elseif ($filterKey == 'status') {
                        if (!empty($filterData)) {
                            foreach ($filterData as $fKey => $fValue) {
                                if (empty($fValue)) {
                                    unset($filterData[$fKey]);
                                }
                            }
                            if (!empty($filterData)) {
                                $query['status'] = join(',', $filterData);
                            }
                        }
                        continue;
                    } else if ($filterKey == 'attr') {
                        if (!empty($filterData)) {
                            $attrChecked = [];
                            foreach ($filterData as $attrTypId => $attrIds) {
                                foreach ($attrIds as $attrId) {
                                    if (!empty((int) $attrId)) {
                                        $attrChecked[$attrTypId][] = $attrId;
                                    }
                                }
                            }
                            if (!empty($attrChecked)) {
                                $query[$filterKey] = $attrChecked;
                            }
                        }
                        continue;
                    }
                    $query[$filterKey] = $filterData;
                }
                if (!empty($querySearch)) {
                    $query['search'] = $querySearch;
                }
                $this->session->write('towarListaOrder', $sortKey);
                return $this->redirect(array_merge(['action' => 'index'], $this->request->params['pass'] + $query));
            }
            $conditions = [
                'Towar.ukryty' => 0
            ];
            $conditions2 = [
                'Towar.ukryty' => 0
            ];
            $this->loadModel('TowarCena');
            $getCountPrices = $this->TowarCena->find('all')->where(['TowarCena.uzytkownik_id' => $this->userId])->count();

            $listLabel = '';
            if (!empty($kategoriaId) && !is_numeric($kategoriaId) || (empty($kategoriaId) && empty($queryS))) {
                switch ($kategoriaId) {
                    case 'promocje': {
                            $conditions['Towar.promocja'] = 1;
                            $conditions2['Towar.promocja'] = 1;
                            $listLabel = $kategoriaId;
                        }
                        break;
                    case 'nowosci': {
                            $conditions['Towar.nowosc'] = 1;
                            $conditions2['Towar.nowosc'] = 1;
                            $listLabel = $kategoriaId;
                        }break;
                    case 'polecane': {
                            $conditions['Towar.polecany'] = 1;
                            $conditions2['Towar.polecany'] = 1;
                            $listLabel = $kategoriaId;
                        } break;
                    case 'wyroznione': {
                            $conditions['Towar.wyrozniony'] = 1;
                            $conditions2['Towar.wyrozniony'] = 1;
                            $listLabel = $kategoriaId;
                        } break;
                    case 'wyprzedaz': {
                            $conditions['Towar.wyprzedaz'] = 1;
                            $conditions2['Towar.wyprzedaz'] = 1;
                            $listLabel = $kategoriaId;
                        }
                        break;
                    case 'gratisy': {
                            $conditions['Towar.gratis'] = 1;
                            $conditions['Towar.gratis_wartosc >'] = 0;
                            $conditions2['Towar.gratis'] = 1;
                            $conditions2['Towar.gratis_wartosc >'] = 0;
                            $listLabel = $kategoriaId;
                        }
                        break;
                    case 'ulubione': {

                            $schowek = $this->session->read('schowek');

                            if (empty($schowek) && !empty($this->userId)) {
                                $schowek = $this->getSchowek();
                            }
                            if (empty($schowek)) {
                                $schowek = [null];
                            }
                            $conditions['Towar.id IN'] = $schowek;
                            $conditions2['Towar.id IN'] = $schowek;
                            $listLabel = $kategoriaId;
                        }
                        break;
                    case 'prezenty': {
                            $conditions['Towar.prezent'] = 1;
                            $conditions2['Towar.prezent'] = 1;
                            $listLabel = $kategoriaId;
                        }
                        break;

                    default:
                        $listLabel = 'wszystkieProdukty';
                        break;
                }
                $crumbs = [$this->Translation->get('breadcrumb.' . $listLabel) => null];

                $seo = Configure::read('seo_' . $listLabel);
                if (!empty($seo) && !empty($seo['title'])) {
                    $this->seoData['title'] = $seo['title'];
                }
                if (!empty($seo) && !empty($seo['keywords'])) {
                    $this->seoData['keywords'] = $seo['keywords'];
                }
                if (!empty($seo) && !empty($seo['description'])) {
                    $this->seoData['description'] = $seo['description'];
                }
                $this->set('listLabel', $listLabel);
                $this->set('crumbs', $crumbs);
            }
            $filters = [];
            $filterQuery = $this->request->query();
            if (!empty($kategoriaId) && is_numeric($kategoriaId)) {
                $filterQuery['cat'] = join(',', $this->Towar->Kategoria->getAllIds($kategoriaId));
            }
            if ($kategoriaId == 'preorders') {
                if (!empty($preorderId) && is_numeric($preorderId)) {
                    $filters['preorders'] = $preorderId;
                } else {
                    $filters['preorders'] = 'all';
                }
            }
            $attrTowarIds = [];
            $cenaTowarIds = [];
            $maxPriceCondAvil = [];
            $attrJoins = [0 => [
                    'table' => 'towar_atrybut',
                    'alias' => 'TowarAtrybut',
                    'type' => 'LEFT',
                    'conditions' => 'Towar.id = TowarAtrybut.towar_id',
            ]];
            if (!empty($filterQuery)) {
                foreach ($filterQuery as $queryKey => $queryData) {
                    if ($queryKey == 'cat' && $queryData != '') {
                        $katIds = explode(',', $queryData);
                        if (count($katIds) == 1) {
                            $katIds = $this->Towar->Kategoria->getAllIds($katIds[0]);
                        }
                        if (!empty($katIds)) {
                            $tIds = $this->Towar->TowarKategoria->find('list', ['keyField' => 'towar_id', 'valueField' => 'towar_id', 'group' => ['towar_id'], 'conditions' => ['kategoria_id IN' => $katIds]])->toArray();
                            if (empty($tIds)) {
                                $tIds = [null];
                            }
                            if (!empty($conditions['Towar.id IN'])) {
                                $conditions['Towar.id IN'] = array_intersect($conditions['Towar.id IN'], $tIds);
                                $conditions2['Towar.id IN'] = array_intersect($conditions2['Towar.id IN'], $tIds);
                            } else {
                                $conditions['Towar.id IN'] = $tIds;
                                $conditions2['Towar.id IN'] = $tIds;
                            }
                            if (!empty($attrTowarIds)) {
                                $attrTowarIds = array_intersect($attrTowarIds, $tIds);
                            } else {
                                $attrTowarIds = $tIds;
                            }
                            if (!empty($cenaTowarIds)) {
                                $cenaTowarIds = array_intersect($cenaTowarIds, $tIds);
                            } else {
                                $cenaTowarIds = $tIds;
                            }
                        }
                        foreach ($katIds as $katId) {
                            $filters['kategoria']['_ids'][$katId] = $katId;
                        }
                    } else if ($queryKey == 'okazje' && $queryData != '') {
                        $okazjeIds = explode(',', $queryData);
                        $attrJoins['okazje'] = [
                                        'table' => 'okazje_towar',
                                        'alias' => 'OkazjeTowar',
                                        'type' => 'INNER',
                                        'conditions' => 'Towar.id = OkazjeTowar.towar_id'
                                    ];
                        $conditions['OkazjeTowar.towar_id IN'] = $okazjeIds;
                        $filters['okazje']=$okazjeIds;
                    } else if ($queryKey == 'type') {
                        $typeIds = explode(',', $queryData);
                        if (!empty($typeIds)) {
                            $tIds = $this->Towar->TowarGatunek->find('list', ['keyField' => 'towar_id', 'valueField' => 'towar_id', 'group' => ['towar_id'], 'conditions' => ['gatunek_id IN' => $typeIds]])->toArray();
                            if (empty($tIds)) {
                                $tIds = [null];
                            }
                            if (!empty($conditions['Towar.id IN'])) {
                                $conditions['Towar.id IN'] = array_intersect($conditions['Towar.id IN'], $tIds);
                                $conditions2['Towar.id IN'] = array_intersect($conditions2['Towar.id IN'], $tIds);
                            } else {
                                $conditions['Towar.id IN'] = $tIds;
                                $conditions2['Towar.id IN'] = $tIds;
                            }
                            if (!empty($attrTowarIds)) {
                                $attrTowarIds = array_intersect($attrTowarIds, $tIds);
                            } else {
                                $attrTowarIds = $tIds;
                            }
                            if (!empty($cenaTowarIds)) {
                                $cenaTowarIds = array_intersect($cenaTowarIds, $tIds);
                            } else {
                                $cenaTowarIds = $tIds;
                            }
                        }
                        foreach ($typeIds as $typeId) {
                            $filters['gatunek']['_ids'][$typeId] = $typeId;
                        }
                    } else if ($queryKey == 'status') {
                        $statusy = explode(',', $queryData);
                        if (!empty($statusy)) {
                            $statusOR = [];
                            foreach ($statusy as $itemStatus) {
                                if ($itemStatus == 'dostepny') {
                                    $conditions['Towar.ilosc >'] = 0;
                                    $conditions2['Towar.ilosc >'] = 0;
                                } elseif ($itemStatus == 'preorder') {
                                    $conditions['Towar.preorder'] = 1;
                                    $conditions2['Towar.preorder'] = 1;

                                    $conditions['Towar.data_premiery >'] = date('Y-m-d');
                                    $conditions2['Towar.data_premiery >'] = date('Y-m-d');
                                } else {
                                    $statusOR[] = 'Towar.' . $itemStatus . ' = 1';
                                }
                                $filters['status'][$itemStatus] = $itemStatus;
                            }
                            if (!empty($statusOR)) {

                                $conditions[] = '(' . join(' OR ', $statusOR) . ')';
                                $conditions2[] = '(' . join(' OR ', $statusOR) . ')';
                            }
                        }
                    } else if ($queryKey == 'wiek') {
                        $filters['wiek'] = $queryData;
                        $conditions['Towar.wiek_id'] = $queryData;
                        $conditions2['Towar.wiek_id'] = $queryData;
                    } else if ($queryKey == 'plec') {
                        $filters['plec'] = $queryData;
                        $conditions[] = ['OR' => ['Towar.plec' => $queryData, 'Towar.plec IS' => NULL]];
                        $conditions2[] = ['OR' => ['Towar.plec' => $queryData, 'Towar.plec IS' => NULL]];
//                        $conditions['Towar.plec IN'] = [$queryData,NULL];
//                        $conditions2['Towar.plec IN'] = [$queryData,NULL];
                    } elseif ($queryKey == 'age') {
                        $ages = explode(';', $queryData);
                        if ($ages[0] == $ages[1]) {
                            $conditions['Towar.pegi_wiek'] = $ages[0];
                            $conditions2['Towar.pegi_wiek'] = $ages[0];
                        } else {

                            $conditions['Towar.pegi_wiek >='] = $ages[0];
                            $conditions['Towar.pegi_wiek <='] = $ages[1];
                            $conditions2['Towar.pegi_wiek >='] = $ages[0];
                            $conditions2['Towar.pegi_wiek <='] = $ages[1];
                        }
                        $filters[$queryKey] = $queryData;
                    } else if ($queryKey == 'price') {
                        $prices = explode(';', $queryData);
                        if (!empty($listLabel) && $listLabel == 'gratisy') {
                            $conditions['Towar.gratis_wartosc >='] = $prices[0];
                            $conditions['Towar.gratis_wartosc <='] = $prices[1];
                            $conditions2['Towar.gratis_wartosc >='] = $prices[0];
                            $conditions2['Towar.gratis_wartosc <='] = $prices[1];
                        } else {

                            $this->loadModel('TowarCena');

                            $priceCond = ['Towar.promocja' => 0, 'TowarCena.uzytkownik_id IS' => null, 'TowarCena.aktywna' => 1, 'TowarCena.cena_sprzedazy >= ' . $prices[0], 'TowarCena.cena_sprzedazy <= ' . $prices[1]];
                            $priceCond2 = ['(Towar.promocja = 1 OR Towar.wyprzedaz = 1)', 'TowarCena.uzytkownik_id IS' => null, 'TowarCena.aktywna' => 1, 'TowarCena.cena_promocja >= ' . $prices[0], 'TowarCena.cena_promocja <= ' . $prices[1]];
                            $priceCond3 = ['(Towar.promocja = 1 OR Towar.wyprzedaz = 1)', 'TowarCena.uzytkownik_id IS' => null, 'TowarCena.aktywna' => 1, 'TowarCena.cena_sprzedazy >= ' . $prices[0], 'TowarCena.cena_sprzedazy <= ' . $prices[1]];

                            $tIds = $this->TowarCena->find('list', ['keyField' => 'towar_id', 'valueField' => 'towar_id', 'conditions' => $priceCond])->join([
                                        'table' => 'towar',
                                        'alias' => 'Towar',
                                        'type' => 'INNER',
                                        'conditions' => 'Towar.id = TowarCena.towar_id'
                                    ])->select(['towar_id' => 'TowarCena.towar_id'])->toArray();
                            $tIds2 = $this->TowarCena->find('list', ['keyField' => 'towar_id', 'valueField' => 'towar_id', 'conditions' => $priceCond2])->join([
                                        'table' => 'towar',
                                        'alias' => 'Towar',
                                        'type' => 'INNER',
                                        'conditions' => 'Towar.id = TowarCena.towar_id'
                                    ])->select(['towar_id' => 'TowarCena.towar_id'])->toArray();
                            $tIds3 = $this->TowarCena->find('list', ['keyField' => 'towar_id', 'valueField' => 'towar_id', 'conditions' => $priceCond3])->join([
                                        'table' => 'towar',
                                        'alias' => 'Towar',
                                        'type' => 'INNER',
                                        'conditions' => 'Towar.id = TowarCena.towar_id'
                                    ])->select(['towar_id' => 'TowarCena.towar_id'])->toArray();
                            $tIds += $tIds2 + $tIds3;
                            if (empty($tIds)) {
                                $tIds = [null];
                            }
                            if (!empty($conditions['Towar.id IN'])) {
                                $conditions['Towar.id IN'] = array_intersect($conditions['Towar.id IN'], $tIds);
                                $conditions2['Towar.id IN'] = array_intersect($conditions2['Towar.id IN'], $tIds);
                            } else {
                                $conditions['Towar.id IN'] = $tIds;
                                $conditions2['Towar.id IN'] = $tIds;
                            }
                            if (!empty($attrTowarIds)) {
                                $attrTowarIds = array_intersect($attrTowarIds, $tIds);
                            } else {
                                $attrTowarIds = $tIds;
                            }
                        }
                        $filters[$queryKey] = $queryData;
                    } else if ($queryKey == 'attr') {
                        if (!empty($queryData)) {
                            foreach ($queryData as $attrTypId => $attrIds) {
                                foreach ($attrIds as $attrId) {
                                    $filters[$queryKey][$attrTypId][$attrId] = $attrId;
                                }
                                $attrJoins[$attrTypId] = [
                                    'table' => 'towar_atrybut',
                                    'alias' => 'TowarAtrybut' . $attrTypId,
                                    'type' => 'Inner',
                                    'conditions' => 'Towar.id = TowarAtrybut' . $attrTypId . '.towar_id'
                                ];
                                $conditions['TowarAtrybut' . $attrTypId . '.atrybut_id IN'] = $attrIds;
                            }
                        }
                    } else if ($queryKey == 'producent') {
                        $conditions['producent_id IN'] = $queryData;
                        $filters[$queryKey] = $queryData;
                    } else if ($queryKey == 'search') {
                        $fraza = explode(' ', $queryData);
                        foreach ($fraza as $word) {
                            $conditions['AND']['OR']['AND'][] = ['Towar.nazwa LIKE' => '%' . $word . '%'];
                            $conditions2['AND']['OR']['AND'][] = ['Towar.nazwa LIKE' => '%' . $word . '%'];
                        }
                        $conditions['AND']['OR'][] = ['Towar.tagi LIKE' => '%' . $queryData . '%'];
                        $conditions['AND']['OR'][] = ['Towar.kod LIKE' => '%' . $queryData . '%'];
                        $conditions2['AND']['OR'][] = ['Towar.ean LIKE' => '%' . $queryData . '%'];
                        $this->set('searchQuery', $queryData);
                    } else if ($queryKey == 'attr_range') {
                        $filters[$queryKey] = $queryData;
                        if (!empty($queryData)) {
                            $idsByRange = [];
                            foreach ($queryData as $attrRangeId => $attrRangeValue) {
                                $exVal = explode(';', $attrRangeValue);
                                $tIds = $this->Towar->TowarAtrybut->find('list', ['keyField' => 'towar_id', 'valueField' => 'towar_id'])->where(['atrybut_id' => $attrRangeId, 'wartosc IS NOT' => null, 'wartosc >=' => $exVal[0], 'wartosc <=' => $exVal[1]])->toArray();
                                if (empty($tIds)) {
                                    $tIds = [null];
                                }

                                if (!empty($attrTowarIds)) {
                                    $attrTowarIds = array_intersect($attrTowarIds, $tIds);
                                } else {
                                    $attrTowarIds = $tIds;
                                }
                                if (!empty($conditions['Towar.id IN'])) {
                                    $conditions['Towar.id IN'] = array_intersect($conditions['Towar.id IN'], $tIds);
                                    $conditions2['Towar.id IN'] = array_intersect($conditions2['Towar.id IN'], $tIds);
                                } else {
                                    $conditions['Towar.id IN'] = $tIds;
                                    $conditions2['Towar.id IN'] = $tIds;
                                }
                            }
                        }
                    }
                }
            }
            if (key_exists('Towar.id IN', $conditions) && empty($conditions['Towar.id IN'])) {
                $conditions['Towar.id IN'] = [null];
                $conditions2['Towar.id IN'] = [null];
            }
            if (key_exists($sortKey, $this->orders)) {
                $order = $this->orders[$sortKey];
            } else {
                $order = $this->orders['kod_asc'];
            }
            $towarCenaDefaultParams = ['Vat', 'Waluta', 'Jednostka', 'conditions' => ['TowarCenaDefault.waluta_id' => $this->active_jezyk->waluta_id]];
            $towarContain = [
                'HotDeal',
                'Wersja',
                'Kategoria',
                'Producent',
                'TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1], 'sort' => ['TowarZdjecie.domyslne' => 'desc', 'TowarZdjecie.kolejnosc' => 'desc']],
                'TowarAtrybut' => ['Atrybut' => ['AtrybutTyp', 'sort' => ['AtrybutTyp.kolejnosc' => 'desc', 'Atrybut.kolejnosc' => 'desc']]],
                'TowarCenaDefault' => $towarCenaDefaultParams
            ];

            if ($listLabel == 'prezenty') {
                $towarContain['Okazje'] = ['sort' => ['kolejnosc' => 'desc', 'nazwa' => 'asc']];
            }
            $this->paginate = [
                'contain' => $towarContain,
                'limit' => $limit,
                'order' => $order,
                'conditions' => $conditions,
                'group' => ['Towar.id'],
                'join' => $attrJoins,
                'sortWhitelist' => ['Towar.kolejnosc', 'Towar.id', 'Towar.kod', 'Towar.nazwa', 'price']
            ];

            $columns = ['price' => 'if((Towar.promocja = 1 OR Towar.wyprzedaz = 1) AND TowarCenaDefault.cena_promocja > 0,TowarCenaDefault.cena_promocja,TowarCenaDefault.cena_sprzedazy)'];

            foreach ($this->Towar->schema()->columns() as $col) {
                $columns[] = 'Towar.' . $col;
            }
//            $columns= array_merge($columns,$this->Towar->Producent->schema()->columns());
            foreach ($this->Towar->Producent->schema()->columns() as $col) {
                $columns[] = 'Producent.' . $col;
            }
            foreach ($this->Towar->HotDeal->schema()->columns() as $col) {
                $columns[] = 'HotDeal.' . $col;
            }
            foreach ($this->Towar->Wersja->schema()->columns() as $col) {
                $columns[] = 'Wersja.' . $col;
            }
//            foreach ($this->Towar->Kategoria->schema()->columns() as $col) {
//                $columns[] = 'Kategoria.' . $col;
//            }
//            $columns= array_merge($columns,$this->Towar->TowarCenaDefault->schema()->columns());
            foreach ($this->Towar->TowarCenaDefault->schema()->columns() as $col) {
                $columns[] = 'TowarCenaDefault.' . $col;
            }
            foreach ($this->Towar->TowarCenaDefault->Vat->schema()->columns() as $col) {
                $columns[] = 'Vat.' . $col;
            }
            foreach ($this->Towar->TowarCenaDefault->Jednostka->schema()->columns() as $col) {
                $columns[] = 'Jednostka.' . $col;
            }
            foreach ($this->Towar->TowarCenaDefault->Waluta->schema()->columns() as $col) {
                $columns[] = 'Waluta.' . $col;
            }
//            if($listLabel=='prezenty'){
//            foreach ($this->Towar->Okazje->schema()->columns() as $col) {
//                $columns[] = 'Okazje.' . $col;
//            }
//            }
            $allQuery = $this->Towar->find()->select($columns);
            $towary = $this->paginate($allQuery);
            $producerCond = $conditions2;
            $attrCond = $conditions;
            if (!empty($kategoriaId)) {
                if (empty($allKatIds)) {
                    $allKatIds = $this->Towar->Kategoria->getAllIds($kategoriaId);
                }
                $producerCond['TowarKategoria.kategoria_id IN'] = $allKatIds;
            }
            //SELECT t.producent_id,t.id, count(DISTINCT(t.id)) FROM `towar` t INNER JOIN towar_kategoria tk ON t.id=tk.towar_id WHERE tk.kategoria_id=8 GROUP BY t.producent_id
            $countByProducer = $this->Towar->find('list', ['keyField' => 'producent_id', 'valueField' => 'all_suma'])->select(['producent_id', 'all_suma' => 'count(DISTINCT(Towar.id))'])->join([
                        'table' => 'towar_kategoria',
                        'alias' => 'TowarKategoria',
                        'type' => 'Inner',
                        'conditions' => 'Towar.id = TowarKategoria.towar_id',
                    ])->group(['Towar.producent_id'])->where($producerCond)->toArray();

            $attrJoins[0]['type'] = 'INNER';
            $countAttrs = $this->Towar->find('list', ['keyField' => 'attr_id', 'valueField' => 'all_suma'])->select(['attr_id' => 'TowarAtrybut.atrybut_id', 'all_suma' => 'count(DISTINCT(Towar.id))'])->join($attrJoins)->group(['TowarAtrybut.atrybut_id'])->where($attrCond)->toArray();
            $allProducers = null;
            if (!empty($countByProducer)) {
                if (empty($this->Producent)) {
                    $this->loadModel('Producent');
                }
                $allProducers = $this->Producent->find('list', ['keyField' => 'id', 'valueField' => 'nazwa'])->where(['Producent.id IN' => array_keys($countByProducer)])->order(['Producent.kolejnosc' => 'desc'])->toArray();
            }
            $this->set(compact('countByProducer', 'allProducers', 'countAttrs'));
            if (!empty($kategoria->parent_kategorium) && !empty($kategoria->parent_kategorium->parent_id)) {
                $kategorieAll = $this->Towar->Kategoria->getCategoryTree($kategoria->parent_kategorium->parent_id);
            } elseif (!empty($kategoria->parent_kategorium)) {
                $kategorieAll = $this->Towar->Kategoria->getCategoryTree($kategoria->parent_kategorium->id);
            } else {
                $kategorieAll = $this->Towar->Kategoria->getCategoryTree((!empty($kategoria) ? $kategoria->id : null));
            }
            if (!empty($kategoria) && empty($kategorieAll)) {
                $kategorieAll = $this->Towar->Kategoria->getCategoryTree((!empty($kategoria->parent_id) ? $kategoria->parent_id : null));
            }
            $priceCond = ['TowarCena.uzytkownik_id IS' => NULL, 'TowarCena.aktywna' => 1];

            if (!empty($cenaTowarIds)) {
                $priceCond['TowarCena.towar_id IN'] = $cenaTowarIds;
            }
            if (!empty($maxPriceCondAvil)) {
                $priceCond = array_merge($priceCond, $maxPriceCondAvil);
            }
            if (!empty($listLabel) && $listLabel == 'gratisy') {
                $condPGratis = $conditions;
                unset($condPGratis['Towar.gratis_wartosc <='], $condPGratis['Towar.gratis_wartosc >=']);
                $maxPrice = $this->Towar->find('all')->where($condPGratis)->max('gratis_wartosc')->gratis_wartosc;
            } else {
                $maxPriceItem = $this->TowarCena->find('all', ['contain' => ['Vat', 'Towar'], 'conditions' => $priceCond])->max('cena_sprzedazy');
                $rodzajCen = $this->cenyRodzaj;
                if (!empty($maxPriceItem)) {
                    $maxPrice = ceil($this->Txt->cenaVat($maxPriceItem->cena_sprzedazy, $maxPriceItem->vat->stawka, $maxPriceItem->rodzaj, $rodzajCen, false));
                } else {
                    $maxPrice = 0;
                }
            }
            $attrCond = [];
            if (!empty($attrTowarIds)) {
                $attrCond = ['towar_id IN' => $attrTowarIds];
            }
            $attrAllowIds = $this->Towar->TowarAtrybut->find('list', ['keyField' => 'atrybut_id', 'valueField' => 'atrybut_id', 'group' => 'atrybut_id', 'conditions' => $attrCond])->toArray();
            $attrAllCond = [];
            if (!empty($attrAllowIds)) {
                $attrAllCond = ['Atrybut.id IN' => $attrAllowIds];
            }
            $maxAttrRange = $this->Towar->TowarAtrybut->find('list', ['keyField' => 'atrybut_id', 'valueField' => 'max'])->select(['atrybut_id', 'max' => 'MAX(wartosc)'])->where(['wartosc IS NOT' => null])->group(['atrybut_id'])->toArray();
            $attrsAll = $this->AtrybutTyp->find('all', ['contain' => ['Atrybut' => ['conditions' => $attrAllCond], 'AtrybutTypParent']]);
            $defCurr = \Cake\Core\Configure::read('currencyDefault');
            $currencyOptions = $this->currencyOptions[$defCurr];
            $currencySymbol = \Cake\Core\Configure::read('currencysSymbol.' . $defCurr);
            if ($listLabel == 'prezenty') {
                $okazje = $this->Towar->Okazje->find('list', ['keyField' => 'id', 'valueField' => 'nazwa'])->order(['kolejnosc' => 'desc', 'nazwa' => 'asc'])->toArray();
                $this->set('okazje', $okazje);
            }
            $this->set(compact('towary', 'kategorieAll', 'filters', 'maxPrice', 'currencyOptions', 'currencySymbol', 'attrsAll', 'maxAttrRange'));
            $this->set('_serialize', ['towary']);
            $this->set('limit', $limit);
            $this->set('sortKey', $sortKey);
            $this->session->write('towarListaLimit', $limit);
            $this->session->write('towarListaOrder', $sortKey);
            $view = $this->session->read('TowarLista.view');
            if (!empty($_GET['view'])) {
                $view = $_GET['view'];
            }
            if (empty($view))
                $view = 'clasic';
            if ($this->request->is('mobile'))
                $view = 'box';
            $this->session->write('TowarLista.view', $view);
            $this->set('listaView', $view);
        }
        if (!empty($kategoria)) {

            if (!empty($kategoria->seo_tytul)) {
                $this->seoData['title'] = $kategoria->seo_tytul;
            } else {
                $this->seoData['title'] = $kategoria->nazwa;
            }
            if (!empty($kategoria->seo_slowa)) {
                $this->seoData['keywords'] = $kategoria->seo_slowa;
            }
            if (!empty($kategoria->seo_opis)) {
                $this->seoData['description'] = $kategoria->seo_opis;
            }
        }
    }

    public function view($id) {

        $towar = $this->Towar->find('all', [
                    'conditions' => ['Towar.id' => $id, 'Towar.ukryty' => 0],
                    'contain' => [
                        'TowarCenaDefault' => $this->towarCenaDefaultParams,
                        'TowarZdjecie' => ['sort' => ['TowarZdjecie.kolejnosc' => 'desc', 'TowarZdjecie.domyslne' => 'desc']],
                        'TowarWariant' => ['Kolor', 'Rozmiar'],
                        'Producent',
                        'Certyfikat',
                        'Wiek',
                        'Opakowanie',
                        'HotDeal',
                        'Atrybut' => ['AtrybutTyp', 'TowarAtrybut' => ['conditions' => ['TowarAtrybut.towar_id' => $id]], 'sort' => ['AtrybutTyp.kolejnosc' => 'desc', 'Atrybut.kolejnosc' => 'desc']],
                        'Podobne' => ['TowarZdjecie', 'TowarCenaDefault' => $this->towarCenaDefaultParams],
                        'Polecane' => ['TowarZdjecie', 'TowarCenaDefault' => $this->towarCenaDefaultParams],
                        'Akcesoria' => ['TowarZdjecie', 'TowarCenaDefault' => $this->towarCenaDefaultParams],
//                        'Zestaw' => ['ZestawTowar' => ['Towar' => ['TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]], 'TowarCenaDefault' => $this->towarCenaDefaultParams]], 'sort' => ['kolejnosc' => 'DESC']],
                        'TowarOpinia' => ['conditions' => ['TowarOpinia.status' => 2]],
                        'Kategoria',
            ]])->first();
        if (empty($towar)) {
            $this->Flash->error($this->Translation->get('errors.itemIsUnavailable'));
            return $this->redirect(['action' => 'index']);
        }
        $conditions = ['Zestaw.towar_id' => $id];
        $zestaw = $this->Towar->Zestaw->find('all', [
            'conditions' => $conditions,
            'contain' => [
                'Towar' => ['TowarCenaDefault' => $this->towarCenaDefaultParams, 'TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]]],
                'ZestawTowar' => ['Towar' => ['TowarCenaDefault' => $this->towarCenaDefaultParams, 'TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]]]]
            ],
            'order' => ['Zestaw.kolejnosc DESC']
        ]);
        $cartItems = $this->session->read('cartItems');
        $this->loadModel('Wysylka');
        $wysylkiNames = $this->Wysylka->find('list', ['keyField' => 'id', 'valueField' => 'nazwa'])->toArray();
        $wysylkiOpis = $this->Wysylka->find('list', ['keyField' => 'id', 'valueField' => 'opis'])->toArray();
        $wysylki = $this->Wysylka->getPrice($towar->waga,$cartItems['total_z_rabatem']);
        $this->loadModel('TowarAtrybut');
        $wersje = $this->TowarAtrybut->find('all', ['conditions' => ['TowarAtrybut.towar_id' => $id, 'TowarAtrybut.wariant' => 1], 'contain' => ['AtrybutPodrzedne', 'TowarZdjecie' => ['sort' => ['TowarZdjecie.kolejnosc' => 'desc']], 'Atrybut' => ['AtrybutTyp', 'AtrybutPodrzedne']], 'order' => ['AtrybutPodrzedne.nazwa' => 'asc']]);
        $this->set('wersje', $wersje);
        $this->set('towar', $towar);
        $this->set('wysylki', $wysylki);
        $this->set('wysylkiNames', $wysylkiNames);
        $this->set('wysylkiOpis', $wysylkiOpis);
        $this->set('zestaw', $zestaw);
        if (!empty($towar['kategoria'])) {
            $kategoria = null;
            $lastCategory = $this->session->read('lastCategory');
            if (count($towar['kategoria']) > 1 && !empty($lastCategory)) {
                foreach ($towar['kategoria'] as $katItem) {
                    if ($katItem['id'] == $lastCategory) {
                        $kategoria = $katItem;
                        break;
                    }
                }
            }
            if (empty($kategoria)) {
                $kategoria = $towar['kategoria'][0];
            }
            $this->set('kategoria', $kategoria);
            $crumbs = $this->Towar->Kategoria->getCrumb($kategoria->id);
            $crumbs = array_reverse($crumbs);
            $crumbs[$towar->nazwa] = null;
            $this->set('crumbs', $crumbs);
        }

        $ostatnioOgladane = $this->request->getCookie('ostatnio_ogladane');
        if (empty($ostatnioOgladane)) {
            $ostatnioOgladane = [];
        } else {
            $ostatnioOgladane = json_decode($ostatnioOgladane);
            if (!is_array($ostatnioOgladane)) {
                $this->writeCookie('ostatnio_ogladane', null, '- 1 day');
                $ostatnioOgladane = [];
            } else {
                $extKey = array_search($towar->id, $ostatnioOgladane);
                if ($extKey !== false) {
                    unset($ostatnioOgladane[$extKey]);
                } else {
                    if (count($ostatnioOgladane) >= 5) {
                        unset($ostatnioOgladane[count($ostatnioOgladane) - 1]);
                    }
                }
                $tmpOst = [];
                $tmpKey = 1;
                foreach ($ostatnioOgladane as $item) {
                    $tmpOst[$tmpKey++] = $item;
                }
                $ostatnioOgladane = $tmpOst;
            }
        }
        $ostatnioOgladane[0] = $towar->id;
        ksort($ostatnioOgladane);
        $this->writeCookie('ostatnio_ogladane', json_encode($ostatnioOgladane));
        $ostatnie = null;
        if (!empty($ostatnioOgladane)) {
            $ostatnie = $this->Towar->find('all', [
                'conditions' => ['Towar.ukryty' => 0, 'Towar.id IN' => $ostatnioOgladane, 'Towar.id NOT IN' => [$towar->id]],
                'contain' => [
                    'TowarCenaDefault' => $this->towarCenaDefaultParams,
                    'TowarZdjecie' => ['sort' => ['TowarZdjecie.kolejnosc' => 'desc', 'TowarZdjecie.domyslne' => 'desc']]
            ]]);
        }
        $this->set('ostatnie', $ostatnie);
        $this->singleSite = true;
        if ($this->request->is('mobile')) {
            $this->forceHideLeft = true;
        }
        if (!empty($towar->seo_tytul)) {
            $this->seoData['title'] = $towar->seo_tytul;
        } else {
            $this->seoData['title'] = $towar->nazwa;
        }
        if (!empty($towar->seo_slowa)) {
            $this->seoData['keywords'] = $towar->seo_slowa;
        }
        if (!empty($towar->seo_opis)) {
            $this->seoData['description'] = $towar->seo_opis;
        }
        $this->loadModel('Powiadomienia');
        $powiadomienie = $this->Powiadomienia->newEntity();
        $this->set('powiadomienie', $powiadomienie);
        if ($towar->hurtownia == 'ptakmodahurt') {
            $this->moreStyle[] = 'https://ptakmodahurt.pl/xmlfiles/data?css=url&all=style';
        }
        if (!empty($cartItems) && $cartItems['total_brutto'] >= c('gratis.wartosc_min')) {
            $cartPrice = $cartItems['total_brutto'];
        } else {
            $cartPrice = c('gratis.wartosc_min');
        }
        if (!empty($cartItems) && !empty($towar->gratis) && $towar->gratis_wartosc <= $cartItems['total_brutto']) {
            $this->set('allowAsGratis', true);
        }
        $gratisy = $this->Towar->find('all', ['contain' => ['Zdjecie']])->where(['Towar.ukryty' => 0, 'Towar.ilosc >' => 0, 'Towar.gratis' => 1, 'Towar.gratis_wartosc >' => 0, 'Towar.gratis_wartosc <=' => $cartPrice])->limit(3)->order(['rand()']);
        $this->set('gratisy', $gratisy);
        $this->loadModel('ZamowienieTowar');
        $towarSprzedane = $this->ZamowienieTowar->find()->where(['ZamowienieTowar.towar_id'=>$towar->id])->count();
        $this->set('towarSprzedane', $towarSprzedane);
    }

    public function zestawy($kategoriaId = null) {
        $this->loadModel('Zestaw');
        $conditions = ['Towar.ilosc > ' => 0];
        $crumbs = [
            t('breadcrumb.zestawy') => r(['controller' => 'Towar', 'action' => 'zestawy'])
        ];
        $kategoria = null;
        $join = [];
        if (!empty($kategoriaId) && is_numeric($kategoriaId)) {
            $join[] = [
                'table' => 'towar_kategoria',
                'alias' => 'TowarKategoria',
                'type' => 'Inner',
                'conditions' => 'Zestaw.towar_id = TowarKategoria.towar_id',
            ];
            $kategoria = $this->Towar->Kategoria->find('all', ['contain' => ['ParentKategoria' => ['ParentKategoria']]])->where(['Kategoria.id' => $kategoriaId])->first();
            if (!empty($kategoria)) {
                $tmpCrumbs = $this->Towar->Kategoria->getCrumb($kategoria->id, [], [], 'zestawy');
                $tmpCrumbs = array_reverse($tmpCrumbs);
                $crumbs = array_merge($crumbs, $tmpCrumbs);
                $this->set('kategoria', $kategoria);
                $allKatIds = $this->Towar->Kategoria->getAllIds($kategoriaId);
                $conditions['TowarKategoria.kategoria_id IN'] = $allKatIds;
            }
        }
        $this->paginate = [
            'conditions' => $conditions,
            'contain' => [
                'Towar' => ['TowarCenaDefault' => $this->towarCenaDefaultParams, 'TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]]],
                'ZestawTowar' => ['Towar' => ['TowarCenaDefault' => $this->towarCenaDefaultParams, 'TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]]]]
            ],
            'order' => ['Zestaw.kolejnosc DESC'],
            'join' => (!empty($join) ? $join : null)
        ];
        $zestawy = $this->paginate($this->Zestaw);

        if (!empty($kategoria->parent_kategorium) && !empty($kategoria->parent_kategorium->parent_id)) {
            $kategorieAll = $this->Towar->Kategoria->getCategoryTree($kategoria->parent_kategorium->parent_id);
        } elseif (!empty($kategoria->parent_kategorium)) {
            $kategorieAll = $this->Towar->Kategoria->getCategoryTree($kategoria->parent_kategorium->id);
        } else {
            $kategorieAll = $this->Towar->Kategoria->getCategoryTree((!empty($kategoria) ? $kategoria->id : null));
        }
        if (!empty($kategoria) && empty($kategorieAll)) {
            $kategorieAll = $this->Towar->Kategoria->getCategoryTree((!empty($kategoria->parent_id) ? $kategoria->parent_id : null));
        }

        $crumbsKey = array_keys($crumbs);
        $crumbs[$crumbsKey[(count($crumbsKey) - 1)]] = null;
        $this->set(compact('zestawy', 'crumbs', 'kategorieAll'));


        $this->seoData['title'] = t('seo.zestawyTitle');
        $this->seoData['keywords'] = t('seo.zestawyKeywords');
        $this->seoData['description'] = t('seo.zestawyDescription');
    }

    public function image($imgId = null, $fileName = null, $znakWodny = false) {
        $this->loadComponent('Upload');
        $this->autoRender = false;
//        $file = file_get_contents($this->filePath['towar'] . $this->Txt->getKatalog($imgId) . DS . $fileName);
        $filePath = $this->filePath['towar'] . $this->Txt->getKatalog($imgId) . DS . $fileName;
        if (!file_exists($filePath)) {
            if (strpos($fileName, 'thumb') === false) {
                $fileName = 'thumb_3_' . $fileName;
                $filePath = $this->filePath['towar'] . $this->Txt->getKatalog($imgId) . DS . $fileName;
            }
        }
        if (file_exists($filePath)) {
            if ($znakWodny) {
                $path = ROOT . DS . 'webroot' . DS . 'img' . DS . 'znak_wodny.png';
                $newFile = $this->Upload->imageDisplay($filePath, $path, \Cake\Core\Configure::read('zdjecia.znak_wodny_polozenie'));
            } else {
                $newFile = $this->Upload->imageDisplay($filePath);
            }
        } else {
            $newFile = $this->Upload->imageDisplay($this->filePath['img'] . 'noPhoto.png');
        }
        header('Content-Type: image/png');
        echo imagepng($newFile);
        die();
    }

    public function opinia($towar_id = null) {
        $this->autoRender = false;
        if (!empty($towar_id)) {
            $this->loadModel('TowarOpinia');
            if ($this->request->is('post')) {
                $data = $this->request->getData();
                $data['data'] = date('Y-m-d H:i:s');
                $data['towar_id'] = $towar_id;
                $data['status'] = 1;
                $data['jezyk'] = \Cake\I18n\I18n::locale();
                $data['opinia'] = strip_tags($data['opinia']);
                $opinia = $this->TowarOpinia->newEntity($data);
                if ($this->TowarOpinia->save($opinia))
                    $this->Flash->success($this->Translation->get('opinia.messageAddSuccess'));
                else
                    $this->Flash->error($this->Translation->get('opinia.messageAddError'));
            }
        } else
            $this->Flash->error($this->Translation->get('opinia.messageAddError'));
        return $this->redirect($_SERVER['HTTP_REFERER']);
    }

    public function addToFavorite($towarId = null) {
        $this->autoRender = false;
        if ($this->request->is('ajax')) {
            $return = [];
            if (!empty($towarId)) {
                $addToFavoriteItem = ['towar_id' => $towarId, 'data' => date('Y-m-d H:i:s')];
                $schowek = $this->session->read('schowek');
                $schowek[$towarId] = $towarId;
                $this->session->write('schowek', $schowek);
                if (!empty($this->userId)) {
                    $addToFavoriteItem['uzytkownik_id'] = $this->userId;
                    $this->loadModel('Schowek');
                    $saveItem = $this->Schowek->newEntity($addToFavoriteItem);
                    $this->Schowek->save($saveItem);
                }
                $return = ['status' => 'success'];
            } else {
                $return = ['status' => 'error'];
            }
            $this->response->type('ajax');
            $this->response->body(json_encode($return));
            return $this->response;
        } else {
            return $this->redirect('/');
        }
    }

    public function removeFavorite($towarId = null) {
        $this->autoRender = false;
        if ($this->request->is('ajax')) {
            $return = [];
            if (!empty($towarId)) {
                $schowek = $this->session->read('schowek');
                unset($schowek[$towarId]);
                $this->session->write('schowek', $schowek);
                if (!empty($this->userId)) {
                    $this->loadModel('Schowek');
                    $this->Schowek->deleteAll(['uzytkownik_id' => $this->userId, 'towar_id' => $towarId]);
                }
                $return = ['status' => 'success'];
            } else {
                $return = ['status' => 'error'];
            }
            $this->response->type('ajax');
            $this->response->body(json_encode($return));
            return $this->response;
        } else {
            return $this->redirect('/');
        }
    }

    public function producent($id = null) {
        if (empty($id)) {
            return $this->redirect(['action' => 'index']);
        }
        $this->loadModel('Producent');
        $producent = $this->Producent->find('all')->where(['Producent.id' => $id, 'Producent.ukryty' => 0])->first();
        if (empty($producent)) {
            return $this->redirect(['action' => 'index']);
        }
        $katList = $this->Towar->find('list', ['groupField' => 'par_id', 'keyField' => 'kat_id', 'valueField' => 'sum_t'])->join(
                        [[
                        'table' => 'towar_kategoria',
                        'alias' => 'TowarKategoria',
                        'type' => 'Inner',
                        'conditions' => 'Towar.id = TowarKategoria.towar_id',
                            ], [
                                'table' => 'kategoria',
                                'alias' => 'Kategoria',
                                'type' => 'Inner',
                                'conditions' => 'TowarKategoria.kategoria_id = Kategoria.id',
                            ]]
                )->select(['kat_id' => 'Kategoria.id', 'par_id' => 'Kategoria.parent_id', 'sum_t' => 'count(Towar.id)'])->where(['Towar.ukryty' => 0, 'Towar.producent_id' => $id, 'Kategoria.ukryta' => 0])->group(['Kategoria.id'])->toArray();
        $allKat = [];
        if (!empty($katList)) {
            $this->loadModel('Kategoria');
            $kategorie = $this->Kategoria->find('all')->where(['Kategoria.id IN' => array_keys($katList), 'Kategoria.ukryta' => 0]);
            foreach ($kategorie as $kategoria) {
                $allKat[$kategoria->id] = $kategoria->toArray();
                $allKat[$kategoria->id]['sum'] = array_sum($katList[$kategoria->id]);
            }
        }
        $this->set(compact('producent', 'allKat'));

        if ($this->request->is('mobile')) {
            $this->forceHideLeft = true;
        }
    }

    public function searchRq() {
        $this->viewBuilder()->setLayout('ajax');
        $queryData = $this->request->getData('search');
        if (!empty($queryData)) {
            $fraza = explode(' ', $queryData);
            $catId = $this->request->getData('cat');
            $conditions = ['Towar.ukryty' => 0];
            foreach ($fraza as $word) {
                $conditions['AND']['OR']['AND'][] = ['Towar.nazwa LIKE' => '%' . $word . '%'];
            }
            $conditions['AND']['OR'][] = ['Towar.tagi LIKE' => '%' . $queryData . '%'];
            $conditions['AND']['OR'][] = ['Towar.kod LIKE' => '%' . $queryData . '%'];
            if (!empty($catId)) {
                $kategoria = $this->Towar->Kategoria->find('all')->where(['Kategoria.id' => $catId])->first();
                if (!empty($kategoria->preorder)) {
                    $conditions['Towar.preorder'] = 1;
                    $conditions['Towar.data_premiery >'] = date('Y-m-d');
                } else {
                    $allKatIds = $this->Towar->Kategoria->getAllIds($catId);
                    $tIds = $this->Towar->TowarKategoria->find('list', ['keyField' => 'towar_id', 'valueField' => 'towar_id', 'group' => ['towar_id'], 'conditions' => ['kategoria_id IN' => $allKatIds]])->toArray();
                    if (empty($tIds)) {
                        $tIds = [null];
                    }
                    $conditions['Towar.id IN'] = $tIds;
                }
            }
            $towary = $this->Towar->find('all')->where($conditions)->limit(10);
            $this->set(compact('towary'));
        } else {
            $this->autoRender = false;
            $this->response->type('ajax');
            $this->response->body('');
            return $this->response;
        }
    }

    public function powiadomOCenie($towarId) {
        $this->viewBuilder()->setLayout('ajax');
        if (empty($this->Towar)) {
            $this->loadModel('Towar');
        }
        $this->loadModel('Powiadomienia');
        $towarCenaDefaultParams = ['Vat', 'Waluta', 'Jednostka', 'conditions' => ['TowarCenaDefault.waluta_id' => $this->active_jezyk->waluta_id]];
        $towarContain = ['HotDeal', 'TowarCenaDefault' => $towarCenaDefaultParams];
        $towar = $this->Towar->find('all', ['contain' => $towarContain])->where(['Towar.id' => $towarId])->first();
        $powiadomienie = $this->Powiadomienia->newEntity();
        if ($this->request->is('post')) {
            $rqData = $this->request->getData();
//            if (!empty($rqData['zgoda'])) {
            $rqData['typ'] = 'cena';
            $rqData['uzytkownik_id'] = $this->Auth->user('id');
            $rqData['towar_id'] = $towar->id;
            $rqData['data_dodania'] = date('Y-m-d H:i:s');
            $rqData['cena'] = $this->Txt->itemPrice($towar);
            $powiadomienie = $this->Powiadomienia->patchEntity($powiadomienie, $rqData);
            if ($this->Powiadomienia->save($powiadomienie)) {
                $this->Flash->success($this->Translation->get('success.zgloszenieZostaloZapisane'));
            } else {
                $this->Flash->error($this->Translation->get('errors.bladZapisuZgloszenia'));
            }
//            } else {
//                $this->Flash->error($this->Translation->get('errors.zgodaNaPrzetwarzanieDanychJestWymagana'));
//            }
        } else {
            if (!empty($this->userId)) {
                $powiadomienie->set('email', $this->Auth->user('email'));
                $powiadomienie->set('imie', $this->Auth->user('imie'));
            }
        }
        if (!$this->request->is('ajax')) {
            return $this->redirect($this->request->referer());
        }
    }

    public function activeBonus() {
        $this->autoRender = false;
        $returnArray = [];
        $bonus = $this->Auth->user('bonusy_suma');
        if (!empty($bonus)) {
            $this->session->write('orderFormData.bonusowe_zlotowki', $bonus);
            $returnArray = ['status' => 'success', 'message' => $this->Translation->get('success.twojeZamowienieZostaniePomniejszoneWPodsumowaniuKoszyka')];
        } else {
            $returnArray = ['status' => 'error', 'message' => $this->Translation->get('errors.niePosiadaszZlotowekDoWykorzystania')];
        }
        $this->response->type('ajax');
        $this->response->body(json_encode($returnArray));
        return $this->response;
    }

}
