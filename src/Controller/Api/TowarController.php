<?php

namespace App\Controller\Api;

use App\Controller\AppController;

/**
 * Towar Controller
 *
 * @property \App\Model\Table\TowarTable $Towar
 *
 * @method \App\Model\Entity\Towar[] paginate($object = null, array $settings = [])
 */
class TowarController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        if (!empty($this->Auth)) {
            $this->Auth->allow();
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function full() {
        $this->loadModel('Uzytkownik');
        $token = $this->request->getQuery('token');
        $user = $this->Uzytkownik->find()->where(['Uzytkownik.api_token' => $token, 'Uzytkownik.typ' => 'b2b'])->first();
        if (!empty($user)) {
            $cfgArr = [
                'contain' => ['Producent', 'Kategoria', 'TowarCenaDefault' => ['Vat', 'Jednostka'], 'Wiek', 'Opakowanie', 'TowarZdjecie', 'TowarAtrybut' => ['Atrybut' => ['AtrybutTyp']]]
            ];
            $towary = $this->Towar->find('all', $cfgArr)->where(['Towar.ukryty'=>0]);
            $ext = $this->request->getParam('_ext');
            if (!empty($ext)) {
                $items = [];
                foreach ($towary as $towar) {
                    $kategorieArr = [];
                    if (!empty($towar->kategoria)) {
                        foreach ($towar->kategoria as $kategoria) {
                            $kategorieArr[] = ['id' => $kategoria->id, 'name' => $kategoria->nazwa, 'path' => $kategoria->sciezka];
                        }
                    }
                    $images = [];
                    if (!empty($towar->towar_zdjecie)) {
                        foreach ($towar->towar_zdjecie as $zdjecie) {
                            $images[] = $this->Txt->towarZdjecieSrc($zdjecie->id, $zdjecie->plik, '', true);
                        }
                    }
                    $atributes = [];
                    if (!empty($towar->towar_atrybut)) {
                        foreach ($towar->towar_atrybut as $towarAtrybut) {
                            if (!empty($towarAtrybut->atrybut->pole)) {
                                $atributes[$towarAtrybut->atrybut->nazwa][] = $towarAtrybut->wartosc;
                            } else {
                                $atributes[$towarAtrybut->atrybut->atrybut_typ->nazwa][] = $towarAtrybut->atrybut->nazwa;
                            }
                        }
                    }
                    $item = [
                        'id' => $towar->id,
                        'name' => $towar->nazwa,
                        'code' => $towar->kod,
                        'ean' => $towar->ean,
                        'producer' => (!empty($towar->producent) ? $towar->producent->nazwa : ''),
                        'category' => $kategorieArr,
                        'price_brutto' => $this->Txt->itemPrice($towar, false, 'brutto'),
                        'vat' => $towar->towar_cena_default->vat->stawka,
                        'unit' => $towar->towar_cena_default->jednostka->jednostka,
                        'quantity' => $towar->ilosc,
                        'quantity_in_box' => (!empty($towar->ilosc_w_kartonie) ? $towar->ilosc_w_kartonie : 1),
                        'description' => str_replace(['src="/'], ['src="' . \Cake\Routing\Router::url('/', true) . '/'], $towar->opis),
                        'images' => $images,
                    ];
                    if (!empty($towar->wiek)) {
                        $item['age'] = $towar->wiek->nazwa;
                    }
                    if (!empty($towar->opakowanie)) {
                        $item['package'] = $towar->opakowanie->nazwa;
                    }
                    if (!empty($towar->wymiary)) {
                        $item['dimensions'] = $towar->wymiary;
                    }
                    if (!empty($towar->wymiary_opakowania)) {
                        $item['package_size'] = $towar->wymiary_opakowania;
                    }
                    if (!empty($towar->baterie)) {
                        $item['batteries'] = $towar->baterie;
                    }
                    if (!empty($atributes)) {
                        $item['attributes'] = $atributes;
                    }
                    $items[] = $item;
                }

                if ($ext == 'json') {
                    $this->response->type('ajax');
                    $this->response->body(json_encode($items));
                    return $this->response;
                }
                $this->set('items', $items);
                $this->render('index');
            }
        } else {
                    $this->response->type('ajax');
                    $this->response->body(t('errors.notAutorizedUserApi'));
                    return $this->response;
        }
    }
    public function min() {
        $this->loadModel('Uzytkownik');
        $token = $this->request->getQuery('token');
        $user = $this->Uzytkownik->find()->where(['Uzytkownik.api_token' => $token, 'Uzytkownik.typ' => 'b2b'])->first();
        if (!empty($user)) {
            $cfgArr = [
                'contain' => ['TowarCenaDefault' => ['Vat', 'Jednostka']]
            ];
            $towary = $this->Towar->find('all', $cfgArr)->where(['Towar.ukryty'=>0]);
            $ext = $this->request->getParam('_ext');
            if (!empty($ext)) {
                $items = [];
                foreach ($towary as $towar) {
                    $item = [
                        'id' => $towar->id,
                        'code' => $towar->kod,
                        'ean' => $towar->ean,
                        'price_brutto' => $this->Txt->itemPrice($towar, false, 'brutto'),
                        'vat' => $towar->towar_cena_default->vat->stawka,
                        'unit' => $towar->towar_cena_default->jednostka->jednostka,
                        'quantity' => $towar->ilosc,
                        'quantity_in_box' => (!empty($towar->ilosc_w_kartonie) ? $towar->ilosc_w_kartonie : 1)
                    ];
                    $items[] = $item;
                }

                if ($ext == 'json') {
                    $this->response->type('ajax');
                    $this->response->body(json_encode($items));
                    return $this->response;
                }
                $this->set('items', $items);
                $this->render('index');
            }
        } else {
                    $this->response->type('ajax');
                    $this->response->body(t('errors.notAutorizedUserApi'));
                    return $this->response;
        }
    }

}
