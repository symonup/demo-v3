<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Network\Email\Email;
use Cake\Core\Configure;
use Psr\Log\LogLevel;
use Exception;

class MailComponent extends Component {

    private $Mail;
    private $profile = 'clever';
    private $profile2 = 'admin';

    public function initialize(array $config) {
        $this->Mail = new Email();
        $this->Mail->configTransport($this->profile, array_merge(['className' => 'Smtp'], Configure::read('smtp')));
        $this->Mail->config($this->profile, [
            'transport' => $this->profile,
            'from' => Configure::read('smtp.username'),
            'emailFormat' => 'html',
                //'template' => 'email',
                //'viewVars' => $this->request->data
        ]);
        $this->MailAdmin = new Email();
        $this->MailAdmin->configTransport($this->profile2, array_merge(['className' => 'Smtp'], Configure::read('smtp')));
        $this->MailAdmin->config($this->profile2, [
            'transport' => $this->profile2,
            'from' => Configure::read('smtp.username'),
            'emailFormat' => 'html',
                //'template' => 'email',
                //'viewVars' => $this->request->data
        ]);
    }

    public function sendMail($email, $subject, $message, $config = []) {
        $attachment=null;
        if(!empty($config['attachment'])){
         $attachment=$config['attachment'];
            unset($config['attachment']);
        }
        $config = array_merge($this->Mail->config($this->profile), $config);
        if ($this->Mail->config('send') == null) {
            $this->Mail->config('send', $config);
        }
        if(!empty($attachment)){
            $this->Mail->attachments($attachment);
        }
        $this->Mail->profile('send')->to($email)->subject($subject);
        try {
            return $this->Mail->send($message);
        } catch (Exception $exc) {
            $this->log($exc->getMessage(), LogLevel::INFO, 'email');
            return false;
        }
    }

    public function sendKontakt($email, $template ,$config = []) {
        $config = array_merge($this->Mail->config($this->profile), $config);
        if ($this->Mail->config('send') == null) {
            $this->Mail->config('send', $config);
        }
        $this->Mail->profile('send')->to(Configure::read('mail.domyslny_email'))->replyTo($email)->subject(__('Formularz kontaktowy'));
        try {
            return $this->Mail->send($template);
        } catch (Exception $exc) {
            $this->log($exc->getMessage(), LogLevel::INFO, 'email');
            return false;
        }
    }
    
    public function sendInfo($sendTo,$temat,$message,$config = []) {
        if(empty($message)) return false;
        $attachment=null;
        
        if(!empty($config['attachment'])){
         $attachment=$config['attachment'];
            unset($config['attachment']);
        }
        $config = array_merge($this->MailAdmin->config($this->profile2), $config);
        if ($this->MailAdmin->config('send') == null) {
            $this->MailAdmin->config('send', $config);
        }
        if(!empty($attachment)){
            $this->MailAdmin->attachments($attachment);
        }
        $this->MailAdmin->profile('send')->to($sendTo)->subject($temat);
        try {
            return $this->MailAdmin->send($message);
        } catch (Exception $exc) {
            $this->log($exc->getMessage(), LogLevel::INFO, 'email');
            return false;
        }
    }

}
