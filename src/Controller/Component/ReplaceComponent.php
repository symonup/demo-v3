<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Configure;

class ReplaceComponent extends Component {

    public $Number;
    public $Txt;
    public $components = ['Translation'];
    public $active_jezyk;
    public $words = [
        'minus',
        ['zero', 'jeden', 'dwa', 'trzy', 'cztery', 'pięć', 'sześć', 'siedem', 'osiem', 'dziewięć'],
        ['dziesięć', 'jedenaście', 'dwanaście', 'trzynaście', 'czternaście', 'piętnaście', 'szesnaście', 'siedemnaście', 'osiemnaście', 'dziewiętnaście'],
        ['dziesięć', 'dwadzieścia', 'trzydzieści', 'czterdzieści', 'pięćdziesiąt', 'sześćdziesiąt', 'siedemdziesiąt', 'osiemdziesiąt', 'dziewięćdziesiąt'],
        ['sto', 'dwieście', 'trzysta', 'czterysta', 'pięćset', 'sześćset', 'siedemset', 'osiemset', 'dziewięćset'],
        ['tysiąc', 'tysiące', 'tysięcy'],
        ['milion', 'miliony', 'milionów'],
        ['miliard', 'miliardy', 'miliardów'],
        ['bilion', 'biliony', 'bilionów'],
        ['biliard', 'biliardy', 'biliardów'],
        ['trylion', 'tryliony', 'trylionów'],
        ['tryliard', 'tryliardy', 'tryliardów'],
        ['kwadrylion', 'kwadryliony', 'kwadrylionów'],
        ['kwintylion', 'kwintyliony', 'kwintylionów'],
        ['sekstylion', 'sekstyliony', 'sekstylionów'],
        ['septylion', 'septyliony', 'septylionów'],
        ['oktylion', 'oktyliony', 'oktylionów'],
        ['nonylion', 'nonyliony', 'nonylionów'],
        ['decylion', 'decyliony', 'decylionów']
    ];

    public function __construct(\Cake\Controller\ComponentRegistry $registry, array $config = array()) {
        parent::__construct($registry, $config);
        $View = new \Cake\View\View;
        $this->Number = new \Cake\View\Helper\NumberHelper($View);
        $this->Txt = new \App\View\Helper\TxtHelper($View);
    }

    public function getTemplate($template, $values, $towary = [], $new = false) {
        $this->active_jezyk = Configure::read('active_jezyk');
        if ($new) {
            $replace = $values;
        } else {
            $replace = array(
                '[%odzyskiwanie-hasla-link%]' => !empty($values['odzyskiwanie-hasla-link']) ? $values['odzyskiwanie-hasla-link'] : '',
                '[%odzyskiwanie-hasla-url%]' => !empty($values['odzyskiwanie-hasla-url']) ? $values['odzyskiwanie-hasla-url'] : '',
                '[%adres-ip%]' => !empty($values['adres-ip']) ? $values['adres-ip'] : '',
                '[%newsletter-potwierdzenie-link%]' => !empty($values['newsletter-potwierdzenie-link']) ? $values['newsletter-potwierdzenie-link'] : '',
                '[%newsletter-potwierdzenie-url%]' => !empty($values['newsletter-potwierdzenie-url']) ? $values['newsletter-potwierdzenie-url'] : '',
                '[%newsletter-token%]' => !empty($values['newsletter-token']) ? $values['newsletter-token'] : '',
                '[%zamowienie-id%]' => !empty($values['zamowienie-id']) ? $values['zamowienie-id'] : '',
                '[%zamowienie-numer%]' => !empty($values['zamowienie-numer']) ? $values['zamowienie-numer'] : '',
                '[%zamowienie-imie%]' => !empty($values['zamowienie-imie']) ? $values['zamowienie-imie'] : '',
                '[%zamowienie-nazwisko%]' => !empty($values['zamowienie-nazwisko']) ? $values['zamowienie-nazwisko'] : '',
                '[%zamowienie-adres-wysylki%]' => !empty($values['zamowienie-adres-wysylki']) ? $values['zamowienie-adres-wysylki'] : '',
                '[%zamowienie-adres-faktury%]' => !empty($values['zamowienie-adres-faktury']) ? $values['zamowienie-adres-faktury'] : '',
                '[%zamowienie-email%]' => !empty($values['zamowienie-email']) ? $values['zamowienie-email'] : '',
                '[%zamowienie-firma%]' => !empty($values['zamowienie-firma']) ? $values['zamowienie-firma'] : '',
                '[%zamowienie-nip%]' => !empty($values['zamowienie-nip']) ? $values['zamowienie-nip'] : '',
                '[%zamowienie-telefon%]' => !empty($values['zamowienie-telefon']) ? $values['zamowienie-telefon'] : '',
                '[%zamowienie-platnosc-wysylka%]' => !empty($values['zamowienie-platnosc-wysylka']) ? $values['zamowienie-platnosc-wysylka'] : '',
                '[%zamowienie-wartosc-produktow%]' => !empty($values['zamowienie-wartosc-produktow']) ? $values['zamowienie-wartosc-produktow'] : '',
                '[%zamowienie-koszt-wysylki%]' => isset($values['zamowienie-koszt-wysylki']) ? $values['zamowienie-koszt-wysylki'] : '',
                '[%zamowienie-wartosc-razem%]' => !empty($values['zamowienie-wartosc-razem']) ? $values['zamowienie-wartosc-razem'] : '',
                '[%zamowienie-prowizja%]' => !empty($values['zamowienie-prowizja']) ? $this->Translation->get('koszyk.ProwizjaZaPłatność{0}', [$this->Txt->cena($values['zamowienie-prowizja'])]) : '',
                '[%zamowienie-uwagi%]' => !empty($values['zamowienie-uwagi']) ? nl2br($values['zamowienie-uwagi']) : '',
                '[%zamowienie-status%]' => !empty($values['zamowienie-status']) ? $values['zamowienie-status'] : '',
                '[%zamowienie-data%]' => !empty($values['zamowienie-data']) ? $values['zamowienie-data'] : '',
                '[%zamowienie-zlozone-platnosc-url%]' => !empty($values['zamowienie-zlozone-platnosc-url']) ? $values['zamowienie-zlozone-platnosc-url'] : '',
                '[%zamowienie-potwierdzenie-url%]' => !empty($values['zamowienie-potwierdzenie-url']) ? $values['zamowienie-potwierdzenie-url'] : '',
                '[%zamowienie-telefoniczne-uzupelnij-url%]' => !empty($values['zamowienie-telefoniczne-uzupelnij-url']) ? $values['zamowienie-telefoniczne-uzupelnij-url'] : '',
                '[%zamowienie-rabat%]' => '', //!empty($values['zamowienie-rabat']) ? $this->Translation->get('zamowienie.WTymRabat{0}', [round($values['zamowienie-rabat'], 3) . '%']) : '',
                '[%zamowienie-waluta%]' => !empty($values['zamowienie-waluta']) ? $values['zamowienie-waluta'] : '',
                '[%zamowienie-rabaty%]' => !empty($values['zamowienie-rabaty']) ? $values['zamowienie-rabaty'] : '',
                '[%zamowienie-bony%]' => !empty($values['zamowienie-bony']) ? $values['zamowienie-bony'] : '',
                '[%termin-wysylki%]' => !empty($values['termin-wysylki']) ? $values['termin-wysylki'] : '',
                '[%zamowienie-dane-klienta%]' => !empty($values['zamowienie-dane-klienta']) ? $values['zamowienie-dane-klienta'] : '',
                '[%zamowienie-opiekun-id%]' => !empty($values['zamowienie-opiekun-id']) ? $values['zamowienie-opiekun-id'] : '',
                '[%zamowienie-opiekun%]' => !empty($values['zamowienie-opiekun']) ? $values['zamowienie-opiekun'] : '',
                '[%uzytkownik-id%]' => !empty($values['uzytkownik-id']) ? $values['uzytkownik-id'] : '',
                '[%uzytkownik-imie%]' => !empty($values['uzytkownik-imie']) ? $values['uzytkownik-imie'] : '',
                '[%uzytkownik-nazwisko%]' => !empty($values['uzytkownik-nazwisko']) ? $values['uzytkownik-nazwisko'] : '',
                '[%uzytkownik-email%]' => !empty($values['uzytkownik-email']) ? $values['uzytkownik-email'] : '',
                '[%uzytkownik-firma%]' => !empty($values['uzytkownik-firma']) ? $values['uzytkownik-firma'] : '',
                '[%uzytkownik-nip%]' => !empty($values['uzytkownik-nip']) ? $values['uzytkownik-nip'] : '',
                '[%uzytkownik-telefon%]' => !empty($values['uzytkownik-telefon']) ? $values['uzytkownik-telefon'] : '',
                '[%uzytkownik-data-rejestracji%]' => !empty($values['uzytkownik-data-rejestracji']) ? $values['uzytkownik-data-rejestracji'] : '',
                '[%uzytkownik-ph%]' => !empty($values['uzytkownik-ph']) ? $values['uzytkownik-ph'] : '',
                '[%uzytkownik-ph-email%]' => !empty($values['uzytkownik-ph-email']) ? $values['uzytkownik-ph-email'] : '',
                '[%uzytkownik-potwierdz-url%]' => !empty($values['uzytkownik-potwierdz-url']) ? $values['uzytkownik-potwierdz-url'] : '',
                '[%zamowienie-platnosc-info%]' => !empty($values['zamowienie-platnosc-info']) ? $values['zamowienie-platnosc-info'] : '',
                '[%paczkomat-info%]' => !empty($values['paczkomat-info']) ? $values['paczkomat-info'] : '',
                '[%zamowienie-bonus%]' => !empty($values['zamowienie-bonus']) ? $values['zamowienie-bonus'] : '',
                '[%zamowienie-bonus-uzyty%]' => !empty($values['zamowienie-bonus-uzyty']) ? $values['zamowienie-bonus-uzyty'] : '',
                '[%zamowienie-preorder-info%]' => !empty($values['zamowienie-preorder-info']) ? $values['zamowienie-preorder-info'] : '',
                '[%zamowienie-punkt-odbioru%]' => !empty($values['zamowienie-punkt-odbioru']) ? $values['zamowienie-punkt-odbioru'] : '',
                '[%link-sledzenia%]' => !empty($values['link-sledzenia']) ? $values['link-sledzenia'] : '',
                '[%nr-listu-przewozowego%]' => !empty($values['nr-listu-przewozowego']) ? $values['nr-listu-przewozowego'] : '',
                '[%towary%]' => '',
                '[%/towary%]' => ''
            );
        }
        if (!empty($towary)) {
            $pos_1 = strpos($template, '[%towary%]');
            $pos_2 = strpos($template, '[%/towary%]');
            $ile_z = $pos_2 - $pos_1;
            $_towary = substr($template, $pos_1, $ile_z);
            $tr = explode('</tr>', $_towary);
            foreach ($tr as $key => $_tr) {
                $s_towar = strpos($_tr, '[%t-');
                $tr_towar = '';
                $_index_t = '';
                if ($s_towar) {
                    $tr_towar = $_tr;
                    $_index_t = $key;
                    break;
                }
            }
            $search_t = array(
                '[%t-lp%]',
                '[%t-nazwa%]',
                '[%t-ilosc%]',
                '[%t-kod%]',
                '[%t-cena%]',
                '[%t-wartosc%]',
                '[%t-jednostka%]',
            );
            $tr_tab_towar = '';
            $lp = 0;
            foreach ($towary as $towar) {
                $cena = $towar->cena_za_sztuke_brutto;
                $wartosc = $towar->cena_razem_brutto;
                if(!empty($towar->rabat)){
                    $wartosc = round($wartosc - ($wartosc * ($towar->rabat/100)),2);
                }
                $lp++;
                $replace_t = array(
                    $lp,
                    $towar->nazwa,
                    $towar->ilosc,
                    (!empty($towar->kod)?$towar->kod:''),
                    $this->Txt->cena($cena),
                    $this->Txt->cena($wartosc),
                    (!empty($towar->jednostka_str)?$towar->jednostka_str:''),
                );
                $tr_tab_towar .= str_replace($search_t, $replace_t, $tr_towar);
                if (count($towary) != $lp)
                    $tr_tab_towar .= '</tr>';
            }
            $tr[$_index_t] = $tr_tab_towar;
            $tr = join('</tr>', $tr);
            $template = str_replace($_towary, $tr, $template);
        }
        return str_replace(array_keys($replace), $replace, $template);
    }
 public function getTemplateFaktura($template, $values, $towary = []) {
        $this->active_jezyk = Configure::read('active_jezyk');
        $replace = $values;
        $replace['[%towary%]']='';
        $replace['[%/towary%]']='';
        if (!empty($towary)) {
            $pos_1 = strpos($template, '[%towary%]');
            $pos_2 = strpos($template, '[%/towary%]');
            $ile_z = $pos_2 - $pos_1;
            $_towary = substr($template, $pos_1, $ile_z);
            $tr = explode('</tr>', $_towary);
            foreach ($tr as $key => $_tr) {
                $s_towar = strpos($_tr, '[%t-');
                $tr_towar = '';
                $_index_t = '';
                if ($s_towar) {
                    $tr_towar = $_tr;
                    $_index_t = $key;
                    break;
                }
            }
            $search_t = array(
                '[%t-lp%]',
                '[%t-nazwa%]',
//                '[%t-pkwiu%]',
                '[%t-ilosc%]',
                '[%t-jednostka%]',
                '[%t-cena-brutto%]',
                '[%t-vat%]',
                '[%t-wartosc-netto%]',
                '[%t-wartosc-vat%]',
                '[%t-wartosc-brutto%]'
            );
            $tr_tab_towar = '';
            $lp = 0;
            foreach ($towary as $towar) {
                $replace_t = array(
                    $towar->lp,
                    $towar->nazwa,
                    $towar->ilosc_jednostka,
                    $towar->jednostka,
                    $this->Txt->cena($towar->cena_brutto),
                    $towar->stawka_vat,
                    $this->Txt->cena($towar->wartosc_netto),
                    $this->Txt->cena($towar->wartosc_vat),
                    $this->Txt->cena($towar->wartosc_brutto)
                );
                $tr_tab_towar .= str_replace($search_t, $replace_t, $tr_towar);
                if (count($towary) != $lp)
                    $tr_tab_towar .= '</tr>';
            }
            $tr[$_index_t] = $tr_tab_towar;
            $tr = join('</tr>', $tr);
            $template = str_replace($_towary, $tr, $template);
        }
        return str_replace(array_keys($replace), $replace, $template);
    }
    public function getNewsletterTemplate($template, $values) {
        $replace = array(
            '[%data%]' => !empty($values['[%data%]']) ? $values['[%data%]'] : '',
            '[%url%]' => !empty($values['[%url%]']) ? $values['[%url%]'] : '',
            '[%token%]' => !empty($values['[%token%]']) ? $values['[%token%]'] : ''
        );
        return str_replace(array_keys($replace), $replace, $template);
    }

    public function truncate($text, $length = 100, $ending = '...', $exact = true, $considerHtml = false) {
        if (is_array($ending)) {
            extract($ending);
        }
        if ($considerHtml) {
            if (mb_strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
                return $text;
            }
            $totalLength = mb_strlen($ending);
            $openTags = array();
            $truncate = '';
            preg_match_all('/(<\/?([\w+]+)[^>]*>)?([^<>]*)/', $text, $tags, PREG_SET_ORDER);
            foreach ($tags as $tag) {
                if (!preg_match('/img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param/s', $tag[2])) {
                    if (preg_match('/<[\w]+[^>]*>/s', $tag[0])) {
                        array_unshift($openTags, $tag[2]);
                    } else if (preg_match('/<\/([\w]+)[^>]*>/s', $tag[0], $closeTag)) {
                        $pos = array_search($closeTag[1], $openTags);
                        if ($pos !== false) {
                            array_splice($openTags, $pos, 1);
                        }
                    }
                }
                $truncate .= $tag[1];

                $contentLength = mb_strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', ' ', $tag[3]));
                if ($contentLength + $totalLength > $length) {
                    $left = $length - $totalLength;
                    $entitiesLength = 0;
                    if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', $tag[3], $entities, PREG_OFFSET_CAPTURE)) {
                        foreach ($entities[0] as $entity) {
                            if ($entity[1] + 1 - $entitiesLength <= $left) {
                                $left--;
                                $entitiesLength += mb_strlen($entity[0]);
                            } else {
                                break;
                            }
                        }
                    }

                    $truncate .= mb_substr($tag[3], 0, $left + $entitiesLength);
                    break;
                } else {
                    $truncate .= $tag[3];
                    $totalLength += $contentLength;
                }
                if ($totalLength >= $length) {
                    break;
                }
            }
        } else {
            if (mb_strlen($text) <= $length) {
                return $text;
            } else {
                $truncate = mb_substr($text, 0, $length - strlen($ending));
            }
        }
        if (!$exact) {
            $spacepos = mb_strrpos($truncate, ' ');
            if (isset($spacepos)) {
                if ($considerHtml) {
                    $bits = mb_substr($truncate, $spacepos);
                    preg_match_all('/<\/([a-z]+)>/', $bits, $droppedTags, PREG_SET_ORDER);
                    if (!empty($droppedTags)) {
                        foreach ($droppedTags as $closingTag) {
                            if (!in_array($closingTag[1], $openTags)) {
                                array_unshift($openTags, $closingTag[1]);
                            }
                        }
                    }
                }
                $truncate = mb_substr($truncate, 0, $spacepos);
            }
        }

        $truncate .= $ending;

        if ($considerHtml) {
            foreach ($openTags as $tag) {
                $truncate .= '</' . $tag . '>';
            }
        }

        return $truncate;
    }

    public function szablonAllegro($towar, $template) {
        $search = array(
            '[%nazwa]',
            '[%opis]',
            '[%opis2]',
            '[%zdjecie_1]',
            '[%zdjecie_2]',
            '[%zdjecie_3]',
            '[%zdjecie_4]',
            '[%zdjecie_5]',
            '[%kod]'
        );



        $replace = array(
            !empty($towar->nazwa) ? $towar->nazwa : '',
            !empty($towar->opis) ? $towar->opis : '',
            !empty($towar->opis2) ? $towar->opis2 : '',
            !empty($towar->towar_zdjecie[0]->plik) ? '<img class="productImg" src="http://' . $_SERVER['SERVER_NAME'] . '/files/images/thumb_3_' . $towar->towar_zdjecie[0]->plik . '"/>' : '',
            !empty($towar->towar_zdjecie[1]->plik) ? '<img class="productImg" src="http://' . $_SERVER['SERVER_NAME'] . '/files/images/thumb_3_' . $towar->towar_zdjecie[1]->plik . '"/>' : '',
            !empty($towar->towar_zdjecie[2]->plik) ? '<img class="productImg" src="http://' . $_SERVER['SERVER_NAME'] . '/files/images/thumb_3_' . $towar->towar_zdjecie[2]->plik . '"/>' : '',
            !empty($towar->towar_zdjecie[3]->plik) ? '<img class="productImg" src="http://' . $_SERVER['SERVER_NAME'] . '/files/images/thumb_3_' . $towar->towar_zdjecie[3]->plik . '"/>' : '',
            !empty($towar->towar_zdjecie[4]->plik) ? '<img class="productImg" src="http://' . $_SERVER['SERVER_NAME'] . '/files/images/thumb_3_' . $towar->towar_zdjecie[4]->plik . '"/>' : '',
            !empty($towar->kod) ? $towar->kod : '',
        );

        $template = str_replace($search, $replace, $template);
        return $template;
    }

    public function szablonFaktura($template, $order = null) {
        $this->active_jezyk = Configure::read('active_jezyk');
        $oplacone = $order->status == 'zrealizowane' || $order->status == 'oplacone';
        $razem = $this->Txt->cena($order->wartosc_razem);
        $nabywca = '';
        if (!empty($order->faktura_imie) || !empty($order->faktura_nazwisko)) {
            $nabywca = trim($order->faktura_imie . ' ' . $order->faktura_nazwisko);
        }
        if (!empty($order->faktura_firma)) {
            if (!empty($nabywca)) {
                $nabywca .= '<br/>';
            }
            $nabywca .= $order->faktura_firma;
        }
        if (!empty($order->faktura_ulica)) {
            if (!empty($nabywca)) {
                $nabywca .= '<br/>';
            }
            $nabywca .= $order->faktura_ulica;
            if (!empty($order->faktura_nr_domu)) {
                $nabywca .= ' ' . $order->faktura_nr_domu;
                if (!empty($order->faktura_nr_lokalu)) {
                    $nabywca .= '/' . $order->faktura_nr_lokalu;
                }
            }
        }
        if (!empty($order->faktura_miasto)) {
            if (!empty($nabywca)) {
                $nabywca .= '<br/>';
            }
            if (!empty($order->faktura_kod)) {
                $nabywca .= $order->faktura_kod . ' ';
            }
            $nabywca .= $order->faktura_miasto;
        }

        if (empty($nabywca)) {
            if (!empty($order->imie) || !empty($order->nazwisko)) {
                $nabywca = trim($order->imie . ' ' . $order->nazwisko);
            }
            if (!empty($order->firma)) {
                if (!empty($nabywca)) {
                    $nabywca .= '<br/>';
                }
                $nabywca .= $order->firma;
            }
            if (!empty($order->ulica)) {
                if (!empty($nabywca)) {
                    $nabywca .= '<br/>';
                }
                $nabywca .= $order->ulica;
                if (!empty($order->nr_domu)) {
                    $nabywca .= ' ' . $order->nr_domu;
                    if (!empty($order->nr_lokalu)) {
                        $nabywca .= '/' . $order->nr_lokalu;
                    }
                }
            }
            if (!empty($order->miasto)) {
                if (!empty($nabywca)) {
                    $nabywca .= '<br/>';
                }
                if (!empty($order->kod)) {
                    $nabywca .= $order->kod . ' ';
                }
                $nabywca .= $order->miasto;
            }
        }

        $towary_zwolnione = false;
        $termin_platnosci = Configure::read('Faktura.termin_platnosci');
        if (empty($termin_platnosci)) {
            $termin_platnosci = 7;
        }

        $replace = [
            '[%miejscowosc%]' => Configure::read('Faktura.miejscowosc'),
            '[%data-wystawienia%]' => !empty($order->data) ? $order->data->format('Y-m-d') : '',
            '[%sprzedawca%]' => Configure::read('Faktura.sprzedawca'),
            '[%nabywca%]' => $nabywca,
            '[%nip-sprzedawca%]' => Configure::read('Faktura.nip'),
            '[%nip-nabywca%]' => !empty($order->faktura_nip) ? $order->faktura_nip : '',
            '[%nr-faktury%]' => !empty($order->id) ? $order->id : '',
            '[%gotowka%]' => (!empty($order->rodzaj_platnosci) && !empty($order->rodzaj_platnosci->pobranie)) ? 'X' : '',
            '[%przelew%]' => (!empty($order->rodzaj_platnosci) && empty($order->rodzaj_platnosci->pobranie)) ? 'X' : '',
            '[%termin-platnosci%]' => !empty($order->data) ? $order->data->addDays($termin_platnosci)->format('Y-m-d') : '',
            '[%nazwa-banku%]' => Configure::read('Faktura.nazwa_banku'),
            '[%nr-konta%]' => Configure::read('Faktura.nr_konta'),
            '[%wartosc%]' => $razem,
            '[%wartosc-currency%]' => $this->Txt->cena($order->wartosc_razem),
            '[%zaplacono%]' => $oplacone ? $razem : 0,
            '[%do-zaplaty%]' => !$oplacone ? $razem : 0,
            '[%slownie%]' => $this->amountToWords($order->wartosc_razem),
            '[%uwagi-tekst%]' => '',
            '[%uwagi%]' => '',
            '[%towary%]' => '',
            '[%/towary%]' => '',
            '[%t-lp%]' => '',
            '[%t-nazwa%]' => '',
            '[%t-ilosc%]' => '',
            '[%t-cena%]' => '',
            '[%t-wartosc%]' => '',
            '[%t-jednostka%]' => '',
            '<p>' => '',
            '</p>' => '',
        ];

        if (!empty($order->zamowienie_towar)) {
            $pos_1 = strpos($template, '[%towary%]');
            $pos_2 = strpos($template, '[%/towary%]');
            $ile_z = $pos_2 - $pos_1;
            $_towary = substr($template, $pos_1, $ile_z);
            $tr = explode('</tr>', $_towary);
            foreach ($tr as $key => $_tr) {
                $s_towar = strpos($_tr, '[%t-');
                $tr_towar = '';
                $_index_t = '';
                if ($s_towar) {
                    $tr_towar = $_tr;
                    $_index_t = $key;
                    break;
                }
            }
            $search_t = [
                '[%t-lp%]',
                '[%t-nazwa%]',
                '[%t-ilosc%]',
                '[%t-cena%]',
                '[%t-wartosc%]',
                '[%t-jednostka%]'
            ];
            $tr_tab_towar = '';
            $lp = 0;
            $zamowienie_count = count($order->zamowienie_towar) + 1;
            foreach ($order->zamowienie_towar as $towar) {
                if (!empty($towar->towar->towar_cena->vat->zwolniony)) {
                    $towary_zwolnione = true;
                }
                $cena = $towar->cena;
                $wartosc = $towar->wartosc_brutto;
                if (!empty($towar->zestaw_rabat)) {
                    $cena = round($cena - (($cena * $towar->zestaw_rabat) / 100), 2);
                    $wartosc = round($wartosc - (($wartosc * $towar->zestaw_rabat) / 100), 2);
                }
                $lp++;
                $replace_t = [
                    $lp,
                    $towar->nazwa,
                    $towar->ilosc,
                    $this->Txt->cena(round($cena, 3)),
                    $this->Txt->cena(round($wartosc, 3)),
                    $towar->jednostka
                ];
                $tr_tab_towar .= str_replace($search_t, $replace_t, $tr_towar);
                if ($zamowienie_count != $lp) {
                    $tr_tab_towar .= '</tr>';
                }
            }

            $replace_t = [
                $lp + 1,
                $order->platnosc_wysylka,
                1,
                $this->Txt->cena($order->koszt_przesylki),
                $this->Txt->cena($order->koszt_przesylki),
                'szt.'
            ];
            $tr_tab_towar .= str_replace($search_t, $replace_t, $tr_towar);

            $tr[$_index_t] = $tr_tab_towar;
            $tr = join('</tr>', $tr);
            $template = str_replace($_towary, $tr, $template);
        }

        if ($towary_zwolnione) {
            $replace['[%uwagi-tekst%]'] = 'Uwagi:';
            $replace['[%uwagi%]'] = Configure::read('Faktura.zwolniony_z_vat');
        }

        return str_replace(array_keys($replace), $replace, $template);
    }

    /**
     * Podaje słowną wartość liczby całkowitej (równierz podaną w postaci stringa)
     *
     * @param integer $int
     * @return string
     */
    private function integerNumberToWords($int) {
        $int = strval($int);
        $in = preg_replace('/[^-\d]+/', '', $int);

        $return = '';

        if ($in{0} == '-') {
            $in = substr($in, 1);
            $return = $this->words[0] . ' ';
        }

        $txt = str_split(strrev($in), 3);

        if ($in == 0) {
            $return = $this->words[1][0] . ' ';
        }

        for ($i = count($txt) - 1; $i >= 0; $i--) {
            $number = (int) strrev($txt[$i]);

            if ($number > 0) {
                if ($i == 0) {
                    $return .= $this->number($number) . ' ';
                } else {
                    $return .= ($number > 1 ? $this->number($number) . ' ' : '')
                            . $this->inflection($this->words[4 + $i], $number) . ' ';
                }
            }
        }

        return $this->clear($return);
    }

    /**
     * Podaje słowną wartość kwoty wraz z wartościami po kropce.
     * Nie przyjmuje wartości przedzielonych przecinkami (jako wartości nie numerycznych).
     *
     * @param integer|string $amount
     * @param string $currencyName
     * @param string $centName
     * @return string
     * @throws \Exception
     */
    private function amountToWords($amount, $currencyName = 'zł', $centName = 'gr') {
        if (!is_numeric($amount)) {
            throw new \Exception('Nieprawidłowa kwota');
        }

        $amountString = number_format($amount, 2, '.', '');
        list($bigAmount, $smallAmount) = explode('.', $amountString);

        $bigAmount = $this->integerNumberToWords($bigAmount) . ' ' . $currencyName . ' ';
        $smallAmount = $this->integerNumberToWords($smallAmount) . ' ' . $centName;

        return $this->clear($bigAmount . $smallAmount);
    }

    /**
     * Czyści podwójne spacje i trimuje
     *
     * @param $string
     * @return mixed
     */
    private function clear($string) {
        return preg_replace('!\s+!', ' ', trim($string));
    }

    /**
     * $inflections = Array('jeden','dwa','pięć')
     *
     * @param string[] $inflections
     * @param $int
     * @return mixed
     */
    private function inflection(array $inflections, $int) {
        $txt = $inflections[2];

        if ($int == 1) {
            $txt = $inflections[0];
        }

        $units = intval(substr($int, -1));

        $rest = $int % 100;

        if (($units > 1 && $units < 5) & !($rest > 10 && $rest < 20)) {
            $txt = $inflections[1];
        }

        return $txt;
    }

    /**
     * Odmiana dla liczb < 1000
     *
     * @param integer $int
     * @return string
     */
    private function number($int) {
        $return = '';

        $j = abs(intval($int));

        if ($j == 0) {
            return $this->words[1][0];
        }

        $units = $j % 10;
        $dozens = intval(($j % 100 - $units) / 10);
        $hundreds = intval(($j - $dozens * 10 - $units) / 100);

        if ($hundreds > 0) {
            $return .= $this->words[4][$hundreds - 1] . ' ';
        }

        if ($dozens > 0) {
            if ($dozens == 1) {
                $return .= $this->words[2][$units] . ' ';
            } else {
                $return .= $this->words[3][$dozens - 1] . ' ';
            }
        }

        if ($units > 0 && $dozens != 1) {
            $return .= $this->words[1][$units] . ' ';
        }

        return $return;
    }

    public function getReplaceOrderData($zamowienie) {
        $rabatyInfo = [];
        if (!empty($zamowienie->rabat_zestaw)) {
            $rabatyInfo[] = $this->Txt->Html->tag('div', $this->Translation->get('zamowienie.rabatZaZestaw{0}', [$this->Txt->cena($zamowienie->rabat_zestaw)]));
        }
        if (!empty($zamowienie->kod_rabatowy) && !empty($zamowienie->kod_rabatowy_wartosc)) {
            if ($zamowienie->kod_rabatowy_typ == 'kwota') {
                $rabatyInfo[] = $this->Txt->Html->tag('div', $this->Translation->get('zamowienie.kodRabatowy{0}{1}', [$zamowienie->kod_rabatowy, $this->Txt->cena($zamowienie->kod_rabatowy_wartosc)]));
            } else {
                $rabatyInfo[] = $this->Txt->Html->tag('div', $this->Translation->get('zamowienie.kodRabatowy{0}{1}', [$zamowienie->kod_rabatowy, $zamowienie->kod_rabatowy_wartosc . '%']));
            }
        }
        $confirmUrl = \Cake\Routing\Router::url(['controller' => 'Cart', 'action' => 'confirm', $zamowienie->confirm_token, '_port' => false], true);
        $checkUrl = parse_url($confirmUrl);
        if (!empty($checkUrl['port'])) {
            $confirmUrl = str_replace(':' . $checkUrl['port'], '', $checkUrl);
        }
        $paymentUrl = \Cake\Routing\Router::url(['controller' => 'Cart', 'action' => 'orderCompleted', $zamowienie->token, '_port' => false], true);
        $checkUrl = parse_url($paymentUrl);
        if (!empty($checkUrl['port'])) {
            $paymentUrl = str_replace(':' . $checkUrl['port'], '', $paymentUrl);
        }
        $paymentInfo = '';
        if (!empty($zamowienie->rodzaj_platnosci)) {
            $platnoscData = $zamowienie->rodzaj_platnosci->toArray();
            $platnoscData['nr_zamowienia'] = (!empty($zamowienie->numer) ? $zamowienie->numer : $zamowienie->id);
            $platnoscData['kwota'] = $this->Txt->cena($zamowienie->wartosc_razem);
            $platnoscData['platnosc_url'] = $paymentUrl;
            if (!empty($zamowienie->wysylka)) {
                foreach ($zamowienie->wysylka->toArray() as $wysylkaField => $wysylka) {
                    $platnoscData['wysylka_' . $wysylkaField] = $wysylka;
                }
            }
            $platnoscData['nazwa'] = str_replace('{termin}', '', $platnoscData['nazwa']);
            $paymentInfo = $this->Translation->get('platnosci_' . $zamowienie->rodzaj_platnosci->id . '.SposobPlatnosciInfoEmail', $platnoscData, true);
        }
        $wysylkaLongKoszt = $this->Txt->cena($zamowienie->wysylka_long_koszt);
        $wysylkaKoszt = $this->Txt->cena($zamowienie->wysylka_koszt);
        $wysylkaKosztTotal = $this->Txt->cena($zamowienie->wysylka_koszt+$zamowienie->wysylka_long_koszt);
        $preorder=false;
        if(!empty($zamowienie->zamowienie_towar)){
            foreach ($zamowienie->zamowienie_towar as $towarItem){
                if(!empty($towarItem->preorder)){
                    $preorder=true;
                }
            }
        }
        if($preorder){
            $preorder=$this->Translation->get('emailTemplate.preorderInfo',[],true);
        }else{
            $preorder='';
        }
        $punktOdbioru='';
        if(!empty($zamowienie->punkty_odbioru_id)){
            if(!empty($zamowienie->punkty_odbioru)){
                $punktOdbioru= $this->Translation->get('emailTemplate.punktOdbioru',['adres'=>nl2br($zamowienie->punkty_odbioru->adres)],true);
            }else{
                $punktOdbioru= $this->Translation->get('emailTemplate.punktOdbioru',['adres'=>nl2br($zamowienie->punkty_odbioru_info)],true);
            }
        }
        return [
            'zamowienie-id' => $zamowienie->id,
            'zamowienie-numer' => (!empty($zamowienie->numer) ? $zamowienie->numer : $zamowienie->id),
            'zamowienie-imie' => $zamowienie->imie,
            'zamowienie-nazwisko' => $zamowienie->nazwisko,
            'zamowienie-adres-wysylki' => (!empty($punktOdbioru)?'':'<b>' . $this->Translation->get('labels.daneDoWysylki') . '</b>:<br/>' . $this->Txt->printUserInfo($zamowienie->toArray(), '<br/>', 'wysylka_') . '<br/>' . $zamowienie->adres_wysylki),
            'zamowienie-adres-faktury' => (!empty($zamowienie->faktura) ? '<b>' . $this->Translation->get('labels.daneDoFaktury') . '</b>:<br/>' . $this->Txt->printUserInfo($zamowienie->toArray(), '<br/>', 'faktura_') . '<br/>' . $zamowienie->adres_faktury : ''),
            'zamowienie-dane-klienta' => '<b>' . $this->Translation->get('labels.daneKlienta') . '</b>:<br/>' . $this->Txt->printUserInfo($zamowienie->toArray(), '<br/>') . '<br/>' . $this->Txt->printAddres($zamowienie->toArray(), '<br/>'),
            'zamowienie-email' => $zamowienie->email,
            'zamowienie-nazwa' => $zamowienie->nazwa,
            'zamowienie-nip' => $zamowienie->nip,
            'zamowienie-telefon' => $zamowienie->telefon,
            'zamowienie-wartosc-razem' => $this->Txt->cena($zamowienie->wartosc_razem),
            'zamowienie-wartosc-produktow' => $this->Txt->cena($zamowienie->wartosc_produktow),
            'zamowienie-koszt-wysylki' => $wysylkaKosztTotal,
            'zamowienie-koszt-wysylki-expres' => $wysylkaKoszt,
            'zamowienie-koszt-wysylki-long' => $wysylkaLongKoszt,
            'zamowienie-platnosc-wysylka' => $zamowienie->platnosc_wysylka,
            'zamowienie-waluta' => $zamowienie->waluta_symbol,
            'zamowienie-uwagi' => nl2br($zamowienie->uwagi),
            'zamowienie-status' => $this->Translation->get('statusy_zamowien.' . $zamowienie->status),
            'zamowienie-data' => (!empty($zamowienie->data) ? $zamowienie->data->format('Y-m-d H:i') : ''),
            'zamowienie-token' => $zamowienie->token,
            'zamowienie-rabat' => $zamowienie->rabat_wartosc,
            'zamowienie-rabat-zestaw' => $zamowienie->rabat_zestaw,
            'zamowienie-kod-rabatowy' => $zamowienie->kod_rabatowy,
            'zamowienie-kod-rabatowy-typ' => $zamowienie->kod_rabatowy_typ,
            'zamowienie-kod-rabatowy-wartosc' => $zamowienie->kod_rabatowy_wartosc,
            'zamowienie-rabaty' => (!empty($rabatyInfo) ? join('', $rabatyInfo) : ''),
            'zamowienie-confirm-url' => $confirmUrl,
            'zamowienie-zlozone-platnosc-url' => $paymentUrl,
            'zamowienie-platnosc-info' => $paymentInfo,
            'paczkomat-info' => (!empty($zamowienie->paczkomat) ? $this->Translation->get('emailTemplate.adresPaczkomatu{0}', [$zamowienie->paczkomat_info], true) : ''),
            'zamowienie-bonus' => (!empty($zamowienie->bonus_suma) ? $this->Translation->get('emailTemplate.bonusZdobyty{0}', [$this->Txt->cena($zamowienie->bonus_suma, $zamowienie->waluta_symbol, true)]) : ''),
            'zamowienie-bonus-uzyty' => (!empty($zamowienie->bonus_uzyty) ? $this->Translation->get('emailTemplate.bonusUzyty{0}', [$this->Txt->cena($zamowienie->bonus_uzyty, $zamowienie->waluta_symbol, true)]) : ''),
            'zamowienie-preorder-info' => $preorder,
            'zamowienie-punkt-odbioru' => $punktOdbioru,
            'adres-ip' => $this->request->clientIp(),
                'link-sledzenia' => $this->Txt->kurierLink($zamowienie->kurier_typ, $zamowienie->nr_listu_przewozowego),
                'nr-listu-przewozowego' => !empty($zamowienie->nr_listu_przewozowego) ? $zamowienie->nr_listu_przewozowego : '',
        ];
    }

}
