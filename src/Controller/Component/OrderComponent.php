<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Configure;
use Psr\Log\LogLevel;
use Exception;
use PHPExcel;

class OrderComponent extends Component {

    var $components = ['Translation'];
    var $Txt;
    public function initialize(array $config) {
        parent::initialize($config);
        $View=new \Cake\View\View();
        $this->Txt=new \App\View\Helper\TxtHelper($View);
    }
    public function excel($zamowienie,$filePath=''){
        if (empty($zamowienie)) {
            return false;
        }
        $cols = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
        $excel = new \PHPExcel();

        $excel->getActiveSheet()->getColumnDimension($cols[1])->setAutoSize(false);
        $excel->getActiveSheet()->getColumnDimension($cols[1])->setWidth(50);
        $excel->getActiveSheet()->getColumnDimension($cols[2])->setAutoSize(false);
        $excel->getActiveSheet()->getColumnDimension($cols[2])->setWidth(25);
        
        $excel->getActiveSheet()->mergeCellsByColumnAndRow(0, 1, 1, 1);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, $this->Txt->printAdmin(__('Excel | Nr zamówienia:'), 'Excel |'));
        $excel->getActiveSheet()->getStyleByColumnAndRow(0, 1)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $excel->getActiveSheet()->mergeCellsByColumnAndRow(2, 1, 3, 1);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(2, 1, (!empty($zamowienie->numer)?$zamowienie->numer:$zamowienie->id));
        $excel->getActiveSheet()->getStyleByColumnAndRow(2, 1)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

        $excel->getActiveSheet()->mergeCellsByColumnAndRow(0, 2, 1, 2);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, $this->Txt->printAdmin(__('Excel | Data zamówienia:'), 'Excel |'));
        $excel->getActiveSheet()->getStyleByColumnAndRow(0, 2)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $excel->getActiveSheet()->mergeCellsByColumnAndRow(2, 2, 3, 2);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, $zamowienie->data->format('Y-m-d H:i:s'));
        $excel->getActiveSheet()->getStyleByColumnAndRow(2, 2)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

        $excel->getActiveSheet()->mergeCellsByColumnAndRow(0, 3, 1, 3);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 3, $this->Txt->printAdmin(__('Excel | Waluta:'), 'Excel |'));
        $excel->getActiveSheet()->getStyleByColumnAndRow(0, 3)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $excel->getActiveSheet()->mergeCellsByColumnAndRow(2, 3, 3, 3);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(2, 3, $zamowienie->waluta_symbol);
        $excel->getActiveSheet()->getStyleByColumnAndRow(2, 3)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

        $excel->getActiveSheet()->mergeCellsByColumnAndRow(4, 1, 7, 1);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(4, 1, $this->Txt->printAdmin(__('Excel | Adres do wysyłki:'), 'Excel |'));
        $excel->getActiveSheet()->getStyleByColumnAndRow(4, 1)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $excel->getActiveSheet()->mergeCellsByColumnAndRow(4, 2, 7, 8);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(4, 2, $this->Txt->printUserInfo($zamowienie->toArray(), "\n", 'wysylka_') . "\n" . $this->Txt->printAddres($zamowienie->toArray(), "\n", 'wysylka_'));
        $excel->getActiveSheet()->getStyleByColumnAndRow(4, 2)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_TOP);

        $excel->getActiveSheet()->mergeCellsByColumnAndRow(8, 1, 11, 1);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(8, 1, $this->Txt->printAdmin(__('Excel | Adres do faktury:'), 'Excel |'));
        $excel->getActiveSheet()->getStyleByColumnAndRow(8, 1)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $excel->getActiveSheet()->mergeCellsByColumnAndRow(8, 2, 11, 8);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(8, 2, $this->Txt->printUserInfo($zamowienie->toArray(), "\n", 'faktura_') . "\n" . $this->Txt->printAddres($zamowienie->toArray(), "\n", 'faktura_'));
        $excel->getActiveSheet()->getStyleByColumnAndRow(8, 2)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_TOP);

        $excel->getActiveSheet()->mergeCellsByColumnAndRow(0, 12, 11, 12);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 12, $this->Txt->printAdmin(__('Excel | Zamówione produkty:'), 'Excel |'));
        $excel->getActiveSheet()->getStyleByColumnAndRow(0, 12)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $excel->getActiveSheet()->getStyleByColumnAndRow(0, 12)->getFont()->setBold(true);

        $startRow = 13;
        $col = 0;
        $row = $startRow + 1;
        $maxCol = 0;
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | lp'), 'Excel |'));
            $excel->getActiveSheet()->getStyleByColumnAndRow(($maxCol - 1), $startRow)->getAlignment()->setWrapText(true);
        $excel->getActiveSheet()->getStyleByColumnAndRow(($maxCol - 1), $startRow)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Product title'), 'Excel |'));
            $excel->getActiveSheet()->getStyleByColumnAndRow(($maxCol - 1), $startRow)->getAlignment()->setWrapText(true);
        $excel->getActiveSheet()->getStyleByColumnAndRow(($maxCol - 1), $startRow)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Index'), 'Excel |'));
            $excel->getActiveSheet()->getStyleByColumnAndRow(($maxCol - 1), $startRow)->getAlignment()->setWrapText(true);
        $excel->getActiveSheet()->getStyleByColumnAndRow(($maxCol - 1), $startRow)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Ordered Pieces'), 'Excel |'));
            $excel->getActiveSheet()->getStyleByColumnAndRow(($maxCol - 1), $startRow)->getAlignment()->setWrapText(true);
        $excel->getActiveSheet()->getStyleByColumnAndRow(($maxCol - 1), $startRow)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Price per Piece after discount'), 'Excel |'));
            $excel->getActiveSheet()->getStyleByColumnAndRow(($maxCol - 1), $startRow)->getAlignment()->setWrapText(true);
        $excel->getActiveSheet()->getStyleByColumnAndRow(($maxCol - 1), $startRow)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | j.m.'), 'Excel |'));
            $excel->getActiveSheet()->getStyleByColumnAndRow(($maxCol - 1), $startRow)->getAlignment()->setWrapText(true);
        $excel->getActiveSheet()->getStyleByColumnAndRow(($maxCol - 1), $startRow)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Pieces per carton'), 'Excel |'));
            $excel->getActiveSheet()->getStyleByColumnAndRow(($maxCol - 1), $startRow)->getAlignment()->setWrapText(true);
        $excel->getActiveSheet()->getStyleByColumnAndRow(($maxCol - 1), $startRow)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel |  vat [%]'), 'Excel |'));
            $excel->getActiveSheet()->getStyleByColumnAndRow(($maxCol - 1), $startRow)->getAlignment()->setWrapText(true);
        $excel->getActiveSheet()->getStyleByColumnAndRow(($maxCol - 1), $startRow)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Ordered Cartons'), 'Excel |'));
            $excel->getActiveSheet()->getStyleByColumnAndRow(($maxCol - 1), $startRow)->getAlignment()->setWrapText(true);
        $excel->getActiveSheet()->getStyleByColumnAndRow(($maxCol - 1), $startRow)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Price per piece'), 'Excel |'));
            $excel->getActiveSheet()->getStyleByColumnAndRow(($maxCol - 1), $startRow)->getAlignment()->setWrapText(true);
        $excel->getActiveSheet()->getStyleByColumnAndRow(($maxCol - 1), $startRow)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Discount [%]'), 'Excel |'));
            $excel->getActiveSheet()->getStyleByColumnAndRow(($maxCol - 1), $startRow)->getAlignment()->setWrapText(true);
        $excel->getActiveSheet()->getStyleByColumnAndRow(($maxCol - 1), $startRow)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol, $startRow, $this->Txt->printAdmin(__('Excel | Line TOTAL after discount'), 'Excel |'));
            $excel->getActiveSheet()->getStyleByColumnAndRow($maxCol, $startRow)->getAlignment()->setWrapText(true);
        $excel->getActiveSheet()->getStyleByColumnAndRow($maxCol, $startRow)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$lp=1;
        foreach ($zamowienie->zamowienie_towar as $item) {
            $priceDef=(!empty($item->cena_podstawowa)?$item->cena_podstawowa:$item->cena_za_sztuke_brutto);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $lp++);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $item->nazwa);
            $excel->getActiveSheet()->getStyleByColumnAndRow(($col - 1), $row)->getAlignment()->setWrapText(true);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $item->kod);
            $excel->getActiveSheet()->getStyleByColumnAndRow(($col - 1), $row)->getAlignment()->setWrapText(true);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=' . $cols[6] . $row . '*' . $cols[8] . $row);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $item->cena_z_rabatem);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $item->jednostka_str);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $item->ilosc_w_kartonie);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $item->vat_stawka);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $item->ilosc);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $priceDef);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, (!empty($item->rabat)?$item->rabat:0));
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=' . $cols[3] . $row . '*' . $cols[4] . $row);

            //-------------------------------//
            $col = 0;
            $row++;
        }
        $fileName = 'order_' . (!empty($zamowienie->numer)?$zamowienie->numer:$zamowienie->id) . '.xls';

        $excel->getActiveSheet()->mergeCellsByColumnAndRow(0, 4, 1, 4);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 4, $this->Txt->printAdmin(__('Excel | Wartość zamówienia'), 'Excel |'));
        $excel->getActiveSheet()->getStyleByColumnAndRow(0, 4)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        $excel->getActiveSheet()->mergeCellsByColumnAndRow(2, 4, 3, 4);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(2, 4, '=' . $cols[11] . $row);
        $excel->getActiveSheet()->getStyleByColumnAndRow(2, 4)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        
        
        $excel->getActiveSheet()->mergeCellsByColumnAndRow(0, 5, 1, 5);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 5, $this->Txt->printAdmin(__('Excel | Opiekun handlowy'), 'Excel |'));
        $excel->getActiveSheet()->getStyleByColumnAndRow(0, 5)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        $excel->getActiveSheet()->mergeCellsByColumnAndRow(2, 5, 3, 5);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(2, 5, (!empty($zamowienie->administrator_dane)?$zamowienie->administrator_dane:''));
        $excel->getActiveSheet()->getStyleByColumnAndRow(2, 5)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        

        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol, $row, '=sum(' . $cols[11] . ($startRow + 1) . ':' . $cols[11] . ($row - 1) . ')');
        $excel->getActiveSheet()->setCellValueByColumnAndRow(($maxCol + 1), $row++, $zamowienie->waluta_symbol);
        
        
        $writer = new \PHPExcel_Writer_Excel5($excel);
        $writer->save($filePath . $fileName);
        return $fileName;
    }
    public function ordersByCurrency($zamowienia,$filePath='',$from=null,$to=null,$waluta=null){
        if(empty($zamowienia) || $zamowienia->count()==0){
            return false;
        }
        $cols = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
        $excel = new \PHPExcel();
        
        
        $excel->getActiveSheet()->mergeCellsByColumnAndRow(0, 1, 10, 1);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, $this->Txt->printAdmin(__('Excel | Raport zamówień wg. waluty'), 'Excel |'));
        
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, $this->Txt->printAdmin(__('Excel | Waluta'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, $waluta);
        
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 3, $this->Txt->printAdmin(__('Excel | Data od'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, date('Y-m-d', strtotime($from)));
        
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 4, $this->Txt->printAdmin(__('Excel | Data do'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow(1, 4, date('Y-m-d', strtotime($to)));
        
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 5, $this->Txt->printAdmin(__('Excel | Ilość zamówień'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow(1, 5, $zamowienia->count());
        
        $startRow = 7;
        $col = 0;
        $row = $startRow + 1;
        $maxCol = 0;
        //-----Nagłówki-------//
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Nr. zam'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Data'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Waluta'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Wartość'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Status'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Id klienta'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Klient'), 'Excel |'));
        
        //-------Content---------//
        $wartosc=0;
        foreach ($zamowienia as $zamowienie){
            $wartosc+=$zamowienie->wartosc_razem;
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, (!empty($zamowienie->numer)?$zamowienie->numer:$zamowienie->id));
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $zamowienie->data->format('Y-m-d H:i:s'));
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $zamowienie->waluta_symbol);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $zamowienie->wartosc_razem);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $zamowienie->status);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $zamowienie->uzytkownik_id);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $zamowienie->imie.' '.$zamowienie->nazwisko.(!empty($zamowienie->firma)?', '.$zamowienie->firma:''));
        
            //-------------------------------//
            $col = 0;
            $row++;
        }
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 6, $this->Txt->printAdmin(__('Excel | Łączna wartość'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, $wartosc);
        $fileName='raport_zam_waluta_'.date('YmdHis').'.xls';
        $writer = new \PHPExcel_Writer_Excel5($excel);
        $writer->save($filePath . $fileName);
        return $fileName;
    }
    public function allProducts($towary,$filePath='',$from=null,$to=null,$waluta=null){
        if(empty($towary) || $towary->count()==0){
            return false;
        }
        $cols = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
        $excel = new \PHPExcel();
        
        
        $excel->getActiveSheet()->mergeCellsByColumnAndRow(0, 1, 10, 1);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, $this->Txt->printAdmin(__('Excel | Raport zamówionych towarów wg. waluty'), 'Excel |'));
        
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, $this->Txt->printAdmin(__('Excel | Waluta'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, $waluta);
        
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 3, $this->Txt->printAdmin(__('Excel | Data od'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, date('Y-m-d', strtotime($from)));
        
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 4, $this->Txt->printAdmin(__('Excel | Data do'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow(1, 4, date('Y-m-d', strtotime($to)));
        
        $startRow = 7;
        $col = 0;
        $row = $startRow + 1;
        $maxCol = 0;
        //-----Nagłówki-------//
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Id towaru'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Nazwa'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Kod'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | W kartonie'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Waga'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Objętość'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Cena'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Rabat'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Cena z rabatem'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Ilość kartonów'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Ilość szt.'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Wartość'), 'Excel |'));
        
        //-------Content---------//
        $wartosc=0;
        $ilosc=0;
        foreach ($towary as $item){
            $ilosc+=($item->ilosc*$item->ilosc_w_kartonie);
            $priceDef=(!empty($item->cena_podstawowa)?$item->cena_podstawowa:$item->cena_za_sztuke_brutto);
            $cenaRabat=round($item->cena_z_rabatem,3);//round((!empty($item->rabat)?($priceDef-($priceDef*($item->rabat/100))):$item->cena_za_sztuke_brutto),3);
            $wartosc+=($cenaRabat*($item->ilosc*$item->ilosc_w_kartonie));
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $item->towar_id);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $item->nazwa);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $item->kod);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $item->ilosc_w_kartonie);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $item->waga_kartonu);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $item->objetosc_kartonu);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $priceDef);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, (!empty($item->rabat)?$item->rabat:0));
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $cenaRabat);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $item->ilosc);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, ($item->ilosc*$item->ilosc_w_kartonie));
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=' . $cols[10] . $row . '*' . $cols[8] . $row);
           
            //-------------------------------//
            $col = 0;
            $row++;
        }
        
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 5, $this->Txt->printAdmin(__('Excel | Łączna ilość szt.'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow(1, 5, $ilosc);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 6, $this->Txt->printAdmin(__('Excel | Łączna wartość'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, $wartosc);
        $fileName='raport_towary_'.date('YmdHis').'.xls';
        $writer = new \PHPExcel_Writer_Excel5($excel);
        $writer->save($filePath . $fileName);
        return $fileName;
    }
    public function ordersByCustomer($zamowienia,$filePath='',$from=null,$to=null,$uzytkownik=null){
        if(empty($zamowienia) || $zamowienia->count()==0){
            return false;
        }
        $cols = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
        $excel = new \PHPExcel();
        
        
        $excel->getActiveSheet()->mergeCellsByColumnAndRow(0, 1, 10, 1);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, $this->Txt->printAdmin(__('Excel | Raport zamówień wg. klienta'), 'Excel |'));
        
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, $this->Txt->printAdmin(__('Excel | Klient'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, $uzytkownik->imie.' '.$uzytkownik->nazwisko.(!empty($uzytkownik->firma)?', '.$uzytkownik->firma:''));
        
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 3, $this->Txt->printAdmin(__('Excel | Data od'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, date('Y-m-d', strtotime($from)));
        
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 4, $this->Txt->printAdmin(__('Excel | Data do'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow(1, 4, date('Y-m-d', strtotime($to)));
        
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 5, $this->Txt->printAdmin(__('Excel | Ilość zamówień'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow(1, 5, $zamowienia->count());
        
        $startRow = 7;
        $col = 0;
        $row = $startRow + 1;
        $maxCol = 0;
        //-----Nagłówki-------//
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Nr. zam'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Data'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Waluta'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Wartość'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Status'), 'Excel |'));
        
        //-------Content---------//
        $wartosci=[];
        foreach ($zamowienia as $zamowienie){
            if(!key_exists($zamowienie->waluta_symbol, $wartosci)){
                $wartosci[$zamowienie->waluta_symbol]=0;
            }
            $wartosci[$zamowienie->waluta_symbol]+=$zamowienie->wartosc_razem;
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, (!empty($zamowienie->numer)?$zamowienie->numer:$zamowienie->id));
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $zamowienie->data->format('Y-m-d H:i:s'));
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $zamowienie->waluta_symbol);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $zamowienie->wartosc_razem);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $zamowienie->status);
           
            //-------------------------------//
            $col = 0;
            $row++;
        }
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 6, $this->Txt->printAdmin(__('Excel | Łączna wartość'), 'Excel |'));
        $wi=1;
        foreach ($wartosci as $waluta => $wartosc){
        $excel->getActiveSheet()->setCellValueByColumnAndRow($wi++, 6, $waluta);
        $excel->getActiveSheet()->setCellValueByColumnAndRow($wi++, 6, $wartosc);
        }
        $fileName='raport_zam_klient_'.date('YmdHis').'.xls';
        $writer = new \PHPExcel_Writer_Excel5($excel);
        $writer->save($filePath . $fileName);
        return $fileName;
    }
    public function allProductsUser($towary,$filePath='',$from=null,$to=null,$uzytkownik=null,$walutaByOrderId=[]){
        if(empty($towary) || $towary->count()==0){
            return false;
        }
        $cols = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
        $excel = new \PHPExcel();
        
        
        $excel->getActiveSheet()->mergeCellsByColumnAndRow(0, 1, 10, 1);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, $this->Txt->printAdmin(__('Excel | Raport zamówionych towarów wg. klienta'), 'Excel |'));
        
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, $this->Txt->printAdmin(__('Excel | Klient'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, $uzytkownik->imie.' '.$uzytkownik->nazwisko.(!empty($uzytkownik->firma)?', '.$uzytkownik->firma:''));
        
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 3, $this->Txt->printAdmin(__('Excel | Data od'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, date('Y-m-d', strtotime($from)));
        
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 4, $this->Txt->printAdmin(__('Excel | Data do'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow(1, 4, date('Y-m-d', strtotime($to)));
        
        $startRow = 7;
        $col = 0;
        $row = $startRow + 1;
        $maxCol = 0;
        //-----Nagłówki-------//
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Id towaru'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Nazwa'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Kod'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | W kartonie'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Waga'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Objętość'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Cena'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Ilość kartonów'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Ilość szt.'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Wartość'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow($maxCol++, $startRow, $this->Txt->printAdmin(__('Excel | Waluta'), 'Excel |'));
        
        //-------Content---------//
        $wartosci=[];
        $ilosc=0;
        foreach ($towary as $item){
            
            if(!key_exists($walutaByOrderId[$item->zamowienie_id], $wartosci)){
                $wartosci[$walutaByOrderId[$item->zamowienie_id]]=0;
            }
            
            $priceDef=(!empty($item->cena_podstawowa)?$item->cena_podstawowa:$item->cena_za_sztuke_brutto);
            $cenaRabat=round($item->cena_z_rabatem,3);//round((!empty($item->rabat)?($priceDef-($priceDef*($item->rabat/100))):$item->cena_za_sztuke_brutto),3);
            $wartosci[$walutaByOrderId[$item->zamowienie_id]]+=($cenaRabat*($item->ilosc*$item->ilosc_w_kartonie));
            $ilosc+=($item->ilosc*$item->ilosc_w_kartonie);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $item->towar_id);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $item->nazwa);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $item->kod);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $item->ilosc_w_kartonie);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $item->waga_kartonu);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $item->objetosc_kartonu);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $priceDef);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, (!empty($item->rabat)?$item->rabat:0));
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $cenaRabat);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $item->ilosc);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, ($item->ilosc*$item->ilosc_w_kartonie));
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=' . $cols[10] . $row . '*' . $cols[8] . $row);
            $excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $walutaByOrderId[$item->zamowienie_id]);
           
            //-------------------------------//
            $col = 0;
            $row++;
        }
        
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 5, $this->Txt->printAdmin(__('Excel | Łączna ilość szt.'), 'Excel |'));
        $excel->getActiveSheet()->setCellValueByColumnAndRow(1, 5, $ilosc);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, 6, $this->Txt->printAdmin(__('Excel | Łączna wartość'), 'Excel |'));
        $wi=1;
        foreach ($wartosci as $waluta => $wartosc){
        $excel->getActiveSheet()->setCellValueByColumnAndRow($wi++, 6, $waluta);
        $excel->getActiveSheet()->setCellValueByColumnAndRow($wi++, 6, $wartosc);
        }
        $fileName='raport_towary_klient_'.date('YmdHis').'.xls';
        $writer = new \PHPExcel_Writer_Excel5($excel);
        $writer->save($filePath . $fileName);
        return $fileName;
    }
}
