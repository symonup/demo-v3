<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Translation\Translation;

class TranslationComponent extends Component {

    public $Translation;

    public function __construct(ComponentRegistry $registry, array $config = []) {
        parent::__construct($registry, $config);
        $this->Translation = new Translation();
    }

    public function shutdown() {
        $this->Translation->saveFiles();
    }

    public function get($key, $param = []) {
        return $this->Translation->get($key, $param);
    }

    public function add($locale) {
        $this->Translation->add($locale);
    }

    public function delete($file) {
        $this->Translation->delete($file);
    }
    
    public function setLocale($locale) {
        $this->Translation->setLocale($locale);
    }
    
    public function save($data) {
        return $this->Translation->save($data);
    }
    
    public function getLang() {
        return $this->Translation->getLang();
    }
public function setPath($path){
    return $this->Translation->setPath($path);
}
}
