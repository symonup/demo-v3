<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Configure;

class UploadComponent extends Component {

    function saveFile($destination, $file, $filename = null, $thumbs = null, $znak_wodny = false, $import_image = false, $allowExtensions = []) {

        if ($this->checkDir($destination)) {
            $file_upload = false;
            $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
            if (!empty($allowExtensions) && !in_array($ext, $allowExtensions)) {
                return false;
            }
            if (empty($filename)) {
                $filename = uniqid('', true) . '.' . $ext;
            }
            if (!$import_image) {
                $file_upload = move_uploaded_file($file['tmp_name'], $destination . $filename);
            } else {
                $file_upload = file_put_contents($destination . $filename, $file['image']);
            }
            unset($file);
            if ($file_upload) {
                if ($znak_wodny)
                    $this->znak_wodny($destination . $filename);
                if (!empty($thumbs)) {
                    foreach ($thumbs as $thumb => $options) {
                        if (empty($options))
                            continue;
                        if (empty($options['iaspect']))
                            $options['iaspect'] = false;
                        if (empty($options['iaspectColor']))
                            $options['iaspectColor'] = false;
                        $this->resizeImage($destination . $filename, $destination . $thumb . '_' . $filename, $options['width'], $options['height'], $options['iaspect'], $options['iaspectColor'], (!empty($options['znak_wodny']) ? $options['znak_wodny'] : false));
                    }
                }
                return $filename;
            } else
                return false;
        } else {
            return false;
        }
    }

    public function moveAll($from, $to, $fileName, $thumbs = []) {
        if (empty($from) || empty($to) || empty($fileName)) {
            return false;
        }
        if ($this->checkDir($to)) {
            if (copy($from . $fileName, $to . $fileName)) {
                if (!empty($thumbs)) {
                    foreach ($thumbs as $thumb => $thOptions) {
                        if (copy($from . $thumb . '_' . $fileName, $to . $thumb . '_' . $fileName)) {
                            unlink($from . $thumb . '_' . $fileName);
                        }
                    }
                }
                unlink($from . $fileName);
            }
        }
    }

    public function resizeImage($sSource, $sDestination, $nMaxWidth, $nMaxHeight, $iaspect = false, $iaspectColor = false, $znak_wodny = false) {
        $tmp_source = str_replace(['.jpg', '.jpeg', '.gif', '.png'], ['_orig.jpg', '_orig.jpeg', '_orig.gif', '_orig.png'], $sSource);
        if (!$znak_wodny && file_exists($tmp_source))
            $sSource = $tmp_source;
        $filetype = pathinfo($sSource);
        $filetype = strtolower($filetype['extension']);

        if ($filetype == 'jpg')
            $filetype = 'jpeg';

        $function = 'imagecreatefrom' . $filetype;
        if (!$image = @$function($sSource))
            return false;
        $width = imagesx($image);
        $height = imagesy($image);

        //zdjecie docelowe jest mniejsze od maksymalnych rozmiarow
        if ($nMaxWidth >= $width && $nMaxHeight >= $height) {
            $newWidth = $width;
            $newHeight = $height;
        } else {
            $nRatioW = $nMaxWidth / $width;
            $nRatioH = $nMaxHeight / $height;

            $nRatio = min($nRatioW, $nRatioH);

            $newHeight = $height * $nRatio;
            $newWidth = $width * $nRatio;
        }

        if ($iaspect) {
            $newimage = imagecreatetruecolor($nMaxWidth, $nMaxHeight);
            $bckColor = imagecolorallocate($newimage, hexdec(substr($iaspectColor, 0, 2)), hexdec(substr($iaspectColor, 2, 2)), hexdec(substr($iaspectColor, 4, 2)));
            ImageFill($newimage, 1, 1, $bckColor);
            $dst_x = ($nMaxWidth - $newWidth) / 2;
            $dst_y = ($nMaxHeight - $newHeight) / 2;
            imagecopyresampled($newimage, $image, $dst_x, $dst_y, 0, 0, $newWidth, $newHeight, $width, $height);
        } else {
            $newimage = imagecreatetruecolor($newWidth, $newHeight);
            imagecopyresampled($newimage, $image, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
        }

        if ($filetype == 'gif' || $filetype == 'png') {
            $black = imagecolorallocate($image, 0, 0, 0);
            $trnprt_indx = imagecolortransparent($image);
            if ($trnprt_indx >= 0) {
                $trnprt_color = imagecolorsforindex($image, $trnprt_indx);
                $trnprt_indx = imagecolorallocate($newimage, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);
                imagefill($newimage, 0, 0, $trnprt_indx);
                imagecolortransparent($newimage, $trnprt_indx);
            }
            // Always make a transparent background color for PNGs that don't have one allocated already
            elseif ($filetype == 'png') {
                imagealphablending($newimage, false);
                imagesavealpha($newimage, true);
                $color = imagecolorallocatealpha($newimage, 255, 255, 255, 127);
                imagefilledrectangle($newimage, 0, 0, $width, $height, $color);
//				imagefill($newimage, 0, 0, $color);
//                var_dump($width, $height,$newWidth, $newHeight,$nMaxWidth, $nMaxHeight,($nMaxWidth-$newWidth)/2, ($nMaxHeight-$newHeight)/2); die();
                if ($iaspect)
                    imagecopyresampled($newimage, $image, ($nMaxWidth - $newWidth) / 2, ($nMaxHeight - $newHeight) / 2, 0, 0, $newWidth, $newHeight, $width, $height);
                else
                    imagecopyresampled($newimage, $image, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
            }
        }
        $sSource = str_replace('_orig', '', $sSource);
        if ($filetype == 'jpeg')
            $saveResult = imagejpeg($newimage, $sDestination, 90);
        elseif ($filetype == 'gif')
            $saveResult = imagegif($newimage, $sDestination);
        elseif ($filetype == 'png')
            $saveResult = imagepng($newimage, $sDestination, 0);
        unset($newimage, $sDestination, $image, $bckColor, $black, $trnprt_color, $trnprt_indx, $color);
        if ($saveResult) {
            unset($saveResult);
            return true;
        } else {
            unset($saveResult);
            return false;
        }
    }

    public function deleteFile($file) {
        if (!file_exists($file))
            return true;
        if (unlink($file))
            return true;
        else
            return false;
    }

    public function znak_wodny($sSource) {
        $copySource = str_replace(['.jpg', '.jpeg', '.gif', '.png'], ['_orig.jpg', '_orig.jpeg', '_orig.gif', '_orig.png'], $sSource);
        if (!file_exists($copySource)) {
            copy($sSource, $copySource);
        } else
            $sSource = $copySource;
        $filetype = pathinfo($sSource);
        $filetype = strtolower($filetype['extension']);

        if ($filetype == 'jpg')
            $filetype = 'jpeg';

        $function = 'imagecreatefrom' . $filetype;
        if (!$image = @$function($sSource))
            return false;
        $width = imagesx($image);
        $height = imagesy($image);
        if (!file_exists((ROOT . DS . 'webroot' . DS . 'img' . DS . 'znak_wodny.png'))) {
            return false;
        }
        $znak = imagecreatefrompng(ROOT . DS . 'webroot' . DS . 'img' . DS . 'znak_wodny.png'); // plik ktĂłry bÄ™dzie dodany na obraz 03.jpg

        $newimage = imagecreatetruecolor($width, $height);
        imagecopyresampled($newimage, $image, 0, 0, 0, 0, $width, $height, $width, $height);

        $znaczek_w = imagesx($znak); // szerokoĹ›Ä‡ znaczka
        $znaczek_h = imagesy($znak); // wysokoĹ›Ä‡ znaczka

        $od_x = ($width - $znaczek_w); // / 2; // Wyznaczanie poĹ‚oĹĽenie w tym przypadku br czyli dolny prawy rĂłg
        $od_y = ($height - $znaczek_h); // / 2; // Wyznaczanie poĹ‚oĹĽenie w tym przypadku br czyli dolny prawy rĂłg
        $polozenie = Configure::read('zdjecia.znak_wodny_polozenie');
        switch ($polozenie) {
            case 'left_top': {
                    $od_x = 0;
                    $od_y = 0;
                } break;
            case 'right_top': {
                    $od_x = ($width - $znaczek_w);
                    $od_y = 0;
                } break;
            case 'middle': {
                    $od_x = ($width - $znaczek_w) / 2;
                    $od_y = ($height - $znaczek_h) / 2;
                } break;
            case 'left_bottom': {
                    $od_x = 0;
                    $od_y = ($height - $znaczek_h);
                } break;
            case 'right_bottom': {
                    $od_x = ($width - $znaczek_w);
                    $od_y = ($height - $znaczek_h);
                } break;

            default: {
                    $od_x = ($width - $znaczek_w);
                    $od_y = ($height - $znaczek_h);
                }
                break;
        }
        imagecopyresampled($newimage, $znak, $od_x, $od_y, 0, 0, $znaczek_w, $znaczek_h, $znaczek_w, $znaczek_h);

        $sSource = str_replace('_orig', '', $sSource);
        if ($filetype == 'jpeg')
            $saveResult = imagejpeg($newimage, $sSource, 90);
        elseif ($filetype == 'gif')
            $saveResult = imagegif($newimage, $sSource);
        elseif ($filetype == 'png')
            $saveResult = imagepng($newimage, $sSource, 0);
        if ($saveResult) {
            return true;
        } else
            return false;
    }

    public function imageStream($sSource, $sDestination, $filename = null, $thumbs = [], $znak_wodny = false, $filetype = 'png') {
        if (empty($sSource)) {
            return false;
        }
        if (mb_strtolower($filetype) == 'jpg') {
            $filetype = 'jpeg';
        }
        if (empty($filename)) {
            $filename = uniqid() . '.' . $filetype;
        }
        if (!$image = imagecreatefromstring($sSource)) {
            return false;
        }
        if ($this->checkDir($sDestination)) {
            if ($filetype == 'jpeg')
                $saveResult = imagejpeg($image, $sDestination . $filename, 90);
            elseif ($filetype == 'gif')
                $saveResult = imagegif($image, $sDestination . $filename);
            elseif ($filetype == 'png')
                $saveResult = imagepng($image, $sDestination . $filename, 9, -1);

            if ($saveResult)
                return $filename;
            else
                return false;
        } else {
            return false;
        }
    }

    public function makeDir($path, $dir) {
        if (!is_dir($path . $dir)) {
            if (!mkdir($path . $dir, 0777, true)) {
                return false;
            } else
                return $dir;
        } else
            return $dir;
    }

    public function recurse_copy($src, $dst) {
        $dir = opendir($src);
        @mkdir($dst);
        while (false !== ( $file = readdir($dir))) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if (is_dir($src . '/' . $file)) {
                    $this->recurse_copy($src . '/' . $file, $dst . '/' . $file);
                } else {
                    copy($src . '/' . $file, $dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }

    public function checkDir($destination) {
        $destination = str_replace(ROOT, '', $destination);
        $dirOk = true;
        $katalogi = explode(DS, $destination);
        if (count($katalogi) > 0) {
            $katalogPath = ROOT;
            foreach ($katalogi as $katalog) {
                if (empty($katalog)) {
                    $katalogPath .= DS;
                    continue;
                }
                if (!is_dir($katalogPath . $katalog)) {
                    if (!mkdir($katalogPath . $katalog, 0777)) {
                        $dirOk = false;
                        break;
                    }
                }
                $katalogPath .= $katalog . DS;
            }
        }
        return $dirOk;
    }

    public function imageDisplay($sSource, $path = null, $polozenie = 'middle') {
        if (empty($sSource))
            return false;
        $filetype = 'png';
        $filetype = pathinfo($sSource);
        $filetype = strtolower($filetype['extension']);

        if ($filetype == 'jpg')
            $filetype = 'jpeg';

        $function = 'imagecreatefrom' . $filetype;
        if (!$image = @$function($sSource))
            return false;
        // ustawiam transparentność
        imagealphablending($image, true);
        imagesavealpha($image, true);
        // pobieram wymiary
        $width = imagesx($image);
        $height = imagesy($image);
        // Sprawdzam czy ma być znak wodny
        if (empty($path) || !file_exists($path)) {
            return $image;
        }
        $znak = imagecreatefrompng($path); // pobieram znak wodny ze ścieżki
        // Pobieram wymiary znaku wodnego
        $znaczek_w = imagesx($znak);
        $znaczek_h = imagesy($znak);

        $od_x = ($width - $znaczek_w); // / 2; // Wyznaczanie położenia w tym przypadku right_bottom czyli dolny prawy róg
        $od_y = ($height - $znaczek_h); // / 2; // Wyznaczanie położenia w tym przypadku right_bottom czyli dolny prawy róg
        //Ustalam położenie obrazka
        switch ($polozenie) {
            case 'left_top': {
                    $od_x = 0;
                    $od_y = 0;
                } break;
            case 'right_top': {
                    $od_x = ($width - $znaczek_w);
                    $od_y = 0;
                } break;
            case 'middle': {
                    $od_x = ($width - $znaczek_w) / 2;
                    $od_y = ($height - $znaczek_h) / 2;
                } break;
            case 'center': {
                    $od_x = ($width - $znaczek_w) / 2;
                    $od_y = ($height - $znaczek_h) / 2;
                } break;
            case 'left_bottom': {
                    $od_x = 0;
                    $od_y = ($height - $znaczek_h);
                } break;
            case 'right_bottom': {
                    $od_x = ($width - $znaczek_w);
                    $od_y = ($height - $znaczek_h);
                } break;

            default: {
                    $od_x = ($width - $znaczek_w);
                    $od_y = ($height - $znaczek_h);
                }
                break;
        }
        // Kopiuje znak wodny na obrazek
        imagecopy($image, $znak, $od_x, $od_y, 0, 0, $znaczek_w, $znaczek_h);
        // Zwracam obrazek ze znakiem wodnym
        return $image;
    }

}
