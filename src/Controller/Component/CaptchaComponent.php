<?php
namespace Cake\Controller\Component;

use Cake\Controller\Component;
use Cake\Datasource\RepositoryInterface;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use \Cake\Routing\Router;
use Cake\Network\Session;
use Cake\View\Helper\SessionHelper;
use Cake\Controller\Component\FlashComponent;
use App\Controller\Component\TranslationComponent;

class CaptchaComponent extends Component {

    var $controller = null;
    var $components = ['Session', 'RequestHandler','Flash','Translation'];
    public $width = 100;

    /** Height of the image */
    public $height = 35;

    /** Dictionary word file (empty for randnom text) */
    public $wordsFile = 'words/en.php';

    /**
     * Path for resource files (fonts, words, etc.)
     *
     * "resources" by default. For security reasons, is better move this
     * directory to another location outise the web server
     *
     */
    public $pathType = 1;
    public $resourcesPath = 'cakecaptcha/resources';

    /** Min word length (for non-dictionary random text generation) */
    public $minWordLength = 5;

    /**
     * Max word length (for non-dictionary random text generation)
     * 
     * Used for dictionary words indicating the word-length
     * for font-size modification purposes
     */
    public $maxWordLength = 5;

    /** Sessionname to store the original text */
    public $session_var = 'captcha';

    /** Background color in RGB-array */
    public $backgroundColor = array(255, 0, 100);

    /** Foreground colors in RGB-array */
    public $colors = array(
//        array(27, 78, 181), // blue
//        array(22, 163, 35), // green
//        array(214, 36, 7), // red
        array(167, 167, 167), // gray
    );

    /** Shadow color in RGB-array or null */
    public $shadowColor = array(255, 255, 255);

    /**
     * Font configuration
     *
     * - font: TTF file
     * - spacing: relative pixel space between character
     * - minSize: min font size
     * - maxSize: max font size
     */
    public $fonts = array(
        'Times' => array('spacing' => 0, 'minSize' => 20, 'maxSize' => 20, 'font' => 'TimesNewRomanBold.ttf'),
    );

    /** Wave configuracion in X and Y axes */
    public $Yperiod = 10;
    public $Yamplitude = 10;
    public $Xperiod = 9;
    public $Xamplitude = 0;

    /** letter rotation clockwise */
    public $maxRotation = 0;

    /**
     * Internal image size factor (for better image quality)
     * 1: low, 2: medium, 3: high
     */
    public $scale = 3;

    /**
     * Blur effect for better image quality (but slower image processing).
     * Better image results with scale=3
     */
    public $blur = false;

    /** Debug? */
    public $debug = false;

    /** Image format: jpeg or png */
    public $imageFormat = 'png';

    /** GD image */
    public $im;

    public $session;
    
    public function startup($controller) {
//        $this->controller = $controller;
//        var_dump($this->Session->read('test')); die();
    }
    
    public function __construct(\Cake\Controller\ComponentRegistry $registry, array $config = array()) {
        parent::__construct($registry, $config);
    }

    public function initialize(array $config) {
        parent::initialize($config);
//        var_dump($config); die();
        $this->session=new Session();
//        $this->controller = $controller;
//        $this->resourcesPath = $this->controller->webroot . 'cakecaptcha/resources';
        unset($this->wordsFile);
    }

    function getCaptcha() {
//        $this->controller->autoRender = false;
        $this->CreateImage();
    }

    function configCaptcha($config) {
        if (is_array($config)) {
            foreach ($config as $key => $value) {
                switch ($key) {
                    case 'blur' :
                        if ($value == true || $value == false) {
                            $this->blur = $value;
                        }
                        break;
                    case 'quality' :
                        if ($value == 1 || $value == 2 || $value == 3) {
                            $this->scale = $value;
                        }
                        break;
                    case 'format' :
                        if ($value == 'jpeg' || $value == 'png') {
                            $this->imageFormat = $value;
                        }
                        break;
                    case 'wordFile' :
                        if ($value == true || $value == false) {
                            if ($value)
                                $this->wordsFile = 'words/en.php';
                        }
                        break;
                    case 'minLength' :
                        if (is_int($value)) {
                            $this->minWordLength = $value;
                        }
                        break;
                    case 'maxLenght' :
                        if (is_int($value)) {
                            $this->maxWordLength = $value;
                        }
                        break;
                    case 'width' :
                        if (is_int($value)) {
                            $this->width = $value;
                        }
                        break;
                    case 'height' :
                        if (is_int($value)) {
                            $this->height = $value;
                        }
                        break;
                    case 'background' :
                        if (is_array($value)) {
                            if (count($value) == 3) {
                                foreach ($value as $k => $v) {
                                    $this->backgroundColor[$k] = $v;
                                }
                            }
                        }
                        break;
                    case 'color' :
                        if (is_array($value)) {
                            if (in_array(true, $value, true)) {
                                $this->colors = array();
                                foreach ($value as $k => $v) {
                                    if (is_array($v) && count($v) == 3) {
                                        if (gettype($v[0]) == 'integer' & gettype($v[1]) == 'integer' & gettype($v[2]) == 'integer')
                                            $this->colors[] = $v;
                                    }
                                }
                            }
                            else {
                                foreach ($value as $k => $v) {
                                    if (is_array($v) && count($v) == 3) {
                                        if (gettype($v[0]) == 'integer' & gettype($v[1]) == 'integer' & gettype($v[2]) == 'integer')
                                            $this->colors[] = $v;
                                    }
                                }
                            }
                        }
                        break;
                    case 'pathType' :
                        if ($value == 1) {
                            $this->resourcesPath = $this->controller->webroot . 'cakecaptcha/resources';
                        } else if ($value = 2) {
                            $this->resourcesPath = realpath('.') . '/cakecaptcha/resources';
                        } else {
                            
                        }
                }
            }
        }
    }

    function validateCaptcha($request=null) {
        if (isset($request->data['cakecaptcha']['captcha']) && !empty($request->data['cakecaptcha']['captcha'])) {
            if ($this->session->check("cakecaptcha." . $this->session_var)) {
                if ($this->session->read("cakecaptcha." . $this->session_var) == $request->data['cakecaptcha']['captcha']) {
                    $this->session->delete("cakecaptch." . $this->session_var);
//                    $this->controller->set('captchaerror', false);
                    return true;
                } else {
                    $this->Flash->error($this->Translation->get('errors.contactWrittenCodeIsInvalid'));
//                    $this->controller->session->setFlash($this->controller->lngMessage['message']['wpisany kod jest nieprawidlowy'],'error');
//                    $this->controller->set('captchaerror', $this->controller->lngMessage['message']['wpisany kod jest nieprawidlowy']);
                    return false;
                }
            }
        } else {
            if ($request->is('post')) {
                    $this->Flash->error($this->Translation->get('kontakt.contactEnterTheCode'));
            } 
            return false;
        }
    }

    function validateGetCaptcha($model = "cakecaptcha") {
        if (isset($this->controller->params['url']['data'][$model]['captcha'])) {
            if (empty($this->controller->params['url']['data'][$model]['captcha'])) {
                $this->controller->set('captchaerror', 'Musisz przepisać kod z obrazka');
                return false;
            }
            if ($this->controller->session->check("cakecaptcha." . $this->session_var)) {
                if ($this->controller->session->read("cakecaptcha." . $this->session_var) == $this->controller->params['url']['data'][$model]['captcha']) {
                    $this->controller->session->delete("cakecaptch." . $this->session_var);
                    $this->controller->set('captchaerror', false);
                    return true;
                } else {
                    $this->controller->session->setFlash($this->controller->lngMessage['message']['wpisany kod jest nieprawidlowy'],'error');
                    //debug($this->controller);
                    $this->controller->set('captchaerror', $this->controller->lngMessage['message']['wpisany kod jest nieprawidlowy']);
                    return false;
                }
            }
        } else {
            $this->controller->set('captchaerror', false);
            return false;
        }
    }


    protected function CreateImage() {
        $ini = microtime(true);

        /** Initialization */
        $this->ImageAllocate();

        /** Text insertion */
        $text = $this->GetCaptchaText();
        $fontcfg = $this->fonts[array_rand($this->fonts)];
        $this->WriteText($text, $fontcfg);

        //  $_SESSION[$this->session_var] = $text;
        $this->session->write('cakecaptcha.' . $this->session_var, $text);

        /** Transformations */
        //$this->WaveImage(); // italic
//        $this->bgImage();
        if ($this->blur && function_exists('imagefilter')) {
            imagefilter($this->im, IMG_FILTER_GAUSSIAN_BLUR);
        }
        $this->ReduceImage();


        if ($this->debug) {
            imagestring($this->im, 1, 1, $this->height - 8, "$text {$fontcfg['font']} " . round((microtime(true) - $ini) * 1000) . "ms", $this->GdFgColor
            );
        }


        /** Output */
        $this->WriteImage();
        $this->Cleanup();
    }

    /**
     * Creates the image resources
     */
    protected function ImageAllocate() {
        // Cleanup
        if (!empty($this->im)) {
            imagedestroy($this->im);
        }

        $this->im = imagecreate($this->width * $this->scale, $this->height * $this->scale);
$bgc=imagecolorallocatealpha($this->im, 255, 255, 255,127);
        // Foreground color
        $color = $this->colors[mt_rand(0, sizeof($this->colors) - 1)];
        $this->GdFgColor = imagecolorallocatealpha($this->im, $color[0], $color[1], $color[2],30);

        // Shadow color
        if (!empty($this->shadowColor) && is_array($this->shadowColor) && sizeof($this->shadowColor) >= 3) {
            $this->GdShadowColor = imagecolorallocate($this->im, $this->shadowColor[0], $this->shadowColor[1], $this->shadowColor[2]
            );
        }
        $this->bgImage();
    }

    /**
     * Text generation
     *
     * @return string Text
     */
    protected function GetCaptchaText() {
        $text = $this->GetDictionaryCaptchaText();
        if (!$text) {
            $text = $this->GetRandomCaptchaText();
        }
        return $text;
    }

    /**
     * Random text generation
     *
     * @return string Text
     */
    protected function GetRandomCaptchaText($length = null) {
        if (empty($length)) {
            $length = rand($this->minWordLength, $this->maxWordLength);
        }

        $words = "0123456789";

        $text = "";
        for ($i = 0; $i < $length; $i++) {
            $text .= substr($words, mt_rand(0, 9), 1);
            }
        return $text;
    }

    /**
     * Random dictionary word generation
     *
     * @param boolean $extended Add extended "fake" words
     * @return string Word
     */
    function GetDictionaryCaptchaText($extended = false) {
        if (empty($this->wordsFile)) {
            return false;
        }
        //echo "words file";
        // Full path of words file
        if (substr($this->wordsFile, 0, 1) == '/') {
            $wordsfile = $this->wordsFile;
        } else {
            $wordsfile = $this->resourcesPath . '/' . $this->wordsFile;
        }

        $fp = fopen($wordsfile, "r");
        $length = strlen(fgets($fp));
        if (!$length) {
            return false;
        }
        $line = rand(1, (filesize($wordsfile) / $length) - 2);
        if (fseek($fp, $length * $line) == -1) {
            return false;
        }
        $text = trim(fgets($fp));
        fclose($fp);


        /** Change ramdom volcals */
        if ($extended) {
            $text = preg_split('//', $text, -1, PREG_SPLIT_NO_EMPTY);
            $vocals = array('a', 'e', 'i', 'o', 'u');
            foreach ($text as $i => $char) {
                if (mt_rand(0, 1) && in_array($char, $vocals)) {
                    $text[$i] = $vocals[mt_rand(0, 4)];
                }
            }
            $text = implode('', $text);
        }

        return $text;
    }

    /**
     * Text insertion
     */
    protected function WriteText($text, $fontcfg = array()) {
        if (empty($fontcfg)) {
            // Select the font configuration
            $fontcfg = $this->fonts[array_rand($this->fonts)];
        }

        // Full path of font file
        $fontfile = $this->resourcesPath . '/fonts/' . $fontcfg['font'];


        /** Increase font-size for shortest words: 9% for each glyp missing */
        $lettersMissing = $this->maxWordLength - strlen($text);
        $fontSizefactor = 1 + ($lettersMissing * 0.09);

        // Text generation (char by char)
        $x = 15 * $this->scale;
        $y = round(($this->height * 27 / 40) * $this->scale);
        $length = strlen($text);
        for ($i = 0; $i < $length; $i++) {
            $degree = rand($this->maxRotation * -1, $this->maxRotation);
            $fontsize = rand($fontcfg['minSize'], $fontcfg['maxSize']) * $this->scale * $fontSizefactor;
            $letter = substr($text, $i, 1);

            if ($this->shadowColor) {
                $coords = imagettftext($this->im, $fontsize, $degree, $x + $this->scale, $y + $this->scale, $this->GdShadowColor, $fontfile, $letter);
            }
            $coords = imagettftext($this->im, $fontsize, $degree, $x, $y, $this->GdFgColor, $fontfile, $letter);
            $x += ($coords[2] - $x) + ($fontcfg['spacing'] * $this->scale);
        }
    }

    /**
     * Wave filter
     */
    protected function WaveImage() {
        // X-axis wave generation
        $xp = $this->scale * $this->Xperiod * rand(1, 3);
        $k = rand(0, 100);
        for ($i = 0; $i < ($this->width * $this->scale); $i++) {
            imagecopy($this->im, $this->im, $i - 1, sin($k + $i / $xp) * ($this->scale * $this->Xamplitude), $i, 0, 1, $this->height * $this->scale);
        }

        // Y-axis wave generation
        $k = rand(0, 100);
        $yp = $this->scale * $this->Yperiod * rand(1, 2);
        for ($i = 0; $i < ($this->height * $this->scale); $i++) {
            imagecopy($this->im, $this->im, sin($k + $i / $yp) * ($this->scale * $this->Yamplitude), $i - 1, 0, $i, $this->width * $this->scale, 1);
        }
    }
    /**
     * BackgroundImg
     */
    protected function bgImage(){
        $znak = imagecreatefrompng(ROOT . DS . 'webroot' . DS . 'cakecaptcha' . DS .'bg.png'); // pobieram tlo ze ścieżki
        // Pobieram wymiary tla
        $znaczek_w = imagesx($znak);
        $znaczek_h = imagesy($znak);
        imagecopy($this->im, $znak, 0, 0, 0, 0, $znaczek_w, $znaczek_h);
    }

    /**
     * Reduce the image to the final size
     */
    protected function ReduceImage() {
        // Reduzco el tama�o de la imagen
        $imResampled = imagecreate($this->width, $this->height);
        imagecopyresampled($imResampled, $this->im, 0, 0, 0, 0, $this->width, $this->height, $this->width * $this->scale, $this->height * $this->scale
        );
        imagedestroy($this->im);
        $this->im = $imResampled;
    }

    /**
     * File generation
     */
    protected function WriteImage() {
        if ($this->imageFormat == 'png' && function_exists('imagepng')) {
            header("Content-type: image/png");
            imagepng($this->im);
        } else {
            header("Content-type: image/jpeg");
            imagejpeg($this->im, null, 80);
        }
    }

    /**
     * Cleanup
     */
    protected function Cleanup() {
        imagedestroy($this->im);
    }

}

?>