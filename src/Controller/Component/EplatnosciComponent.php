<?php

namespace Cake\Controller\Component;

use Cake\Controller\Component;
use Cake\Datasource\RepositoryInterface;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use \Cake\Routing\Router;

class EplatnosciComponent extends Component {

    public $controller;
    public $Platnosc;
    public $Zamowienie;
    public $waluty;
    public $jezyki;
    public $options = array();
    public $ustawienia = array(
        'DotpayIp' => array('217.17.41.5', '195.150.9.37'),
        'PaypalUrl' => 'https://www.paypal.com/cgi-bin/webscr',
        'PaypalTestUrl' => 'https://www.sandbox.paypal.com/cgi-bin/webscr',
        'PayU' => 'https://secure.payu.com/api/v2_1/orders',
        'P24.opisOK' => 'TEST_OK',
        'P24.opisBlad' => 'TEST_ERR',
        'P24.hostWeryfikacja' => 'https://secure.przelewy24.pl/transakcja.php'
    );

    public function initialize(array $config) {
        parent::initialize($config);
        $this->Platnosc = TableRegistry::get('Platnosc');
        $this->Zamowienie = TableRegistry::get('Zamowienie');
        $waluta = TableRegistry::get('Waluta');
        $jezyk = TableRegistry::get('Jezyk');
        $this->waluty = $waluta->find('list', ['keyField' => 'id', 'valueField' => 'symbol'])->toArray();
        $this->jezyki = $jezyk->find('list', ['keyField' => 'locale', 'valueField' => 'symbol'])->toArray();
    }

    public function startup($controller) {
        $this->controller = $controller;
    }

    /**
     * Przygotuj dane dla zamówienia (w oparciu o id lub token)
     * @param int $id
     * @param string $token
     */
    public function prepareData($zamowienie) {


        //brak zamówienia
        if (empty($zamowienie)) {
            return 'BrakZamowienia';
        }

        //sprawdz czy platnosc jest aktywna
        if (empty($zamowienie->rodzaj_platnosci->aktywna)) {
            return 'PlatnoscNieaktywna';
        }

        //czy platnosc istnieje
        if (!empty($zamowienie->platnosc)) {
            //zamowienie/platnosc oplacone
            if ($zamowienie->platnosc[0]->status == 'wykonana' || $zamowienie->platnosc[0]->status == 'oplacone') {
                return 'ZamowienieOplacone';
            }

            //czy jest ten sam rodzaj platnosci
            if (!empty($zamowienie->rodzaj_platnosci) && $zamowienie->rodzaj_platnosci->id != $zamowienie->platnosc[0]->rodzaj_platnosci_id) {
                return 'NieprawidlowyRodzajPlatnosci';
            }

            if (!empty($zamowienie->rodzaj_platnosci) && $zamowienie->rodzaj_platnosci->platnosc_elektroniczna == 1) {
                $platnoscAktualizacja = $this->Platnosc->get($zamowienie->platnosc[0]->id);
                $platnoscAktualizacja->status = 'rozpoczeta';
                $platnoscAktualizacja->data_rozpoczecia = date('Y-m-d H:i:s');
                $this->Platnosc->save($platnoscAktualizacja);
            }
        }

        //Dotpay
        if ($zamowienie->rodzaj_platnosci->id == 3)
            $this->prepareDotpayOptions($zamowienie);
        //Przelewy24
        elseif ($zamowienie->rodzaj_platnosci->id == 4)
            $this->preparePrzelewy24Options($zamowienie);
        //Płatności.pl
        elseif ($zamowienie->rodzaj_platnosci->id == 5)
            $this->preparePayUOptions($zamowienie);
        //PayPal
        elseif ($zamowienie->rodzaj_platnosci->id == 6)
            $this->preparePaypalOptions($zamowienie);
        return $this->options;
    }

    /**
     * Przygotuj opcje dla formularza DOTPAY
     * @param array $zamowienie
     */
    public function prepareDotpayOptions($zamowienie) {
        $dostepneJezyki = array('pl_PL' => 'pl', 'en_US' => 'en', 'de_DE' => 'de', 'it' => 'it', 'fr' => 'fr', 'es_ES' => 'es', 'cs_CZ' => 'cz', 'ru_RU' => 'ru', 'bg_BG' => 'bg');
        if (array_key_exists($zamowienie->lng, $dostepneJezyki))
            $jezyk = $dostepneJezyki[$zamowienie->lng];
        else
            $jezyk = 'pl';

        $opcje = array(
            'url' => 'https://ssl.dotpay.pl/',
            'id' => $zamowienie->rodzaj_platnosci->konfig1,
            'kwota' => round($zamowienie->platnosc[0]->kwota,2),
            'waluta' => $this->waluty[$zamowienie->platnosc[0]->waluta_id],
            'jezyk' => $jezyk,
            'opis' => str_replace('[%zamowienie_id]', $zamowienie->id, $zamowienie->platnosc[0]->opis_lng),
            'URL' => Router::url(array('controller' => 'zamowienie', 'action' => 'podziekowanie', 3, $zamowienie->platnosc[0]->token2), true),
            'typ' => '0',
            'txtguzik' => __('Powrót do serwisu', true),
            'URLC' => Router::url(array('controller' => 'zamowienie', 'action' => 'platnosc', 3, $zamowienie->platnosc[0]->token), true),
            'control' => $zamowienie->platnosc[0]->token,
            'imie' => $zamowienie->imie,
            'nazwisko' => $zamowienie->nazwisko,
            'email' => $zamowienie->email
        );
        $this->options = $opcje;
    }

    /**
     * Przygotuj opcje dla formularza Przelewy24
     * @param array $zamowienie
     */
    public function preparePrzelewy24Options($zamowienie) {
        $opcje = array(
            'url' => 'https://secure.przelewy24.pl/index.php',
            'p24_session_id' => $zamowienie->platnosc[0]->token . '|' . microtime(),
            'p24_id_sprzedawcy' => $zamowienie->rodzaj_platnosci->konfig1,
            'p24_kwota' => $zamowienie->platnosc[0]->kwota * 100,
            'p24_opis' => str_replace('[%zamowienie_id]', $zamowienie->id, $zamowienie->platnosc[0]->opis_lng),
            'p24_klient' => $zamowienie->imie . ' ' . $zamowienie->nazwisko,
            /* 'p24_adres' => $zamowienie['Uzytkownik']['ulica'].' '.$zamowienie['Uzytkownik']['nrdomu'].' '.$zamowienie['Uzytkownik']['nrlokalu'],
              'p24_kod' => $zamowienie['Uzytkownik']['kod'],
              'p24_miasto' => $zamowienie['Uzytkownik']['miasto'], */
            'p24_kraj' => 'PL',
            'p24_email' => $zamowienie->email,
            'p24_language' => $this->jezyki[$zamowienie->lng],
            'p24_return_url_ok' => Router::url(array('controller' => 'zamowienie', 'action' => 'platnosc', 4, $zamowienie->platnosc[0]->token), true),
            'p24_return_url_error' => Router::url(array('controller' => 'zamowienie', 'action' => 'platnosc', 4, $zamowienie->platnosc[0]->token), true),
        );

        if ($zamowienie->rodzaj_platnosci->konfig2 == 1)
            $opcje['p24_opis'] = $this->ustawienia['P24.opisOK'];
        elseif ($zamowienie->rodzaj_platnosci->konfig2 == 2)
            $opcje['p24_opis'] = $this->ustawienia['P24.opisBlad'];

        $this->options = $opcje;
    }

    /**
     * Przygotuj opcje dla formularza PayU
     * @param array $zamowienie
     */
    public function preparePayUOptions($zamowienie) {
        $ip = $_SERVER['REMOTE_ADDR'];
        $opcje = array(
            'url' => 'https://secure.payu.com/api/v2_1/orders',
            'customerIp' => $ip,
            'merchantPosId' => $zamowienie->rodzaj_platnosci->konfig1,
            'description' => str_replace('[%zamowienie_id]', $zamowienie->id, $zamowienie->platnosc[0]->opis_lng),
            'totalAmount' => $zamowienie->platnosc[0]->kwota * 100,
            'currencyCode' => $this->waluty[$zamowienie->platnosc[0]->waluta_id],
            'notifyUrl' => Router::url(array('controller' => 'zamowienie', 'action' => 'platnosc', 5, $zamowienie->platnosc[0]->token), true),
            'continueUrl' => Router::url(array('controller' => 'zamowienie', 'action' => 'podziekowanie', 4, $zamowienie->platnosc[0]->token2), true),
            'shippingMethods[0].country' => 'PL',
            'shippingMethods[0].price' => $zamowienie->koszt_przesylki * 100,
            'shippingMethods[0].name' => $zamowienie->wysylka->nazwa,
            'buyer.email' => $zamowienie->email,
            'buyer.firstName' => $zamowienie->imie,
            'buyer.lastName' => $zamowienie->nazwisko,
            'buyer.delivery.recipientName' => $zamowienie->imie . ' ' . $zamowienie->nazwisko,
            'buyer.delivery.street' => $zamowienie->ulica . ' ' . $zamowienie->nr_domu . (!empty($zamowienie->nr_lokalu) ? '/' . $zamowienie->nr_lokalu : ''),
            'buyer.delivery.postalCode' => $zamowienie->kod,
            'buyer.delivery.city' => $zamowienie->miasto,
            'buyer.delivery.countryCode' => 'PL'
        );
        if (!empty($zamowienie->platnosc[0]->doplata)) {
            unset(
                    $opcje['shippingMethods[0].country'],
                    $opcje['shippingMethods[0].price'],
                    $opcje['shippingMethods[0].name'],
                    $opcje['buyer.delivery.recipientName'],
                    $opcje['buyer.delivery.street'],
                    $opcje['buyer.delivery.postalCode'],
                    $opcje['buyer.delivery.city'],
                    $opcje['buyer.delivery.countryCode']
                    );
            $opcje['products[0].name']=$zamowienie->platnosc[0]->opis_lng;
            $opcje['products[0].unitPrice']=$zamowienie->platnosc[0]->kwota;
            $opcje['products[0].quantity']=1;
        }
        if (empty($zamowienie->platnosc[0]->doplata)) {
            foreach ($zamowienie->zamowienie_towar as $key => $item) {
                $opcje['products[' . $key . '].name'] = str_replace('"', '', $item->nazwa);
                $opcje['products[' . $key . '].unitPrice'] = $item->cena * 100;
                $opcje['products[' . $key . '].quantity'] = $item->ilosc;
            }
        }
        $opcje['OpenPayu-Signature'] = $this->generate_payu_signature($opcje, $zamowienie->rodzaj_platnosci->konfig2, $zamowienie->rodzaj_platnosci->konfig1);

        $this->options = $opcje;
    }

    private function generate_payu_signature($form, $secondKey, $posId) {
        unset($form['url']);
        ksort($form);
        $content = '';
        foreach ($form as $value) {
            $content = $content . $value;
        }
        $content = $content . $secondKey;
        $result = "sender=" . $posId . ";";
        $result = $result . "algorithm=MD5;";
        $result = $result . "signature=" . md5($content);
        return $result;
    }

    /**
     * Przygotuj opcje dla formularza PAYPAL
     * @param array $zamowienie
     */
    public function preparePaypalOptions($zamowienie) {
        //tryb testowy
        if ($zamowienie->rodzaj_platnosci->konfig3 == 1) {
            $url = $this->ustawienia['PaypalTestUrl'];
            $business = $zamowienie->rodzaj_platnosci->konfig2;
        }
        //tryb testowy
        else {
            $url = $this->ustawienia['PaypalUrl'];
            $business = $zamowienie->rodzaj_platnosci->konfig1;
        }

        $opcje = array(
            'url' => $url,
            'cmd' => '_xclick',
            'business' => $business,
            'lc' => strtoupper($this->jezyki[$zamowienie->lng]),
            'currency_code' => $this->waluty[$zamowienie->platnosc[0]->waluta_id],
            'item_name' => str_replace('[%zamowienie_id]', $zamowienie->id, $zamowienie->platnosc[0]->opis_lng),
            'amount' => number_format($zamowienie->platnosc[0]->kwota, 2),
            'return' => Router::url(array('controller' => 'zamowienie', 'action' => 'podziekowanie', 6, $zamowienie->platnosc[0]->token2), true),
            'notify_url' => Router::url(array('controller' => 'zamowienie', 'action' => 'platnosc', 6, $zamowienie->platnosc[0]->token), true),
            'custom' => $zamowienie->platnosc[0]->token,
            'first_name' => $zamowienie->imie,
            'last_name' => $zamowienie->nazwisko,
            'email' => $zamowienie->email,
            'charset' => 'UTF-8'
        );
        $this->options = $opcje;
    }

    /**
     * Walidacja danych DotPay
     * @param array $rodzajPlatnosci
     * @return mixed
     */
    public function validateDotpayIpn($rodzajPlatnosci, $post) {
        if (empty($post['control']))
            return false;

        if (empty($post['t_status']))
            return false;

        if (empty($post['id']) || $post['id'] != $rodzajPlatnosci->konfig1)
            return false;

        $checkString = $rodzajPlatnosci->konfig2 . ':' . $rodzajPlatnosci->konfig1 . ':' . $post['control'] . ':' .
                $post['t_id'] . ':' . $post['amount'] . ':' . $post['email'] . ':' .
                '' . ':' . '' . ':' . '' . ':' .
                '' . ':' . $_POST['t_status'];
        if (md5($checkString) != $post['md5']) {
            $this->log('Nieprawidłowa suma kontrolna.', 'dotpay');
            return false;
        }
        $dane = array();
        $dane['token'] = $post['control'];
        $dane['kwota'] = $post['amount'];

        if ($post['t_status'] == 2) {
            $dane['oplacone'] = 1;
            $dane['status'] = 'wykonana';
        } elseif ($post['t_status'] == 3 || $post['t_status'] == 4) {
            $dane['oplacone'] = 0;
            $dane['status'] = 'odrzucona';
        }

        return $dane;
    }

    /**
     * Walidacja danych Przelewy24
     * @param array $rodzajPlatnosci
     * @return mixed
     */
    public function validatePrzelewy24Ipn($rodzajPlatnosci, $post) {
        if (empty($post['p24_session_id']) || empty($post['p24_order_id']) || empty($post['p24_order_id_full']))
            return false;

        $session_id = explode('|', $post['p24_session_id']);
        $token = $session_id[0];

        //pobieramy dane zamowienie
        $platnosc = $this->Platnosc->find('all', array(
                    'conditions' => [
                        'Platnosc.token' => $token,
                    ]
                ))->first();
        if (empty($platnosc))
            return false;

        //kwota w groszach
        $kwota = $platnosc->kwota * 100;

        //wyslanie danych weryfikujacych
        $P = array();

        $P[] = 'p24_session_id=' . $token;
        $P[] = 'p24_order_id=' . $post['p24_order_id'];
        $P[] = 'p24_id_sprzedawcy=' . $rodzajPlatnosci->konfig1;
        $P[] = 'p24_kwota=' . $kwota;
        $P[] = 'p24_crc=' . md5($token.$post['p24_order_id'].$rodzajPlatnosci->konfig1.$kwota);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, join("&", $P));
        curl_setopt($ch, CURLOPT_URL, $this->ustawienia['P24.hostWeryfikacja']);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $result = curl_exec($ch);
        curl_close($ch);

        //tabela z otrzymanymi danymi po weryfikacji
        $verifyResult = array();

        //podzielenie odpowiedzi na linie
        $T = explode(chr(13) . chr(10), $result);

        //czy został odczytane wiersz z "RESULT"
        $res = false;

        //petla po liniach
        foreach ($T as $line) {
            //usun znaki konca linii
            $line = ereg_replace("[\n\r]", "", $line);

            //najpierw szukamy "RESULT"
            if ($line != "RESULT" and !$res)
                continue;

            //"RESULT" już zostało znalezione - odpowiedz zapisujemy
            if ($res)
                $verifyResult[] = $line;
            //wlasnie znalezlismy "RESULT"
            else
                $res = true;
        }


        $dane = array();
        $dane['token'] = $token;
        $dane['kwota'] = $platnosc->kwota;

        //transakcja prawidłowa
        if ($verifyResult[0] == "TRUE") {
            $dane['oplacone'] = 1;
            $dane['status'] = 'wykonana';
        }
        //transakcja błędna
        else {
            $dane['oplacone'] = 0;
            $dane['status'] = 'odrzucona';
        }

        return $dane;
    }

    /**
     * Walidacja danych PayU
     */
    public function validatePayUIpn($rodzajPlatnosci, $signature, $data, $md5Value) {
        $dane = [];
        if (!empty($signature)) {
            $respArr = explode(';', $signature);
            $arr = [];
            foreach ($respArr as $rp) {
                $_tmp = explode('=', $rp);
                $arr[$_tmp[0]] = $_tmp[1];
            }
            if ($arr['signature'] == $md5Value) {
                $this->log('PayU ' . $data->order->orderId . ' status: ' . $data->order->status, \Psr\Log\LogLevel::INFO);
                if ($data->order->status == 'COMPLETED') {
                    $dane['status'] = 'wykonana';
                } else
                if ($data->order->status == 'CANCELED' || $data->order->status == 'REJECTED') {
                    $dane['status'] = 'odrzucona';
                }
            } else {
                $this->log('PayU niepoprawny klucz', \Psr\Log\LogLevel::INFO);
            }
        }
        return $dane;
    }

    /**
     * Walidacja danych PayPal
     * @param array $rodzajPlatnosci
     * @return mixed
     */
    public function validatePaypalIpn($rodzajPlatnosci, $post) {
        //tryb testowy
        if ($rodzajPlatnosci->konfig3 == 1) {
            $url = $this->ustawienia['PaypalTestUrl'];
            $business = $rodzajPlatnosci->konfig2;
        }
        //tryb testowy
        else {
            $url = $this->ustawienia['PaypalUrl'];
            $business = $rodzajPlatnosci->konfig1;
        }

        if ($post['business'] != $business) {
            $this->log('Nieprawidłowe konto PayPal', 'paypal');
            die();
        }

        //wyslanie danych weryfikujacych
        $P = array();

        foreach ($post as $field => $value) {
            $P[] = $field . '=' . urlencode($value) . '&';
        }

        $P[] = "cmd=_notify-validate";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        if (count($P))
            curl_setopt($ch, CURLOPT_POSTFIELDS, join("&", $P));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $ipnResponse = curl_exec($ch);
        curl_close($ch);

        if (substr("VERIFIED", $ipnResponse) === FALSE) {
            $this->log('Dane niezweryfikowane.', 'paypal');
            return false;
        }

        $dane = array();
        $dane['token'] = $post['custom'];
        $dane['kwota'] = $post['mc_gross'];

        if ($post['payment_status'] == 'Completed') {
            $dane['oplacone'] = 1;
            $dane['status'] = 'wykonana';
        } elseif (in_array($post['payment_status'], array('Denied', 'Expired', 'Failed', 'Refunded', 'Reversed', 'Voided'))) {
            $dane['oplacone'] = 0;
            $dane['status'] = 'odrzucona';
        }
        else
            return false;

        return $dane;
    }

}

?>
