<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         3.0.4
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\View;

use Cake\Event\EventManager;
use Cake\Network\Request;
use Cake\Network\Response;

/**
 * A view class that is used for AJAX responses.
 * Currently only switches the default layout and sets the response type -
 * which just maps to text/html by default.
 */
class AjaxView extends AppView {

    /**
     * The name of the layout file to render the view inside of. The name
     * specified is the filename of the layout in /src/Template/Layout without
     * the .ctp extension.
     *
     * @var string
     */
    public $layout = 'ajax';

    /**
     * Initialization hook method.
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();
        $defFormTemplate = [
            'inputContainer' => '<div class="form-group {{type}}{{required}}">{{content}}</div>',
            'inputContainerError' => '<div class="form-group {{type}}{{required}} error">{{content}}</div>',
            'formGroup' => '{{label}}{{error}}{{input}}',
            'error' => '<span class="error invalid-feedback">{{content}}</span>',
            'submitContainer' => '<div class="form-group submit">{{content}}</div>',
            'input' => '<input type="{{type}}" class="form-control" name="{{name}}"{{attrs}}/>',
            'textarea' => '<textarea class="form-control" name="{{name}}"{{attrs}}>{{value}}</textarea>',
            'select' => '<select class="form-control" name="{{name}}"{{attrs}}>{{content}}</select>',
            'selectMultiple' => '<select class="form-control" name="{{name}}[]" multiple="multiple"{{attrs}}>{{content}}</select>',
            'file' => '<div class="file-add"><input type="file" name="{{name}}"{{attrs}}><button type="button" class="btn btn-sm btn-info"><i class="fa fa-plus"></i> _FILE_ADD_LABEL_</button><div class="upload-file-preview"><img src="_FILE_SRC_"/></div></div>',
        ];
        if (empty($this->request->params['prefix'])) {
            $frontTemplate = [
                'checkbox' => '<input type="checkbox" name="{{name}}" value="{{value}}"{{attrs}}>',
                'checkboxFormGroup' => '{{label}}',
                'checkboxWrapper' => '<div class="checkbox">{{label}}</div>',
                'nestingLabel' => '{{hidden}}{{input}}<label{{attrs}}>{{text}}</label>'
            ];
            $defFormTemplate = array_merge($defFormTemplate, $frontTemplate);
        }
        $this->Form->setTemplates($defFormTemplate);
        $this->response->type('ajax');
    }

}
