<?php

namespace Cake\View\Helper;

use Cake\Core\Configure;
use Cake\Network\Response;
use Cake\View\Helper;
use Cake\View\StringTemplateTrait;
use Cake\View\View;

    class CaptchaHelper extends Helper {

    public $helpers = ['Html', 'Form','Translation'];
    private $captchaerror;
    private $view;
    private $required;

    public function __construct(View $View, array $config = array()) {
        parent::__construct($View, $config);
        $this->view = $View;
        $this->captchaerror = (!empty($View->viewVars['captchaerror']) ? $View->viewVars['captchaerror'] : null);
    }

    function input($controller = null, $required = true,$prefix=false) {
        $this->required = !empty($required) ? 'required' : '';
        if (is_null($controller)) {
            $controller = $this->view->request->params['controller'];
        }
        $output = $this->writeCaptcha($controller,$prefix);
        return $output;
    }

    protected function writeCaptcha($controller,$prefix=false) {
        $return = '';
        $return.='<div class="captcha-form">';
        $return.= $this->view->Html->image(\Cake\Routing\Router::url(['controller' => $controller, 'action' => 'captcha','prefix'=>$prefix], true), array('id' => 'cakecaptcha'));

        if ($this->captchaerror) {
            $return.= "<div class='error'><div class='error-message'>" . $this->captchaerror . "</div></div>";
        }
        $return.=' <a href="#captcha" data-toggle="tooltip" title="' . $this->Translation->get('kontaktForm.contactRefreshImage') . '" class="btn btn-xs btn-link" onclick="document.getElementById(\'cakecaptcha\').src = \'' . \Cake\Routing\Router::url(['controller' => $controller, 'action' => 'captcha','prefix'=>$prefix]) . '?\' + Math.random(); document.getElementById(\'captcha-form\').focus();" id="change-image"><i class="fas fa-sync"></i></a>';
        $return.='</div>';
        $return.= $this->Form->input('cakecaptcha', array('id' => 'captcha-form', 'class' => 'form-control', 'format' => array('before', 'input', 'between', 'label', 'after', 'error'), 'name' => 'cakecaptcha[captcha]','label'=>false, 'placeholder' => $this->Translation->get('kontaktForm.contactEnterTheCode'), $this->required, 'div' => array('class' => 'captcha-div')));
        return $return;
    }

}

?>
