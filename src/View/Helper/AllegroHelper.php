<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\Core\Configure;
use Cake\I18n\I18n;
use Cake\Network\Session;

class AllegroHelper extends Helper {

    public $helpers = ['Html', 'Form', 'Number', 'Translation'];

    public function getParamInput($paramData = [],$key=0,$values=[]) {
        if (empty($paramData)) {
            return '';
        }
        $control = '';
        $required = !empty($paramData['required']);
        $unit=(!empty($paramData['unit'])?' ['.$paramData['unit'].']':'');
                    $value=$this->getValue($paramData['id'], $values);
        switch ($paramData['type']) {
            case 'dictionary': {
                    $multiple = false;
                    if (!empty($paramData['restrictions']) && !empty($paramData['restrictions']['multipleChoices'])) {
                        $multiple = 'checkbox';
                    }
                    $options = [];
                    foreach ($paramData['dictionary'] as $option) {
                        $options[$option['id']] = $option['value'];
                    }
                    $control = $this->Form->control('parameters.' . $key . '.id', ['value' => $paramData['id'], 'type' => 'hidden']) . $this->Form->control('parameters.' . $key . '.valuesIds', ['type' => 'select', 'multiple' => $multiple,'value'=>(!empty($multiple)?$value:((!empty($value))?join('', $value):'')),'multiple-key'=>(!$multiple?false:$paramData['id']),'class'=>(!$multiple?'form-control':'multiple-item'), 'required' => $required, 'label' => $paramData['name'].$unit, 'options' => $options]);
                }break;
            case 'integer': {
                    if (key_exists('restrictions', $paramData) && key_exists('range', $paramData['restrictions']) && !empty($paramData['restrictions']['range'])) {
                        $control = $this->Form->control('parameters.' . $key . '.id', ['value' => $paramData['id'], 'type' => 'hidden']) . $this->Form->control('parameters.' . $key . '.rangeValue', ['type' => 'range','class'=>'form-control','value'=> (!empty($value)?$value['from'].';'.$value['to']:''), 'min' => ((key_exists('restrictions', $paramData) && key_exists('min', $paramData['restrictions'])) ? $paramData['restrictions']['min'] : null), 'max' => ((key_exists('restrictions', $paramData) && key_exists('max', $paramData['restrictions'])) ? $paramData['restrictions']['max'] : null), 'required' => $required, 'label' => $paramData['name'].$unit]);
                    } else {
                        $control = $this->Form->control('parameters.' . $key . '.id', ['value' => $paramData['id'], 'type' => 'hidden']) . $this->Form->control('parameters.' . $key . '.values', ['type' => 'number','class'=>'form-control','value'=> (!empty($value)?join('', $value):''), 'min' => ((key_exists('restrictions', $paramData) && key_exists('min', $paramData['restrictions'])) ? $paramData['restrictions']['min'] : null), 'max' => ((key_exists('restrictions', $paramData) && key_exists('max', $paramData['restrictions'])) ? $paramData['restrictions']['max'] : null), 'required' => $required, 'label' => $paramData['name'].$unit]);
                    }
                }break;
            case 'float': {
                    if (key_exists('restrictions', $paramData) && key_exists('range', $paramData['restrictions']) && !empty($paramData['restrictions']['range'])) {
                        $control = $this->Form->control('parameters.' . $key . '.id', ['value' => $paramData['id'], 'type' => 'hidden']) . $this->Form->control('parameters.' . $key . '.rangeValue', ['type' => 'range','class'=>'form-control','value'=> (!empty($value)?$value['from'].';'.$value['to']:''), 'min' => ((key_exists('restrictions', $paramData) && key_exists('min', $paramData['restrictions'])) ? $paramData['restrictions']['min'] : null), 'max' => ((key_exists('restrictions', $paramData) && key_exists('max', $paramData['restrictions'])) ? $paramData['restrictions']['max'] : null), 'required' => $required, 'label' => $paramData['name'].$unit]);
                    } else {
                        $control = $this->Form->control('parameters.' . $key . '.id', ['value' => $paramData['id'], 'type' => 'hidden']) . $this->Form->control('parameters.' . $key . '.values', ['type' => 'text','class'=>'form-control','value'=> (!empty($value)?join('', $value):''), 'data-type' => 'float', 'required' => $required, 'label' => $paramData['name'].$unit]);
                    }
                }break;
            case 'string': {
                    $control = $this->Form->control('parameters.' . $key . '.id', ['value' => $paramData['id'], 'type' => 'hidden']) . $this->Form->control('parameters.' . $key . '.values', ['type' => 'textarea','class'=>'form-control','value'=> (!empty($value)?join('', $value):''), 'required' => $required, 'label' => $paramData['name'].$unit]);
                }break;
        }
        return $control;
    }
    private function getValue($param,$values){
        if(!empty($values)){
        foreach ($values as $valueKey => $value){
            if(!empty($value['id']) && $value['id'] == $param){
                if(!empty($value['valuesIds'])){
                    return $value['valuesIds'];
                }
                if(!empty($value['values'])){
                    return $value['values'];
                }
                if(!empty($value['rangeValue'])){
                    return $value['rangeValue'];
                }
            }
        }
        }
        return '';
    }
}

