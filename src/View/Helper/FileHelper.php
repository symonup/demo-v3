<?php
namespace App\View\Helper;

use Cake\View\Helper;

class FileHelper extends Helper
{
    public $helpers = ['Html','Form'];
    public function control($name, $btnLabel='' ,$options=[])
    {
        if(empty($options) || empty($options['type']) || $options['type']!=='file'){
            $options['type']='file';
        }
        if(empty($options) || empty($options['src'])){
            $options['src']='';
        }
        $options['value']='';
        return str_replace(['_FILE_ADD_LABEL_','_FILE_SRC_'],[$btnLabel,$options['src']],$this->Form->control($name,$options));
    }
}
