<?php
namespace Cake\View\Helper;

use Cake\Core\Configure;
use Cake\Network\Response;
use Cake\View\Helper;
use Cake\View\StringTemplateTrait;
use Cake\View\View;
class TableHelper extends Helper {
public $helpers = ['Html','Form'];
    public function create($tbody = array(), $thead = array(), $tfooter = array(),$sort=false,$classTable='table table-hover') {

        $_tbody = '';
        $_thead = '';
        $_tfooter = '';
        if (!empty($tbody)) {
            foreach ($tbody as $tr) {
                $tds = '';
                foreach ($tr as $tdBody) {
                    if (!is_array($tdBody)) {
                        $tds.=$this->Html->tag('td', $tdBody);
                    } else {
                        if (!empty($tdBody[0]) && !empty($tdBody[1]))
                            $tds.=$this->Html->tag('td', $tdBody[0], $tdBody[1]);
                    }
                }
                $optTr=[];
                if($sort) $optTr['class']='sort-item';
                $_tbody.=$this->Html->tag('tr', $tds,$optTr);
            }
        }
        if (!empty($thead)) {
            foreach ($thead as $tdHead) {
                if (!is_array($tdHead)) {
                    $_thead.=$this->Html->tag('th', $tdHead);
                } else {
                    if (!empty($tdHead[0]) && !empty($tdHead[1]))
                        $_thead.=$this->Html->tag('th', $tdHead[0], $tdHead[1]);
                }
            }
        }
        if (!empty($tfooter)) {
            foreach ($tfooter as $tdFooter) {
                if (!is_array($tdFooter)) {
                    $_tfooter.=$this->Html->tag('td', $tdFooter);
                } else {
                    if (!empty($tdFooter[0]) && !empty($tdFooter[1]))
                        $_tfooter.=$this->Html->tag('td', $tdFooter[0], $tdFooter[1]);
                }
            }
        }
        $table = '';
        if (!empty($_thead))
            $table.=$this->Html->tag('thead', $this->Html->tag('tr', $_thead));

        if (!empty($_tbody))
        {
            $optTbody=[];
            if($sort) $optTbody['class']='sort';
            $table.=$this->Html->tag('tbody', $_tbody,$optTbody);
        }

        if (!empty($_tfooter))
            $table.=$this->Html->tag('tfooter', $this->Html->tag('tr', $_tfooter));
        
        return $this->Html->tag('table',$table,array('class'=>$classTable));
    }

}
?>
