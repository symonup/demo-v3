<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Translation\Translation;

class TranslationHelper extends Helper {

    public $Translation;
    public $helpers = ['Html'];

    public function __construct(View $view, $config = []) {
        parent::__construct($view, $config);
        $this->Translation = new Translation();
    }

    public function afterLayout() {
        $this->Translation->saveFiles();
    }

    public function setLocale($locale) {
        $this->Translation->setLocale($locale);
    }
    
    public function get($key, $param = [],$wysiwyg=false) {
        return $this->Translation->get($key, $param, $wysiwyg);
    }

    public function key($key) {
        return $this->Translation->key($key);
    }

    public function inputs($form, $languages, $inputs,$hidden=false,$prefix=null) {
        $tabs = [];
        foreach ($languages as $locale => $nazwa) {
            $_inputs = [];
            $link = $this->Html->link($nazwa, '#'.(!empty($prefix)?$prefix:'') . $locale, ['data-toggle' => 'tab']);
            $tabs['tab'][] = $this->Html->tag('li', $link, ['class' => (empty($tabs['tab']) ? 'active' : '')]);

            foreach ($inputs as $key => $value) {
               
                if (is_array($value)) {
                    if (!empty($value['label'])) {
                        if(is_array($value['label'])){
                            $value['label']['text'] = __($value['label']['text'], $locale);
                        }
                        else $value['label'] = __($value['label'], $locale);
                    }
                    if (!empty($value['lng'])) {
                        $value['lng'] = __($value['lng'], $locale);
                    }
                    $_tmpKey = explode('.', $key);
                    if (count($_tmpKey) > 1) {
                        $inputName = $_tmpKey[count($_tmpKey) - 1];
                        unset($_tmpKey[count($_tmpKey) - 1]);
                        $inputName = join('.', $_tmpKey) . '._translations.' . $locale . '.' . $inputName;
                    }
                    else
                        $inputName = '_translations.' . $locale . '.' . $key;
                    $_inputs[] = $form->control($inputName, $value);
                } else {
                    $_tmpKey = explode('.', $value);
                    if (count($_tmpKey) > 1) {
                        $inputName = $_tmpKey[count($_tmpKey) - 1];
                        unset($_tmpKey[count($_tmpKey) - 1]);
                        $inputName = join('.', $_tmpKey) . '._translations.' . $locale . '.' . $inputName;
                    }
                    else
                        $inputName = '_translations.' . $locale . '.' . $value;
                    $_inputs[] = $form->control($inputName);
                }
            }

            $tabs['content'][] = $this->Html->tag('div', implode('', $_inputs), ['id' => (!empty($prefix)?$prefix:'').$locale, 'class' => 'tab-pane fade' . (empty($tabs['content']) ? ' in active' : '')]);
        }
        if($hidden)
        {
            return implode('', $tabs['content']);
        }
        else return $this->Html->tag('div',$this->Html->tag('ul', implode('', $tabs['tab']), array('class' => 'nav nav-tabs')) .
                $this->Html->tag('div', implode('', $tabs['content']), array('class' => 'tab-content'))
                ,['class'=>'translation-inputs']);
    }

}
