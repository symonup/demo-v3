<?php

namespace Cake\View\Helper;

use Cake\Core\Configure;
use Cake\Network\Response;
use Cake\View\Helper;
use Cake\View\StringTemplateTrait;
use Cake\View\View;
use \PHPExcel;
use \PHPExcel_IOFactory;

class XmlInputHelper extends Helper {

    public $helpers = ['Html', 'Form'];
    public $jezyki = [];

    public function input() {
        
    }

    function GUID() {
        if (function_exists('com_create_guid') === true) {
            return trim(com_create_guid(), '{}');
        }

        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

}

?>