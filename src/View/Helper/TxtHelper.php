<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\Core\Configure;
use Cake\I18n\I18n;
use Cake\Network\Session;

class TxtHelper extends Helper {

    public $helpers = ['Html', 'Form', 'Number', 'Translation'];
    public $days = [
        1 => 'poniedziałek',
        2 => 'wtorek',
        3 => 'środa',
        4 => 'czwartek',
        5 => 'piątek',
        6 => 'sobota',
        7 => 'niedziela'
    ];
    public $kraje;
    public $cart;

    public function initialize(array $config) {
        parent::initialize($config);
        $this->kraje = Configure::read('kraje');
    }

    public function printAdmin($txt = '', $prefix = null) {
        if (!empty($prefix)) {
            return str_replace($prefix, '', $txt);
        } else
            return str_replace('Admin | ', '', $txt);
    }

    public function langLabel($txt = '') {
        return str_replace('Lang | ', '', $txt);
    }

    public function date_range($first, $last, $step = '+1 day', $output_format = 'Y-m-d', $minDate = null, $maxDate = null) {

        $dates = array();
        if (empty($minDate)) {
            $minDate = strtotime($first);
        }
        $current = strtotime($first);
        $last = strtotime($last);
        if (empty($maxDate)) {
            $maxDate = $last;
        }

        while ($current <= $last) {
            if ($current >= $minDate && $current <= $last) {
                $dates[] = date($output_format, $current);
            }
            $current = strtotime($step, $current);
        }

        return $dates;
    }

    public function friendlyUrl($text) {
        $tabela = array(
            //WIN
            "\xb9" => "a", "\xa5" => "A", "\xe6" => "c", "\xc6" => "C",
            "\xea" => "e", "\xca" => "E", "\xb3" => "l", "\xa3" => "L",
            "\xf3" => "o", "\xd3" => "O", "\x9c" => "s", "\x8c" => "S",
            "\x9f" => "z", "\xaf" => "Z", "\xbf" => "z", "\xac" => "Z",
            "\xf1" => "n", "\xd1" => "N",
            //UTF
            "\xc4\x85" => "a", "\xc4\x84" => "A", "\xc4\x87" => "c", "\xc4\x86" => "C",
            "\xc4\x99" => "e", "\xc4\x98" => "E", "\xc5\x82" => "l", "\xc5\x81" => "L",
            "\xc3\xb3" => "o", "\xc3\x93" => "O", "\xc5\x9b" => "s", "\xc5\x9a" => "S",
            "\xc5\xbc" => "z", "\xc5\xbb" => "Z", "\xc5\xba" => "z", "\xc5\xb9" => "Z",
            "\xc5\x84" => "n", "\xc5\x83" => "N",
            //ISO
            "\xb1" => "a", "\xa1" => "A", "\xe6" => "c", "\xc6" => "C",
            "\xea" => "e", "\xca" => "E", "\xb3" => "l", "\xa3" => "L",
            "\xf3" => "o", "\xd3" => "O", "\xb6" => "s", "\xa6" => "S",
            "\xbc" => "z", "\xac" => "Z", "\xbf" => "z", "\xaf" => "Z",
            "\xf1" => "n", "\xd1" => "N",
            //I to co nie potrzebne
            " " => "-", "$" => "-", "!" => "-", "@" => "-", "#" => "-", "%" => "-", "/" => "-", "[" => "", "]" => "", "(" => "-", ")" => "-");

        return mb_strtolower(strtr($text, $tabela));
    }

    public function cenaVat($value = 0, $vat = 23, $in = 'netto', $out = 'brutto', $label = true) {
        if ($in == $out || empty($vat)) {
            $cena = $value;
        } else {
            $vat = 1 + ($vat / 100);
            switch ($in) {
                case 'netto': {
                        $cena = ($value * $vat);
                    }break;
                case 'brutto': {
                        $cena = ($value / $vat);
                    }break;
            }
        }
        if ($label) {
            return $this->cena($cena);
        } else {
            return round($cena, 2);
        }
    }

    public function currencyJsOptions($currency = null) {
        $currencyOptions = Configure::read('currencyOptions');
        if (empty($currency)) {
            $currency = Configure::read('currencyDefault');
        }
        $options = $currencyOptions[$currency];
        $symbol = Configure::read('currencysSymbol.' . $currency);
        return json_encode(['symbol' => $symbol, 'options' => $options]);
    }

    public function cena($value, $currency = null, $round = false) {
        $currencyOptions = Configure::read('currencyOptions');
        if (empty($currency)) {
            $currency = Configure::read('currencyDefault');
        }
        $options = (key_exists($currency, $currencyOptions) ? $currencyOptions[$currency] : null);
        $value = (string) round($value, 2);
        if (empty($options)) {
            return $this->Number->currency($value);
        }
        $tmp = explode('.', $value);
        $thousend = '';
        $after = '';
        $before = '';
        $space = '';
        $symbol = Configure::read('currencysSymbol.' . $currency);
        if (!empty($options['space'])) {
            $space = ' ';
        }
        if ($options['after']) {
            $after = $space . $symbol;
        } else {
            $before = $symbol . $space;
        }
        if (($tmp[0] / 1000) >= 1) {
            $thousend = $tmp[0] / 1000;
            $tmpTh = explode('.', (string) $thousend);
            if (count($tmpTh) > 1) {
                $add0 = '';
                if (strlen($tmpTh[1]) < 3) {
                    for ($i = strlen($tmpTh[1]); $i < 3; $i++) {
                        $add0 .= '0';
                    }
                }
            } else {
                $add0 = '000';
            }
            $thousend = $tmpTh[0] . $options['thousend'] . (!empty($tmpTh[1]) ? $tmpTh[1] : '') . $add0;
            if (!empty($tmp[1])) {
                if (strlen($tmp[1]) == 1) {
                    $decimal = $tmp[1] . '0';
                } else {
                    $decimal = $tmp[1];
                }
            } else {
                $decimal = '00';
            }
            if ($round && $decimal == '00') {
                return $before . $thousend . $after;
            } else
                return $before . $thousend . $options['decimal'] . $decimal . $after;
        } else {
            if (!empty($tmp[1])) {
                if (strlen($tmp[1]) == 1) {
                    $decimal = $tmp[1] . '0';
                } else {
                    $decimal = $tmp[1];
                }
            } else {
                $decimal = '00';
            }
            if ($round && $decimal == '00') {
                return $before . $tmp[0] . $after;
            } else
                return $before . $tmp[0] . $options['decimal'] . $decimal . $after;
        }
    }

    public function printBool($value, $link = null, $specialText = []) {
        if (!empty($link)) {
            return $this->Html->link((!empty($value) ? (!empty($specialText) ? $specialText[1] : $this->printAdmin(__('Admin | Tak'))) : (!empty($specialText) ? $specialText[0] : $this->printAdmin(__('Admin | Nie')))), $link, ['class' => 'ajaxLink']);
        } else {
            if (empty($value)) {
                return (!empty($specialText) ? $specialText[0] : $this->printAdmin(__('Admin | Nie')));
            } else {
                return (!empty($specialText) ? $specialText[1] : $this->printAdmin(__('Admin | Tak')));
            }
        }
    }

    public function file($input, $label, $src = '') {
        return str_replace(['_FILE_ADD_LABEL_', '_FILE_SRC_'], [$label, $src], $input);
    }

    public function getKatalog($id = null) {
        if (empty($id)) {
            return '';
        }
        $num = 10000000 + (int) $id;
        return substr((string) $num, 1, 4);
    }

    public function clearText($text = null, $cut = null, $complete = '') {
        if (empty($text)) {
            return $text;
        }
        $text = strip_tags($text);
        if (empty($cut) || (strlen($text) <= $cut)) {
            return $text;
        }
        $text = substr($text, 0, ($cut - 1));
        $text = substr($text, 0, strrpos($text, ' '));
        return trim($text) . $complete;
    }

    public function cutText($text = null, $cut = null, $complete = '') {
        if (empty($text)) {
            return $text;
        }
        $text = strip_tags($text);
        if (empty($cut) || (strlen($text) <= $cut)) {
            return $text;
        }
        $text = substr($text, 0, ($cut - 1));
        return trim($text) . $complete;
    }

    public function printItems($count = 0) {
        $itemsTxt = '';
        switch (I18n::locale()) {
            case 'pl_PL' : {
                    if ($count == 1) {
                        $itemsTxt = 'item';
                    } else {
                        $lastChar = (int) substr((string) $count, (strlen((string) $count) - 1), 1);
                        if ($lastChar >= 2 || $lastChar <= 4) {
                            $itemsTxt = 'items';
                        } else {
                            $itemsTxt = 'items_ow';
                        }
                    }
                } break;
            default: {
                    if ($count == 0 || $count > 1) {
                        $itemsTxt = 'items';
                    } else {
                        $itemsTxt = 'item';
                    }
                }
        }
        return $this->Translation->get('labels.{0}' . $itemsTxt, [$count]);
    }

    public function printAddres($adres = null, $break = '<br/>', $prefix = '') {
        if (empty($adres)) {
            return '';
        }
        $retunAdres = '';
        $adresTxt = [];
        if (!empty($adres[$prefix . 'ulica'])) {
            $adresTxt[] = $adres[$prefix . 'ulica'] . (!empty($adres[$prefix . 'nr_domu']) ? (' ' . $adres[$prefix . 'nr_domu'] . (!empty($adres[$prefix . 'nr_lokalu']) ? '/' . $adres[$prefix . 'nr_lokalu'] : '')) : '');
        }
        if (!empty($adres[$prefix . 'miasto']) || !empty($adres[$prefix . 'kod'])) {
            $adresTxt[] = $adres[$prefix . 'kod'] . ' ' . $adres[$prefix . 'miasto'];
        }
        if (key_exists($prefix . 'kraj', $adres) && !empty($this->kraje[$adres[$prefix . 'kraj']])) {
            $adresTxt[] = $this->kraje[$adres[$prefix . 'kraj']];
        }

        return join($break, $adresTxt);
    }

    public function printUserInfo($user = null, $break = '<br/>', $prefix = '') {
        if (empty($user)) {
            return '';
        }
        $retunTxt = '';
        $userTxt = [];
        if (!empty($user[$prefix . 'imie']) || !empty($user[$prefix . 'nazwisko'])) {
            $userTxt[] = $user[$prefix . 'imie'] . (!empty($user[$prefix . 'nazwisko']) ? (!empty($user[$prefix . 'imie']) ? ' ' : '') . $user[$prefix . 'nazwisko'] : '');
        }
        if (!empty($user[$prefix . 'firma'])) {
            $userTxt[] = $user[$prefix . 'firma'];
        }
        if (!empty($user[$prefix . 'nip'])) {
            $userTxt[] = $this->Translation->get('labels.Nip') . ': ' . $user[$prefix . 'nip'];
        }
        if (!empty($user[$prefix . 'telefon'])) {
            $userTxt[] = $this->Translation->get('labels.Phone') . ': ' . (!empty($user[$prefix . 'kierunkowy']) ? $user[$prefix . 'kierunkowy'] . ' ' : '') . $user[$prefix . 'telefon'];
        }
        if (!empty($user[$prefix . 'email'])) {
            $userTxt[] = $this->Translation->get('labels.Email') . ': ' . $user[$prefix . 'email'];
        }
        return join($break, $userTxt);
    }

    public function dateDiff($fDate, $sDate, $type = 'd') {
        $dataStart = new \DateTime($fDate);
        $daraEnd = new \DateTime($sDate);
        $dateDiff = $dataStart->diff($daraEnd);
        return $dateDiff->{$type};
    }

    public function clearToken($token, $add = '') {
        if (empty($token)) {
            return '';
        }
        return str_replace(md5($add . date('Ymd')), '', $token);
    }

    public function getToken($token, $add = '') {
        return $token . md5($add . date('Ymd'));
    }

    public function timeToFloat($time) {
        $tmp = explode(':', $time);
        $h = (int) $tmp[0];
        $m = (int) $tmp[1];
        $mf = ((((int) $m * 100) / 60) / 100);
        return ($h + $mf);
    }

    public function floatToTime($float, $label = false) {
        $tmp = explode('.', $float);
        $h = (int) $tmp[0];
        if (!empty($tmp[1])) {
            $m = (float) ('0.' . $tmp[1]);
            $m = round($m * 60);
            if ($m < 10 && !$label) {
                $m = '0' . $m;
            }
        } else {
            $m = '00';
        }
        if ($label) {
            return (!empty($h) ? $h . 'h' : '') . (($m != '00') ? (!empty($h) ? ' ' : '') . $m . ' min' : '');
        } else
            return $h . ':' . $m;
    }

    public function opiniaCount($count) {
        if (empty($count)) {
            return $this->Translation->get('opinia.{0}Opini', [0]);
        } elseif ($count == 1) {
            return $this->Translation->get('opinia.{0}Opinia', [1]);
        } else {
            if ($count < 20 && $count > 9) {
                return $this->Translation->get('opinia.{0}Opini', [$count]);
            }
            $lastChar = (int) substr((string) $count, (strlen((string) $count) - 1), 1);
            if ($lastChar > 1 && $lastChar < 5) {
                return $this->Translation->get('opinia.{0}Opinie', [$count]);
            } else {
                return $this->Translation->get('opinia.{0}Opini', [$count]);
            }
        }
    }

    public function itemPrice($towar, $label = false, $type = false) {
        $itemPrice = 0;
        $etykietyPromocji = explode(',', Configure::read('ceny.etykietyPromocji'));
        $aktywneKody = Configure::read('aktywneKody');
        $priceCode = (!empty($_GET['code'])?$_GET['code']:null);
        $promocja = false;
        if (!empty($etykietyPromocji)) {
            foreach ($etykietyPromocji as $etykieta) {
                if (!empty(trim($etykieta))) {
                    if (!empty($towar->{trim($etykieta)})) {
                        $promocja = true;
                        break;
                    }
                }
            }
        }
        if (!empty($towar->hot_deal) && !empty($towar->hot_deal->cena)) {
            $itemPrice = $this->cenaVat($towar->hot_deal->cena, $towar->towar_cena_default->vat->stawka, $towar->towar_cena_default->rodzaj, (!$type ? \Cake\Core\Configure::read('ceny.rodzaj') : $type), false);
        } elseif (!empty($towar->towar_cena_default)) {
            $itemPrice = $this->cenaVat(
                    (($promocja && !empty($towar->towar_cena_default->cena_promocja)) ? $towar->towar_cena_default->cena_promocja : $towar->towar_cena_default->cena_sprzedazy), $towar->towar_cena_default->vat->stawka, $towar->towar_cena_default->rodzaj, (!$type ? \Cake\Core\Configure::read('ceny.rodzaj') : $type), false);
        }
        if(!empty($priceCode) && !empty($aktywneKody) && key_exists($priceCode, $aktywneKody)){
            if($aktywneKody[$priceCode]['typ']=='procent'){
                if($aktywneKody[$priceCode]['rodzaj']=='ogolny' || ($aktywneKody[$priceCode]['rodzaj']=='producent' && key_exists($towar->producent_id,$aktywneKody[$priceCode]['producent'])) || ($aktywneKody[$priceCode]['rodzaj']=='kategoria' && !empty($towar->kategoria) && key_exists($towar->kategoria[0]->id,$aktywneKody[$priceCode]['kategoria']))){
                    $rabat = $aktywneKody[$priceCode]['rabat']/100;
                    $itemPrice = round($itemPrice - ($itemPrice * $rabat),2);
                }
            }
        }

        return ($label ? $this->cena($itemPrice) : $itemPrice);
    }

    public function itemPriceArr($towar) {
        $itemPrice = 0;
        $vat = 0;
        $return = [];
        if (!empty($towar->towar_cena_default)) {
            $etykietyPromocji = explode(',', Configure::read('ceny.etykietyPromocji'));
            $promocja = false;
            if (!empty($etykietyPromocji)) {
                foreach ($etykietyPromocji as $etykieta) {
                    if (!empty(trim($etykieta))) {
                        if (!empty($towar->{trim($etykieta)})) {
                            $promocja = true;
                            break;
                        }
                    }
                }
            }
            $itemPrice = $this->cenaVat((($promocja && !empty($towar->towar_cena_default->cena_promocja)) ? $towar->towar_cena_default->cena_promocja : $towar->towar_cena_default->cena_sprzedazy), $towar->towar_cena_default->vat->stawka, $towar->towar_cena_default->rodzaj, \Cake\Core\Configure::read('ceny.rodzaj'), false);
            $vat = $towar->towar_cena_default->vat->stawka;
            $return = ['price' => $itemPrice, 'vat' => $vat, 'vat_id' => $towar->towar_cena_default->vat->id, 'jednostka' => ['id' => $towar->towar_cena_default->jednostka->id, 'jednostka' => $towar->towar_cena_default->jednostka]];
        }

        return $return;
    }

    public function itemPriceDef($towar, $label = false) {

        $itemPrice = $this->cenaVat(
                $towar->towar_cena_default->cena_sprzedazy,
                $towar->towar_cena_default->vat->stawka,
                $towar->towar_cena_default->rodzaj, \Cake\Core\Configure::read('ceny.rodzaj'),
                false);
        return ($label ? $this->cena($itemPrice) : $itemPrice);
    }

    public function towarZdjecieSrc($id, $name, $thumb = '', $fullUrl = false) {
//        return \Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'image', $id, $thumb . $name, 'prefix' => false], $fullUrl);

        $filePath = Configure::read('filePath');
        $displayPath = Configure::read('displayPath');
        $imageSrc = '';
        if (file_exists($filePath['towar'] . $this->getKatalog($id) . DS . $thumb . $name)) {
            $imageSrc = $displayPath['towar'] . $this->getKatalog($id) . '/' . $thumb . $name; //$this->towarZdjecieSrc($towarZdjecie->id, $towarZdjecie->plik, $thumb);
        } else {
            $imageSrc = $displayPath['img'] . 'noPhoto.png';
        }
        return \Cake\Routing\Router::url($imageSrc, $fullUrl);
    }

    public function towarZdjecie($towar, $thumb = 'thumb_', $lazy = false) {
        $filePath = Configure::read('filePath');
        $displayPath = Configure::read('displayPath');
        if (empty($towar)) {
            return $this->Html->image($displayPath['img'] . 'noPhoto.png', ['alt' => null]);
        }
        $alt = $towar->nazwa;
        $towarZdjecie = (!empty($towar->zdjecie)?$towar->zdjecie:(!empty($towar->towar_zdjecie)?$towar->towar_zdjecie[0]:null));
        if (empty($towarZdjecie)) {
            $imageSrc = $displayPath['img'] . 'noPhoto.png';
            return $this->Html->image($imageSrc, ['alt' => $alt]);
        }
        $loadImage = $displayPath['img'] . 'img_load.gif';
        $imageSrc = '';
        if (file_exists($filePath['towar'] . $this->getKatalog($towarZdjecie->id) . DS . $thumb . $towarZdjecie->plik)) {
            $imageSrc = $displayPath['towar'] . $this->getKatalog($towarZdjecie->id) . '/' . $thumb . $towarZdjecie->plik; //$this->towarZdjecieSrc($towarZdjecie->id, $towarZdjecie->plik, $thumb);
        } elseif (!empty($towarZdjecie->remote_url)) {
            $imageSrc = $towarZdjecie->remote_url;
        }
        if (!empty($towarZdjecie->alt)) {
            $alt = $towarZdjecie->alt;
        }

        if (empty($imageSrc)) {
            $imageSrc = $displayPath['img'] . 'noPhoto.png';
        }
        if ($lazy) {
            return $this->Html->image($loadImage, ['alt' => $alt, 'lazy-image' => $imageSrc]);
        } else {
            return $this->Html->image($imageSrc, ['alt' => $alt]);
        }
    }

    public function kodRabatowyHtml($checkCode, $kodRabatowy, $rabatValue) {
        return $this->Html->tag('div', $this->Form->control('promocja_kod', ['type' => 'hidden', 'value' => $kodRabatowy]) . $this->Form->control('promocja_kod_value', ['type' => 'hidden', 'value' => $rabatValue]) . $this->Form->control('promocja_kod_typ', ['type' => 'hidden', 'value' => $checkCode->typ]) . $this->Translation->get('koszyk.KodRabatowy{kod}{rabat}', ['kod' => $this->Html->tag('span', $kodRabatowy), 'rabat' => (($checkCode->typ == 'kwota') ? $this->Html->tag('span', $this->cena($rabatValue)) : $this->Html->tag('span', $rabatValue . '%'))], true), ['class' => 'rabatKodVal']);
    }

    public function generatePassword($length = 8) {
        $code = [];
        $code[] = $this->generateRandomSpecialChar(1);
        $length--;
        for ($i = 1; $i <= $length; $i++) {
            $code[] = $this->generateRandomStringAll(1);
        }
        shuffle($code);
        return join('', $code);
    }

    public function getBonCode($length = 6) {
        $code = [];
        for ($i = 1; $i <= $length; $i++) {
            $code[] = $this->generateRandomStringUpper(1);
        }
        shuffle($code);
        return join('', $code);
    }

    public function getPin($length = 4) {
        $code = [];
        for ($i = 1; $i <= $length; $i++) {
            $code[] = $this->generateRandomNumber(1);
        }
        shuffle($code);
        return join('', $code);
    }

    public function randomOrderNumber($length = 6) {
        $code = [];
        for ($i = 1; $i <= $length; $i++) {
            $code[] = $this->generateRandomStringUpper(1);
        }
        shuffle($code);
        return join('', $code);
    }

    public function validateEmail($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    private function generateRandomString($length = 10) {
        return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
    }

    private function generateRandomStringAll($length = 10) {
        return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!#%@$*', ceil($length / strlen($x)))), 1, $length);
    }

    private function generateRandomSpecialChar($length = 1) {
        return substr(str_shuffle(str_repeat($x = '!#%@$*', ceil($length / strlen($x)))), 1, $length);
    }

    private function generateRandomStringUpper($length = 10) {
        return substr(str_shuffle(str_repeat($x = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
    }

    private function generateRandomNumber($length = 10) {
        return substr(str_shuffle(str_repeat($x = '0123456789', ceil($length / strlen($x)))), 1, $length);
    }

    private function generateRandomStringAlphaUpper($length = 10) {
        return substr(str_shuffle(str_repeat($x = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
    }

    private function generateRandomStringAlpha($length = 10) {
        return substr(str_shuffle(str_repeat($x = 'abcdefghijklmnopqrstuvwxyz', ceil($length / strlen($x)))), 1, $length);
    }

    public function datetimeDiff($dt1, $dt2) {
        $t1 = strtotime($dt1);
        $t2 = strtotime($dt2);

        $dtd = new \stdClass();
        $dtd->interval = $t2 - $t1;
        $dtd->total_sec = abs($t2 - $t1);
        $dtd->total_min = floor($dtd->total_sec / 60);
        $dtd->total_hour = floor($dtd->total_min / 60);
        $dtd->total_day = floor($dtd->total_hour / 24);

        $dtd->day = $dtd->total_day;
        $dtd->hour = $dtd->total_hour - ($dtd->total_day * 24);
        $dtd->min = $dtd->total_min - ($dtd->total_hour * 60);
        $dtd->sec = $dtd->total_sec - ($dtd->total_min * 60);
        return $dtd;
    }

    public function printDate($item, $format = 'Y-m-d', $onEmpty = '', $timestamp = false) {
        if (empty($item)) {
            if ($timestamp) {
                return FALSE;
            }
            return $onEmpty;
        }
        $date = '';
        if (is_object($item)) {
            $date = $item->format($format);
        } else {
            $date = date($format, strtotime($item));
        }
        if ($timestamp) {
            return strtotime($date);
        } else {
            return $date;
        }
    }

    public function getDayName($date, $w = false) {
        $day = $this->printDate($date, 'N'); //date('N', strtotime($date));
        $days = [
            1 => $this->Translation->get('dniTygodnia.' . ($w ? 'w_' : '') . 'poniedzialek'),
            2 => $this->Translation->get('dniTygodnia.' . ($w ? 'w_' : '') . 'wtorek'),
            3 => $this->Translation->get('dniTygodnia.' . ($w ? 'w_' : '') . 'sroda'),
            4 => $this->Translation->get('dniTygodnia.' . ($w ? 'w_' : '') . 'czwartek'),
            5 => $this->Translation->get('dniTygodnia.' . ($w ? 'w_' : '') . 'piatek'),
            6 => $this->Translation->get('dniTygodnia.' . ($w ? 'w_' : '') . 'sobota'),
            7 => $this->Translation->get('dniTygodnia.' . ($w ? 'w_' : '') . 'niedziela'),
        ];
        return $days[$day];
    }

    public function dateInfo($date, $pastLabel = null) {
        $time = $this->printDate($date, 'Y-m-d H:i:s', '', true);
        $now = time();
        $today = $this->printDate(new \DateTime(), 'Y-m-d 23:59:59', '', true);
        $tomorow = $this->printDate(new \DateTime('+ 1 day'), 'Y-m-d 23:59:59', '', true);
        $week = $this->printDate(new \DateTime('+ 6 day'), 'Y-m-d 23:59:59', '', true);
        if ($time < $now) {
            return (!empty($pastLabel) ? $pastLabel : '');
        } elseif ($time <= $today) {
            return t('labels.dzisiaj');
        } elseif ($time <= $tomorow) {
            return t('labels.jutro');
        } elseif ($time <= $week) {
            return $this->getDayName($date, true);
        }
        return $this->printDate($date, 'Y-m-d');
    }

    public function deliveryDay($date = null, $blockedDates = []) {
        if (empty($date)) {
            $date = date('Y-m-d');
        }
        if (empty($blockedDates)) {
            $blockedDays = explode('|', Configure::read('dostawa.block_days'));
            if (!empty($blockedDays)) {
                foreach ($blockedDays as $blockDate) {
                    $tmpDate = date('Y-m-d', strtotime($blockDate . '-' . date('Y')));
                    $blockedDates[$tmpDate] = $tmpDate;
                    $tmpDate = date('Y-m-d', strtotime($blockDate . '-' . date('Y', strtotime('+ 1 year'))));
                    $blockedDates[$tmpDate] = $tmpDate;
                }
            }
        }
        $day = date('N', strtotime($date));
        if ($day > 5) {
            if ($day == 6) {
                $date = date('Y-m-d', strtotime($date . ' + 2 days'));
            } else {
                $date = date('Y-m-d', strtotime($date . ' + 1 days'));
            }
        }
        $wysylkaTime = Configure::read('dostawa.maxTime');
        if (empty($wysylkaTime)) {
            $wysylkaTime = '12:00:00';
        }
        if (!empty($blockedDates) && key_exists($date, $blockedDates)) {
            return $this->deliveryDay(date('Y-m-d', strtotime($date . ' + 1 day')), $blockedDates);
        }
        if (time() > strtotime($date . ' ' . $wysylkaTime) || date('N', strtotime($date)) > 5) {
            return $this->deliveryDay(date('Y-m-d', strtotime($date . ' + 1 day')), $blockedDates);
        }
        return $date . ' ' . $wysylkaTime;
    }

    public function getBlockDateItem($date, $label) {
        if (empty($date) || empty($label)) {
            return '';
        }
        return $this->Html->tag('li', $label . '<i class="fa fa-times removeBlockDay" confirm-message="' . $this->printAdmin(__('Admin | Czy na pewno chcesz odblokować datę {0}?', [$label])) . '"></i>', ['class' => 'block-day-item', 'date' => $date]);
    }

    public function kurierLink($kurier, $nrListu, $label = '') {
        if (empty($kurier) || empty($nrListu)) {
            return '';
        }
        $link = \Cake\Core\Configure::read($kurier . '.link_sledzenia');
        if (empty($link)) {
            return '';
        }
        $link = $link . $nrListu;
        return $this->Html->link((!empty($label) ? $label : $link), $link, ['target' => '_blank']);
    }

    public function towarViewUrl($towar, $full = false, $urlQuery = []) {
        $urlArr = ['controller' => 'Towar', 'action' => 'view', $towar['id'], $this->friendlyUrl((!empty($towar['friendly_url']) ? $towar['friendly_url'] : $towar['nazwa'])), 'prefix' => false];
        if (!empty($urlQuery)) {
            foreach ($urlQuery as $queryName => $queryValue) {
                if ($queryName == 'action' || $queryName == 'controller') {
                    $urlArr[][$queryName] = $queryValue;
                } else {
                    $urlArr[$queryName] = $queryValue;
                }
            }
        }
        return \Cake\Routing\Router::url($urlArr, $full);
    }

    public function helpInfo($type, $field) {
        $helepTekst = Configure::read('helpInfoText');
        if (!empty($helepTekst) && key_exists($type, $helepTekst) && !empty($helepTekst[$type][$field])) {
            return $this->Html->tag('span', '<i class="fa fa-info"></i>', ['class' => 'help-info', 'data-toggle' => 'popover-info', 'data-placement' => 'auto', 'data-container' => 'body', 'data-content' => str_replace('"', "'", $helepTekst[$type][$field]), 'data-html' => 'true', 'data-trigger' => 'hover | click']);
        } else {
            return '';
        }
    }

    public function wysylkaCena($waga = 0) {
        return $this->cena(24, null, true);
    }

    public function odmiana($num = 0, $labels = [], $onlyLabel = false) {
        if (empty($labels)) {
            return false;
        }
        if (empty($num)) {
            $num = 0;
        }
        if ($num == 1) {
            return (!$onlyLabel ? $num . ' ' : '') . $labels[0];
        }
        if ($num >= 10 && $num <= 20) {
            return (!$onlyLabel ? $num . ' ' : '') . $labels[2];
        }
        $lastNum = (int) substr((string) $num, (strlen($num) - 1), 1);
        if ($lastNum > 1 && $lastNum <= 4) {
            return (!$onlyLabel ? $num . ' ' : '') . $labels[1];
        } else {
            return (!$onlyLabel ? $num . ' ' : '') . $labels[2];
        }
    }

    public function arrayToXml($array, $rootElement = null, $xml = null) {
        $_xml = $xml;

        // If there is no Root Element then insert root 
        if ($_xml === null) {
            $_xml = new \SimpleXMLElement($rootElement !== null ? $rootElement : '<root/>');
        }

        // Visit all key value pair 
        foreach ($array as $k => $v) {
            if (is_numeric($k)) {
                $k = 'item_' . $k;
            }
            // If there is nested array then 
            if (is_array($v)) {

                // Call function for nested array 
                $this->arrayToXml($v, $k, $_xml->addChild($k));
            } else {

                // Simply add child element.  
//                $_xml->addChild($k, '<![CDATA['.$v.']]>');

                $child = $_xml->{$k};
                if (empty($child)) {
                    $child = $xml->addChild($k);
                }

                $node = dom_import_simplexml($child);
                $owner = $node->ownerDocument;
                $node->appendChild($owner->createCDATASection($v));
            }
        }

        return $_xml->asXML();
    }
    public function subiektDate($str) {
        //20200401000000
        if(empty($str)){
            return null;
        }
        $y=substr($str, 0,4);
        $m= substr($str, 4,2);
        $d= substr($str, 6,2);
        return new \DateTime(__('{0}-{1}-{2}',[$y,$m,$d]));
    }
}
