<?php

namespace App\View\Helper;

use Cake\View\Helper;

class BannerHelper extends Helper {

    public $helpers = ['Html', 'Form'];

    public function elements($slide) {
        if (empty($slide)) {
            return false;
        }
        if (!empty($slide->banner_slides_elements)) {
            $returnElementAbsolute = [];
            $returnElementRelative = [];
            $returnElement = [];
            foreach ($slide->banner_slides_elements as $slideElement) {
                if ($slideElement->position == 'absolute')
                    $returnElementAbsolute[] = $this->element($slideElement);
                else
                    $returnElementRelative[] = $this->element($slideElement);
            }
            if (!empty($returnElementAbsolute)) {
                $returnElement[] = $this->Html->tag('div', join('', $returnElementAbsolute), ['class' => 'slideElementContainer']);
            }
            if (!empty($returnElementRelative)) {
                $returnElement[] = join('', $returnElementRelative);
            }
            if (!empty($returnElement)) {
                return join('', $returnElement);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function element($slideElement) {
        if (!empty($slideElement)) {
            $defStyle='';
            if($slideElement->position=='absolute'){
                $defStyle='left:' . ($slideElement->pos_left) . '%;top:' . $slideElement->pos_top . '%;' . (($slideElement->_width !== '') ? ' width:' . $slideElement->_width . 'px;' : '') . (($slideElement->_height !== '') ? ' height:' . $slideElement->_height . 'px;' : '');
            }
            $slideElementParams = ['class' => 'banner-caption', 'id' => 'slide-element-' . $slideElement->banner_slides_id . '-' . $slideElement->id, 'position' => $slideElement->position, 'style' => $defStyle . (!empty($slideElement->css) ? str_replace(["\r", "\n", "\t"], ['', '', ''], $slideElement->css) : '')];
            if ($slideElement->type == 'file') {
                $elementItem = $this->Html->image('/files/banner_slides_elements/' . $slideElement->file);
            } else {
                $elementItem = $slideElement->text;
            }
            if (!empty($slideElement->is_linked)) {
                $slideElementParams['escape'] = false;
                $slideElementParams['target'] = $slideElement->target;
                $slideElementParams['title'] = $slideElement->title;
                return $this->Html->link($elementItem, $slideElement->link, $slideElementParams);
            } else {
                return $this->Html->tag('div', $elementItem, $slideElementParams);
            }
        } else
            return null;
    }

}
