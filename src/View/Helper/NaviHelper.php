<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\Routing\Router;

class NaviHelper extends Helper {

    public $helpers = ['Html', 'Form', 'Translation'];

    public function friendlyURL($url) {
        $tabela = array(
            //WIN
            "\xb9" => "a", "\xa5" => "A", "\xe6" => "c", "\xc6" => "C",
            "\xea" => "e", "\xca" => "E", "\xb3" => "l", "\xa3" => "L",
            "\xf3" => "o", "\xd3" => "O", "\x9c" => "s", "\x8c" => "S",
            "\x9f" => "z", "\xaf" => "Z", "\xbf" => "z", "\xac" => "Z",
            "\xf1" => "n", "\xd1" => "N",
            //UTF
            "\xc4\x85" => "a", "\xc4\x84" => "A", "\xc4\x87" => "c", "\xc4\x86" => "C",
            "\xc4\x99" => "e", "\xc4\x98" => "E", "\xc5\x82" => "l", "\xc5\x81" => "L",
            "\xc3\xb3" => "o", "\xc3\x93" => "O", "\xc5\x9b" => "s", "\xc5\x9a" => "S",
            "\xc5\xbc" => "z", "\xc5\xbb" => "Z", "\xc5\xba" => "z", "\xc5\xb9" => "Z",
            "\xc5\x84" => "n", "\xc5\x83" => "N",
            //ISO
            "\xb1" => "a", "\xa1" => "A", "\xe6" => "c", "\xc6" => "C",
            "\xea" => "e", "\xca" => "E", "\xb3" => "l", "\xa3" => "L",
            "\xf3" => "o", "\xd3" => "O", "\xb6" => "s", "\xa6" => "S",
            "\xbc" => "z", "\xac" => "Z", "\xbf" => "z", "\xaf" => "Z",
            "\xf1" => "n", "\xd1" => "N",
            //I to co nie potrzebne
            " " => "-", "$" => "-", "!" => "-", "@" => "-", "#" => "-", "%" => "-", "/" => "-","[" => "","]" => "","(" => "-",")" => "-");

        return strtolower(strtr($url, $tabela));
    }

    public function breadcrumb($crumbs = []) {
        if (empty($crumbs))
            return false;
//        $this->Html->addCrumb(ucfirst($this->Translation->get('breadcrumb.JestesTutaj')));
        $this->Html->addCrumb($this->Translation->get('breadcrumb.StronaGlowna'), '/');
        foreach ($crumbs as $crumbName => $crumb) {
            $this->Html->addCrumb(ucfirst(mb_strtolower($crumbName)), $crumb);
        }
        $retDiv=$this->Html->tag('div', $this->Html->tag('div', $this->Html->getCrumbList(['firstClass' => false, 'lastClass' => 'active', 'class' => 'breadcrumb']), ['class' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12']), ['class' => 'row']);
        $crumbLen=strlen(strip_tags($retDiv));
        $maxLen=180;
        if($crumbLen>$maxLen)
        {
            $rVal=$crumbLen-$maxLen;
            $tmpValue=$this->Html->_crumbs[count($this->Html->_crumbs)-1][0];
            $cutValue=substr($tmpValue,0,  strlen($tmpValue)-$rVal);
            $lastSpacePos=strripos ($cutValue,' ');
            $tmpValue=substr($cutValue,0,  $lastSpacePos);
            $this->Html->_crumbs[count($this->Html->_crumbs)-1][0]=$tmpValue.'...';
            $retDiv=$this->Html->tag('div', $this->Html->tag('div', $this->Html->getCrumbList(['firstClass' => false, 'lastClass' => 'active', 'class' => 'breadcrumb']), ['class' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12']), ['class' => 'row']);
        }
        return $retDiv;
    }

    public function getIcon($iconClass = '') {
        return $this->Html->tag('span', ' ', ['class' => 'glyphicon ' . $iconClass, 'aria-hidden' => 'true']);
    }

    public function highlightStr($haystack, $needle, $highlightColorValue) {
        // return $haystack if there is no highlight color or strings given, nothing to do.
        if (strlen($highlightColorValue) < 1 || strlen($haystack) < 1 || strlen($needle) < 1) {
            return $haystack;
        }
        preg_match_all("/$needle+/i", $haystack, $matches);
        if (is_array($matches[0]) && count($matches[0]) >= 1) {
            foreach ($matches[0] as $match) {
                $haystack = str_replace($match, '<span style="background-color:' . $highlightColorValue . ';">' . $match . '</span>', $haystack);
            }
        }
        return $haystack;
    }

    public function boldStr($haystack, $needle) {
        if($needle=='b' || $needle=='B'){
            return $haystack;
        }
        preg_match_all("/$needle+/i", $haystack, $matches);
        if (is_array($matches[0]) && count($matches[0]) >= 1) {
            foreach ($matches[0] as $match) {
                $haystack = str_replace($match, '<b>' . $match . '</b>', $haystack);
            }
        }
        return $haystack;
    }

    public function categoriesTree($categories = [], $limit = 3,$activeId=null,$producent_id=null,$action='index') {
        if (empty($categories) || $limit==0)
            return false;
        $list='';
        foreach ($categories as $catId => $category) {
            if(empty($producent_id)) $list.=$this->Html->tag('li',$this->Html->link('<span class="caret"></span> '.$category['nazwa'],['controller'=>'towar','action'=>$action,$catId,$this->friendlyURL($category['nazwa'])],['escape'=>false]).((!empty($category['sub']))?$this->Html->tag('ul',$this->categoriesTree($category['sub'],($limit-1),$activeId,null,$action),['class'=>'dropdown-menu','role'=>'menu']):''),['class'=>((!empty($activeId)&&$activeId==$category['id'])?'open active':'')]);
            else $list.=$this->Html->tag('li',$this->Html->link('<span class="caret"></span> '.$category['nazwa'],['controller'=>'towar','action'=>$action,$producent_id,$catId,$this->friendlyURL($category['nazwa'])],['escape'=>false]).((!empty($category['sub']))?$this->Html->tag('ul',$this->categoriesTree($category['sub'],($limit-1),$activeId,$producent_id,$action),['class'=>'dropdown-menu','role'=>'menu']):''),['class'=>((!empty($activeId)&&$activeId==$category['id'])?'open active':'')]);
        }
        return $list;
    }
    public function getAllCatByUl($categories = [], $limit = 20,$activeId=null,$producent_id=null,$action='index',$preorder=false)
    {
        if (empty($categories) || $limit==0)
            return false;
        $list='';
        foreach ($categories as $catId => $category) {
            if(empty($producent_id)) $list.=$this->Html->tag('li',$this->Html->link($this->Html->tag('span',$category['nazwa']),['controller'=>'Towar','action'=>$action,((!empty($category['preorder']) || $preorder)?'preorders':$catId),($preorder?$catId:(!empty($category['preorder'])?null:$this->friendlyURL($category['nazwa']))),($preorder?$this->friendlyURL($category['nazwa']):null)],['escape'=>false,'title'=>$category['nazwa']]).((!empty($category['sub']))?$this->Html->tag('ul',$this->getAllCatByUl($category['sub'],($limit-1),$activeId,null,$action,(!empty ($category['preorder']))),['class'=>'sub-count-'. ceil(count($category['sub'])/11)]):''),['class'=>((!empty ($activeId) && $activeId==$category['id'])?'curent'.(!empty($category['sub'])?' open':''):'')]);
            else $list.=$this->Html->tag('li',$this->Html->link($category['nazwa'],['controller'=>'Towar','action'=>$action,$producent_id,$catId,$this->friendlyURL($category['nazwa'])],['escape'=>false]).((!empty($category['sub']))?$this->Html->tag('ul',$this->getAllCatByUl($category['sub'],($limit-1),$activeId,$producent_id,$action),[]):''),['class'=>((!empty ($activeId) && $activeId==$category['id'])?'open curent':'')]);
        }
        return $list;
    }
    public function getAllCatByUlFull($categories = [], $limit = 2,$activeId=null,$action='index',$preorder=false,$subMenuElements=[],$baseLimit=2)
    {
        if (empty($categories) || $limit==0)
            return false;
        $list='';
        $elemCount=0;
        foreach ($categories as $catId => $category) {
            $elemCount++;
            $list.=$this->Html->tag('li',$this->Html->link((!empty($category['icon_class'])?$this->Html->tag('i','',['class'=>'cat-ico flat '.$category['icon_class']]):'').$this->Html->tag('span',$category['nazwa']),['controller'=>'Towar','action'=>$action,((!empty($category['preorder']) || $preorder)?'preorders':$catId),($preorder?$catId:(!empty($category['preorder'])?null:$this->friendlyURL($category['nazwa']))),($preorder?$this->friendlyURL($category['nazwa']):null)],['escape'=>false,'title'=>$category['nazwa']]).((!empty($category['sub']))?$this->Html->tag('div',$this->Html->tag('ul',$this->getAllCatByUlFull($category['sub'],($limit-1),$activeId,$action,(!empty($category['preorder'])),$subMenuElements,$baseLimit),['class'=>'sub-count-'. ceil(count($category['sub'])/11)]).(!empty($subMenuElements[$catId])?$this->Html->tag('div',(!empty($category['ikona'])?$this->Html->tag('div',$this->Html->image(c('displayPath.kategoria_ikony').$category['ikona']),['class'=>'kat-img']):'').$this->Html->tag('div',t('labels.polecanyProduktWTejKategorii'),['class'=>'polecany-label']).$subMenuElements[$catId],['class'=>'submenu-element']):''),['class'=>'sub-main-menu level-'.$limit.' _'.$elemCount.' '.(($elemCount>5)?' to-right':'')]):''),['class'=>(!empty($subMenuElements[$catId])?'with-promo-item':'no-promo-item')]);
        }
        return $list;
    }
    public function getAllCatByUlMobile($categories = [], $limit = 20,$activeId=null,$action='index',$preorder=false,$dropDown=false)
    {
        if (empty($categories) || $limit==0)
            return false;
        $list='';
        foreach ($categories as $catId => $category) {
            if(!empty($category['sub']) && !$dropDown && $limit > 1){
                    $list.=$this->Html->tag('li',$this->Html->link($category['nazwa'],['controller'=>'towar','action'=>$action,((!empty($category['preorder']) || $preorder)?'preorders':$catId),($preorder?$catId:(!empty($category['preorder'])?null:$this->friendlyURL($category['nazwa']))),($preorder?$this->friendlyURL($category['nazwa']):null)],['escape'=>false,'title'=>$category['nazwa'],'class'=>'nav-link dropdown-toggle'.(($catId==$activeId)?' active':''),'id'=>'navbarDropdownCategory-'.$catId, 'role'=>'button', 'data-toggle'=>'dropdown', 'aria-haspopup'=>'true', 'aria-expanded'=>'false']).((!empty($category['sub']))?$this->Html->tag('div',$this->getAllCatByUlMobile($category['sub'],($limit-1),$activeId,$action,(!empty($category['preorder'])),true),['class'=>'dropdown-menu','aria-labelledby'=>'navbarDropdownCategory-'.$catId]):''),['class'=>'nav-item dropdown mobile-category'.(($catId==$activeId)?' active':'')]);
                
            }elseif($dropDown){
                $list.=$this->Html->link($category['nazwa'],['controller'=>'towar','action'=>$action,((!empty($category['preorder']) || $preorder)?'preorders':$catId),($preorder?$catId:(!empty($category['preorder'])?null:$this->friendlyURL($category['nazwa']))),($preorder?$this->friendlyURL($category['nazwa']):null)],['escape'=>false,'title'=>$category['nazwa'],'class'=>'dropdown-item'.(($catId==$activeId)?' active':'')]);
            }else{
                $list.=$this->Html->tag('li',$this->Html->link($category['nazwa'],['controller'=>'towar','action'=>$action,((!empty($category['preorder']) || $preorder)?'preorders':$catId),($preorder?$catId:(!empty($category['preorder'])?null:$this->friendlyURL($category['nazwa']))),($preorder?$this->friendlyURL($category['nazwa']):null)],['escape'=>false,'class'=>'nav-link'.(($catId==$activeId)?' active':''),'title'=>$category['nazwa']]),['class'=>'nav-item mobile-category'.(($catId==$activeId)?' active':'')]);
            }
            
        }
        return $list;
    }
    public function getAllCatByMobileDropdown($categories = [],$activeId=null)
    {
        if (empty($categories))
            return false;
        $list='';
        foreach ($categories as $catId => $category) {
            $list.=$this->Html->link($category['nazwa'],['controller'=>'towar','action'=>'lista',$catId,$this->friendlyURL($category['nazwa'])],['escape'=>false,'title'=>$category['nazwa'],'class'=>'dropdown-item'.(($catId==$activeId)?' active':'')]);
            }
        return $list;
    }
    public function shortText($str='',$length=50,$end='...')
    {
        if(strlen($str)<=$length) return $str;
        $tmp=substr($str, 0, $length);
        return substr($str,0,strrpos($tmp,' ')).(($length<strlen($str))?$end:'');
    }
    public function categoriesTreeInput($categories = [], $kategoriaSelected = [],$preorderSelected=null,$labelLink=false,$preorder=false) {
        if (empty($categories))
            return false;
        $list='';
        foreach ($categories as $catId => $category) {
            $linkUrl=['controller'=>'towar','action'=>'index',((!empty($category['preorder']) || $preorder)?'preorders':$category['id']),($preorder?$category['id']:(!empty($category['preorder'])?null:$this->friendlyURL($category['nazwa']))),($preorder?$this->friendlyURL($category['nazwa']):null)];
//                     ['controller'=>'Towar','action'=>'index',$category['id'],$this->friendlyURL($category['nazwa'])]
            $checked=((empty($category['preorder']) && !$preorder && !empty($kategoriaSelected[$category['id']]))?true:false);
            if(!empty($preorderSelected) && !empty($category['preorder']) || $preorder){
                if($preorderSelected=='all' && !$preorder){
                    $checked=true;
                }
                if($preorder && $preorderSelected==$category['id']){
                    $checked=true;
                }
            }
            $list.=$this->Html->tag('li',$this->Form->input('kategoria._ids[]',['name'=>'kategoria[_ids][]','label'=>['text'=>($labelLink?$this->Html->link($category['nazwa'],$linkUrl):$category['nazwa']),'escape'=>false],'id'=>'kategoria-ids-'.$category['id'],'hiddenField'=>false,'value'=>$category['id'],'checked'=>$checked,'type'=>'checkbox']).((!empty($category['sub']))?$this->Html->tag('ul',$this->categoriesTreeInput($category['sub'],$kategoriaSelected,$preorderSelected,$labelLink,(!empty($category['preorder']))),['class'=>'sub']):''),[]);
        }
        return $list;
    }
    public function categoriesTreeInputAdmin($categories = [], $kategoriaSelected = [],$labelLink=false) {
        if (empty($categories))
            return false;
        $list='';
        foreach ($categories as $catId => $category) {
            $list.=$this->Html->tag('li',(!empty($category['sub'])?'<i class="caret"></i>':'').$this->Form->input('kategoria._ids[]',['name'=>'kategoria[_ids][]','label'=>['text'=>($labelLink?$this->Html->link($category['nazwa'],['controller'=>'Towar','action'=>'index',$category['id'],$this->friendlyURL($category['nazwa'])]):$category['nazwa']),'escape'=>false],'id'=>'kategoria-ids-'.$category['id'],'hiddenField'=>false,'value'=>$category['id'],'checked'=>(!empty($kategoriaSelected[$category['id']])?true:false),'type'=>'checkbox']).((!empty($category['sub']))?$this->Html->tag('ul',$this->categoriesTreeInputAdmin($category['sub'],$kategoriaSelected,$labelLink),['class'=>'sub']):''));
        }
        return $list;
    }
    public function towarViewUrl($towar,$full=false){
        return Router::url(['controller'=>'Towar','action'=>'view',$towar->id,$this->friendlyUrl($towar->nazwa)],$full);
    }
}
