<?php

namespace Cake\View\Helper;

use Cake\Core\Configure;
use Cake\Network\Response;
use Cake\View\Helper;
use Cake\View\StringTemplateTrait;
use Cake\View\View;

class ActionHelper extends Helper {

    public $helpers = ['Html', 'Form', 'Navi'];

    public function create($actions = []) {
        $ikony = Configure::read('akcje.ikony');
        $act = '';
        if (!empty($actions)) {
            foreach ($actions as $action => $options) {
                $tmpAct = explode('-', $action);
                $txtAction = $action;
                if (count($tmpAct) > 1) {
                    $_action = [
                        'controller' => $tmpAct[0],
                        'action' => $tmpAct[1]
                    ];
                    $txtAction = $tmpAct[1];
                }
                else
                    $_action = [
                        'action' => $action
                    ];
                if (!empty($options['controller'])) {
                    $_action = ['controller' => $options['controller'], 'action' => $action];
                }
                if (!empty($options['pass']) && is_array($options['pass']))
                    $_action = array_merge($_action, $options['pass']);
                if (empty($options['options']))
                    $options['options'] = [];
                else{
                    if(!empty($options['options']['no-admin']))
                    {
                        $_action['prefix']=false;
                    }
                }
                if ($action == 'delete' || $action == 'send' || $action == 'delete_aukcja') {
                    if (empty($options['options']['class']))
                        $options['options']['class'] = 'btn btn-xs btn-danger';
                    else
                        $options['options']['class'] = 'btn btn-xs ' . $options['options']['class'];
                    if (!empty($options['options']['glyphicon']) && !empty($ikony)) {
                        if (empty($options['options']['title'])) {
                            $options['options']['title'] = (!empty($options['name']) ? $options['name'] : $txtAction);
                        }
                        $act.=(!empty($act) ? ' ' : '') . $this->Form->postLink($this->Navi->getIcon($options['options']['glyphicon']) . '<span class="sr-only">' . (!empty($options['name']) ? $options['name'] : $txtAction) . '</span>', $_action, $options['options']);
                    }
                    else
                        $act.=(!empty($act) ? ' ' : '') . $this->Form->postLink((!empty($options['name']) ? $options['name'] : $action), $_action, $options['options']);
                }
                else {

                    if (empty($options['options']['class']))
                        $options['options']['class'] = 'btn btn-xs btn-default';
                    else
                        $options['options']['class'] = 'btn btn-xs ' . $options['options']['class'];
                    if (!empty($options['options']['glyphicon']) && !empty($ikony)) {
                        if (empty($options['options']['title'])) {
                            $options['options']['title'] = (!empty($options['name']) ? $options['name'] : $txtAction);
                        }
                        $act.=(!empty($act) ? ' ' : '') . $this->Html->link($this->Navi->getIcon($options['options']['glyphicon']) . '<span class="sr-only">' . (!empty($options['name']) ? $options['name'] : $txtAction) . '</span>', $_action, $options['options']);
                    }
                    else
                        $act.=(!empty($act) ? ' ' : '') . $this->Html->link((!empty($options['name']) ? $options['name'] : $txtAction), $_action, $options['options']);
                }
            }
        }
        return $act;
    }

}

?>
