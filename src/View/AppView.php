<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\View;

use Cake\View\View;

/**
 * Application View
 *
 * Your application’s default view class
 *
 * @link http://book.cakephp.org/3.0/en/views.html#the-app-view
 */
class AppView extends View {

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading helpers.
     *
     * e.g. `$this->loadHelper('Html');`
     *
     * @return void
     */
    public function initialize() {
        $defFormTemplate = [
            'inputContainer' => '<div class="form-group {{type}}{{required}} {{groupClass}}">{{content}}</div>',
            'inputContainerError' => '<div class="form-group {{type}}{{required}} {{groupClass}} error">{{content}}</div>',
            'formGroup' => '{{label}}{{error}}{{input}}',
            'error' => '<span class="error invalid-feedback">{{content}}</span>',
            'submitContainer' => '<div class="form-group submit">{{content}}</div>',
            'input' => '<input type="{{type}}" class="form-control" name="{{name}}"{{attrs}}/>',
            'textarea' => '<textarea class="form-control" name="{{name}}"{{attrs}}>{{value}}</textarea>',
            'select' => '<select class="form-control" name="{{name}}"{{attrs}}>{{content}}</select>',
            'selectMultiple' => '<select class="form-control" name="{{name}}[]" multiple="multiple"{{attrs}}>{{content}}</select>',
            'file' => '<div class="file-add"><input type="file" name="{{name}}"{{attrs}}><button type="button" class="btn btn-sm btn-info" {{btnAttr}}><i class="fa fa-plus"></i> _FILE_ADD_LABEL_</button><div class="upload-file-preview {{previewClass}}"  def-width="{{defWidth}}" def-height="{{defHeight}}"><img src="_FILE_SRC_"/></div></div>',
            
        ];
        if(empty($this->request->params['prefix'])){
            $frontTemplate=[
            'checkbox' => '<input type="checkbox" name="{{name}}" value="{{value}}"{{attrs}}>',
        'checkboxFormGroup' => '{{label}}',
        'checkboxWrapper' => '<div class="checkbox">{{label}}</div>',
             'nestingLabel' => '{{hidden}}{{input}}<label{{attrs}}>{{text}}</label>'    
            ];
            $defFormTemplate= array_merge($defFormTemplate,$frontTemplate);
        }
        $this->Form->setTemplates($defFormTemplate);
    }

}
