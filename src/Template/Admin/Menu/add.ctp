<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
                <?= $this->Form->create($menu) ?>
            <div class="x_title">
                    <h2><?=$this->Txt->printAdmin(__('Admin | Add {0}',[__('Admin | Menu')]))?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                                              <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> '.__('Admin | Wróć do {0}',[__('Admin | Listy')])), ['action' => 'index'],['escape'=>false,'class'=>'btn btn-xs btn-warning']) ?></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
            <div class="x_content">
    <fieldset>
        <?php
            $lngInputs=[];
            echo $this->Form->control('typ',['type'=>'select','options'=>$menuTypes,'label'=>$this->Txt->printAdmin(__('Admin | Miejsce umieszczenia linku'))]);
                        echo $this->Form->control('parent_id', ['options' => $parentMenu, 'empty' => true,'label'=>$this->Txt->printAdmin(__('Admin | Element nadrzędny')),'default'=>$parentId]);
                         echo $this->Form->control('jezyk_id', ['options'=> $_languages,'empty'=>$this->Txt->printAdmin(__('Admin | Wszystkie języki')),'label' => $this->Txt->printAdmin(__('Admin | Język'))]);
                 
                        echo $this->Form->control('nazwa',['label'=>$this->Txt->printAdmin(__('Admin | Etykieta linku'))]);
            echo $this->Form->control('url',['label'=>$this->Txt->printAdmin(__('Admin | Adres url'))]);
            echo $this->Form->control('title',['label'=>$this->Txt->printAdmin(__('Admin | Title'))]);
            echo $this->Form->control('target',['options'=>$targets,'label'=>$this->Txt->printAdmin(__('Admin | Target'))]);
            echo $this->Form->control('ukryty',['label'=>$this->Txt->printAdmin(__('Admin | Element niewidoczny na stronie'))]);
        ?>
    </fieldset>
            </div>
            <div class="x_footer text-right">
    <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')),['class'=>'btn btn-sm btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
