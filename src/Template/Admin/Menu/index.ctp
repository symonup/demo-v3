<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Menu[]|\Cake\Collection\CollectionInterface $menu
  */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Menu')).(!empty($parentMenu)?' - '.$parentMenu->nazwa:'') ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Html->tag('i','',['class'=>'fa fa-plus']).' '.$this->Txt->printAdmin(__('Admin | Dodaj {0}',[$this->Txt->printAdmin(__('Admin | element do menu'))])), ['action' => 'add',$parentId],['escape'=>false,'class'=>'btn btn-xs btn-primary']) ?></li>
                <?php if(!empty($parentMenu)){
                    ?>
                       <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> '.__('Admin | Wróć do {0}',[__('Admin | głównych linków')])), ['action' => 'index'],['escape'=>false,'class'=>'btn btn-xs btn-warning']) ?></li>
                    <?php
                }?>
                </ul>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped">
                    <thead>
            <tr>
                                <th data-priority="2"><?= $this->Txt->printAdmin(__('Admin | Id')) ?></th>
                                <th data-priority="4"><?= $this->Txt->printAdmin(__('Admin | Etykieta linku')) ?></th>
                                <th data-priority="5"><?= $this->Txt->printAdmin(__('Admin | Adres url')) ?></th>
                                <th data-priority="5"><?= $this->Txt->printAdmin(__('Admin | Linki podrzędne')) ?></th>
                                <th data-priority="8"><?= $this->Txt->printAdmin(__('Admin | Element ukryty')) ?></th>
                                <th data-priority="8"><?= $this->Txt->printAdmin(__('Admin | Typ')) ?></th>
                                <th data-priority="8"><?= $this->Txt->printAdmin(__('Admin | Język')) ?></th>
                                <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
            </tr>
        </thead>
        <tbody class="sorted-kolejnosc" sort-target="<?= Cake\Routing\Router::url(['action'=>'sort','kolejnosc'])?>">
            <?php foreach ($menu as $menu): ?>
            <tr sort-item-id="<?= $menu->id ?>">
                <td><?= $menu->id ?></td>
                <td><?= $menu->nazwa ?></td>
                <td><?= $menu->url ?></td>
                <td>
                    <?php if(!empty($menu->child_menu)){
                        $subItems=[];
     foreach ($menu->child_menu as $subMenu){
         $subItems[]=$subMenu->nazwa;
     }
     ?>
                    <a href="<?= Cake\Routing\Router::url(['action'=>'index',$menu->id])?>"><?= join('<br/>', $subItems)?></a>
                    <?php
                    } ?>
                </td>
                <td><?= $this->Txt->printBool($menu->ukryty, str_replace($basePath, '/', Cake\Routing\Router::url(['controller' => 'Menu', 'action' => 'setField', $menu->id, 'ukryty', !(bool) $menu->ukryty]))) ?></td>
                <td><?= $menuTypes[$menu->typ] ?></td>
                <td><?=(!empty($menu->jezyk_id)?$_languages[$menu->jezyk_id]:$this->Txt->printAdmin(__('Admin | Wszystkie')))?></td>
                <td class="actions">
                    <?php 
                    if(empty($parentMenu)){
                    echo $this->Html->link('<i class="fa fa-sort-amount-desc "></i>', ['action' => 'index', $menu->id],['escape'=>false,'class'=>'btn btn-default btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Linki podrzędne'))]);
                    }?>
                    <?= $this->Html->link('<i class="fa fa-cogs"></i>', ['action' => 'edit', $menu->id],['escape'=>false,'class'=>'btn btn-default btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Edytuj'))]) ?>
                    <?= $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $menu->id], ['confirm-btn-yes'=>$this->Txt->printAdmin(__('Admin | Tak')),'confirm-btn-no'=>$this->Txt->printAdmin(__('Admin | Nie')),'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', str_replace('"',"'",$menu->nazwa))),'escape'=>false,'class'=>'btn btn-danger btn-xs confirm-link','data-toggle'=>'tooltip', 'data-placement'=>'top','data-html'=>'true','title'=>$this->Txt->printAdmin(__('Admin | Usuń'))]) ?>
                <spna class="sort-item-point btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="<?=$this->Txt->printAdmin(__('Admin | Przeciągnij aby zmienić kolejność'))?>"><i class="fa fa-arrows-alt"></i></spna>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
                </table>
                <?php
                if(!empty($tips)){
                    echo $this->element('admin/tips',['tips'=>$tips]);
                }
                ?>
            </div>
        </div>
    </div>
</div>