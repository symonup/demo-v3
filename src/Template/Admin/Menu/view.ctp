<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Menu $menu
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Menu'), ['action' => 'edit', $menu->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Menu'), ['action' => 'delete', $menu->id], ['confirm' => __('Are you sure you want to delete # {0}?', $menu->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Menu'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Menu'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Parent Menu'), ['controller' => 'Menu', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Parent Menu'), ['controller' => 'Menu', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="menu view large-9 medium-8 columns content">
    <h3><?= h($menu->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Parent Menu') ?></th>
            <td><?= $menu->has('parent_menu') ? $this->Html->link($menu->parent_menu->title, ['controller' => 'Menu', 'action' => 'view', $menu->parent_menu->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nazwa') ?></th>
            <td><?= h($menu->nazwa) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Url') ?></th>
            <td><?= h($menu->url) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($menu->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Target') ?></th>
            <td><?= h($menu->target) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($menu->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Kolejnosc') ?></th>
            <td><?= $this->Number->format($menu->kolejnosc) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ukryty') ?></th>
            <td><?= $menu->ukryty ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Menu') ?></h4>
        <?php if (!empty($menu->child_menu)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Parent Id') ?></th>
                <th scope="col"><?= __('Nazwa') ?></th>
                <th scope="col"><?= __('Url') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Target') ?></th>
                <th scope="col"><?= __('Ukryty') ?></th>
                <th scope="col"><?= __('Kolejnosc') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($menu->child_menu as $childMenu): ?>
            <tr>
                <td><?= h($childMenu->id) ?></td>
                <td><?= h($childMenu->parent_id) ?></td>
                <td><?= h($childMenu->nazwa) ?></td>
                <td><?= h($childMenu->url) ?></td>
                <td><?= h($childMenu->title) ?></td>
                <td><?= h($childMenu->target) ?></td>
                <td><?= h($childMenu->ukryty) ?></td>
                <td><?= h($childMenu->kolejnosc) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Menu', 'action' => 'view', $childMenu->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Menu', 'action' => 'edit', $childMenu->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Menu', 'action' => 'delete', $childMenu->id], ['confirm' => __('Are you sure you want to delete # {0}?', $childMenu->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
