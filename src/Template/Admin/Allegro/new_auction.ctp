<?php
/**
 * @var \App\View\AppView $this
 */
$requestData=null;
if(!empty($allegroAukcja->item_id)){
    $requestData= json_decode($allegroAukcja->item_info, true);
}
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <?= $this->Form->create($allegroAukcja, ['id' => 'form-new-auction']) ?>
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Edit {0}', [__('Admin | Nowa aukcja allegro')])) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> ' . __('Admin | Wróć do {0}', [__('Admin | Listy')])), (!empty($allegroAukcja->item_id)?['action'=>'index']:['controller' => 'Towar', 'action' => 'index']), ['escape' => false, 'class' => 'btn btn-xs btn-warning']) ?></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php 
                if(!empty($towar->allegro_aukcja) && empty($allegroAukcja->item_id)){
                    $oldAuctions=[];
                    foreach ($towar->allegro_aukcja as $oldAukcja){
                        $oldAuctions[]=$oldAukcja->item_id;
                    }
                    ?>
                <div class="alert alert-danger">
                    <?= $this->Txt->printAdmin(__('Admin | Wybrany towar ma już przypisaną aukcje, czy na pewno chcesz dodać nową?')) ?>
                    <div><?= join('<br/>', $oldAuctions)?></div>
                </div>
                <?php
                }
                ?>
                <fieldset>
                    <?php
                    $allowSubmit=true;
                    if (empty($shippingRates)) {
                        $allowSubmit=false;
                        ?>
                        <div class="alert alert-danger">
                            <?= $this->Txt->printAdmin(__('Admin | Nie masz zdefiniowanego cennika dostaw, zaloguj się na swoje konto w allegro i skonfiguruj cennik dostaw aby móc wystawić aukcje.')) ?>
                        </div>
                        <?php
                    } else {
                        $lngInputs = [];
                        if(!empty($allegroAukcja->item_id)){
                            echo $this->Html->tag('div',$allegroAukcja->item_id,['id'=>'offer-id']);
                        }
                        echo $this->Form->control('name', ['value' => (!empty($requestData['name'])?$requestData['name']:$this->Txt->clearText($towar->nazwa,50)),'maxlength'=>50, 'type' => 'text', 'label' => $this->Txt->printAdmin(__('Admin | Nazwa aukcji/produktu'))]);
                        ?>
                        <button type="button" new-auction="true" class="btn btn-primary btn-xs setAllegroCategoryBtn" data-target="<?= \Cake\Routing\Router::url(['controller' => 'Allegro', 'action' => 'getCategories', $allegroKonto->id, 'modal' => 1]) ?>" input-target="#default-category" path-target="#default-category-path" path-id-target="default-category-path-id"><?= $this->Txt->printAdmin(__('Admin | Ustaw kategorie')) ?></button>
                        <div class="allegro-cat-path-html"><?= $allegroKonto->defaultCategoryPath ?></div>
                        <?php
                        echo $this->Form->control('category.id', ['allegro-cat-id' => 'true', 'data-target' => \Cake\Routing\Router::url(['controller' => 'Allegro', 'action' => 'getCategory']), 'value' => ((!empty($requestData) && !empty($requestData['category']['id']))?$requestData['category']['id']:$allegroKonto->defaultCategory), 'type' => 'hidden']);
                        ?>
                        <div id="category-params">

                        </div>
                        <?php
                        echo $this->Form->control('ean', ['value' => (!empty($requestData)?$requestData['ean']:$towar->ean), 'type' => 'text', 'label' => 'EAN']);
                        if(!empty($requestData['images'])){
                            ?>
                            <div class="images">
                                <?php
                            foreach ($requestData['images'] as $imgKey => $img){
                                ?>
                                    <div class="image-item">
                                        <img src="<?= $img['url'] ?>"/>
                                        <input type="hidden" class="image-to-send" name="images[][url]" value="<?= $img['url'] ?>"/>
                                    </div>
                                    <?php
                            }
                            ?>
                            </div>
                                <?php
                        }
                        elseif (!empty($towar->towar_zdjecie)) {
                            ?>
                            <div class="images">
                                <?php
                                foreach ($towar->towar_zdjecie as $zdjecie) {
                                    ?>
                                    <div class="image-item">
                                        <img src="<?= $displayPath['towar'] . $this->Txt->getKatalog($zdjecie['id']) . '/thumb_' . $zdjecie['plik'] ?>"/>
                                        <input type="hidden" class="image-to-send" image-id="<?= $zdjecie['id'] ?>" name="images[][url]" value="<?= $zdjecie['id'] ?>"/>
                                        <span class="btn btn-xs btn-danger remove"><i class="fa fa-trash"></i></span>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                            <div>
                                <hr/>
                                <button type="button" id="send-images" error-message="<?=$this->Txt->printAdmin(__('Admin | Wymagane jest wysłanie minimum 1 zdjęcia do allegro'))?>" data-target="<?= Cake\Routing\Router::url(['action' => 'uploadImages', $allegroKonto->id]) ?>" class="btn btn-sm btn-info"><i class="fa fa-upload reload-unactive"></i><i class="fa fa-spinner reload-active"></i>  <?=$this->Txt->printAdmin(__('Admin | Wyślij zdjęcia do allegro'))?></button>
                            </div>
                            
                            <?php
                        }
                            $key = 1;
                            
                            ?>
                            <div class="description">
                                <div class="description-items">
                                    <?php 
                                    if(!empty($requestData) && !empty($requestData['description'])){
                                        foreach ($requestData['description']['sections'] as $descSections){
                                            echo $this->element('admin/allegro/default_description_item', ['key' => $key,'descValue'=>$descSections['items']]);
                                            $key++;
                                        }
                                    }else{
                                      echo $this->element('admin/allegro/default_description_item', ['key' => $key,'descItemType'=>'text','defaultDesc'=> str_replace(['<p></p>','<p><br></p>',"\r","\n",'  '],['','','','',''],preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/i",'<$1$2>', strip_tags($towar->opis, '<br><br/><p><h1><h2><b><ul><ol><li>')))]);
                                    }
                                    ?>
                                </div>
                                <div class="text-center">
                                    <button type="button" id="add-description-line" key="<?= $key ?>" class="btn btn-info"><?= $this->Txt->printAdmin(__('Admin | Dodaj kolejny wiersz')) ?></button>
                                </div>
                            </div>
                        <div class="row">
                            <?php 
                            $shippingOptions=[];
                                                        foreach ($shippingRates as $sRate){
                                                            $shippingOptions[$sRate['id']]=$sRate['name'];
                                                        }
                                                $cena=0;
                                                $waluta='PLN';
                                    if (!empty($towar->towar_cena)) {
                                        foreach ($towar->towar_cena as $towarCena) {
                                            $cena=$towarCena->cena_sprzedazy;
                                            if($towarCena->has('walutum')){
                                            $waluta=$towarCena->walutum->symbol;
                                            }
                                            break;
                                        }
                                    }
                                    ?>
                            <div class="col-md-6">
                            <?php
                                    $sellingModeSelected=((!empty($requestData) && !empty($requestData['sellingMode']))?$requestData['sellingMode']['format']:'BUY_NOW');
                                                        echo $this->Form->control('sellingMode.format',['value'=>$sellingModeSelected,'type'=>'select','options'=>$sellingMode,'required'=>true,'label'=>$this->Txt->printAdmin(__('Admin | Rodzaj oferty'))]);
                                                        echo $this->Form->control('sellingMode.price.amount',['value'=>((!empty($requestData) && !empty($requestData['sellingMode']))?$requestData['sellingMode']['price']['amount']:$cena),'type'=>'text','data-type'=>'float','required'=>true,'label'=>$this->Txt->printAdmin(__('Admin | Cena'))]);
                                                        echo $this->Form->control('sellingMode.price.currency',['value'=>((!empty($requestData) && !empty($requestData['sellingMode']))?$requestData['sellingMode']['price']['currency']:$waluta),'type'=>'text','required'=>true,'label'=>$this->Txt->printAdmin(__('Admin | Waluta'))]);
                                                        echo $this->Form->control('sellingMode.startingPrice',['value'=>((!empty($requestData) && !empty($requestData['sellingMode']))?$requestData['sellingMode']['startingPrice']:''),'type'=>'text','data-type'=>'float','required'=>false,'label'=>$this->Txt->printAdmin(__('Admin | Cena wyjściowa (uzupełnij dla aukcji)'))]);
                                                        echo $this->Form->control('sellingMode.minimalPrice',['value'=>((!empty($requestData) && !empty($requestData['sellingMode']))?$requestData['sellingMode']['minimalPrice']:''),'type'=>'text','data-type'=>'float','required'=>false,'label'=>$this->Txt->printAdmin(__('Admin | Cena minimalna (uzupełnij dla aukcji)'))]);
                                                        
                                                        ?>
                            <div class="sellingModeDuration <?=(($sellingModeSelected!='BUY_NOW')?'hidden':'')?>" type="BUY_NOW">
                                <?=$this->Form->control('publication.duration',['value'=>((!empty($requestData) && !empty($requestData['publication']))?$requestData['publication']['duration']:''),'type'=>'select','options'=>$duration['buyNow'],'disabled'=>(($sellingModeSelected=='BUY_NOW')?false:true),'required'=>true,'label'=>$this->Txt->printAdmin(__('Admin | Czas trwania oferty'))])?>
                            </div>
                            <div class="sellingModeDuration <?=(($sellingModeSelected!='AUCTION')?'hidden':'')?>" type="AUCTION">
                                <?=$this->Form->control('publication.duration',['value'=>((!empty($requestData) && !empty($requestData['publication']))?$requestData['publication']['duration']:''),'type'=>'select','options'=>$duration['auction'],'disabled'=>(($sellingModeSelected=='AUCTION')?false:true),'required'=>true,'label'=>$this->Txt->printAdmin(__('Admin | Czas trwania oferty'))])?>
                            </div>
                            <div class="sellingModeDuration <?=(($sellingModeSelected!='ADVERTISEMENT')?'hidden':'')?>" type="ADVERTISEMENT">
                                <?=$this->Form->control('publication.duration',['value'=>((!empty($requestData) && !empty($requestData['publication']))?$requestData['publication']['duration']:''),'type'=>'select','options'=>$duration['advertisment'],'disabled'=>(($sellingModeSelected=='ADVERTISEMENT')?false:true),'required'=>true,'label'=>$this->Txt->printAdmin(__('Admin | Czas trwania oferty'))])?>
                            </div>
                                <?php 
                                echo $this->Form->control('publication.startingAt',['value'=>((!empty($requestData) && !empty($requestData['publication']))?$requestData['publication']['startingAt']:''),'type'=>'text','datepicker'=>'datetime','label'=>$this->Txt->printAdmin(__('Admin | Data rozpoczęcia sprzedaży (podaj jeżeli oferta ma być wystawiona w przyszłości)'))]);
                                                        echo $this->Form->control('publication.endingAt',['value'=>((!empty($requestData) && !empty($requestData['publication']))?$requestData['publication']['endingAt']:''),'type'=>'text','datepicker'=>'datetime','label'=>$this->Txt->printAdmin(__('Admin | Data zakończenia oferty'))]);
                                                        
                                                        echo $this->Form->control('stock.available',['value'=>((!empty($requestData) && !empty($requestData['stock']))?$requestData['stock']['available']:$towar->ilosc),'type'=>'number','required'=>true,'error-message'=>$this->Txt->printAdmin(__('Admin | Ilość musi być większa od 0')),'required'=>true,'label'=>$this->Txt->printAdmin(__('Admin | Ilość dostępna w ofercie'))]);
                                                        echo $this->Form->control('stock.unit',['value'=>((!empty($requestData) && !empty($requestData['stock']))?$requestData['stock']['unit']:''),'type'=>'select','options'=>$jednostki,'required'=>true,'label'=>$this->Txt->printAdmin(__('Admin | Jednostka'))]);
                                                        
                                ?>
                            </div>
                            <div class="col-md-6">
                            <?php
                                                        
                                                        
                                                        echo $this->Form->control('delivery.shippingRates.id',['value'=>((!empty($requestData) && !empty($requestData['delivery']))?$requestData['delivery']['shippingRates']['id']:''),'type'=>'select','options'=>$shippingOptions,'required'=>true,'label'=>$this->Txt->printAdmin(__('Admin | Cennik dostaw'))]);
                                                        echo $this->Form->control('delivery.handlingTime',['value'=>((!empty($requestData) && !empty($requestData['delivery']))?$requestData['delivery']['handlingTime']:''),'type'=>'select','options'=>$shippingTimes,'required'=>true,'label'=>$this->Txt->printAdmin(__('Admin | Czas wysyłki'))]);
                                                        echo $this->Form->control('delivery.additionalInfo',['value'=>((!empty($requestData) && !empty($requestData['delivery']))?$requestData['delivery']['additionalInfo']:''),'type'=>'textarea','required'=>false,'label'=>$this->Txt->printAdmin(__('Admin | Dodatkowe informacje'))]);
                                                        echo $this->Form->control('delivery.shipmentDate',['value'=>((!empty($requestData) && !empty($requestData['delivery']))?$requestData['delivery']['shipmentDate']:''),'type'=>'text','datepicker'=>'datetime','label'=>$this->Txt->printAdmin(__('Admin | Wysyłka od (np. dla przedsprzedaży)'))]);
                                                        echo $this->Form->control('payments.invoice',['value'=>((!empty($requestData) && !empty($requestData['payments']))?$requestData['payments']['invoice']:''),'type'=>'select','options'=>$invoiceOptions,'required'=>true,'label'=>$this->Txt->printAdmin(__('Admin | Faktura'))]);
//                                                        echo $this->Form->control('promotion',['type'=>'select','options'=>$promotionOptions,'multiple'=>'checkbox','label'=>$this->Txt->printAdmin(__('Admin | Wyróżnij ofertę'))]);
                                                        foreach ($promotionOptions as $promoKey => $promoName){
                                                            echo $this->Form->control('promotion.'.$promoKey,['value'=>((!empty($requestData) && !empty($requestData['promotion']))?$requestData['promotion'][$promoKey]:''),'type'=>'radio','options'=>[0=>'NIE',1=>'TAK'],'default'=>0,'label'=>$promoName]);
                                                        }
                                                        echo $this->Form->control('location.countryCode',['type'=>'select','value'=>((!empty($requestData) && !empty($requestData['location']))?$requestData['location']['countryCode']:$allegroKonto->kraj),'options'=> \Cake\Core\Configure::read('kraje'),'required'=>true,'label'=>$this->Txt->printAdmin(__('Admin | Kraj'))]);
                                                        echo $this->Form->control('location.province',['type'=>'select','value'=>((!empty($requestData) && !empty($requestData['location']))?$requestData['location']['province']:$allegroKonto->wojewodztwo),'options'=>$wojewodztwa,'value'=>'','required'=>true,'label'=>$this->Txt->printAdmin(__('Admin | Województwo'))]);
                                                        echo $this->Form->control('location.city',['type'=>'text','value'=>((!empty($requestData) && !empty($requestData['location']))?$requestData['location']['city']:$allegroKonto->miasto),'required'=>true,'label'=>$this->Txt->printAdmin(__('Admin | Miasto'))]);
                                                        echo $this->Form->control('location.postCode',['type'=>'text','value'=>((!empty($requestData) && !empty($requestData['location']))?$requestData['location']['postCode']:$allegroKonto->kod_pocztowy),'required'=>true,'label'=>$this->Txt->printAdmin(__('Admin | Kod pocztowy'))]);
                                                        ?>
                            </div>
                        </div>
                            <?php } ?>
                </fieldset>
            </div>
            <div class="x_footer text-right">
                <?= ($allowSubmit?$this->Form->button('<i class="fa fa-floppy-o reload-unactive"></i><i class="fa fa-spinner reload-active"></i> '.$this->Txt->printAdmin(__('Admin | Zapisz')), ['escape'=>false,'class' => 'btn btn-sm btn-success']):'') ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<div style="display: none" id="description-items-default">
<div data-type="text">
    <?=$this->element('admin/allegro/description_items',['descItemType'=>'text'])?>
</div>
<div data-type="image">
    <?=$this->element('admin/allegro/description_items',['descItemType'=>'image'])?>
</div>
<div data-type="text-image">
    <?=$this->element('admin/allegro/description_items',['descItemType'=>'text-image'])?>
</div>
<div data-type="image-text">
    <?=$this->element('admin/allegro/description_items',['descItemType'=>'image-text'])?>
</div>
<div data-type="image-image">
    <?=$this->element('admin/allegro/description_items',['descItemType'=>'image-image'])?>
</div>
    <div id="default-description-item">
        <?= $this->element('admin/allegro/default_description_item', ['key' => '%key%','descItemType'=>'text']) ?>
    </div>
</div>
<?php 
if(!empty($requestData['images'])){
    $allImagesResults=[];
                            foreach ($requestData['images'] as $imgKey => $img){
                                $allImagesResults[$imgKey]=['location'=>$img['url']];
                            }
    echo $this->element('admin/allegro/description_images',['allImagesResults'=>$allImagesResults]);
                        }
?>