<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Aukcje allegro')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Txt->printAdmin(__('Admin | Wymuś pełną synchronizację stanów i cen')), ['action' => 'forceActualization'], ['class' => 'btn btn-xs btn-danger confirm-link', 'confirm-message' => $this->Txt->printAdmin(__('Admin | Czy na pewno chcesz wymusić pełną synchronizacje allegro?'))]) ?></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php
                echo $this->Form->create(null, ['url' => ['action' => 'index']]);
                ?>
                <div class="row align-items-end">
                    <div class="col-xs-12 col-md-3"><?= $this->Form->control('status', ['type' => 'select', 'options' => $offerStates, 'value' => ((!empty($filters) && key_exists('status', $filters)) ? $filters['status'] : ''), 'empty' => $this->Txt->printAdmin(__('Admin | Wszystkie')), 'label' => $this->Txt->printAdmin(__('Admin | Status'))]) ?></div>
                    <div class="col-xs-12 col-md-5"><?= $this->Form->control('name', ['type' => 'text', 'value' => ((!empty($filters) && key_exists('name', $filters)) ? $filters['name'] : ''), 'label' => $this->Txt->printAdmin(__('Admin | Nazwa aukcji'))]) ?></div>
                    <div class="col-xs-12 col-md-4"><label style="display: block">&nbsp;</label><input type="submit" class="btn btn-success" value="Filtruj"><?= (!empty($filters) ? '<input type="submit" class="btn btn-warning" name="clear" value="Wyczyść">' : '') ?></div>
                </div>
                <?php
                echo $this->Form->end();
                if (!empty($offers['offers'])) {
                    echo $this->Form->create(null, ['url' => ['action' => 'activateOffers']]);
                    ?>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th></th>
                                <th scope="col"><?= $this->Txt->printAdmin(__('Admin | Id')) ?></th>
                                <th scope="col"><?= $this->Txt->printAdmin(__('Admin | Nazwa')) ?></th>
                                <th scope="col"><?= $this->Txt->printAdmin(__('Admin | Status')) ?></th>
                                <th scope="col"><?= $this->Txt->printAdmin(__('Admin | Sprzedano')) ?></th>
                                <th scope="col"><?= $this->Txt->printAdmin(__('Admin | Dostępne')) ?></th>
                                <th scope="col"><?= $this->Txt->printAdmin(__('Admin | Cena')) ?></th>
                                <th scope="col"><?= $this->Txt->printAdmin(__('Admin | Data wystawienia')) ?></th>
                                <th scope="col"><?= $this->Txt->printAdmin(__('Admin | Data zakończenia')) ?></th>
                                <th scope="col"><?= $this->Txt->printAdmin(__('Admin | Wyświetlania')) ?></th>
                                <th scope="col"><?= $this->Txt->printAdmin(__('Admin | Obserwuje')) ?></th>
                                <th scope="col"><?= $this->Txt->printAdmin(__('Admin | Produkt')) ?></th>
                                <th scope="col"><?= $this->Txt->printAdmin(__('Admin | Ilość w sklepie')) ?></th>
                                <th scope="col" class="actions"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
                            </tr>
                        </thead>
                        <tbody>

    <?php
    foreach ($offers['offers'] as $key => $offer) {
        ?>
                                <tr>
                                    <td><?php
                        if ($offer['publication']['status'] != 'ACTIVATING') {
                            echo $this->Form->control('offers.' . $key . '.id', ['type' => 'checkbox', 'value' => $offer['id'], 'label' => FALSE]);
                        }
        ?></td>
                                    <td><a href="https://<?= Cake\Core\Configure::read('allegroLink') ?>/oferta/<?= $offer['id'] ?>" target="_blank"><?= $offer['id'] ?></a></td>
                                    <td><?= $offer['name'] ?></td>
                                    <td><?= $offer['publication']['status'] ?></td>
                                    <td><?= $offer['stock']['sold'] ?></td>
                                    <td><?= $offer['stock']['available'] ?></td>
                                    <td><?= $this->Txt->cena($offer['sellingMode']['price']['amount'], $offer['sellingMode']['price']['currency']) ?></td>
                                    <td><?= $this->Txt->printDate($offer['publication']['startedAt'], 'Y-m-d H:i') ?></td>
                                    <td><?= $this->Txt->printDate($offer['publication']['endedAt'], 'Y-m-d H:i') ?></td>
                                    <td><?= $offer['stats']['visitsCount'] ?></td>
                                    <td><?= $offer['stats']['watchersCount'] ?></td>
                                    <td><?=((!empty($existAuctionNames) && !empty($existAuction) && key_exists($offer['id'], $existAuction) && key_exists($existAuction[$offer['id']], $existAuctionNames))?$this->Html->link($existAuctionNames[$existAuction[$offer['id']]], ['controller' => 'Towar', 'action' => 'edit', $existAuction[$offer['id']]], ['escape' => false, 'target'=>'_blank']):'')?></td>
                                    <td><?=((!empty($existAuctionIlosc) && !empty($existAuction) && key_exists($offer['id'], $existAuction) && key_exists($existAuction[$offer['id']], $existAuctionIlosc))?$existAuctionIlosc[$existAuction[$offer['id']]]:'')?></td>
                                    <td class="actions">
        <?php
        if (key_exists($offer['id'], $existAuction)) {
            if ($offer['publication']['status'] != 'ACTIVE') {
                echo $this->Html->link('<i class="fa fa-cogs"></i>', ['action' => 'newAuction', $existAuction[$offer['id']], $offer['id']], ['escape' => false, 'class' => 'btn btn-default btn-xs', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $this->Txt->printAdmin(__('Admin | Edytuj'))]);
            }
            echo $this->Html->link('<i class="fa fa-chain-broken"></i>', ['action' => 'unlink', $existAuction[$offer['id']], $offer['id']], ['escape' => false, 'class' => 'btn btn-danger btn-xs confirm-link', 'confirm-message' => $this->Txt->printAdmin(__('Admin | Czy na pewno chcesz rozłączyć aukcję allegro z produktem w sklepie?')), 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $this->Txt->printAdmin(__('Admin | Rozłącz'))]);
        } elseif (!key_exists($offer['id'], $existAuction)) {
            echo $this->Html->link('<i class="fa fa-link"></i>', ['action' => 'powiaz', $allegroKonto->id, $offer['id']], ['escape' => false, 'class' => 'btn btn-default btn-xs allegro-powiaz', 'item-id' => $offer['id'], 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $this->Txt->printAdmin(__('Admin | Powiąż aukcje z produktem'))]);
        }
        /*
          <?= $this->Html->link('<i class="fa fa-cogs"></i>', ['action' => 'edit', $offer['id']], ['escape' => false, 'class' => 'btn btn-default btn-xs', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $this->Txt->printAdmin(__('Admin | Edytuj'))]) ?>
          <?= $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $offer['id']], ['confirm-btn-yes' => $this->Txt->printAdmin(__('Admin | Tak')), 'confirm-btn-no' => $this->Txt->printAdmin(__('Admin | Nie')), 'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $offer['id'])), 'escape' => false, 'class' => 'btn btn-danger btn-xs confirm-link', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $this->Txt->printAdmin(__('Admin | Usuń'))]) ?>
         */
        ?> </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                    <div class="text-right">
    <?= $this->element('admin/custom_paginator', ['pages' => $pages, 'page' => $page]) ?>
                    </div>
                        <?php
                        echo $this->Form->control('action', ['type' => 'select', 'options' => ['ACTIVATE' => 'Wystaw', 'END' => 'Zakończ'], 'label' => 'Akcja']);
                        echo $this->Form->submit('Zapisz', ['class' => 'btn btn-success']);
                        echo $this->Form->end();
                    } else {
                        ?>
                    <div class="alert alert-info">
                    <?= $this->Txt->printAdmin(__('Admin | Brak aukcji')) ?>
                    </div>
                        <?php
                    }
                    ?>
            </div>
        </div>
    </div>
</div>
<?=
$this->element('admin/allegro/powiaz_modal')?>