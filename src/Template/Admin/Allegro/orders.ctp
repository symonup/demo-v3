<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Towar[]|\Cake\Collection\CollectionInterface $towar
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Zamówienia z Allegro')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                </ul>
                <?= $this->element('admin/paginator_top') ?>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-responsive">
                    <thead>
                        <tr>
                            <th data-priority="2"><?= $this->Paginator->sort('id', $this->Txt->printAdmin(__('Admin | id'))) ?></th>
                            <th data-priority="15"><?= $this->Paginator->sort('data_zakupu', $this->Txt->printAdmin(__('Admin | Data'))) ?></th>
                            <th data-priority="15"><?= $this->Txt->printAdmin(__('Admin | Id klienta')) ?></th>
                            <th data-priority="15"><?= $this->Paginator->sort('kupujacy_login', $this->Txt->printAdmin(__('Admin | Login'))) ?></th>
                            <th data-priority="15"><?= $this->Txt->printAdmin(__('Admin | Email')) ?></th>
                            <th data-priority="15"><?= $this->Txt->printAdmin(__('Admin | Telefon')) ?></th>
                            <th data-priority="15"><?= $this->Txt->printAdmin(__('Admin | Wartość')) ?></th>
                            <th data-priority="15"><?= $this->Txt->printAdmin(__('Admin | Status')) ?></th>
                            
                            <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
                        </tr>
                        <tr class="table-filters">
                            <td></td>
                            <td><?= $this->Form->control('data_zakupu', ['label' => false, 'type' => 'text','datepicker'=>'date','autocomplete'=>'off', 'value' => ((!empty($filter) && key_exists('data_zakupu', $filter)) ? $filter['data_zakupu'] : '')]) ?></td>
                            <td class="short"><?= $this->Form->control('kupujacy_id', ['label' => false, 'type' => 'text', 'value' => ((!empty($filter) && key_exists('id', $filter)) ? $filter['id'] : '')]) ?></td>
                            <td class="short"><?= $this->Form->control('kupujacy_login', ['label' => false, 'type' => 'text', 'value' => ((!empty($filter) && key_exists('id', $filter)) ? $filter['id'] : '')]) ?></td>
                            <td><?= $this->Form->control('kupujacy_email', ['label' => false, 'type' => 'text', 'value' => ((!empty($filter) && key_exists('id', $filter)) ? $filter['id'] : '')]) ?></td>
                            <td></td>
                            <td></td>
                            <td class="short"><?= $this->Form->control('status', ['type' => 'select', 'value' => ((!empty($filter) && key_exists('status', $filter)) ? $filter['status'] : ''), 'options' => $statusy, 'empty' => $this->Txt->printAdmin(__('Admin | Wybierz')), 'label' => false]) ?></td>
                            <td class="actions"><button type="submit" data-toggle="tooltip" data-placment="top" data-container="body" title="<?= $this->Txt->printAdmin(__('Admin | Szukaj')) ?>" class="btn btn-xs btn-warning"><i class="fa fa-search"></i></button><?php if (!empty($filter)) { ?><button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placment="top" data-container="body" title="<?= $this->Txt->printAdmin(__('Admin | Wyczyść filtr')) ?>" name="clear-filter"><i class="fa fa-remove"></i></button><?php } ?></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($zamowienia as $zamowienie): ?>
                            <tr id="row_<?= $zamowienie->id ?>">
                                <td><?=$zamowienie->id?></td>
                                <td><?=$this->Txt->printDate($zamowienie->data_zakupu,'Y-m-d H:i:s')?></td>
                                <td><?=$zamowienie->kupujacy_id?></td>
                                <td><?=$zamowienie->kupujacy_login?></td>
                                <td><?=$zamowienie->kupujacy_email?></td>
                                <td><?=$zamowienie->kupujacy_telefon?></td>
                                <td><?=$this->Txt->cena($zamowienie->wartosc_razem,$zamowienie->waluta)?></td>
                                <td><?=(key_exists($zamowienie->status, $statusy)?$statusy[$zamowienie->status]:$zamowienie->status)?></td>
                                <td class="actions">
                                    <?= $this->Html->link($this->Txt->printAdmin(__('Admin | Szczegóły')), ['controller' => 'Allegro', 'action' => 'orderView', $zamowienie->id], ['escape' => false]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <?= $this->element('admin/paginator') ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>