<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Szczegóły zamówienia numer: {0}', [(!empty($zamowienie->order_id) ? $zamowienie->order_id : $zamowienie->id)])) ?></h2>
                <ul class="nav navbar-right panel_toolbox">

                    <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> ' . __('Admin | Wróć do {0}', [__('Admin | listy zamówień')])), ['action' => 'orders'], ['escape' => false, 'class' => 'btn btn-xs btn-warning']) ?></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="orderDetalisBox">
                                    <h4><?= $this->Txt->printAdmin(__('Admin | Dane zamówienia')) ?></h4>
                                    <ul>
                                        <li><span><?= $this->Txt->printAdmin(__('Admin | Numer zamówienia:')) ?></span> <span><?= (!empty($zamowienie->order_id) ? $zamowienie->order_id : $zamowienie->id) ?></span></li>
                                        <li><span><?= $this->Txt->printAdmin(__('Admin | Data złożenia:')) ?></span> <span><?= $this->Txt->printDate($zamowienie->data_zakupu,'Y-m-d H:i') ?></span></li>
                                        <li><span><?= $this->Txt->printAdmin(__('Admin | Waluta:')) ?></span> <span><?= $zamowienie->waluta ?></span></li>
                                        <li><span><?= $this->Txt->printAdmin(__('Admin | Wartość zamówienia:')) ?></span> <span><?= $this->Txt->cena($zamowienie->wartosc_razem, $zamowienie->waluta) ?></span></li>
                                    </ul>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="orderDetalisBox">
                                    <h4><?= $this->Txt->printAdmin(__('Admin | Dane klienta')) ?></h4>
                                    <ul>
                                        <li><span><?= $this->Txt->printAdmin(__('Admin | Imie i Nazwisko:')) ?></span> <span><?= $zamowienie->kupujacy_imie ?> <?= $zamowienie->kupujacy_nazwisko ?></span></li>
                                        <li><span><?= $this->Txt->printAdmin(__('Admin | Email:')) ?></span> <span><?= $zamowienie->kupujacy_email ?></span></li>
                                        <li><span><?= $this->Txt->printAdmin(__('Admin | Telefon:')) ?></span> <span><?= $zamowienie->kupujacy_telefon ?></span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <?php
                        if (!empty($zamowienie->uwagi)) {
                            ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="orderDetalisBox">
                                        <h4><?= $this->Txt->printAdmin(__('Admin | Uwagi do zamówienia')) ?></h4>
                                        <div><?= nl2br(strip_tags($zamowienie->uwagi)) ?></div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="orderDetalisBox">
                                    <h4><?= $this->Txt->printAdmin(__('Admin | Dane wysyłki/odbioru')) ?></h4>
                                    <?php 
                                    if(!empty($zamowienie->paczkomat_id)){
                                        ?>
                                    <div>
                                        <b>Paczkomat: <?=$zamowienie->paczkomat_id?></b><br/>
                                         <?=$zamowienie->paczkomat_nazwa?><br/>
                                         <?= nl2br($zamowienie->paczkomat_adres)?><br/>
                                         <?=$zamowienie->paczkomat_opis?>
                                         <hr/>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                    <div><?= $this->Txt->printUserInfo($zamowienie->toArray(), '<br/>', 'wysylka_') ?></div>
                                    <div><?= $this->Txt->printAddres($zamowienie->toArray(), '<br/>', 'wysylka_') ?></div>
                                    <?php if(!empty($zamowienie->punkty_odbioru_id)){
                                        echo $this->Html->tag('div', '<b>'.$this->Txt->printAdmin(__('Admin | Odbiór w punkcie:')).'</b><br/>'.nl2br($zamowienie->punkty_odbioru_info));
                                    }?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="orderDetalisBox">
                                    <h4><?= $this->Txt->printAdmin(__('Admin | Dane do faktury')) ?></h4>
                                    <div><?= $this->Txt->printUserInfo($zamowienie->toArray(), '<br/>', 'faktura_') ?></div>
                                    <div><?= $this->Txt->printAddres($zamowienie->toArray(), '<br/>', 'faktura_') ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="orderDetalisBox">
                            <form id="orderNotatkiForm" class="ajaxForm" action="<?= Cake\Routing\Router::url(['action' => 'orderUpdate', $zamowienie->id]) ?>">
                                <h4><?= $this->Txt->printAdmin(__('Admin | Notatki wewnętrzne')) ?></h4>
                                <?= $this->Form->control('notatki', ['type' => 'textarea', 'value' => $zamowienie->notatki, 'label' => false]) ?>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-floppy-o reload-unactive"></i><i class="fa fa-spinner reload-active"></i> <?= $this->Txt->printAdmin(__('Admin | Zapisz')) ?></button>
                                </div>
                            </form>
                        </div>
                        <div class="orderDetalisBox">
                            <form id="orderStatusForm" class="ajaxForm" action="<?= Cake\Routing\Router::url(['action' => 'orderUpdate', $zamowienie->id, 'status']) ?>">
                                <h4><?= $this->Txt->printAdmin(__('Admin | Status zamówienia')) ?></h4>
                                <?= $this->Form->control('status', ['type' => 'select', 'value' => $zamowienie->status, 'label' => false, 'options' => $statusy]) ?>
                                <div class="inputTerminWysylki" <?= (($zamowienie->status != 'przyjete') ? 'style="display:none;"' : '') ?>>
                                    <?= $this->Form->control('termin_wysylki', ['type' => 'text','autocomplete'=>'off', 'value' => (!empty($zamowienie->termin_wysylki) ? $zamowienie->termin_wysylki->format('Y-m-d') : ''), 'label' => $this->Txt->printAdmin(__('Admin | Przewidywany termin wysyłki')), 'datepicker' => 'date']) ?>
                                </div>
                                <div class="inputListPrzewozowy" <?= (($zamowienie->status != 'wyslane') ? 'style="display:none;"' : '') ?>>
                                    <?= $this->Form->control('kurier_typ', ['type' => 'select','disabled'=>($zamowienie->status != 'wyslane'),'options'=>['dpd'=>'DPD','dhl'=>'DHL','ups'=>'UPS','inpost'=>'InPost','poczta'=>'Poczta Polska'],'empty'=>$this->Txt->printAdmin(__('Admin | Wybierz kuriera')), 'value' => $zamowienie->kurier_typ, 'label' => $this->Txt->printAdmin(__('Admin | Typ kuriera'))]) ?>
                                    <?= $this->Form->control('nr_listu_przewozowego', ['type' => 'text','disabled'=>($zamowienie->status != 'wyslane'),'autocomplete'=>'off', 'value' => $zamowienie->nr_listu_przewozowego, 'label' => $this->Txt->printAdmin(__('Admin | Nr listu przewozowego'))]) ?>
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-floppy-o reload-unactive"></i><i class="fa fa-spinner reload-active"></i> <?= $this->Txt->printAdmin(__('Admin | Zapisz')) ?></button>
                                </div>
                            </form>
                            <?php if(!empty($zamowienie->kurier_typ) && !empty($zamowienie->nr_listu_przewozowego)){
                                echo $this->Html->tag('h4',$this->Txt->printAdmin(__('Admin | Link do śledzenia przesyłki'))).$this->Txt->kurierLink($zamowienie->kurier_typ,$zamowienie->nr_listu_przewozowego);
                            } ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <h4><?= $this->Txt->printAdmin(__('Admin | Produkty w zamówieniu')) ?></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th><?= $this->Txt->printAdmin(__('Admin | Produkt')) ?></th>
                                    <th><?= $this->Txt->printAdmin(__('Admin | Platforma')) ?></th>
                                    <th><?= $this->Txt->printAdmin(__('Admin | Preorder')) ?></th>
                                    <th><?= $this->Txt->printAdmin(__('Admin | Kod')) ?></th>
                                    <th><?= $this->Txt->printAdmin(__('Admin | Cena brutto')) ?></th>
                                    <th><?= $this->Txt->printAdmin(__('Admin | Ilość')) ?></th>
                                    <th class="text-right"><?= $this->Txt->printAdmin(__('Admin | Wartość brutto')) ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sumWaga = 0;
                                $sumObjetosc = 0;
                                $wartosc=0;
                                foreach ($zamowienie->allegro_zamowienie_towar as $item) {
                                    ?>
                                    <tr>
                                        <td><?= (!empty($item->towar_id) ? $this->Html->link($item->nazwa, ['controller' => 'Towar', 'action' => 'view', $item->towar_id, $this->Navi->friendlyUrl($item->nazwa), 'prefix' => false], ['target' => '_blank']) : $item->nazwa) ?></td>
                                        <td><?= ($item->has('towar')?$item->towar->platforma->nazwa:'') ?></td>
                                        <td><?= ($item->has('towar')?(!empty($item->towar->data_premiery) ? $item->towar->data_premiery->format('Y-m-d') : ''):'') ?></td>
                                        <td><?= ($item->has('towar')?$item->towar->kod:'') ?></td>
                                        <td><?= $this->Txt->cena($item->cena_za_sztuke, $zamowienie->waluta) ?></td>
                                        <td><?= $item->ilosc ?></td>
                                        <td class="text-right"><?= $this->Txt->cena(($item->cena_za_sztuke*$item->ilosc), $zamowienie->waluta) ?></td>
                                    </tr>
                                    <?php
                                    $wartosc+=($item->cena_za_sztuke*$item->ilosc);
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="6" class="text-right"><?= $this->Txt->printAdmin(__('Admin | Łączna wartość produktów')) ?></th>
                                    <?php /* <th class="text-right" style="white-space: nowrap;"><?=$this->Txt->cena($zamowienie->wartosc_produktow_netto,$zamowienie->waluta_symbol)?></th> */ ?>
                                    <th class="text-right" style="white-space: nowrap;"><?= $this->Txt->cena($wartosc, $zamowienie->waluta) ?></th>
                                </tr>
                                <tr>
                                    <th colspan="6" class="text-right"><?= $zamowienie->payment_type.' '.$zamowienie->payment_provider.' - '.$zamowienie->allegro_wysylka_nazwa ?></th>
                                    <th class="text-right" style="white-space: nowrap;"><?= $this->Txt->cena($zamowienie->allegro_wysylka_koszt, $zamowienie->waluta) ?></th>
                                </tr>
                                <tr>
                                    <th colspan="6" class="text-right"><?= $this->Txt->printAdmin(__('Admin | Łączna wartość zamówienia')) ?></th>
                                    <?php /* <th class="text-right" style="white-space: nowrap;"><?=$this->Txt->cena($zamowienie->wartosc_razem_netto,$zamowienie->waluta_symbol)?></th> */ ?>
                                    <th class="text-right" style="white-space: nowrap;"><?= $this->Txt->cena($zamowienie->wartosc_razem, $zamowienie->waluta) ?></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="row">
                    
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <h4>Wysyłka zamówienia</h4>
    <?php
        echo $this->element('admin/allegro_kurierzy');
    ?>
</div>
                </div>
            </div>
        </div>
    </div>
</div>
