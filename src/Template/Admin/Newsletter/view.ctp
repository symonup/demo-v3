<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
                <?= $this->Form->create($newsletter) ?>
            <div class="x_title">
                    <h2><?=$this->Txt->printAdmin(__('Admin | Podgląd {0}',[__('Admin | Newsletter')]))?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> '.__('Admin | Wróć do {0}',[__('Admin | Listy')])), ['action' => 'index'],['escape'=>false,'class'=>'btn btn-xs btn-warning']) ?></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
            <div class="x_content">
    <fieldset>
        <ul>
            <li><?=$this->Txt->printAdmin(__('Admin | Grupa docelowa'))?>: <?=$newsletterTargetGroups[$newsletter->typ]?></li>
            <li><?=$this->Txt->printAdmin(__('Admin | Temat'))?>: <?=$newsletter->temat?></li>
        </ul>
        <?php
            $lngInputs=[];
            $lngInputs['tresc']=['type'=>'textarea','summernote'=>'true','label'=>$this->Txt->printAdmin(__('Admin | Treść - {0}'))];
                    echo $this->Translation->inputs($this->Form, $languages, $lngInputs);
                            ?>
    </fieldset>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
        <?php
                if(!empty($tips)){
                    echo $this->element('admin/tips',['tips'=>$tips]);
                }
                ?>
</div>
