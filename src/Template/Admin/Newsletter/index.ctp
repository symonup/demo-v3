<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Newsletter[]|\Cake\Collection\CollectionInterface $newsletter
  */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Newsletter')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Html->tag('i','',['class'=>'fa fa-plus']).' '.$this->Txt->printAdmin(__('Admin | Dodaj {0}',[$this->Txt->printAdmin(__('Admin | Newsletter'))])), ['action' => 'add'],['escape'=>false,'class'=>'btn btn-xs btn-primary']) ?></li>
                </ul>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped data-table">
                    <thead>
            <tr>
                                <th data-priority="2"><?= $this->Txt->printAdmin(__('Admin | id')) ?></th>
                                <th data-priority="3"><?= $this->Txt->printAdmin(__('Admin | Grupa docelowa')) ?></th>
                                <th data-priority="3"><?= $this->Txt->printAdmin(__('Admin | Temat')) ?></th>
                                <th data-priority="6"><?= $this->Txt->printAdmin(__('Admin | Data utworzenia')) ?></th>
                                <th data-priority="6"><?= $this->Txt->printAdmin(__('Admin | Data wysłania')) ?></th>
                                <th data-priority="8"><?= $this->Txt->printAdmin(__('Admin | Status')) ?></th>
                                <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($newsletter as $newsletter): ?>
            <tr>
                <td><?= $newsletter->id ?></td>
                <td><?= $newsletterTargetGroups[$newsletter->typ]?></td>
                <td><?= $newsletter->temat ?></td>
                <td><?= (!empty($newsletter->created)?$newsletter->created->format('Y-m-d H:i'):'') ?></td>
                <td><?= (!empty($newsletter->data)?$newsletter->data->format('Y-m-d H:i'):$this->Txt->printAdmin(__('Admin | Nie wysłany'))) ?></td>
                <td><?= (empty($newsletter->status)?$this->Txt->printAdmin(__('Admin | Nie wysłany')):(($newsletter->status==1)?$this->Txt->printAdmin(__('Admin | W trakcie wysyłki')):$this->Txt->printAdmin(__('Admin | Wysłany')))) ?></td>
                <td class="actions">
                    <?php 
                    if(!$newsletter->status>0){
                    echo $this->Html->link('<i class="fa fa-cogs"></i>', ['action' => 'edit', $newsletter->id],['escape'=>false,'class'=>'btn btn-default btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Edytuj'))]);
                    echo $this->Html->link('<i class="fa fa-send"></i>', ['action' => 'send', $newsletter->id],['escape'=>false,'class'=>'btn btn-warning btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Wyślij'))]);
                    echo $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $newsletter->id], ['confirm-btn-yes'=>$this->Txt->printAdmin(__('Admin | Tak')),'confirm-btn-no'=>$this->Txt->printAdmin(__('Admin | Nie')),'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $newsletter->id)),'escape'=>false,'class'=>'btn btn-danger btn-xs confirm-link','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Usuń'))]);
                  }else{
                     echo $this->Html->link('<i class="fa fa-eye"></i>', ['action' => 'view', $newsletter->id],['escape'=>false,'class'=>'btn btn-default btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Podgląd'))]);
                    }
                    ?>
                   </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
                </table>
                <?php
                if(!empty($tips)){
                    echo $this->element('admin/tips',['tips'=>$tips]);
                }
                ?>
            </div>
        </div>
    </div>
</div>