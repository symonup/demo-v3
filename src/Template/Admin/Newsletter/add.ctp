<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
                <?= $this->Form->create($newsletter) ?>
            <div class="x_title">
                    <h2><?=$this->Txt->printAdmin(__('Admin | Add {0}',[__('Admin | Newsletter')]))?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                                              <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> '.__('Admin | Wróć do {0}',[__('Admin | Listy')])), ['action' => 'index'],['escape'=>false,'class'=>'btn btn-xs btn-warning']) ?></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
            <div class="x_content">
    <fieldset>
        <?php
            $lngInputs=[];
            echo $this->Form->control('typ',['type'=>'select','options'=>$newsletterTypes,'default'=>'','empty'=>$this->Txt->printAdmin(__('Admin | Wybierz grupę adresatów')),'label'=>$this->Txt->printAdmin(__('Admin | Wyślij newsletter do')),'required'=>true]);
                            echo $this->Form->control('temat',['label'=>$this->Txt->printAdmin(__('Admin | Temat'))]);
                              echo $this->Form->control('tresc',['type'=>'textarea','summernote'=>'true','label'=>$this->Txt->printAdmin(__('Admin | Treść'))]);
//                    echo $this->Translation->inputs($this->Form, $languages, $lngInputs);
                            ?>
    </fieldset>
            </div>
            <div class="x_footer text-right">
    <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')),['class'=>'btn btn-sm btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
    <?php
                if(!empty($tips)){
                    echo $this->element('admin/tips',['tips'=>$tips]);
                }
                ?>
</div>
