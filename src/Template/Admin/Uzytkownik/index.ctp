<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Uzytkownik[]|\Cake\Collection\CollectionInterface $uzytkownik
  */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Lista klientów {0}',[(($type=='b2b')?$this->Txt->printAdmin(__('Admin | hurtowych')):$this->Txt->printAdmin(__('Admin | indywidualnych')))])) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Html->tag('i','',['class'=>'fa fa-plus']).' '.$this->Txt->printAdmin(__('Admin | Dodaj {0}',[$this->Txt->printAdmin(__('Admin | użytkownia'))])), ['action' => 'add',$type],['escape'=>false,'class'=>'btn btn-xs btn-primary']) ?></li>
                </ul>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped data-table">
                    <thead>
            <tr>
                                <th data-priority="2"><?= $this->Txt->printAdmin(__('Admin | ID')) ?></th>
                                <th data-priority="4"><?= $this->Txt->printAdmin(__('Admin | Email')) ?></th>
                                <th data-priority="5"><?= $this->Txt->printAdmin(__('Admin | Telefon')) ?></th>
                                <th data-priority="6"><?= $this->Txt->printAdmin(__('Admin | Imię i Nazwisko')) ?></th>
                                <th data-priority="14"><?= $this->Txt->printAdmin(__('Admin | Data rejestracji')) ?></th>
                                <th data-priority="15"><?= $this->Txt->printAdmin(__('Admin | Aktywny')) ?></th>
                                <th data-priority="16"><?= $this->Txt->printAdmin(__('Admin | Firma')) ?></th>
                                <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($uzytkownik as $uzytkownik): ?>
            <tr>
                <td><?= $uzytkownik->id ?></td>
                <td><?= $uzytkownik->email ?></td>
                <td style="white-space: nowrap;"><?= (!empty($uzytkownik->kierunkowy)?$uzytkownik->kierunkowy.' '.str_replace($uzytkownik->kierunkowy,$uzytkownik->kierunkowy,$uzytkownik->telefon):$uzytkownik->telefon) ?></td>
                <td><?= $uzytkownik->imie ?> <?= $uzytkownik->nazwisko ?></td>
                <td><?= (!empty($uzytkownik->data_rejestracji)?$uzytkownik->data_rejestracji->format('Y-m-d H:i:s'):'') ?></td>
                <td><?= $this->Txt->printBool($uzytkownik->aktywny) ?></td>
                <td><?= $uzytkownik->firma ?></td>
                <td class="actions">
                    <?= $this->Html->link('<i class="fa fa-cogs"></i>', ['action' => 'edit', $uzytkownik->id],['escape'=>false,'class'=>'btn btn-default btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Edytuj'))]) ?>
                    <?= $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $uzytkownik->id], ['confirm-btn-yes'=>$this->Txt->printAdmin(__('Admin | Tak')),'confirm-btn-no'=>$this->Txt->printAdmin(__('Admin | Nie')),'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $uzytkownik->id)),'escape'=>false,'class'=>'btn btn-danger btn-xs confirm-link','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Usuń'))]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
                </table>
            </div>
        </div>
    </div>
</div>