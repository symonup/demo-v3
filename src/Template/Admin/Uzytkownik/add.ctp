<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <?= $this->Form->create($uzytkownik) ?>
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Add {0}', [__('Admin | Uzytkownik')])) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> ' . __('Admin | Wróć do {0}', [__('Admin | Listy')])), ['action' => 'index',$uzytkownik->typ], ['escape' => false, 'class' => 'btn btn-xs btn-warning']) ?></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <fieldset>
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $this->Form->control('typ', ['type' => 'select', 'options' => ['b2c' => $this->Txt->printAdmin(__('Admin | Indywidualny')), 'b2b' => $this->Txt->printAdmin(__('Admin | Hurtowy'))], 'label' => $this->Txt->printAdmin(__('Admin | Rodzaj klienta')), 'id' => 'options-by-type', 'default' => 'b2c']) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-4"><?= $this->Form->control('email', ['required'=>true,'label' => $this->Txt->printAdmin(__('Admin | Email'))]) ?></div>
                        <div class="col-xs-12 col-md-4"><?= $this->Form->control('password', ['required'=>true,'type' => 'password', 'value' => '', 'label' => $this->Txt->printAdmin(__('Admin | Hasło'))]) ?></div>
                        <div class="col-xs-12 col-md-4"><?= $this->Form->control('telefon', ['required'=>true,'label' => $this->Txt->printAdmin(__('Admin | Telefon'))]) ?></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-6"><?= $this->Form->control('imie', ['required'=>true,'label' => $this->Txt->printAdmin(__('Admin | Imię'))]) ?></div>
                        <div class="col-xs-12 col-md-6"><?= $this->Form->control('nazwisko', ['required'=>true,'label' => $this->Txt->printAdmin(__('Admin | Nazwisko'))]) ?></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-6"><?= $this->Form->control('firma', ['label' => $this->Txt->printAdmin(__('Admin | Firma'))]) ?></div>
                        <div class="col-xs-12 col-md-6"><?= $this->Form->control('nip', ['label' => $this->Txt->printAdmin(__('Admin | Nip'))]) ?></div>
                    </div>
                    <div class="options-item-type <?= (($uzytkownik->typ == 'b2b') ? 'active' : '') ?>" data-type="b2b">
                        <hr/>
                        <h4><?= $this->Txt->printAdmin(__('Admin | Adres firmy klienta hurtowego')) ?></h4>
                        
                        <div class="row">
                            <div class="col-xs-12 col-md-6"><?= $this->Form->control('firma_ulica', ['required'=>true,'disabled'=>($uzytkownik->typ == 'b2c'),'label' => $this->Txt->printAdmin(__('Admin | Ulica'))]) ?></div>
                            <div class="col-xs-12 col-md-3"><?= $this->Form->control('firma_nr_domu', ['required'=>true,'disabled'=>($uzytkownik->typ == 'b2c'),'label' => $this->Txt->printAdmin(__('Admin | Nr budynku'))]) ?></div>
                            <div class="col-xs-12 col-md-3"><?= $this->Form->control('firma_nr_lokalu', ['disabled'=>($uzytkownik->typ == 'b2c'),'label' => $this->Txt->printAdmin(__('Admin Nr lokalu| '))]) ?></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-4"><?= $this->Form->control('firma_kod', ['required'=>true,'disabled'=>($uzytkownik->typ == 'b2c'),'label' => $this->Txt->printAdmin(__('Admin | Kod pocztowy'))]) ?></div>
                            <div class="col-xs-12 col-md-8"><?= $this->Form->control('firma_miasto', ['required'=>true,'disabled'=>($uzytkownik->typ == 'b2c'),'label' => $this->Txt->printAdmin(__('Admin | Miasto'))]) ?></div>
                        </div>
                        <?= $this->Form->control('api', ['disabled'=>($uzytkownik->typ == 'b2c'),'label' => $this->Txt->printAdmin(__('Admin | Zezwól na dostęp do pliku dropshipping')), 'type' => 'checkbox']) ?>
                    </div>
                    <?php
                    echo $this->Form->control('aktywny', ['label' => $this->Txt->printAdmin(__('Admin | Zezwól na dostęp do serwisu')), 'type' => 'checkbox']);
                    ?>
                </fieldset>
            </div>
            <div class="x_footer text-right">
                <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')), ['class' => 'btn btn-sm btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
