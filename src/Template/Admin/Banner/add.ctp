<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Dodaj {0}',[__('Admin | Baner')])) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                                               <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> '.__('Admin | Wróć do {0}',[__('Admin | Listy')])), ['action' => 'index'],['escape'=>false,'class'=>'btn btn-xs btn-warning']) ?></li>
                    </ul>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
                            <?= $this->Form->create($banner) ?>
    <fieldset>
        <?php
            echo $this->Form->control('nazwa',['class'=>'form-control','label'=>$this->Txt->printAdmin(__('Admin | Nazwa'))]);
             echo $this->Form->control('glowny',['label'=>$this->Txt->printAdmin(__('Admin | Baner na stronę główną'))]);
             echo $this->Form->control('ukryty',['label'=>$this->Txt->printAdmin(__('Admin | Baner niewidoczny na stronie'))]);
            echo $this->Form->control('type',['type'=>'hidden','value'=>'carusel','class'=>'form-control']);
            echo $this->Form->control('css',['class'=>'form-control','label'=>$this->Txt->printAdmin(__('Admin | Css')),'code-mirror-css'=>'true']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Admin | Zapisz i przejdź dalej'),['class'=>'btn btn-sm btn-primary','name'=>'next-step']) ?>
    <?= $this->Form->end() ?>
                        </div>
                        <!-- .panel-body -->
                    </div>
        </div>
    </div>
