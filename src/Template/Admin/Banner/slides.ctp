<?php $this->start('css');   
 echo $this->Html->css(['banner.css?'.uniqid()]) ;
          $this->end();
  ?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Slajdy banera: {0}', [$this->Html->tag('b', $banner->nazwa)])) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-plus']) . ' ' . $this->Txt->printAdmin(__('Admin | Dodaj {0}', [$this->Txt->printAdmin(__('Admin | Slajd'))])), ['action' => 'slideAdd', $banner->id], ['escape' => false, 'class' => 'btn btn-xs btn-primary']) ?></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th style="width: 40px;"><?= $this->Txt->printAdmin(__('Admin | Lp')) ?></th>
                            <th style="width: 40px;"><?= $this->Txt->printAdmin(__('Admin | Id')) ?></th>
                            <th><?= $this->Txt->printAdmin(__('Admin | Slajd')) ?></th>
                            <th style="width: 120px;" data-orderable="false"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $lp = 1;
                        if (!empty($banner->banner_slides)) {
                            foreach ($banner->banner_slides as $item) {
                                ?>
                                <tr>
                                    <td><?= $lp++ ?></td>
                                    <td><?= $item['id'] ?></td>
                                    <td><?= $this->element('admin/banner/slide', ['slide' => $item]) ?></td>
                                    <td class="text-right">
                                        <a data-toggle="tooltip" data-placement="top" title="<?= $this->Txt->printAdmin(__('Admin | Elementy slajdu')) ?>" href="<?= \Cake\Routing\Router::url(['action' => 'slideElementAdd', $item['banner_id'], $item['id']]) ?>" class="btn btn-default btn-xs"><i class="fa fa-cubes"></i></a>

                                        <a data-toggle="tooltip" data-placement="top" title="<?= $this->Txt->printAdmin(__('Admin | Edytuj')) ?>" href="<?= \Cake\Routing\Router::url(['action' => 'slideEdit', $item['id']]) ?>" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></a>
                                            <?php
                                            echo $this->Form->postLink(
                                                    '<i class="fa fa-remove"></i>', ['action' => 'deleteSlide', $item['id']], ['confirm' => __('Admin | Are you sure you want to delete # {0}?', $item['id']), 'escape' => false, 'class' => 'btn btn-danger btn-xs', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => __('Admin | Usuń')]
                                            );
                                            ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>