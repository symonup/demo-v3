<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Elementy slajdu')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                                               <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> '.__('Admin | Wróć do {0}',[__('Admin | Listy')])), ['action' => 'index'],['escape'=>false,'class'=>'btn btn-xs btn-warning']) ?></li>
                    </ul>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
<?= $this->Form->create(null, ['type' => 'file']) ?>
<?= $this->Form->control('id', ['type' => 'hidden', 'value' => $bannerSlideId]) ?>
<fieldset>
    <div class="row">
        <div class="col-md-12">
            <div class="panel-group" id="slide-elements-container">
                <?php
                if (!empty($banner->banner_slides[0]->banner_slides_elements)) {
                    foreach ($banner->banner_slides[0]->banner_slides_elements as $num => $slideElement) {
                        echo $this->element('admin/banner/element_default', ['itemElement' => ['number' => $num, 'item' => $slideElement]]);
                    }
                }
                ?>
            </div>
                                    <div class="mt-10 mb-10">
                                        <button type="button" class="btn btn-sm btn-primary" id="add-next-slide-element"><?=$this->Txt->printAdmin(__('Admin | Dodaj element'))?></button>
                                    </div>
        </div>
    </div>
</fieldset>
<?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')), ['class' => 'btn btn-sm btn-success']) ?>
<?= $this->Form->end() ?>
<div class="hidden">
    <?=$this->element('admin/banner/element_default')?>
</div>
<div class="baner_podglad" id="banner-preview">
<h3 style="float:  left;margin: 3px 5px;"><?= $this->Txt->printAdmin(__('Admin | Podgląd banera')) ?></h3>
    <div>
        <?php
        echo $this->Html->tag('span', $this->Html->tag('span', '0', array('id' => 'crW')) . $this->Html->tag('span', '0', array('id' => 'crH')) . $this->Html->tag('span', '0', array('id' => 'crT')) . $this->Html->tag('span', '0', array('id' => 'crL')), array('id' => 'cords'));
        echo $this->Html->image($displayPath['banner_slides'] . $banner->banner_slides[0]->file, array('alt' => $banner->banner_slides[0]->file, 'id' => 'preview-target','width'=>'100%','height'=>'327px'));
        ?>
    </div>
</div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var ctLink = <?php
        if (!empty($banner->banner_slides[0]->banner_slides_elements))
            echo count($banner->banner_slides[0]->banner_slides_elements);
        else
            echo 0;
        ?>;
    var banerId =<?= $banner->banner_slides[0]->id ?>;
    var imgWidth = $('#preview-target').width();
    var imgHeight = $('#preview-target').height();
    var bannerJC
    $(document).ready(function () {
        imgWidth = $('#preview-target').width();
        imgHeight = $('#preview-target').height();
        bannerJC = $('#preview-target').Jcrop({
            onSelect: showCoords,
            onChange: showCoords
        });
    });
    setTimeout(function () {
<?php
if (!empty($banner->banner_slides[0]->banner_slides_elements)) {
    foreach ($banner->banner_slides[0]->banner_slides_elements as $key => $link) {
        echo '$(\'<div class="linkArea" id="newLink_' . $key . '" style="position:absolute; width:' . $link->_width . 'px;height:' . $link->_height . 'px; top:' . $link->pos_top . '%; left:' . $link->pos_left . '%;"><div ' . (!empty($link->css) ? 'style="' . str_replace(["\r", "\n"], ['', ''], $link->css) . '"' : '') . '>' . str_replace(["\r", "\n"], ['', ''], $link->text) . '</div></div>\').appendTo($(\'.jcrop-holder\'));';
    }
}
?>
    }, 300);
</script>