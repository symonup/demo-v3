<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Dodaj',[__('Admin | Slajd')])) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                                               <li><?= $this->Html->link('<i class="fa fa-chevron-left "></i> ' . $this->Txt->printAdmin(__('Admin | Powrót')), ['action' => 'slides',$bannerId], ['escape' => false, 'class' => 'btn btn-sm btn-warning']) ?> </li>
                    </ul>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
                 <?= $this->Form->create($bannerSlide,['type'=>'file']) ?>
                    <fieldset>
                        <?php
                        echo $this->Form->control('banner_id', ['type' => 'hidden', 'value' => $bannerId]);
                                echo $this->Form->control('title', ['class' => 'form-control', 'label' => $this->Txt->printAdmin(__('Admin | Nazwa slajdu'))]);
                        echo $this->Form->control('type', ['class' => 'form-control', 'type' => 'select', 'id' => 'slide-type', 'options' => $slidesTypes, 'default' => 'file', 'label' => $this->Txt->printAdmin(__('Admin | Type'))]);
                        echo $this->Form->control('is_linked', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Link na całym slajdzie')), 'id' => 'slide-is-linked']);
                        /* TYPY INPUTS */
                        $slideTypes = [
                            'file' => [$this->File->control('file', $this->Txt->printAdmin(__('Admin | Dodaj plik')), ['label' => $this->Txt->printAdmin(__('Admin | Wgraj plik banera')),'src'=>(!empty($bannerSlide['file'])?'/files/banner_slides/'.$bannerSlide['file']:''),'value'=>''])],
                            'html' => [$this->Form->control('height', ['class' => 'form-control', 'label' => $this->Txt->printAdmin(__('Admin | Wysykość slajdu'))])],
                            'link' => [
                                $this->Form->control('link', ['class' => 'form-control', 'label' => $this->Txt->printAdmin(__('Admin | Link url'))]),
                                $this->Form->control('target', ['class' => 'form-control', 'label' => $this->Txt->printAdmin(__('Admin | Link target')), 'type' => 'select', 'options' => ['_self' => $this->Txt->printAdmin(__('Admin | Aktualna strona')), '_blank' => $this->Txt->printAdmin(__('Admin | Nowa karta'))], 'default' => '_self'])
                            ]
                        ];
                        foreach ($slideTypes as $slideType => $inputs) {
                            if (!empty($inputs)) {
                                echo $this->Html->tag('div', join('', $inputs), ['class' => 'banner-slide-type', 'data-type' => $slideType]);
                            }
                        }


                        echo $this->Form->control('css', ['class' => 'form-control', 'label' => $this->Txt->printAdmin(__('Admin | Css')), 'code-mirror-css' => 'true']);
                        ?>
                    </fieldset>
                    <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')), ['class' => 'btn btn-sm btn-success']) ?>
                    <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
