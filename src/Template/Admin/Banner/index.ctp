<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Banery')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Html->tag('i','',['class'=>'fa fa-plus']).' '.$this->Txt->printAdmin(__('Admin | Dodaj {0}',[$this->Txt->printAdmin(__('Admin | Baner'))])), ['action' => 'add'],['escape'=>false,'class'=>'btn btn-xs btn-primary']) ?></li>
                </ul>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped data-table">
                    <thead>
            <tr>
                                
                                    <th data-priority="4" ><?= $this->Txt->printAdmin(__('Admin | Lp')) ?></th>
                                    <th data-priority="3" ><?= $this->Txt->printAdmin(__('Admin | Id')) ?></th>
                                    <th data-priority="2" ><?= $this->Txt->printAdmin(__('Admin | Nazwa')) ?></th>
                                    <th data-priority="2" ><?= $this->Txt->printAdmin(__('Admin | Strona główna')) ?></th>
                                    <th data-priority="2" ><?= $this->Txt->printAdmin(__('Admin | Ukryty')) ?></th>
                                    <th data-priority="1" data-orderable="false"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
            </tr>
        </thead>
        <tbody>
            <?php 
            $lp=1;
            foreach ($banner as $item): ?>
            <tr>
                <td><?= $lp++ ?></td>
                                        <td><?= $item['id'] ?></td>
                                        <td><?= $item['nazwa'] ?></td>
                                        <td><?= $this->Txt->printBool($item['glowny']) ?></td>
                                        <td><?= $this->Txt->printBool($item['ukryty']) ?></td>
                                        <td class="actions">
                                            <?php 
                    echo $this->Html->link('<i class="fa fa-cogs"></i>', ['action' => 'edit', $item->id],['escape'=>false,'class'=>'btn btn-default btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Edytuj'))]);
                    echo $this->Html->link('<i class="fa fa-cubes"></i>', ['action' => 'slides', $item->id],['escape'=>false,'class'=>'btn btn-warning btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Slajdy'))]);
                    echo $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $item->id], ['confirm-btn-yes'=>$this->Txt->printAdmin(__('Admin | Tak')),'confirm-btn-no'=>$this->Txt->printAdmin(__('Admin | Nie')),'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $item->nazwa)),'escape'=>false,'class'=>'btn btn-danger btn-xs confirm-link','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Usuń'))]);
                   ?>
                                        </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
                </table>
                <?php
                if(!empty($tips)){
                    echo $this->element('admin/tips',['tips'=>$tips]);
                }
                ?>
            </div>
        </div>
    </div>
</div>