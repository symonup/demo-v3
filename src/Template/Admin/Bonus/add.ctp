<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
                <?= $this->Form->create($bonus) ?>
            <div class="x_title">
                    <h2><?=$this->Txt->printAdmin(__('Admin | Add {0}',[__('Admin | Bonus')]))?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                                              <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> '.__('Admin | Wróć do {0}',[__('Admin | Listy')])), ['action' => 'index'],['escape'=>false,'class'=>'btn btn-xs btn-warning']) ?></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
            <div class="x_content">
    <fieldset>
        <?php
            $lngInputs=[];
                        echo $this->Form->control('wartosc',['type'=>'text','data-type'=>'float','label'=>$this->Txt->printAdmin(__('Admin | Ilość bonusowych złotówek'))]);
        ?>
    </fieldset>
            </div>
            <div class="x_footer text-right">
    <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')),['class'=>'btn btn-sm btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
