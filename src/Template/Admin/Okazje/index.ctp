<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Okazje[]|\Cake\Collection\CollectionInterface $okazje
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Okazje')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-plus']) . ' ' . $this->Txt->printAdmin(__('Admin | Dodaj {0}', [$this->Txt->printAdmin(__('Admin | Okazje'))])), ['action' => 'add'], ['escape' => false, 'class' => 'btn btn-xs btn-primary']) ?></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped data-table">
                    <thead>
                        <tr>
                            <th data-priority="2"><?= $this->Txt->printAdmin(__('Admin | id')) ?></th>
                            <th data-priority="3"><?= $this->Txt->printAdmin(__('Admin | Nazwa')) ?></th>
                            <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
                        </tr>
                    </thead>
                    <tbody class="sorted-kolejnosc" sort-target="<?= Cake\Routing\Router::url(['action' => 'sort', 'kolejnosc']) ?>">
                        <?php foreach ($okazje as $okazje): ?>
                            <tr sort-item-id="<?= $okazje->id ?>">
                                <td><?= $okazje->id ?></td>
                                <td><?= $okazje->nazwa ?></td>
                                <td class="actions">
                                    <?= $this->Html->link('<i class="fa fa-cogs"></i>', ['action' => 'edit', $okazje->id], ['escape' => false, 'class' => 'btn btn-default btn-xs', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $this->Txt->printAdmin(__('Admin | Edytuj'))]) ?>
                                    <?= $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $okazje->id], ['confirm-btn-yes' => $this->Txt->printAdmin(__('Admin | Tak')), 'confirm-btn-no' => $this->Txt->printAdmin(__('Admin | Nie')), 'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $okazje->id)), 'escape' => false, 'class' => 'btn btn-danger btn-xs confirm-link', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $this->Txt->printAdmin(__('Admin | Usuń'))]) ?>
                                    <?php if (!key_exists('sort', $_GET) && empty($filter)) { ?>
                            <spna class="sort-item-point btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="<?= $this->Txt->printAdmin(__('Admin | Przeciągnij aby zmienić kolejność')) ?>"><i class="fa fa-arrows-alt"></i></spna>
                            <?php } ?>
                        </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>