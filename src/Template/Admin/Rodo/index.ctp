<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Rodo[]|\Cake\Collection\CollectionInterface $rodo
  */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Rodo')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Html->tag('i','',['class'=>'fa fa-plus']).' '.$this->Txt->printAdmin(__('Admin | Dodaj {0}',[$this->Txt->printAdmin(__('Admin | Rodo'))])), ['action' => 'add'],['escape'=>false,'class'=>'btn btn-xs btn-primary']) ?></li>
                    <li>
                        <?=$this->Form->create(null,['type'=>'file','id'=>'form-import','url'=> \Cake\Routing\Router::url(['action'=>'import'])])?>
                        <div class="file-container">
                            <input type="file" id="import-plik" onchange="$('#form-import').submit();" name="plik" style="display: none;"/>
                            <button type="button" class="btn btn-xs btn-primary" onclick="$('#import-plik').trigger('click');"><i class="fa fa-upload"></i> <?=$this->Txt->printAdmin(__('Admin | Importuj z csv'))?></button>
                        </div>
                        <?=$this->Form->end()?>
                    </li>
                </ul>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped">
                    <thead>
            <tr>
                                <th data-priority="3"><?= $this->Txt->printAdmin(__('Admin | email')) ?></th>
                                <th data-priority="5"><?= $this->Txt->printAdmin(__('Admin | data zatwierdzenia')) ?></th>
                                <th data-priority="6"><?= $this->Txt->printAdmin(__('Admin | zgoda_1')) ?></th>
                                <th data-priority="7"><?= $this->Txt->printAdmin(__('Admin | zgoda_2')) ?></th>
                                <th data-priority="8"><?= $this->Txt->printAdmin(__('Admin | zgoda_3')) ?></th>
                                <th data-priority="9"><?= $this->Txt->printAdmin(__('Admin | zgoda_4')) ?></th>
                                <th data-priority="10"><?= $this->Txt->printAdmin(__('Admin | zgoda_5')) ?></th>
                                <th data-priority="11"><?= $this->Txt->printAdmin(__('Admin | zgoda_6')) ?></th>
                                <th data-priority="12"><?= $this->Txt->printAdmin(__('Admin | zgoda_7')) ?></th>
                                <th data-priority="13"><?= $this->Txt->printAdmin(__('Admin | zgoda_8')) ?></th>
                                <th data-priority="14"><?= $this->Txt->printAdmin(__('Admin | telefon')) ?></th>
                                <th data-priority="16"><?= $this->Txt->printAdmin(__('Admin | data wysłania')) ?></th>
                                <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($rodo as $rodo): ?>
            <tr>
                <td><?= $rodo->email ?></td>
                <td><?= $rodo->data_zatwierdzenia ?></td>
                <td><?= $this->Txt->printBool($rodo->zgoda_1) ?></td>
                <td><?= $this->Txt->printBool($rodo->zgoda_2) ?></td>
                <td><?= $this->Txt->printBool($rodo->zgoda_3) ?></td>
                <td><?= $this->Txt->printBool($rodo->zgoda_4) ?></td>
                <td><?= $this->Txt->printBool($rodo->zgoda_5) ?></td>
                <td><?= $this->Txt->printBool($rodo->zgoda_6) ?></td>
                <td><?= $this->Txt->printBool($rodo->zgoda_7) ?></td>
                <td><?= $this->Txt->printBool($rodo->zgoda_8) ?></td>
                <td><?= $rodo->telefon ?></td>
                <td><?= $rodo->data_wyslania ?></td>
                <td class="actions">
                    <?= $this->Html->link('<i class="fa fa-cogs"></i>', ['action' => 'edit', $rodo->id],['escape'=>false,'class'=>'btn btn-default btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Edytuj'))]) ?>
                    <?= $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $rodo->id], ['confirm-btn-yes'=>$this->Txt->printAdmin(__('Admin | Tak')),'confirm-btn-no'=>$this->Txt->printAdmin(__('Admin | Nie')),'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $rodo->id)),'escape'=>false,'class'=>'btn btn-danger btn-xs confirm-link','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Usuń'))]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
                </table>
                <?=$this->element('admin/paginator')?>
            </div>
        </div>
    </div>
</div>