<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Gatunek[]|\Cake\Collection\CollectionInterface $gatunek
  */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Gatunek')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Html->tag('i','',['class'=>'fa fa-plus']).' '.$this->Txt->printAdmin(__('Admin | Dodaj {0}',[$this->Txt->printAdmin(__('Admin | Gatunek'))])), ['action' => 'add'],['escape'=>false,'class'=>'btn btn-xs btn-primary']) ?></li>
                </ul>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped">
                    <thead>
            <tr>
                                <th data-priority="2"><?= $this->Txt->printAdmin(__('Admin | id')) ?></th>
                                <th data-priority="3"><?= $this->Txt->printAdmin(__('Admin | Zdjęcie')) ?></th>
                                <th data-priority="3"><?= $this->Txt->printAdmin(__('Admin | Nazwa')) ?></th>
                                <th data-priority="3"><?= $this->Txt->printAdmin(__('Admin | Nazwa na głównej')) ?></th>
                                <th data-priority="3"><?= $this->Txt->printAdmin(__('Admin | Url')) ?></th>
                                <th data-priority="6"><?= $this->Txt->printAdmin(__('Admin | Strona główna')) ?></th>
                                <th data-priority="6"><?= $this->Txt->printAdmin(__('Admin | Ukryty')) ?></th>
                                <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($gatunek as $gatunek): ?>
            <tr>
                <td><?= $gatunek->id ?></td>
                <td><?=(!empty($gatunek->zdjecie) ? $this->Html->image($displayPath['gatunek'].$gatunek->zdjecie,['style'=>'max-height:80px;']) : '')?></td>
                <td><?= $gatunek->nazwa ?></td>
                <td><?= (!empty($gatunek->nazwa_2)?$gatunek->nazwa_2:$gatunek->nazwa) ?></td>
                <td><a href="<?= \Cake\Routing\Router::url(['controller'=>'Towar','action'=>'index','type'=>$gatunek->id,'prefix'=>false]) ?>" target="_blank"><?= \Cake\Routing\Router::url(['controller'=>'Towar','action'=>'index','type'=>$gatunek->id,'prefix'=>false]) ?></a></td>
                <td><?=$this->Txt->printBool($gatunek->glowna, str_replace($basePath, '/', Cake\Routing\Router::url(['controller' => 'Gatunek', 'action' => 'setField', $gatunek->id, 'glowna', (!empty($gatunek->glowna)?0:1)])))?></td>
                <td><?=$this->Txt->printBool($gatunek->ukryty, str_replace($basePath, '/', Cake\Routing\Router::url(['controller' => 'Gatunek', 'action' => 'setField', $gatunek->id, 'ukryty', (!empty($gatunek->ukryty)?0:1)])))?></td>
                <td class="actions">
                    <?= $this->Html->link('<i class="fa fa-cogs"></i>', ['action' => 'edit', $gatunek->id],['escape'=>false,'class'=>'btn btn-default btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Edytuj'))]) ?>
                    <?= $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $gatunek->id], ['confirm-btn-yes'=>$this->Txt->printAdmin(__('Admin | Tak')),'confirm-btn-no'=>$this->Txt->printAdmin(__('Admin | Nie')),'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $gatunek->id)),'escape'=>false,'class'=>'btn btn-danger btn-xs confirm-link','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Usuń'))]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
                </table>
            </div>
        </div>
    </div>
</div>