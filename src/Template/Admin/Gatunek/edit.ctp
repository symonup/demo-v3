<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <?= $this->Form->create($gatunek,['type'=>'file']) ?>
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Edit {0}', [__('Admin | Gatunek')])) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?=
                        $this->Html->link(
                                $this->Html->Tag('i', '', ['class' => 'fa fa-trash']) . ' ' . $this->Txt->printAdmin(__('Admin | Usuń')), ['action' => 'delete', $gatunek->id], ['confirm-btn-yes' => $this->Txt->printAdmin(__('Admin | Tak')), 'confirm-btn-no' => $this->Txt->printAdmin(__('Admin | Nie')), 'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $gatunek->id)), 'class' => 'btn btn-xs btn-danger confirm-link', 'escape' => false]
                        )
                        ?></li>
                    <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> ' . __('Admin | Wróć do {0}', [__('Admin | Listy')])), ['action' => 'index'], ['escape' => false, 'class' => 'btn btn-xs btn-warning']) ?></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <fieldset>
                    <?php
                    $lngInputs = [];
                    $lngInputs['nazwa'] = ['label' => $this->Txt->printAdmin(__('Admin | Nazwa - {0}'))];
                    $lngInputs['nazwa_2'] = ['label' => $this->Txt->printAdmin(__('Admin | Nazwa na kafelkach strony głównej - {0}'))];
                    echo $this->Translation->inputs($this->Form, $languages, $lngInputs);
                    echo $this->Form->control('mask', ['label' => ['text'=>$this->Txt->printAdmin(__('Admin | Przyjazny URL (np: <i>gry-akcji</i> zamiast <i>towar/lista?typ=1</i>)')),'escape'=>false]]);
                    echo $this->Txt->file($this->Form->control('zdjecie', ['type' => 'file', 'label' => false]), $this->Txt->printAdmin(__('Admin | Dodaj zdjęcie')), (!empty($gatunek->zdjecie) ? $displayPath['gatunek'] . $gatunek->zdjecie : ''));
                    echo $this->Form->control('glowna', ['label' => $this->Txt->printAdmin(__('Admin | Wyświetlaj na stronie głównej'))]);
                    echo $this->Form->control('ukryty', ['label' => $this->Txt->printAdmin(__('Admin | Gatunek niewidoczny na stronie'))]);
                    ?>
                </fieldset>
            </div>
            <div class="x_footer text-right">
                <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')), ['class' => 'btn btn-sm btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
