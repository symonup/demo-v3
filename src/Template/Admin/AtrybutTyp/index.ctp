<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\AtrybutTyp[]|\Cake\Collection\CollectionInterface $atrybutTyp
  */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Atrybut Typ')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Html->tag('i','',['class'=>'fa fa-plus']).' '.$this->Txt->printAdmin(__('Admin | Dodaj {0}',[$this->Txt->printAdmin(__('Admin | nowy typ atrybutów'))])), ['action' => 'add'],['escape'=>false,'class'=>'btn btn-xs btn-primary']) ?></li>
       <?php /* <li><?= $this->Html->link($this->Html->tag('i','',['class'=>'fa fa-plus']).' '.$this->Txt->printAdmin(__('Admin | Dodaj {0}',[$this->Txt->printAdmin(__('Admin | Atrybut Group'))])), ['controller' => 'AtrybutTyp', 'action' => 'add_group'],['escape'=>false,'class'=>'btn btn-xs btn-primary']) ?></li> */ ?>
                </ul>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create(null, ['url' => ['controller' => 'AtrybutTyp', 'action' => 'index']]) ?>
                <table class="table table-striped">
                    <thead>
            <tr>
                                <th data-priority="2"><?= $this->Paginator->sort('id',$this->Txt->printAdmin(__('Admin | id'))) ?></th>
                                <th data-priority="3"><?= $this->Paginator->sort('nazwa',$this->Txt->printAdmin(__('Admin | nazwa'))) ?></th>
                                <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
            </tr>
                        <tr class="table-filters">
                            <td class="short"><?= $this->Form->control('id', ['label' => false, 'type' => 'text', 'value' => ((!empty($filter) && key_exists('id', $filter)) ? $filter['id'] : '')]) ?></td>
                            <td><?= $this->Form->control('nazwa', ['label' => false, 'type' => 'text', 'value' => ((!empty($filter) && key_exists('nazwa', $filter)) ? $filter['nazwa'] : '')]) ?></td>
                            <td class="actions"><button type="submit" data-toggle="tooltip" data-placment="top" data-container="body" title="<?= $this->Txt->printAdmin(__('Admin | Szukaj')) ?>" class="btn btn-xs btn-warning"><i class="fa fa-search"></i></button><?php if (!empty($filter) || key_exists('sort', $_GET)) { ?><button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placment="top" data-container="body" title="<?= $this->Txt->printAdmin(__('Admin | Wyczyść filtr')) ?>" name="clear-filter"><i class="fa fa-remove"></i></button><?php } ?></td>
                        </tr>
        </thead>
        <tbody class="sorted-kolejnosc" sort-target="<?= Cake\Routing\Router::url(['action' => 'sort', 'kolejnosc']) ?>">
            <?php foreach ($atrybutTyp as $atrybutTyp): ?>
            <?php 
            $group=false;
            if(!empty($atrybutTyp->atrybut_typ_parent)){
                $group=$atrybutTyp->atrybut_typ_parent->id;
            if(empty($tdGroups[$atrybutTyp->atrybut_typ_parent->id])){
                $tdGroups[$atrybutTyp->atrybut_typ_parent->id]=[];
                ?>
                    <tr sort-item-id="<?= $atrybutTyp->id ?>" class="group main-item" group_id="<?=$atrybutTyp->atrybut_typ_parent->id?>" group_name="<?=$atrybutTyp->atrybut_typ_parent->nazwa?>">
                        <td colspan="2">
                            <a href="javascript:void(0);" class="rozwin-grupe" group="<?=$atrybutTyp->atrybut_typ_parent->id?>"><?=$atrybutTyp->atrybut_typ_parent->nazwa?> <span class="caret"></span></a>
                        </td>
                        <td class="actions">
                                 <?= $this->Html->link('<i class="fa fa-cogs"></i>', ['action' => 'edit_group', $atrybutTyp->atrybut_typ_parent->id],['escape'=>false,'class'=>'btn btn-default btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Edytuj'))]) ?>
                    <?= $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete_group', $atrybutTyp->atrybut_typ_parent->id], ['confirm-btn-yes'=>$this->Txt->printAdmin(__('Admin | Tak')),'confirm-btn-no'=>$this->Txt->printAdmin(__('Admin | Nie')),'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $atrybutTyp->atrybut_typ_parent->nazwa)),'escape'=>false,'class'=>'btn btn-danger btn-xs confirm-link','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Usuń'))]) ?>
                        </td>
                    </tr>
                    <?php
            }
            $tdGroups[$atrybutTyp->atrybut_typ_parent->id][$atrybutTyp->id] = $atrybutTyp->nazwa;
        }
            ?>
            <tr  sort-item-id="<?= $atrybutTyp->id ?>" <?=(($group!==false)?'class="group" group_id="'.$group.'" group_name="'.$atrybutTyp->atrybut_typ_parent->nazwa.'"':'')?>>
                <td><?= $atrybutTyp->id ?></td>
                <td><?= $this->Html->link(h($atrybutTyp->nazwa), ['controller' => 'Atrybut', 'action' => 'index', $atrybutTyp->id], ['escape' => false]); ?></td>
                <td class="actions">
                    <?= $this->Html->link('<i class="fa fa-list"></i>', ['controller' => 'Atrybut', 'action' => 'index', $atrybutTyp->id], ['escape' => false,'class'=>'btn btn-default btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Lista wartości atrybutów'))]); ?>
                    <?= $this->Html->link('<i class="fa fa-cogs"></i>', ['action' => 'edit', $atrybutTyp->id],['escape'=>false,'class'=>'btn btn-default btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Edytuj'))]) ?>
                    <?= $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $atrybutTyp->id], ['confirm-btn-yes'=>$this->Txt->printAdmin(__('Admin | Tak')),'confirm-btn-no'=>$this->Txt->printAdmin(__('Admin | Nie')),'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $atrybutTyp->nazwa)),'escape'=>false,'class'=>'btn btn-danger btn-xs confirm-link','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Usuń'))]) ?>
                    <?php if (!key_exists('sort', $_GET) && empty($filter)) { ?>
                            <spna class="sort-item-point btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="<?= $this->Txt->printAdmin(__('Admin | Przeciągnij aby zmienić kolejność')) ?>"><i class="fa fa-arrows-alt"></i></spna>
                            <?php } ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
                </table>
                <?= $this->element('admin/paginator') ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>