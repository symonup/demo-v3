<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Miasto $miasto
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Miasto'), ['action' => 'edit', $miasto->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Miasto'), ['action' => 'delete', $miasto->id], ['confirm' => __('Are you sure you want to delete # {0}?', $miasto->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Miasto'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Miasto'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="miasto view large-9 medium-8 columns content">
    <h3><?= h($miasto->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nazwa') ?></th>
            <td><?= h($miasto->nazwa) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Rodzaj') ?></th>
            <td><?= h($miasto->rodzaj) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Label Position') ?></th>
            <td><?= h($miasto->label_position) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($miasto->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Szerokosc') ?></th>
            <td><?= $this->Number->format($miasto->szerokosc) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dlugosc') ?></th>
            <td><?= $this->Number->format($miasto->dlugosc) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ukryte') ?></th>
            <td><?= $miasto->ukryte ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
