<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Miasto[]|\Cake\Collection\CollectionInterface $miasto
  */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Miasto')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Html->tag('i','',['class'=>'fa fa-plus']).' '.$this->Txt->printAdmin(__('Admin | Dodaj {0}',[$this->Txt->printAdmin(__('Admin | Miasto'))])), ['action' => 'add'],['escape'=>false,'class'=>'btn btn-xs btn-primary']) ?></li>
                </ul>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped data-table">
                    <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nazwa') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ukryte') ?></th>
                <th scope="col"><?= $this->Paginator->sort('szerokosc') ?></th>
                <th scope="col"><?= $this->Paginator->sort('dlugosc') ?></th>
                <th scope="col"><?= $this->Paginator->sort('rodzaj') ?></th>
                <th scope="col"><?= $this->Paginator->sort('label_position') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cords_n_stopnie') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cords_n_minuty') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cords_e_stopnie') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cords_e_minuty') ?></th>
                <th scope="col" class="actions"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($miasto as $miasto): ?>
            <tr>
                <td><?= $this->Number->format($miasto->id) ?></td>
                <td><?= h($miasto->nazwa) ?></td>
                <td><?= h($miasto->ukryte) ?></td>
                <td><?= $this->Number->format($miasto->szerokosc) ?></td>
                <td><?= $this->Number->format($miasto->dlugosc) ?></td>
                <td><?= h($miasto->rodzaj) ?></td>
                <td><?= h($miasto->label_position) ?></td>
                <td><?= $this->Number->format($miasto->cords_n_stopnie) ?></td>
                <td><?= $this->Number->format($miasto->cords_n_minuty) ?></td>
                <td><?= $this->Number->format($miasto->cords_e_stopnie) ?></td>
                <td><?= $this->Number->format($miasto->cords_e_minuty) ?></td>
                <td class="actions">
                    <?= $this->Html->link('<i class="fa fa-cogs"></i>', ['action' => 'edit', $miasto->id],['escape'=>false,'class'=>'btn btn-default btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Edytuj'))]) ?>
                    <?= $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $miasto->id], ['confirm-btn-yes'=>$this->Txt->printAdmin(__('Admin | Tak')),'confirm-btn-no'=>$this->Txt->printAdmin(__('Admin | Nie')),'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $miasto->id)),'escape'=>false,'class'=>'btn btn-danger btn-xs confirm-link','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Usuń'))]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
                </table>
            </div>
        </div>
    </div>
</div>