<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Towar[]|\Cake\Collection\CollectionInterface $towar
  */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Porównywarki')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    </ul>
                <?=$this->element('admin/paginator_top')?>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php if(empty($_GET['page'])) { ?>
                <div class="row">
                <div class="col-md-6">
                    <h3>Linki do generowania plików</h3>
                    <ul>
                        <li><a href="<?= \Cake\Routing\Router::url(['controller'=>'Cron','action'=>'porownywarki','google','prefix'=>false],true)?>"><?= \Cake\Routing\Router::url(['controller'=>'Cron','action'=>'porownywarki','google','prefix'=>false],true)?></a></li>
                        <li><a href="<?= \Cake\Routing\Router::url(['controller'=>'Cron','action'=>'porownywarki','ceneo','prefix'=>false],true)?>"><?= \Cake\Routing\Router::url(['controller'=>'Cron','action'=>'porownywarki','ceneo','prefix'=>false],true)?></a></li>
                        <li><a href="<?= \Cake\Routing\Router::url(['controller'=>'Cron','action'=>'porownywarki','skapiec','prefix'=>false],true)?>"><?= \Cake\Routing\Router::url(['controller'=>'Cron','action'=>'porownywarki','skapiec','prefix'=>false],true)?></a></li>
                    </ul>
                </div>
                    <div class="col-md-6">
                    <?php 
        if (strpos($_SERVER['SERVER_NAME'], 'www.') === 0) {
            $fileprefix = str_replace('.', '_', str_replace('www.', '', $_SERVER['SERVER_NAME'])) . '_';
        } else {
            $fileprefix = str_replace('.', '_', $_SERVER['SERVER_NAME']) . '_';
        }
                    ?>
                    <h3>Linki do plików</h3>
                    <ul>
                        <li><a href="<?= \Cake\Routing\Router::url($displayPath['webroot'].'files/xml/'.$fileprefix.'google.xml',true)?>" target="_blank"><?= \Cake\Routing\Router::url($displayPath['webroot'].'files/xml/'.$fileprefix.'google.xml',true)?></a></li>
                        <li><a href="<?= \Cake\Routing\Router::url($displayPath['webroot'].'files/xml/'.$fileprefix.'ceneo.xml',true)?>" target="_blank"><?= \Cake\Routing\Router::url($displayPath['webroot'].'files/xml/'.$fileprefix.'ceneo.xml',true)?></a></li>
                        <li><a href="<?= \Cake\Routing\Router::url($displayPath['webroot'].'files/xml/'.$fileprefix.'skapiec.xml',true)?>" target="_blank"><?= \Cake\Routing\Router::url($displayPath['webroot'].'files/xml/'.$fileprefix.'skapiec.xml',true)?></a></li>
                    </ul>
                </div>
                </div>
                <?php } ?>
                <?=$this->Form->create(null,['url'=>['controller'=>'Towar','action'=>'porownywarki']])?>
                <div class="row">
                <div class="col-xs-12 col-md-4">
                    <?=$this->Form->control('kategoria',['onchange'=>'submit();','type'=>'select','value'=>((!empty($filter) && key_exists('kategoria', $filter))?$filter['kategoria']:''),'options'=>$katArr,'empty'=>$this->Txt->printAdmin(__('Admin | Wszystkie kategorie')),'label'=>$this->Txt->printAdmin(__('Admin | Kategoria'))])?>
                </div>
                </div>
                <table class="table table-striped table-responsive">
                    <thead>
            <tr>
                                <th data-priority="2"><?= $this->Paginator->sort('id',$this->Txt->printAdmin(__('Admin | id'))) ?></th>
                                <th data-priority="15"><?= $this->Txt->printAdmin(__('Admin | zdjęcie')) ?></th>
                                <th data-priority="3"><?= $this->Paginator->sort('kod',$this->Txt->printAdmin(__('Admin | kod'))) ?></th>
                                <th data-priority="15"><?= $this->Paginator->sort('nazwa',$this->Txt->printAdmin(__('Admin | nazwa'))) ?></th>
                                <th data-priority="15"><?= $this->Txt->printAdmin(__('Admin | cena')) ?></th>
                                <th class="text-center" data-priority="4"><?= $this->Paginator->sort('ilosc',$this->Txt->printAdmin(__('Admin | ilość'))) ?></th>
                                <th data-priority="5"><?= $this->Txt->printAdmin(__('Admin | producent')) ?></th>
                                <th data-priority="5"><?= $this->Txt->printAdmin(__('Admin | platforma')) ?></th>
                                <th data-priority="6"><?= $this->Paginator->sort('ukryty',$this->Txt->printAdmin(__('Admin | ukryty'))) ?></th>
                                <th data-priority="7"><?= $this->Paginator->sort('p_google',$this->Txt->printAdmin(__('Admin | Google'))) ?></th>
                                <th data-priority="9"><?= $this->Paginator->sort('p_ceneo',$this->Txt->printAdmin(__('Admin | Ceneo'))) ?></th>
                                <th data-priority="10"><?= $this->Paginator->sort('p_skapiec',$this->Txt->printAdmin(__('Admin | Skąpiec'))) ?></th>
                                <th></th>
            </tr>
            <tr class="table-filters">
                <td class="short hidden-xs hidden-sm"><?=$this->Form->control('id',['label'=>false,'type'=>'text','value'=>((!empty($filter) && key_exists('id', $filter))?$filter['id']:'')])?></td>
                <td class="hidden-xs hidden-sm"></td>
                <td><?=$this->Form->control('kod',['label'=>false,'type'=>'text','value'=>((!empty($filter) && key_exists('kod', $filter))?$filter['kod']:'')])?></td>
                <td><?=$this->Form->control('nazwa',['label'=>false,'type'=>'text','value'=>((!empty($filter) && key_exists('nazwa', $filter))?$filter['nazwa']:'')])?></td>
                <td></td>
                <td><?=$this->Form->control('ilosc',['label'=>false,'data-toggle'=>'tooltip','data-placment'=>'top','data-container'=>'body','title'=>$this->Txt->printAdmin(__('Admin | W tym polu można stosować znaki specjalne: >, <, >=, <=, != Przykład zastosowania: >=5 czyli towary których ilość jest większa lub równa 5.')),'type'=>'text','value'=>((!empty($filter) && key_exists('ilosc', $filter))?$filter['ilosc']:'')])?></td>
                <td data-priority="10"><?=$this->Form->control('producent_id',['type'=>'select','value'=>((!empty($filter) && key_exists('producent_id', $filter))?$filter['producent_id']:''),'options'=>$producenci,'empty'=>$this->Txt->printAdmin(__('Admin | Wszyscy producenci')),'label'=>false])?></td>
                <td data-priority="10"><?=$this->Form->control('platforma_id',['type'=>'select','value'=>((!empty($filter) && key_exists('platforma_id', $filter))?$filter['platforma_id']:''),'options'=>$platformy,'empty'=>$this->Txt->printAdmin(__('Admin | Wszystkie platformy')),'label'=>false])?></td>
                <td class="hidden-xs hidden-sm"><?=$this->Form->control('ukryty',['label'=>false,'type'=>'checkbox','value'=>1,'checked'=>((!empty($filter) && key_exists('ukryty', $filter))?true:false)])?></td>
                <td class="hidden-xs hidden-sm"><?=$this->Form->control('p_google',['label'=>false,'type'=>'checkbox','value'=>1,'checked'=>((!empty($filter) && key_exists('p_google', $filter))?true:false)])?></td>
                <td class="hidden-xs hidden-sm"><?=$this->Form->control('p_ceneo',['label'=>false,'type'=>'checkbox','value'=>1,'checked'=>((!empty($filter) && key_exists('p_ceneo', $filter))?true:false)])?></td>
                <td class="hidden-xs hidden-sm"><?=$this->Form->control('p_skapiec',['label'=>false,'type'=>'checkbox','value'=>1,'checked'=>((!empty($filter) && key_exists('p_skapiec', $filter))?true:false)])?></td>
                <td class="actions"><button type="submit" data-toggle="tooltip" data-placment="top" data-container="body" title="<?=$this->Txt->printAdmin(__('Admin | Szukaj'))?>" class="btn btn-xs btn-warning"><i class="fa fa-search"></i></button><?php if(!empty($filter)){?><button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placment="top" data-container="body" title="<?=$this->Txt->printAdmin(__('Admin | Wyczyść filtr'))?>" name="clear-filter"><i class="fa fa-remove"></i></button><?php } ?></td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($towar as $towar): ?>
            <tr id="row_<?=$towar->id?>">
                <td><?= $this->Number->format($towar->id) ?></td>
                <td><?=$this->Html->link((!empty($towar->towar_zdjecie)?$this->Html->image(str_replace('/b2b/', '/', $displayPath['towar']).$this->Txt->getKatalog($towar->towar_zdjecie[0]->id).'/thumb_'.$towar->towar_zdjecie[0]->plik,['alt'=>(!empty($towar->towar_zdjecie[0]->alt)?$towar->towar_zdjecie[0]->alt:$towar->nazwa),'style'=>'max-height:70px; max-width:100px;']):NO_PHOTO), ['controller'=>'TowarZdjecie','action' => 'index', $towar->id],['escape'=>false])?></td>
                <td><?= h($towar->kod) ?></td>
                <td><?= h($towar->nazwa) ?></td>
                <td>
                <?php 
                                    $ceny = [];
                                    if (!empty($towar->towar_cena)) {
                                        foreach ($towar->towar_cena as $towarCena) {
                                            if (!key_exists($towarCena->walutum->id, $ceny)) {
                                                $ceny[$towarCena->walutum->id] = $this->Txt->cena($towarCena->cena_sprzedazy, $towarCena->walutum->symbol);
                                            }
                                        }
                                        if (!empty($ceny)) {
                                            echo join('<br/>', $ceny);
                                        }
                                    }
                                    ?>
                </td>
                <td class="text-center"><?= $towar->ilosc ?>
                </td>
                <td><?= $towar->has('producent') ? $this->Html->link($towar->producent->nazwa, ['controller' => 'Producent', 'action' => 'edit', $towar->producent->id]) : '' ?></td>
                <td><?= $towar->has('platforma') ? $this->Html->link($towar->platforma->nazwa, ['controller' => 'Platforma', 'action' => 'edit', $towar->platforma->id]) : '' ?></td>
                <td>
                        <?=$this->Txt->printBool($towar->ukryty, str_replace($basePath, '/', Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'setField', $towar->id, 'ukryty', (!empty($towar->ukryty)?0:1)])))?>
                </td>
                <td>
                        <?=$this->Txt->printBool($towar->p_google, str_replace($basePath, '/', Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'setField', $towar->id, 'p_google', (!empty($towar->p_google)?0:1)])))?>
                </td>
                <td>
                        <?=$this->Txt->printBool($towar->p_ceneo, str_replace($basePath, '/', Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'setField', $towar->id, 'p_ceneo', (!empty($towar->p_ceneo)?0:1)])))?>
                </td>
                <td>
                        <?=$this->Txt->printBool($towar->p_skapiec, str_replace($basePath, '/', Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'setField', $towar->id, 'p_skapiec', (!empty($towar->p_skapiec)?0:1)])))?>
                </td>
                <td class="actions"></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
                </table>
                <?=$this->element('admin/paginator')?>
                <?=$this->Form->end()?>
            </div>
        </div>
    </div>
</div>