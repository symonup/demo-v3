<?php
/**
 * @var \App\View\AppView $this
 */
?>
<script type="text/javascript">
    var editTowarId = '<?= $towar->id ?>';
</script>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <?= $this->Form->create($towar) ?>
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Edit {0}', [__('Admin | Towar')])) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?=
                        $this->Html->link(
                                $this->Html->Tag('i', '', ['class' => 'fa fa-trash']) . ' ' . $this->Txt->printAdmin(__('Admin | Usuń')), ['action' => 'delete', $towar->id], ['confirm-btn-yes' => $this->Txt->printAdmin(__('Admin | Tak')), 'confirm-btn-no' => $this->Txt->printAdmin(__('Admin | Nie')), 'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $towar->id)), 'class' => 'btn btn-xs btn-danger confirm-link', 'escape' => false]
                        )
                        ?></li>
                    <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> ' . __('Admin | Wróć do {0}', [__('Admin | Listy')])), ['action' => 'index', '#' => 'row_' . $towar->id], ['escape' => false, 'class' => 'btn btn-xs btn-warning']) ?></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <fieldset>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#informacje" aria-controls="informacje" role="tab" data-toggle="tab">Informacje podstawowe</a></li>
                        <li role="presentation"><a class="towar_edit_tab" href="#atrybuty" aria-controls="atrybuty" role="tab" data-toggle="tab">Atrybuty</a></li>
                        <li role="presentation"><a class="towar_edit_tab"  href="#powiazania" aria-controls="powiazania" role="tab" data-toggle="tab">Powiązania</a></li>
                 <li role="presentation"><a class="towar_edit_tab"  href="#hot-deal" aria-controls="hot-deal" role="tab" data-toggle="tab">Produkt dnia</a></li>
                        <li role="presentation"><a class="towar_edit_tab"  href="#seo" aria-controls="seo" role="tab" data-toggle="tab">SEO</a></li>
                        <li role="presentation"><a class="towar_edit_tab" hide-save="true" href="#zestawy" aria-controls="zestawy" role="tab" data-toggle="tab">Zestawy</a></li>
                        <li role="presentation"><a class="towar_edit_tab" hide-save="true"  href="#zdjecia" aria-controls="zdjecia" role="tab" data-toggle="tab">Zdjęcia</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <?php
                        $lngInputs = [];
                        $lngInputs2 = [];
                        $lngInputs['nazwa'] = ['label' => ['text' => $this->Txt->printAdmin(__('Admin | Nazwa produktu - {0}')) . $this->Txt->helpInfo('towar', 'nazwa'), 'escape' => false]];
                        $lngInputs['zestaw_nazwa'] = ['label' => ['text' => $this->Txt->printAdmin(__('Admin | Nazwa produktu w zestawie - {0}')) . $this->Txt->helpInfo('towar', 'zestaw_nazwa'), 'escape' => false]];
                        $lngInputs['nazwa_promocja'] = ['label' => ['text' => $this->Txt->printAdmin(__('Admin | Nazwa w produkt tygodnia - {0}')) . $this->Txt->helpInfo('towar', 'nazwa_promocja'), 'escape' => false]];
                        $lngInputs['opis'] = ['label' => ['text' => $this->Txt->printAdmin(__('Admin | Pełny opis produktu - {0}')) . $this->Txt->helpInfo('towar', 'opis'), 'escape' => false], 'summernote' => 'true'];
                        $lngInputs['opis2'] = ['label' => ['text' => $this->Txt->printAdmin(__('Admin | Alternatywny opis dla wersji mobilnej - {0}')) . $this->Txt->helpInfo('towar', 'opis2'), 'escape' => false], 'summernote' => 'true'];
                        $lngInputs2['seo_tytul'] = ['label' => ['text' => $this->Txt->printAdmin(__('Admin | SEO tytuł - {0}')) . $this->Txt->helpInfo('towar', 'seo_tytul'), 'escape' => false]];
                        $lngInputs2['seo_slowa'] = ['label' => ['text' => $this->Txt->printAdmin(__('Admin | SEO słowa kluczowe - {0}')) . $this->Txt->helpInfo('towar', 'seo_slowa'), 'escape' => false]];
                        $lngInputs2['seo_opis'] = ['label' => ['text' => $this->Txt->printAdmin(__('Admin | SEO opis - {0}')) . $this->Txt->helpInfo('towar', 'seo_opis'), 'escape' => false]];
                        ?>

                        <div role="tabpanel" class="tab-pane active" id="informacje">
                            <div class="row">
                                <div class="col-md-4">
                                    <h3><?= $this->Txt->printAdmin(__('Admin | Dane podstawowe')) ?></h3>
                                    <?php
                                    echo $this->Form->control('producent_id', ['options' => $producent, 'empty' => $this->Txt->printAdmin(__('Admin | Wybierz')), 'label' => ['text' => $this->Txt->printAdmin(__('Admin | Producent')) . $this->Txt->helpInfo('towar', 'producent_id'), 'escape' => false]]);
                                    echo $this->Form->control('kod', ['label' => ['text' => $this->Txt->printAdmin(__('Admin | Kod produktu')) . $this->Txt->helpInfo('towar', 'kod'), 'escape' => false]]);
                                    echo $this->Form->control('ean', ['label' => ['text' => $this->Txt->printAdmin(__('Admin | Kod EAN')) . $this->Txt->helpInfo('towar', 'ean'), 'escape' => false]]);
                                    echo $this->Form->control('plec', ['options' => ['girl'=>$this->Txt->printAdmin(__('Admin | Dziewczynka')),'boy'=>$this->Txt->printAdmin(__('Admin | Chłopiec'))], 'empty' => $this->Txt->printAdmin(__('Admin | Niedotyczy')), 'label' => ['text' => $this->Txt->printAdmin(__('Admin | Płeć')) . $this->Txt->helpInfo('towar', 'plec'), 'escape' => false]]);
                                    echo $this->Form->control('wiek_id', ['options' => $wiek, 'empty' => $this->Txt->printAdmin(__('Admin | Niedotyczy')), 'label' => ['text' => $this->Txt->printAdmin(__('Admin | Wiek')) . $this->Txt->helpInfo('towar', 'wiek_id'), 'escape' => false]]);
                                    echo $this->Form->control('opakowanie_id', ['options' => $opakowanie, 'empty' => $this->Txt->printAdmin(__('Admin | Niedotyczy')), 'label' => ['text' => $this->Txt->printAdmin(__('Admin | Opakowanie')) . $this->Txt->helpInfo('towar', 'opakowanie_id'), 'escape' => false]]);
                                    echo $this->Form->control('ilosc_w_kartonie', ['type'=>'number','min'=>1,'default'=>1,'label' => ['text' => $this->Txt->printAdmin(__('Admin | Ilość szt. w kartonie')) . $this->Txt->helpInfo('towar', 'ilosc_w_kartonie'), 'escape' => false],'required'=>false]);
                                    ?>
                                    
                                            <?=$this->Form->control('gratis', ['type'=>'checkbox','label' => ['text' => $this->Txt->printAdmin(__('Admin | Może być użyty jako gratis')) . $this->Txt->helpInfo('towar', 'gratis'), 'escape' => false]]);?>
                                            <?=$this->Form->control('gratis_wartosc', ['type'=>'text','data-type'=>'float','label' => ['text' => $this->Txt->printAdmin(__('Admin | Minimalna wartość zamówienia dla gratisu')) . $this->Txt->helpInfo('towar', 'gratis_wartosc'), 'escape' => false]]);?>
                                            
                                </div>
                                <div class="col-md-8">
                                    <h3><?= $this->Txt->printAdmin(__('Admin | Ceny i ilości')) ?></h3>
                                    <div class="row">
                                        <div class="col-md-3"><?php
                                            echo $this->Form->control('ilosc', ['label' => $this->Txt->printAdmin(__('Admin | Ilość'))]);
                                            ?>
                                        </div>
                                        <div class="col-md-3"><?php
                                            echo $this->Form->control('max_ilosc', ['label' => ['text' => $this->Txt->printAdmin(__('Admin | 100% dostępności')) . $this->Txt->helpInfo('towar', 'max_ilosc'), 'escape' => false]]);
                                            ?>
                                        </div>
                                    <?php /*   <div class="col-md-3"><?php
                                            echo $this->Form->control('bonus_id', ['type' => 'select', 'options' => $bonusy, 'empty' => $this->Txt->printAdmin(__('Admin | Brak bonusu')), 'label' => ['text' => $this->Txt->printAdmin(__('Admin | Bonusowe złotówki')) . $this->Txt->helpInfo('towar', 'bonus_id'), 'escape' => false]]);
                                            ?>
                                        </div> */ ?>
                                        <div class="col-md-3"><?php
                                            echo $this->Form->control('waga', ['type' => 'text', 'data-type' => 'float', 'label' => ['text' => $this->Txt->printAdmin(__('Admin | Waga')) . $this->Txt->helpInfo('towar', 'waga'), 'escape' => false]]);
                                            ?>
                                        </div>
                                    </div>
                                    <?php
                                    $cenaTabsContent = [];
                                    $cenaTabsLi = [];
                                    foreach ($waluty as $waluta) {
                                        $tabContent = '';
                                        $cenaTabsLi[] = $this->Html->tag('li', $this->Html->tag('a', $waluta->symbol, ['href' => '#waluta-tab-' . $waluta->id, 'aria-controls' => 'waluta-tab-' . $waluta->id, 'role' => 'tab', 'data-toggle' => 'tab']), ['role' => 'presentation', 'class' => (empty($cenaTabsLi) ? 'active' : '')]);
                                        $tabContent .= $this->Form->control('towar_cena.' . $waluta->id . '.id', ['type' => 'hidden']);
                                        $tabContent .= $this->Form->control('towar_cena.' . $waluta->id . '.waluta_id', ['type' => 'hidden', 'value' => $waluta->id]);
                                        $tabContent .= $this->Form->control('towar_cena.' . $waluta->id . '.rodzaj', ['type' => 'hidden', 'value' => 'brutto', 'required' => false]);
                                        $tabContent .= $this->Form->control('towar_cena.' . $waluta->id . '.cena_katalogowa', ['type' => 'hidden', 'data-type' => 'float', 'value' => 0, 'required' => false]);
                                        $tabContent .= '<div class="col-md-6">';
                                        $tabContent .= $this->Form->control('towar_cena.' . $waluta->id . '.vat_id', ['type' => 'select', 'options' => $vat, 'required' => false]);
                                        $tabContent .= '</div><div class="col-md-6">';
                                        $tabContent .= $this->Form->control('towar_cena.' . $waluta->id . '.jednostka_id', ['type' => 'select', 'options' => $jednostki, 'required' => false]);
                                        $tabContent .= '</div><div class="col-md-6">';
                                        $tabContent .= $this->Form->control('towar_cena.' . $waluta->id . '.cena_sprzedazy', ['type' => 'text', 'data-type' => 'float', 'default' => (!empty($towar->towar_cena_default) ? $towar->towar_cena_default->cena_sprzedazy : ''), 'label' => ['text' => $this->Txt->printAdmin(__('Admin | Cena sprzedaży')) . $this->Txt->helpInfo('towar', 'cena_sprzedazy'), 'escape' => false], 'required' => false]);
                                        $tabContent .= '</div><div class="col-md-6">';
                                        $tabContent .= $this->Form->control('towar_cena.' . $waluta->id . '.cena_promocja', ['type' => 'text', 'data-type' => 'float', 'default' => (!empty($towar->towar_cena_default) ? $towar->towar_cena_default->cena_promocja : ''), 'label' => ['text' => $this->Txt->printAdmin(__('Admin | Cena promocyjna')) . $this->Txt->helpInfo('towar', 'cena_promocja'), 'escape' => false], 'required' => false]);
                                        $tabContent .= '</div>';
                                        $cenaTabsContent[] = $this->Html->tag('div', $this->Html->tag('div', $tabContent, ['class' => 'row']), ['role' => 'tabpanel', 'class' => 'tab-pane tab-ceny overflow_hidden' . (empty($cenaTabsContent) ? ' active' : ''), 'id' => 'waluta-tab-' . $waluta->id]);
                                    }
                                    ?>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <?= join('', $cenaTabsLi) ?>
                                            </ul>
                                            <div class="tab-content">
                                                <?= join('', $cenaTabsContent) ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12"><hr/></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-4">
                                            <?=$this->Form->control('punkty', ['label' => ['text' => $this->Txt->printAdmin(__('Admin | Punkty za zamówienie')) . $this->Txt->helpInfo('towar', 'punkty'), 'escape' => false]]);?>
                                        </div>
                                        <div class="col-xs-12 col-md-4">
                                            <?=$this->Form->control('gwarancja', ['placeholder'=>$this->Txt->printAdmin(__('Admin | np. 2 lata')),'label' => ['text' => $this->Txt->printAdmin(__('Admin | Informacje o gwarancji')) . $this->Txt->helpInfo('towar', 'gwarancja'), 'escape' => false]]);?>
                                        </div>
                                        <div class="col-xs-12 col-md-4">
                                            <?=$this->Form->control('zwrot', ['placeholder'=>$this->Txt->printAdmin(__('Admin | np. 30 dni na zwrot')),'label' => ['text' => $this->Txt->printAdmin(__('Admin | Informacje o zwrocie')) . $this->Txt->helpInfo('towar', 'zwrot'), 'escape' => false]]);?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-4">
                                            <?=$this->Form->control('wymiary', ['placeholder'=>$this->Txt->printAdmin(__('Admin | np. 30 x 20 x 15 cm')),'label' => ['text' => $this->Txt->printAdmin(__('Admin | Wymiary')) . $this->Txt->helpInfo('towar', 'wymiary'), 'escape' => false]]);?>
                                        </div>
                                        <div class="col-xs-12 col-md-4">
                                            <?=$this->Form->control('wymiary_opakowania', ['placeholder'=>$this->Txt->printAdmin(__('Admin | np. 30 x 20 x 15 cm')),'label' => ['text' => $this->Txt->printAdmin(__('Admin | Wymiary opakowania')) . $this->Txt->helpInfo('towar', 'wymiary_opakowania'), 'escape' => false]]);?>
                                        </div>
                                        <div class="col-xs-12 col-md-4">
                                            <?=$this->Form->control('waga_kg', ['placeholder'=>$this->Txt->printAdmin(__('Admin | np. 2,5 kg')),'label' => ['text' => $this->Txt->printAdmin(__('Admin | Rzeczywista waga')) . $this->Txt->helpInfo('towar', 'waga_kg'), 'escape' => false]]);?>
                                        </div>
                                        <div class="col-xs-12 col-md-4">
                                            <?=$this->Form->control('baterie', ['placeholder'=>$this->Txt->printAdmin(__('Admin | np. 3x AAA (w zestawie)')),'label' => ['text' => $this->Txt->printAdmin(__('Admin | Baterie')) . $this->Txt->helpInfo('towar', 'baterie'), 'escape' => false]]);?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-lg-12">
                                    <h3><?= $this->Txt->printAdmin(__('Admin | Nazwa i opis')) ?></h3>
                                    <?php
                                    echo $this->Translation->inputs($this->Form, $languages, $lngInputs);
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-lg-12">
                                    <?= $this->Form->control('tagi', ['type' => 'text', 'tagsinput' => 'true', 'label' => ['text' => $this->Txt->printAdmin(__('Admin | Tagi wyszukiwania')) . $this->Txt->helpInfo('towar', 'tagi'), 'escape' => false]]) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <h3>Kategorie</h3>
                                    <?= $this->element('admin/kategorie', ['katArr' => $katArr, 'kategoriaSelected' => $kategoriaSelected]) ?>
                                </div>
                                <div class="col-xs-12 col-md-6 child-cols-3">
                                    <h3>Certyfikaty</h3>
                                    <?= $this->Form->control('certyfikat._ids', ['options' => $certyfikat, 'multiple' => 'checkbox', 'label' => false]) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 child-cols-3">
                                    <h3>Prezent</h3>
                                    <?= $this->Form->control('prezent', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Nadaje się na prezent'))]); ?>
                                    <?= $this->Form->control('okazje._ids', ['options' => $okazje, 'multiple' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Wybierz okazje'))]) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <h3><?= $this->Txt->printAdmin(__('Admin | Opcje')) ?></h3>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <?php
                                    echo $this->Form->control('promocja', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Promocja'))]);
                                    echo $this->Form->control('polecany', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Polecany'))]);
                                    echo $this->Form->control('wyprzedaz', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Wyprzedaż'))]);
                                    echo $this->Form->control('wyrozniony', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Bestseller'))]);
                                    echo $this->Form->control('nowosc', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Nowość'))]);
                                    ?>    
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <?php
//                                    echo $this->Form->control('produkt_tygodnia', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Hit tygodnia'))]);
//                                    echo $this->Form->control('bez_konfekcji', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Produkt nie podlego konfekcji'))]);
//                                    echo $this->Form->control('wyklucz_z_gm', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Produkt nieuwzględniany w Google Merchant'))]);
//                                    echo $this->Form->control('mix', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Mix wielu zapachów'))]);
                                    echo $this->Form->control('ukryty', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Produkt nie widoczny na stronie'))]);
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="atrybuty">
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <?= $this->element('admin/towar_atrybuty') ?>
                                </div>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="powiazania">  
                            <?php /*  <div class="row">
                              <div class="col-xs-12 col-lg-12">
                              <?php
                              echo $this->element('admin/produkty_akcesoria');
                              ?>
                              </div>
                              </div> */ ?>
                            <div class="row">
                                <div class="col-xs-12 col-lg-12">
                                    <?php
                                    echo $this->element('admin/produkty_podobne');
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-lg-12">
                                    <?php
                                    echo $this->element('admin/produkty_polecane');
                                    ?>
                                </div>
                            </div>
                            <?php /*   <div class="row">
                              <div class="col-lg-12">
                              <?php
                              echo $this->element('admin/produkty_artykuly');
                              ?>
                              </div>
                              </div> */ ?>
                        </div>

                  <div role="tabpanel" class="tab-pane" id="hot-deal">
                            <div class="row align-content-end">
                                <div class="col-xs-12 col-md-12">
                                    <h3><?= $this->Txt->printAdmin(__('Admin | PRODUKT DNIA')) ?></h3>
                                </div>
                                <div class="col-xs-12 col-md-12"><?= $this->Form->control('hot_deal_edit.aktywna', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Oferta aktywna'))]) ?></div>
                                <div class="col-xs-12 col-md-4"><?= $this->Form->control('hot_deal_edit.cena', ['type' => 'text', 'data-type' => 'float', 'label' => $this->Txt->printAdmin(__('Admin | Cena w promocji'))]) ?></div>
                                <div class="col-xs-12 col-md-4"><?= $this->Form->control('hot_deal_edit.ilosc', ['type' => 'number', 'label' => $this->Txt->printAdmin(__('Admin | Ilość dostępna w promocji'))]) ?></div>
                                <div class="col-xs-12 col-md-4"><?= $this->Form->control('hot_deal_edit.sprzedano', ['type' => 'number', 'label' => $this->Txt->printAdmin(__('Admin | Sprzedano (wartość będzie się zwiększać)'))]) ?></div>
                                <div class="col-xs-12 col-md-6"><?= $this->Form->control('hot_deal_edit.data_od', ['type' => 'text', 'autocomplete' => 'off', 'datepicker' => 'datetime', 'value' => ((!empty($towar->hot_deal_edit) && !empty($towar->hot_deal_edit->data_od)) ? $towar->hot_deal_edit->data_od->format('Y-m-d H:i:s') : ''), 'label' => $this->Txt->printAdmin(__('Admin | Data rozpoczęcia promocji'))]) ?></div>
                                <div class="col-xs-12 col-md-6"><?= $this->Form->control('hot_deal_edit.data_do', ['type' => 'text', 'autocomplete' => 'off', 'datepicker' => 'datetime', 'value' => ((!empty($towar->hot_deal_edit) && !empty($towar->hot_deal_edit->data_do)) ? $towar->hot_deal_edit->data_do->format('Y-m-d H:i:s') : ''), 'label' => $this->Txt->printAdmin(__('Admin | Data zakończenia promocji'))]) ?></div>

                                <div class="col-xs-12 col-md-12">
                                    <?= $this->Translation->inputs($this->Form, $languages, ['hot_deal_edit.nazwa' => ['type' => 'text', 'label' => $this->Txt->printAdmin(__('Admin | Nazwa produktu w promocji'))]]) ?>
                                </div>
                            </div>
                        </div> 
                        <div role="tabpanel" class="tab-pane" id="seo">
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <h3><?= $this->Txt->printAdmin(__('Admin | Dane SEO')) ?></h3>
                                </div>
                                <div class="col-xs-12 col-md-12">
                                    <?php
                                    echo $this->Translation->inputs($this->Form, $languages, $lngInputs2);
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="zestawy">
                            <div class="row">
                                <div class="col-xs-12" style="padding: 0;">
                                    <iframe src="<?= \Cake\Routing\Router::url(['controller' => 'Zestaw', 'action' => 'index', $towar->id, 'iframe' => 1]) ?>" class="iframe-content"></iframe>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="zdjecia">
                            <div class="row">
                                <div class="col-xs-12" style="padding: 0;">
                                    <iframe src="<?= \Cake\Routing\Router::url(['controller' => 'TowarZdjecie', 'action' => 'index', $towar->id, 'iframe' => 1]) ?>" class="iframe-content"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            </fieldset>
        </div>
        <div class="x_footer text-right">
            <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')), ['class' => 'btn btn-sm btn-success']) ?>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
</div>
