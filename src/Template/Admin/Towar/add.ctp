<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <?= $this->Form->create($towar) ?>
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Add {0}', [__('Admin | Towar')])) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> ' . __('Admin | Wróć do {0}', [__('Admin | Listy')])), ['action' => 'index', '#' => (!empty($towar->id) ? 'row_' . $towar->id : null)], ['escape' => false, 'class' => 'btn btn-xs btn-warning']) ?></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <fieldset>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#informacje" aria-controls="informacje" role="tab" data-toggle="tab">Informacje podstawowe</a></li>
                        <li role="presentation"><a href="#atrybuty" aria-controls="atrybuty" role="tab" data-toggle="tab">Atrybuty</a></li>
                        <li role="presentation"><a href="#powiazania" aria-controls="powiazania" role="tab" data-toggle="tab">Powiązania</a></li>
                   <?php /*     <li role="presentation"><a class="towar_edit_tab"  href="#hot-deal" aria-controls="hot-deal" role="tab" data-toggle="tab">Hot Deal</a></li> */ ?>
                        <li role="presentation"><a href="#seo" aria-controls="seo" role="tab" data-toggle="tab">SEO</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <?php
                        $lngInputs = [];
                        $lngInputs2 = [];
                        $lngInputs['nazwa'] = ['label' => ['text' => $this->Txt->printAdmin(__('Admin | Nazwa produktu - {0}')) . $this->Txt->helpInfo('towar', 'nazwa'), 'escape' => false]];
                        $lngInputs['zestaw_nazwa'] = ['label' => ['text' => $this->Txt->printAdmin(__('Admin | Nazwa produktu w zestawie - {0}')) . $this->Txt->helpInfo('towar', 'zestaw_nazwa'), 'escape' => false]];
                        $lngInputs['nazwa_promocja'] = ['label' => ['text' => $this->Txt->printAdmin(__('Admin | Nazwa w produkt tygodnia - {0}')) . $this->Txt->helpInfo('towar', 'nazwa_promocja'), 'escape' => false]];
                        $lngInputs['opis'] = ['label' => ['text' => $this->Txt->printAdmin(__('Admin | Pełny opis produktu - {0}')) . $this->Txt->helpInfo('towar', 'opis'), 'escape' => false], 'summernote' => 'true'];
                        $lngInputs['opis2'] = ['label' => ['text' => $this->Txt->printAdmin(__('Admin | Alternatywny opis dla wersji mobilnej - {0}')) . $this->Txt->helpInfo('towar', 'opis2'), 'escape' => false], 'summernote' => 'true'];
                        $lngInputs2['seo_tytul'] = ['label' => ['text' => $this->Txt->printAdmin(__('Admin | SEO tytuł - {0}')) . $this->Txt->helpInfo('towar', 'seo_tytul'), 'escape' => false]];
                        $lngInputs2['seo_slowa'] = ['label' => ['text' => $this->Txt->printAdmin(__('Admin | SEO słowa kluczowe - {0}')) . $this->Txt->helpInfo('towar', 'seo_slowa'), 'escape' => false]];
                        $lngInputs2['seo_opis'] = ['label' => ['text' => $this->Txt->printAdmin(__('Admin | SEO opis - {0}')) . $this->Txt->helpInfo('towar', 'seo_opis'), 'escape' => false]];
                        ?>

                        <div role="tabpanel" class="tab-pane active" id="informacje">
                            <div class="row">
                                <div class="col-md-4">
                                    <h3><?= $this->Txt->printAdmin(__('Admin | Dane podstawowe')) ?></h3>
                                    <?php
                                    echo $this->Form->control('producent_id', ['options' => $producent, 'empty' => $this->Txt->printAdmin(__('Admin | Wybierz')), 'label' => ['text' => $this->Txt->printAdmin(__('Admin | Producent')) . $this->Txt->helpInfo('towar', 'producent_id'), 'escape' => false]]);
//                                    echo $this->Form->control('platforma_id', ['options' => $platformy, 'empty' => $this->Txt->printAdmin(__('Admin | Niedotyczy')), 'label' => ['text' => $this->Txt->printAdmin(__('Admin | Platforma')) . $this->Txt->helpInfo('towar', 'platforma_id'), 'escape' => false]]);
//                                    echo $this->Form->control('wersja_id', ['options' => $wersje, 'empty' => $this->Txt->printAdmin(__('Admin | Niedotyczy')), 'label' => ['text' => $this->Txt->printAdmin(__('Admin | Wersja')) . $this->Txt->helpInfo('towar', 'wersja_id'), 'escape' => false]]);
                                    echo $this->Form->control('kod', ['label' => ['text' => $this->Txt->printAdmin(__('Admin | Kod produktu')) . $this->Txt->helpInfo('towar', 'kod'), 'escape' => false]]);
                                    echo $this->Form->control('ean', ['label' => ['text' => $this->Txt->printAdmin(__('Admin | Kod EAN')) . $this->Txt->helpInfo('towar', 'ean'), 'escape' => false]]);
                                   echo $this->Form->control('wiek_id', ['options' => $wiek, 'empty' => $this->Txt->printAdmin(__('Admin | Niedotyczy')), 'label' => ['text' => $this->Txt->printAdmin(__('Admin | Wiek')) . $this->Txt->helpInfo('towar', 'wiek_id'), 'escape' => false]]);
                                    echo $this->Form->control('opakowanie_id', ['options' => $opakowanie, 'empty' => $this->Txt->printAdmin(__('Admin | Niedotyczy')), 'label' => ['text' => $this->Txt->printAdmin(__('Admin | Opakowanie')) . $this->Txt->helpInfo('towar', 'opakowanie_id'), 'escape' => false]]);
                                  echo $this->Form->control('ilosc_w_kartonie', ['type'=>'number','min'=>1,'default'=>1,'label' => ['text' => $this->Txt->printAdmin(__('Admin | Ilość szt. w kartonie')) . $this->Txt->helpInfo('towar', 'ilosc_w_kartonie'), 'escape' => false],'required'=>false]);
                                       ?>    
                                </div>
                                <div class="col-md-8">
                                    <h3><?= $this->Txt->printAdmin(__('Admin | Ceny i ilości')) ?></h3>
                                    <div class="row">
                                        <div class="col-md-3"><?php
                                            echo $this->Form->control('ilosc', ['label' => $this->Txt->printAdmin(__('Admin | Ilość'))]);
                                            ?>
                                        </div>
                                        <div class="col-md-3"><?php
                                            echo $this->Form->control('max_ilosc', ['label' => ['text' => $this->Txt->printAdmin(__('Admin | 100% dostępności')) . $this->Txt->helpInfo('towar', 'max_ilosc'), 'escape' => false]]);
                                            ?>
                                        </div>
                                    <?php  /*  <div class="col-md-3"><?php
                                            echo $this->Form->control('bonus_id', ['type' => 'select', 'options' => $bonusy, 'empty' => $this->Txt->printAdmin(__('Admin | Brak bonusu')), 'label' => ['text' => $this->Txt->printAdmin(__('Admin | Bonusowe złotówki')) . $this->Txt->helpInfo('towar', 'bonus_id'), 'escape' => false]]);
                                            ?>
                                        </div> */ ?>
                                        <div class="col-md-3"><?php
                                            echo $this->Form->control('waga', ['type' => 'text', 'data-type' => 'float', 'label' => ['text' => $this->Txt->printAdmin(__('Admin | Waga')) . $this->Txt->helpInfo('towar', 'waga'), 'escape' => false]]);
                                            ?>
                                        </div>
                                    </div>
                                    <?php
                                    $cenaTabsContent = [];
                                    $cenaTabsLi = [];
                                    foreach ($waluty as $waluta) {
                                        $tabContent = '';
                                        $cenaTabsLi[] = $this->Html->tag('li', $this->Html->tag('a', $waluta->symbol, ['href' => '#waluta-tab-' . $waluta->id, 'aria-controls' => 'waluta-tab-' . $waluta->id, 'role' => 'tab', 'data-toggle' => 'tab']), ['role' => 'presentation', 'class' => (empty($cenaTabsLi) ? 'active' : '')]);
                                        $tabContent .= $this->Form->control('towar_cena.' . $waluta->id . '.waluta_id', ['type' => 'hidden', 'value' => $waluta->id]);
                                        $tabContent .= $this->Form->control('towar_cena.' . $waluta->id . '.rodzaj', ['type' => 'hidden', 'value' => 'brutto', 'required' => false]);
                                        $tabContent .= $this->Form->control('towar_cena.' . $waluta->id . '.cena_katalogowa', ['type' => 'hidden', 'data-type' => 'float', 'value' => 0, 'required' => false]);
                                        $tabContent .= '<div class="col-md-6">';
                                        $tabContent .= $this->Form->control('towar_cena.' . $waluta->id . '.vat_id', ['type' => 'select', 'options' => $vat, 'required' => false]);
                                        $tabContent .= '</div><div class="col-md-6">';
                                        $tabContent .= $this->Form->control('towar_cena.' . $waluta->id . '.jednostka_id', ['type' => 'select', 'options' => $jednostki, 'required' => false]);
                                        $tabContent .= '</div><div class="col-md-6">';
                                        $tabContent .= $this->Form->control('towar_cena.' . $waluta->id . '.cena_sprzedazy', ['type' => 'text', 'data-type' => 'float', 'default' => (!empty($towar->towar_cena_default) ? $towar->towar_cena_default->cena_sprzedazy : ''), 'label' => ['text' => $this->Txt->printAdmin(__('Admin | Cena sprzedaży')) . $this->Txt->helpInfo('towar', 'cena_sprzedazy'), 'escape' => false], 'required' => false]);
                                        $tabContent .= '</div><div class="col-md-6">';
                                        $tabContent .= $this->Form->control('towar_cena.' . $waluta->id . '.cena_promocja', ['type' => 'text', 'data-type' => 'float', 'default' => (!empty($towar->towar_cena_default) ? $towar->towar_cena_default->cena_promocja : ''), 'label' => ['text' => $this->Txt->printAdmin(__('Admin | Cena promocyjna')) . $this->Txt->helpInfo('towar', 'cena_promocja'), 'escape' => false], 'required' => false]);
                                        $tabContent .= '</div>';
                                        
                                        $cenaTabsContent[] = $this->Html->tag('div', $this->Html->tag('div', $tabContent, ['class' => 'row']), ['role' => 'tabpanel', 'class' => 'tab-pane tab-ceny overflow_hidden' . (empty($cenaTabsContent) ? ' active' : ''), 'id' => 'waluta-tab-' . $waluta->id]);
                                    }
                                    ?>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <?= join('', $cenaTabsLi) ?>
                                            </ul>
                                            <div class="tab-content">
                                                <?= join('', $cenaTabsContent) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                     <?php /*       <div class="row greyArea">
                                <div class="col-xs-12 col-lg-12">
                                    <h3><?= $this->Txt->printAdmin(__('Admin | Premiera')) . $this->Txt->helpInfo('towar', 'premiera_info') ?></h3>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-4">
                                            <?= $this->Form->control('data_premiery', ['type' => 'text', 'autocomplete' => 'off', 'datepicker' => 'date', 'value' => (!empty($towar->data_premiery) ? $towar->data_premiery->format('Y-m-d') : ''), 'label' => $this->Txt->printAdmin(__('Admin | Data premiery'))]) ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <?= $this->Form->control('preorder', ['type' => 'checkbox', 'label' => ['text' => $this->Txt->printAdmin(__('Admin | Pozwól zamawiać pre-order')) . $this->Txt->helpInfo('towar', 'preorder'), 'escape' => false]]) ?>
                                        </div>
                                        <div class="col-xs-12 col-md-4"><?= $this->Form->control('preorder_limit', ['type' => 'number', 'label' => ['text' => $this->Txt->printAdmin(__('Admin | Limit preorderów')) . $this->Txt->helpInfo('towar', 'preorder_limit'), 'escape' => false]]) ?></div>
                                        <div class="col-xs-12 col-md-4"><?= $this->Form->control('preorder_od', ['type' => 'text', 'autocomplete' => 'off', 'datepicker' => 'datetime', 'value' => (!empty($towar->preorder_od) ? $towar->preorder_od->format('Y-m-d H:i') : ''), 'label' => ['text' => $this->Txt->printAdmin(__('Admin | Początek sprzedaży')) . $this->Txt->helpInfo('towar', 'preorder_od'), 'escape' => false]]) ?></div>
                                        <div class="col-xs-12 col-md-4"><?= $this->Form->control('preorder_do', ['type' => 'text', 'autocomplete' => 'off', 'datepicker' => 'datetime', 'value' => (!empty($towar->preorder_do) ? $towar->preorder_do->format('Y-m-d H:i') : ''), 'label' => ['text' => $this->Txt->printAdmin(__('Admin | Koniec sprzedaży')) . $this->Txt->helpInfo('towar', 'preorder_do'), 'escape' => false]]) ?></div>
                                    </div>
                                </div>
                            </div> */ ?>
                            <div class="row">
                                <div class="col-xs-12 col-lg-12">
                                    <h3><?= $this->Txt->printAdmin(__('Admin | Nazwa i opis')) ?></h3>
                                    <?php
                                    echo $this->Translation->inputs($this->Form, $languages, $lngInputs);
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-lg-12">
                                    <?= $this->Form->control('tagi', ['type' => 'text', 'tagsinput' => 'true', 'label' => ['text' => $this->Txt->printAdmin(__('Admin | Tagi wyszukiwania')) . $this->Txt->helpInfo('towar', 'tagi'), 'escape' => false]]) ?>
                                </div>
                            </div>
                      <?php /*      <div class="row greyArea">
                                <div class="col-xs-12">
                                    <h3><?= $this->Txt->printAdmin(__('Admin | Klasyfikacja PEGI')) ?></h3>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <h4><?= $this->Txt->printAdmin(__('Admin | Wiek')) ?></h4>
                                    <?= $this->Form->control('pegi_wiek', ['options' => \Cake\Core\Configure::read('pegi_wiek'), 'empty' => $this->Txt->printAdmin(__('Admin | Bez ograniczeń')), 'label' => false]) ?>
                                </div>
                                <div class="col-xs-12 col-md-6 child-cols-3">
                                    <h4><?= $this->Txt->printAdmin(__('Admin | Rodzaj')) ?></h4>
                                    <?= $this->Form->control('pegi_typ', ['options' => \Cake\Core\Configure::read('pegi_typ'), 'multiple' => 'checkbox', 'label' => false]) ?>
                                </div>
                            </div> */ ?>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <h3>Kategorie</h3>
                                    <?= $this->element('admin/kategorie', ['katArr' => $katArr, 'kategoriaSelected' => $kategoriaSelected]) ?>
                                </div>
                             
                                <div class="col-xs-12 col-md-6 child-cols-3">
                                    <h3>Certyfikaty</h3>
                                    <?= $this->Form->control('certyfikat._ids', ['options' => $certyfikat, 'multiple' => 'checkbox', 'label' => false]) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 child-cols-3">
                                    <h3>Prezent</h3>
                                    <?= $this->Form->control('prezent', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Nadaje się na prezent'))]); ?>
                                    <?= $this->Form->control('okazje._ids', ['options' => $okazje, 'multiple' => 'checkbox', 'label' => false]) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <h3><?= $this->Txt->printAdmin(__('Admin | Opcje')) ?></h3>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <?php
                                    echo $this->Form->control('promocja', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Promocja'))]);
                                    echo $this->Form->control('polecany', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Polecany'))]);
                                    echo $this->Form->control('wyprzedaz', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Wyprzedaż'))]);
                                    echo $this->Form->control('wyrozniony', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Bestseller'))]);
                                    echo $this->Form->control('nowosc', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Nowość'))]);
                                    ?>    
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <?php
//                                    echo $this->Form->control('produkt_tygodnia', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Hit tygodnia'))]);
//                                    echo $this->Form->control('bez_konfekcji', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Produkt nie podlego konfekcji'))]);
//                                    echo $this->Form->control('wyklucz_z_gm', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Produkt nieuwzględniany w Google Merchant'))]);
//                                    echo $this->Form->control('mix', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Mix wielu zapachów'))]);
                                    echo $this->Form->control('ukryty', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Produkt nie widoczny na stronie'))]);
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="atrybuty">
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <?= $this->element('admin/towar_atrybuty') ?>
                                </div>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="powiazania">  
                            <?php /*  <div class="row">
                              <div class="col-xs-12 col-lg-12">
                              <?php
                              echo $this->element('admin/produkty_akcesoria');
                              ?>
                              </div>
                              </div> */ ?>
                            <div class="row">
                                <div class="col-xs-12 col-lg-12">
                                    <?php
                                    echo $this->element('admin/produkty_podobne');
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                              <div class="col-xs-12 col-lg-12">
                              <?php
                              echo $this->element('admin/produkty_polecane');
                              ?>
                              </div>
                            </div>
                            <?php /*   <div class="row">
                              <div class="col-lg-12">
                              <?php
                              echo $this->element('admin/produkty_artykuly');
                              ?>
                              </div>
                              </div> */ ?>
                        </div>

                    <?php /*    <div role="tabpanel" class="tab-pane" id="hot-deal">
                            <div class="row align-content-end">
                                <div class="col-xs-12 col-md-12">
                                    <h3><?= $this->Txt->printAdmin(__('Admin | Oferta HOT DEAL')) ?></h3>
                                </div>
                                <div class="col-xs-12 col-md-12"><?= $this->Form->control('hot_deal_edit.aktywna', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Oferta aktywna'))]) ?></div>
                                <div class="col-xs-12 col-md-4"><?= $this->Form->control('hot_deal_edit.cena', ['type' => 'text', 'data-type' => 'float', 'label' => $this->Txt->printAdmin(__('Admin | Cena w promocji'))]) ?></div>
                                <div class="col-xs-12 col-md-4"><?= $this->Form->control('hot_deal_edit.ilosc', ['type' => 'number', 'label' => $this->Txt->printAdmin(__('Admin | Ilość dostępna w promocji'))]) ?></div>
                                <div class="col-xs-12 col-md-4"><?= $this->Form->control('hot_deal_edit.sprzedano', ['type' => 'number', 'label' => $this->Txt->printAdmin(__('Admin | Sprzedano (wartość będzie się zwiększać)'))]) ?></div>
                                <div class="col-xs-12 col-md-6"><?= $this->Form->control('hot_deal_edit.data_od', ['type' => 'text', 'autocomplete' => 'off', 'datepicker' => 'datetime', 'value' => ((!empty($towar->hot_deal_edit) && !empty($towar->hot_deal_edit->data_od)) ? $towar->hot_deal_edit->data_od->format('Y-m-d H:i:s') : ''), 'label' => $this->Txt->printAdmin(__('Admin | Data rozpoczęcia promocji'))]) ?></div>
                                <div class="col-xs-12 col-md-6"><?= $this->Form->control('hot_deal_edit.data_do', ['type' => 'text', 'autocomplete' => 'off', 'datepicker' => 'datetime', 'value' => ((!empty($towar->hot_deal_edit) && !empty($towar->hot_deal_edit->data_do)) ? $towar->hot_deal_edit->data_do->format('Y-m-d H:i:s') : ''), 'label' => $this->Txt->printAdmin(__('Admin | Data zakończenia promocji'))]) ?></div>

                                <div class="col-xs-12 col-md-12">
                                    <?= $this->Translation->inputs($this->Form, $languages, ['hot_deal_edit.nazwa' => ['type' => 'text', 'label' => $this->Txt->printAdmin(__('Admin | Nazwa produktu w promocji'))]]) ?>
                                </div>
                            </div>
                        </div> */ ?>
                        <div role="tabpanel" class="tab-pane" id="seo">
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <h3><?= $this->Txt->printAdmin(__('Admin | Dane SEO')) ?></h3>
                                </div>
                                <div class="col-xs-12 col-md-12">
                                    <?php
                                    echo $this->Translation->inputs($this->Form, $languages, $lngInputs2);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="x_footer text-right">
                <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')), ['class' => 'btn btn-sm btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
