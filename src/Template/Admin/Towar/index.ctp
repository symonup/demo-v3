<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Towar[]|\Cake\Collection\CollectionInterface $towar
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Towar')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-plus']) . ' ' . $this->Txt->printAdmin(__('Admin | Dodaj {0}', [$this->Txt->printAdmin(__('Admin | Towar'))])), ['action' => 'add'], ['escape' => false, 'class' => 'btn btn-xs btn-primary']) ?></li>
                </ul>
                <?= $this->element('admin/paginator_top') ?>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create(null, ['url' => ['controller' => 'Towar', 'action' => 'index']]) ?>
                <table class="table table-striped table-responsive">
                    <thead>
                        <tr>
                            <th data-priority="2"><?= $this->Paginator->sort('id', $this->Txt->printAdmin(__('Admin | id'))) ?></th>
                            <th data-priority="15"><?= $this->Txt->printAdmin(__('Admin | zdjęcie')) ?></th>
                            <th data-priority="15"><?= $this->Paginator->sort('nazwa', $this->Txt->printAdmin(__('Admin | nazwa'))) ?></th>
                            <th data-priority="15"><?= $this->Txt->printAdmin(__('Admin | cena')) ?></th>
                            <th data-priority="4"><?= $this->Paginator->sort('ilosc', $this->Txt->printAdmin(__('Admin | ilość'))) ?></th>
                            <th data-priority="5"><?= $this->Txt->printAdmin(__('Admin | Producent')) ?></th>
                            <th data-priority="6"><?= $this->Paginator->sort('ukryty', $this->Txt->printAdmin(__('Admin | ukryty'))) ?></th>
                            <th data-priority="7"><?= $this->Paginator->sort('promocja', $this->Txt->printAdmin(__('Admin | promocja'))) ?></th>
                            <th data-priority="9"><?= $this->Paginator->sort('wyrpzedaz', $this->Txt->printAdmin(__('Admin | wyprzedaż'))) ?></th>
                            <th data-priority="10"><?= $this->Paginator->sort('nowosc', $this->Txt->printAdmin(__('Admin | nowość'))) ?></th>
                            <th data-priority="11"><?= $this->Paginator->sort('polecany', $this->Txt->printAdmin(__('Admin | polecany'))) ?></th>
                            <th data-priority="11"><?= $this->Paginator->sort('wyrozniony', $this->Txt->printAdmin(__('Admin | Bestseller'))) ?></th>
                      <?php /*        <th data-priority="11"><?= $this->Paginator->sort('produkt_tygodnia', $this->Txt->printAdmin(__('Admin | Hit tygodnia'))) ?></th> */ ?>
                     <th data-priority="15"><?= $this->Txt->printAdmin(__('Admin | Produkt dnia')) ?></th>
                            <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
                        </tr>
                        <tr class="table-filters">
                            <td class="short hidden-xs hidden-sm"><?= $this->Form->control('id', ['label' => false, 'type' => 'text', 'value' => ((!empty($filter) && key_exists('id', $filter)) ? $filter['id'] : '')]) ?></td>
                            <td class="hidden-xs hidden-sm"></td>
                            <td><?= $this->Form->control('nazwa', ['label' => false, 'type' => 'text', 'value' => ((!empty($filter) && key_exists('nazwa', $filter)) ? $filter['nazwa'] : '')]) ?></td>
                            <td></td>
                            <td><?= $this->Form->control('ilosc', ['label' => false, 'data-toggle' => 'tooltip', 'data-placment' => 'top', 'data-container' => 'body', 'title' => $this->Txt->printAdmin(__('Admin | W tym polu można stosować znaki specjalne: >, <, >=, <=, != Przykład zastosowania: >=5 czyli towary których ilość jest większa lub równa 5.')), 'type' => 'text', 'value' => ((!empty($filter) && key_exists('ilosc', $filter)) ? $filter['ilosc'] : '')]) ?></td>
                            <td data-priority="10"><?= $this->Form->control('producent_id', ['type' => 'select', 'value' => ((!empty($filter) && key_exists('producent_id', $filter)) ? $filter['producent_id'] : ''), 'options' => $producenci, 'empty' => $this->Txt->printAdmin(__('Admin | Wszyscy producenci')), 'label' => false]) ?></td>
                            <td class="hidden-xs hidden-sm"><?= $this->Form->control('ukryty', ['label' => false, 'type' => 'checkbox', 'value' => 1, 'checked' => ((!empty($filter) && key_exists('ukryty', $filter)) ? true : false)]) ?></td>
                            <td class="hidden-xs hidden-sm"><?= $this->Form->control('promocja', ['label' => false, 'type' => 'checkbox', 'value' => 1, 'checked' => ((!empty($filter) && key_exists('promocja', $filter)) ? true : false)]) ?></td>
                            <td class="hidden-xs hidden-sm"><?= $this->Form->control('wyprzedaz', ['label' => false, 'type' => 'checkbox', 'value' => 1, 'checked' => ((!empty($filter) && key_exists('wyprzedaz', $filter)) ? true : false)]) ?></td>
                            <td class="hidden-xs hidden-sm"><?= $this->Form->control('nowosc', ['label' => false, 'type' => 'checkbox', 'value' => 1, 'checked' => ((!empty($filter) && key_exists('nowosc', $filter)) ? true : false)]) ?></td>
                            <td class="hidden-xs hidden-sm"><?= $this->Form->control('polecany', ['label' => false, 'type' => 'checkbox', 'value' => 1, 'checked' => ((!empty($filter) && key_exists('polecany', $filter)) ? true : false)]) ?></td>
                            <td class="hidden-xs hidden-sm"><?= $this->Form->control('wyrozniony', ['label' => false, 'type' => 'checkbox', 'value' => 1, 'checked' => ((!empty($filter) && key_exists('wyrozniony', $filter)) ? true : false)]) ?></td>
                       <?php /*     <td class="hidden-xs hidden-sm"><?= $this->Form->control('produkt_tygodnia', ['label' => false, 'type' => 'checkbox', 'value' => 1, 'checked' => ((!empty($filter) && key_exists('produkt_tygodnia', $filter)) ? true : false)]) ?></td> */ ?>
                       <td data-priority="10"><?= $this->Form->control('hot_deal', ['type' => 'select', 'value' => ((!empty($filter) && key_exists('hot_deal', $filter)) ? $filter['hot_deal'] : ''), 'options' => ['all' => $this->Txt->printAdmin(__('Admin | Wszystkie')),'active' => $this->Txt->printAdmin(__('Admin | Aktywne')),'finished' => $this->Txt->printAdmin(__('Admin | Zakończone'))], 'empty' => $this->Txt->printAdmin(__('Admin | Wybierz')), 'label' => false]) ?></td>
                            <td class="actions"><button type="submit" data-toggle="tooltip" data-placment="top" data-container="body" title="<?= $this->Txt->printAdmin(__('Admin | Szukaj')) ?>" class="btn btn-xs btn-warning"><i class="fa fa-search"></i></button><?php if (!empty($filter)) { ?><button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placment="top" data-container="body" title="<?= $this->Txt->printAdmin(__('Admin | Wyczyść filtr')) ?>" name="clear-filter"><i class="fa fa-remove"></i></button><?php } ?></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($towar as $towar): ?>
                            <tr id="row_<?= $towar->id ?>">
                                <td><?= $this->Number->format($towar->id) ?></td>
                                <td><?= $this->Html->link((!empty($towar->towar_zdjecie) ? $this->Html->image(str_replace('/b2b/', '/', $displayPath['towar']) . $this->Txt->getKatalog($towar->towar_zdjecie[0]->id) . '/thumb_' . $towar->towar_zdjecie[0]->plik, ['alt' => (!empty($towar->towar_zdjecie[0]->alt) ? $towar->towar_zdjecie[0]->alt : $towar->nazwa), 'style' => 'max-height:70px; max-width:100px;']) : NO_PHOTO), ['controller' => 'TowarZdjecie', 'action' => 'index', $towar->id], ['escape' => false]) ?></td>
                                <td><?= h($towar->nazwa) ?>
                                    <div><?php
                                        echo (!empty($towar->pegi_wiek) ? $this->Html->tag('span', $this->Html->image($displayPath['webroot'] . 'img/pegi/w-' . $towar->pegi_wiek . '.png'), ['class' => 'pegi-icon']) : '');
                                        if (!empty($towar->pegi_typ)) {
                                            $pegiTypes = \Cake\Core\Configure::read('pegi_typ');
                                            foreach (explode(',', $towar->pegi_typ) as $pegiTyp) {
                                                echo $this->Html->tag('span', $this->Html->image($displayPath['webroot'] . 'img/pegi/t-' . $pegiTyp . '.png'), ['class' => 'pegi-icon', 'data-toggle' => 'tooltip', 'title' => $pegiTypes[$pegiTyp]]);
                                            }
                                        }
                                        ?></div>
                                </td>
                                <td>
                                    <?php
                                    $ceny = [];
                                    if (!empty($towar->towar_cena)) {
                                        foreach ($towar->towar_cena as $towarCena) {
                                            if (!key_exists($towarCena->walutum->id, $ceny)) {
                                                $ceny[$towarCena->walutum->id] = $this->Txt->cena($towarCena->cena_sprzedazy, $towarCena->walutum->symbol);
                                            }
                                        }
                                        if (!empty($ceny)) {
                                            echo join('<br/>', $ceny);
                                        }
                                    }
                                    ?>
                                </td>
                                <td><?= $this->Number->format($towar->ilosc) ?></td>
                                <td><?= $towar->has('producent') ? $this->Html->link($towar->producent->nazwa, ['controller' => 'Producent', 'action' => 'edit', $towar->producent->id]) : '' ?></td>
                                <td><?= $this->Txt->printBool($towar->ukryty, str_replace($basePath, '/', Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'setField', $towar->id, 'ukryty', (!empty($towar->ukryty) ? 0 : 1)]))) ?></td>
                                <td><?= $this->Txt->printBool($towar->promocja, str_replace($basePath, '/', Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'setField', $towar->id, 'promocja', (!empty($towar->promocja) ? 0 : 1)]))) ?></td>
                                <td><?= $this->Txt->printBool($towar->wyprzedaz, str_replace($basePath, '/', Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'setField', $towar->id, 'wyprzedaz', (!empty($towar->wyprzedaz) ? 0 : 1)]))) ?></td>
                                <td><?= $this->Txt->printBool($towar->nowosc, str_replace($basePath, '/', Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'setField', $towar->id, 'nowosc', (!empty($towar->nowosc) ? 0 : 1)]))) ?></td>
                                <td><?= $this->Txt->printBool($towar->polecany, str_replace($basePath, '/', Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'setField', $towar->id, 'polecany', (!empty($towar->polecany) ? 0 : 1)]))) ?></td>
                                <td><?= $this->Txt->printBool($towar->wyrozniony, str_replace($basePath, '/', Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'setField', $towar->id, 'wyrozniony', (!empty($towar->wyrozniony) ? 0 : 1)]))) ?></td>
                            <?php /*  <td><?= $this->Txt->printBool($towar->produkt_tygodnia, str_replace($basePath, '/', Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'setField', $towar->id, 'produkt_tygodnia', (!empty($towar->produkt_tygodnia) ? 0 : 1)]))) ?></td> */ ?>
                               <td><?php 
                                if(!empty($towar->hot_deal)){
                                    echo $this->Html->tag('div',$this->Txt->printAdmin(__('Admin | Pozostało {0} szt.',[$towar->hot_deal->ilosc])));
                                    echo $this->Html->tag('div',$this->Txt->printAdmin(__('Admin | Koniec {0}',[$towar->hot_deal->data_do->format('Y-m-d H:i')])));
                                }elseif(!empty ($towar->hot_deal_edit)){
                                    if(strtotime($towar->hot_deal_edit->data_do->format('Y-m-d H:i'))<time() || empty($towar->hot_deal_edit->ilosc)){
                                        echo $this->Txt->printAdmin(__('Admin | Zakończona'));
                                    }
                                    else echo $this->Txt->printAdmin(__('Admin | Nieaktywna'));
                                }
                                else{
                                    echo $this->Txt->printBool(false);
                                }
                                ?></td> 
                                <td class="actions">
                                    <div class="btn-group">
                                        <a href="<?= \Cake\Routing\Router::url(['action' => 'edit', $towar->id]) ?>" class="btn btn-default btn-xs" data-toggle="tooltip" data-placment="top" data-container="body" title="<?= $this->Txt->printAdmin(__('Admin | Edytuj')) ?>"><i class="fa fa-cogs"></i></a><button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><?= $this->Html->link($this->Txt->printAdmin(__('Admin | Podgląd')), ['controller' => 'Towar', 'action' => 'view', $towar->id, $this->Navi->friendlyUrl($towar->nazwa), 'prefix' => false], ['escape' => false, 'target' => '_blank']) ?></li>
                                            <li><?= $this->Html->link($this->Txt->printAdmin(__('Admin | Dodaj podobny')), ['action' => 'add', $towar->id], ['escape' => false]) ?></li>
                                            <li><?= $this->Html->link($this->Txt->printAdmin(__('Admin | Wystaw na allegro')), ['controller'=>'Allegro','action' => 'newAuction', $towar->id], ['escape' => false]) ?></li>
                                            <li role="separator" class="divider"></li>
                                            <li><?= $this->Html->link($this->Txt->printAdmin(__('Admin | Usuń')), ['action' => 'delete', $towar->id], ['confirm-btn-yes' => $this->Txt->printAdmin(__('Admin | Tak')), 'confirm-btn-no' => $this->Txt->printAdmin(__('Admin | Nie')), 'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $towar->nazwa)), 'escape' => false, 'class' => 'confirm-link']) ?></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <?= $this->element('admin/paginator') ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>