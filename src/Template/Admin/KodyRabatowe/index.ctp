<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\KodyRabatowe[]|\Cake\Collection\CollectionInterface $kodyRabatowe
  */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Kody Rabatowe')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Html->tag('i','',['class'=>'fa fa-plus']).' '.$this->Txt->printAdmin(__('Admin | Dodaj {0}',[$this->Txt->printAdmin(__('Admin | Kody Rabatowe'))])), ['action' => 'add'],['escape'=>false,'class'=>'btn btn-xs btn-primary']) ?></li>
                </ul>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped data-table">
                    <thead>
            <tr>
                                <th data-priority="2"><?= $this->Txt->printAdmin(__('Admin | ID')) ?></th>
                                <th data-priority="3"><?= $this->Txt->printAdmin(__('Admin | Nazwa')) ?></th>
                                <th data-priority="4"><?= $this->Txt->printAdmin(__('Admin | Kod')) ?></th>
                                <th data-priority="5"><?= $this->Txt->printAdmin(__('Admin | Rabat')) ?></th>
                                <th data-priority="10"><?= $this->Txt->printAdmin(__('Admin | Typ')) ?></th>
                                <th data-priority="6"><?= $this->Txt->printAdmin(__('Admin | Od kiedy ważny')) ?></th>
                                <th data-priority="7"><?= $this->Txt->printAdmin(__('Admin | Do kiedy ważny')) ?></th>
                                <th data-priority="8"><?= $this->Txt->printAdmin(__('Admin | Zablokowany')) ?></th>
                                <th data-priority="9"><?= $this->Txt->printAdmin(__('Admin | Data dodania')) ?></th>
                                <th data-priority="11"><?= $this->Txt->printAdmin(__('Admin | Rodzaj')) ?></th>
                                <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($kodyRabatowe as $kodyRabatowe): ?>
            <tr>
                <td><?= $kodyRabatowe->id ?></td>
                <td><?= $kodyRabatowe->nazwa ?></td>
                <td><?= $kodyRabatowe->kod ?></td>
                <td><?= $kodyRabatowe->rabat ?></td>
                <td><?= $kodyRabatowe->typ ?></td>
                <td><?= (!empty($kodyRabatowe->data_start)?$kodyRabatowe->data_start->format('Y-m-d'):'') ?></td>
                <td><?= (!empty($kodyRabatowe->data_end)?$kodyRabatowe->data_end->format('Y-m-d'):'') ?></td>
                <td><?= $this->Txt->printBool($kodyRabatowe->zablokuj) ?></td>
                <td><?= (!empty($kodyRabatowe->data_dodania)?$kodyRabatowe->data_dodania->format('Y-m-d'):'') ?></td>
                <td><?= $rabatRodzaj[$kodyRabatowe->rodzaj] ?></td>
                <td class="actions">
                    <?= $this->Html->link('<i class="fa fa-cogs"></i>', ['action' => 'edit', $kodyRabatowe->id],['escape'=>false,'class'=>'btn btn-default btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Edytuj'))]) ?>
                    <?= $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $kodyRabatowe->id], ['confirm-btn-yes'=>$this->Txt->printAdmin(__('Admin | Tak')),'confirm-btn-no'=>$this->Txt->printAdmin(__('Admin | Nie')),'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $kodyRabatowe->id)),'escape'=>false,'class'=>'btn btn-danger btn-xs confirm-link','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Usuń'))]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
                </table>
            </div>
        </div>
    </div>
</div>