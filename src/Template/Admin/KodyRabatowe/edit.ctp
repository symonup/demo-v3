<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <?= $this->Form->create($kodyRabatowe) ?>
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Edit {0}', [__('Admin | Kody Rabatowe')])) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?=
                        $this->Html->link(
                                $this->Html->Tag('i', '', ['class' => 'fa fa-trash']) . ' ' . $this->Txt->printAdmin(__('Admin | Usuń')),
                                ['action' => 'delete', $kodyRabatowe->id],
                                ['confirm-btn-yes' => $this->Txt->printAdmin(__('Admin | Tak')), 'confirm-btn-no' => $this->Txt->printAdmin(__('Admin | Nie')), 'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $kodyRabatowe->id)), 'class' => 'btn btn-xs btn-danger confirm-link', 'escape' => false]
                        )
                        ?></li>
                    <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> ' . __('Admin | Wróć do {0}', [__('Admin | Listy')])), ['action' => 'index'], ['escape' => false, 'class' => 'btn btn-xs btn-warning']) ?></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <fieldset>
                    <?php
                    $lngInputs = [];
                    echo $this->Form->control('nazwa', ['label' => $this->Txt->printAdmin(__('Admin | Nazwa'))]);
                    echo $this->Form->control('opis', ['type' => 'textarea', 'rows' => 2, 'label' => $this->Txt->printAdmin(__('Admin | Opis'))]);
                    echo $this->Form->input('klient_typ', ['class' => 'form-control', 'label' => __('Rodzaj klientów'), 'options' => ['b2c' => $this->Txt->printAdmin(__('Admin | Indywidualni')), 'b2b' => $this->Txt->printAdmin(__('Admin | Hurtowi'))]]);
                    echo $this->Form->input('rodzaj', ['class' => 'form-control', 'change-action' => 'show-on-action', 'label' => __('Typ'), 'options' => $rabatRodzaj]);
                    echo $this->Form->input('typ', ['class' => 'form-control', 'label' => __('Rodzaj bonusu'), 'options' => ['procent' => $this->Txt->printAdmin(__('Admin | Rabat procentowy'))]]);
                    echo $this->Form->input('rabat', ['class' => 'form-control', 'label' => __('Wartość bonusu')]);
                    echo $this->Form->control('kod', ['label' => $this->Txt->printAdmin(__('Admin | Kod'))]);
                    echo $this->Form->input('min_order', ['class' => 'form-control', 'type' => 'text', 'data-type' => 'float', 'label' => __('Minimalna wartość zamówienia')]);
                    echo $this->Form->control('data_start', ['type' => 'text', 'datepicker' => 'datetime', 'value' => (!empty($kodyRabatowe->data_start) ? $kodyRabatowe->data_start->format('Y-m-d H:i:s') : ''), 'empty' => true, 'label' => $this->Txt->printAdmin(__('Admin | Data początku ważności kodu')), 'default' => date('Y-m-d H:i:00')]);
                    echo $this->Form->control('data_end', ['type' => 'text', 'datepicker' => 'datetime', 'value' => (!empty($kodyRabatowe->data_end) ? $kodyRabatowe->data_end->format('Y-m-d H:i:s') : ''), 'empty' => true, 'label' => $this->Txt->printAdmin(__('Admin | Data końca ważności kodu')), 'default' => date('Y-m-d H:i:00')]);
                    echo $this->Form->control('multi', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Może łączyć się z innymi kodami'))]);
                    echo $this->Form->control('zablokuj', ['label' => $this->Txt->printAdmin(__('Admin | Zablokuj dostępność kodu'))]);
                    ?>
                    <div class="show-on-action show-on-kategoria">
                        <h3>Kategorie</h3>
<?= $this->element('admin/kategorie', ['katArr' => $katArr, 'kategoriaSelected' => $kategoriaSelected]) ?>
                    </div>
                    <div class="show-on-action show-on-producent child-col-3">
                        <h3>Producenci</h3>
<?= $this->Form->control('producent._ids', ['type' => 'select', 'multiple' => 'checkbox', 'options' => $producenci, 'label' => false]) ?>
                    </div>
                </fieldset>
            </div>
            <div class="x_footer text-right">
            <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')), ['class' => 'btn btn-sm btn-success']) ?>
            </div>
<?= $this->Form->end() ?>
        </div>
    </div>
</div>
