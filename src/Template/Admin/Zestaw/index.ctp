<div class="row">
    <div class="col-xs-12 col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <?php 
                if(!empty($towar)){
                      ?>
                <h2><?= $this->Txt->printAdmin(__('Admin | Lista zestawów dla towaru: {0}',[$towar->nazwa])) ?></h2>
                <?php 
                }else{
                    ?>
                <h2><?= $this->Txt->printAdmin(__('Admin | Lista zestawów')) ?></h2>
                <?php 
                }
                ?>
                <ul class="nav navbar-right panel_toolbox">
                    <?php
                    if (!empty($id)) {
                        ?>
                        <li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-plus']) . ' ' . $this->Txt->printAdmin(__('Admin | Dodaj {0}', [$this->Txt->printAdmin(__('Admin | zestaw'))])), ['action' => 'add', $id,'iframe'=>(!empty($iframe)?$iframe:null)], ['escape' => false, 'class' => 'btn btn-xs btn-primary']) ?></li>
                <?php
                    }
                    if(!empty($id) && empty($iframe)){
                        ?>
                            <li><?= $this->Html->link($this->Txt->printAdmin( __('Admin | Przejdź do {0}', [__('Admin | wszystkich zestawów')])), ['action' => 'index','iframe'=>(!empty($iframe)?$iframe:null)], ['escape' => false, 'class' => 'btn btn-xs btn-warning']) ?></li>
                            <li><?= $this->Html->link($this->Txt->printAdmin( __('Admin | Przejdź do {0}', [__('Admin | karty produktu')])), ['controller'=>'Towar','action' => 'edit',$id], ['escape' => false, 'class' => 'btn btn-xs btn-info']) ?></li>
                        <?php
                    }
                    ?>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php
    $th = [];
    $td = [];
    $th[] = $this->Paginator->sort('id');
    $th[] = __('Towar');
    $th[] = __('Actions');
    foreach ($zestaw as $item) {
        $tds = [];
        $tds[] = $item->nazwa;
        $tds[] = $this->element('admin/towar_zestaw', ['zestaw' => $item]);
        $tds[] = (($item->typ == 1) ? 'rabat: '.$item->rabat . '%' : (($item->typ == 2) ? 'rabat: '.$this->Txt->cena($item->rabat) : 'bon: ' . $this->Txt->cena($item->rabat)));

        $tds[] = $this->Html->link('<i class="fa fa-cogs"></i>', ['action' => 'edit', $item->id,'iframe'=>(!empty($iframe)?$iframe:null)], ['escape' => false, 'class' => 'btn btn-xs btn-default'])
                . $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $item->id,'t'=>$item->token,'iframe'=>(!empty($iframe)?$iframe:null)], ['escape' => false,'confirm-message'=>$this->Txt->printAdmin(__('Admin | Czy na pewno chcesz usunąć zestaw?')), 'class' => 'btn btn-xs btn-danger confirm-link'])
                . $this->Form->input('sort.' . $item->id, ['type' => 'hidden', 'value' => $item->kolejnosc, 'class' => 'tableSort'])
                . $this->Form->input('sortIds[]', ['type' => 'hidden', 'value' => $item->id, 'class' => 'tableSortIds']);

        $td[] = $tds;
    }
    if(!empty($allTowary)){
        ?>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <?=$this->Form->control('towar_id',['options'=>$allTowary,'label'=>__('Zestaw dla produktu:'),'empty'=>__('Wszystkie zestawy'),'data-target'=> '/admin/zestaw/index/','id'=>'selectZestawItem','select2'=>'true'])?>
                    </div>
                </div>
                <?php
    }
    if (!empty($td)) {
        ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Zestaw</th>
                    <th></th>
                    <th>Bonus</th>
                    <th class="actions">Actions</th>
                </tr>
            </thead>
            <tbody>

                <?php
                foreach ($td as $tr) {
                    ?>
                    <tr>
                        <?= '<td>' . join('</td><td>', $tr) . '</td>' ?>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <?php
    }
    ?>
            </div>
        </div>
    </div>
</div>
