<?php
echo $this->Html->tag('div',
        $this->Html->tag('div',
        $this->Html->tag('div',(!empty($towar->towar_zdjecie)?((file_exists($filePath['towar'].$this->Txt->getKatalog($towar->towar_zdjecie[0]->id).DS.$towar->towar_zdjecie[0]->plik))?$this->Html->image('/files/images/'.$this->Txt->getKatalog($towar->towar_zdjecie[0]->id).'/thumb_'.$towar->towar_zdjecie[0]->plik):NO_PHOTO):NO_PHOTO),['class'=>'image'])
        .$this->Html->tag('div',$towar->nazwa,['class'=>'name'])
                
                )
        .$this->Form->input('zestaw_towar.'.$towar->id.'.towar_id',['type'=>'hidden','value'=>$towar->id])
        .$this->Html->tag('div',$this->Form->input('zestaw_towar.'.$towar->id.'.wymiana',['type'=>'hidden','value'=>0]),['class'=>'wymiana_value hidden','towar_id'=>$towar->id])
//        .$this->Html->tag('div',$this->Form->input('zestaw_towar.'.$towar->id.'.wymiana',['type'=>'radio','value'=>0,'options'=>[0=>__('Towar stały'),1=>__('Lista towarów'),2=>__('Towary z kategorii')],'label'=>'Wymiana towaru w zestawie']),['class'=>'wymiana_value','towar_id'=>$towar->id])
//        .$this->Html->tag('div',$this->element('admin/produkty_wymiana')
//                .$this->Html->tag('div',$this->Form->input('zestaw_towar.'.$towar->id.'.kategoria_id',['label'=>__('Wybierz kategorię do wymiany produktu:'),'type'=>'select','options'=>$kategoria,'class'=>'form-control','empty'=>true,'disabled'=>true]),['class'=>'zestaw_wymiana_kategorie','style'=>'display:none;'])
//                ,['class'=>'wymiana_content','id'=>'wymiana_'.$towar->id])
        . $this->Html->tag('div',$this->Html->tag('button',__('Usuń').' '.$this->Navi->getIcon('glyphicon-remove-circle'),['type'=>'button','class'=>'btn btn-sm btn-danger remove_zestaw_towar','data-confirm'=>__('Czy napewno chcesz usunąć produkt {0} z zestawu',$towar->nazwa),'escape'=>false]),['class'=>'text-right clear-both'])
        .'<i class="fa fa-arrows fa-2x zestaw-order-handle"></i>',['class'=>'zestaw_element']);
?>
