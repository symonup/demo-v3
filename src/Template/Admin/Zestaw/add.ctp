<div class="row">
    <div class="col-xs-12 col-lg-12">
        <div class="x_panel">
            <?= $this->Form->create($zestaw, ['class' => 'form-default']) ?>
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Dodaj zestaw dla towaru: {0}', [$towar[0]->nazwa])) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> ' . __('Admin | Wróć do {0}', [__('Admin | Listy')])), ['action' => 'index', (!empty($towar[0]->id) ? $towar[0]->id : null),'#'=>(!empty($towar[0]->id)?'row_'.$towar[0]->id:null), 'iframe' => (!empty($iframe) ? $iframe : null)], ['escape' => false, 'class' => 'btn btn-xs btn-warning']) ?></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <fieldset>
                    <?php
                    echo $this->Form->input('typ', ['type' => 'hidden','value'=>1]);
                    echo $this->Form->input('nazwa', ['class' => 'form-control', 'label' => __('Nazwa zestawu')]);
                    echo $this->Form->input('rabat', ['class' => 'form-control', 'label' => __('Wartość rabatu w %')]);
                    echo $this->Form->input('search', ['type' => 'text', 'label' => $this->Txt->printAdmin(__('Admin | Wyszukaj produktu do zestawu podając jego nazwę lub kod.')), 'autocomplete' => 'off', 'id' => 'findElement', 'class' => 'form-control', 'div' => ['class' => 'form-group input text'], 'templates' => ['formGroup' => '{{label}}{{input}}<div class="searching"></div>']]);
                    echo $this->Html->tag('div', '', ['id' => 'findResult']);
                    echo $this->Html->tag('div', '', ['id' => 'elementy_zestawu', 'class' => 'elementy_zestawu_sortable']);
                    ?>
                </fieldset>
            </div>
            <div class="x_footer">
                <?= $this->Form->submit(__('zapisz'), array('class' => 'btn btn-md btn-success')); ?>
            </div>

            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
