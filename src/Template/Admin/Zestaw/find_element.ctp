<?php
$elements = '';
if (!empty($towary)) {
    foreach ($towary as $towar) {
        $elements.=$this->Html->tag('tr', $this->Html->tag('td', $towar->nazwa)
                . $this->Html->tag('td', (!empty($towar->platforma)?$towar->platforma->nazwa:''))
                . $this->Html->tag('td', (!empty($towar->towar_cena)?$this->Txt->cena($towar->towar_cena[0]->cena_sprzedazy):''))
                . $this->Html->tag('td', (!empty($towar->towar_cena)?$this->Txt->cena($towar->towar_cena[0]->cena_promocja):''))
                . $this->Html->tag('td', (($towar->promocja==1)?__('yes'):__('no')))
                . $this->Html->tag('td', $this->Html->tag('button', __('Dodaj towar do zestawu'), ['type' => 'button', 'class' => 'btn btn-xs btn-primary','towar_id'=>$towar->id]))
                , ['class' => 'find_element']);
    }
    echo $this->Html->tag('table',$this->Html->tag('tr',
            $this->Html->tag('th',__('Nazwa'))
            .$this->Html->tag('th',__('Platforma'))
            .$this->Html->tag('th',__('Cena'))
            .$this->Html->tag('th',__('Cena promocyjna'))
            .$this->Html->tag('th',__('Promocja'))
            .$this->Html->tag('th',' ')
            )
            .$elements,['class'=>'find_elements table table-hover']);
}
else
    echo __('Nie znaleziono towaru');
?>
