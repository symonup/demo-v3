<div class="row">
    <div class="col-xs-12 col-lg-12">
        <div class="x_panel">
            <?= $this->Form->create($zestaw, ['class' => 'form-default']) ?>
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Edytuj zestaw dla towaru: {0}', [$zestaw->towar->nazwa])) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> ' . __('Admin | Wróć do {0}', [__('Admin | Listy')])), ['action' => 'index', (!empty($zestaw->towar->id) ? $zestaw->towar->id : null),'#'=>(!empty($zestaw->towar->id)?'row_'.$zestaw->towar->id:null),'iframe'=>(!empty($iframe)?$iframe:null)], ['escape' => false, 'class' => 'btn btn-xs btn-warning']) ?></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <fieldset>
                    <?php
                    echo $this->Form->input('nazwa', ['class' => 'form-control', 'label' => __('Nazwa zestawu')]);
                    echo $this->Form->input('rabat', ['class' => 'form-control', 'label' => __('Wartość rabatu w %')]);
                    $elementy = '';
                    foreach ($zestaw->zestaw_towar as $zestaw_towar) {
                        $towar = $zestaw_towar->towar;
                        
                        $elementy .= $this->Html->tag('div', $this->Html->tag('div', $this->Html->tag('div', (!empty($towar->towar_zdjecie)?$this->Html->image(str_replace('/b2b/', '/', $displayPath['towar']).$this->Txt->getKatalog($towar->towar_zdjecie[0]->id).'/thumb_'.$towar->towar_zdjecie[0]->plik,['alt'=>(!empty($towar->towar_zdjecie[0]->alt)?$towar->towar_zdjecie[0]->alt:$towar->nazwa)]):NO_PHOTO), ['class' => 'image'])
                                        . $this->Html->tag('div', $towar->nazwa, ['class' => 'name'])
                                ), ['class' => 'zestaw_element']);
                    }
                    echo $this->Html->tag('div', $elementy, ['id' => 'elementy_zestawu', 'class' => 'elementy_zestawu_sortable']);
                    ?>
                </fieldset>

            </div>
            <div class="x_footer">
                <?= $this->Form->submit(__('zapisz'), array('class' => 'btn btn-md btn-success')); ?>
            </div>

            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<div class="zestaw col-xs-12 col-sm-12 col-md-10 col-lg-10">

</div>
