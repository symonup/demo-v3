<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Zestaw'), ['action' => 'edit', $zestaw->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Zestaw'), ['action' => 'delete', $zestaw->id], ['confirm' => __('Are you sure you want to delete # {0}?', $zestaw->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Zestaw'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Zestaw'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Towar'), ['controller' => 'Towar', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Towar'), ['controller' => 'Towar', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="zestaw view large-10 medium-9 columns">
    <h2><?= h($zestaw->id) ?></h2>
    <div class="row">
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($zestaw->id) ?></p>
            <h6 class="subheader"><?= __('Towar Id') ?></h6>
            <p><?= $this->Number->format($zestaw->towar_id) ?></p>
            <h6 class="subheader"><?= __('Rabat') ?></h6>
            <p><?= $this->Number->format($zestaw->rabat) ?></p>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Towar') ?></h4>
    <?php if (!empty($zestaw->towar)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Kod') ?></th>
            <th><?= __('Ilosc') ?></th>
            <th><?= __('Producent Id') ?></th>
            <th><?= __('Ukryty') ?></th>
            <th><?= __('Promocja') ?></th>
            <th><?= __('Polecany') ?></th>
            <th><?= __('Wyprzedaz') ?></th>
            <th><?= __('Wyrozniony') ?></th>
            <th><?= __('Gwarancja Producenta') ?></th>
            <th><?= __('Bez Konfekcji') ?></th>
            <th><?= __('Nazwa') ?></th>
            <th><?= __('Opis') ?></th>
            <th><?= __('Opis2') ?></th>
            <th><?= __('Hint Cena') ?></th>
            <th><?= __('Seo Tytul') ?></th>
            <th><?= __('Seo Slowa') ?></th>
            <th><?= __('Seo Opis') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($zestaw->towar as $towar): ?>
        <tr>
            <td><?= h($towar->id) ?></td>
            <td><?= h($towar->kod) ?></td>
            <td><?= h($towar->ilosc) ?></td>
            <td><?= h($towar->producent_id) ?></td>
            <td><?= h($towar->ukryty) ?></td>
            <td><?= h($towar->promocja) ?></td>
            <td><?= h($towar->polecany) ?></td>
            <td><?= h($towar->wyprzedaz) ?></td>
            <td><?= h($towar->wyrozniony) ?></td>
            <td><?= h($towar->gwarancja_producenta) ?></td>
            <td><?= h($towar->bez_konfekcji) ?></td>
            <td><?= h($towar->nazwa) ?></td>
            <td><?= h($towar->opis) ?></td>
            <td><?= h($towar->opis2) ?></td>
            <td><?= h($towar->hint_cena) ?></td>
            <td><?= h($towar->seo_tytul) ?></td>
            <td><?= h($towar->seo_slowa) ?></td>
            <td><?= h($towar->seo_opis) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Towar', 'action' => 'view', $towar->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Towar', 'action' => 'edit', $towar->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Towar', 'action' => 'delete', $towar->id], ['confirm' => __('Are you sure you want to delete # {0}?', $towar->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
