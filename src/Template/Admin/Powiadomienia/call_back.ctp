<div class="modal fade modalRemoveOnClose" id="callback-reply-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <?=$this->Form->create($callBack,['class'=>'ajaxFormModal','modal-target'=>'#callback-reply-modal'])?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?=$this->Txt->printAdmin(__('Admin | Obsługa oddzwonienia'))?></h4>
      </div>
      <div class="modal-body">
          <ul>
              <li><?= $this->Txt->printAdmin(__('Admin | Telefon: {0}',[$callBack->telefon])) ?></li>
              <li><?= $this->Txt->printAdmin(__('Admin | Data zgłoszenia: {0}',[$this->Txt->printDate($callBack->data,'Y-m-d H:i')])) ?></li>
          </ul>
          <?=$this->Form->control('status',['type'=>'select','options'=>$callsStat,'label'=>$this->Txt->printAdmin(__('Admin | Status'))])?>
          <?=$this->Form->control('notatki',['type'=>'textarea','id'=>'callback-notatki-tresc','label'=>$this->Txt->printAdmin(__('Admin | Notatki'))])?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->Txt->printAdmin(__('Admin | Anuluj'))?></button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-save reload-unactive"></i><i class="fa fa-spinner reload-active"></i> <?=$this->Txt->printAdmin(__('Admin | Zapisz'))?></button>
      </div>
        <?=$this->Form->end()?>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->