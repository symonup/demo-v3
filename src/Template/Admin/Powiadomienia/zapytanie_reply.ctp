<div class="modal fade modalRemoveOnClose" id="quick-reply-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <?= $this->Form->create(null, ['class' => 'ajaxFormModal', 'modal-target' => '#quick-reply-modal']) ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= $this->Txt->printAdmin(__('Admin | Odpowiedź na zapytanie')) ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <ul>
                            <li><?= $this->Txt->printAdmin(__('Admin | Nr: {0}', [$zapytanie->nr_zgloszenia])) ?></li>
                            <li><?= $this->Txt->printAdmin(__('Admin | Imię: {0}', [$zapytanie->imie])) ?></li>
                            <li><?= $this->Txt->printAdmin(__('Admin | Email: {0}', [$zapytanie->email])) ?></li>
                            <li><?= $this->Txt->printAdmin(__('Admin | Telefon: {0}', [$zapytanie->telefon])) ?></li>
                            <li><?= $this->Txt->printAdmin(__('Admin | Treść:')) ?><div class="like-textarea"><?= nl2br($zapytanie->tresc) ?></div></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-md-8">
                        <?= $this->Form->control('temat', ['type' => 'text', 'id' => 'zapytanie-odpowiedz-temat', 'label' => $this->Txt->printAdmin(__('Admin | Temat')), 'value' => $temat]) ?>
                        <div class="text-right"><button type="button" class="btn btn-xs btn-info szablon-odpowiedz" data-target="#zapytanie-odpowiedz-tresc"><?= $this->Txt->printAdmin(__('Admin | Uzyj szablonu')) ?></button></div>
                        <?= $this->Form->control('odpowiedz', ['type' => 'textarea', 'summernote' => 'true', 'id' => 'zapytanie-odpowiedz-tresc','value'=>$zapytanie->odpowiedz, 'label' => $this->Txt->printAdmin(__('Admin | Odpowiedź'))]) ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= $this->Txt->printAdmin(__('Admin | Anuluj')) ?></button>
                <button type="submit" class="btn btn-primary"><i class="fa fa-send reload-unactive"></i><i class="fa fa-spinner reload-active"></i> <?= $this->Txt->printAdmin(__('Admin | Wyślij odpowiedź')) ?></button>
            </div>
            <?= $this->Form->end() ?>
            <textarea id="szablon-zapytanie" style="display: none;">
                <?= $szablon ?>
            </textarea>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->