<div class="modal fade modalRemoveOnClose" id="quick-reply-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <?=$this->Form->create(null,['class'=>'ajaxFormModal','modal-target'=>'#quick-reply-modal'])?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?=$this->Txt->printAdmin(__('Admin | Odpowiedź na zapytanie'))?></h4>
      </div>
      <div class="modal-body">
          <ul>
              <li><?= $this->Txt->printAdmin(__('Admin | Imię: {0}',[$zapytanie->imie])) ?></li>
              <li><?= $this->Txt->printAdmin(__('Admin | Email: {0}',[$zapytanie->email])) ?></li>
              <li><?= $this->Txt->printAdmin(__('Admin | Produkt: {0}',[$zapytanie->has('towar') ? $this->Html->link($zapytanie->towar->nazwa.' ('.$this->Txt->printAdmin(__('Admin | Stan: {0}',[$zapytanie->towar->ilosc])).')', ['controller' => 'Towar', 'action' => 'view', $zapytanie->towar->id]) : ''])) ?></li>
              <?php if($zapytanie->has('punkty_odbioru')) { ?>   
              <li><?= $this->Txt->printAdmin(__('Admin | Punkt odbioru: {0}',[$zapytanie->has('punkty_odbioru') ? $this->Html->link($zapytanie->punkty_odbioru->nazwa, ['controller' => 'PunktyOdbioru', 'action' => 'view', $zapytanie->punkty_odbioru->id]) : ''])) ?></li>
              <?php } if(!empty($zapytanie->tresc)) { ?>        
              <li><?= $this->Txt->printAdmin(__('Admin | Treść:')) ?><div class="like-textarea"><?= nl2br($zapytanie->tresc) ?></div></li>
                    <?php } ?>
          </ul>
          <?=$this->Form->control('dostepny',['type'=>'select','options'=>[0=>$this->Txt->printBool(0),1=>$this->Txt->printBool(1)],'label'=>$this->Txt->printAdmin(__('Admin | Dostępne'))])?>
          <div class="text-right"><button type="button" class="btn btn-xs btn-info szablon-odpowiedz" data-target="#zapytanie-odpowiedz-tresc"><?=$this->Txt->printAdmin(__('Admin | Uzyj szablonu'))?></button></div>
          <?=$this->Form->control('odpowiedz',['type'=>'textarea','summernote'=>'true','id'=>'zapytanie-odpowiedz-tresc','label'=>$this->Txt->printAdmin(__('Admin | Odpowiedź'))])?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->Txt->printAdmin(__('Admin | Anuluj'))?></button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-send reload-unactive"></i><i class="fa fa-spinner reload-active"></i> <?=$this->Txt->printAdmin(__('Admin | Wyślij odpowiedź'))?></button>
      </div>
        <?=$this->Form->end()?>
        <textarea id="szablon-zapytanie-tak" style="display: none;">
            <?=$szablonZapytanieTak?>
        </textarea>
        <textarea id="szablon-zapytanie-nie" style="display: none;">
            <?=$szablonZapytanieNie?>
        </textarea>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->