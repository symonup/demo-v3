<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Powiadomienium[]|\Cake\Collection\CollectionInterface $powiadomienia
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Zapytania')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
<?php 
$tabActive='dostepnosc';
if(!empty($_GET['tab'])){
    $tabActive=$_GET['tab'];
}
?>
                <div>

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" <?=(($tabActive=='dostepnosc')?'class="active"':'')?>><a href="#tab-dostepnosc" aria-controls="tab-dostepnosc" role="tab" data-toggle="tab">Powiadomienia o dostępności <span class="label label-info" id="labelCountDostepnosc"><?=$zapytaniaODostepnoscCount?></span></a></li>
                     <li role="presentation" <?=(($tabActive=='zapytanie')?'class="active"':'')?>><a href="#tab-zapytanie" aria-controls="tab-zapytanie" role="tab" data-toggle="tab">Zapytania o produkt <span class="label label-info" id="labelCountPremiery"><?=$zapytaniaOProduktCount?></span></a></li>
                     <li role="presentation" <?=(($tabActive=='callback')?'class="active"':'')?>><a href="#tab-callback" aria-controls="tab-callback" role="tab" data-toggle="tab">Oddzwoń <span class="label label-info" id="labelCountCallback"><?=$callBackCount?></span></a></li>
                     <?php /*      <li role="presentation" <?=(($tabActive=='zamowienie')?'class="active"':'')?>><a href="#tab-zamowienie" aria-controls="tab-zamowienie" role="tab" data-toggle="tab">Gry na zamówienie <span class="label label-info" id="labelCountNaZamowienie"><?=$naZamowienieCount?></span></a></li>
                        <li role="presentation" <?=(($tabActive=='ceny')?'class="active"':'')?>><a href="#tab-ceny" aria-controls="tab-ceny" role="tab" data-toggle="tab">Powiadomienia o cenie</a></li> */ ?>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane <?=(($tabActive=='dostepnosc')?' active':'')?>" id="tab-dostepnosc">
                            <table class="table table-striped data-table" def-sort-direction="desc" def-sort="7">
                                <thead>
                                    <tr>
                                        <th data-priority="2"><?= $this->Txt->printAdmin(__('Admin | id')) ?></th>
                                        <th data-priority="10"><?= $this->Txt->printAdmin(__('Admin | Data zapytania')) ?></th>
                                        <th data-priority="5"><?= $this->Txt->printAdmin(__('Admin | Imię')) ?></th>
                                        <th data-priority="6"><?= $this->Txt->printAdmin(__('Admin | Email')) ?></th>
                                        <th data-priority="7"><?= $this->Txt->printAdmin(__('Admin | Zgoda na przetwarzanie danych')) ?></th>
                                        <th data-priority="8"><?= $this->Txt->printAdmin(__('Admin | Produkt')) ?></th>
                                        <th data-priority="13"><?= $this->Txt->printAdmin(__('Admin | Punkt odbioru')) ?></th>
                                        <th data-priority="11"><?= $this->Txt->printAdmin(__('Admin | Wysłana odpowiedź')) ?></th>
                                        <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($zapytaniaODostepnosc as $powiadomienie): ?>
                                        <tr>
                                            <td><?= $powiadomienie->id ?></td>
                                            <td><?= $this->Txt->printDate($powiadomienie->data_dodania, 'Y-m-d H:i') ?></td>
                                            <td><?= $powiadomienie->imie ?></td>
                                            <td><?= $powiadomienie->email ?></td>
                                            <td><?= $this->Txt->printBool($powiadomienie->zgoda) ?></td>
                                            <td><?= $powiadomienie->has('towar') ? $this->Html->link($powiadomienie->towar->nazwa . ' (' . $this->Txt->printAdmin(__('Admin | Stan: {0}', [$powiadomienie->towar->ilosc])) . ')', ['controller' => 'Towar', 'action' => 'view', $powiadomienie->towar->id]) : '' ?></td>
                                            <td><?= $powiadomienie->has('punkty_odbioru') ? $this->Html->link($powiadomienie->punkty_odbioru->nazwa, ['controller' => 'PunktyOdbioru', 'action' => 'view', $powiadomienie->punkty_odbioru->id]) : '' ?></td>
                                            <td class="wyslane" dostepnosc-item-id="<?= $powiadomienie->id ?>"><?= ((!empty($powiadomienie->wyslane) && !empty($powiadomienie->data_wyslania)) ? $this->Txt->printDate($powiadomienie->data_wyslania, 'Y-m-d H:i') : $this->Txt->printBool($powiadomienie->wyslane)) . (!empty($powiadomienie->wyslane) ? $this->Html->tag('span', $this->Txt->printAdmin(__('Admin | Dosępny: {0}', [$this->Txt->printBool($powiadomienie->dostepny)])), ['class' => 'odpowiedz-info', 'data-toggle' => 'popover', 'data-html' => 'true', 'data-trigger' => 'click | hover', 'data-container' => 'body', 'data-placement' => 'top', 'data-content' => addslashes($powiadomienie->odpowiedz)]) : '') ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link('<i class="fa fa-reply"></i>', ['action' => 'quickReply', $powiadomienie->id], ['escape' => false, 'class' => 'btn btn-default btn-xs ajaxLinkPopUp', 'modal-target' => '#quick-reply-modal', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $this->Txt->printAdmin(__('Admin | Odpowiedz'))]) ?>
                                                <?php // $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $powiadomienie->id], ['confirm-btn-yes' => $this->Txt->printAdmin(__('Admin | Tak')), 'confirm-btn-no' => $this->Txt->printAdmin(__('Admin | Nie')), 'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $powiadomienie->id)), 'escape' => false, 'class' => 'btn btn-danger btn-xs confirm-link', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $this->Txt->printAdmin(__('Admin | Usuń'))]) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
               <div role="tabpanel" class="tab-pane <?=(($tabActive=='premiery')?' active':'')?>" id="tab-zapytanie">
                            <table class="table table-striped data-table" def-sort-direction="desc" def-sort="7">
                                <thead>
                                    <tr>
                                        <th data-priority="2"><?= $this->Txt->printAdmin(__('Admin | id')) ?></th>
                                        <th data-priority="10"><?= $this->Txt->printAdmin(__('Admin | Data zapytania')) ?></th>
                                        <th data-priority="5"><?= $this->Txt->printAdmin(__('Admin | Imię')) ?></th>
                                        <th data-priority="6"><?= $this->Txt->printAdmin(__('Admin | Email')) ?></th>
                                        <th data-priority="7"><?= $this->Txt->printAdmin(__('Admin | Zgoda na przetwarzanie danych')) ?></th>
                                        <th data-priority="8"><?= $this->Txt->printAdmin(__('Admin | Produkt')) ?></th>
                                        <th data-priority="13"><?= $this->Txt->printAdmin(__('Admin | Treść zapytania')) ?></th>
                                        <th data-priority="11"><?= $this->Txt->printAdmin(__('Admin | Wysłana odpowiedź')) ?></th>
                                        <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($zapytaniaOProdukt as $powiadomienie): ?>
                                        <tr>
                                            <td><?= $powiadomienie->id ?></td>
                                            <td><?= $this->Txt->printDate($powiadomienie->data_dodania, 'Y-m-d H:i') ?></td>
                                            <td><?= $powiadomienie->imie ?></td>
                                            <td><?= $powiadomienie->email ?></td>
                                            <td><?= $this->Txt->printBool($powiadomienie->zgoda) ?></td>
                                            <td><?= $powiadomienie->has('towar') ? $this->Html->link($powiadomienie->towar->nazwa . ' (' . $this->Txt->printAdmin(__('Admin | Stan: {0}', [$powiadomienie->towar->ilosc])) . ')', ['controller' => 'Towar', 'action' => 'view', $powiadomienie->towar->id]) : '' ?></td>
                                            <td>
                                                <?= nl2br($powiadomienie->tresc)?>
                                            </td>
                                            <td class="wyslane" dostepnosc-item-id="<?= $powiadomienie->id ?>"><?= ((!empty($powiadomienie->wyslane) && !empty($powiadomienie->data_wyslania)) ? $this->Txt->printDate($powiadomienie->data_wyslania, 'Y-m-d H:i') : $this->Txt->printBool($powiadomienie->wyslane)) . (!empty($powiadomienie->wyslane) ? $this->Html->tag('span', $this->Txt->printAdmin(__('Admin | Dosępny: {0}', [$this->Txt->printBool($powiadomienie->dostepny)])), ['class' => 'odpowiedz-info', 'data-toggle' => 'popover', 'data-html' => 'true', 'data-trigger' => 'click | hover', 'data-container' => 'body', 'data-placement' => 'top', 'data-content' => addslashes($powiadomienie->odpowiedz)]) : '') ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link('<i class="fa fa-reply"></i>', ['action' => 'quickReply', $powiadomienie->id], ['escape' => false, 'class' => 'btn btn-default btn-xs ajaxLinkPopUp', 'modal-target' => '#quick-reply-modal', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $this->Txt->printAdmin(__('Admin | Odpowiedz'))]) ?>
                                                <?php // $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $powiadomienie->id], ['confirm-btn-yes' => $this->Txt->printAdmin(__('Admin | Tak')), 'confirm-btn-no' => $this->Txt->printAdmin(__('Admin | Nie')), 'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $powiadomienie->id)), 'escape' => false, 'class' => 'btn btn-danger btn-xs confirm-link', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $this->Txt->printAdmin(__('Admin | Usuń'))]) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        
                        
                        <div role="tabpanel" class="tab-pane <?=(($tabActive=='callback')?' active':'')?>" id="tab-callback">
                            <table class="table table-striped data-table" def-sort-direction="desc" def-sort="1">
                                <thead>
                                    <tr>
                                        <th data-priority="2"><?= $this->Txt->printAdmin(__('Admin | id')) ?></th>
                                        <th data-priority="10"><?= $this->Txt->printAdmin(__('Admin | Data zgloszenie')) ?></th>
                                        <th data-priority="5"><?= $this->Txt->printAdmin(__('Admin | Telefon')) ?></th>
                                        <th data-priority="6"><?= $this->Txt->printAdmin(__('Admin | Status')) ?></th>
                                        <th data-priority="7"><?= $this->Txt->printAdmin(__('Admin | Zgoda na przetwarzanie danych')) ?></th>
                                        <th data-priority="8"><?= $this->Txt->printAdmin(__('Admin | Notatki')) ?></th>
                                        <th data-priority="13"><?= $this->Txt->printAdmin(__('Admin | Oddzwonił')) ?></th>
                                        <th data-priority="13"><?= $this->Txt->printAdmin(__('Admin | Data oddzwonienia')) ?></th>
                                        <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    foreach ($callBack as $callItem): ?>
                                        <tr call-id="<?=$callItem->id?>">
                                            <td><?= $callItem->id ?></td>
                                            <td><?= $this->Txt->printDate($callItem->data, 'Y-m-d H:i') ?></td>
                                            <td><?= $callItem->telefon ?></td>
                                            <td class="call_status"><?= $callsStat[$callItem->status] ?></td>
                                            <td><?= $this->Txt->printBool($callItem->zgoda) ?></td>
                                            <td class="call_notatki">
                                                <?= nl2br($callItem->notatki)?>
                                            </td>
                                            <td class="call_admin"><?= $callItem->administrator_dane ?></td>
                                            <td class="call_data"><?= $this->Txt->printDate($callItem->data_status, 'Y-m-d H:i') ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link('<i class="fa fa-reply"></i>', ['action' => 'callBack', $callItem->id], ['escape' => false, 'class' => 'btn btn-default btn-xs ajaxLinkPopUp', 'modal-target' => '#callback-reply-modal', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $this->Txt->printAdmin(__('Admin | Obsłuż'))]) ?>
                                                <?php // $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $powiadomienie->id], ['confirm-btn-yes' => $this->Txt->printAdmin(__('Admin | Tak')), 'confirm-btn-no' => $this->Txt->printAdmin(__('Admin | Nie')), 'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $powiadomienie->id)), 'escape' => false, 'class' => 'btn btn-danger btn-xs confirm-link', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $this->Txt->printAdmin(__('Admin | Usuń'))]) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                  <?php       /*        <div role="tabpanel" class="tab-pane <?=(($tabActive=='zamowienie')?' active':'')?>" id="tab-zamowienie">
<table class="table table-striped data-table" def-sort-direction="desc" def-sort="8">
                                <thead>
                                    <tr>
                                        <th data-priority="2"><?= $this->Txt->printAdmin(__('Admin | Nr zgłoszenia')) ?></th>
                                        <th data-priority="10"><?= $this->Txt->printAdmin(__('Admin | Data zapytania')) ?></th>
                                        <th data-priority="5"><?= $this->Txt->printAdmin(__('Admin | Imię')) ?></th>
                                        <th data-priority="6"><?= $this->Txt->printAdmin(__('Admin | Email')) ?></th>
                                        <th data-priority="6"><?= $this->Txt->printAdmin(__('Admin | Telefon')) ?></th>
                                        <th data-priority="7"><?= $this->Txt->printAdmin(__('Admin | Zgoda na przetwarzanie danych')) ?></th>
                                        <th data-priority="8"><?= $this->Txt->printAdmin(__('Admin | Platforma')) ?></th>
                                        <th data-priority="13"><?= $this->Txt->printAdmin(__('Admin | Treść zapytania')) ?></th>
                                        <th data-priority="11"><?= $this->Txt->printAdmin(__('Admin | Wysłana odpowiedź')) ?></th>
                                        <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($naZamowienie as $powiadomienie): ?>
                                        <tr>
                                            <td><?= $powiadomienie->nr_zgloszenia ?></td>
                                            <td><?= $this->Txt->printDate($powiadomienie->data_dodania, 'Y-m-d H:i') ?></td>
                                            <td><?= $powiadomienie->imie ?></td>
                                            <td><?= $powiadomienie->email ?></td>
                                            <td><?= $powiadomienie->telefon ?></td>
                                            <td><?= $this->Txt->printBool($powiadomienie->zgoda_dane) ?></td>
                                            <td><?= $powiadomienie->has('platforma') ? $platformyNames[$powiadomienie->platforma] : '' ?></td>
                                            <td>
                                                <?= nl2br($powiadomienie->tresc)?>
                                            </td>
                                            <td class="wyslane" zapytanie-item-id="<?= $powiadomienie->id ?>"><?= (!empty($powiadomienie->data_wyslania) ? $this->Txt->printDate($powiadomienie->data_wyslania, 'Y-m-d H:i') : $this->Txt->printBool(0)) . (!empty($powiadomienie->odpowiedz) ? $this->Html->tag('span', $this->Txt->printAdmin(__('Admin | Pokaż odpowiedź')), ['class' => 'odpowiedz-info', 'data-toggle' => 'popover', 'data-html' => 'true', 'data-trigger' => 'click | hover', 'data-container' => 'body', 'data-placement' => 'top', 'data-content' => addslashes($powiadomienie->odpowiedz)]) : '') ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link('<i class="fa fa-reply"></i>', ['action' => 'zapytanieReply', $powiadomienie->id], ['escape' => false, 'class' => 'btn btn-default btn-xs ajaxLinkPopUp', 'modal-target' => '#quick-reply-modal', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $this->Txt->printAdmin(__('Admin | Odpowiedz'))]) ?>
                                                <?php // $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $powiadomienie->id], ['confirm-btn-yes' => $this->Txt->printAdmin(__('Admin | Tak')), 'confirm-btn-no' => $this->Txt->printAdmin(__('Admin | Nie')), 'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $powiadomienie->id)), 'escape' => false, 'class' => 'btn btn-danger btn-xs confirm-link', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $this->Txt->printAdmin(__('Admin | Usuń'))]) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane <?=(($tabActive=='ceny')?' active':'')?>" id="tab-ceny">
<table class="table table-striped data-table">
                                <thead>
                                    <tr>
                                        <th data-priority="2"><?= $this->Txt->printAdmin(__('Admin | id')) ?></th>
                                        <th data-priority="10"><?= $this->Txt->printAdmin(__('Admin | Data zapytania')) ?></th>
                                        <th data-priority="5"><?= $this->Txt->printAdmin(__('Admin | Imię')) ?></th>
                                        <th data-priority="6"><?= $this->Txt->printAdmin(__('Admin | Email')) ?></th>
                                        <th data-priority="7"><?= $this->Txt->printAdmin(__('Admin | Zgoda na przetwarzanie danych')) ?></th>
                                        <th data-priority="8"><?= $this->Txt->printAdmin(__('Admin | Produkt')) ?></th>
                                        <th data-priority="13"><?= $this->Txt->printAdmin(__('Admin | Cena w momencie zapytania')) ?></th>
                                        <th data-priority="11"><?= $this->Txt->printAdmin(__('Admin | Wysłana odpowiedź')) ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($zapytaniaOCene as $powiadomienie): ?>
                                        <tr>
                                            <td><?= $powiadomienie->id ?></td>
                                            <td><?= $this->Txt->printDate($powiadomienie->data_dodania, 'Y-m-d H:i') ?></td>
                                            <td><?= $powiadomienie->imie ?></td>
                                            <td><?= $powiadomienie->email ?></td>
                                            <td><?= $this->Txt->printBool($powiadomienie->zgoda) ?></td>
                                            <td><?= $powiadomienie->has('towar') ? $this->Html->link($powiadomienie->towar->nazwa . ' (' . $this->Txt->printAdmin(__('Admin | Stan: {0}', [$powiadomienie->towar->ilosc])) . ')', ['controller' => 'Towar', 'action' => 'view', $powiadomienie->towar->id]) : '' ?></td>
                                            <td><?= $this->Txt->cena($powiadomienie->cena) ?></td>
                                            <td class="wyslane" dostepnosc-item-id="<?= $powiadomienie->id ?>"><?= ((!empty($powiadomienie->wyslane) && !empty($powiadomienie->data_wyslania)) ? $this->Txt->printDate($powiadomienie->data_wyslania, 'Y-m-d H:i') : $this->Txt->printBool($powiadomienie->wyslane)) . (!empty($powiadomienie->wyslane) ? $this->Html->tag('span', $this->Txt->printAdmin(__('Admin | Zobacz odpowiedź')), ['class' => 'odpowiedz-info', 'data-toggle' => 'popover', 'data-html' => 'true', 'data-trigger' => 'click | hover', 'data-container' => 'body', 'data-placement' => 'top', 'data-content' => addslashes($powiadomienie->odpowiedz)]) : '') ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div> */ ?>
                    </div>

                </div>


            </div>
        </div>
    </div>
</div>