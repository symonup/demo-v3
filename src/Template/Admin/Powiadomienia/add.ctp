<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
                <?= $this->Form->create($powiadomienium) ?>
            <div class="x_title">
                    <h2><?=$this->Txt->printAdmin(__('Admin | Add {0}',[__('Admin | Powiadomienium')]))?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                                              <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> '.__('Admin | Wróć do {0}',[__('Admin | Listy')])), ['action' => 'index'],['escape'=>false,'class'=>'btn btn-xs btn-warning']) ?></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
            <div class="x_content">
    <fieldset>
        <?php
            $lngInputs=[];
                        echo $this->Form->control('typ',['label'=>$this->Txt->printAdmin(__('Admin | typ'))]);
            echo $this->Form->control('uzytkownik_id', ['options' => $uzytkownik, 'empty' => true,'label'=>$this->Txt->printAdmin(__('Admin | uzytkownik_id'))]);
            echo $this->Form->control('imie',['label'=>$this->Txt->printAdmin(__('Admin | imie'))]);
            echo $this->Form->control('email',['label'=>$this->Txt->printAdmin(__('Admin | email'))]);
            echo $this->Form->control('zgoda',['label'=>$this->Txt->printAdmin(__('Admin | zgoda'))]);
            echo $this->Form->control('towar_id', ['options' => $towar, 'empty' => true,'label'=>$this->Txt->printAdmin(__('Admin | towar_id'))]);
            echo $this->Form->control('cena',['label'=>$this->Txt->printAdmin(__('Admin | cena'))]);
            echo $this->Form->control('data_dodania',['label'=>$this->Txt->printAdmin(__('Admin | data_dodania'))]);
            echo $this->Form->control('wyslane',['label'=>$this->Txt->printAdmin(__('Admin | wyslane'))]);
            echo $this->Form->control('data_wyslania', ['type'=>'text','datepicker'=>'datetime','empty' => true,'label'=>$this->Txt->printAdmin(__('Admin | data_wyslania')),'default' => date('Y-m-d H:i:s')]);
            echo $this->Form->control('tresc',['label'=>$this->Txt->printAdmin(__('Admin | tresc'))]);
            echo $this->Form->control('punkt_odbioru', ['options' => $punktyOdbioru, 'empty' => true,'label'=>$this->Txt->printAdmin(__('Admin | punkt_odbioru'))]);
        ?>
    </fieldset>
            </div>
            <div class="x_footer text-right">
    <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')),['class'=>'btn btn-sm btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
