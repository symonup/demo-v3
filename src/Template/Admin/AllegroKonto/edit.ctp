<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <?= $this->Form->create($allegroKonto) ?>
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Edit {0}', [__('Admin | Allegro Konto')])) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?=
                        $this->Html->link(
                                $this->Html->Tag('i', '', ['class' => 'fa fa-trash']) . ' ' . $this->Txt->printAdmin(__('Admin | Usuń')), ['action' => 'delete', $allegroKonto->id], ['confirm-btn-yes' => $this->Txt->printAdmin(__('Admin | Tak')), 'confirm-btn-no' => $this->Txt->printAdmin(__('Admin | Nie')), 'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $allegroKonto->id)), 'class' => 'btn btn-xs btn-danger confirm-link', 'escape' => false]
                        )
                        ?></li>
                    <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> ' . __('Admin | Wróć do {0}', [__('Admin | Listy')])), ['action' => 'index'], ['escape' => false, 'class' => 'btn btn-xs btn-warning']) ?></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <fieldset>
                    <?php
                    $lngInputs = [];
                    echo $this->Form->control('name', ['label' => $this->Txt->printAdmin(__('Admin | Nazwa'))]);
                    echo $this->Form->control('userId', ['label' => $this->Txt->printAdmin(__('Admin | Identyfikator konta Allegro'))]);
                    echo $this->Form->control('kraj', ['label' => $this->Txt->printAdmin(__('Admin | Kraj')),'default'=>'PL','options'=> Cake\Core\Configure::read('kraje')]);
                    echo $this->Form->control('wojewodztwo', ['label' => $this->Txt->printAdmin(__('Admin | Województwo')),'options'=> $wojewodztwa]);
                    echo $this->Form->control('miasto', ['label' => $this->Txt->printAdmin(__('Admin | Miasto'))]);
                    echo $this->Form->control('kod_pocztowy', ['label' => $this->Txt->printAdmin(__('Admin | Kod pocztowy'))]);
                    echo $this->Form->control('restClientId', ['label' => $this->Txt->printAdmin(__('Admin | webClientId'))]);
                    echo $this->Form->control('restClientSecret', ['label' => $this->Txt->printAdmin(__('Admin | webClientSecret'))]);
                    echo $this->Form->control('restApiKey', ['label' => $this->Txt->printAdmin(__('Admin | webApiKey'))]);
                    echo $this->Form->control('restRedirectUri', ['label' => $this->Txt->printAdmin(__('Admin | webRedirectUri'))]);
                    echo $this->Form->control('soapApiKey', ['label' => $this->Txt->printAdmin(__('Admin | soapApiKey'))]);
                    echo $this->Form->control('devClientId', ['label' => $this->Txt->printAdmin(__('Admin | ClientId dla urządzeń'))]);
                    echo $this->Form->control('devClientSecret', ['label' => $this->Txt->printAdmin(__('Admin | ClientSecret dla urządzeń'))]);
                    ?>
                    <button type="button" class="btn btn-primary btn-xs setAllegroCategoryBtn" data-target="<?= \Cake\Routing\Router::url(['controller' => 'Allegro', 'action' => 'getCategories', $allegroKonto->id, 'modal' => 1]) ?>" input-target="#default-category" path-target="#default-category-path" path-id-target="default-category-path-id"><?= $this->Txt->printAdmin(__('Admin | Ustaw domyślną kategorię')) ?></button>
                    <div class="allegro-cat-path-html"><?=$allegroKonto->defaultCategoryPath?></div>
                        <?php
                    echo $this->Form->control('defaultCategory', ['allegro-cat-id'=>'true','type' => 'hidden']);
                    echo $this->Form->control('defaultCategoryPath', ['allegro-cat-path'=>'true','type' => 'hidden']);
                    echo $this->Form->control('defaultCategoryPathId', ['allegro-cat-path-id'=>'true','type' => 'hidden']);
                    echo $this->Form->control('isDefault', ['label' => $this->Txt->printAdmin(__('Admin | Ustaw jako domyślne'))]);
                    echo $this->Form->control('automaticRenew', ['label' => $this->Txt->printAdmin(__('Admin | Wznawiaj aukcje automatycznie po zmianie stanu magazynowego'))]);
                    ?>
                </fieldset>
            </div>
            <div class="x_footer text-right">
                <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')), ['class' => 'btn btn-sm btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
