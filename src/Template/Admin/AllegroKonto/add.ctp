<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
                <?= $this->Form->create($allegroKonto) ?>
            <div class="x_title">
                    <h2><?=$this->Txt->printAdmin(__('Admin | Add {0}',[__('Admin | Allegro Konto')]))?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                                              <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> '.__('Admin | Wróć do {0}',[__('Admin | Listy')])), ['action' => 'index'],['escape'=>false,'class'=>'btn btn-xs btn-warning']) ?></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
            <div class="x_content">
    <fieldset>
        <?php
            $lngInputs=[];
                    echo $this->Form->control('name', ['label' => $this->Txt->printAdmin(__('Admin | Nazwa'))]);
                    echo $this->Form->control('userId', ['label' => $this->Txt->printAdmin(__('Admin | Identyfikator konta Allegro'))]);
                    echo $this->Form->control('kraj', ['label' => $this->Txt->printAdmin(__('Admin | Kraj')),'default'=>'PL','options'=> Cake\Core\Configure::read('kraje')]);
                    echo $this->Form->control('wojewodztwo', ['label' => $this->Txt->printAdmin(__('Admin | Województwo')),'options'=> $wojewodztwa]);
                    echo $this->Form->control('miasto', ['label' => $this->Txt->printAdmin(__('Admin | Miasto'))]);
                    echo $this->Form->control('kod_pocztowy', ['label' => $this->Txt->printAdmin(__('Admin | Kod pocztowy'))]);
                    echo $this->Form->control('restClientId', ['label' => $this->Txt->printAdmin(__('Admin | webClientId'))]);
                    echo $this->Form->control('restClientSecret', ['label' => $this->Txt->printAdmin(__('Admin | webClientSecret'))]);
                    echo $this->Form->control('restApiKey', ['label' => $this->Txt->printAdmin(__('Admin | webApiKey'))]);
                    echo $this->Form->control('restRedirectUri', ['label' => $this->Txt->printAdmin(__('Admin | webRedirectUri'))]);
                    echo $this->Form->control('soapApiKey', ['label' => $this->Txt->printAdmin(__('Admin | soapApiKey'))]);
                    echo $this->Form->control('devClientId', ['label' => $this->Txt->printAdmin(__('Admin | ClientId dla urządzeń'))]);
                    echo $this->Form->control('devClientSecret', ['label' => $this->Txt->printAdmin(__('Admin | ClientSecret dla urządzeń'))]);
                    echo $this->Form->control('isDefault', ['label' => $this->Txt->printAdmin(__('Admin | Ustaw jako domyślne'))]);
                    echo $this->Form->control('automaticRenew', ['label' => $this->Txt->printAdmin(__('Admin | Wznawiaj aukcje automatycznie po zmianie stanu magazynowego'))]);
        ?>
    </fieldset>
            </div>
            <div class="x_footer text-right">
    <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')),['class'=>'btn btn-sm btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
