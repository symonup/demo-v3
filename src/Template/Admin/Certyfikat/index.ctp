<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Certyfikat[]|\Cake\Collection\CollectionInterface $certyfikat
  */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Certyfikat')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Html->tag('i','',['class'=>'fa fa-plus']).' '.$this->Txt->printAdmin(__('Admin | Dodaj {0}',[$this->Txt->printAdmin(__('Admin | Certyfikat'))])), ['action' => 'add'],['escape'=>false,'class'=>'btn btn-xs btn-primary']) ?></li>
                </ul>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped data-table">
                    <thead>
            <tr>
                                <th data-priority="2"><?= $this->Txt->printAdmin(__('Admin | id')) ?></th>
                                <th data-priority="4"><?= $this->Txt->printAdmin(__('Admin | Skrót')) ?></th>
                                <th data-priority="3"><?= $this->Txt->printAdmin(__('Admin | Pełna nazwa')) ?></th>
                                <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($certyfikat as $certyfikat): ?>
            <tr>
                <td><?= $certyfikat->id ?></td>
                <td><?= $certyfikat->skrot ?></td>
                <td><?= $certyfikat->nazwa ?></td>
                <td class="actions">
                    <?= $this->Html->link('<i class="fa fa-cogs"></i>', ['action' => 'edit', $certyfikat->id],['escape'=>false,'class'=>'btn btn-default btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Edytuj'))]) ?>
                    <?= $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $certyfikat->id], ['confirm-btn-yes'=>$this->Txt->printAdmin(__('Admin | Tak')),'confirm-btn-no'=>$this->Txt->printAdmin(__('Admin | Nie')),'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $certyfikat->id)),'escape'=>false,'class'=>'btn btn-danger btn-xs confirm-link','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Usuń'))]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
                </table>
            </div>
        </div>
    </div>
</div>