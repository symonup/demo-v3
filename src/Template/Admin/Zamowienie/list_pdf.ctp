<style type="text/css">
    table{
        border-collapse: collapse;
        width: 100%;
        margin-bottom: 10px;
    }
    table td{
        border: 1px solid #ccc;
        padding: 3px;
    }
    table th{
        border: 1px solid #ccc;
        padding: 3px;
        background: #ccc;
    }
    table td.sep{
        border:none;
    }
    .text-right{
        text-align: right;
    }
    .text-left{
        text-align: left;
    }
    .text-center{
        text-align: center;
    }
</style>
<table>
        <?php 
        $lp=1;
        foreach ($zamowienie as $zamowienie): ?>
        <tr>
            <th rowspan="<?=count($zamowienie->zamowienie_towar)+2?>"><?= $lp++ ?></th>
            <th class="text-left">Numer: <?= $zamowienie->numer ?> | Klient: <?= $zamowienie->imie ?> <?= $zamowienie->nazwisko ?> | <?= $zamowienie->email ?></th>
            <th>Ilość</th>
            <th>Cena</th>
        </tr>
        
                        <?php foreach ($zamowienie->zamowienie_towar as $item) {
                            ?>
                            <tr>
                                <td><?= $item->nazwa . ' ' . $item->platforma ?></td>
                                <td><?= $item->ilosc ?> szt.</td>
                                <td class="text-center"><?= $this->Txt->cena($item->cena_razem_brutto, $zamowienie->waluta_symbol, true) ?></td>
                            </tr>
                            <?php }
                        ?>
            <tr>
                <td class="text-right" colspan="2"><?= $zamowienie->wysylka->nazwa ?> (<?= $this->Txt->cena($zamowienie->wysylka_koszt, $zamowienie->waluta_symbol, true) ?>) | <b>Łączna wartość:</b></td>
                <td class="text-center"><b><?= $this->Txt->cena($zamowienie->wartosc_razem, $zamowienie->waluta_symbol) ?></b></td>
            </tr>
            <tr>
                <td class="sep">&nbsp;</td>
            </tr>
        <?php endforeach; ?>
</table>