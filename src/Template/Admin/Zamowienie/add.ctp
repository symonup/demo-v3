<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <?= $this->Form->create($zamowienie, ['id' => 'addZamowienieForm', 'type' => 'post']) ?>
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Add {0}', [__('Admin | Zamowienie')])) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> ' . __('Admin | Wróć do {0}', [__('Admin | Listy')])), ['action' => 'index'], ['escape' => false, 'class' => 'btn btn-xs btn-warning']) ?></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <fieldset>
                    <?php if (!empty($klient)) {
                        ?>
                        <div class="row">
                            <div class="col-lg-12">
                                <h3><?= $this->Txt->printAdmin(__('Admin | Tworzysz zamówienie dla klienta: {0}', [$klient->imie . ' ' . $klient->nazwisko . ' ' . $klient->firma])) ?></h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-lg-3"><input type="text" class="tableFinder form-control" placeholder="<?= $this->Txt->printAdmin(__('Admin | Szukaj..')) ?>" data-target="#addToNewZamItems" /></div>
                            <div class="col-md-8 col-lg-9"><label><input type="checkbox" class="pokaz-wybrane"/> <?= $this->Txt->printAdmin(__('Admin | Pokaż tylko wybrane')) ?></label></div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table table-striped table-hover scroll" on-page="10">
                                    <thead>
                                        <tr>
                                            <th class="w-20"></th>
                                            <th class="w-250"><?= $this->Txt->printAdmin(__('Admin | Towar')) ?></th>
                                            <th><?= $this->Txt->printAdmin(__('Admin | Kod')) ?></th>
                                            <th class="text-center"><?= $this->Txt->printAdmin(__('Admin | Ilość w kartonie')) ?></th>
                                            <th class="text-center"><?= $this->Txt->printAdmin(__('Admin | Waga kartonu')) ?></th>
                                            <th class="text-center"><?= $this->Txt->printAdmin(__('Admin | Objętość kartonu')) ?></th>
                                            <th class="text-center"><?= $this->Txt->printAdmin(__('Admin | Cena netto')) ?></th>
                                            <th class="text-center"><?= $this->Txt->printAdmin(__('Admin | Vat')) ?></th>
                                            <th class="text-center"><?= $this->Txt->printAdmin(__('Admin | Cena brutto')) ?></th>
                                            <th class="text-center"><?= $this->Txt->printAdmin(__('Admin | Ilość kartonów')) ?></th>
                                            <th class="text-right"><?= $this->Txt->printAdmin(__('Admin | Wartość netto')) ?></th>
                                            <th class="text-right"><?= $this->Txt->printAdmin(__('Admin | Wartość brutto')) ?></th>
                                        </tr>
                                    </thead>
                                    <tbody id="addToNewZamItems">
                                        <?php
                                        $tKey = 0;
                                        foreach ($towary as $towar) {
                                            $cenaNetto = $this->Txt->cenaVat($towar->towar_cena[0]->cena_sprzedazy, $towar->towar_cena[0]->vat->stawka, $towar->towar_cena[0]->rodzaj, 'netto', false);
                                            $cenaBrutto = $this->Txt->cenaVat($towar->towar_cena[0]->cena_sprzedazy, $towar->towar_cena[0]->vat->stawka, $towar->towar_cena[0]->rodzaj, 'brutto', false);
                                            
                                            if (!empty($towar->towar_atrybut)) {
                                                foreach ($towar->towar_atrybut as $towarAtrybut) {
                                                    ?>
                                                    <tr>
                                                        <td class="w-20">
                                                            <input type="checkbox" class="add_towar_item_checkbox" id="towar_<?= $towar->id ?>_<?= $towarAtrybut->id ?>"/>
                                                            <input type="hidden" disabled="disabled" name="zamowienie_towar[<?= $tKey ?>][towar_id]" value="<?= $towar->id ?>"/>
                                                            <input type="hidden" disabled="disabled" name="zamowienie_towar[<?= $tKey ?>][ilosc_w_kartonie]" value="<?= $towar->ilosc_w_kartonie ?>"/>
                                                            <input type="hidden" disabled="disabled" name="zamowienie_towar[<?= $tKey ?>][cena_za_sztuke]" value="<?= $cenaBrutto ?>"/>
                                                            <input type="hidden" disabled="disabled" name="zamowienie_towar[<?= $tKey ?>][cena_za_sztuke_netto]" value="<?= $cenaNetto ?>"/>
                                                            <input type="hidden" disabled="disabled" name="zamowienie_towar[<?= $tKey ?>][cena_za_sztuke_brutto]" value="<?= $cenaBrutto ?>"/>
                                                            <input type="hidden" disabled="disabled" name="zamowienie_towar[<?= $tKey ?>][kod]" value="<?= $towar->kod . (!empty($towarAtrybut->atrybut_podrzedne) ? $towarAtrybut->atrybut_podrzedne->nazwa : '') ?>"/>
                                                            <input type="hidden" disabled="disabled" name="zamowienie_towar[<?= $tKey ?>][nazwa]" value="<?= $towar->nazwa . ' ' . $towarAtrybut->atrybut->nazwa ?>"/>
                                                            <input type="hidden" disabled="disabled" name="zamowienie_towar[<?= $tKey ?>][wariant_id]" value="<?= $towarAtrybut->id ?>"/>
                                                            <input type="hidden" disabled="disabled" name="zamowienie_towar[<?= $tKey ?>][jednostka_id]" value="<?= $towar->towar_cena[0]->jednostka->id ?>"/>
                                                            <input type="hidden" disabled="disabled" name="zamowienie_towar[<?= $tKey ?>][jednostka_str]" value="<?= $towar->towar_cena[0]->jednostka->jednostka ?>"/>
                                                            <input type="hidden" disabled="disabled" name="zamowienie_towar[<?= $tKey ?>][vat_id]" value="<?= $towar->towar_cena[0]->vat->id ?>"/>
                                                            <input type="hidden" disabled="disabled" name="zamowienie_towar[<?= $tKey ?>][vat_stawka]" value="<?= $towar->towar_cena[0]->vat->stawka ?>"/>
                                                            <input type="hidden" disabled="disabled" name="zamowienie_towar[<?= $tKey ?>][objetosc_kartonu]" value="<?= $towar->objetosc_kartonu ?>"/>
                                                            <input type="hidden" disabled="disabled" name="zamowienie_towar[<?= $tKey ?>][waga_kartonu]" value="<?= $towar->waga_kartonu ?>"/>
                                                        </td>
                                                        <td class="w-250 text-left"><?= $towar->nazwa ?> <?= $towarAtrybut->atrybut->nazwa ?></td>
                                                        <td style="white-space: nowrap;"><?= $towar->kod . (!empty($towarAtrybut->atrybut_podrzedne) ? $towarAtrybut->atrybut_podrzedne->nazwa : '') ?></td>
                                                        <td class="text-center"><?= $towar->ilosc_w_kartonie ?></td>
                                                        <td class="text-center"><?= $towar->waga_kartonu ?></td>
                                                        <td class="text-center"><?= $towar->objetosc_kartonu ?></td>
                                                        <td class="text-center"><?= $cenaNetto ?></td>
                                                        <td class="text-center"><?= $towar->towar_cena[0]->vat->stawka ?></td>
                                                        <td class="text-center"><?= $cenaBrutto ?></td>
                                                        <td class="text-center"><input style="max-width: 80px; padding: 0 0 0 4px; text-align: center;" cena-netto="<?= $cenaNetto ?>" cena-brutto="<?= $cenaBrutto ?>" ilosc-w-kartonie="<?= $towar->ilosc_w_kartonie ?>" type="number" name="zamowienie_towar[<?= $tKey ?>][ilosc]" class="form-control input-ilosc"/></td>
                                                        <td class="cena_razem_netto text-right">0</td>
                                                        <td class="cena_razem_brutto text-right">0</td>
                                                    </tr>
                                                    <?php
                                                    $tKey++;
                                                }
                                            } else {
                                                ?>
                                                <tr>
                                                    <td class="w-20">
                                                        <input type="checkbox" class="add_towar_item_checkbox" id="towar_<?= $towar->id ?>"/>
                                                        <input type="hidden" disabled="disabled" name="zamowienie_towar[<?= $tKey ?>][towar_id]" value="<?= $towar->id ?>"/>
                                                        <input type="hidden" disabled="disabled" name="zamowienie_towar[<?= $tKey ?>][ilosc_w_kartonie]" value="<?= $towar->ilosc_w_kartonie ?>"/>
                                                        <input type="hidden" disabled="disabled" name="zamowienie_towar[<?= $tKey ?>][cena_za_sztuke]" value="<?= $cenaBrutto ?>"/>
                                                        <input type="hidden" disabled="disabled" name="zamowienie_towar[<?= $tKey ?>][cena_za_sztuke_netto]" value="<?= $cenaNetto ?>"/>
                                                        <input type="hidden" disabled="disabled" name="zamowienie_towar[<?= $tKey ?>][cena_za_sztuke_brutto]" value="<?= $cenaBrutto ?>"/>
                                                        <input type="hidden" disabled="disabled" name="zamowienie_towar[<?= $tKey ?>][kod]" value="<?= $towar->kod ?>"/>
                                                        <input type="hidden" disabled="disabled" name="zamowienie_towar[<?= $tKey ?>][nazwa]" value="<?= $towar->nazwa ?>"/>
                                                        <input type="hidden" disabled="disabled" name="zamowienie_towar[<?= $tKey ?>][jednostka_id]" value="<?= $towar->towar_cena[0]->jednostka->id ?>"/>
                                                        <input type="hidden" disabled="disabled" name="zamowienie_towar[<?= $tKey ?>][jednostka_str]" value="<?= $towar->towar_cena[0]->jednostka->jednostka ?>"/>
                                                        <input type="hidden" disabled="disabled" name="zamowienie_towar[<?= $tKey ?>][vat_id]" value="<?= $towar->towar_cena[0]->vat->id ?>"/>
                                                        <input type="hidden" disabled="disabled" name="zamowienie_towar[<?= $tKey ?>][vat_stawka]" value="<?= $towar->towar_cena[0]->vat->stawka ?>"/>
                                                        <input type="hidden" disabled="disabled" name="zamowienie_towar[<?= $tKey ?>][objetosc_kartonu]" value="<?= $towar->objetosc_kartonu ?>"/>
                                                        <input type="hidden" disabled="disabled" name="zamowienie_towar[<?= $tKey ?>][waga_kartonu]" value="<?= $towar->waga_kartonu ?>"/>
                                                    </td>
                                                    <td class="w-250 text-left"><?= $towar->nazwa ?></td>
                                                    <td style="white-space: nowrap;"><?= $towar->kod ?></td>
                                                    <td class="text-center"><?= $towar->ilosc_w_kartonie ?></td>
                                                    <td class="text-center"><?= $towar->waga_kartonu ?></td>
                                                    <td class="text-center"><?= $towar->objetosc_kartonu ?></td>
                                                    <td class="text-center"><?= $cenaNetto ?></td>
                                                    <td class="text-center"><?= $towar->towar_cena[0]->vat->stawka ?></td>
                                                    <td class="text-center"><?= $cenaBrutto ?></td>
                                                    <td class="text-center"><input style="max-width: 80px; padding: 0 0 0 4px; text-align: center;" cena-netto="<?= $cenaNetto ?>" cena-brutto="<?= $cenaBrutto ?>" ilosc-w-kartonie="<?= $towar->ilosc_w_kartonie ?>" type="number" name="zamowienie_towar[<?= $tKey ?>][ilosc]" class="form-control input-ilosc"/></td>
                                                    <td class="cena_razem_netto text-right">0</td>
                                                    <td class="cena_razem_brutto text-right">0</td>
                                                </tr>
                                                <?php
                                                $tKey++;
                                            }
                                        }
                                        ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="10" class="text-right"><?= $this->Txt->printAdmin(__('Admin | Razem')) ?></td>
                                            <td class="zamowienie_wartosc_razem_netto text-right">0</td>
                                            <td class="zamowienie_wartosc_razem text-right">0</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <h3><?= $this->Txt->printAdmin(__('Admin | Dane zamówienia')) ?></h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <h4><?= $this->Txt->printAdmin(__('Admin | Adres wysyłki')) ?></h4>
                                <div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <?= $this->Form->control('wysylka_imie', ['label' => $this->Txt->printAdmin(__('Admin | Imię')), 'value' => (!empty($klient->uzytkownik_wysylka) ? $klient->uzytkownik_wysylka->imie : '')]) ?>
                                        </div>
                                        <div class="col-lg-6">
                                            <?= $this->Form->control('wysylka_nazwisko', ['label' => $this->Txt->printAdmin(__('Admin | Nazwisko')), 'value' => (!empty($klient->uzytkownik_wysylka) ? $klient->uzytkownik_wysylka->nazwisko : '')]) ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <?= $this->Form->control('wysylka_firma', ['label' => $this->Txt->printAdmin(__('Admin | Firma')), 'value' => (!empty($klient->uzytkownik_wysylka) ? $klient->uzytkownik_wysylka->firma : '')]) ?>
                                        </div>
                                        <div class="col-lg-6">
                                            <?= $this->Form->control('wysylka_nip', ['label' => $this->Txt->printAdmin(__('Admin | Nip')), 'value' => (!empty($klient->uzytkownik_wysylka) ? $klient->uzytkownik_wysylka->nip : '')]) ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="phoneContainer form-group">
                                                <label><?= $this->Txt->printAdmin(__('Admin | Telefon')) ?></label>
                                                <div class="cuntryPhone">
                                                    <span class="fa fa-caret-down cuntryPhoneHolder"><?= (!empty($klient->uzytkownik_wysylka) ? $klient->uzytkownik_wysylka->kierunkowy : Cake\Core\Configure::read('adres.default_phone')) ?></span>
                                                    <?= $this->Form->control('kierunkowy', ['type' => 'select', 'options' => $cuntryPhones, 'default' => (!empty($klient->uzytkownik_wysylka) ? $klient->uzytkownik_wysylka->kierunkowy : Cake\Core\Configure::read('adres.default_phone')), 'label' => false]) ?>
                                                </div>
                                                <?= $this->Form->control('wysylka_telefon', ['type' => 'text','data-type'=>'phone', 'label' => false, 'value' => (!empty($klient->uzytkownik_wysylka) ? $klient->uzytkownik_wysylka->telefon : '')]) ?>
                                            </div>

                                            <?php // $this->Form->control('wysylka_telefon', ['label' => $this->Txt->printAdmin(__('Admin | Telefon')), 'value' => (!empty($klient->uzytkownik_wysylka) ? $klient->uzytkownik_wysylka->telefon : '')]) ?>
                                        </div>
                                        <div class="col-lg-6">
                                            <?= $this->Form->control('wysylka_email', ['label' => $this->Txt->printAdmin(__('Admin | Email')), 'value' => (!empty($klient->uzytkownik_wysylka) ? $klient->uzytkownik_wysylka->email : '')]) ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <?= $this->Form->control('wysylka_ulica', ['label' => $this->Txt->printAdmin(__('Admin | Ulica')), 'value' => (!empty($klient->uzytkownik_wysylka) ? $klient->uzytkownik_wysylka->ulica : '')]) ?>
                                        </div>
                                        <div class="col-lg-3">
                                            <?= $this->Form->control('wysylka_nr_domu', ['label' => $this->Txt->printAdmin(__('Admin | Nr domu')), 'value' => (!empty($klient->uzytkownik_wysylka) ? $klient->uzytkownik_wysylka->nr_domu : '')]) ?>
                                        </div>
                                        <div class="col-lg-3">
                                            <?= $this->Form->control('wysylka_nr_lokalu', ['label' => $this->Txt->printAdmin(__('Admin | Nr lokalu')), 'value' => (!empty($klient->uzytkownik_wysylka) ? $klient->uzytkownik_wysylka->nr_lokalu : '')]) ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <?= $this->Form->control('wysylka_kod', ['label' => $this->Txt->printAdmin(__('Admin | Kod pocztowy')), 'value' => (!empty($klient->uzytkownik_wysylka) ? $klient->uzytkownik_wysylka->kod : '')]) ?>
                                        </div>
                                        <div class="col-lg-6">
                                            <?= $this->Form->control('wysylka_miasto', ['label' => $this->Txt->printAdmin(__('Admin | Miasto')), 'value' => (!empty($klient->uzytkownik_wysylka) ? $klient->uzytkownik_wysylka->miasto : '')]) ?>
                                        </div>
                                        <div class="col-lg-3">
                                            <?= $this->Form->control('wysylka_kraj', ['label' => $this->Txt->printAdmin(__('Admin | Kraj')), 'options' => Cake\Core\Configure::read('kraje'), 'default' => Cake\Core\Configure::read('adres.default_country'), 'value' => (!empty($klient->uzytkownik_wysylka) ? $klient->uzytkownik_wysylka->kraj : '')]) ?>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <h4><?= $this->Txt->printAdmin(__('Admin | Dane do faktury')) ?></h4>
                                <div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <?= $this->Form->control('faktura_imie', ['label' => $this->Txt->printAdmin(__('Admin | Imię')), 'value' => (!empty($klient->uzytkownik_faktura) ? $klient->uzytkownik_faktura->imie : '')]) ?>
                                        </div>
                                        <div class="col-lg-6">
                                            <?= $this->Form->control('faktura_nazwisko', ['label' => $this->Txt->printAdmin(__('Admin | Nazwisko')), 'value' => (!empty($klient->uzytkownik_faktura) ? $klient->uzytkownik_faktura->nazwisko : '')]) ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <?= $this->Form->control('faktura_firma', ['label' => $this->Txt->printAdmin(__('Admin | Firma')), 'value' => (!empty($klient->uzytkownik_faktura) ? $klient->uzytkownik_faktura->firma : '')]) ?>
                                        </div>
                                        <div class="col-lg-6">
                                            <?= $this->Form->control('faktura_nip', ['label' => $this->Txt->printAdmin(__('Admin | Nip')), 'value' => (!empty($klient->uzytkownik_faktura) ? $klient->uzytkownik_faktura->nip : '')]) ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <?= $this->Form->control('faktura_ulica', ['label' => $this->Txt->printAdmin(__('Admin | Ulica')), 'value' => (!empty($klient->uzytkownik_faktura) ? $klient->uzytkownik_faktura->ulica : '')]) ?>
                                        </div>
                                        <div class="col-lg-3">
                                            <?= $this->Form->control('faktura_nr_domu', ['label' => $this->Txt->printAdmin(__('Admin | Nr domu')), 'value' => (!empty($klient->uzytkownik_faktura) ? $klient->uzytkownik_faktura->nr_domu : '')]) ?>
                                        </div>
                                        <div class="col-lg-3">
                                            <?= $this->Form->control('faktura_nr_lokalu', ['label' => $this->Txt->printAdmin(__('Admin | Nr lokalu')), 'value' => (!empty($klient->uzytkownik_faktura) ? $klient->uzytkownik_faktura->nr_lokalu : '')]) ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <?= $this->Form->control('faktura_kod', ['label' => $this->Txt->printAdmin(__('Admin | Kod pocztowy')), 'value' => (!empty($klient->uzytkownik_faktura) ? $klient->uzytkownik_faktura->kod : '')]) ?>
                                        </div>
                                        <div class="col-lg-6">
                                            <?= $this->Form->control('faktura_miasto', ['label' => $this->Txt->printAdmin(__('Admin | Miasto')), 'value' => (!empty($klient->uzytkownik_faktura) ? $klient->uzytkownik_faktura->miasto : '')]) ?>
                                        </div>
                                        <div class="col-lg-3">
                                            <?= $this->Form->control('faktura_kraj', ['label' => $this->Txt->printAdmin(__('Admin | Kraj')), 'options' => Cake\Core\Configure::read('kraje'), 'default' => Cake\Core\Configure::read('adres.default_country'), 'value' => (!empty($klient->uzytkownik_faktura) ? $klient->uzytkownik_faktura->kraj : '')]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">    
                                <?= $this->Form->control('uwagi', ['type' => 'textarea', 'label' => $this->Txt->printAdmin(__('Admin | Uwagi widoczne przez klienta'))]) ?>
                            </div>
                            <div class="col-md-6">    
                                <?= $this->Form->control('notatki', ['type' => 'textarea', 'label' => $this->Txt->printAdmin(__('Admin | Notatki wewnętrzne'))]) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <h4><?= $this->Txt->printAdmin(__('Admin | Łączna wartość zamówienia netto')) ?>: <span class="zamowienie_wartosc_razem_netto">0</span> <?= $klient->walutum->symbol ?></h4>
                                <h4><?= $this->Txt->printAdmin(__('Admin | Łączna wartość zamówienia brutto')) ?>: <span class="zamowienie_wartosc_razem">0</span> <?= $klient->walutum->symbol ?></h4>
                            </div>
                        </div>
                        <?php
                    } else {
                        ?>
                        <h3><?= $this->Txt->printAdmin(__('Admin | Wybierz klienta dla którego realizujesz zamówienie')) ?></h3>
                        <table class="table table-striped table-hover data-table">
                            <thead>
                                <tr>
                                    <th><?= $this->Txt->printAdmin(__('Admin | Klient')) ?></th>
                                    <th><?= $this->Txt->printAdmin(__('Admin | Firma')) ?></th>
                                    <th><?= $this->Txt->printAdmin(__('Admin | Nip')) ?></th>
                                    <th><?= $this->Txt->printAdmin(__('Admin | Email')) ?></th>
                                    <th><?= $this->Txt->printAdmin(__('Admin | Telefon')) ?></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($klienci as $itemKlient) {
                                    ?>
                                    <tr>
                                        <td><?= $itemKlient->imie ?> <?= $itemKlient->nazwisko ?></td>
                                        <td><?= $itemKlient->firma ?></td>
                                        <td><?= $itemKlient->nip ?></td>
                                        <td><?= $itemKlient->email ?></td>
                                        <td><?= $itemKlient->telefon ?></td>
                                        <td><a href="<?= \Cake\Routing\Router::url(['action' => 'add', $itemKlient->id]) ?>" class="btn btn-xs btn-primary"><?= $this->Txt->printAdmin(__('Admin | Wybierz')) ?></a></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    <?php }
                    ?>

                </fieldset>
            </div>
            <?php if (!empty($klient)) { ?>
                <div class="x_footer text-right">
                    <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz zamówienie')), ['class' => 'btn btn-sm btn-success']) ?>
                </div>
            <?php } ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
