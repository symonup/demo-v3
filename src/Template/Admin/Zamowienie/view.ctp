<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Szczegóły zamówienia numer: {0}', [(!empty($zamowienie->numer) ? $zamowienie->numer : $zamowienie->id)])) ?></h2>
                <ul class="nav navbar-right panel_toolbox">

                    <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> ' . __('Admin | Wróć do {0}', [__('Admin | listy zamówień')])), ['action' => 'index'], ['escape' => false, 'class' => 'btn btn-xs btn-warning']) ?></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="orderDetalisBox">
                                    <h4><?= $this->Txt->printAdmin(__('Admin | Dane zamówienia')) ?></h4>
                                    <ul>
                                        <li><span><?= $this->Txt->printAdmin(__('Admin | Numer zamówienia:')) ?></span> <span><?= (!empty($zamowienie->numer) ? $zamowienie->numer : $zamowienie->id) ?></span></li>
                                        <li><span><?= $this->Txt->printAdmin(__('Admin | Data złożenia:')) ?></span> <span><?= (!empty($zamowienie->data) ? $zamowienie->data->format('Y-m-d H:i') : '') ?></span></li>
                                        <li><span><?= $this->Txt->printAdmin(__('Admin | Waluta:')) ?></span> <span><?= $zamowienie->waluta_symbol ?></span></li>
                                        <li><span><?= $this->Txt->printAdmin(__('Admin | Wartość zamówienia:')) ?></span> <span><?= $this->Txt->cena($zamowienie->wartosc_razem, $zamowienie->waluta_symbol) ?></span></li>
                                    </ul>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="orderDetalisBox">
                                    <h4><?= $this->Txt->printAdmin(__('Admin | Dane klienta')) ?></h4>
                                    <ul>
                                        <li><span><?= $this->Txt->printAdmin(__('Admin | Imie i Nazwisko:')) ?></span> <span><?= $zamowienie->imie ?> <?= $zamowienie->nazwisko ?></span></li><li><span><?= $this->Txt->printAdmin(__('Admin | Email:')) ?></span> <span><?= $zamowienie->email ?></span></li>
                                        <li><span><?= $this->Txt->printAdmin(__('Admin | Firma:')) ?></span> <span><?= $zamowienie->firma ?></span></li>
                                        <li><span><?= $this->Txt->printAdmin(__('Admin | Nip:')) ?></span> <span><?= $zamowienie->nip ?></span></li>
                                        <li><span><?= $this->Txt->printAdmin(__('Admin | Telefon:')) ?></span> <span><?= $zamowienie->telefon ?></span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <?php
                        if (!empty($zamowienie->uwagi)) {
                            ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="orderDetalisBox">
                                        <h4><?= $this->Txt->printAdmin(__('Admin | Uwagi do zamówienia')) ?></h4>
                                        <div><?= nl2br(strip_tags($zamowienie->uwagi)) ?></div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="orderDetalisBox">
                                    <h4><?= $this->Txt->printAdmin(__('Admin | Dane wysyłki/odbioru')) ?></h4>
                                    <?php 
                                    if(!empty($zamowienie->paczkomat)){
                                        ?>
                                    <div>
                                        <b>Paczkomat: <?=$zamowienie->paczkomat?></b><br/>
                                         <?= $zamowienie->paczkomat_info?><br/>
                                         <hr/>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                    <div><?= $this->Txt->printUserInfo($zamowienie->toArray(), '<br/>', 'wysylka_') ?></div>
                                    <div><?= $this->Txt->printAddres($zamowienie->toArray(), '<br/>', 'wysylka_') ?></div>
                                    <?php if(!empty($zamowienie->punkty_odbioru_id)){
                                        echo $this->Html->tag('div', '<b>'.$this->Txt->printAdmin(__('Admin | Odbiór w punkcie:')).'</b><br/>'.nl2br($zamowienie->punkty_odbioru_info));
                                    }?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="orderDetalisBox">
                                    <h4><?= $this->Txt->printAdmin(__('Admin | Dane do faktury')) ?></h4>
                                    <div><?= $this->Txt->printUserInfo($zamowienie->toArray(), '<br/>', 'faktura_') ?></div>
                                    <div><?= $this->Txt->printAddres($zamowienie->toArray(), '<br/>', 'faktura_') ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="orderDetalisBox">
                            <form id="orderNotatkiForm" class="ajaxForm" action="<?= Cake\Routing\Router::url(['action' => 'update', $zamowienie->id]) ?>">
                                <h4><?= $this->Txt->printAdmin(__('Admin | Notatki wewnętrzne')) ?></h4>
                                <?= $this->Form->control('notatki', ['type' => 'textarea', 'value' => $zamowienie->notatki, 'label' => false]) ?>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-floppy-o reload-unactive"></i><i class="fa fa-spinner reload-active"></i> <?= $this->Txt->printAdmin(__('Admin | Zapisz')) ?></button>
                                </div>
                            </form>
                        </div>
                        <div class="orderDetalisBox">
                            <form id="orderStatusForm" class="ajaxForm" action="<?= Cake\Routing\Router::url(['action' => 'update', $zamowienie->id, 'status']) ?>">
                                <h4><?= $this->Txt->printAdmin(__('Admin | Status zamówienia')) ?></h4>
                                <?= $this->Form->control('status', ['type' => 'select', 'value' => $zamowienie->status, 'label' => false, 'options' => $statusy]) ?>
                                <div class="inputTerminWysylki" <?= (($zamowienie->status != 'przyjete') ? 'style="display:none;"' : '') ?>>
                                    <?= $this->Form->control('termin_wysylki', ['type' => 'text','autocomplete'=>'off', 'value' => (!empty($zamowienie->termin_wysylki) ? $zamowienie->termin_wysylki->format('Y-m-d') : ''), 'label' => $this->Txt->printAdmin(__('Admin | Przewidywany termin wysyłki')), 'datepicker' => 'date']) ?>
                                </div>
                                <div class="inputListPrzewozowy" <?= (($zamowienie->status != 'wyslane') ? 'style="display:none;"' : '') ?>>
                                    <?= $this->Form->control('kurier_typ', ['type' => 'select','disabled'=>($zamowienie->status != 'wyslane'),'options'=>['dpd'=>'DPD','dhl'=>'DHL','ups'=>'UPS','inpost'=>'InPost','poczta'=>'Poczta Polska'],'empty'=>$this->Txt->printAdmin(__('Admin | Wybierz kuriera')), 'value' => $zamowienie->kurier_typ, 'label' => $this->Txt->printAdmin(__('Admin | Typ kuriera'))]) ?>
                                    <?= $this->Form->control('nr_listu_przewozowego', ['type' => 'text','disabled'=>($zamowienie->status != 'wyslane'),'autocomplete'=>'off', 'value' => $zamowienie->nr_listu_przewozowego, 'label' => $this->Txt->printAdmin(__('Admin | Nr listu przewozowego'))]) ?>
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-floppy-o reload-unactive"></i><i class="fa fa-spinner reload-active"></i> <?= $this->Txt->printAdmin(__('Admin | Zapisz')) ?></button>
                                </div>
                            </form>
                            <?php if(!empty($zamowienie->kurier_typ) && !empty($zamowienie->nr_listu_przewozowego)){
                                echo $this->Html->tag('h4',$this->Txt->printAdmin(__('Admin | Link do śledzenia przesyłki'))).$this->Txt->kurierLink($zamowienie->kurier_typ,$zamowienie->nr_listu_przewozowego);
                            } ?>
                        </div>
                        <?php /* if (!empty($zamowienie->uzytkownik_id)) { ?>
                            <div class="orderDetalisBox">
                                <form id="orderBonusConfirm" class="ajaxForm" action="<?= Cake\Routing\Router::url(['action' => 'update', $zamowienie->id, 'bonus']) ?>">
                                    <h4><?= $this->Txt->printAdmin(__('Admin | Bonusowe złotówki')) ?></h4>
                                    <div class="row">
                                        <div class="col-12 col-md-4">
                                            <h5><?= $this->Txt->printAdmin(__('Admin | Bonus z zamówienia')) ?></h5>
                                            <div><?= $this->Txt->cena($zamowienie->bonus_suma, $zamowienie->waluta_symbol, true) ?></div>
                                        </div>
                                        <div class="col-12 col-md-4">
                                            <h5><?= $this->Txt->printAdmin(__('Admin | Bonus przyznany')) ?></h5>
                                            <div id="bonus_przyznany"><?= $this->Txt->cena($zamowienie->bonus_przyznany, $zamowienie->waluta_symbol, true) ?></div>
                                        </div>
                                        <div class="col-12 col-md-4">
                                            <h5><?= $this->Txt->printAdmin(__('Admin | Przyznaj bonus')) ?></h5>
                                            <div><input type="number" class="form-control" name="bonus_przyznany"/></div>
                                        </div>
                                    </div>
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-floppy-o reload-unactive"></i><i class="fa fa-spinner reload-active"></i> <?= $this->Txt->printAdmin(__('Admin | Zapisz')) ?></button>
                                    </div>
                                </form>
                            </div>
                        <?php } */ ?>
                    </div>
                </div>
                <?php /* <div class="row">
                  <div class="col-lg-12 text-right">
                  <a href="<?= Cake\Routing\Router::url(['action'=>'getPdf',$zamowienie->id,1])?>" target="_blank" class="btn btn-warning btn-md"><i class="fa fa-file-pdf-o"></i> <?=$this->Txt->printAdmin(__('Admin | Pobierz zamówienie w pliku pdf'))?></a>
                  <a href="<?= Cake\Routing\Router::url(['action'=>'getExcel',$zamowienie->id])?>" target="_blank" class="btn btn-info btn-md"><i class="fa fa-file-excel-o"></i> <?=$this->Txt->printAdmin(__('Admin | Pobierz zamówienie w pliku excel'))?></a>
                  </div>
                  </div> */ ?>
                <div class="row">
                    <div class="col-lg-12">
                        <h4><?= $this->Txt->printAdmin(__('Admin | Produkty w zamówieniu')) ?></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th><?= $this->Txt->printAdmin(__('Admin | Produkt')) ?></th>
                              <?php /*      <th><?= $this->Txt->printAdmin(__('Admin | Platforma')) ?></th>
                                    <th><?= $this->Txt->printAdmin(__('Admin | Data premiery')) ?></th>
                                    <th><?= $this->Txt->printAdmin(__('Admin | Preorder')) ?></th>
                                    <th><?= $this->Txt->printAdmin(__('Admin | Hot Deal')) ?></th> */ ?>
                                    <th><?= $this->Txt->printAdmin(__('Admin | Kod')) ?></th>
                                    <th><?= $this->Txt->printAdmin(__('Admin | Cena brutto')) ?></th>
                                    <th><?= $this->Txt->printAdmin(__('Admin | Rabat')) ?></th>
                                    <th><?= $this->Txt->printAdmin(__('Admin | Cena z rabatem')) ?></th>
                                    <th><?= $this->Txt->printAdmin(__('Admin | Ilość')) ?></th>
                                    <th class="text-right"><?= $this->Txt->printAdmin(__('Admin | Wartość brutto')) ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sumWaga = 0;
                                $sumObjetosc = 0;
                                foreach ($zamowienie->zamowienie_towar as $item) {
                                    $itemPrice=$item->cena_za_sztuke_brutto;
                                    if(!empty($item->rabat)){
                                        $itemPrice = round($itemPrice-($itemPrice*($item->rabat/100)),2);
                                    }
                                    ?>
                                    <tr>
                                        <td><?= (!empty($item->towar_id) ? $this->Html->link($item->nazwa, ['controller' => 'Towar', 'action' => 'view', $item->towar_id, $this->Navi->friendlyUrl($item->nazwa), 'prefix' => false], ['target' => '_blank']) : $item->nazwa) ?></td>
                            <?php     /*       <td><?= $item->platforma ?></td>
                                        <td><?= (!empty($item->data_premiery) ? $item->data_premiery->format('Y-m-d') : '') ?></td>
                                        <td><?= $this->Txt->printBool($item->preorder) ?></td>
                                        <td><?= $this->Txt->printBool(!empty($item->hot_deal)) ?></td> */ ?>
                                        <td><?= $item->kod ?></td>
                                        <td><?= $this->Txt->cena($item->cena_za_sztuke_brutto, $zamowienie->waluta_symbol) ?></td>
                                        <td><?= (!empty($item->rabat) ? $item->rabat : 0) . '%' ?></td>
                                        <td><?= $this->Txt->cena($itemPrice, $zamowienie->waluta_symbol) ?></td>
                                        <td><?= $item->ilosc ?></td>
                                        <td class="text-right"><?= $this->Txt->cena(($itemPrice * $item->ilosc), $zamowienie->waluta_symbol) ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <?php
                                if (!empty($zamowienie->kod_rabatowy) && !empty($zamowienie->kod_rabatowy_wartosc)) {
                                    ?>
                                    <tr>
                                        <th colspan="6" class="text-right"><?= $this->Txt->printAdmin(__('Admin | Kod rabatowy {0}', [$zamowienie->kod_rabatowy])) ?></th>
                                        <th class="text-right"><?= (($zamowienie->kod_rabatowy_typ == 'kwota') ? $this->Txt->cena($zamowienie->kod_rabatowy_wartosc, $zamowienie->waluta_symbol) : $zamowienie->kod_rabatowy_wartosc . '%') ?></th>
                                    </tr>
                                    <?php
                                }
                                ?>
                                <tr>
                                    <th colspan="6" class="text-right"><?= $this->Txt->printAdmin(__('Admin | Łączna wartość produktów')) ?></th>
                                    <?php /* <th class="text-right" style="white-space: nowrap;"><?=$this->Txt->cena($zamowienie->wartosc_produktow_netto,$zamowienie->waluta_symbol)?></th> */ ?>
                                    <th class="text-right" style="white-space: nowrap;"><?= $this->Txt->cena($zamowienie->wartosc_produktow, $zamowienie->waluta_symbol) ?></th>
                                </tr>
                                <tr>
                                    <th colspan="6" class="text-right"><?= $zamowienie->platnosc_wysylka ?></th>
                                    <th class="text-right" style="white-space: nowrap;"><?= $this->Txt->cena($zamowienie->wysylka_koszt, $zamowienie->waluta_symbol) ?></th>
                                </tr>
                                <?php
                                if (!empty($zamowienie->bonus_uzyty)) {
                                    ?>
                                    <tr>
                                        <th colspan="6" class="text-right"><?= $this->Txt->printAdmin(__('Admin | Użyte bonusowe złotówki')) ?></th>
                                        <th class="text-right">- <?= $this->Txt->cena($zamowienie->bonus_uzyty, $zamowienie->waluta_symbol, true) ?></th>
                                    </tr>
                                    <?php
                                }
                                ?>
                                <tr>
                                    <th colspan="6" class="text-right"><?= $this->Txt->printAdmin(__('Admin | Łączna wartość zamówienia')) ?></th>
                                    <?php /* <th class="text-right" style="white-space: nowrap;"><?=$this->Txt->cena($zamowienie->wartosc_razem_netto,$zamowienie->waluta_symbol)?></th> */ ?>
                                    <th class="text-right" style="white-space: nowrap;"><?= $this->Txt->cena($zamowienie->wartosc_razem, $zamowienie->waluta_symbol) ?></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <?php
                if (!empty($zamowienie->karta_podarunkowa_kod)) {
                    ?>

                    <div class="row">
                        <div class="col-lg-12">
                            <h4><?= $this->Txt->printAdmin(__('Admin | Karty podarunkowe wygenerowane do zamówienia')) ?></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th><?= $this->Txt->printAdmin(__('Admin | Nazwa')) ?></th>
                                        <th><?= $this->Txt->printAdmin(__('Admin | Data dodania')) ?></th>
                                        <th><?= $this->Txt->printAdmin(__('Admin | Wartość')) ?></th>
                                        <th><?= $this->Txt->printAdmin(__('Admin | Aktywna')) ?></th>
                                        <th><?= $this->Txt->printAdmin(__('Admin | Kod')) ?></th>
                                        <th><?= $this->Txt->printAdmin(__('Admin | Wysłana')) ?></th>
                                        <th><?= $this->Txt->printAdmin(__('Admin | Data wysłania')) ?></th>
                                        <th><?= $this->Txt->printAdmin(__('Admin | Użyta')) ?></th>
                                        <th><?= $this->Txt->printAdmin(__('Admin | Data użycia')) ?></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $wyslane = false;
                                    foreach ($zamowienie->karta_podarunkowa_kod as $karta) {
                                        if (!empty($karta->wyslany)) {
                                            $wyslane = true;
                                        }
                                        ?>
                                        <tr>
                                            <td><?= $karta->karta_podarunkowa->nazwa ?></td>
                                            <td><?= $karta->data_dodania->format('Y-m-d H:i') ?></td>
                                            <td><?= $this->Txt->cena($karta->wartosc) ?></td>
                                            <td><?= $this->Txt->printBool($karta->aktywny, str_replace($basePath, '/', Cake\Routing\Router::url(['controller' => 'KartaPodarunkowa', 'action' => 'setFieldKod', $karta->id, 'aktywny', (!empty($karta->aktywny) ? 0 : 1)])), [$this->Txt->printAdmin('Admin | Aktywuj'), $this->Txt->printAdmin('Admin | Tak')]) ?></td>
                                            <td><?= $karta->kod ?></td>
                                            <td><?= $this->Txt->printBool($karta->wyslany) ?></td>
                                            <td><?= (!empty($karta->data_wyslania) ? $karta->data_wyslania->format('Y-m-d H:i') : '') ?></td>
                                            <td><?= $this->Txt->printBool($karta->uzyty) ?></td>
                                            <td><?= (!empty($karta->data_uzycia) ? $karta->data_uzycia->format('Y-m-d H:i') : '') ?></td>
                                            <td>    <a class="btn btn-info btn-xs" href="<?= \Cake\Routing\Router::url(['action' => 'wyslijKarte', $karta->id, 1, 1]) ?>" data-toggle="tooltip" title="<?= $this->Txt->printAdmin(__('Admin | Pobierz PDF')) ?>"><i class="fa fa-download"></i></a></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php
                            if ($wyslane) {
                                ?>
                                <a class="btn btn-warning confirm-link" confirm-message="<?= $this->Txt->printAdmin(__('Admin | Czy na pewno chcesz wysłać ponownie wszystkie aktwyne karty?')) ?>" href="<?= \Cake\Routing\Router::url(['action' => 'wyslijKarty', $zamowienie->id, 0, 1]) ?>"><?= $this->Txt->printAdmin(__('Admin | Wyślij ponownie wszystkie aktywne karty')) ?> <i class="fa fa-send"></i></a>
                                <?php
                            } else {
                                ?>
                                <a class="btn btn-primary" href="<?= \Cake\Routing\Router::url(['action' => 'wyslijKarty', $zamowienie->id]) ?>"><?= $this->Txt->printAdmin(__('Admin | Wyślij wszystkie aktywne karty')) ?> <i class="fa fa-send"></i></a>
                                    <?php
                                }
                                ?>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div class="row">
                    
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <h4>Wysyłka zamówienia</h4>
    <?php
        echo $this->element('admin/kurierzy');
    ?>
</div>
                </div>
            </div>
        </div>
    </div>
</div>
