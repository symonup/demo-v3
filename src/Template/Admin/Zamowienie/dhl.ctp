<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Zlecenia DHL')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">

                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-xs-12">
    <?php 
    $bookTypes=[
        'exPickup'=>'Paczki, Listy (EX)',
        'drPickup'=>'Palety (DR)'
    ];
    if(!empty($dhlServices)) { ?>
    <h4>Dostępność kuriera dla kodu pocztowego <?= substr($dhlServicesPostCode, 0,2).'-'.substr($dhlServicesPostCode, 2,3)?> w dniu <?=$dhlServicesDate?></h4>
    <div class="row">
        <div class="col-lg-4">
            <h5>Dostępność usług:</h5>
            <?php 
            $bookHours=[];
            if(is_object($dhlServices) && property_exists($dhlServices, 'getPostalCodeServicesResult')){
                $dhlServices->getPostalCodeServicesResult->domesticExpress9;
                $dhlServices->getPostalCodeServicesResult->domesticExpress12;
                $dhlServices->getPostalCodeServicesResult->deliveryEvening;
                $dhlServices->getPostalCodeServicesResult->pickupOnSaturday;
                $dhlServices->getPostalCodeServicesResult->deliverySaturday;
                ?>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Usługa</th>
                        <th>Dostępna</th>
                    </tr>
                </thead>
                <tbody>
            <?php
                foreach ($dhlServices->getPostalCodeServicesResult as $field => $value){
                    if(!is_bool($value)){
                        if(strpos($field, 'From')!==false){
                        $bookHours[str_replace('From', '', $field)]['From']=$value;
                        }elseif(strpos($field, 'To')!==false){
                        $bookHours[str_replace('To', '', $field)]['To']=$value;
                        }else{
                            $bookHours[$field]=$value;
                        }
                        continue;
                    }
                    ?>
                    <tr>
                        <td><?=$field?></td>
                        <td><b><?=($value?__('Tak'):__('Nie'))?></b></td>
                    </tr>
            <?php
                }
                ?>
            </tbody>
            </table>
        </div>
        <div class="col-lg-8">
            <h5>Godziny odbioru przesyłek</h5>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Rodzaj</th>
                        <th>Od</th>
                        <th>Do</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                                    foreach ($bookHours as $bookType => $bookValues){
                                        if(is_array($bookValues)){
                                            ?>
                    <tr>
                        <td><?=((!empty($bookTypes) && key_exists($bookType,$bookTypes))?$bookTypes[$bookType]:$bookType)?></td>
                        <td><?=$bookValues['From']?></td>
                        <td><?=$bookValues['To']?></td>
                    </tr>
                    <?php
                                        }else{
                                               ?>
                    <tr>
                        <td><?=((!empty($bookTypes) && key_exists($bookType,$bookTypes))?$bookTypes[$bookType]:$bookType)?></td>
                        <td colspan="2"><?=$bookValues?></td>
                    </tr>
                    <?php
                                        }
                                    }
                    ?>
                </tbody>
            </table>
            <?php
            }elseif(is_string ($dhlServices)){
                ?>
            <div class="alert alert-danger"><?=$dhlServices?></div>
            <?php
            }
    ?>
        </div>
    </div>
    <?php } ?>
    
</div>
<div class="actions col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <h4>Filtracja</h4>
    <?php
    echo $this->Form->create('filter', ['class' => 'form-inline filter_towar','url'=>  Cake\Routing\Router::url($this->request->pass)]);
    echo $this->Form->input('status', ['type' => 'select', 'label' => __('Status'), 'options' => $allStatus, 'empty' => __('wszystkie'), 'class' => 'form-control', 'value' => (!empty($filter['status']) ? $filter['status'] : '')]);
    echo $this->Form->input('zamowienie_id', ['type' => 'text', 'label' => __('Nr. zamówienia'),'style'=>'max-width:110px;', 'class' => 'form-control', 'value' => (!empty($filter['zamowienie_id']) ? $filter['zamowienie_id'] : '')]);
    echo $this->Form->input('id', ['type' => 'text', 'label' => __('Nr. przesyłki'),'style'=>'max-width:150px;', 'class' => 'form-control', 'value' => (!empty($filter['id']) ? $filter['id'] : '')]);
    echo $this->Form->input('order_id', ['type' => 'text', 'label' => __('Nr. zlecenia'),'style'=>'max-width:150px;', 'class' => 'form-control', 'value' => (!empty($filter['order_id']) ? $filter['order_id'] : '')]);
    echo $this->Form->input('data', ['type' => 'text','autocomplete'=>'off', 'label' => __('Data nadania'),'style'=>'max-width:110px;', 'class' => 'form-control datepicker', 'value' => (!empty($filter['data']) ? $filter['data'] : '')]);
    echo $this->Html->tag('div', $this->Html->tag('button', '<span class="glyphicon glyphicon-search" aria-hidden="true"></span> ' . __('szukaj'), ['escape' => false, 'type' => 'submit', 'name' => 'filtruj', 'class' => 'btn btn-md btn-success']), ['class' => 'form-group']);
    echo $this->Html->tag('div', $this->Html->tag('button', '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> ' . __('wyczysc filtr'), ['escape' => false, 'type' => 'submit', 'name' => 'wyczysc_filtr', 'class' => 'btn btn-md btn-warning']), ['class' => 'form-group']);
    echo $this->Form->end();
    ?>
    <hr/>
</div>

<div class="zamowienie index col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <?php
    $th = [];
    $td = [];
    $th[]=$this->Form->input('select_all_checkbox',['id'=>'select_all_checkbox','type'=>'checkbox','label'=>false,'templates'=>['inputContainer' => '<div class="form-group checkbox" style="display:inline-block;">{{content}}</div>']]);
    $th[] = $this->Paginator->sort('zamowienie_id', __('Nr zamówienia'));
    $th[] =  $this->Paginator->sort('shipmentDate', __('Data nadania'));
    $th[] =  $this->Paginator->sort('shipmentId', __('Numer przesyłki'));
    $th[] =  $this->Paginator->sort('orderId', __('Numer zlecenia'));
    $th[] =  $this->Paginator->sort('type', __('Rodzaj'));
    $th[] =  $this->Paginator->sort('type', __('Zawartość'));
    $th[] =  $this->Paginator->sort('type', __('Uwagi'));
    $th[] = __('Actions');
    foreach ($zlecenia as $zlecenie) {
        $action=[];
        $tds = [];
        $tds[]=(empty($zlecenie->orderId)?$this->Form->input('shipmentIdList[]',['type'=>'checkbox','data-nadania'=>(!empty( $zlecenie->shipmentDate)?$zlecenie->shipmentDate->format('Y-m-d'):''),'label'=>false,'value'=>$zlecenie->shipmentId,'class'=>'order-checkbox']):'');
        $tds[] = (!empty($zlecenie->zamowienie_id)?$this->Html->link(($zlecenie->has('zamowienie')?(!empty($zlecenie->zamowienie->numer)?$zlecenie->zamowienie->numer:$zlecenie->zamowienie->id):$zlecenie->zamowienie_id),['controller'=>'Zamowienie','action'=>'view',$zlecenie->zamowienie_id,'k_tab'=>'dhlWebApi'],['target'=>'_blank']):(!empty($zlecenie->allegro_zamowienie_id)?$this->Html->link('Allegro: '.$zlecenie->allegro_zamowienie_id,['controller'=>'Allegro','action'=>'szczegoly',$zlecenie->allegro_zamowienie_id,'k_tab'=>'dhlWebApi'],['target'=>'_blank']):''));
        $tds[] = (!empty( $zlecenie->shipmentDate)?$zlecenie->shipmentDate->format('Y-m-d'):'');
        $tds[] = $zlecenie->shipmentId;
        $tds[] = $zlecenie->orderId;
        $tds[] = ((!empty($zlecenie->type) && key_exists($zlecenie->type, $courierTypes))?$courierTypes[$zlecenie->type]:$zlecenie->type);
        $tds[] = nl2br($zlecenie->opis);
        $tds[] = nl2br($zlecenie->uwagi);
        $act=[];
        $act[]=$this->Html->link('<i class="fa fa-print"></i>',['controller'=>'Zamowienie','action'=>'dhlGetLabel',$zlecenie->shipmentId],['escape'=>false,'target'=>'_blank', 'class'=>'btn btn-xs btn-default','title' => __('Pobierz list przewozowy'),'data-toggle'=>'tooltip', 'data-placement'=>'top']);
        if(!empty($zlecenie->orderId)){
        $act[]=$this->Html->link('<i class="fa fa-ban"></i>',['controller'=>'Zamowienie','action'=>'dhlCancelBook',$zlecenie->orderId],['escape'=>false,'confirm-message'=>'Czy na pewno chcesz odwołać kuriera? Anulowane zostaną wszystkie przesyłki z tego zlecenia!','class'=>'confirm-link btn btn-danger btn-xs', 'title' => __('Odwołaj kuriera'),'data-toggle'=>'tooltip', 'data-placement'=>'top']);
        }else{
        $act[]=$this->Html->link('<i class="fa fa-edit"></i>',['controller'=>(!empty($zlecenie->zamowienie_id)?'Zamowienie':'Allegro'),'action'=>(!empty($zlecenie->zamowienie_id)?'view':'szczegoly'),(!empty($zlecenie->zamowienie_id)?$zlecenie->zamowienie_id:$zlecenie->allegro_zamowienie_id),'k_tab'=>'dhlWebApi','dhl_edit'=>$zlecenie->shipmentId],['escape'=>false,'class'=>'btn btn-xs btn-default', 'title' => __('Edytuj przesyłkę'),'data-toggle'=>'tooltip', 'data-placement'=>'top']);
       $act[]=$this->Html->link('<i class="fa fa-trash"></i>',['controller'=>'Zamowienie','action'=>'dhlDeleteShipment',$zlecenie->shipmentId],['escape'=>false,'confirm-message'=>'Czy na pewno chcesz usunąć przesyłkę?','class'=>'confirm-link btn btn-danger btn-xs', 'title' => __('Usuń przesyłkę'),'data-toggle'=>'tooltip', 'data-placement'=>'top']);
        
        }
       
        $tds[] = join(' ',$act);
             

        $td[] = $tds;
    }
    echo $this->Table->create($td, $th, null, false);
    ?>
    <hr/>
    <h4>Parametry zlecenia dla kuriera:</h4>
    <div class="row">
        <div class="col-xs-12 col-md-3">
            <?=$this->Form->input('pickupDate',['label'=>__('Data nadania'),'autocomplete'=>'off','type'=>'text','value'=>(!empty($filter['data']) ? $filter['data'] : date('Y-m-d')),'datepicker'=>'date','book_info'=>'true'])?>
        </div>
        <div class="col-xs-12 col-md-2">
            <?=$this->Form->input('pickupTimeFrom',['label'=>__('Godzina OD'),'autocomplete'=>'off','type'=>'text','value'=>'08:00','datepicker'=>'time','book_info'=>'true'])?>
        </div>
        <div class="col-xs-12 col-md-2">
            <?=$this->Form->input('pickupTimeTo',['label'=>__('Godzina DO'),'autocomplete'=>'off','type'=>'text','value'=>'16:00','datepicker'=>'time','book_info'=>'true'])?>
        </div>
        <div class="col-xs-12 col-md-5">
            <?=$this->Form->input('additionalInfo',['label'=>__('Uwagi dla kuriera'),'type'=>'textarea','class'=>'book_info','book_info'=>'true'])?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 text-right"><?=$this->Html->link('<span class="glyphicon glyphicon-time" aria-hidden="true"></span> ' . __('Zamów kuriera'), 'javascript:void(0);', ['escape' => false, 'class' => 'btn btn-sm btn-primary','id'=>'dhl_book','data-action'=> \Cake\Routing\Router::url(['action'=>'dhl_book'])])?></div>
    </div>
    <?php
    echo $this->element('admin/paginator',['goTo'=>true]);
    ?>

</div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->Html->script(array('/layout-admin/build/js/dhl.js')) ?>