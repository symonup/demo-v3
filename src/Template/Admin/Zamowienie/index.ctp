<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Zamowienie[]|\Cake\Collection\CollectionInterface $zamowienie
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Lista zamówień')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <?php /* <li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-plus']) . ' ' . $this->Txt->printAdmin(__('Admin | Dodaj {0}', [$this->Txt->printAdmin(__('Admin | zamówienie'))])), ['action' => 'add'], ['escape' => false, 'class' => 'btn btn-xs btn-primary']) ?></li> */ ?>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div>
                    <div class="row">
                        <div class="col-lg-12"><h4><?= $this->Txt->printAdmin(__('Admin | Filtracja')) ?></h4></div>
                    </div>
                    <div class="row">
                        <?= $this->Form->create(null, ['type' => 'post']) ?>
                        <div class="col-lg-3 col-md-4">
                            <?= $this->Form->control('status', ['type' => 'select', 'options' => $statusy, 'empty' => $this->Txt->printAdmin(__('Admin | Wszystkie')), 'value' => ((!empty($filter) && !empty($filter['status'])) ? $filter['status'] : '')]) ?>
                        </div>
                        <div class="col-lg-3 col-md-4">
                            <?= $this->Form->control('wysylka_id', ['type' => 'select', 'options' => $wysylki, 'empty' => $this->Txt->printAdmin(__('Admin | Wszystkie')), 'value' => ((!empty($filter) && !empty($filter['wysylka_id'])) ? $filter['wysylka_id'] : '')]) ?>
                        </div>
                        <?php /*   <div class="col-lg-3 col-md-4">
                          <?=$this->Form->control('bonusy',['type'=>'select','options'=>$filterBonusy,'empty'=>$this->Txt->printAdmin(__('Admin | Wszystkie')),'value'=>((!empty($filter)&&!empty($filter['bonusy']))?$filter['bonusy']:'')])?>
                          </div> */ ?>
                        <?php /*  <div class="col-lg-2 col-md-4">
                          <?=$this->Form->control('preorder',['type'=>'radio','options'=>[0=>$this->Txt->printAdmin(__('Admin | Wszystkie')),1=>$this->Txt->printAdmin(__('Admin | Preorder')),2=>$this->Txt->printAdmin(__('Admin | Bez preorder'))],'label'=>false,'value'=>((!empty($filter)&&!empty($filter['preorder']))?$filter['preorder']:0)])?>
                          </div> */ ?>
                        <div class="col-md-2 col-lg-1">
                            <div class="form-group">
                                <label style="display: block;">&nbsp;</label>
                                <button type="submit" class="btn btn-xs btn-success" name="filter"><i class="fa fa-search"></i></button>
                                <?php
                                if (!empty($filter)) {
                                    ?>
                                    <button type="submit" class="btn btn-xs btn-warning" name="clear_filter" data-toggle="tooltip" title="<?= $this->Txt->printAdmin(__('Admin | Wyczyść filtr')) ?>"><i class="fa fa-times"></i></button>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12"><hr/></div>
                </div>
                <div class="row">
                    <div class="col-xs-12">

                        <?= $this->Form->create(null, ['type' => 'file', 'id' => 'order-fv-form', 'url' => ['controller' => 'Zamowienie', 'action' => 'importSubiektFv']]) ?>
                        <?= $this->Txt->file($this->Form->control('file', ['type' => 'file', 'file-type' => 'other', 'auto-submit' => 'true', 'templateVars' => ['btnAttr' => 'data-toggle="popover" data-trigger="hover | click" data-html="true" data-content="Aby wygenerować plik z falturami w subiekcie przejdź do listy faktur sprzedazy, zaznacz faktury które chcesz wyeksportować i kliknij w opcje Wyślij do pliku. Zapisz dane w formacie EDI++ i wgraj zapisany plik do sklepu."'], 'label' => false]), $this->Txt->printAdmin(__('Admin | Wgraj faktury z subiekta')), '') ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
                <?= $this->Form->create(null, ['id' => 'order-list-form', 'url' => ['controller' => 'Zamowienie', 'action' => 'orderListAction']]) ?>
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <div>
                            <button type="submit" name="pdf" class="btn btn-primary"><?= $this->Txt->printAdmin(__('Admin | Pobierz pdf')) ?></button>
                            <button type="submit" name="inpost" class="btn btn-info"><?= $this->Txt->printAdmin(__('Admin | Pobierz plik paczkomat inpost')) ?></button>
                            <button type="submit" name="geis" class="btn btn-info"><?= $this->Txt->printAdmin(__('Admin | Pobierz plik geis')) ?></button>
                        </div>
                        <button type="submit" name="subiekt" class="btn btn-warning" data-toggle="popover" data-trigger="hover | click" data-html="true" data-content="Pobierz plik CSV do programu importującego wiele dokumentów do Subiekta GT. Dokumentacja programu: <a href='https://www.artoit.pl/moduly/gt/import_dokumentu.html' target='_blank'>https://www.artoit.pl/moduly/gt/import_dokumentu.html</a>"><?= $this->Txt->printAdmin(__('Admin | CSV SGTImporter')) ?></button>
                        <button type="submit" name="subiekt_epp" value="ZK" class="btn btn-success" data-toggle="tooltip" title="<?= $this->Txt->printAdmin(__('Admin | Pobierz plik epp dla subiekta jako zamówienie klienta')) ?>"><?= $this->Txt->printAdmin(__('Admin | Subiekt epp ZK')) ?></button>
                        <button type="submit" name="subiekt_epp" value="FS" class="btn btn-danger" data-toggle="tooltip" title="<?= $this->Txt->printAdmin(__('Admin | Pobierz plik epp dla subiekta jako faktura sprzedaży')) ?>"><?= $this->Txt->printAdmin(__('Admin | Subiekt epp FS')) ?></button>
                    </div>
                    <?php /*   <div class="col-12 col-md-4 text-center">
                      <button type="submit" style="margin-bottom: 0;" name="bonus" class="btn btn-warning"><?=$this->Txt->printAdmin(__('Admin | Przyznaj bonusowe złotówki'))?></button>
                      </div> */ ?>
                    <div class="col-xs-12 col-md-4 text-right">
                        <div class="form-inline">
                            <?= $this->Form->control('status', ['type' => 'select', 'options' => $statusy, 'label' => false, 'value' => '', 'empty' => $this->Txt->printAdmin(__('Admin | Ustaw status'))]) ?>
                            <button style="margin: 0;" type="submit" name="set_status" class="btn btn-success"><?= $this->Txt->printAdmin(__('Admin | Zapisz')) ?></button>
                        </div>

                        <label><input type="checkbox" name="send_status_mails" CHECKED value="1"/> <?= $this->Txt->printAdmin(__('Admin | Wyślij wiadomości o zmianie statusu')) ?></label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12"><hr/></div>
                </div>
                <table class="table table-striped data-table" def-sort="2" def-sort-direction="desc">
                    <thead>
                        <tr>
                            <th><?= $this->Form->control('check_all', ['type' => 'checkbox', 'name' => false, 'check-all' => 'list-check', 'label' => false]) ?></th>
                            <th data-priority="2"><?= $this->Txt->printAdmin(__('Admin | Nr zamówienia')) ?></th>
                            <th data-priority="3"><?= $this->Txt->printAdmin(__('Admin | Data')) ?></th>
                            <th data-priority="7"><?= $this->Txt->printAdmin(__('Admin | Produkty')) ?></th>
                            <th data-priority="9"><?= $this->Txt->printAdmin(__('Admin | Wysyłka')) ?></th>
                            <th data-priority="12"><?= $this->Txt->printAdmin(__('Admin | Wartość')) ?></th>
                            <th data-priority="13"><?= $this->Txt->printAdmin(__('Admin | Status')) ?></th>
                            <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($zamowienie as $zamowienie): ?>
                            <?php
                            $items = [];
                            if (!empty($zamowienie->zamowienie_towar)) {
                                foreach ($zamowienie->zamowienie_towar as $item) {
                                    $items[] = $item->nazwa . ' ' . $item->platforma . ' ' . $item->ilosc . 'szt. ' . $this->Txt->cena($item->cena_razem_brutto, $zamowienie->waluta_symbol, true) . (!empty($item->preorder) ? ' <span style="color:#ff0000;">PREORDER</span>' : '');
                                }
                            }
                            $bonus = '';
                            if (!empty($zamowienie->uzytkownik_id) && !empty($zamowienie->bonus_suma)) {
                                $bTooltip = (!empty($zamowienie->bonus_przyznany) ? $this->Txt->printAdmin(__('Admin | Przyznano {0} bonusu', [$this->Txt->cena($zamowienie->bonus_przyznany, $zamowienie->waluta_symbol, true)])) : $this->Txt->printAdmin(__('Admin | {0} bonusu do przyznania', [$this->Txt->cena($zamowienie->bonus_suma, $zamowienie->waluta_symbol, true)])));
                                $bonus = '<br/><span data-toggle="tooltip" title="' . $bTooltip . '" class="bonus-info-list ' . (!empty($zamowienie->bonus_przyznany) ? 'przyznany' : '') . '"><i class="fa fa-lightbulb-o"></i></span>';
                            }
                            ?>
                            <tr>
                                <td><input type="checkbox" name="ids[]" value="<?= $zamowienie->id ?>" check-item="list-check"></td>
                                <td><?= (!empty($zamowienie->numer) ? $zamowienie->numer : $zamowienie->id) ?><?= $bonus ?>
                                    <?php
                                    if (!empty($zamowienie->faktura_subiekt)) {
                                        $faktury = [];
                                        foreach ($zamowienie->faktura_subiekt as $faktura) {
                                            $faktury[] = $this->Html->link($faktura->numer_dokumentu, ['controller' => 'Home', 'action' => 'getFv', $faktura->token, 'prefix' => false], ['target' => '_blank']);
                                        }
                                        echo $this->Html->tag('div', join('<br>', $faktury));
                                    }
                                    ?>
                                </td>
                                <td><?= (!empty($zamowienie->data) ? $zamowienie->data->format('Y-m-d H:i') : '') ?></td>
                                <td><?= join('<br/>', $items) ?></td>
                                <td><?= (!empty($zamowienie->wysylka) ? $zamowienie->wysylka->nazwa : '') ?></td>
                                <td><?= $this->Txt->cena($zamowienie->wartosc_razem, $zamowienie->waluta_symbol) ?></td>
                                <td><?= $statusy[$zamowienie->status] ?></td>
                                <td class="actions">
    <?= $this->Html->link('<i class="fa fa-eye"></i>', ['action' => 'view', $zamowienie->id], ['escape' => false, 'class' => 'btn btn-default btn-xs', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $this->Txt->printAdmin(__('Admin | Zobacz szczegóły'))]) ?>
                            <?= $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $zamowienie->id], ['confirm-btn-yes' => $this->Txt->printAdmin(__('Admin | Tak')), 'confirm-btn-no' => $this->Txt->printAdmin(__('Admin | Nie')), 'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $zamowienie->id)), 'escape' => false, 'class' => 'btn btn-danger btn-xs confirm-link', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $this->Txt->printAdmin(__('Admin | Usuń'))]) ?>
                                </td>
                            </tr>
                <?php endforeach; ?>
                    </tbody>
                </table>
<?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>