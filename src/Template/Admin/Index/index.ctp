<!-- top tiles -->
<div class="row tile_count">
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-user"></i> <?= $this->Txt->printAdmin(__('Admin | Liczba zarejsetrowanych użytkowników')) ?></span>
        <div class="count"><?= $usersCount ?></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-user"></i> <?= $this->Txt->printAdmin(__('Admin | Liczba zarejsetrowanych użytkowników w odstatnim tygodniu')) ?></span>
        <div class="count"><?= $lastTimeUsersCount ?></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-user"></i> <?= $this->Txt->printAdmin(__('Admin | Oczekują na aktywacje')) ?></span>
        <div class="count"><?= $unactiveUsersCount ?></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-shopping-cart"></i> <?= $this->Txt->printAdmin(__('Admin | Liczba wszystkich zamówień')) ?></span>
        <div class="count"><?= $allOrdersCount ?></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-shopping-cart"></i> <?= $this->Txt->printAdmin(__('Admin | Liczba zamówień z ostatniego tygodnia')) ?></span>
        <div class="count"><?= $lastOrdersCount ?></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-shopping-cart"></i> <?= $this->Txt->printAdmin(__('Admin | Zam. oczekujące')) ?></span>
        <div class="count"><?= $oczekujaceOrdersCount ?></div>
    </div>
</div>
<!-- /top tiles -->

<div class="row">


    <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="x_panel tile fixed_height_320">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Zamówienia oczekujące')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content scroll-content">
                <table class="table table-striped table-hover" style="width:100%">
                    <thead>
                        <tr>
                            <th><p><?= $this->Txt->printAdmin(__('Admin | Nr')) ?></p></th>
                            <th><p><?= $this->Txt->printAdmin(__('Admin | Data')) ?></p></th>
                            <th><p><?= $this->Txt->printAdmin(__('Admin | Wartość')) ?></p></th>
                            <th><p><?= $this->Txt->printAdmin(__('Admin | Status')) ?></p></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($oczekujaceOrders as $order) {
                            ?>
                            <tr>
                                <td><?= $order->id ?></td>
                                <td style="white-space: nowrap;"><?= $order->data->format('Y-m-d') ?><br/><?= $order->data->format('H:i:s') ?></td>
                                <td><?= $this->Txt->cena($order->wartosc_razem, $order->waluta_symbol) ?></td>
                                <td><?= $statusy[$order->status] ?></td>
                                <td><a data-toggle="tooltip" data-placement="top" title="<?= $this->Txt->printAdmin(__('Admin | Przejdź do zamówienia')) ?>" href="<?= Cake\Routing\Router::url(['controller' => 'Zamowienie', 'action' => 'view', $order->id]) ?>"><i class="fa fa-eye"></i></a></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="x_panel tile fixed_height_320 overflow_hidden">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Ostatnie zamówienia')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content scroll-content">
                <table class="table table-striped table-hover" style="width:100%">
                    <thead>
                        <tr>
                            <th><p><?= $this->Txt->printAdmin(__('Admin | Nr')) ?></p></th>
                            <th><p><?= $this->Txt->printAdmin(__('Admin | Data')) ?></p></th>
                            <th><p><?= $this->Txt->printAdmin(__('Admin | Wartość')) ?></p></th>
                            <th><p><?= $this->Txt->printAdmin(__('Admin | Status')) ?></p></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($lastOrders as $order) {
                            ?>
                            <tr>
                                <td><?= $order->id ?></td>
                                <td style="white-space: nowrap;"><?= $order->data->format('Y-m-d') ?><br/><?= $order->data->format('H:i:s') ?></td>
                                <td><?= $this->Txt->cena($order->wartosc_razem, $order->waluta_symbol) ?></td>
                                <td><?= $statusy[$order->status] ?></td>
                                <td><a data-toggle="tooltip" data-placement="top" title="<?= $this->Txt->printAdmin(__('Admin | Przejdź do zamówienia')) ?>" href="<?= Cake\Routing\Router::url(['controller' => 'Zamowienie', 'action' => 'view', $order->id]) ?>"><i class="fa fa-eye"></i></a></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="x_panel tile fixed_height_320">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Szybki dostęp')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="dashboard-widget-content">
                    <ul class="quick-list">
                  <?php /*      <li><i class="fa fa-bell"></i><a href="<?= Cake\Routing\Router::url(['controller' => 'Powiadomienia', 'action' => 'index', 'tab' => 'dostepnosc']) ?>"><?= $this->Txt->printAdmin(__('Admin | Zapytania o dostępność')) ?> <span class="label label-warning"><?= $zapytaniaODostepnoscCount ?></span></a></li> */ ?>
                  <?php /*       <li><i class="fa fa-bell"></i><a href="<?= Cake\Routing\Router::url(['controller' => 'Powiadomienia', 'action' => 'index', 'tab' => 'premiery']) ?>"><?= $this->Txt->printAdmin(__('Admin | Zapytania o premiery')) ?> <span class="label label-warning"><?= $zapytaniaOPremieryCount ?></span></a></li> */ ?>
                        <li><i class="fa fa-bell"></i><a href="<?= Cake\Routing\Router::url(['controller' => 'Powiadomienia', 'action' => 'index', 'tab' => 'zamowienie']) ?>"><?= $this->Txt->printAdmin(__('Admin | Zapytania o produkty')) ?> <span class="label label-warning"><?= $naZamowienieCount ?></span></a></li>
                        <li><i class="fa fa-cogs"></i><a href="<?= Cake\Routing\Router::url(['controller' => 'Konfiguracja', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Admin | Konfiguracja')) ?></a></li>
                        <li><i class="fa fa-shopping-cart"></i><a href="<?= Cake\Routing\Router::url(['controller' => 'Zamowienie', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Admin | Zamówienia')) ?></a></li>
                        <li><i class="fa fa-list"></i><a href="<?= Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Admin | Produkty')) ?></a></li>
                        <?php /* <li><i class="fa fa-pie-chart"></i><a href="<?= Cake\Routing\Router::url(['controller'=>'Raport','action'=>'index'])?>"><?=$this->Txt->printAdmin(__('Admin | Raporty'))?></a></li>
                         */ ?>  <li><i class="fa fa-sign-out"></i><a href="<?= Cake\Routing\Router::url(['controller' => 'Index', 'action' => 'logout']) ?>"><?= $this->Txt->printAdmin(__('Admin | Wyloguj')) ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</div>
<br/>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="dashboard_graph">

            <div class="row x_title">
                <div class="col-md-6">
                    <h3><?= $this->Txt->printAdmin(__('Admin | Statystyki: ilość zamówień')) ?></h3>
                </div>
                <div class="col-md-6">
                    <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                        <span><?= $dateFrom ?> - <?= $dateTo ?></span> <b class="caret"></b>
                    </div>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div id="wykres_zamowien" class="demo-placeholder"></div>
            </div>
            <form id="indexStatForm" style="display: none;" method="post">
                <input type="hidden" id="statFrom" name="from" value="<?= (!empty($dateFrom) ? $dateFrom : date('Y-m-d', strtotime('- 7 days'))) ?>"/>
                <input type="hidden" id="statTo" name="to" value="<?= (!empty($dateTo) ? $dateTo : date('Y-m-d')) ?>"/>
            </form>
            <div class="clearfix"></div>
        </div>
    </div>

</div>
<script type="text/javascript">
    var statystykiArray = <?= $statystykiArrayToJs ?>;
    var statystykiMax = <?= $maxWykresCount + 5 ?>;
</script>