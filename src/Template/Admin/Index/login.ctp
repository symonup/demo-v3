<div>
      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <?=$this->Form->create()?>
              <h1><?=$this->Txt->printAdmin(__('Admin | Logowanie do panelu administratora'))?></h1>
              <div>
                <input type="text" class="form-control" name="login" placeholder="<?=$this->Txt->printAdmin(__('Admin | Login'))?>" required="" />
              </div>
              <div>
                <input type="password" class="form-control" name="password" placeholder="<?=$this->Txt->printAdmin(__('Admin | Password'))?>" required="" />
              </div>
              <div>
                    <button class="btn btn-default submit" type="submit"><?=$this->Txt->printAdmin(__('Admin | Log in'))?></button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">

                <div class="clearfix"></div>
                <br />

                <div>
                    <h1><i class="fa fa-laptop"></i> <?= Cake\Core\Configure::read('admin.nazwa_serwisu')?>!</h1>
                  <p>©<?=date('Y')?></p>
                </div>
              </div>
            <?=$this->Form->end()?>
          </section>
        </div>
      </div>
    </div>