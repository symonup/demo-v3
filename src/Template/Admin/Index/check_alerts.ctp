<?php
$maxAlerts = 6;
$showAlerts = false;
if (!empty($warnings)) {
    $showAlerts = true;
    foreach ($warnings as $alertKey => $alert) {
        if ($maxAlerts <= 0) {
            break;
        }
        if ($alertKey == 'dostepnosc') {
         ?>
                <li><a href="<?= \Cake\Routing\Router::url(['controller' => 'Powiadomienia', 'action' => 'index', 'tab'=>'dostepnosc']) ?>"><span>
                            <span><?= $this->Txt->printAdmin(__('Admin | Nowe zapytania o dostepność')) ?></span>
                        </span>
                        <span class="message">
                            <?= $this->Txt->printAdmin(__('Admin | Ilość zapytań {0}', [$alert])) ?> 
                        </span></a></li>
                <?php
        $maxAlerts--;
        }
        if ($alertKey == 'premiery') {
         ?>
                <li><a href="<?= \Cake\Routing\Router::url(['controller' => 'Powiadomienia', 'action' => 'index', 'tab'=>'premiery']) ?>"><span>
                            <span><?= $this->Txt->printAdmin(__('Admin | Nowe zapytania o premierę')) ?></span>
                        </span>
                        <span class="message">
                            <?= $this->Txt->printAdmin(__('Admin | Ilość zapytań {0}', [$alert])) ?> 
                        </span></a></li>
                <?php
        $maxAlerts--;
        }
        if ($alertKey == 'zapytania') {
         ?>
                <li><a href="<?= \Cake\Routing\Router::url(['controller' => 'Powiadomienia', 'action' => 'index', 'tab'=>'zamowienie']) ?>"><span>
                            <span><?= $this->Txt->printAdmin(__('Admin | Nowe zapytania o produkt')) ?></span>
                        </span>
                        <span class="message">
                            <?= $this->Txt->printAdmin(__('Admin | Ilość zapytań {0}', [$alert])) ?> 
                        </span></a></li>
                <?php
        $maxAlerts--;
        }
        if ($alertKey == 'call_back') {
         ?>
                <li><a href="<?= \Cake\Routing\Router::url(['controller' => 'Powiadomienia', 'action' => 'index','tab'=>'callback']) ?>"><span>
                            <span><?= $this->Txt->printAdmin(__('Admin | Oddzwoń!!!')) ?></span>
                        </span>
                        <span class="message">
                            <?= $this->Txt->printAdmin(__('Admin | Ilość zgłoszeń {0}', [$alert])) ?> 
                        </span></a></li>
                <?php
        $maxAlerts--;
        }
        if ($alertKey == 'zamowienia_oczekujace') {
        if ($maxAlerts <= 0) {
            break;
        }
            foreach ($alert as $itemId => $data) {
        if ($maxAlerts <= 0) {
            break;
        }
                ?>
                <li><a href="<?= \Cake\Routing\Router::url(['controller' => 'Zamowienie', 'action' => 'view', $itemId]) ?>"><span>
                            <span><?= $this->Txt->printAdmin(__('Admin | Zamówienie oczekuje na realizacje')) ?></span>
                        </span>
                        <span class="message">
                            <?= $this->Txt->printAdmin(__('Admin | Zamówienie nr {0} z dnia {1} oczekuje na realizacje', [$itemId,$data])) ?> 
                        </span></a>
                </li>
                <?php
        $maxAlerts--;
            }
        }
        if ($alertKey == 'zamowienia_niewyslane') {
        if ($maxAlerts <= 0) {
            break;
        }
            foreach ($alert as $itemId => $data) {
        if ($maxAlerts <= 0) {
            break;
        }
                ?>
                <li><a href="<?= \Cake\Routing\Router::url(['controller' => 'Zamowienie', 'action' => 'view', $itemId]) ?>"><span>
                            <span><?= $this->Txt->printAdmin(__('Admin | Zamówienie nie wysłane')) ?></span>
                        </span>
                        <span class="message">
                            <?= $this->Txt->printAdmin(__('Admin | Zamówienie nr {0} powinno być wysłane dnia {1}', [$itemId,$data])) ?> 
                        </span></a>
                </li>
                <?php
        $maxAlerts--;
            }
        }
        ?>
        <?php
    }
}
if ($showAlerts) {
    ?>

    <li>
        <div class="text-center">
            <a href="<?= \Cake\Routing\Router::url(['controller' => 'Powiadomienia', 'action' => 'index']) ?>">
                <strong><?= $this->Txt->printAdmin(__('Admin | Zobacz wszystkie wiadomości')) ?></strong>
                <i class="fa fa-angle-right"></i>
            </a>
        </div>
    </li>
<?php } else {
    ?>
    <li><span><?= $this->Txt->printAdmin(__('Admin | Brak wiadomości')) ?></span></li>
    <?php }
?>