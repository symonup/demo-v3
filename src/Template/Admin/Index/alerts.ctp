<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                    <h2><?=$this->Txt->printAdmin(__('Admin | Wiadomości i ostrzerzenia'))?></h2>
                    <div class="clearfix"></div>
                  </div>
            <div class="x_content">
                <?php 
                if(!empty($allAlerts)){
                    foreach ($allAlerts as $warningType => $warning){
                        switch ($warningType){
                            case 'uzytkownicy' : {
                                $messages=[];
                                    foreach ($warning as $itemId => $alert){
                                     $messages[]=$this->Html->link($this->Txt->printAdmin(__('Admin | {0} oczekuje na akceptacje',[$alert])),['controller' => 'Uzytkownik', 'action' => 'edit', $itemId],['class'=>'alert alert-warning alert-message']);   
                                    }
                                    if(!empty($messages)){
                                        echo join('', $messages);
                                    }
                            } break;
                            case 'zamowienia_oczekujace' : {
                                $messages=[];
                                    foreach ($warning as $itemId => $alert){
                                     $messages[]=$this->Html->link($this->Txt->printAdmin(__('Admin | Zamówienie nr {0} z dnia {1} oczekuje na realizacje', [$itemId,$alert])),['controller' => 'Zamowienie', 'view' => 'edit', $itemId],['class'=>'alert alert-warning alert-message']);   
                                    }
                                    if(!empty($messages)){
                                        echo join('', $messages);
                                    }
                            } break;
                            case 'zamowienia_niewyslane' : {
                                $messages=[];
                                    foreach ($warning as $itemId => $alert){
                                     $messages[]=$this->Html->link($this->Txt->printAdmin(__('Admin | Zamówienie nr {0} powinno być wysłane dnia {1}', [$itemId,$alert])),['controller' => 'Zamowienie', 'action' => 'view', $itemId],['class'=>'alert alert-warning alert-message']);   
                                    }
                                    if(!empty($messages)){
                                        echo join('', $messages);
                                    }
                            } break;
                        }
                    }
                }
                else{
                    ?>
                <div class="alert alert-success">
                    <table class="simple-table">
                        <tr>
                            <td><i class="fa fa-thumbs-o-up fa-2x"></i></td>
                            <td><p><?=$this->Txt->printAdmin('Admin | Brak wiadomości')?></p></td>
                        </tr>
                    </table>
                </div>
                <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>