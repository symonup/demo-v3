<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
                <?= $this->Form->create($jezyk,['type'=>'file']) ?>
            <div class="x_title">
                    <h2><?=$this->Txt->printAdmin(__('Admin | Add {0}',[__('Admin | Jezyk')]))?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                                              <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> '.__('Admin | Wróć do {0}',[__('Admin | Listy')])), ['action' => 'index'],['escape'=>false,'class'=>'btn btn-xs btn-warning']) ?></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
            <div class="x_content">
    <fieldset>
        <?php
            $lngInputs=[];
                        echo $this->Form->control('nazwa',['label'=>$this->Txt->printAdmin(__('Admin | Nazwa języka'))]);
            echo $this->Form->control('locale',['label'=>$this->Txt->printAdmin(__('Admin | Locale')),'options'=> \Cake\Core\Configure::read('Locale')]);
            echo $this->Form->control('symbol',['label'=>$this->Txt->printAdmin(__('Admin | Symbol języka'))]);
            echo $this->Form->control('waluta_id', ['options' => $waluta,'label'=>$this->Txt->printAdmin(__('Admin | Domyślna waluta'))]);
            echo $this->Form->control('domyslny',['type'=>'hidden','value'=>0]);
            echo $this->Form->control('aktywny',['label'=>$this->Txt->printAdmin(__('Admin | Aktywny')),'type'=>'checkbox']);
            echo $this->Txt->file($this->Form->control('flaga', ['type' => 'file','label'=>false]),$this->Txt->printAdmin(__('Admin | Flaga')),(!empty($jezyk->flaga)?$displayPath['lang'].$jezyk->flaga:''));
            
//            echo $this->Form->control('domena',['label'=>$this->Txt->printAdmin(__('Admin | domena'))]);
        ?>
    </fieldset>
            </div>
            <div class="x_footer text-right">
    <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')),['class'=>'btn btn-sm btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
