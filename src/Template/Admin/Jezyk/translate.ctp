<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <?= $this->Form->create($jezyk) ?>
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Edytujesz {0}', [__('Admin | słownik {0}', [$jezyk->nazwa])])) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> ' . __('Admin | Wróć do {0}', [__('Admin | Listy')])), ['action' => 'translations'], ['escape' => false, 'class' => 'btn btn-xs btn-warning']) ?></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <fieldset>
                    <div class="panel-group" id="translations-panel" role="tablist" aria-multiselectable="true">
                        <?php
                        $tabs = [];
                        $active = false;
                        foreach ($lang as $prefix => $_lang) {
                            ?>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="<?= "lang-head-$prefix" ?>">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#translations-panel" href="#<?= "lang-$prefix" ?>" aria-expanded="true" aria-controls="<?= "lang-$prefix" ?>">
                                            <?= $this->Txt->printAdmin(__("Admin | lang-$prefix")) ?>
                                        </a>
                                    </h4>
                                </div>
                                <div id="<?= "lang-$prefix" ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="<?= "lang-head-$prefix" ?>">
                                    <div class="panel-body">
                                        <?php
                                        foreach ($_lang as $key => $value) {
                                            $wysiwyg = (bool) $value['wysiwyg'];
                                            echo $this->Form->input("$prefix.$key", ['type' => 'textarea', 'label' => $this->Txt->langLabel(__('Lang | ' . $this->Translation->key($key))), 'value' => $value, 'class' => 'form-control', 'summernote' => (($wysiwyg == true) ? 'true' : 'false'), 'rows' => 1]);
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </fieldset>
            </div>
            <div class="x_footer text-right">
                <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')), ['class' => 'btn btn-sm btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>