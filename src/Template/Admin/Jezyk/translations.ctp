<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Jezyk[]|\Cake\Collection\CollectionInterface $jezyk
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Słowniki')) ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th data-priority="3"><?= $this->Txt->printAdmin(__('Admin | Język')) ?></th>
                            <th data-priority="4"><?= $this->Txt->printAdmin(__('Admin | Locale')) ?></th>
                            <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($jezyk as $jezyk): ?>
                            <tr>
                                <td><?= $jezyk->nazwa ?></td>
                                <td><?= $jezyk->locale ?></td>
                                <td class="actions">
                                    <?= $this->Html->link('<i class="fa fa-cogs"></i>', ['action' => 'translate', $jezyk->id], ['escape' => false, 'class' => 'btn btn-default btn-xs', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $this->Txt->printAdmin(__('Admin | Edytuj słownik'))]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>