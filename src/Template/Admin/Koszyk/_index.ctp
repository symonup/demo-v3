<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Koszyk'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Uzytkownik'), ['controller' => 'Uzytkownik', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Uzytkownik'), ['controller' => 'Uzytkownik', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Zamowienie'), ['controller' => 'Zamowienie', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Zamowienie'), ['controller' => 'Zamowienie', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Koszyk Towar'), ['controller' => 'KoszykTowar', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Koszyk Towar'), ['controller' => 'KoszykTowar', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="koszyk index large-9 medium-8 columns content">
    <h3><?= __('Koszyk') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('session_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('uzytkownik_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('data') ?></th>
                <th scope="col"><?= $this->Paginator->sort('last_update') ?></th>
                <th scope="col"><?= $this->Paginator->sort('zamowienie_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('aktywny') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($koszyk as $koszyk): ?>
            <tr>
                <td><?= $this->Number->format($koszyk->id) ?></td>
                <td><?= h($koszyk->session_id) ?></td>
                <td><?= $koszyk->has('uzytkownik') ? $this->Html->link($koszyk->uzytkownik->id, ['controller' => 'Uzytkownik', 'action' => 'view', $koszyk->uzytkownik->id]) : '' ?></td>
                <td><?= h($koszyk->data) ?></td>
                <td><?= h($koszyk->last_update) ?></td>
                <td><?= $koszyk->has('zamowienie') ? $this->Html->link($koszyk->zamowienie->id, ['controller' => 'Zamowienie', 'action' => 'view', $koszyk->zamowienie->id]) : '' ?></td>
                <td><?= h($koszyk->aktywny) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $koszyk->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $koszyk->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $koszyk->id], ['confirm' => __('Are you sure you want to delete # {0}?', $koszyk->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
