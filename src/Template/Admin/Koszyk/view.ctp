<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Koszyk'), ['action' => 'edit', $koszyk->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Koszyk'), ['action' => 'delete', $koszyk->id], ['confirm' => __('Are you sure you want to delete # {0}?', $koszyk->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Koszyk'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Koszyk'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Uzytkownik'), ['controller' => 'Uzytkownik', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Uzytkownik'), ['controller' => 'Uzytkownik', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Zamowienie'), ['controller' => 'Zamowienie', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Zamowienie'), ['controller' => 'Zamowienie', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Koszyk Towar'), ['controller' => 'KoszykTowar', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Koszyk Towar'), ['controller' => 'KoszykTowar', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="koszyk view large-9 medium-8 columns content">
    <h3><?= h($koszyk->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Session Id') ?></th>
            <td><?= h($koszyk->session_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Uzytkownik') ?></th>
            <td><?= $koszyk->has('uzytkownik') ? $this->Html->link($koszyk->uzytkownik->id, ['controller' => 'Uzytkownik', 'action' => 'view', $koszyk->uzytkownik->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Zamowienie') ?></th>
            <td><?= $koszyk->has('zamowienie') ? $this->Html->link($koszyk->zamowienie->id, ['controller' => 'Zamowienie', 'action' => 'view', $koszyk->zamowienie->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($koszyk->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Data') ?></th>
            <td><?= h($koszyk->data) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Last Update') ?></th>
            <td><?= h($koszyk->last_update) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Aktywny') ?></th>
            <td><?= $koszyk->aktywny ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Koszyk Towar') ?></h4>
        <?php if (!empty($koszyk->koszyk_towar)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Koszyk Id') ?></th>
                <th scope="col"><?= __('Towar Id') ?></th>
                <th scope="col"><?= __('Koszyk Key') ?></th>
                <th scope="col"><?= __('Dodany') ?></th>
                <th scope="col"><?= __('Usuniety') ?></th>
                <th scope="col"><?= __('Data Dodania') ?></th>
                <th scope="col"><?= __('Data Usuniecia') ?></th>
                <th scope="col"><?= __('Ilosc') ?></th>
                <th scope="col"><?= __('Cena') ?></th>
                <th scope="col"><?= __('Notatki') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($koszyk->koszyk_towar as $koszykTowar): ?>
            <tr>
                <td><?= h($koszykTowar->id) ?></td>
                <td><?= h($koszykTowar->koszyk_id) ?></td>
                <td><?= h($koszykTowar->towar_id) ?></td>
                <td><?= h($koszykTowar->koszyk_key) ?></td>
                <td><?= h($koszykTowar->dodany) ?></td>
                <td><?= h($koszykTowar->usuniety) ?></td>
                <td><?= h($koszykTowar->data_dodania) ?></td>
                <td><?= h($koszykTowar->data_usuniecia) ?></td>
                <td><?= h($koszykTowar->ilosc) ?></td>
                <td><?= h($koszykTowar->cena) ?></td>
                <td><?= h($koszykTowar->notatki) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'KoszykTowar', 'action' => 'view', $koszykTowar->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'KoszykTowar', 'action' => 'edit', $koszykTowar->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'KoszykTowar', 'action' => 'delete', $koszykTowar->id], ['confirm' => __('Are you sure you want to delete # {0}?', $koszykTowar->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
