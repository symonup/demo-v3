<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $koszyk->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $koszyk->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Koszyk'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Uzytkownik'), ['controller' => 'Uzytkownik', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Uzytkownik'), ['controller' => 'Uzytkownik', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Zamowienie'), ['controller' => 'Zamowienie', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Zamowienie'), ['controller' => 'Zamowienie', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Koszyk Towar'), ['controller' => 'KoszykTowar', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Koszyk Towar'), ['controller' => 'KoszykTowar', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="koszyk form large-9 medium-8 columns content">
    <?= $this->Form->create($koszyk) ?>
    <fieldset>
        <legend><?= __('Edit Koszyk') ?></legend>
        <?php
            echo $this->Form->input('session_id');
            echo $this->Form->input('uzytkownik_id', ['options' => $uzytkownik, 'empty' => true]);
            echo $this->Form->input('data');
            echo $this->Form->input('last_update', ['empty' => true]);
            echo $this->Form->input('zamowienie_id', ['options' => $zamowienie, 'empty' => true]);
            echo $this->Form->input('aktywny');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
