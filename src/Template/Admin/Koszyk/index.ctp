<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Koszyki')) ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div>
                    <?php
                    echo $this->Form->create('filter', ['class' => 'form-inline filter_towar', 'url' => Cake\Routing\Router::url($this->request->pass)]);
                    echo $this->Form->input('status', ['type' => 'select', 'label' => __('Status'), 'options' => ['zrealizowane' => __('Zrealizowane'), 'niezrealizowane' => __('Niezrealizowane'), 'towary_usuniete' => __('Z usuniętymi towarami'), 'bez_usunietych' => __('Bez usuniętych towarów')], 'empty' => __('wszystkie'), 'class' => 'form-control', 'value' => (!empty($filter['status']) ? $filter['status'] : '')]);
                    echo $this->Form->input('data_od', ['type' => 'text', 'label' => __('Data od'), 'data-type' => 'date', 'class' => 'form-control', 'value' => (!empty($filter['data_od']) ? $filter['data_od'] : '')]);
                    echo $this->Form->input('data_do', ['type' => 'text', 'label' => __('Data do'), 'data-type' => 'date', 'class' => 'form-control', 'value' => (!empty($filter['data_do']) ? $filter['data_do'] : '')]);
                    echo $this->Html->tag('div', $this->Html->tag('button', '<span class="glyphicon glyphicon-search" aria-hidden="true"></span> ' . __('szukaj'), ['escape' => false, 'type' => 'submit', 'name' => 'filtruj', 'class' => 'btn btn-md btn-success']), ['class' => 'form-group']);
                    echo $this->Html->tag('div', $this->Html->tag('button', '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> ' . __('wyczysc filtr'), ['escape' => false, 'type' => 'submit', 'name' => 'wyczysc_filtr', 'class' => 'btn btn-md btn-warning']), ['class' => 'form-group']);
                    echo $this->Form->end();
                    ?>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <button class="btn btn-primary btn-sm showAllDetalis">Rozwiń wszystkie szczegóły</button>
                        <button class="btn btn-default btn-sm hideAllDetalis">Zwiń wszystkie szczegóły</button>
                    </div>
                    <div class="col-md-4 text-right">
                        <a href="<?= \Cake\Routing\Router::url(['action' => 'raport']) ?>" target="_blank" class="btn btn-info btn-sm">Pobierz raport</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th><?= $this->Paginator->sort('id', __('Id')) ?></th>
                                    <th><?= $this->Paginator->sort('domena', __('Domena')) ?></th>
                                    <th><?= $this->Paginator->sort('data', __('Data stworzenia')) ?></th>
                                    <th><?= $this->Paginator->sort('last_update', __('Ostatnia aktualizacja')) ?></th>
                                    <th><?= __('W koszyku') ?></th>
                                    <th><?= __('Usunięte') ?></th>
                                    <th><?= $this->Paginator->sort('status', __('Status')) ?></th>
                                    <th><?= __('Zamówienie') ?></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($koszyk as $koszyk) {
                                    $usuniete = (!empty($koszyk->koszyk_towar_usuniete) ? count($koszyk->koszyk_towar_usuniete) : 0);
                                    $wKoszyku = (!empty($koszyk->koszyk_towar) ? count($koszyk->koszyk_towar) : 0) - $usuniete;
                                    ?>
                                    <tr>

                                        <td><?= $koszyk->id ?></td>
                                        <td><?= (empty($koszyk->domena) ? __('elektrohurt.net.pl') : $koszyk->domena) ?></td>
                                        <td><?= (!empty($koszyk->data) ? $koszyk->data->format('Y-m-d H:i:s') : '') ?></td>
                                        <td><?= (!empty($koszyk->last_update) ? $koszyk->last_update->format('Y-m-d H:i:s') : '') ?></td>
                                        <td><?= $wKoszyku ?></td>
                                        <td><?= $usuniete ?></td>
                                        <td><?= (!empty($koszyk->zamowienie_id) ? 'zrealizowany' : 'niezrealizowany') ?></td>
                                        <td><?= (!empty($koszyk->zamowienie_id) ? '<a href="' . \Cake\Routing\Router::url(['controller' => 'Zamowienie', 'action' => 'view', $koszyk->zamowienie_id]) . '" target="_blank">' . $koszyk->zamowienie_id . '</a>' : 'brak') ?></td>
                                        <td><button item="<?= $koszyk->id ?>" type="button" class="btn btn-default btn-sm show_detalis">Pokaż szczegóły</button></td>
                                    </tr>
                                    <tr id="item_<?= $koszyk->id ?>" class="koszyk-item-detalis">
                                        <td colspan="9">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Towar</th>
                                                        <th>Status</th>
                                                        <th>Data dodania</th>
                                                        <th>Data usunięcia</th>
                                                        <th>Ilość</th>
                                                        <th>Cena</th>
                                                        <th>Szczegóły</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if (!empty($koszyk->koszyk_towar)) {
                                                        foreach ($koszyk->koszyk_towar as $item) {
                                                            ?>
                                                            <tr>
                                                                <td><?= (!empty($item->towar) ? $item->towar->nazwa : $item->towar_id) ?></td>
                                                                <td><?= (!empty($item->usuniety) ? __('Usunięty') : __('W koszyku')) ?></td>
                                                                <td><?= (!empty($item->data_dodania) ? $item->data_dodania->format('Y-m-d H:i:s') : '-') ?></td>
                                                                <td><?= (!empty($item->data_usuniecia) ? $item->data_usuniecia->format('Y-m-d H:i:s') : '-') ?></td>
                                                                <td><?= (!empty($item->ilosc) ? $item->ilosc : '-') ?></td>
                                                                <td><?= (!empty($item->cena) ? $this->Number->currency($item->cena) : '-') ?></td>
                                                                <td><?= nl2br($item->notatki) ?></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                        <?php
                        echo $this->element('admin/paginator');
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
