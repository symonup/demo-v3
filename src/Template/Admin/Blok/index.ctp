<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Blok[]|\Cake\Collection\CollectionInterface $blok
  */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Blok')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Html->tag('i','',['class'=>'fa fa-plus']).' '.$this->Txt->printAdmin(__('Admin | Dodaj {0}',[$this->Txt->printAdmin(__('Admin | Blok'))])), ['action' => 'add'],['escape'=>false,'class'=>'btn btn-xs btn-primary']) ?></li>
                </ul>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped data-table">
                    <thead>
            <tr>
                                <th data-priority="2"><?= $this->Txt->printAdmin(__('Admin | Id')) ?></th>
                                <th data-priority="3"><?= $this->Txt->printAdmin(__('Admin | Nazwa bloku')) ?></th>
                                <th data-priority="4"><?= $this->Txt->printAdmin(__('Admin | Blok niewidoczny na stronie')) ?></th>
                <th scope="col"><?= $this->Paginator->sort('locale',$this->Txt->printAdmin(__('Admin | Język'))) ?></th>
                                <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($blok as $blok): ?>
            <tr>
                <td><?= $blok->id ?></td>
                <td><?= $blok->nazwa ?></td>
                <td><?= $this->Txt->printBool($blok->ukryty) ?></td>
                <td><?= (!empty($languages[$blok->locale])?$languages[$blok->locale]:$blok->locale) ?></td>
                <td class="actions">
                    <?= $this->Html->link('<i class="fa fa-cogs"></i>', ['action' => 'edit', $blok->id],['escape'=>false,'class'=>'btn btn-default btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Edytuj'))]) ?>
                    <?= $this->Html->link('<i class="fa fa-copy"></i>', ['action' => 'add', $blok->id],['escape'=>false,'class'=>'btn btn-default btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Kopiuj {0}',[$this->Txt->printAdmin(__('Admin | blok'))]))])?>
                    <?= $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $blok->id], ['confirm-btn-yes'=>$this->Txt->printAdmin(__('Admin | Tak')),'confirm-btn-no'=>$this->Txt->printAdmin(__('Admin | Nie')),'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $blok->id)),'escape'=>false,'class'=>'btn btn-danger btn-xs confirm-link','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Usuń'))]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
                </table>
                <?php
                if(!empty($tips)){
                    echo $this->element('admin/tips',['tips'=>$tips]);
                }
                ?>
            </div>
        </div>
    </div>
</div>