<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
                <?= $this->Form->create($blok) ?>
            <div class="x_title">
                    <h2><?=$this->Txt->printAdmin(__('Admin | Add {0}',[__('Admin | Blok')]))?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                                              <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> '.__('Admin | Wróć do {0}',[__('Admin | Listy')])), ['action' => 'index'],['escape'=>false,'class'=>'btn btn-xs btn-warning']) ?></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
            <div class="x_content">
    <fieldset>
        <?php
            $lngInputs=[];
                                echo $this->Form->control('locale', ['options'=>$languages,'label' => $this->Txt->printAdmin(__('Admin | Język'))]);
                        echo $this->Form->control('nazwa',['label'=>$this->Txt->printAdmin(__('Admin | Nazwa bloku'))]);
            echo $this->Form->control('ukryty',['label'=>$this->Txt->printAdmin(__('Admin | Blok niewidoczny na stronie'))]);
            echo $this->Form->control('tresc',['label'=>$this->Txt->printAdmin(__('Admin | Zawartość bloku')), 'summernote' => 'true']);
        ?>
    </fieldset>
            </div>
            <div class="x_footer text-right">
    <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')),['class'=>'btn btn-sm btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <h3><?= $this->Txt->printAdmin(__('Admin | Dozwolone zaczepy'))?></h3>
        <p>
            <?= $this->Txt->printAdmin(__('Admin | Podczas tworzenia lub edycji kodu HTML Twojego bloku możesz używać zaczepów, które wyświetlą np. formularz kontaktowy. Zaczepy mają określoną budowę i musi ona być zachowana. Jeżeli w zaczepie zostanie zmieniony choć jeden znak zaczep nie zostanie rozpoznany i nie wyświeli się prawidłowo.'))?>
        </p>
    </div>
    <div class="col-lg-6">
        <table class="table table-striped">
            <thead>
                <tr>
                    <td><?= $this->Txt->printAdmin(__('Admin | Zaczep')) ?></td>
                    <td><?= $this->Txt->printAdmin(__('Admin | Nazwa zaczepu')) ?></td>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($blocks as $zaczep => $name) {
                    ?>
                    <tr>
                        <td><?= $zaczep ?></td>
                        <td><?= $name ?></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="col-lg-6">
        <h4><?= $this->Txt->printAdmin(__('Admin | Przykład umieszczenia zaczepu w kodzie HTML bloku'))?></h4>
        <pre>
            &lt;div&gt;
                &lt;h2&gt;<?= $this->Txt->printAdmin(__('Admin | Formularz kontaktowy'))?>&lt;/h2&gt;
                &lt;p&gt;<?= $this->Txt->printAdmin(__('Admin | Napisz do nas'))?>&lt;/p&gt;
                &lt;div&gt;<b>[%-_kontakt-form_-%]</b>&lt;/div&gt;
            &lt;/div&gt;
        </pre>
    </div>
</div>
