<div class="row">
    <div class="col-lg-3 col-md-6">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Zamówienia wg. waluty')) ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?=$this->Form->create(null,['type'=>'post'])?>
                <?=$this->Form->control('waluta',['type'=>'select','options'=>$waluty,'label'=>$this->Txt->printAdmin(__('Admin | Waluta'))])?>
                <?=$this->Form->control('from',['type'=>'text','datepicker'=>'date','default'=>date('Y-m-d', strtotime('- 30 days')),'label'=>$this->Txt->printAdmin(__('Admin | Data początkowa'))])?>
                <?=$this->Form->control('to',['type'=>'text','datepicker'=>'date','default'=>date('Y-m-d'),'label'=>$this->Txt->printAdmin(__('Admin | Data końcowa'))])?>
                <?=$this->Form->control('statusy.wg_waluty',['options'=>$statusy,'id'=>'status_waluty','multiple'=>'checkbox','default'=>'zrealizowane','label'=>$this->Txt->printAdmin(__('Admin | Uwzględniaj statusy:'))])?>
                <div class="text-right">
                    <input type="hidden" name="typ" value="wg_waluty"/>
                    <button class="btn btn-info" type="submit"><i class="fa fa-download"></i> <?=$this->Txt->printAdmin(__('Admin | pobierz raport'))?></button>
                </div>        
                <?=$this->Form->end()?>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Zamawiane towary')) ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?=$this->Form->create(null,['type'=>'post'])?>
                <?=$this->Form->control('waluta',['type'=>'select','options'=>$waluty,'label'=>$this->Txt->printAdmin(__('Admin | Waluta'))])?>
                <?=$this->Form->control('from',['type'=>'text','datepicker'=>'date','default'=>date('Y-m-d', strtotime('- 30 days')),'label'=>$this->Txt->printAdmin(__('Admin | Data początkowa'))])?>
                <?=$this->Form->control('to',['type'=>'text','datepicker'=>'date','default'=>date('Y-m-d'),'label'=>$this->Txt->printAdmin(__('Admin | Data końcowa'))])?>
                <?=$this->Form->control('statusy.towary',['options'=>$statusy,'multiple'=>'checkbox','id'=>'status_towary','default'=>'zrealizowane','label'=>$this->Txt->printAdmin(__('Admin | Uwzględniaj statusy:'))])?>
                <div class="text-right">
                    <input type="hidden" name="typ" value="towary"/>
                    <button class="btn btn-info" type="submit"><i class="fa fa-download"></i> <?=$this->Txt->printAdmin(__('Admin | pobierz raport'))?></button>
                </div>        
                <?=$this->Form->end()?>
            </div>
        </div>
    </div>
    <div class="row hidden-lg"></div>
    <div class="col-lg-3 col-md-6">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Zamówienia wg. klienta')) ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?=$this->Form->create(null,['type'=>'post'])?>
                <?=$this->Form->control('uzytkownik',['type'=>'select','options'=>$klienci,'label'=>$this->Txt->printAdmin(__('Admin | Klient'))])?>
                <?=$this->Form->control('from',['type'=>'text','datepicker'=>'date','default'=>date('Y-m-d', strtotime('- 30 days')),'label'=>$this->Txt->printAdmin(__('Admin | Data początkowa'))])?>
                <?=$this->Form->control('to',['type'=>'text','datepicker'=>'date','default'=>date('Y-m-d'),'label'=>$this->Txt->printAdmin(__('Admin | Data końcowa'))])?>
                <?=$this->Form->control('statusy.wg_klienta',['options'=>$statusy,'id'=>'status_klient','multiple'=>'checkbox','default'=>'zrealizowane','label'=>$this->Txt->printAdmin(__('Admin | Uwzględniaj statusy:'))])?>
                <div class="text-right">
                    <input type="hidden" name="typ" value="wg_klienta"/>
                    <button class="btn btn-info" type="submit"><i class="fa fa-download"></i> <?=$this->Txt->printAdmin(__('Admin | pobierz raport'))?></button>
                </div>        
                <?=$this->Form->end()?>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Towary wg. klienta')) ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?=$this->Form->create(null,['type'=>'post'])?>
                <?=$this->Form->control('uzytkownik',['type'=>'select','options'=>$klienci,'label'=>$this->Txt->printAdmin(__('Admin | Klient'))])?>
                <?=$this->Form->control('from',['type'=>'text','datepicker'=>'date','default'=>date('Y-m-d', strtotime('- 30 days')),'label'=>$this->Txt->printAdmin(__('Admin | Data początkowa'))])?>
                <?=$this->Form->control('to',['type'=>'text','datepicker'=>'date','default'=>date('Y-m-d'),'label'=>$this->Txt->printAdmin(__('Admin | Data końcowa'))])?>
                <?=$this->Form->control('statusy.towary_klient',['options'=>$statusy,'id'=>'status_towary_klient','multiple'=>'checkbox','default'=>'zrealizowane','label'=>$this->Txt->printAdmin(__('Admin | Uwzględniaj statusy:'))])?>
                <div class="text-right">
                    <input type="hidden" name="typ" value="towary_klient"/>
                    <button class="btn btn-info" type="submit"><i class="fa fa-download"></i> <?=$this->Txt->printAdmin(__('Admin | pobierz raport'))?></button>
                </div>        
                <?=$this->Form->end()?>
            </div>
        </div>
    </div>
</div>