<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <?= $this->Form->create($producent,['type'=>'file']) ?>
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Edit {0}', [__('Admin | Producent')])) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?=
                        $this->Html->link(
                                $this->Html->Tag('i', '', ['class' => 'fa fa-trash']) . ' ' . $this->Txt->printAdmin(__('Admin | Usuń')), ['action' => 'delete', $producent->id], ['confirm-btn-yes' => $this->Txt->printAdmin(__('Admin | Tak')), 'confirm-btn-no' => $this->Txt->printAdmin(__('Admin | Nie')), 'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $producent->id)), 'class' => 'btn btn-xs btn-danger confirm-link', 'escape' => false]
                        )
                        ?></li>
                    <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> ' . __('Admin | Wróć do {0}', [__('Admin | Listy')])), ['action' => 'index'], ['escape' => false, 'class' => 'btn btn-xs btn-warning']) ?></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <fieldset>
                    <?php
                    $lngInputs = [];
                    echo $this->Form->control('nazwa', ['label' => $this->Txt->printAdmin(__('Admin | Nazwa producenta'))]);
                    echo $this->Txt->file($this->Form->control('logo', ['type' => 'file', 'label' => false]), $this->Txt->printAdmin(__('Admin | Dodaj logo')), (!empty($producent->logo) ? $displayPath['producent'] . $producent->logo : ''));
//                    echo $this->Form->control('na_zamowienie', ['label' => $this->Txt->printAdmin(__('Admin | Czas realizacji dostawy dla produktów niedostępnych'))]);
//                    $lngInputs['naglowek'] = ['summernote' => 'true','label' => $this->Txt->printAdmin(__('Admin | Nagłówek na stronie producenta - {0}'))];
//                    $lngInputs['gwarancja'] = ['summernote' => 'true','label' => $this->Txt->printAdmin(__('Admin | Informacje o gwarancji producenta - {0}'))];
//                    echo $this->Form->control('www', ['label' => $this->Txt->printAdmin(__('Admin | Adres strony www producenta'))]);
//                    echo $this->Translation->inputs($this->Form, $languages, $lngInputs);
                    echo $this->Form->control('ukryty', ['type'=>'checkbox','label' => $this->Txt->printAdmin(__('Admin | Producent niewidoczny dla klienta'))]);
                    ?>
                </fieldset>
                <hr>
                <div class="greyArea">
                    <h3><?=$this->Txt->printAdmin(__('Admin | Mapowanie nazw producentów do integracji z hurtowniami'))?></h3>
                    <?php                                        foreach ($hurtownie as $hurtowniaKey => $hurtowniaNazwa){
                        echo $this->Form->control('nazwa_'.$hurtowniaKey,['type'=>'text','label'=>$hurtowniaNazwa]);
                    } ?>
                </div>
            </div>
            <div class="x_footer text-right">
                <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')), ['class' => 'btn btn-sm btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
