<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Wysylka[]|\Cake\Collection\CollectionInterface $wysylka
  */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Wysylka')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Html->tag('i','',['class'=>'fa fa-plus']).' '.$this->Txt->printAdmin(__('Admin | Dodaj {0}',[$this->Txt->printAdmin(__('Admin | Wysylka'))])), ['action' => 'add'],['escape'=>false,'class'=>'btn btn-xs btn-primary']) ?></li>
                    <li><?= $this->Html->link($this->Html->tag('i','',['class'=>'fa fa-plane']).' '.$this->Txt->printAdmin(__('Admin | Dodatkowe koszty krajów')), ['action' => 'kraje'],['escape'=>false,'class'=>'btn btn-xs btn-info']) ?></li>
                </ul>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped data-table">
                    <thead>
            <tr>
                                <th data-priority="2"><?= $this->Txt->printAdmin(__('Admin | id')) ?></th>
                                <th data-priority="3"><?= $this->Txt->printAdmin(__('Admin | Nazwa')) ?></th>
                               <th data-priority="7"><?= $this->Txt->printAdmin(__('Admin | Koszt dodatkowy')) ?></th>
                                <th data-priority="8"><?= $this->Txt->printAdmin(__('Admin | aktywna')) ?></th>
                                <th data-priority="10"><?= $this->Txt->printAdmin(__('Admin | Duzy gabaryt')) ?></th>
                                <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
            </tr>
        </thead>
        <tbody>
            <?php $rodzaje=[0=>__('Paczki zwykłe'),1=>__('Paczki wielkogabarytowe'),2=>__('Paczki zwykłe i wielkogabarytowe')]; ?>
            <?php foreach ($wysylka as $wysylka): ?>
            <tr>
                <td><?= $wysylka->id ?></td>
                <td><?= $wysylka->nazwa ?></td>
                 <td><?= $this->Txt->cena($wysylka->koszt_dodatkowy) ?></td>
                <td><?= $this->Txt->printBool($wysylka->aktywna) ?></td>
                <td><?= $rodzaje[$wysylka->duzy_gabaryt] ?></td>
                <td class="actions">
                    <?= $this->Html->link('<i class="fa fa-cogs"></i>', ['action' => 'edit', $wysylka->id],['escape'=>false,'class'=>'btn btn-default btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Edytuj'))]) ?>
                    <?= $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $wysylka->id], ['confirm-btn-yes'=>$this->Txt->printAdmin(__('Admin | Tak')),'confirm-btn-no'=>$this->Txt->printAdmin(__('Admin | Nie')),'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $wysylka->id)),'escape'=>false,'class'=>'btn btn-danger btn-xs confirm-link','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Usuń'))]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
                </table>
            </div>
        </div>
    </div>
</div>