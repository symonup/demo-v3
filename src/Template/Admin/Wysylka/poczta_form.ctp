<?php
use Cake\Core\Configure;
use Cake\Routing\Router;
echo $this->Form->create('Nadawca', array('id'=>'NadawcaAdminSzczegolyForm','url' => Router::url(array('controller'=>'zamowienie','action' => 'kurier_xml', 'poczta_'.date('ymd_His') . '_'.$zamId . '.xml', 'poczta'))));
echo $this->Html->tag('h3', 'Poczta Polska plik xml');
echo $this->Form->input('Nadawca.attr.Struktura', array('type' => 'hidden', 'value' => '1.6'));
echo $this->Form->input('Nadawca.attr.Nazwa', array('type' => 'hidden', 'value' => Configure::read('poczta.poczta_xml_nadawca_nazwa')));
echo $this->Form->input('Nadawca.attr.NazwaSkrocona', array('type' => 'hidden', 'value' => Configure::read('poczta.poczta_xml_nadawca_nazwa_skrucona')));
echo $this->Form->input('Nadawca.attr.Ulica', array('type' => 'hidden', 'value' => Configure::read('poczta.poczta_xml_nadawca_ulica')));
echo $this->Form->input('Nadawca.attr.Dom', array('type' => 'hidden', 'value' => Configure::read('poczta.poczta_xml_nadawca_dom')));
echo $this->Form->input('Nadawca.attr.Lokal', array('type' => 'hidden', 'value' => Configure::read('poczta.poczta_xml_nadawca_lokal')));
echo $this->Form->input('Nadawca.attr.Miejscowosc', array('type' => 'hidden', 'value' => Configure::read('poczta.poczta_xml_nadawca_miasto')));
echo $this->Form->input('Nadawca.attr.Kod', array('type' => 'hidden', 'value' => str_replace('-', '', Configure::read('poczta.poczta_xml_nadawca_kod'))));
echo $this->Form->input('Nadawca.attr.NIP', array('type' => 'hidden', 'value' => Configure::read('poczta.poczta_xml_nadawca_nip')));
echo $this->Form->input('Nadawca.attr.Zrodlo', array('type' => 'hidden', 'value' => 'NADAWCA'));
echo $this->Form->input('Nadawca.attr.Guid', array('type' => 'hidden', 'value' => Configure::read('poczta.poczta_xml_nadawca_guid')));

echo $this->Form->input('Nadawca.Zbior.attr.Nazwa', array('type' => 'hidden', 'value' => date('Y-m-d') . '\1'));
echo $this->Form->input('Nadawca.Zbior.attr.DataUtworzenia', array('type' => 'hidden', 'value' => date('Y-m-dTH:i:s')));
echo $this->Form->input('Nadawca.Zbior.attr.Opis', array('type' => 'hidden', 'value' => 'Przykladowe przesylki XML'));
echo $this->Form->input('Nadawca.Zbior.attr.IloscPrzesylek', array('type' => 'hidden', 'value' => '1'));
echo $this->Form->input('Nadawca.Zbior.attr.Guid', array('type' => 'hidden', 'value' => $this->XmlInput->GUID()));

echo $this->Form->input('Nadawca.Zbior.Przesylka.attr.Guid', array('type' => 'hidden', 'value' => $this->XmlInput->GUID()));
switch ($typ) {
    case '845' : {
        echo '<div class="shipCont col-xs-12 col-sm-12 col-md-4 col-lg-4">';
            echo $this->Form->input('Nadawca.Zbior.Przesylka.0.Atrybut.val', array('label' => __('Symbol'), 'type'=>'hidden', 'value' => '845'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.0.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.0.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Symbol'));

//echo $this->Form->input('Nadawca.Zbior.Przesylka.1.Atrybut.val', array('label' => __('NrNadania'), 'value' => '00159007733847203010'));
//echo $this->Form->input('Nadawca.Zbior.Przesylka.1.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
//echo $this->Form->input('Nadawca.Zbior.Przesylka.1.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'NrNadania'));
//echo $this->Form->input('Nadawca.Zbior.Przesylka.2.Atrybut.val', array('label' => __('Masa'), 'value' => '5000'));
//echo $this->Form->input('Nadawca.Zbior.Przesylka.2.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
//echo $this->Form->input('Nadawca.Zbior.Przesylka.2.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Masa'));
//echo $this->Form->input('Nadawca.Zbior.Przesylka.3.Atrybut.val', array('label' => __('Umowa'), 'value' => '22613'));
//echo $this->Form->input('Nadawca.Zbior.Przesylka.3.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
//echo $this->Form->input('Nadawca.Zbior.Przesylka.3.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Umowa'));
//echo $this->Form->input('Nadawca.Zbior.Przesylka.4.Atrybut.val', array('label' => __('KartaUmowy'), 'value' => '36857'));
//echo $this->Form->input('Nadawca.Zbior.Przesylka.4.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
//echo $this->Form->input('Nadawca.Zbior.Przesylka.4.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'KartaUmowy'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.5.Atrybut.val', array('label' => __('Kategoria'), 'options' => array('P' => 'Priorytetowa', 'E' => 'Ekonomiczna'), 'selected' => 'E'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.5.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.5.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Kategoria'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.6.Atrybut.val', array('label' => __('Poste Restante'), 'options' => array('T' => 'tak', 'N' => 'nie'), 'selected' => 'N'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.6.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.6.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'PosteRestante'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.7.Atrybut.val', array('label' => __('Egzemplarz biblioteczny'), 'options' => array('T' => 'tak', 'N' => 'nie'), 'selected' => 'N'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.7.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.7.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'EgzBibl'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.8.Atrybut.val', array('label' => __('Dla ociemniałych'), 'options' => array('T' => 'tak', 'N' => 'nie'), 'selected' => 'N'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.8.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.8.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'DlaOciemn'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.9.Atrybut.val', array('label' => __('Ilosc'), 'value' => '1'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.9.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.9.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Ilosc'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.10.Atrybut.val', array('label' => __('Uslugi'), 'options' => array('Z' => 'żądanie zwrotu/dosłania', 'W' => 'zadeklarowanie wartości', 'O' => 'potwierdzenie odbioru'), 'multiple' => true));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.10.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.10.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Uslugi'));

//            echo $this->Form->input('Nadawca.Zbior.Przesylka.11.Atrybut.val', array('label' => __('Wartosc w groszach'), 'value' => $daneForm['Wartosc']['wartosc_przesylki']));
//            echo $this->Form->input('Nadawca.Zbior.Przesylka.11.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
//            echo $this->Form->input('Nadawca.Zbior.Przesylka.11.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Wartosc'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.12.Atrybut.val', array('label' => __('Ilość potwierdzeń odbioru'), 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.12.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.12.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'IloscPotwOdb'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.13.Atrybut.val', array('label' => __('Strefa'), 'options' => array('A' => 'gabaryt A', 'B' => 'gabaryt B'), 'selected' => 'A'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.13.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.13.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Strefa'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.14.Atrybut.val', array('label' => __('Wersja'), 'value' => '1'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.14.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.14.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Wersja'));
echo '</div><div class="shipCont col-xs-12 col-sm-12 col-md-4 col-lg-4">';
            echo $this->Form->input('Nadawca.Zbior.Przesylka.15.Atrybut.val', array('label' => __('Nazwa'), 'value' => $daneForm['Odbiorca']['nazwa']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.15.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.15.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Nazwa'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.16.Atrybut.val', array('label' => __('NazwaII'), 'value' => $daneForm['Odbiorca']['nazwa2']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.16.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.16.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'NazwaII'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.17.Atrybut.val', array('label' => __('Ulica'), 'value' => $daneForm['Odbiorca']['ulica']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.17.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.17.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Ulica'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.18.Atrybut.val', array('label' => __('Dom'), 'value' => $daneForm['Odbiorca']['dom']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.18.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.18.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Dom'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.19.Atrybut.val', array('label' => __('Lokal'), 'value' => $daneForm['Odbiorca']['lokal']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.19.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.19.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Lokal'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.20.Atrybut.val', array('label' => __('Miejscowosc'), 'value' => $daneForm['Odbiorca']['miasto']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.20.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.20.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Miejscowosc'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.21.Atrybut.val', array('label' => __('Kod'), 'value' => $daneForm['Odbiorca']['kod']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.21.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.21.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Kod'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.22.Atrybut.val', array('label' => __('Kraj'), 'value' => 'POLSKA'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.22.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.22.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Kraj'));
            echo '</div>';
        } break;
    case '846' : {
        echo '<div class="shipCont col-xs-12 col-sm-12 col-md-4 col-lg-4">';
            echo $this->Form->input('Nadawca.Zbior.Przesylka.0.Atrybut.val', array('label' => __('Symbol'), 'type'=>'hidden', 'value' => '846'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.0.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.0.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Symbol'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.5.Atrybut.val', array('label' => __('Kategoria'), 'options' => array('P' => 'Priorytetowa', 'E' => 'Ekonomiczna'), 'selected' => 'E'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.5.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.5.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Kategoria'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.6.Atrybut.val', array('label' => __('Poste Restante'), 'options' => array('T' => 'tak', 'N' => 'nie'), 'selected' => 'N'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.6.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.6.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'PosteRestante'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.7.Atrybut.val', array('label' => __('Egzemplarz biblioteczny'), 'options' => array('T' => 'tak', 'N' => 'nie'), 'selected' => 'N'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.7.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.7.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'EgzBibl'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.8.Atrybut.val', array('label' => __('Dla ociemniałych'), 'options' => array('T' => 'tak', 'N' => 'nie'), 'selected' => 'N'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.8.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.8.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'DlaOciemn'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.9.Atrybut.val', array('label' => __('Ilosc'), 'value' => '1'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.9.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.9.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Ilosc'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.10.Atrybut.val', array('label' => __('Uslugi'), 'options' => array('Z' => 'żądanie zwrotu/dosłania', 'R' => 'polecenie', 'O' => 'potwierdzenie odbioru'), 'selected' => 'R', 'multiple' => true));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.10.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.10.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Uslugi'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.11.Atrybut.val', array('label' => __('Wartosc w groszach'), 'value' => $daneForm['Wartosc']['wartosc_przesylki']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.11.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.11.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Wartosc'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.12.Atrybut.val', array('label' => __('Ilość potwierdzeń odbioru'), 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.12.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.12.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'IloscPotwOdb'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.13.Atrybut.val', array('label' => __('Strefa'), 'options' => array('A' => 'gabaryt A', 'B' => 'gabaryt B'), 'selected' => 'A'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.13.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.13.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Strefa'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.14.Atrybut.val', array('label' => __('Wersja'), 'value' => '1'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.14.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.14.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Wersja'));
echo '</div><div class="shipCont col-xs-12 col-sm-12 col-md-4 col-lg-4">';
            echo $this->Form->input('Nadawca.Zbior.Przesylka.15.Atrybut.val', array('label' => __('Nazwa'), 'value' => $daneForm['Odbiorca']['nazwa']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.15.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.15.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Nazwa'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.16.Atrybut.val', array('label' => __('NazwaII'), 'value' => $daneForm['Odbiorca']['nazwa2']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.16.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.16.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'NazwaII'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.17.Atrybut.val', array('label' => __('Ulica'), 'value' => $daneForm['Odbiorca']['ulica']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.17.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.17.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Ulica'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.18.Atrybut.val', array('label' => __('Dom'), 'value' => $daneForm['Odbiorca']['dom']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.18.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.18.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Dom'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.19.Atrybut.val', array('label' => __('Lokal'), 'value' => $daneForm['Odbiorca']['lokal']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.19.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.19.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Lokal'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.20.Atrybut.val', array('label' => __('Miejscowosc'), 'value' => $daneForm['Odbiorca']['miasto']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.20.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.20.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Miejscowosc'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.21.Atrybut.val', array('label' => __('Kod'), 'value' => $daneForm['Odbiorca']['kod']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.21.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.21.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Kod'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.22.Atrybut.val', array('label' => __('Kraj'), 'value' => 'POLSKA'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.22.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.22.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Kraj'));
            echo '</div>';
        }break;
    case '848' : {
        echo '<div class="shipCont col-xs-12 col-sm-12 col-md-4 col-lg-4">';
            echo $this->Form->input('Nadawca.Zbior.Przesylka.0.Atrybut.val', array('label' => __('Symbol'), 'type'=>'hidden', 'value' => '848'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.0.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.0.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Symbol'));
            
            echo $this->Form->input('Nadawca.Zbior.Przesylka.1.Atrybut.val', array('label' => __('Kwota pobrania w groszach'), 'value' => $daneForm['Wartosc']['wartosc_razem'],'tmpreq'=>'1','required'=>true));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.1.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.1.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'KwotaPobrania'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.2.Atrybut.val', array('label' => __('Sposób przekazania kwoty pobrania'), 'options' => array('P' => 'na wskazany adres', 'S' => 'na rachunek bankowy'),'tmpreq'=>'1','required'=>true));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.2.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.2.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'SposobPobrania'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.5.Atrybut.val', array('label' => __('Kategoria'), 'options' => array('P' => 'Priorytetowa', 'E' => 'Ekonomiczna'), 'selected' => 'E','tmpreq'=>'1','required'=>true));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.5.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.5.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Kategoria'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.6.Atrybut.val', array('label' => __('Poste Restante'), 'options' => array('T' => 'tak', 'N' => 'nie'), 'selected' => 'N','tmpreq'=>'1','required'=>true));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.6.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.6.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'PosteRestante'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.9.Atrybut.val', array('label' => __('Ilosc'), 'value' => '1','tmpreq'=>'1','required'=>true));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.9.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.9.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Ilosc'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.10.Atrybut.val', array('label' => __('Uslugi'), 'options' => array('S' => 'sprawdzenie zawartości', 'W' => 'zadeklarowanie wartości','K'=>'ostrożnie', 'O' => 'potwierdzenie odbioru'), 'multiple' => true));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.10.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.10.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Uslugi'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.11.Atrybut.val', array('label' => __('Deklarowana wartość przesyłki; podana w groszach.'), 'value' => $daneForm['Wartosc']['wartosc_przesylki']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.11.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.11.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Wartosc'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.12.Atrybut.val', array('label' => __('Ilość potwierdzeń odbioru'), 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.12.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.12.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'IloscPotwOdb'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.13.Atrybut.val', array('label' => __('Strefa'), 'options' => array('A' => 'gabaryt A', 'B' => 'gabaryt B'), 'selected' => 'A'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.13.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.13.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Strefa'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.14.Atrybut.val', array('label' => __('Wersja'), 'value' => '1'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.14.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.14.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Wersja'));
echo '</div><div class="shipCont col-xs-12 col-sm-12 col-md-4 col-lg-4">';
            echo $this->Form->input('Nadawca.Zbior.Przesylka.15.Atrybut.val', array('label' => __('Nazwa'), 'value' => $daneForm['Odbiorca']['nazwa']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.15.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.15.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Nazwa'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.16.Atrybut.val', array('label' => __('NazwaII'), 'value' => $daneForm['Odbiorca']['nazwa2']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.16.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.16.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'NazwaII'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.17.Atrybut.val', array('label' => __('Ulica'), 'value' => $daneForm['Odbiorca']['ulica']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.17.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.17.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Ulica'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.18.Atrybut.val', array('label' => __('Dom'), 'value' => $daneForm['Odbiorca']['dom']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.18.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.18.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Dom'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.19.Atrybut.val', array('label' => __('Lokal'), 'value' => $daneForm['Odbiorca']['lokal']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.19.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.19.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Lokal'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.20.Atrybut.val', array('label' => __('Miejscowosc'), 'value' => $daneForm['Odbiorca']['miasto']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.20.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.20.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Miejscowosc'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.21.Atrybut.val', array('label' => __('Kod'), 'value' => $daneForm['Odbiorca']['kod']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.21.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.21.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Kod'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.22.Atrybut.val', array('label' => __('Kraj'), 'value' => 'POLSKA'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.22.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.22.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Kraj'));
            echo '</div>';
        } break;
    case '840' : {
        echo '<div class="shipCont col-xs-12 col-sm-12 col-md-4 col-lg-4">';
            echo $this->Form->input('Nadawca.Zbior.Przesylka.0.Atrybut.val', array('label' => __('Symbol'), 'type'=>'hidden', 'value' => '840'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.0.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.0.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Symbol'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.5.Atrybut.val', array('label' => __('Kategoria'), 'options' => array('P' => 'Priorytetowa', 'E' => 'Ekonomiczna'), 'selected' => 'E','tmpreq'=>'1','required'=>true));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.5.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.5.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Kategoria'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.6.Atrybut.val', array('label' => __('Poste Restante'), 'options' => array('T' => 'tak', 'N' => 'nie'), 'selected' => 'N','tmpreq'=>'1','tmpreq'=>'1','required'=>true));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.6.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.6.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'PosteRestante'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.7.Atrybut.val', array('label' => __('Egzemplarz biblioteczny'), 'options' => array('T' => 'tak', 'N' => 'nie'), 'selected' => 'N'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.7.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.7.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'EgzBibl'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.8.Atrybut.val', array('label' => __('Dla ociemniałych'), 'options' => array('T' => 'tak', 'N' => 'nie'), 'selected' => 'N'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.8.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.8.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'DlaOciemn'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.9.Atrybut.val', array('label' => __('Ilosc'), 'value' => '1','tmpreq'=>'1','required'=>true));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.9.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.9.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Ilosc'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.13.Atrybut.val', array('label' => __('Strefa'), 'options' => array('A' => 'gabaryt A', 'B' => 'gabaryt B'), 'selected' => 'A'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.13.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.13.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Strefa'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.14.Atrybut.val', array('label' => __('Wersja'), 'value' => '1'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.14.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.14.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Wersja'));
echo '</div><div class="shipCont col-xs-12 col-sm-12 col-md-4 col-lg-4">';
            echo $this->Form->input('Nadawca.Zbior.Przesylka.15.Atrybut.val', array('label' => __('Nazwa'), 'value' => $daneForm['Odbiorca']['nazwa']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.15.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.15.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Nazwa'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.16.Atrybut.val', array('label' => __('NazwaII'), 'value' => $daneForm['Odbiorca']['nazwa2']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.16.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.16.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'NazwaII'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.17.Atrybut.val', array('label' => __('Ulica'), 'value' => $daneForm['Odbiorca']['ulica']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.17.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.17.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Ulica'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.18.Atrybut.val', array('label' => __('Dom'), 'value' => $daneForm['Odbiorca']['dom']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.18.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.18.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Dom'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.19.Atrybut.val', array('label' => __('Lokal'), 'value' => $daneForm['Odbiorca']['lokal']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.19.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.19.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Lokal'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.20.Atrybut.val', array('label' => __('Miejscowosc'), 'value' => $daneForm['Odbiorca']['miasto']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.20.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.20.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Miejscowosc'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.21.Atrybut.val', array('label' => __('Kod'), 'value' => $daneForm['Odbiorca']['kod']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.21.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.21.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Kod'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.22.Atrybut.val', array('label' => __('Kraj'), 'value' => 'POLSKA'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.22.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.22.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Kraj'));
            echo '</div>';
        } break;
    case '850' : {
        echo '<div class="shipCont col-xs-12 col-sm-12 col-md-4 col-lg-4">';
            echo $this->Form->input('Nadawca.Zbior.Przesylka.0.Atrybut.val', array('label' => __('Symbol'), 'type'=>'hidden', 'value' => '850'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.0.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.0.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Symbol'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.1.Atrybut.val', array('label' => __('Ilosc'), 'value' => '1','tmpreq'=>'1','required'=>true));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.1.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.1.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Ilosc'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.2.Atrybut.val', array('label' => __('Uslugi'), 'options' => array('S' => 'sprawdzenie zawartości', 'W' => 'zadeklarowanie wartości','K'=>'ostrożnie', 'O' => 'potwierdzenie odbioru'), 'multiple' => true));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.2.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.2.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Uslugi'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.3.Atrybut.val', array('label' => __('Deklarowana wartość przesyłki; podana w groszach.'), 'value' => $daneForm['Wartosc']['wartosc_przesylki']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.3.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.3.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Wartosc'));
            
            echo $this->Form->input('Nadawca.Zbior.Przesylka.4.Atrybut.val', array('label' => __('Kwota pobrania w groszach'), 'value' => $daneForm['Wartosc']['wartosc_razem']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.4.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.4.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'KwotaPobrania'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.5.Atrybut.val', array('label' => __('Informuje o rodzaju przesyłki dla E-PRZESYŁKI'), 'options' => array('E'=>'standard','P' => 'pobranie na wskazany adres', 'S' => 'pobranie na rachunek bankowy')));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.5.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.5.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'SposobPobrania'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.6.Atrybut.val', array('label' => __('Informuje o sposobie powiadomienia nadawcy o oczekującej E-PRZESYŁCE'), 'options' => array('E'=>'E-mail','M' => 'SMS')));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.6.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.6.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'SposobPowiadomieniaNadawcy'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.7.Atrybut.val', array('label' => __('Informuje o konkretnej wartości dla sposobu powiadomienia nadawcy: e-mail (50 znaków), SMS (9 cyfr)'), 'value' => '1','tmpreq'=>'1','required'=>true));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.7.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.7.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'KontaktNadawcy'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.8.Atrybut.val', array('label' => __('Informuje o sposobie powiadomienia adresata o oczekującej E-PRZESYŁCE'), 'options' => array('E'=>'E-mail','M' => 'SMS')));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.8.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.8.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'SposobPowiadomieniaAdresata'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.9.Atrybut.val', array('label' => __('Informuje o konkretnej wartości dla sposobu powiadomienia adresata: e-mail (50 znaków), SMS (9 cyfr)'), 'value' => '1','tmpreq'=>'1','required'=>true));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.9.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.9.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'KontaktAdresata'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.10.Atrybut.val', array('label' => __('Kod pocztowy Placówki Pocztowej wydającej E-PRZESYŁKĘ'), 'value' => '','tmpreq'=>'1','required'=>true));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.10.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.10.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'KodUP'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.11.Atrybut.val', array('label' => __('Nazwa Placówki Pocztowej wydającej E-PRZESYŁKĘ'), 'value' => '','tmpreq'=>'1','required'=>true));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.11.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.11.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'MiejscowoscUP'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.14.Atrybut.val', array('label' => __('Wersja'), 'value' => '1'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.14.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.14.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Wersja'));
            echo '</div><div class="shipCont col-xs-12 col-sm-12 col-md-4 col-lg-4">';
            echo $this->Form->input('Nadawca.Zbior.Przesylka.15.Atrybut.val', array('label' => __('Nazwa'), 'value' => $daneForm['Odbiorca']['nazwa']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.15.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.15.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Nazwa'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.16.Atrybut.val', array('label' => __('NazwaII'), 'value' => $daneForm['Odbiorca']['nazwa2']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.16.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.16.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'NazwaII'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.17.Atrybut.val', array('label' => __('Ulica'), 'value' => $daneForm['Odbiorca']['ulica']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.17.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.17.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Ulica'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.18.Atrybut.val', array('label' => __('Dom'), 'value' => $daneForm['Odbiorca']['dom']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.18.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.18.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Dom'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.19.Atrybut.val', array('label' => __('Lokal'), 'value' => $daneForm['Odbiorca']['lokal']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.19.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.19.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Lokal'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.20.Atrybut.val', array('label' => __('Miejscowosc'), 'value' => $daneForm['Odbiorca']['miasto']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.20.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.20.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Miejscowosc'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.21.Atrybut.val', array('label' => __('Kod'), 'value' => $daneForm['Odbiorca']['kod']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.21.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.21.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Kod'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.22.Atrybut.val', array('label' => __('Kraj'), 'value' => 'POLSKA'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.22.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.22.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Kraj'));
            echo '</div>';
        } break;
    case '812' : {
        echo '<div class="shipCont col-xs-12 col-sm-12 col-md-4 col-lg-4">';
            echo $this->Form->input('Nadawca.Zbior.Przesylka.0.Atrybut.val', array('label' => __('Symbol'), 'type'=>'hidden', 'value' => '812'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.0.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.0.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Symbol'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.1.Atrybut.val', array('label' => __('Ilosc'), 'value' => '1','tmpreq'=>'1','required'=>true));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.1.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.1.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Ilosc'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.2.Atrybut.val', array('label' => __('Uslugi'), 'options' => array('C'=>'Chroniona',
'W'=>'Zadeklarowanie wartości',
'D'=>'Potwierdzenie doręczenia',
'O'=>'Potwierdzenie odbioru',
'Z'=>'Doręczenie do rąk własnych',
'N'=>'Doręczenie w niedzielę lub święto',
'S'=>'Sprawdzenie zawartości przesyłki przez odbiorcę',
'P'=>'Odbiór od nadawcy w godzinach 8:00-20:00',
'X'=>'Odbiór przesyłki w niedzielę lub święto',
'G'=>'Doręczenie przesyłki w godzinach 8:00-20:00',
'E'=>'Doręczenie przesyłki we wskazanym dniu',
'R'=>'Zwrot podpisanych dokumentów',
'B'=>'Doręczenie przesyłki w sobotę'), 'multiple' => true));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.2.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.2.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Uslugi'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.3.Atrybut.val', array('label' => __('Odległość dla nadawanej przesyłki POCZTEX.'), 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.3.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.3.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Odleglosc'));
            
            echo $this->Form->input('Nadawca.Zbior.Przesylka.4.Atrybut.val', array('label' => __('Termin-serwis dla przesyłki'), 'options' => array(
                'B'=>'Bezpośredni',
'D'=>'Na dziś krajowy',
'M'=>'Na dziś miejski',
'U'=>'Na dziś miejski super',
'A'=>'Na dziś aglomeracja',
'P'=>'Na jutro południe',
'R'=>'Na jutro poranek',
'S'=>'Na jutro standard')));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.4.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.4.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Termin'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.5.Atrybut.val', array('label' => __('Opłata za nadanie przesyłki.'), 'options' => array('N'=>'nadawca','A'=>'adresat')));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.5.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.5.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'UiszczaOplate'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.6.Atrybut.val', array('label' => __('Rodzaj przesyłki'), 'options' => array('Z'=>'zwykły','E'=>'full pack 1','F'=>'full pack 2')));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.6.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.6.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Typ'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.7.Atrybut.val', array('label' => __('Ponadwymiarowa'), 'options' => array('T'=>'Tak','N'=>'Nie'),'selected'=>'N'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.7.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.7.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Ponadwymiarowa'));
            
            echo $this->Form->input('Nadawca.Zbior.Przesylka.14.Atrybut.val', array('label' => __('Wersja'), 'value' => '2'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.14.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.14.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Wersja'));
            echo '</div><div class="shipCont col-xs-12 col-sm-12 col-md-4 col-lg-4">';
            echo $this->Form->input('Nadawca.Zbior.Przesylka.15.Atrybut.val', array('label' => __('Nazwa'), 'value' => $daneForm['Odbiorca']['nazwa']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.15.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.15.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Nazwa'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.16.Atrybut.val', array('label' => __('NazwaII'), 'value' => $daneForm['Odbiorca']['nazwa2']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.16.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.16.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'NazwaII'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.17.Atrybut.val', array('label' => __('Ulica'), 'value' => $daneForm['Odbiorca']['ulica']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.17.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.17.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Ulica'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.18.Atrybut.val', array('label' => __('Dom'), 'value' => $daneForm['Odbiorca']['dom']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.18.Atrybut.attr.typ', array('type' => 'hidden', 'value' => ''));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.18.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Dom'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.19.Atrybut.val', array('label' => __('Lokal'), 'value' => $daneForm['Odbiorca']['lokal']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.19.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.19.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Lokal'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.20.Atrybut.val', array('label' => __('Miejscowosc'), 'value' => $daneForm['Odbiorca']['miasto']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.20.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.20.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Miejscowosc'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.21.Atrybut.val', array('label' => __('Kod'), 'value' => $daneForm['Odbiorca']['kod']));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.21.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.21.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Kod'));

            echo $this->Form->input('Nadawca.Zbior.Przesylka.22.Atrybut.val', array('label' => __('Kraj'), 'value' => 'POLSKA'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.22.Atrybut.attr.typ', array('type' => 'hidden', 'value' => 'Adresat'));
            echo $this->Form->input('Nadawca.Zbior.Przesylka.22.Atrybut.attr.nazwa', array('type' => 'hidden', 'value' => 'Kraj'));
            echo '</div>';
        } break;
}
echo $this->Html->tag('div',$this->Form->submit('Pobierz',['class'=>'btn btn-success']),['class'=>'col-xs-12 text-right']);
echo $this->Form->end();
?>
