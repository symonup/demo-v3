<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
                <?= $this->Form->create($wysylka) ?>
            <div class="x_title">
                    <h2><?=$this->Txt->printAdmin(__('Admin | Add {0}',[__('Admin | Wysylka')]))?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                                              <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> '.__('Admin | Wróć do {0}',[__('Admin | Listy')])), ['action' => 'index'],['escape'=>false,'class'=>'btn btn-xs btn-warning']) ?></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
            <div class="x_content">
    <fieldset>
        <?php
            $lngInputs=[];
                                $lngInputs['nazwa']=['label'=>$this->Txt->printAdmin(__('Admin | nazwa - {0}'))];
                                $lngInputs['opis']=['label'=>$this->Txt->printAdmin(__('Admin | opis - {0}'))];
                                $lngInputs['komunikat']=['label'=>$this->Txt->printAdmin(__('Admin | Komunikat po złożeniu zamówienia - {0}')),'type'=>'textarea','summernote'=>'true'];
                    echo $this->Translation->inputs($this->Form, $languages, $lngInputs);
                 echo $this->Form->input('subiekt_kod', ['type' => 'text','label'=>__('Symbol w subiekcie')]);
                 echo $this->Form->input('vat_id', ['options' => $vat,'label'=>__('Vat')]);
            echo $this->Form->input('koszt_dodatkowy',['type'=>'text','data-type'=>'float','label'=>__('Koszt dodatkowy')]);
            echo $this->Form->input('aktywna',['type'=>'checkbox','label'=>__('Aktywna')]);
            echo $this->Form->input('koszty_kraj',['type'=>'checkbox','label'=>__('Dostępna dla innych krajów niż {0}',[\Cake\Core\Configure::read('wysylka.krajPodstawowy')])]);
            echo $this->Form->input('paczkomat',['type'=>'checkbox','label'=>__('Paczkomat Inpost')]);
            echo $this->Form->input('odbior_osobisty',['type'=>'checkbox','label'=>__('Odbiór osobisty')]);
            echo $this->Form->input('elektroniczna',['type'=>'checkbox','label'=>__('E-wysyłka (Zaznacz jeżeli wysyłka nie jest fizyczna)')]);
            echo $this->Form->input('is_long',['type'=>'checkbox','label'=>__('Długoterminowa wysyłka zbiorcza')]);
            echo $this->Form->input('duzy_gabaryt',['type'=>'radio','options'=>[0=>__('Paczki zwykłe'),1=>__('Paczki wielkogabarytowe'),2=>__('Paczki zwykłe i wielkogabarytowe')],'default'=>0,'label'=>__('Rodzaj paczek')]);
            echo $this->Form->input('rodzaj_platnosci._ids', ['options' => $rodzajPlatnosci,'multiple'=>'checkbox','label'=>__('Rodzaj płatności')]);
            echo $this->Form->control('punkty_odbioru._ids', ['options' => $punktyOdbioru, 'multiple' => 'checkbox', 'label' => ['text'=>$this->Txt->printAdmin(__('Admin | Punkty odbioru')).$this->Html->tag('span',$this->Txt->printAdmin(__('Admin | Zaznacz tylko dla odbioru osobistego')),['class'=>'label-more-info']),'escape'=>false]]);
            ?>
            <div>
            <hr/>
            <h4>Przedziały wagowe</h4>
        </div>
        <div id="przedzialy">
            <div class="wysylka_koszt"><div>Od</div><div>Koszt brutto</div></div>
        <?php
            if(!empty($wysylka->wysylka_koszt))
            {
                $tr='';
                foreach ($wysylka->wysylka_koszt as $key => $platnosc)
                {
                    $item= $this->Form->input('wysylka_koszt.'.$key.'.id',['type'=>'hidden']);
                    $item.=$this->Form->input('wysylka_koszt.'.$key.'.od',['type'=>'number','label'=>false]);
//                    $item.=$this->Form->input('wysylka_koszt.'.$key.'.koszt_netto',['type'=>'text','data-type'=>'float','label'=>false,'class'=>'wysylka_koszt_netto_value']);
                    $item.=$this->Form->input('wysylka_koszt.'.$key.'.koszt_brutto',['type'=>'text','data-type'=>'float','label'=>false,'class'=>'wysylka_koszt_brutto_value']);
                    $tr.=$this->Html->tag('div',$item.$this->Html->tag('div',$this->Html->link($this->Navi->getIcon('glyphicon-remove'),'javascript:void(0);',['escape'=>false,'onclick'=>"$(this).parents('.wysylka_koszt').remove();return false;"]),['class'=>'remove']),['class'=>'wysylka_koszt','data-key'=>$key]);
                }
                echo $tr;
                
            }
            ?>
            </div>
            <?php
            echo $this->Html->link(__('Dodaj przedział'),'javascript:addPrzedzial();',['escape'=>false,'class'=>'btn btn-primary btn-sm']);
        ?>
        <hr/>
    </fieldset>
            </div>
            <div class="x_footer text-right">
    <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')),['class'=>'btn btn-sm btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<script type="text/javascript">
function addPrzedzial()
{
    var next = 0;
    var przedzial = $('#przedzialy > div:last-child:not(:first-child)');
    if(przedzial.length>0)
        {
            next = parseFloat(przedzial.attr('data-key'))+1;
        }
        $('<div data-key="'+next+'" class="wysylka_koszt"><div class="form-group number required"><input type="number" class="form-control" id="wysylka-koszt-'+next+'-od" required="required" name="wysylka_koszt['+next+'][od]"></div><div class="form-group text required"><input type="text" class="form-control wysylka_koszt_brutto_value" id="wysylka-koszt-'+next+'-koszt-brutto" required="required" data-type="float" name="wysylka_koszt['+next+'][koszt_brutto]"></div><div class="remove"><a href="javascript:void(0);" onclick="$(this).parents(\'.wysylka_koszt\').remove();return false;"><span aria-hidden="true" class="glyphicon glyphicon-remove"> </span></a></div></div>').appendTo('#przedzialy');
}
$(document).on('click','.wysylka_koszt .remove > a',function(){
    $(this).parents('.wysylka_koszt').remove();
});
</script>