<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Wysylka[]|\Cake\Collection\CollectionInterface $wysylka
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Koszty dodatkowe dla innych krajów')) ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped data-table">
                    <thead>
                        <tr>
                            <th data-priority="2"><?= $this->Txt->printAdmin(__('Admin | Kraj')) ?></th>
                            <?php foreach ($wysylki as $wysylkaId => $wysylkaName) {
                                ?>
                                <th data-priority="3"><?= $wysylkaName ?></th>
                            <?php }
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $rodzaje = [0 => __('Paczki zwykłe'), 1 => __('Paczki wielkogabarytowe'), 2 => __('Paczki zwykłe i wielkogabarytowe')]; ?>
                        <?php
                        $valDefault = \Cake\Core\Configure::read('wysylka.krajKosztDodatkowy');
                        foreach ($kraje as $symbol => $kraj):
                            ?>
                            <tr>
                                <td><?= $kraj ?></td>
                                <?php foreach ($wysylki as $wysylkaId => $wysylkaName) {
                                    ?>
                                    <td>
                                        <form method="POST" class="ajax-multi-form-submit" action="<?= \Cake\Routing\Router::url(['action' => 'kraje', $wysylkaId, $symbol]) ?>">
                                            <input type="hidden" name="wysylka[<?= $wysylkaId ?>][kraj]" value="<?= $symbol ?>"/>
                                            <input type="hidden" name="wysylka[<?= $wysylkaId ?>][wysylka_id]" value="<?= $wysylkaId ?>"/>
                                            <input class="on-change-submit" class="form-control" type="text" data-type="float" wysylka-id="<?= $wysylkaId ?>" kraj="<?= $symbol ?>" placeholder="<?= $valDefault ?>" name="wysylka[<?= $wysylkaId ?>][koszt]" value="<?= ((!empty($koszty) && key_exists($wysylkaId, $koszty) && key_exists($symbol, $koszty[$wysylkaId])) ? $koszty[$wysylkaId][$symbol] : '') ?>"/>
                                        </form>
                                    </td>
                                <?php }
                                ?>
                            </tr>
<?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>