<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
                <?= $this->Form->create($administrator) ?>
            <div class="x_title">
                    <h2><?=$this->Txt->printAdmin(__('Admin | Add {0}',[__('Admin | Administrator')]))?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                                              <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> '.__('Admin | Wróć do {0}',[__('Admin | Listy')])), ['action' => 'index'],['escape'=>false,'class'=>'btn btn-xs btn-warning']) ?></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
            <div class="x_content">
                <fieldset>
                    <?php
                    $lngInputs = [];
                    echo $this->Form->control('login', ['label' => $this->Txt->printAdmin(__('Admin | Login'))]);
                    echo $this->Form->control('password', ['label' => $this->Txt->printAdmin(__('Admin | Hasło')),'value'=>'','required'=>false]);
                    echo $this->Form->control('role_id', ['options' => $roles, 'label' => $this->Txt->printAdmin(__('Admin | Uprawnienia'))]);
                    echo $this->Form->control('imie', ['label' => $this->Txt->printAdmin(__('Admin | Imię'))]);
                    echo $this->Form->control('nazwisko', ['label' => $this->Txt->printAdmin(__('Admin | Nazwisko'))]);
                    echo $this->Form->control('email', ['label' => $this->Txt->printAdmin(__('Admin | Email'))]);
                    ?>
                </fieldset>
            </div>
            <div class="x_footer text-right">
    <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')),['class'=>'btn btn-sm btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
