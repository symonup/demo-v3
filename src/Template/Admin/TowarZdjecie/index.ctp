<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TowarZdjecie[]|\Cake\Collection\CollectionInterface $towarZdjecie
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Zdjęcia produktu: [{0}] {1}', [$towar->kod,$towar->nazwa])) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <?php if(empty($iframe)){ ?>
                    <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> '.__('Admin | Wróć do {0}',[__('Admin | listy produktów')])), ['controller'=>'Towar','action' => 'index','#'=>(!empty($towar->id)?'row_'.$towar->id:null)],['escape'=>false,'class'=>'btn btn-xs btn-warning']) ?></li>
                    <?php } ?>
                    <li><form enctype="multipart/form-data" method="post" accept-charset="utf-8" id="add-towar-zdjecie-form"><input type="file" style="display: none;" name="files[]" id="add-towar-zdjecie" multiple/><?= $this->Html->tag('button', $this->Html->tag('i', '', ['class' => 'fa fa-plus']) . ' ' . $this->Txt->printAdmin(__('Admin | Dodaj {0}', [$this->Txt->printAdmin(__('Admin | Zdjecia'))])), ['type' => 'button', 'class' => 'btn btn-xs btn-primary add-towar-zdjecie']) ?></form></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th data-priority="2"><?= $this->Txt->printAdmin(__('Admin | id')) ?></th>
                            <th data-priority="4"><?= $this->Txt->printAdmin(__('Admin | zdjęcie')) ?></th>
                            <th data-priority="6"><?= $this->Txt->printAdmin(__('Admin | domyślne')) ?></th>
                            <th data-priority="6"><?= $this->Txt->printAdmin(__('Admin | ukryte')) ?></th>
                            <th data-priority="8"><?= $this->Txt->printAdmin(__('Admin | alt')) ?></th>
                            <th data-priority="8"><?= $this->Txt->printAdmin(__('Admin | Wariant')) ?></th>
                            <th data-priority="8"><?= $this->Txt->printAdmin(__('Admin | katalog')) ?></th>
                            <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
                        </tr>
                    </thead>
                    <tbody class="sorted-kolejnosc" sort-target="<?= Cake\Routing\Router::url(['action'=>'sort','kolejnosc'])?>">
                        <?php foreach ($towarZdjecie as $towarZdjecie): ?>
                            <tr sort-item-id="<?= $towarZdjecie->id ?>">
                                <td><?= $towarZdjecie->id ?></td>
                                <td class="text-center"><?= $this->Html->image(str_replace('/b2b/', '/', $displayPath['towar']) . $this->Txt->getKatalog($towarZdjecie->id) . '/thumb_' . $towarZdjecie->plik) ?></td>
                                <td><?= $this->Txt->printBool($towarZdjecie->domyslne, (!empty($towarZdjecie->domyslne) ? null : str_replace($basePath, '/', Cake\Routing\Router::url(['controller' => 'TowarZdjecie', 'action' => 'setField', $towarZdjecie->id, 'domyslne', !(bool) $towarZdjecie->domyslne])))) ?></td>
                                <td><?= $this->Txt->printBool($towarZdjecie->ukryte, str_replace($basePath, '/', Cake\Routing\Router::url(['controller' => 'TowarZdjecie', 'action' => 'setField', $towarZdjecie->id, 'ukryte', !(bool) $towarZdjecie->ukryte]))) ?></td>
                                <td>
                                    <div class="input-group towar-zdjecie-alt">
                                        <input item-id="<?=$towarZdjecie->id?>" type="text" value="<?= $towarZdjecie->alt ?>" class="form-control">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-success"><i class="fa fa-save reload-unactive"></i><i class="fa fa-spinner reload-active"></i></button>
                                        </span>
                                    </div>
                                </td>
                                <td class="zdjecie-wariant">
                                    <?php 
                                    if(!empty($warianty)){
                                        echo $this->Form->control('wariant_id.'.$towarZdjecie->id,['type'=>'select','item-id'=>$towarZdjecie->id,'label'=>false,'options'=>$warianty,'empty'=>$this->Txt->printAdmin(__('Admin | Przypisz do wariantu')),'value'=>$towarZdjecie->wariant_id]);
                                    }
                                    else{
                                        echo $this->Txt->printAdmin(__('Admin | Brak wariantów'));
                                    }
                                    ?>
                                </td>
                                <td><?= $this->Txt->getKatalog($towarZdjecie->id) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $towarZdjecie->id,'iframe'=>(!empty($iframe)?$iframe:null)], ['confirm-btn-yes' => $this->Txt->printAdmin(__('Admin | Tak')), 'confirm-btn-no' => $this->Txt->printAdmin(__('Admin | Nie')), 'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $towarZdjecie->id)), 'escape' => false, 'class' => 'btn btn-danger btn-xs confirm-link', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $this->Txt->printAdmin(__('Admin | Usuń'))]) ?>
                    <spna class="sort-item-point btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="<?=$this->Txt->printAdmin(__('Admin | Przeciągnij aby zmienić kolejność'))?>"><i class="fa fa-arrows-alt"></i></spna>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>