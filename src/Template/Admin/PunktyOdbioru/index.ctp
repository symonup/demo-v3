<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\PunktyOdbioru[]|\Cake\Collection\CollectionInterface $punktyOdbioru
  */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Punkty Odbioru')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Html->tag('i','',['class'=>'fa fa-plus']).' '.$this->Txt->printAdmin(__('Admin | Dodaj {0}',[$this->Txt->printAdmin(__('Admin | Punkty Odbioru'))])), ['action' => 'add'],['escape'=>false,'class'=>'btn btn-xs btn-primary']) ?></li>
                </ul>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped data-table">
                    <thead>
            <tr>
                                <th data-priority="2"><?= $this->Txt->printAdmin(__('Admin | Id')) ?></th>
                                <th data-priority="3"><?= $this->Txt->printAdmin(__('Admin | Nazwa')) ?></th>
                                <th data-priority="4"><?= $this->Txt->printAdmin(__('Admin | Adres')) ?></th>
                                <th data-priority="5"><?= $this->Txt->printAdmin(__('Admin | Email')) ?></th>
                                <th data-priority="6"><?= $this->Txt->printAdmin(__('Admin | Telefon')) ?></th>
                                <th data-priority="7"><?= $this->Txt->printAdmin(__('Admin | Aktywny')) ?></th>
                                <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($punktyOdbioru as $punktyOdbioru): ?>
            <tr>
                <td><?= $punktyOdbioru->id ?></td>
                <td><?= $punktyOdbioru->nazwa ?></td>
                <td><?= nl2br($punktyOdbioru->adres) ?></td>
                <td><?= $punktyOdbioru->email ?></td>
                <td><?= $punktyOdbioru->telefon ?></td>
                <td><?= $this->Txt->printBool($punktyOdbioru->aktywny, str_replace($basePath, '/', Cake\Routing\Router::url(['controller' => 'PunktyOdbioru', 'action' => 'setField', $punktyOdbioru->id, 'aktywny', (!empty($punktyOdbioru->aktywny) ? 0 : 1)]))) ?></td>
                                
                <td class="actions">
                    <?= $this->Html->link('<i class="fa fa-cogs"></i>', ['action' => 'edit', $punktyOdbioru->id],['escape'=>false,'class'=>'btn btn-default btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Edytuj'))]) ?>
                    <?= $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $punktyOdbioru->id], ['confirm-btn-yes'=>$this->Txt->printAdmin(__('Admin | Tak')),'confirm-btn-no'=>$this->Txt->printAdmin(__('Admin | Nie')),'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $punktyOdbioru->id)),'escape'=>false,'class'=>'btn btn-danger btn-xs confirm-link','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Usuń'))]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
                </table>
            </div>
        </div>
    </div>
</div>