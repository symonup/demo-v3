<?php
/**
  * @var \App\View\AppView $this
  */
$kropkaPrzecinek=[','=>$this->Txt->printAdmin(__('Admin | Przecinek')),'.'=>$this->Txt->printAdmin(__('Admin | Kropka'))];
$kropkaPrzecinekOdstep= array_merge($kropkaPrzecinek,[' '=>$this->Txt->printAdmin(__('Admin | Odstęp(spacja)'))]);
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
                <?= $this->Form->create($walutum) ?>
            <div class="x_title">
                    <h2><?=$this->Txt->printAdmin(__('Admin | Add {0}',[__('Admin | Walutum')]))?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                                              <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> '.__('Admin | Wróć do {0}',[__('Admin | Listy')])), ['action' => 'index'],['escape'=>false,'class'=>'btn btn-xs btn-warning']) ?></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
            <div class="x_content">
    <fieldset>
        <?php
            $lngInputs=[];
                        echo $this->Form->control('symbol',['label'=>$this->Txt->printAdmin(__('Admin | Typ waluty')),'options'=>$symbole,'default'=>'PLN']);
            echo $this->Form->control('nazwa',['label'=>$this->Txt->printAdmin(__('Admin | Nazwa'))]);
            echo $this->Form->control('kurs',['type'=>'hidden','value'=>1]);
            echo $this->Form->control('separator_d',['options'=>$kropkaPrzecinek,'default'=>',','label'=>$this->Txt->printAdmin(__('Admin | Separator dziesiętnych'))]);
            echo $this->Form->control('separator_t',['options'=>$kropkaPrzecinekOdstep,'default'=>' ','label'=>$this->Txt->printAdmin(__('Admin | Separator tysięcy'))]);
            echo $this->Form->control('symbol_przed',['label'=>$this->Txt->printAdmin(__('Admin | Wyświetlaj symbol przed kwotą')),'type'=>'checkbox']);
            echo $this->Form->control('space',['label'=>$this->Txt->printAdmin(__('Admin | Odstęp między symbolem a kwotą')),'type'=>'checkbox']);
            echo $this->Form->control('domyslna',['label'=>$this->Txt->printAdmin(__('Admin | Ustaw tą walutę jako domyślną')),'type'=>'checkbox']);
        ?>
    </fieldset>
            </div>
            <div class="x_footer text-right">
    <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')),['class'=>'btn btn-sm btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
