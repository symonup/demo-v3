<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
                <?= $this->Form->create($wiek) ?>
            <div class="x_title">
                    <h2><?=$this->Txt->printAdmin(__('Admin | Edit {0}',[__('Admin | Wiek')]))?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                                <li><?= $this->Html->link(
                $this->Html->Tag('i','',['class'=>'fa fa-trash']).' '.$this->Txt->printAdmin(__('Admin | Usuń')),
                ['action' => 'delete', $wiek->id],
                ['confirm-btn-yes'=>$this->Txt->printAdmin(__('Admin | Tak')),'confirm-btn-no'=>$this->Txt->printAdmin(__('Admin | Nie')),'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $wiek->id)),'class'=>'btn btn-xs btn-danger confirm-link','escape'=>false]
            )?></li>
                      <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> '.__('Admin | Wróć do {0}',[__('Admin | Listy')])), ['action' => 'index'],['escape'=>false,'class'=>'btn btn-xs btn-warning']) ?></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
            <div class="x_content">
    <fieldset>
        <?php
            $lngInputs=[];
        ?>
        <div class="row">
            <div class="col-xs-12 col-md-12"><?=$this->Form->control('nazwa',['label'=>$this->Txt->printAdmin(__('Admin | Nazwa przedziału'))])?></div>
            <div class="col-xs-12 col-md-3"><?=$this->Form->control('miesiace_od',['label'=>$this->Txt->printAdmin(__('Admin | Wiek w miesiącach OD'))])?></div>
            <div class="col-xs-12 col-md-3"><?=$this->Form->control('miesiace_do',['label'=>$this->Txt->printAdmin(__('Admin | Wiek w miesiącach DO'))])?></div>
            <div class="col-xs-12 col-md-3"><?=$this->Form->control('lata_od',['label'=>$this->Txt->printAdmin(__('Admin | Wiek w latach OD'))])?></div>
            <div class="col-xs-12 col-md-3"><?=$this->Form->control('lata_do',['label'=>$this->Txt->printAdmin(__('Admin | Wiek w latach DO'))])?></div>
        </div>
    </fieldset>
            </div>
            <div class="x_footer text-right">
    <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')),['class'=>'btn btn-sm btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
