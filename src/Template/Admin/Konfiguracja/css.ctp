<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
                <?= $this->Form->create(null) ?>
            <div class="x_title">
                    <h2><?=$this->Txt->printAdmin(__('Admin | Edytujesz style strony. Zmiany które tu wprowadzisz mają wpływ na wygląd strony.'))?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                     </ul>
                    <div class="clearfix"></div>
                  </div>
            <div class="x_content">
    <fieldset>
        <?php
                    
                        echo $this->Form->control('tresc',['code-mirror-css'=>'true','type'=>'textarea','value'=>$tresc,'class'=>'form-control','label'=>false]);
                            ?>
    </fieldset>
                
                
            </div>
            <div class="x_footer text-right">
    <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')),['class'=>'btn btn-sm btn-success']) ?>
            </div>
            <?= $this->Form->end() ?><?php
                if(!empty($tips)){
                    echo $this->element('admin/tips',['tips'=>$tips]);
                }
                ?>
        </div>
    </div>
</div>
