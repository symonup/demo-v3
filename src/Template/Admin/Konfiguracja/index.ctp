<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Konfiguracja[]|\Cake\Collection\CollectionInterface $konfiguracja
 */
?>
<div class="row">
    <div class="col-xs-12 col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Konfiguracja')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php
                $cfgArr = [];
                foreach ($konfiguracja as $konfiguracja) {
                    if ($konfiguracja->typ == 'dostawa' || $konfiguracja->typ == 'admin') {
                        continue;
                    }
                    $cfgArr[$konfiguracja->typ][] = $this->element('admin/cfg_tr', ['konfiguracja' => $konfiguracja]);
                }
                ?>
                <div class="panel-group" id="cfg-panel" role="tablist" aria-multiselectable="true">
                    <?php
                    $i = 0;
                    foreach ($cfgArr as $cfgName => $cfgItems) {
                        ?>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="<?= "cfg-head-$cfgName" ?>">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#cfg-panel" href="#<?= "cfg-$cfgName" ?>" aria-expanded="true" aria-controls="<?= "cfg-$cfgName" ?>">
    <?= $this->Txt->printAdmin(__('Admin | cfg-' . $cfgName)) ?> <i class="fa fa-chevron-down"></i>
                                    </a>
                                </h4>
                            </div>
                            <div id="<?= "cfg-$cfgName" ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="<?= "cfg-head-$cfgName" ?>">
                                <div class="panel-body">
                                    <table class="table table-striped data-table">
                                        <thead>
                                            <tr>
                                                <th scope="col"><?= $this->Txt->printAdmin(__('Admin | Id')) ?></th>
                                                <th scope="col"><?= $this->Txt->printAdmin(__('Admin | Nazwa')) ?></th>
                                                <th scope="col"><?= $this->Txt->printAdmin(__('Admin | Wartość')) ?></th>
                                                <th scope="col" class="actions"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
    <?= join('', $cfgItems) ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php
                        $i++;
                    }
                    ?>
                </div>
                <?php
                if (!empty($tips)) {
                    echo $this->element('admin/tips', ['tips' => $tips]);
                }
                ?>
            </div>
        </div>
    </div>