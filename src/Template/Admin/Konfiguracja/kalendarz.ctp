<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                    <h2><?=$this->Txt->printAdmin(__('Admin | Konfiguracja dni w których nie są wysyłane paczki'))?></h2>
                    <div class="clearfix"></div>
                  </div>
            <div class="x_content">
                        <div class="row">
                            <div class="col-lg-12">
                                <h3>Dni wolne od wysyłek</h3>
                            </div>
                        </div>
                <div class="row">
                    <div class="col-md-6">
                        <div>
                        <label class="col-lg-12">Podaj dni w których przesyłki nie są realizowane (święta,urlopy, itp.) Daty jednorazowe należy usunąć po ich przeminięciu.</label>
                        <div class="control-group">
                            <div class="controls">
                              <div class="col-md-5 xdisplay_inputx form-group has-feedback">
                                <input type="text" class="form-control has-feedback-left daypicker blockDayInput" placeholder="<?=date('d-m')?>">
                                <span class="fa fa-calendar form-control-feedback left" aria-hidden="true"></span>
                              </div>
                        <button type="button" class="btn btn-success btn-md saveBlockDay">Zapisz</button>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                            <input type="hidden" id="allBlockedDays" value="<?= Cake\Core\Configure::read('dostawa.block_days')?>"/>
                            <ul class="blockDayContainer">
                        <?php 
                        if(!empty($allBlockDays)){
                            foreach ($allBlockDays as $blockDate => $blockLabel){
                            echo $this->Txt->getBlockDateItem($blockDate,$blockLabel);
                            }
                        }
                        ?>    
                            </ul>
                    </div>
                </div>
                <?=$this->Form->create(null,['class'=>'ajaxForm','url'=>['action'=>'ajaxUpdate']])?>
                <div class="row">
                    <div class="col-xs-12 col-md-3">
                        <label>Godzina wysyłki</label>
                        <input type="hidden" name="typ" value="dostawa"/><input type="hidden" name="nazwa" value="maxTime"/><input autocomplete="off" class="form-control" value="<?=c('dostawa.maxTime')?>" type="text" datepicker="time" name="wartosc"/></div>
                    <div class="col-xs-12 col-md-4">
                        <label style="display: block;">&nbsp;</label>
                        <button type="submit" class="btn btn-success">Zapisz</button></div>
                </div>
                <?=$this->Form->end()?>
            </div>
        </div>
    </div>
</div>