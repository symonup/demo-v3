<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="row">
    <div class="col-xs-12 col-lg-12">
        <div class="x_panel">
            <?= $this->Form->create($konfiguracja) ?>
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Config | Edytujesz wartość {0} w grupie konfiguracji {1}', [__('Config | ' . $konfiguracja->label), $this->Txt->printAdmin(__('Admin | cfg-' . $konfiguracja->typ))]), 'Config | ') ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> ' . __('Admin | Wróć do {0}', [__('Admin | Listy')])), ['action' => 'index'], ['escape' => false, 'class' => 'btn btn-xs btn-warning']) ?></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <fieldset>
                    <?php
                    $cfgControl = ['type' => $konfiguracja->rodzaj, 'label' => $this->Txt->printAdmin(__('Config | ' . $konfiguracja->label))];
                    switch ($konfiguracja->rodzaj) {
                        case 'text' : {
                                
                            } break;
                        case 'textarea' : {
                                if (!empty($konfiguracja->html)) {
                                    $cfgControl['summernote'] = 'true';
                                }
                                if ($konfiguracja->typ == 'script') {
                                    $cfgControl['code-mirror-html'] = 'true';
                                }
                                $cfgControl['class'] = 'form-control';
                            } break;
                        case 'number' : {
                                
                            } break;
                        case 'email' : {
                                
                            } break;
                        case 'checkbox' : {
                                
                            } break;
                        case 'select_multiple' : {
                            $cfgControl['type']='select';
                            $cfgControl['multiple']='checkbox';
                                if (!empty($konfiguracja->options)) {
                                    $tmpOpt = explode('|', $konfiguracja->options);
                                    $options = [];
                                    foreach ($tmpOpt as $optionArr) {
                                        $tmpOption = explode('=', $optionArr);
                                        $options[$tmpOption[0]] = $this->Txt->printAdmin(__('Admin | ' . (key_exists(1, $tmpOption) ? $tmpOption[1] : $tmpOption[0])));
                                    }
                                    $cfgControl['options'] = $options;
                                }
                                if(!empty($konfiguracja->wartosc)){
                                    $cfgControl['value']= explode(',', $konfiguracja->wartosc);
                                }
                            } break;
                        case 'select' : {
                                if (!empty($konfiguracja->options)) {
                                    $tmpOpt = explode('|', $konfiguracja->options);
                                    $options = [];
                                    foreach ($tmpOpt as $optionArr) {
                                        $tmpOption = explode('=', $optionArr);
                                        $options[$tmpOption[0]] = $this->Txt->printAdmin(__('Admin | ' . (key_exists(1, $tmpOption) ? $tmpOption[1] : $tmpOption[0])));
                                    }
                                    $cfgControl['options'] = $options;
                                } else {
                                    if($konfiguracja->typ=='wysylka' && $konfiguracja->nazwa==='krajPodstawowy'){
                                        $cfgControl['options']= \Cake\Core\Configure::read('kraje');
                                    }
//                                if($konfiguracja->nazwa=='znak_wodny_polozenie' && $konfiguracja->typ=='zdjecia'){
//                        $cfgControl['options']=['left_top'=>$this->Txt->printAdmin(__('Admin | Lewy górny róg')),'right_top'=>$this->Txt->printAdmin(__('Admin | Prawy górny róg')),'right_bottom'=>$this->Txt->printAdmin(__('Admin | Prawy dolny róg')),'left_bottom'=>$this->Txt->printAdmin(__('Admin | Lewy dolny róg')),'center'=>$this->Txt->printAdmin(__('Admin | Środek'))];
//                    }
//                    if($konfiguracja->nazwa=='rodzaj' && $konfiguracja->typ=='ceny'){
//                        $cfgControl['options']=['netto'=>$this->Txt->printAdmin(__('Admin | netto')),'brutto'=>$this->Txt->printAdmin(__('Admin | brutto'))];
//                    }
                                }
                            } break;
                    }
                    echo $this->Form->control('wartosc', $cfgControl);
                    ?>
                </fieldset>
            </div>
            <div class="x_footer text-right">
                <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')), ['class' => 'btn btn-sm btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
