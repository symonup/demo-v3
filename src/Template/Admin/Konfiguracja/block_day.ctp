<div class="modal fade" id="block-day-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?= $this->Form->create($blockedDay) ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel2"><?= $this->Txt->printAdmin(__('Admin | Zablokuj dzien {0}',[$data])) ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                <?php
                echo $this->Form->control('data',['type'=>'hidden','value'=>$data]);
                    echo $this->Form->control('przyczyna',['class'=>'form-control','label'=>$this->Txt->printAdmin(__('Admin | Powód blokady')),'required'=>true]);
                    echo $this->Form->control('cyklicznie',['label'=>$this->Txt->printAdmin(__('Admin | Blokuj cyklicznie (co roku)'))]);
                    ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= $this->Txt->printAdmin(__('Admin | Anuluj')) ?></button>
                <button type="submit" class="btn btn-success"><?= (!empty($blockedDay->id)?$this->Txt->printAdmin(__('Admin | Zapisz zmiany')):$this->Txt->printAdmin(__('Admin | Zablokuj dzień'))) ?></button>
                <?php
                if(!empty($blockedDay->id)){
                    ?>
                <button type="button" data="<?=$data?>" class="btn btn-danger unBlockDay" confirm-message="<?= $this->Txt->printAdmin(__('Admin | Czy na pewnio chcesz odblokować dzień {0}?',[$data])) ?>"><?= $this->Txt->printAdmin(__('Admin | Odblokuj dzień')) ?></button>
                <?php
                }
                ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>