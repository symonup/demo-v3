<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
                <?= $this->Form->create($blog,['type'=>'file','class'=>'crop-form','img-max-w'=>640,'img-max-h'=>273]) ?>
            <div class="x_title">
                    <h2><?=$this->Txt->printAdmin(__('Admin | Dodaj {0}',[__('Admin | artykuł')]))?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                                              <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> '.__('Admin | Wróć do {0}',[__('Admin | Listy')])), ['action' => 'index'],['escape'=>false,'class'=>'btn btn-xs btn-warning']) ?></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
            <div class="x_content">
    <fieldset>
        <?php
            $lngInputs=[];
                        echo $this->Form->control('tytul',['label'=>$this->Txt->printAdmin(__('Admin | Tytuł'))]);
            echo $this->Form->control('opis',['label'=>$this->Txt->printAdmin(__('Admin | Opis skrócony')),'class'=>'form-control']);
            echo $this->Form->control('tresc',['label'=>$this->Txt->printAdmin(__('Admin | Pełna treść')),'summernote'=>'true']);
            echo $this->Txt->file($this->Form->control('zdjecie', ['type' => 'file','label'=>false,'templateVars'=>['defWidth'=>'640','defHeight'=>'273','previewClass'=>'crop-img']]),$this->Txt->printAdmin(__('Admin | Zdjęcie')),(!empty($blog->zdjecie)?$displayPath['blog'].$blog->zdjecie:''));
            
            echo $this->Form->control('ukryty',['label'=>$this->Txt->printAdmin(__('Admin | Artykuł niewidoczny'))]);
            ?>
    </fieldset>
            </div>
            <div class="x_footer text-right">
    <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')),['class'=>'btn btn-sm btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
