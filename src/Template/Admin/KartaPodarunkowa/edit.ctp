<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
                <?= $this->Form->create($kartaPodarunkowa,['type'=>'file']) ?>
            <div class="x_title">
                    <h2><?=$this->Txt->printAdmin(__('Admin | Edytuj {0}',[__('Admin | kartę podarunkową')]))?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                                <li><?= $this->Html->link(
                $this->Html->Tag('i','',['class'=>'fa fa-trash']).' '.$this->Txt->printAdmin(__('Admin | Usuń')),
                ['action' => 'delete', $kartaPodarunkowa->id],
                ['confirm-btn-yes'=>$this->Txt->printAdmin(__('Admin | Tak')),'confirm-btn-no'=>$this->Txt->printAdmin(__('Admin | Nie')),'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $kartaPodarunkowa->id)),'class'=>'btn btn-xs btn-danger confirm-link','escape'=>false]
            )?></li>
                      <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> '.__('Admin | Wróć do {0}',[__('Admin | Listy')])), ['action' => 'index'],['escape'=>false,'class'=>'btn btn-xs btn-warning']) ?></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
            <div class="x_content">
    <fieldset>
        <?php
            $lngInputs=[];
                    echo $this->Form->control('nazwa', ['label' => $this->Txt->printAdmin(__('Admin | Nazwa'))]);
                    echo $this->Form->control('wartosc', ['type'=>'text','data-type'=>'float','label' => $this->Txt->printAdmin(__('Admin | Wartość'))]);
                    echo $this->Form->control('cena', ['type'=>'text','data-type'=>'float','label' => $this->Txt->printAdmin(__('Admin | Cena zakupu'))]);
                    echo $this->Txt->file($this->Form->control('zdjecie', ['type' => 'file', 'label' => false]), $this->Txt->printAdmin(__('Admin | Dodaj zdjęcie')), (!empty($kartaPodarunkowa->zdjecie) ? $displayPath['karta_podarunkowa'] . $kartaPodarunkowa->zdjecie : ''));
                    echo $this->Form->control('aktywna', ['label' => $this->Txt->printAdmin(__('Admin | Aktywna'))]);
        ?>
    </fieldset>
            </div>
            <div class="x_footer text-right">
    <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')),['class'=>'btn btn-sm btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
