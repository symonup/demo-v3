<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\KartaPodarunkowa[]|\Cake\Collection\CollectionInterface $kartaPodarunkowa
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Kody kart podarunkowych')) ?></h2><ul class="nav navbar-right panel_toolbox">
                                
                      <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> '.__('Admin | Wróć do {0}',[__('Admin | listy kart')])), ['action' => 'index'],['escape'=>false,'class'=>'btn btn-xs btn-warning']) ?></li>
                    </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped data-table">
                    <thead>
                        <tr>
                            <th data-priority="2"><?= $this->Txt->printAdmin(__('Admin | Id')) ?></th>
                            <th data-priority="2"><?= $this->Txt->printAdmin(__('Admin | Nazwa')) ?></th>
                            <th data-priority="3"><?= $this->Txt->printAdmin(__('Admin | Zamówienie')) ?></th>
                            <th data-priority="5"><?= $this->Txt->printAdmin(__('Admin | Data dodania')) ?></th>
                            <th data-priority="3"><?= $this->Txt->printAdmin(__('Admin | Kod')) ?></th>
                            <th data-priority="4"><?= $this->Txt->printAdmin(__('Admin | Wartość')) ?></th>
                            <th data-priority="6"><?= $this->Txt->printAdmin(__('Admin | Aktywny')) ?></th>
                            <th data-priority="7"><?= $this->Txt->printAdmin(__('Admin | Użyty')) ?></th>
                            <th data-priority="8"><?= $this->Txt->printAdmin(__('Admin | Data użycia')) ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($kody as $kod): ?>
                            <tr>
                                <td><?= $kod->id ?></td>
                                <td><?= ($kod->has('karta_podarunkowa')?$kod->karta_podarunkowa->nazwa:'') ?></td>
                                <td><?= ($kod->has('zamowienie')?$this->Html->link($kod->zamowienie->numer.' ('.(key_exists($kod->zamowienie->status, $zamowienieStatusy)?$zamowienieStatusy[$kod->zamowienie->status]:$kod->zamowienie->status).')',['controller'=>'Zamowienie','action'=>'view',$kod->zamowienie->id]):'') ?></td>
                                <td><?= $kod->data_dodania->format('Y-m-d H:i') ?></td>
                                <td><?= $kod->kod ?></td>
                                <td><?= $this->Txt->cena($kod->wartosc) ?></td>
                                <td><?= $this->Txt->printBool($kod->aktywny, str_replace($basePath, '/', Cake\Routing\Router::url(['controller' => 'KartaPodarunkowa', 'action' => 'setFieldKod', $kod->id, 'aktywny', (!empty($kod->aktywny) ? 0 : 1)])), [$this->Txt->printAdmin('Admin | Aktywuj'), $this->Txt->printAdmin('Admin | Tak')]) ?></td>
                                <td><?= $this->Txt->printBool($kod->uzyty) ?></td>
                                <td><?= (!empty($kod->data_uzycia) ? $kod->data_uzycia->format('Y-m-d H:i') : '') ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>