<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\KartaPodarunkowa[]|\Cake\Collection\CollectionInterface $kartaPodarunkowa
  */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Karty podarunkowe')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Html->tag('i','',['class'=>'fa fa-list']).' '.$this->Txt->printAdmin(__('Admin | Lista wszystkich kodów')), ['action' => 'kodyAll'],['escape'=>false,'class'=>'btn btn-xs btn-info']) ?></li>
                    <li><?= $this->Html->link($this->Html->tag('i','',['class'=>'fa fa-plus']).' '.$this->Txt->printAdmin(__('Admin | Dodaj {0}',[$this->Txt->printAdmin(__('Admin | kartę podarunkową'))])), ['action' => 'add'],['escape'=>false,'class'=>'btn btn-xs btn-primary']) ?></li>
                </ul>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped data-table">
                    <thead>
            <tr>
                                <th data-priority="2"><?= $this->Txt->printAdmin(__('Admin | Id')) ?></th>
                                <th></th>
                                <th data-priority="3"><?= $this->Txt->printAdmin(__('Admin | Nazwa')) ?></th>
                                <th data-priority="4"><?= $this->Txt->printAdmin(__('Admin | Wartość')) ?></th>
                                <th data-priority="4"><?= $this->Txt->printAdmin(__('Admin | Cena')) ?></th>
                                <th data-priority="5"><?= $this->Txt->printAdmin(__('Admin | Data dodania')) ?></th>
                                <th data-priority="6"><?= $this->Txt->printAdmin(__('Admin | Aktywna')) ?></th>
                                <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($kartaPodarunkowa as $kartaPodarunkowa): ?>
            <tr>
                <td><?= $kartaPodarunkowa->id ?></td>
                <td><?=(!empty($kartaPodarunkowa->zdjecie) ? $this->Html->image($displayPath['karta_podarunkowa'] . $kartaPodarunkowa->zdjecie,['style'=>'max-height:50px;']) : '')?></td>
                <td><?= $kartaPodarunkowa->nazwa ?></td>
                <td><?= $this->Txt->cena($kartaPodarunkowa->wartosc) ?></td>
                <td><?= $this->Txt->cena($kartaPodarunkowa->cena) ?></td>
                <td><?= $kartaPodarunkowa->data_dodania ?></td>
                <td><?= $this->Txt->printBool($kartaPodarunkowa->aktywna, str_replace($basePath, '/', Cake\Routing\Router::url(['controller' => 'KartaPodarunkowa', 'action' => 'setField', $kartaPodarunkowa->id, 'aktywna', (!empty($kartaPodarunkowa->aktywna) ? 0 : 1)]))) ?></td>
                                <td class="actions">
                    <?= $this->Html->link('<i class="fa fa-credit-card"></i>', ['action' => 'kody', $kartaPodarunkowa->id], ['escape'=>false,'class'=>'btn btn-default btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Lista kodów'))]) ?>
                    <?= $this->Html->link('<i class="fa fa-cogs"></i>', ['action' => 'edit', $kartaPodarunkowa->id],['escape'=>false,'class'=>'btn btn-default btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Edytuj'))]) ?>
                    <?= $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $kartaPodarunkowa->id], ['confirm-btn-yes'=>$this->Txt->printAdmin(__('Admin | Tak')),'confirm-btn-no'=>$this->Txt->printAdmin(__('Admin | Nie')),'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $kartaPodarunkowa->id)),'escape'=>false,'class'=>'btn btn-danger btn-xs confirm-link','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Usuń'))]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
                </table>
            </div>
        </div>
    </div>
</div>