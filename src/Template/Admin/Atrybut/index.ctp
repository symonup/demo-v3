<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Atrybut[]|\Cake\Collection\CollectionInterface $atrybut
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Atrybut')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-plus']) . ' ' . $this->Txt->printAdmin(__('Admin | Dodaj {0}', [$this->Txt->printAdmin(__('Admin | Atrybut'))])), ['action' => 'add', $typId], ['escape' => false, 'class' => 'btn btn-xs btn-primary']) ?></li>
                    <li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-chevron-left']) . ' ' . $this->Txt->printAdmin(__('Admin | Wróć do {0}', [$this->Txt->printAdmin(__('Admin | Atrybut Typ'))])), ['controller' => 'AtrybutTyp', 'action' => 'index'], ['escape' => false, 'class' => 'btn btn-xs btn-warning']) ?></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create(null, ['url' => ['controller' => 'Atrybut', 'action' => 'index',$typId]]) ?>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th data-priority="2"><?= $this->Txt->printAdmin(__('Admin | id')) ?></th>
                            <th data-priority="3"><?= $this->Paginator->sort('nazwa', $this->Txt->printAdmin(__('Admin | nazwa'))) ?></th>
                            <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
                        </tr>
                        <tr class="table-filters">
                            <td class="short"><?= $this->Form->control('id', ['label' => false, 'type' => 'text', 'value' => ((!empty($filter) && key_exists('id', $filter)) ? $filter['id'] : '')]) ?></td>
                            <td><?= $this->Form->control('nazwa', ['label' => false, 'type' => 'text', 'value' => ((!empty($filter) && key_exists('nazwa', $filter)) ? $filter['nazwa'] : '')]) ?></td>
                            <td class="actions"><button type="submit" data-toggle="tooltip" data-placment="top" data-container="body" title="<?= $this->Txt->printAdmin(__('Admin | Szukaj')) ?>" class="btn btn-xs btn-warning"><i class="fa fa-search"></i></button><?php if (!empty($filter) || key_exists('sort', $_GET)) { ?><button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placment="top" data-container="body" title="<?= $this->Txt->printAdmin(__('Admin | Wyczyść filtr')) ?>" name="clear-filter"><i class="fa fa-remove"></i></button><?php } ?></td>
                        </tr>
                    </thead>
                    <tbody class="sorted-kolejnosc" sort-target="<?= Cake\Routing\Router::url(['action' => 'sort', 'kolejnosc']) ?>">
                        <?php foreach ($atrybut as $atrybut): ?>
                            <tr sort-item-id="<?= $atrybut->id ?>">
                                <td><?= $atrybut->id ?></td>
                                <td><?= h($atrybut->nazwa) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link('<i class="fa fa-cogs"></i>', ['action' => 'edit', $atrybut->id], ['escape' => false, 'class' => 'btn btn-default btn-xs', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $this->Txt->printAdmin(__('Admin | Edytuj'))]) ?>
                                    <?= $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $atrybut->id], ['confirm-btn-yes' => $this->Txt->printAdmin(__('Admin | Tak')), 'confirm-btn-no' => $this->Txt->printAdmin(__('Admin | Nie')), 'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $atrybut->nazwa)), 'escape' => false, 'class' => 'btn btn-danger btn-xs confirm-link', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $this->Txt->printAdmin(__('Admin | Usuń'))]) ?>
                                    <?php if (!key_exists('sort', $_GET) && empty($filter)) { ?>
                            <spna class="sort-item-point btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="<?= $this->Txt->printAdmin(__('Admin | Przeciągnij aby zmienić kolejność')) ?>"><i class="fa fa-arrows-alt"></i></spna>
                            <?php } ?>
                        </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <?= $this->element('admin/paginator') ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>