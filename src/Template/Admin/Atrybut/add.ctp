<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
                <?= $this->Form->create($atrybut) ?>
            <div class="x_title">
                    <h2><?=$this->Txt->printAdmin(__('Admin | Add {0}',[__('Admin | Atrybut')]))?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                                              <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> '.__('Admin | Wróć do {0}',[__('Admin | Listy')])), ['action' => 'index',$typId],['escape'=>false,'class'=>'btn btn-xs btn-warning']) ?></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
            <div class="x_content">
    <fieldset>
        <?php
            $lngInputs=[];
                        echo $this->Form->control('atrybut_typ_id', ['options' => $atrybutTyp,'default'=>$typId,'label'=>$this->Txt->printAdmin(__('Admin | Typ atrybutu'))]);
                    $lngInputs['nazwa']=['label'=>$this->Txt->printAdmin(__('Admin | Nazwa - {0}'))];
                    $lngInputs['pod_tytul']=['label'=>$this->Txt->printAdmin(__('Admin | Pod tytuł jeżeli atrybut posiada dodatkowe wartości - {0}'))];
                                echo $this->Translation->inputs($this->Form, $languages, $lngInputs);
                        echo $this->Form->control('pole',['type'=>'checkbox','label'=>$this->Txt->printAdmin(__('Admin | Wartość liczbowa'))]);
        ?>
        <div class="">
        <input type="text" value="" name="atrybut_podrzedne[][nazwa]" disabled="disabled" class="form-control subAttrValDef"/>
            <h4><?=$this->Txt->printAdmin(__('Admin | Wartości podrzędne'))?></h4>
            <div id="wartosci_podrzedne">
                <?php 
                if(!empty($atrybut->atrybut_podrzedne)){
                    foreach ($atrybut->atrybut_podrzedne as $attrSub){
                        ?>
                <div class="subAttrItem">
                <input type="hidden" value="<?=$attrSub->id?>" name="atrybut_podrzedne[<?=$attrSub->id?>][id]"/>
                <input type="text" value="<?=$attrSub->nazwa?>" name="atrybut_podrzedne[<?=$attrSub->id?>][nazwa]" class="form-control"/>
                </div>
                <?php
                    }
                }
                ?>
            </div>
            <button class="btn btn-primary btn-sm attrSubAdd" type="button"><i class="glyphicon glyphicon-plus"></i> <?=$this->Txt->printAdmin(__('Admin | Dodaj wartość'))?></button>
        </div>
    </fieldset>
            </div>
            <div class="x_footer text-right">
    <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')),['class'=>'btn btn-sm btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
