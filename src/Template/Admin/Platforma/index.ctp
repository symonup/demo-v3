<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Platforma[]|\Cake\Collection\CollectionInterface $platforma
  */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Platforma')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Html->tag('i','',['class'=>'fa fa-plus']).' '.$this->Txt->printAdmin(__('Admin | Dodaj {0}',[$this->Txt->printAdmin(__('Admin | Platforma'))])), ['action' => 'add'],['escape'=>false,'class'=>'btn btn-xs btn-primary']) ?></li>
                </ul>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped data-table">
                    <thead>
            <tr>
                                <th data-priority="2"><?= $this->Txt->printAdmin(__('Admin | id')) ?></th>
                                <th data-priority="3"><?= $this->Txt->printAdmin(__('Admin | Nazwa')) ?></th>
                                <th data-priority="4"><?= $this->Txt->printAdmin(__('Admin | Logo')) ?></th>
                                <th data-priority="4"><?= $this->Txt->printAdmin(__('Admin | Logo alternatywne')) ?></th>
                                <th data-priority="4"><?= $this->Txt->printAdmin(__('Admin | Preorder')) ?></th>
                                <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($platforma as $platforma): ?>
            <tr>
                <td><?= $platforma->id ?></td>
                <td><?= $platforma->nazwa ?></td>
                <td><?= (!empty($platforma->logo)?$this->Html->image($displayPath['platforma'].$platforma->logo):'') ?></td>
                <td><?= (!empty($platforma->logo_2)?$this->Html->image($displayPath['platforma'].$platforma->logo_2):'') ?></td>
                <td><?= $this->Txt->printBool($platforma->preorder, str_replace($basePath, '/', Cake\Routing\Router::url(['controller' => 'Platforma', 'action' => 'setField', $platforma->id, 'preorder', (!empty($platforma->preorder) ? 0 : 1)]))) ?></td>
                                
                <td class="actions">
                    <?= $this->Html->link('<i class="fa fa-cogs"></i>', ['action' => 'edit', $platforma->id],['escape'=>false,'class'=>'btn btn-default btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Edytuj'))]) ?>
                    <?= $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $platforma->id], ['confirm-btn-yes'=>$this->Txt->printAdmin(__('Admin | Tak')),'confirm-btn-no'=>$this->Txt->printAdmin(__('Admin | Nie')),'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $platforma->id)),'escape'=>false,'class'=>'btn btn-danger btn-xs confirm-link','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Usuń'))]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
                </table>
            </div>
        </div>
    </div>
</div>