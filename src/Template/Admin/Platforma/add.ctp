<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
                <?= $this->Form->create($platforma,['type'=>'file']) ?>
            <div class="x_title">
                    <h2><?=$this->Txt->printAdmin(__('Admin | Add {0}',[__('Admin | Platforma')]))?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                                              <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> '.__('Admin | Wróć do {0}',[__('Admin | Listy')])), ['action' => 'index'],['escape'=>false,'class'=>'btn btn-xs btn-warning']) ?></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
            <div class="x_content">
    <fieldset>
        <?php
            $lngInputs=[];
                        echo $this->Form->control('nazwa',['label'=>$this->Txt->printAdmin(__('Admin | nazwa'))]);
            echo $this->Txt->file($this->Form->control('logo', ['type' => 'file', 'label' => false]), $this->Txt->printAdmin(__('Admin | Dodaj logo')).$this->Txt->helpInfo('platforma','logo'), (!empty($platforma->logo) ? $displayPath['platforma'] . $platforma->logo : ''));
           echo $this->Txt->file($this->Form->control('logo_2', ['type' => 'file', 'label' => false]), $this->Txt->printAdmin(__('Admin | Dodaj logo alternatywne (widoczne np. w ofercie Hot Deal)')).$this->Txt->helpInfo('platforma','logo_2'), (!empty($platforma->logo_2) ? $displayPath['platforma'] . $platforma->logo_2 : ''));
                 echo $this->Form->control('preorder', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Uwzględniaj w menu PREORDER'))]);    
        ?>
    </fieldset>
            </div>
            <div class="x_footer text-right">
    <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')),['class'=>'btn btn-sm btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
