<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Edytuj {0}', [__('Admin | link w stopce')])) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?=
                        $this->Form->postLink(
                                '<i class="fa fa-trash"></i> ' . __('Usuń'), ['action' => 'delete', $footerLink->id], ['confirm' => __('Are you sure you want to delete # {0}?', $footerLink->label), 'class' => 'btn btn-danger btn-xs', 'escape' => false]
                        )
                        ?></li>
                    <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> ' . __('Admin | Wróć do {0}', [__('Admin | Listy')])), ['action' => 'index'], ['escape' => false, 'class' => 'btn btn-xs btn-warning']) ?></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($footerLink) ?>
                <fieldset>
                    <?php
                    if (!empty($footerLink->strona)) {
                        echo $this->Form->control('strona_id', ['type' => 'hidden']);
                    }
                    echo $this->Form->control('grupa', ['type' => 'select', 'options' => $fGroups, 'label' => $this->Txt->printAdmin(__('Admin | Grupa'))]);
                    echo $this->Form->control('url', ['disabled' => !empty($footerLink->strona), 'label' => $this->Txt->printAdmin(__('Admin | Adres url'))]);
                    echo $this->Form->control('label', ['disabled' => !empty($footerLink->strona), 'label' => $this->Txt->printAdmin(__('Admin | Nazwa wyświetlana'))]);
                    echo $this->Form->control('title', ['disabled' => !empty($footerLink->strona), 'label' => $this->Txt->printAdmin(__('Admin | Tytuł linku'))]);
                    echo $this->Form->control('target', ['label' => $this->Txt->printAdmin(__('Admin | Wyświetlaj w')), 'options' => $linkTarget, 'default' => '_self']);
                    echo $this->Form->control('nofollow', ['label' => $this->Txt->printAdmin(__('Admin | Dodaj atrybuy rel="nofollow"')), 'type' => 'checkbox']);
                    ?>
                </fieldset>
                    <?= $this->Form->submit($this->Txt->printAdmin(__('Admin | Zapisz')), ['class' => 'btn btn-sm btn-success']) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>