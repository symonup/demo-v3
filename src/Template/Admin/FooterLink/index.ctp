<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Dodatkowe linki w stopce')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-plus"></i> ' . __('Admin | Dodaj {0}', [__('Admin | link')])), ['action' => 'add'], ['escape' => false, 'class' => 'btn btn-xs btn-primary']) ?></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('url', $this->Txt->printAdmin(__('Admin | Adres url'))) ?></th>
                            <th scope="col"><?= $this->Paginator->sort('label', $this->Txt->printAdmin(__('Admin | Nazwa wyświetlana'))) ?></th>
                            <th scope="col"><?= $this->Paginator->sort('title', $this->Txt->printAdmin(__('Admin | Tytuł linku'))) ?></th>
                            <th scope="col"><?= $this->Paginator->sort('target', $this->Txt->printAdmin(__('Admin | Wyświetlaj w'))) ?></th>
                            <th scope="col"><?= $this->Paginator->sort('target', $this->Txt->printAdmin(__('Admin | Grupa'))) ?></th>
                            <th scope="col" class="actions"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody class="sorted-kolejnosc" sort-target="<?= Cake\Routing\Router::url(['action' => 'sort', 'kolejnosc']) ?>">
                        <?php foreach ($footerLink as $footerLink): ?>
                            <tr sort-item-id="<?= $footerLink->id ?>">
                                <td><?= $footerLink->id ?></td>
                                <td><?= (!empty($footerLink->strona) ? $this->Txt->printAdmin(__('Admin | Link do strony ({0}): {1}', [$this->Html->link($footerLink->strona->id, ['controller' => 'Strona', 'action' => 'edit', $footerLink->strona->id]), $footerLink->strona->nazwa])) : $footerLink->url) ?></td>
                                <td><?= h($footerLink->label) ?></td>
                                <td><?= h($footerLink->title) ?></td>
                                <td><?= $linkTarget[$footerLink->target] ?></td>
                                <td><?= $fGroups[$footerLink->grupa] ?></td>
                                <td class="actions">
                                    <?php if (!key_exists('sort', $_GET) && empty($filter)) { ?>
                            <spna class="sort-item-point btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="<?= $this->Txt->printAdmin(__('Admin | Przeciągnij aby zmienić kolejność')) ?>"><i class="fa fa-arrows-alt"></i></spna>
                            <?php } ?>
                            <?= $this->Html->link('<i class="fa fa-cogs"></i>', ['action' => 'edit', $footerLink->id], ['escape' => false, 'class' => 'btn btn-default btn-xs', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $this->Txt->printAdmin(__('Admin | Edytuj {0}', [$this->Txt->printAdmin(__('Admin | link'))]))]) ?>
                            <?= $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $footerLink->id], ['confirm-btn-yes' => $this->Txt->printAdmin(__('Admin | Tak')), 'confirm-btn-no' => $this->Txt->printAdmin(__('Admin | Nie')), 'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', strip_tags($footerLink->label))), 'escape' => false, 'class' => 'confirm-link btn btn-xs btn-danger', 'data-toggle' => 'tooltip', 'title' => $this->Txt->printAdmin(__('Admin | Usuń'))]) ?>
                        </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <?php
                if (!empty($tips)) {
                    echo $this->element('admin/tips', ['tips' => $tips]);
                }
                ?>
            </div>
        </div>
    </div>
</div>
