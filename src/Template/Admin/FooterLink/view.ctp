<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\FooterLink $footerLink
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Footer Link'), ['action' => 'edit', $footerLink->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Footer Link'), ['action' => 'delete', $footerLink->id], ['confirm' => __('Are you sure you want to delete # {0}?', $footerLink->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Footer Link'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Footer Link'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="footerLink view large-9 medium-8 columns content">
    <h3><?= h($footerLink->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Url') ?></th>
            <td><?= h($footerLink->url) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Label') ?></th>
            <td><?= h($footerLink->label) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($footerLink->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Target') ?></th>
            <td><?= h($footerLink->target) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($footerLink->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Kolejnosc') ?></th>
            <td><?= $this->Number->format($footerLink->kolejnosc) ?></td>
        </tr>
    </table>
</div>
