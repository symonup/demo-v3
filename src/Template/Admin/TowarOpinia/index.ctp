<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\TowarOpinium[]|\Cake\Collection\CollectionInterface $towarOpinia
  */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Towar Opinia')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Html->tag('i','',['class'=>'fa fa-plus']).' '.$this->Txt->printAdmin(__('Admin | Dodaj {0}',[$this->Txt->printAdmin(__('Admin | Towar Opinium'))])), ['action' => 'add'],['escape'=>false,'class'=>'btn btn-xs btn-primary']) ?></li>
                </ul>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped data-table">
                    <thead>
            <tr>
                                <th data-priority="2"><?= $this->Txt->printAdmin(__('Admin | id')) ?></th>
                                <th data-priority="3"><?= $this->Txt->printAdmin(__('Admin | Towar')) ?></th>
                                <th data-priority="4"><?= $this->Txt->printAdmin(__('Admin | Data dodania')) ?></th>
                                <th data-priority="5"><?= $this->Txt->printAdmin(__('Admin | Autor')) ?></th>
                                <th data-priority="6"><?= $this->Txt->printAdmin(__('Admin | Ocena')) ?></th>
                                <th data-priority="7"><?= $this->Txt->printAdmin(__('Admin | Opinia')) ?></th>
                                <th data-priority="8"><?= $this->Txt->printAdmin(__('Admin | Zaakceptowana')) ?></th>
                                <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($towarOpinia as $towarOpinium): ?>
            <tr>
                <td><?= $towarOpinium->id ?></td>
                <td><?= $towarOpinium->has('towar') ? $towarOpinium->towar->nazwa : '' ?></td>
                <td><?= (!empty($towarOpinium->data)?$towarOpinium->data->format('Y-m-d H:i:s'):'') ?></td>
                <td><?= $towarOpinium->autor ?></td>
                <td><?= $towarOpinium->ocena ?></td>
                <td><?= addslashes($towarOpinium->opinia) ?></td>
                <td>
                <?=$this->Txt->printBool((($towarOpinium->status==2)?1:0), str_replace($basePath, '/', \Cake\Routing\Router::url(['action' => 'setField', $towarOpinium->id, 'status', (($towarOpinium->status==2)?1:2)])))?>
                </td>
                <td class="actions">
                    <?= $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $towarOpinium->id], ['confirm-btn-yes'=>$this->Txt->printAdmin(__('Admin | Tak')),'confirm-btn-no'=>$this->Txt->printAdmin(__('Admin | Nie')),'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $towarOpinium->id)),'escape'=>false,'class'=>'btn btn-danger btn-xs confirm-link','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Usuń'))]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
                </table>
            </div>
        </div>
    </div>
</div>