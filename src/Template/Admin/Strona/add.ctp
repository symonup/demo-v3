<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Dodaj {0}', [$this->Txt->printAdmin('Admin | stronę')])) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> ' . __('Admin | Wróć do {0}', [__('Admin | Listy')])), ['action' => 'index'], ['escape' => false, 'class' => 'btn btn-xs btn-warning']) ?></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="alert alert-info">
                    <p><?= $this->Txt->printAdmin(__('Admin | Zawartość strony tworzy się za pomocą bloków. Można kożystać z banerów, bloków i tworzyć nowe elementy HTML. Aby dodać nowy element do strony kliknij w guzik "Dodaj nowy blok" i wybierz rodzaj elementu jaki chcesz dodać.')) ?></p>
                    <p><?= $this->Txt->printAdmin(__('Admin | Wygląd strony możesz zobaczyć klikając w podgląd. Pamiętaj że kliknięcie w podgląd nie zapisuje zmian na stałe, pokazuje jedynie jak strona będzie wyglądać po zapisaniu zmian.')) ?></p>
                    <p class="text-center" style="font-size: 15px;"><b><?= $this->Txt->printAdmin(__('Admin | Pamiętaj o zapisaniu strony po dokonaniu zmian!')) ?></b></p>
                </div>
                <?= $this->Form->create($strona, ['class' => 'pageForm']) ?>
                <fieldset>
                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#ustawienia" id="ustawienia-tab" role="tab" data-toggle="tab" aria-expanded="true"><?= $this->Txt->printAdmin(__('Admin | Ustawienia')) ?></a></li>
                            <li role="presentation" class=""><a href="#zawartosc" role="tab" id="zawartosc-tab" data-toggle="tab" aria-expanded="false"><?= $this->Txt->printAdmin(__('Admin | Edycja zawartości strony')) ?></a></li>
                            <li class=""><a href="#" class="page-preview"><?= $this->Txt->printAdmin(__('Admin | Podgląd')) ?></a></li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="ustawienia" aria-labelledby="ustawienia-tab">
                                <?php
                                echo $this->Form->control('locale', ['options' => $languages, 'label' => $this->Txt->printAdmin(__('Admin | Język'))]);
                                echo $this->Form->control('nazwa', ['label' => $this->Txt->printAdmin(__('Admin | Nazwa'))]);
                                unset($pageTypes['type_home']);
                                echo $this->Form->control('type', ['label' => $this->Txt->printAdmin(__('Admin | Type')), 'options' => $pageTypes, 'default' => 'type_defalut']);
                                echo $this->Form->control('menu_dolne', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Wyświetl link w stopce'))]);
                                echo $this->Form->control('menu_dolne_grupa', ['type' => 'select', 'options' => $fGroups, 'label' => $this->Txt->printAdmin(__('Admin | Grupa w stopce'))]);
                                echo $this->Form->control('ukryta', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Strona nie widoczna'))]);
                                echo $this->Form->control('seo_tytul', ['label' => $this->Txt->printAdmin(__('Admin | Tytuł seo'))]);
                                echo $this->Form->control('seo_slowa', ['label' => $this->Txt->printAdmin(__('Admin | Słowa kluczowe seo'))]);
                                echo $this->Form->control('seo_opis', ['label' => $this->Txt->printAdmin(__('Admin | Opis seo')), 'class' => 'form-control']);
                                echo $this->Form->control('tresc', ['type' => 'hidden', 'label' => $this->Txt->printAdmin(__('Admin | Treść')), 'class' => 'form-control']);
                                ?>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="zawartosc" aria-labelledby="zawartosc-tab">
                                <?php
                                $tresc = $strona->tresc;
                                foreach ($blocks as $blockId => $blockItem) {
                                    $tresc = str_replace($blockId, $blockItem, $tresc);
                                }
                                echo $this->Html->tag('div', $tresc, ['class' => 'page-edit-content']);
                                ?>
                                <div class="text-right">
                                    <button type="button" class="btn btn-primary btn-sm add-new-page-blok"><i class="fa fa-plus"></i> <?= $this->Txt->printAdmin(__('Admin | Dodaj nowy blok')) ?></button>
                                </div>
                            </div>
                        </div>
                    </div>

                </fieldset>
                <?= $this->Form->submit($this->Txt->printAdmin(__('Admin | Zapisz')), ['class' => 'btn btn-sm btn-success']) ?>
                <?= $this->Form->end() ?>
                <form id="previewPage" method="post" action="<?= \Cake\Routing\Router::url(['controller' => 'Pages', 'action' => 'preview', 'prefix' => false]) ?>" target="_blank" style="display: none;">
                    <textarea name="html" id="prevwiew-tresc"></textarea>
                </form>
            </div>
        </div>
    </div>
</div>
