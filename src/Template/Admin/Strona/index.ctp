<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                    <h2><?=$this->Txt->printAdmin(__('Admin | Strony'))?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-plus"></i> '.__('Admin | Dodaj {0}',[__('Admin | stronę')])), ['action' => 'add'],['escape'=>false,'class'=>'btn btn-xs btn-primary']) ?></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
            <div class="x_content">
                <table class="table table-striped data-table">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id',$this->Txt->printAdmin(__('Admin | Id'))) ?></th>
                <th scope="col"><?= $this->Paginator->sort('ukryta',$this->Txt->printAdmin(__('Admin | Niedostępna'))) ?></th>
                <th scope="col"><?= $this->Paginator->sort('nazwa',$this->Txt->printAdmin(__('Admin | Nazwa'))) ?></th>
                <th scope="col"><?= $this->Paginator->sort('type',$this->Txt->printAdmin(__('Admin | Type'))) ?></th>
                <th scope="col"><?= $this->Paginator->sort('locale',$this->Txt->printAdmin(__('Admin | Język'))) ?></th>
                <th scope="col" class="actions"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($strona as $strona): ?>
            <tr>
                <td><?= $strona->id ?></td>
                <td><?= $this->Txt->printBool($strona->ukryta) ?></td>
                <td><?= h($strona->nazwa) ?></td>
                <td><?= $pageTypes[$strona->type] ?></td>
                <td><?= (!empty($languages[$strona->locale])?$languages[$strona->locale]:$strona->locale) ?></td>
                <td class="actions">
    <?= $this->Html->link('<i class="fa fa-cogs"></i>', ['action' => 'edit', $strona->id],['escape'=>false,'class'=>'btn btn-default btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Edytuj {0}',[$this->Txt->printAdmin(__('Admin | stronę'))]))]) ?>
                    <?php 
                    if($strona->type!='type_home') { 
                        echo $this->Html->link('<i class="fa fa-copy"></i>', ['action' => 'add', $strona->id],['escape'=>false,'class'=>'btn btn-default btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Kopiuj {0}',[$this->Txt->printAdmin(__('Admin | stronę'))]))]);
                        echo $this->Form->postLink('<i class="fa fa-trash"></i>', ['action' => 'delete', $strona->id], ['confirm' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $strona->nazwa)),'escape'=>false,'class'=>'btn btn-danger btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Usuń {0}',[$this->Txt->printAdmin(__('Admin | stronę'))]))]); 
                    } ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
                <?php
                if(!empty($tips)){
                    echo $this->element('admin/tips',['tips'=>$tips]);
                }
                ?>
            </div>
        </div>
    </div>
</div>