<div id="blok-list-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3><?= $this->Txt->printAdmin(__('Admin | Wybierz blok jaki chesz dodać do strony.')) ?></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                   <div class="select-blok-item"><div class="page-element html-item">HTML<div class="item-controls"><button type="button" data-toggle="tooltip" data-placment="top" title="Edytuj blok" class="btn btn-xs btn-info item-edit"><i class="fa fa-pencil"></i></button><button type="button" data-toggle="tooltip" data-placment="top" title="Usuń blok" class="btn btn-xs btn-danger item-delete"><i class="fa fa-trash"></i></button><span data-toggle="tooltip" data-placment="top" title="Przesuń blok" class="btn btn-xs btn-default item-move"><i class="fa fa-arrows-alt"></i></span></div><div class="item-content"><textarea></textarea></div></div></div>
                <?php
                foreach ($blocks as $blok) {
                    ?>
                    <div class="select-blok-item"><?= $blok ?></div>
                    <?php
                }
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default btn-sm"><?= $this->Txt->printAdmin(__('Admin | Anuluj'))?></button>
                <button type="button" class="btn btn-success btn-sm" id="select-blok-item"><?= $this->Txt->printAdmin(__('Admin | Wybierz'))?></button>
            </div>
        </div>
    </div>
</div>