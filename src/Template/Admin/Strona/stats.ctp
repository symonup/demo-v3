<div class="row">
    <div class="col-lg-12">
        <h3><?=$this->Txt->printAdmin(__('Admin | Statystyki odwiedzin'))?></h3>
    </div>
</div>
<div class="row tile_count">
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><?=$this->Txt->printAdmin(__('Admin | Wszystich odwiedzin'))?></span>
        <div class="count"><?=$all?></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><?=$this->Txt->printAdmin(__('Admin | W tym miesiącu'))?></span>
        <div class="count"><?=$month?></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><?=$this->Txt->printAdmin(__('Admin | z 7 dni'))?></span>
        <div class="count"><?=$sevenDays?></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><?=$this->Txt->printAdmin(__('Admin | Wczoraj'))?></span>
        <div class="count"><?=$yesterday?></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><?=$this->Txt->printAdmin(__('Admin | Dziś'))?></span>
        <div class="count"><?=$today?></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><?=$this->Txt->printAdmin(__('Admin | Ostatnia godzina'))?></span>
        <div class="count <?=(($now<1)?'red':(($now<5)?'blue':'green'))?>"><?=$now?></div>
    </div>
</div>
<?php 
if(!empty($tips)){
    echo $this->element('admin/tips');
}
?>