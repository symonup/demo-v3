<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
                <?= $this->Form->create($rodzajPlatnosci) ?>
            <div class="x_title">
                    <h2><?=$this->Txt->printAdmin(__('Admin | Edit {0}',[__('Admin | Rodzaj Platnosci')]))?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                                <li><?= $this->Html->link(
                $this->Html->Tag('i','',['class'=>'fa fa-trash']).' '.$this->Txt->printAdmin(__('Admin | Usuń')),
                ['action' => 'delete', $rodzajPlatnosci->id],
                ['confirm-btn-yes'=>$this->Txt->printAdmin(__('Admin | Tak')),'confirm-btn-no'=>$this->Txt->printAdmin(__('Admin | Nie')),'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $rodzajPlatnosci->id)),'class'=>'btn btn-xs btn-danger confirm-link','escape'=>false]
            )?></li>
                      <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> '.__('Admin | Wróć do {0}',[__('Admin | Listy')])), ['action' => 'index'],['escape'=>false,'class'=>'btn btn-xs btn-warning']) ?></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
            <div class="x_content">
    <fieldset>
        <?php
            $lngInputs=[];
            $lngInputs['nazwa'] = ['label' => $this->Txt->printAdmin(__('Admin | Nazwa - {0}'))];
            echo $this->Translation->inputs($this->Form, $languages, $lngInputs);
            echo $this->element('admin/konfig_platnosc/' . $rodzajPlatnosci->id);
            echo $this->Form->control('aktywna',['label'=>$this->Txt->printAdmin(__('Admin | Aktywna'))]);
            echo $this->Form->input('disabled_kraj',['type'=>'checkbox','label'=>__('Nie dostępna dla innych krajów niż {0}',[\Cake\Core\Configure::read('wysylka.krajPodstawowy')])]);
            echo $this->Form->control('auto_submit',['type'=>'checkbox','label'=>$this->Txt->printAdmin(__('Admin | Automatyczne przekierowanie do płatności'))]);
            echo $this->Form->control('koszt_dodatkowy',['type'=>'checkbox','label'=>$this->Txt->printAdmin(__('Admin | Uwzględniaj koszt dodatkowy'))]);
//            echo $this->Form->control('prowizja',['label'=>$this->Txt->printAdmin(__('Admin | Prowizja %'))]);
//            echo $this->Form->control('darmowa_wysylka',['label'=>$this->Txt->printAdmin(__('Admin | Darmowa wysyłka od (dla nieaktywnej wpisz 0)'))]);
            echo $this->Form->control('ikona',['type'=>'radio','label'=>$this->Txt->printAdmin(__('Admin | Ikona')),'escape'=>false,'options'=>['payu.png'=>$this->Html->image($displayPath['img'].'platnosci/payu.png'),'monety.png'=>$this->Html->image($displayPath['img'].'platnosci/monety.png'),'bank.png'=>$this->Html->image($displayPath['img'].'platnosci/bank.png'),'dlon.png'=>$this->Html->image($displayPath['img'].'platnosci/dlon.png')]]);
       echo $this->Form->control('test',['type'=>'checkbox','label'=>$this->Txt->printAdmin(__('Admin | Włącz tryb testowy'))]);
            ?>
    </fieldset>
            </div>
            <div class="x_footer text-right">
    <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')),['class'=>'btn btn-sm btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
