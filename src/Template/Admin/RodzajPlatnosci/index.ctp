<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\RodzajPlatnosci[]|\Cake\Collection\CollectionInterface $rodzajPlatnosci
  */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Rodzaj Platnosci')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                </ul>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped">
                    <thead>
            <tr>
                                <th data-priority="2"><?= $this->Txt->printAdmin(__('Admin | id')) ?></th>
                                <th data-priority="3"><?= $this->Txt->printAdmin(__('Admin | Nazwa')) ?></th>
                                <th data-priority="5"><?= $this->Txt->printAdmin(__('Admin | Płatność elektroniczna')) ?></th>
                                <th data-priority="6"><?= $this->Txt->printAdmin(__('Admin | Pobranie')) ?></th>
                                <th data-priority="12"><?= $this->Txt->printAdmin(__('Admin | Aktywna')) ?></th>
                                <th data-priority="13"><?= $this->Txt->printAdmin(__('Admin | Koszt dodatkowy')) ?></th>
                                <th data-priority="14"><?= $this->Txt->printAdmin(__('Admin | Prowizja')) ?></th>
                                <th data-priority="15"><?= $this->Txt->printAdmin(__('Admin | Darmowa wysyłka od')) ?></th>
                                <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
            </tr>
        </thead>
        <tbody class="sorted-kolejnosc" sort-target="<?= Cake\Routing\Router::url(['action'=>'sort','kolejnosc'])?>">
            <?php foreach ($rodzajPlatnosci as $rodzajPlatnosci): ?>
            <tr sort-item-id="<?= $rodzajPlatnosci->id ?>">
                <td><?= $rodzajPlatnosci->id ?></td>
                <td><?= $rodzajPlatnosci->nazwa ?></td>
                <td><?= $this->Txt->printBool($rodzajPlatnosci->platnosc_elektroniczna) ?></td>
                <td><?= $this->Txt->printBool($rodzajPlatnosci->pobranie) ?></td>
                <td><?= $this->Txt->printBool($rodzajPlatnosci->aktywna) ?></td>
                <td><?= $this->Txt->printBool($rodzajPlatnosci->koszt_dodatkowy) ?></td>
                <td><?= $rodzajPlatnosci->prowizja ?></td>
                <td><?= $rodzajPlatnosci->darmowa_wysylka ?></td>
                <td class="actions">
                    <?= $this->Html->link('<i class="fa fa-cogs"></i>', ['action' => 'edit', $rodzajPlatnosci->id],['escape'=>false,'class'=>'btn btn-default btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Edytuj'))]) ?>
                <spna class="sort-item-point btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="<?=$this->Txt->printAdmin(__('Admin | Przeciągnij aby zmienić kolejność'))?>"><i class="fa fa-arrows-alt"></i></spna>
                 </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
                </table>
            </div>
        </div>
    </div>
</div>