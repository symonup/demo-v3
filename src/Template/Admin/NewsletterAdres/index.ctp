<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\NewsletterAdre[]|\Cake\Collection\CollectionInterface $newsletterAdres
  */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Newsletter Adres')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li>
                        <?=$this->Form->create(null,['type'=>'file','id'=>'form-import','url'=> \Cake\Routing\Router::url(['action'=>'import'])])?>
                        <div class="file-container">
                            <input type="file" id="import-plik" onchange="$('#form-import').submit();" name="plik" style="display: none;"/>
                            <button type="button" class="btn btn-xs btn-primary" onclick="$('#import-plik').trigger('click');"><i class="fa fa-upload"></i> <?=$this->Txt->printAdmin(__('Admin | Importuj z csv'))?></button>
                        </div>
                        <?=$this->Form->end()?>
                    </li>
               </ul>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped data-table">
                    <thead>
            <tr>
                                <th data-priority="2"><?= $this->Txt->printAdmin(__('Admin | Id')) ?></th>
                                <th data-priority="3"><?= $this->Txt->printAdmin(__('Admin | E-mail')) ?></th>
                                <th data-priority="4"><?= $this->Txt->printAdmin(__('Admin | Potwierdzony')) ?></th>
                                <th data-priority="6"><?= $this->Txt->printAdmin(__('Admin | Data dodania')) ?></th>
                                <th data-priority="5"><?= $this->Txt->printAdmin(__('Admin | Token')) ?></th>
                                <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($newsletterAdres as $newsletterAdre): ?>
            <tr>
                <td><?= $newsletterAdre->id ?></td>
                <td><?= $newsletterAdre->email ?></td>
                <td><?= $this->Txt->printBool($newsletterAdre->potwierdzony) ?></td>
                <td><?= (!empty($newsletterAdre->data)?$newsletterAdre->data->format('Y-m-d'):'') ?></td>
                <td><?= $newsletterAdre->token ?></td>
                <td class="actions">
                    <?= $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $newsletterAdre->id], ['confirm-btn-yes'=>$this->Txt->printAdmin(__('Admin | Tak')),'confirm-btn-no'=>$this->Txt->printAdmin(__('Admin | Nie')),'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $newsletterAdre->id)),'escape'=>false,'class'=>'btn btn-danger btn-xs confirm-link','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Usuń'))]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
                </table>
                <?php
                if(!empty($tips)){
                    echo $this->element('admin/tips',['tips'=>$tips]);
                }
                ?>
            </div>
        </div>
    </div>
</div>