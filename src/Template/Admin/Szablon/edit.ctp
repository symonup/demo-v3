<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
                <?= $this->Form->create($szablon) ?>
            <div class="x_title">
                    <h2><?=$this->Txt->printAdmin(__('Admin | Edit {0}',[__('Admin | Szablon')]))?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                                <li><?= $this->Html->link(
                $this->Html->Tag('i','',['class'=>'fa fa-trash']).' '.$this->Txt->printAdmin(__('Admin | Usuń')),
                ['action' => 'delete', $szablon->id],
                ['confirm-btn-yes'=>$this->Txt->printAdmin(__('Admin | Tak')),'confirm-btn-no'=>$this->Txt->printAdmin(__('Admin | Nie')),'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $szablon->id)),'class'=>'btn btn-xs btn-danger confirm-link','escape'=>false]
            )?></li>
                      <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> '.__('Admin | Wróć do {0}',[__('Admin | Listy')])), ['action' => 'index'],['escape'=>false,'class'=>'btn btn-xs btn-warning']) ?></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
            <div class="x_content">
    <fieldset>
        <?php
            $lngInputs=[];
                    $lngInputs['temat']=['label'=>$this->Txt->printAdmin(__('Admin | temat - {0}'))];
                                $lngInputs['tresc']=['label'=>$this->Txt->printAdmin(__('Admin | tresc - {0}')),'summernote'=>'true'];
                    echo $this->Translation->inputs($this->Form, $languages, $lngInputs);
                            ?>
    </fieldset>
            </div>
            <div class="x_footer text-right">
    <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')),['class'=>'btn btn-sm btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
            <div class="row">
                <div class="col-lg-12">
                    <hr/>
                    <h3><?=$this->Txt->printAdmin(__('Admin | Tagi dozwolone w szablonach'))?></h3>
                     <?php 
        $tagi=['[%odzyskiwanie-hasla-link%]' => __('Link do odzyskania hasła'),
            '[%odzyskiwanie-hasla-url%]' => __('Adres url liku do odzyskania hasła'),
            '[%adres-ip%]' => __('Adres IP użytkownika'),
            '[%zamowienie-id%]' => __('Id zamówienia'),
            '[%zamowienie-numer%]' => __('Numer zamówienia'),
            '[%zamowienie-imie%]' => __('Imię Klienta'),
            '[%zamowienie-nazwisko%]' => __('Nazwisko Klienta'),
            '[%zamowienie-adres-wysylki%]' => __('Adres do wysyłki'),
            '[%zamowienie-adres-faktury%]' => __('Dane faktury'),
            '[%zamowienie-dane-klienta%]' => __('Dane klienta'),
            '[%zamowienie-email%]' => __('E-mail Klienta'),
            '[%zamowienie-firma%]' => __('Firma'),
            '[%zamowienie-nip%]' => __('Nip'),
            '[%zamowienie-telefon%]' => __('Telefon Klienta'),
            '[%zamowienie-wartosc-razem%]' => __('Lączna wartość zamówienia'),
            '[%zamowienie-uwagi%]' => __('Uwagi Klienta do zamówienia'),
            '[%zamowienie-status%]' => __('Status zamwówienia'),
            '[%zamowienie-data%]' => __('Data złożenia zamówienia'),
            '[%zamowienie-telefoniczne-uzupelnij-url%]' => __('Url linku do uzupełnienia danych zamówienia telefonicznego'),
            '[%termin-wysylki%]'=>__('Przewidywany termin wysyłki (dostępne przy zmianie statusu)'),
            '[%zamowienie-opiekun%]'=>__('Partner handlowy (PH) [imię i nazwisko]'),
            '[%zamowienie-opiekun-id%]'=>__('Identyfikator partnera handlowego (PH)'),
            '[%uzytkownik-id%]' => __('Identyfikator użytkownika (klienta)'),
            '[%uzytkownik-imie%]' => __('Imię użytkownika (klienta)'),
            '[%uzytkownik-nazwisko%]' => __('Nazwisko użytkownika (klienta)'),
            '[%uzytkownik-email%]' => __('Email użytkownika (klienta)'),
            '[%uzytkownik-firma%]' => __('Firma użytkownika (klienta)'),
            '[%uzytkownik-nip%]' => __('Nip użytkownika (klienta)'),
            '[%uzytkownik-telefon%]' => __('Telefon użytkownika (klienta)'),
            '[%uzytkownik-data-rejestracji%]' => __('Data rejestracji użytkownika (klienta)'),
            '[%uzytkownik-ph%]'=>__('Imie i Nazwisko opiekuna handlowego'),
            '[%uzytkownik-ph-email%]'=>__('Email opiekuna handlowego'),
            
            '[%towary%]'=>__('Znacznik otwierający listę towarów'),
                '[%t-lp%]'=>__('Liczba porządkowa'),
                '[%t-nazwa%]'=>__('Nazwa towaru'),
                '[%t-ilosc%]'=>__('Ilość zamówionych sztuk'),
                '[%t-cena%]'=>__('Cena jednostkowa'),
                '[%t-wartosc%]'=>__('Watość towaru'),
                '[%t-jednostka%]'=>__('Jednostka'),
            '[%/towary%]'=>__('Znacznik zamykający listę towarów')
            ];
        $tr='';
            foreach ($tagi as $key => $label)
            {
                $tr.=$this->Html->tag('tr',$this->Html->tag('td',$key).$this->Html->tag('td',$label));
            }
            echo $this->Html->tag('table',$tr,['class'=>'table table-striped']);
        ?>
                </div>
            </div>
        </div>
    </div>
</div>
