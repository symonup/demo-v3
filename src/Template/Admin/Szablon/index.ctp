<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Szablon[]|\Cake\Collection\CollectionInterface $szablon
  */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Szablon')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                </ul>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped data-table">
                    <thead>
            <tr>
                                <th data-priority="2"><?= $this->Txt->printAdmin(__('Admin | id')) ?></th>
                                <th data-priority="4"><?= $this->Txt->printAdmin(__('Admin | label')) ?></th>
                                <th data-priority="5"><?= $this->Txt->printAdmin(__('Admin | temat')) ?></th>
                                <th data-priority="6"><?= $this->Txt->printAdmin(__('Admin | opis')) ?></th>
                                <th class="actions" data-orderable="false" data-priority="1"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
            </tr>
        </thead>
        <tbody>
            <?php  foreach ($szablon as $szablon): ?>
            <tr>
                <td><?= $szablon->id ?></td>
                <td><?= $szablon->label ?></td>
                <td><?= $szablon->temat ?></td>
                <td><?= $szablon->opis ?></td>
                <td class="actions">
                    <?= $this->Html->link('<i class="fa fa-cogs"></i>', ['action' => 'edit', $szablon->id],['escape'=>false,'class'=>'btn btn-default btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Edytuj'))]) ?>
                </td>
            </tr>
            <?php endforeach;  ?>
        </tbody>
                </table>
            </div>
        </div>
    </div>
</div>