<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <?= $this->Form->create($kategorium, ['type' => 'file']) ?>
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Add {0}', [__('Admin | Kategorium')])) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Txt->printAdmin('<i class="fa fa-chevron-left"></i> ' . __('Admin | Wróć do {0}', [__('Admin | Listy')])), ['action' => 'index'], ['escape' => false, 'class' => 'btn btn-xs btn-warning']) ?></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <fieldset>
                    <?php
                    $lngInputs = [];
                    echo $this->Form->control('parent_id', ['options' => $parentKategoria, 'value' => $id, 'empty' => $this->Txt->printAdmin(__('Admin | Główna kategoria')), 'label' => $this->Txt->printAdmin(__('Admin | Kategoria nadrzędna'))]);
                    $lngInputs['nazwa'] = ['label' => $this->Txt->printAdmin(__('Admin | Nazwa - {0}'))];
                    $lngInputs['naglowek'] = ['label' => $this->Txt->printAdmin(__('Admin | Nagłówek - {0}'))];
//                    $lngInputs['opis_short'] = ['label' => $this->Txt->printAdmin(__('Admin | Opis sktócony (na stronie głównej) - {0}'))];
                    $lngInputs['seo_tekst'] = ['label' => $this->Txt->printAdmin(__('Admin | Opis dodatkowy - {0}'))];
                    $lngInputs['seo_tytul'] = ['label' => $this->Txt->printAdmin(__('Admin | Seo tytuł - {0}'))];
                    $lngInputs['seo_opis'] = ['label' => $this->Txt->printAdmin(__('Admin | Seo opis - {0}'))];
                    $lngInputs['seo_slowa'] = ['label' => $this->Txt->printAdmin(__('Admin | Seo słowa - {0}'))];
                    echo $this->Translation->inputs($this->Form, $languages, $lngInputs);
                    echo $this->Form->control('icon_class', ['type' => 'text', 'label' => $this->Txt->printAdmin(__('Admin | Klasa CSS ikony (flaticons)'))]);
                    echo $this->Txt->file($this->Form->control('zdjecie', ['type' => 'file', 'label' => false]), $this->Txt->printAdmin(__('Admin | Dodaj zdjęcie')), (!empty($kategorium->zdjecie) ? $displayPath['kategoria'] . $kategorium->zdjecie : ''));
                    echo $this->Txt->file($this->Form->control('ikona', ['type' => 'file', 'label' => false]), $this->Txt->printAdmin(__('Admin | Dodaj ikonę')), (!empty($kategorium->ikona) ? $displayPath['kategoria_ikony'] . $kategorium->ikona : ''));
                    echo $this->Form->control('promuj', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Promuj kategorie na stronie głównej'))]);
                    echo $this->Form->control('ukryta', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Kategoria nie widoczna dla klienta'))]);
                    echo $this->Form->control('wyswietl_miniatury', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Wyświetlaj miniatury podkategorii zamiast listy towarów.'))]);
//                    echo $this->Form->control('preorder', ['type' => 'checkbox', 'label' => $this->Txt->printAdmin(__('Admin | Dedykowana dla preorderów'))]);
                   echo $this->Form->control('google_kategoria_id', ['options' => $googleKat,'label'=>$this->Txt->printAdmin(__('Admin | Wybierz odpowiednik kategorii w Google Merchant')), 'empty' => $this->Txt->printAdmin(__('Admin | Wybierz')),'select2'=>'true']);
                     ?>
                </fieldset>
            </div>
            <div class="x_footer text-right">
                <?= $this->Form->button($this->Txt->printAdmin(__('Admin | Zapisz')), ['class' => 'btn btn-sm btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
