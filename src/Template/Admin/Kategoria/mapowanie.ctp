<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Mapowanie kategorii sklepu z hurtowniami')) ?></h2>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?=$this->Form->create()?>
                <div class="text-right">
                    <p class="text-left"><?= $this->Txt->printAdmin(__('Admin | Do każdej kategorii ze sklepu przypisz kategorie pasujące z danej hurtowni. Pamiętaj aby podawać pełne ścieżki kategorii dokładnie w takiej formie w jakiej są przekazywane w pliku integracji. Do każdej kategorii ze sklepu można przypisać wiele kategorii z hurtowni')) ?></p>
                    <p class="text-center"><strong><?= $this->Txt->printAdmin(__('Admin | Pamiętaj o zapisaniu zmian!')) ?></strong></p>
                    <hr/>
                    <button type="submit" class="btn btn-xs btn-success"><?=$this->Txt->printAdmin(__('Admin | Zapisz'))?></button>
                </div>
                <ul class="nav nav-tabs" role="tablist">
                    <?php 
              $tabLiKey=0;
                    foreach ($hurtownie as $hurtowniaKey => $hurtowniaNazwa){
                        ?>
                    <li role="presentation" class="<?=(($tabLiKey==0)?'active':'')?>"><a href="#tab-<?=$hurtowniaKey?>" aria-controls="tab-<?=$hurtowniaKey?>" role="tab" data-toggle="tab"><?=$hurtowniaNazwa?></a></li>
                    <?php
                  $tabLiKey++;  }
                    ?>
                </ul>
                <div class="tab-content">
                <?php 
                
              $tabKey=0;
                    foreach ($hurtownie as $hurtowniaKey => $hurtowniaNazwa){ ?>
                <div role="tabpanel" class="tab-pane <?=(($tabKey==0)?'active':'')?>" id="tab-<?=$hurtowniaKey?>">
                <table class="table table-striped table-bordered">
                    <thead>
                    <th><?= $this->Txt->printAdmin(__('Admin | Sklep')) ?></th>
                    <?php 
                        echo $this->Html->tag('th',$hurtowniaNazwa);
                    ?>
                    </thead>
                    <tbody>
              <?php
              foreach ($allKats as $kategoria){
                ?>
                        <tr>
                            <td style="width:40%;"><?=$kategoria->sciezka?></td>
                            <?php 
                        $addButton = '<div class="text-right"><button kat-id="'.$kategoria->id.'" key="n_'.$kategoria->id.'_'.$hurtowniaKey.'_" add-key="0" hurtownia="'.$hurtowniaKey.'" type="button" class="btn btn-xs btn-primary add-new-map"><i class="fa fa-plus"></i> Dodaj</button></div>';
                        if(!empty($kategoria->kategoria_map)){
                            $tmpItems=[];
                            foreach ($kategoria->kategoria_map as $cMap){
                                if($cMap->hurtownia==$hurtowniaKey){
                                    $tmpItems[]=$this->element('admin/kategoria_map_item',['item'=>$cMap,'hurtownia'=>$hurtowniaKey,'key'=>$cMap->id,'katId'=>$kategoria->id]);
                                }
                            }
                            if(!empty($tmpItems)){
                                echo $this->Html->tag('td', $this->Html->tag('div',join('', $tmpItems),['class'=>'map-container', 'hurtownia'=>$hurtowniaKey, 'kat-id'=>$kategoria->id]).$addButton);
                            }else{
                                echo $this->Html->tag('td',$this->Html->tag('div','',['class'=>'map-container', 'hurtownia'=>$hurtowniaKey, 'kat-id'=>$kategoria->id]).$addButton);
                            }
                        }else{
                            echo $this->Html->tag('td',$this->Html->tag('div','',['class'=>'map-container', 'hurtownia'=>$hurtowniaKey, 'kat-id'=>$kategoria->id]).$addButton);
                        }
                    ?>
                        </tr>
                <?php
              }
              ?></tbody>
                </table>
                </div>
                    <?php $tabKey++; } ?>
                </div>
                <div class="text-right">
                    <hr/>
                    <button type="submit" class="btn btn-lg btn-success"><?=$this->Txt->printAdmin(__('Admin | Zapisz'))?></button>
                </div>
                <?=$this->Form->end()?>
            </div>
        </div>
    </div>
</div>
<div id="map-default" style="display: none;">
    <?=$this->element('admin/kategoria_map_item',['item'=>null,'hurtownia'=>'_hurtownia_','key'=>'_key_','katId'=>'_katid_'])?>
</div>