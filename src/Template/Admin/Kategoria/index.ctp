<?php
if(!empty($crumbs)){
    ?>
<div class="row">
    <div class="col-lg-12">
        <?=$this->element('admin/breadcrumb',['crumbs'=>$crumbs])?>
    </div>
</div>
<?php
}
?>
<div class="row">
    <div class="col-lg-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $this->Txt->printAdmin(__('Admin | Kategoria')) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link($this->Html->tag('i','',['class'=>'fa fa-plus']).' '.$this->Txt->printAdmin(__('Admin | Dodaj {0}',[$this->Txt->printAdmin(__('Admin | Kategorium'))])), ['action' => 'add',$id],['escape'=>false,'class'=>'btn btn-xs btn-primary']) ?></li>
                </ul>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped">
                    <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nazwa') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ukryta') ?></th>
              <th scope="col"><?= $this->Paginator->sort('wyswietl_miniatury',['Wyświetlaj podkategorie']) ?></th> 
                <th scope="col"><?= $this->Paginator->sort('promuj',$this->Txt->printAdmin(__('Admin | Promuj na głównej'))) ?></th>
                <th scope="col" class="actions"><?= $this->Txt->printAdmin(__('Admin | Actions')) ?></th>
            </tr>
        </thead>
        <tbody class="sorted-kolejnosc" sort-target="<?= Cake\Routing\Router::url(['action' => 'sort', 'kolejnosc']) ?>">
            <?php foreach ($kategoria as $kategorium): ?>
            <tr sort-item-id="<?= $kategorium->id ?>">
                <td><?= $this->Number->format($kategorium->id) ?></td>
                <td><?= h($kategorium->nazwa) ?></td>
                <td><?= $this->Txt->printBool($kategorium->ukryta, str_replace($basePath, '/', Cake\Routing\Router::url(['controller' => 'Kategoria', 'action' => 'setField', $kategorium->id, 'ukryta', (!empty($kategorium->ukryta) ? 0 : 1)]))) ?></td>
                <td><?= $this->Txt->printBool($kategorium->wyswietl_miniatury, str_replace($basePath, '/', Cake\Routing\Router::url(['controller' => 'Kategoria', 'action' => 'setField', $kategorium->id, 'wyswietl_miniatury', (!empty($kategorium->wyswietl_miniatury) ? 0 : 1)]))) ?></td> 
                <td><?= $this->Txt->printBool($kategorium->promuj, str_replace($basePath, '/', Cake\Routing\Router::url(['controller' => 'Kategoria', 'action' => 'setField', $kategorium->id, 'promuj', (!empty($kategorium->promuj) ? 0 : 1)]))) ?></td>
                
                <td class="actions">
                    <?= $this->Html->link('<i class="fa fa-sort-amount-desc"></i>', ['action' => 'index', $kategorium->id],['escape'=>false,'class'=>'btn btn-default btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Kategorie podrzędne'))]) ?>
                    <?= $this->Html->link('<i class="fa fa-cogs"></i>', ['action' => 'edit', $kategorium->id],['escape'=>false,'class'=>'btn btn-default btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Edytuj'))]) ?>
                    <?= $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'delete', $kategorium->id], ['confirm-btn-yes'=>$this->Txt->printAdmin(__('Admin | Tak')),'confirm-btn-no'=>$this->Txt->printAdmin(__('Admin | Nie')),'confirm-message' => $this->Txt->printAdmin(__('Admin | Are you sure you want to delete # {0}?', $kategorium->id)),'escape'=>false,'class'=>'btn btn-danger btn-xs confirm-link','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Txt->printAdmin(__('Admin | Usuń'))]) ?>
              <?php if (!key_exists('sort', $_GET) && empty($filter)) { ?>
                            <spna class="sort-item-point btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="<?= $this->Txt->printAdmin(__('Admin | Przeciągnij aby zmienić kolejność')) ?>"><i class="fa fa-arrows-alt"></i></spna>
                            <?php } ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
                </table>
            </div>
        </div>
    </div>
</div>