<div class="modal fade modalRemoveOnHide" id="modal-powiadom-o-dostepnosci" tabindex="-1" role="dialog" aria-labelledby="modal-powiadom-o-dostepnosciLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-powiadom-o-dostepnosciLabel"><?= $this->Translation->get('powiadomODostepnosci.modalTitle') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= $this->Form->create($powiadomienie) ?>
            <div class="modal-body nice-radiobutton">
                <div><?= $this->Translation->get('powiadomODostepnosci.modalInfo{nazwa}', ['nazwa' => $towar->nazwa], true) ?></div>
                <?= $this->Form->control('imie', ['type' => 'text', 'label' => false, 'placeholder' => $this->Translation->get('powiadomODostepnosci.imiePlaceholder')]) ?>
                <?= $this->Form->control('email', ['type' => 'email', 'label' => false, 'placeholder' => $this->Translation->get('powiadomODostepnosci.emailPlaceholder')]) ?>
                    <?php 
                    if(!empty($_GET['t'])){
                    if(!empty($punktyOdbioru)){
                        echo $this->Form->control('punkt_odbioru',['type'=>'radio','label'=>['text'=>$this->Translation->get('powiadomODostepnosci.punktyOdbioruLabel'),'escape'=>false],'escape'=>false,'options'=>$punktyOdbioru]);
                    }
                    }
                    echo (($typ=='zapytanie') ? $this->Form->control('tresc', ['type' => 'textarea', 'label' => false, 'placeholder' => $this->Translation->get('powiadomODostepnosci.trescPlaceholder')]) : '');
                    ?>
             <?php /*   <div class="nice-checkbox">
                    <?= $this->Form->control('zgoda', ['type' => 'checkbox', 'required' => true, 'id' => 'zgoda-powiadomienie-'. uniqid(), 'label' => ['text' => $this->Translation->get('powiadomODostepnosci.zgodaLabel'), 'escape' => false]]) ?>
                </div> */ ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= $this->Translation->get('powiadomODostepnosci.btnAnuluj') ?></button>
                <button type="submit" class="btn btn-primary btn-color-1"><?= $this->Translation->get('powiadomODostepnosci.btnZapisz') ?></button>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>