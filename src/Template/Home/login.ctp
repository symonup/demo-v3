<div class="container login-container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-4 col-lg-3 pd-t-35">
            <?= $this->Form->create($logUzytkownik, ['class' => 'login-form']) ?>
            <a class="login-logo" href="<?= Cake\Routing\Router::url(['controller' => 'Home', 'action' => 'index']) ?>"><img src="<?= $displayPath['webroot'] ?>img/logo_full.png"/></a>

            <div class="row">
                <div class="col-12 no-padding">
                    <div class="logowanie-head"><?=t('loginPage.Logowanie')?></div>
                    <?= $this->Form->control('email', ['type' => 'email', 'required' => true, 'id' => 'login-email', 'label' => t('loginPage.emailInputLabel')]) ?>
                    <?= $this->Form->control('password', ['type' => 'password', 'id' => 'login-password', 'required' => true, 'label' => t('loginPage.passwordInputLabel')]) ?>
                </div>
            </div>
            <div class="row login-footer">
                <div class="col-12 col-md-auto text-left md-padding-right-0">    
                    <?= $this->Form->control('remember_me', ['type' => 'checkbox', 'required' => false, 'label' => ['text' => t('loginPage.rememberMeLabel'), 'escape' => false], 'templateVars' => ['groupClass' => 'rememberMeGroup']]) ?>
                </div>
                <div class="col-12 col-md text-right md-padding-left-0">   
                    <a class="forget-link" href="<?= Cake\Routing\Router::url(['controller' => 'Home', 'action' => 'resetPassword']) ?>"><?= t('loginPage.resetPasswordLink') ?></a>
                </div>
                <div class="col-12 text-center">
                    <button type="submit" class="btn btn-sm btn-block btn-success fs-18" name="login"><?= t('loginPage.signInButton') ?></button>
                </div>
            </div>
            <?= $this->Form->end() ?>
        </div>
        <div class="col-1 d-none d-md-flex"></div>
        <div class="col-12 col-md-auto pd-t-35">
            <div class="text-center register-action">
                <div class="join-header"><?= t('loginPage.joinHeader') ?></div>
                <div class="join-type-select"><?= t('loginPage.wybierzRodzajKonta') ?></div>
                <a href="<?=r(['controller'=>'Home','action'=>'register','b2c'])?>" class="btn btn-outline-color-2 btn-sm mt-15 getPupUpLink" modal-target="#modal-register"><?= t('loginPage.zalozNoweKonto') ?></a><br/>
                <a href="<?=r(['controller'=>'Home','action'=>'register','b2b'])?>" class="btn btn-outline-color-2 btn-sm mt-15 getPupUpLink" modal-target="#modal-register"><?= t('loginPage.zalozNoweKontoB2B') ?></a><br/>
                <a href="<?=r(['controller'=>'Home','action'=>'index'])?>" class="btn btn-outline-color-1 btn-sm mt-15"><i class="fa fa-angle-left"></i> <?= t('loginPage.wrocDoSklepu') ?></a>
            </div>
        </div>
    </div>
</div>