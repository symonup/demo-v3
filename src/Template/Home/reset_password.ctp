<div class="container login-container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-6 col-lg-5">
            <h2><?= $this->Translation->get('loginPage.resetPasswordHeader') ?></h2>
            <p><?= $this->Translation->get('loginPage.resetPasswordInfo'.(!empty($token)?'Password':'Email'),[],true) ?></p>
        </div>
    </div>
    <?= $this->Form->create($uzytkownik, ['class' => 'form-default']) ?>
    <?php if (!empty($token)) : ?>
        <div class="row justify-content-center">
            <div class="col-12 col-md-6 col-lg-5">
                <?= $this->Form->control('password', ['type' => 'password', 'required' => true, 'label' => $this->Translation->get('loginPage.passwordInputLabel')]) ?>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-md-6 col-lg-5">
                <?= $this->Form->control('repeat_password', ['type' => 'password', 'required' => true, 'label' => $this->Translation->get('loginPage.repeatPasswordInputLabel')]) ?>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-md-6 col-lg-5 text-right">
                <?= $this->Form->button($this->Translation->get('loginPage.resetPasswordSavedBtn'), ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    <?php else : ?>
        <div class="row justify-content-center">
            <div class="col-12 col-md-6 col-lg-5">
                <?= $this->Form->input('email', ['label' => $this->Translation->get('loginPage.emailInputLabel'), 'class' => 'form-control']) ?>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-md-6 col-lg-5 text-right">
                <?= $this->Form->button($this->Translation->get('loginPage.resetPasswordSendBtn'), ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    <?php endif; ?>
    <?= $this->Form->end() ?>
</div>