<?php
$routerParams = [
    'controller' => $this->request->params['controller'],
    'action' => $this->request->params['action'],
];
$routerParams += $this->request->params['pass'];
if (!empty($this->request->query)) {
    $routerParams += $this->request->query;
}
unset($routerParams['view']);
$promoMainKays = [];
if (!empty($promowaneKategorie)) {
    $promoMainKays = array_keys($promowaneKategorie);
}
$searchKat = [];
foreach ($mainCatsDef as $mainKatItem) {
    $searchKat[$mainKatItem['id']] = $mainKatItem['nazwa'];
}
?>
<div class="container-fluid <?=($mobile?'mt-20':'mt-50')?>">
    <div class="row">
        <div class="col-12 col-md">
            <?=$this->element('default/search_box',['searchKat'=>$searchKat])?>
            <div class="main-banner">
                <?= $this->element('default/banner', ['banner' => $banner]) ?>
            </div>
        </div>
        <div class="col-12 col-md-12 col-lg-4 ps-relative home-top-left <?=(!$mobile?'pd-l-0':'')?>">
            <ul class="home-menu">
                <li>
                    <div>
                        <span><?=t('home.promo1Tytul')?></span>
                        <?=t('home.promo1PodTytul')?>
                    </div>
                    <a href="<?=c('links.homePromo1')?>"><?=t('home.promoSprawdz')?></a>
                </li>
                <li>
                    <div>
                        <span><?=t('home.promo2Tytul')?></span>
                        <?=t('home.promo2PodTytul')?>
                    </div>
                    <a href="<?=c('links.homePromo2')?>"><?=t('home.promoSprawdz')?></a>
                </li>
                <li>
                    <div>
                        <span><?=t('home.promo3Tytul')?></span>
                        <?=t('home.promo3PodTytul')?>
                    </div>
                    <a href="<?=c('links.homePromo3')?>"><?=t('home.promoSprawdz')?></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="row align-items-end">
        <div class="col-12 col-md">
            <?= $this->element('default/home_kontakt') ?>
        </div>
        <div class="col-12 col-md-12 col-lg-4 ps-relative <?=(!$mobile?'pd-l-0':'')?>">
            <ul class="home-menu produkt-dnia-ul">
                <li>
                    <div>
                        <span>Produkt dnia</span>
                        zamów teraz
                    </div>
                    <a href="<?= (!empty($hotDeal) ? Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'view', $hotDeal->towar->id, $this->Navi->friendlyUrl($hotDeal->towar->nazwa)]) : '#') ?>">Sprawdź szczegóły</a>
                </li>
            </ul>
            <?php
            if (!empty($hotDeal)) {
                echo $this->element('default/hot_deal');
            }
            ?>
        </div>
    </div>
</div>
<?= $this->element('default/home_category') ?>
<div class="container-fluid">
    <div class="row"><div class="col-12"><?= t('home.infoHead') ?></div></div>
</div>
<div class="home-info-text-img">
    <div class="container-fluid">
        <?= t('home.infoTekst') ?>
    </div>
</div>
<?php if (!empty($promocje) && $promocje->count() > 0) { ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="headerLine"><span><?= t('home.naszePromocje') ?></span></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md home-pr-content">
                <div class="row justify-content-between">
                    <?php
                    if (!$mobile) {
                        echo $this->element('default/items_slider', ['sliderId' => 'homeSliderPromocje', 'items' => $promocje, 'perPage' => 5]);
                    } else {
                        echo $this->element('default/items_mobile_carusel', ['items' => $promocje, 'hideBadge' => false, 'hideBadgeTyp' => ['nowosc']]);
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php } if (!empty($nowosci) && $nowosci->count() > 0) { ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="headerLine"><span><?= t('home.naszeNowosci') ?></span></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md home-pr-content">
                <div class="row justify-content-between">
                    <?php
                    if (!$mobile) {
                        echo $this->element('default/items_slider', ['sliderId' => 'homeSliderNowosci', 'items' => $nowosci, 'perPage' => 5]);
                    } else {
                        echo $this->element('default/items_mobile_carusel', ['items' => $nowosci, 'hideBadge' => false, 'hideBadgeTyp' => ['nowosc']]);
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php } if (!empty($bestsellers) && $bestsellers->count() > 0) { ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="headerLine"><span><?= t('home.naszeBestsellery') ?></span></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md home-pr-content">
                <div class="row justify-content-between">
                    <?php
                    if (!$mobile) {
                        echo $this->element('default/items_slider', ['sliderId' => 'homeSliderBestsellery', 'items' => $bestsellers, 'perPage' => 5]);
                    } else {
                        echo $this->element('default/items_mobile_carusel', ['items' => $bestsellers, 'hideBadge' => false, 'hideBadgeTyp' => ['nowosc']]);
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php
}
if (!empty($artykuly) && $artykuly->count() > 0) {
    ?>
    <div class="container-fluid mt-50">
        <div class="row align-items-end mb-30">
            <div class="col">
                <div class="header"><span><?= t('home.najnowszeArtykuly') ?></span></div>
            </div>
            <div class="col-auto">
                <a class="gold-link" href="<?= Cake\Routing\Router::url(['controller'=>'Blog','action'=>'index'])?>"><?= t('home.Blog') ?> <i class="fa fa-angle-right"></i></a>
            </div>
        </div>
        <div class="row">
            <?php foreach ($artykuly as $artykul) {
                ?>
                <div class="col-12 col-sm-6 col-md-3">
                <?= $this->element('default/blog/item', ['item' => $artykul]) ?>
                </div>
                <?php }
            ?>
        </div>
    </div>
    <?php
}
?>