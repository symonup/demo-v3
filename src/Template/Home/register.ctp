<div class="modal fade modalRemoveOnHide" id="modal-register" tabindex="-1" role="dialog">
    <div class="modal-dialog <?= (($type == 'b2b') ? 'modal-lg' : '') ?>" role="document">
        <div class="modal-content">
            <?= $this->Form->create($uzytkownik, ['class' => 'register-form ' . $type]) ?>
            <div class="modal-header">
                <h5 class="modal-title"><?= t('registerPopUp.header_' . $type) ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <?php
                    if ($type == 'b2b') {
                        ?>
                        <div class="col-12 col-md-6">
                            <div class="row">
                                <div class="col-12">
                                    <h5><?= t('registerPopUp.daneDoLogowania') ?></h5>
                                </div>
                                <div class="col-12">
                                    <?= $this->Form->control('email', ['type' => 'email', 'required' => true, 'label' => t('registerPopUp.emailInputLabel')]) ?>
                                </div>
                                <div class="col-12 col-md-12">
                                    <?= $this->Form->control('password', ['type' => 'password', 'id' => 'register-password', 'required' => true, 'label' => t('registerPopUp.passwordInputLabel')]) ?>
                                </div>
                                <div class="col-12 col-md-12">
                                    <?= $this->Form->control('repeat_password', ['type' => 'password', 'required' => true, 'label' => t('registerPopUp.repeatPasswordInputLabel')]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="row">
                                <div class="col-12">
                                    <h5><?= t('registerPopUp.daneFirmy') ?></h5>
                                </div>
                                <div class="col-12 col-md-8">
                                    <?= $this->Form->control('firma', ['type' => 'text', 'required' => true, 'label' => t('registerPopUp.firmaInputLabel')]) ?>
                                </div>
                                <div class="col-12 col-md-4 md-padding-left-0">
                                    <?= $this->Form->control('nip', ['type' => 'text', 'required' => true, 'label' => t('registerPopUp.nipInputLabel')]) ?>
                                </div>
                                <div class="col-12 col-md-7">
                                    <?= $this->Form->control('firma_ulica', ['type' => 'text', 'required' => true, 'label' => t('registerPopUp.ulicaInputLabel')]) ?>
                                </div>
                                <div class="col-12 col-md md-padding-left-0">
                                    <?= $this->Form->control('firma_nr_domu', ['type' => 'text', 'required' => true, 'label' => t('registerPopUp.nrBudynkuInputLabel')]) ?>
                                </div>
                                <div class="col-12 col-md md-padding-left-0">
                                    <?= $this->Form->control('firma_nr_lokalu', ['type' => 'text', 'required' => false, 'label' => t('registerPopUp.nrLokaluInputLabel')]) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-4 md-padding-right-0">
                                    <?= $this->Form->control('firma_kod', ['type' => 'text', 'required' => true, 'label' => t('registerPopUp.kodPocztowyInputLabel')]) ?>
                                </div>
                                <div class="col-12 col-md-8">
                                    <?= $this->Form->control('firma_miasto', ['type' => 'text', 'required' => true, 'label' => t('registerPopUp.miastoInputLabel')]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <h5><?= t('registerPopUp.osobaKontaktowa') ?></h5>
                        </div>
                        <div class="col-12 col-md-4">
                            <?= $this->Form->control('imie', ['type' => 'text', 'required' => true, 'label' => t('registerPopUp.nameInputLabel')]) ?>
                        </div>
                        <div class="col-12 col-md-4">
                            <?= $this->Form->control('nazwisko', ['type' => 'text', 'required' => true, 'label' => t('registerPopUp.lastNameInputLabel')]) ?>
                        </div>
                        <div class="col-12 col-md-4">
                            <?= $this->Form->control('telefon', ['type' => 'text', 'required' => true, 'label' => t('registerPopUp.phoneInputLabel')]) ?>
                        </div>
                        <?php
                    } else {
                        ?>
                        <div class="col-12">
                            <?= $this->Form->control('email', ['type' => 'email', 'required' => true, 'label' => t('registerPopUp.emailInputLabel')]) ?>
                        </div>
                        <div class="col-12 col-md-6">
                            <?= $this->Form->control('password', ['type' => 'password', 'id' => 'register-password', 'required' => true, 'label' => t('registerPopUp.passwordInputLabel')]) ?>
                        </div>
                        <div class="col-12 col-md-6">
                            <?= $this->Form->control('repeat_password', ['type' => 'password', 'required' => true, 'label' => t('registerPopUp.repeatPasswordInputLabel')]) ?>
                        </div>
                        <div class="col-12 col-md-6">
                            <?= $this->Form->control('imie', ['type' => 'text', 'required' => true, 'label' => t('registerPopUp.nameInputLabel')]) ?>
                        </div>
                        <div class="col-12 col-md-6">
                            <?= $this->Form->control('nazwisko', ['type' => 'text', 'required' => true, 'label' => t('registerPopUp.lastNameInputLabel')]) ?>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="col-12 nice-checkbox type-2">
                        <?php
                        $zgodaTekst = t('registerPopUp.consentInputLabel');
                        if (!empty($zgodaTekst) && $zgodaTekst != 'consentInputLabel') {
                            echo $this->Form->control('zgoda', ['type' => 'checkbox', 'required' => true, 'label' => ['text' => t('registerPopUp.consentInputLabel'), 'escape' => false]]);
                        }
                        ?>
                        <?= $this->Form->control('regulamin', ['type' => 'checkbox','id'=>'register-regulamin', 'required' => true, 'label' => ['text' => t('registerPopUp.regulationsInputLabel'), 'escape' => false]]) ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= t('registerPopUp.Anuluj') ?></button>
                <button type="submit" class="btn btn-success"><?= t('registerPopUp.zarejestrujBtn') ?></button>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>