<div class="modal fade modalRemoveOnHide" id="get-talk-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= $this->Translation->get('getTalk.modalTitle') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= $this->Form->create() ?>
            <div class="modal-body">
                <div><?= $this->Translation->get('getTalk.modalInfo', [], true) ?></div>
                <?= $this->Form->control('telefon', ['type' => 'text', 'required' => true, 'label' => false, 'placeholder' => $this->Translation->get('getTalk.telefonPlaceholder')]) ?>
             
              <div class="nice-checkbox type-2">
                    <?= $this->Form->control('zgoda', ['type' => 'checkbox', 'required' => true, 'id' => 'zgoda-powiadomienie-'. uniqid(), 'label' => ['text' => $this->Translation->get('getTalk.zgodaLabel'), 'escape' => false]]) ?>
                </div> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= $this->Translation->get('getTalk.btnAnuluj') ?></button>
                <button type="submit" class="btn btn-primary btn-color-1"><?= $this->Translation->get('getTalk.btnZapisz') ?></button>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>