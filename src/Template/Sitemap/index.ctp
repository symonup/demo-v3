<div class="container">
    <?php
    use Cake\Routing\Router;
    echo $this->Html->tag('div', (!empty($crumbs) ? $this->Navi->breadcrumb($crumbs) : '') . $this->Html->tag('h1', $this->Translation->get('sitemap.MapaStrony')), ['class' => 'row']);
    echo $this->Html->tag('h4', $this->Translation->get('sitemap.Kategorie'));
    echo $this->Html->tag('div', $this->Html->tag('ul', $this->Navi->getAllCatByUl($categoriesTree), [])
            , []);
    echo $this->Html->tag('h4', $this->Translation->get('sitemap.Towary'));
    $siteTowary='';
    foreach ($towary as $towar)
    {
        $siteTowary.=$this->Html->tag('li',$this->Html->link($towar->nazwa,  $this->Txt->towarViewUrl($towar),['escape'=>false,'title'=>$towar->nazwa]));
    }
    echo $this->Html->tag('div', $this->Html->tag('ul',$siteTowary), []);
    
    echo $this->Html->tag('h4', $this->Translation->get('sitemap.Producenci'));
    $siteProducenci='';
    foreach ($producenci as $producent)
    {
        $siteProducenci.=$this->Html->tag('li',$this->Html->link($producent->nazwa,  Router::url(['controller'=>'towar','action'=>'producent',$producent->id,$this->Txt->friendlyUrl($producent->nazwa)]),['escape'=>false,'title'=>$producent->nazwa]));
    }
    echo $this->Html->tag('div', $this->Html->tag('ul',$siteProducenci), []);
    
    echo $this->Html->tag('h4   ', $this->Translation->get('sitemap.Strony'));
    $siteStrony='';
    foreach ($strony as $strona)
    {
        $siteStrony.=$this->Html->tag('li',$this->Html->link($strona->nazwa,  Router::url(['controller'=>'strona','action'=>'view',$strona->id,$this->Txt->friendlyUrl($strona->nazwa)]),['escape'=>false,'title'=>$strona->nazwa]));
    }
    $siteStrony.=$this->Html->tag('li',$this->Html->link($this->Translation->get('sitemap.Kontakt'),  Router::url(['controller'=>'kontakt','action'=>'index']),['escape'=>false,'title'=>$this->Translation->get('sitemap.Kontakt')]));
    echo $this->Html->tag('div', $this->Html->tag('ul',$siteStrony), []);
    ?>
</div>