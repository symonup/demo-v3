<?php
    use Cake\Routing\Router;

$urls = [];
    if ($typ == 'strony') {
        foreach ($kategorie as $kategoria) {
            $urls[] = $this->Html->tag('url', $this->Html->tag('loc', Router::url(['controller' => 'Towar', 'action' => 'index', $kategoria->id, $this->Txt->friendlyUrl($kategoria->nazwa)], true))
                    . $this->Html->tag('lastmod', date('Y-m-d'))
                    . $this->Html->tag('changefreq', 'always')
//                    . $this->Html->tag('priority', '1')
            );
        }
        if(!empty($gatunki) && $gatunki->count()>0){
            foreach ($gatunki as $gatunek){
                 $urls[] = $this->Html->tag('url', $this->Html->tag('loc', Router::url(['controller' => 'Towar', 'action' => 'index', 'type'=>$gatunek->id], true))
                    . $this->Html->tag('lastmod', date('Y-m-d'))
                    . $this->Html->tag('changefreq', 'always')
//                    . $this->Html->tag('priority', '1')
            );
            }
        }
        $urls[] = $this->Html->tag('url', $this->Html->tag('loc', Router::url(['controller' => 'Towar', 'action' => 'index', 'wyprzedaz'], true))
                . $this->Html->tag('lastmod', date('Y-m-d'))
                . $this->Html->tag('changefreq', 'always')
//                . $this->Html->tag('priority', '1')
        );
        $urls[] = $this->Html->tag('url', $this->Html->tag('loc', Router::url(['controller' => 'Towar', 'action' => 'index', 'polecane'], true))
                . $this->Html->tag('lastmod', date('Y-m-d'))
                . $this->Html->tag('changefreq', 'always')
//                . $this->Html->tag('priority', '1')
        );
        $urls[] = $this->Html->tag('url', $this->Html->tag('loc', Router::url(['controller' => 'Towar', 'action' => 'index', 'promocje'], true))
                . $this->Html->tag('lastmod', date('Y-m-d'))
                . $this->Html->tag('changefreq', 'always')
//                . $this->Html->tag('priority', '1')
        );
        $urls[] = $this->Html->tag('url', $this->Html->tag('loc', Router::url(['controller' => 'Towar', 'action' => 'index', 'wyroznione'], true))
                . $this->Html->tag('lastmod', date('Y-m-d'))
                . $this->Html->tag('changefreq', 'always')
                . $this->Html->tag('priority', '1')
        );
//        foreach ($producenci as $producent) {
//            $urls[] = $this->Html->tag('url', $this->Html->tag('loc', Router::url(['controller' => 'Towar', 'action' => 'producent', $producent->id, $this->Txt->friendlyUrl($producent->nazwa)], true))
//                    . $this->Html->tag('lastmod', date('Y-m-d'))
//                    . $this->Html->tag('changefreq', 'always')
////                    . $this->Html->tag('priority', '1')
//            );
//        }

        foreach ($strony as $strona) {
            $urls[] = $this->Html->tag('url', $this->Html->tag('loc', Router::url(['controller' => 'Pages', 'action' => 'view', $strona->id, $this->Txt->friendlyUrl($strona->nazwa)], true))
                    . $this->Html->tag('lastmod', date('Y-m-d'))
                    . $this->Html->tag('changefreq', 'always')
//                    . $this->Html->tag('priority', '1')
            );
        }
//        $urls[] = $this->Html->tag('url', $this->Html->tag('loc', Router::url(['controller' => 'Kontakt', 'action' => 'index'], true))
//                . $this->Html->tag('lastmod', date('Y-m-d'))
//                . $this->Html->tag('changefreq', 'always')
////                . $this->Html->tag('priority', '1')
//        );
    } else if ($typ == 'produkty') {
        foreach ($towary as $towar) {
            $urls[] = $this->Html->tag('url', $this->Html->tag('loc', $this->Txt->towarViewUrl($towar,true))
                    . "\r\n" . $this->Html->tag('lastmod', date('Y-m-d'))
                    . "\r\n" . $this->Html->tag('changefreq', 'always')
//                    . "\r\n" . $this->Html->tag('priority', '1')
                    . (!empty($towar->towar_zdjecie) ? "\r\n" . $this->Html->tag('image:image', $this->Html->tag('image:loc', Router::url($displayPath['towar'] . $this->Txt->getKatalog($towar->towar_zdjecie[0]->id) . '/thumb_3_' . $towar->towar_zdjecie[0]->plik,true)) . $this->Html->tag('image:title', str_replace('&','&amp;',(!empty($towar->towar_zdjecie[0]->alt) ? $towar->towar_zdjecie[0]->alt : $towar->nazwa)))) : '')
            );
        }
    } else {
        for ($i = 1; $i <= $towarPages; $i++) {
            $urls[] = $this->Html->tag('sitemap', $this->Html->tag('loc', Router::url(['controller' => 'Sitemap', 'action' => 'xml', 'produkty', $i, '_ext' => 'xml'], true))
            );
        }
        $urls[] = $this->Html->tag('sitemap', $this->Html->tag('loc', Router::url(['controller' => 'Sitemap', 'action' => 'xml', 'strony', '_ext' => 'xml'], true))
        );
    }
    if ($typ == 'all') {
        echo $this->Html->tag('sitemapindex', join("\r\n", $urls), ['xmlns' => 'http://www.sitemaps.org/schemas/sitemap/0.9', 'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance', 'xsi:schemaLocation' => 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd']);
    } else
    if ($typ == 'produkty') {
        echo $this->Html->tag('urlset', join("\r\n", $urls), ['xmlns' => 'http://www.sitemaps.org/schemas/sitemap/0.9', 'xmlns:image' => 'http://www.google.com/schemas/sitemap-image/1.1']);
    } else {
        echo $this->Html->tag('urlset', join("\r\n", $urls), ['xmlns' => 'http://www.sitemaps.org/schemas/sitemap/0.9']);
    }