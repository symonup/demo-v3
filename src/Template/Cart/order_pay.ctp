<div class="container-fluid mt-20">
    <div class="row justify-content-center">
        <div class="col-md-8 col-xxl-6">
            <?php
            if ($zamowienie->status == 'oplacone') {
                ?>
            <div class="alert alert-success alert-block text-center m-t-20"><?=$this->Translation->get('platnosc.DziekujemyZaOplacenieZamowieniaNr{0}', [(!empty($zamowienie->numer)?$zamowienie->numer:$zamowienie->id)], true)?></div>
            <?php
            } else {
                if(!empty($zamowienie->platnosc) && $zamowienie->platnosc[0]->status=='odrzucona'){
                    ?>
            <div class="alert alert-danger alert-block text-center m-t-20"><?=$this->Translation->get('platnosc.PlatnoscOdrzucona{0}', [$this->Html->link($this->Translation->get('labels.zaplacPonownie'),['controller'=>'Cart','action'=>'orderCompleted',$zamowienie->token],['class'=>'btn btn-success'])], true)?></div>
            <?php
                }else{
                ?>
            <div class="alert alert-success alert-block text-center m-t-20" page-reload-all="5"><?=$this->Translation->get('platnosc.TwojaPlatnoscMusiZostacZweryfikowana',[], true)?></div>
            <?php
                } }
            ?>
        </div>
    </div>
</div>
