<div class="modal" id="cart-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title difFont"><?=$this->Translation->get('koszyk.popUpItemHasBeenAddedHeader')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p><?=$this->Translation->get('koszyk.popUpItemHasBeenAddedInfo')?></p>
        <?php
        $items=$cartItems['items'];
        if(!empty($items)){
  ?>
        <div id="cartPreview" class="addPopup">
    <div class="previewItemsContainer">
        <div class="previewItemsTable">
    <?php 
    $cartZestawy=[];
    foreach ($items as $itemKey => $item){
        if(!empty($item['zestaw_bonus'])){
            continue;
        }
        if(!empty($item['zestaw_key'])){
            $cartZestawy[$item['zestaw_key']][$itemKey]=$item;
            continue;
        }
        $wersjaImgSrc = $displayPath['img'] . 'noPhoto.png';
        $wersjaAlt = $item['nazwa'];
        if (!empty($item['zdjecie'])) {
            if(!empty($item['karta_podarunkowa']) && file_exists($filePath['karta_podarunkowa'] . $item['zdjecie'])){
                    $wersjaImgSrc = $displayPath['karta_podarunkowa'] . $item['zdjecie'];
                }
                elseif (file_exists($filePath['towar'] . $this->Txt->getKatalog($item['zdjecie']['id']) . DS . 'thumb_' . $item['zdjecie']['plik'])) {
                $wersjaImgSrc = \Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'image', $item['zdjecie']['id'], 'thumb_' . $item['zdjecie']['plik']]);
            }
        }
        ?>
    <div class="item-preview">
        <div class="image"><img src="<?= $wersjaImgSrc ?>" alt="<?= $wersjaAlt ?>"/></div>
        <div class="name"><?=$item['nazwa']?></div>
        <div class="ilosc"><?=$item['ilosc']?> <?=$item['jednostka_str']?></div>
        <div class="cena"><?=(!empty($item['gratis'])?t('koszyk.gratis'):$this->Txt->cena($item['cena_razem_'.\Cake\Core\Configure::read('ceny.rodzaj')]))?></div>
    </div>
    <?php
    } 
    ?>
        </div>
      <?php  if(!empty($cartZestawy)){
        ?>
            <div class="preview-zestaw-container">
            <?php
        foreach ($cartZestawy as $zestawKey => $zestawItems){
            ?>
            <div class="preview-zestaw">
            <?php
            $zestawIlosc=1;
            $zestawWartosc=0;
            $zi=1;
            foreach ($zestawItems as $itemKey => $item){
                $zestawIlosc=$item['ilosc'];
                $zestawWartosc+=$item['cena_razem_'.\Cake\Core\Configure::read('ceny.rodzaj')];
                $wersjaImgSrc = $displayPath['img'] . 'noPhoto.png';
        $wersjaAlt = $item['nazwa'];
        if (!empty($item['zdjecie'])) {
            if (file_exists($filePath['towar'] . $this->Txt->getKatalog($item['zdjecie']['id']) . DS . 'thumb_' . $item['zdjecie']['plik'])) {
                $wersjaImgSrc = \Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'image', $item['zdjecie']['id'], 'thumb_' . $item['zdjecie']['plik']]);
            }
        }
                ?>
                <div class="preview-z-item <?=(($zi== count($zestawItems))?'last':'')?>" title="<?=$item['nazwa']?>" data-toggle="tooltip">
                    <div class="image"><img src="<?= $wersjaImgSrc ?>" alt="<?= $wersjaAlt ?>"/></div>
                </div>
                <?php
                $zi++;
            }
            ?>
                <div class="ilosc"><?=$zestawIlosc?> <?=$this->Translation->get('labels.szt')?></div>
                <div class="cena"><?=$this->Txt->cena($zestawWartosc)?></div>
            </div>
                <?php
        }
        ?>
            </div>
                <?php
    }
    ?>
    </div>
</div>
<?php
} ?>
        <p class="text-right"><?=$this->Translation->get('koszyk.cartTotalValue{0}',[$this->Html->tag('b',$this->Txt->cena($cartItems['total_value']))])?></p>
        <?php 
        if(!empty($cartItems['rabat'])){
            ?>
        <p><?=$this->Translation->get('koszyk.currentDiscount{0}',[$cartItems['rabat'].'%'])?></p>
        <p><?=$this->Translation->get('koszyk.cartTotalValueAfterDiscount{0}',[$this->Txt->cena($cartItems['rabat_value'])])?></p>
        <?php
        } 
        if(!empty($cartItems['next_rabat'])){
            ?>
        <p><?=$this->Translation->get('koszyk.toReachThe{0}DiscountYouAreMissing{1}',[$cartItems['next_rabat'].'%',$this->Txt->cena(($cartItems['next_kwota']-$cartItems['total_value']))])?></p>
        <?php
        }
        ?>
      </div>
      <div class="modal-footer text-center">
          <div class="row">
              <div class="col-12 text-right">
                <button type="button" class="btn btn-outline-dark xs-btn-block" data-dismiss="modal"><?=$this->Translation->get('koszyk.popUpcontinueShopping')?></button>
                <a href="<?= \Cake\Routing\Router::url(['controller'=>'Cart','action'=>'index'])?>" class="btn btn-color-1 xs-btn-block"><?=$this->Translation->get('koszyk.popUpgoToShoppingCart')?></a>
              </div>
          </div>
        </div>
    </div>
  </div>
</div>