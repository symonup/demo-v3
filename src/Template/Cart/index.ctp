<?php
$priceType = \Cake\Core\Configure::read('ceny.rodzaj');
$totalBrutto = 0;
$lp = 1;
?>
<div class="container cart-container <?= (!empty($_GET['order_open']) ? 'hide' : '') ?>">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="pageHead border-1-bottom-e7"><?= $this->Translation->get('koszyk.cartHeader') ?></h1>
        </div>
    </div>
    <?php if (!empty($cartItems['items'])) { ?>
        <div class="row">
            <div class="col-12 col-lg-7">
                <div class="row">
                    <div class="col-12">
                        <a href="<?= \Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'index']) ?>" class="btn btn-success btn-sm"><?= $this->Translation->get('koszyk.dodajKolejnaPozycje') ?></a>
                    </div>
                </div>
                <?php 
                if(!empty($cartItems['preorder'])){
                    ?>
                <div class="row mt-10">
                    <div class="col-12">
                        <div class="alert alert-danger"><?= $this->Translation->get('koszyk.preorderInfo',[],true) ?></div>
                    </div>
                </div>
                <?php
                }
                ?>
                <table class="table koszyk-table">
                    <tbody>
                        <?php
                        $rabat = 0;
                        $zestawyInCart = [];
                        if (!empty($cartItems['items'])) {
                            foreach ($cartItems['items'] as $itemKey => $item) {
                                if (!empty($item['zestaw_id'])) {
                                    $zestawyInCart[$item['zestaw_key']][$itemKey] = $item;
                                    continue;
                                }
                                echo $this->element('default/order/cart_item', ['itemKey' => $itemKey, 'item' => $item, 'lp' => $lp++]);
                                $razem = $item['ilosc'] * $item['cena_z_rabatem'];
                                $totalBrutto += $razem;
                            }
                            $rabatKwotaZestaw = 0;
                            if (!empty($zestawyInCart)) {
                                $zestawCount = 0;
                                foreach ($zestawyInCart as $zestawKey => $zestawItems) {
                                    $zestawCount++;
                                    if ($zestawCount > 1) {
//                                        echo $this->element('default/order/cart_item_separate');
                                    }
                                    $zi = 1;
                                    $bonus = [];
                                    $bonusKey = null;
                                    $zestawWartosc = 0;
                                    $zestawId = null;
                                    $zestawTowarId = null;
                                    $zestawIlosc = 0;
                                    $razemSztuka = 0;
                                    $zestawSklad = [];
                                    $zestawImages = [];
                                    foreach ($zestawItems as $zestawItemKey => $zestawItem) {
                                        if(key_exists('zdjecie', $zestawItem)){
                                        $zestawImages[]=$zestawItem['zdjecie'];
                                        }
                                        if (!empty($zestawItem['zestaw_bonus'])) {
                                            $bonus = $zestawItem;
                                            $bonusKey = $zestawItemKey;
                                            if ($zestawItem['typ'] == 'kwota') {
                                                $rabatKwotaZestaw += ($zestawItem['wartosc'] * $zestawItem['zestaw_ilosc']);
                                            }
                                            continue;
                                        } else {
                                            if (empty($zestawId)) {
                                                $zestawId = $zestawItem['zestaw_id'];
                                            }
                                            if (empty($zestawTowarId)) {
                                                $zestawTowarId = $zestawItem['towar_id'];
                                            }
                                            if (empty($zestawIlosc)) {
                                                $zestawIlosc = $zestawItem['ilosc'];
                                            }
                                            $zestawSklad[] = $zestawItem['nazwa'];
                                        }
                                        ?>

                                        <?php
                                        $razem = $zestawItem['ilosc'] * $zestawItem['cena_z_rabatem'];
                                        $razemSztuka += $zestawItem['cena_z_rabatem'];
                                        $zestawWartosc += $zestawItem['cena_podstawowa'];
                                        $totalBrutto += $razem;
                                        $zi++;
                                    }
//                                    $razemSztuka = ($razemSztuka - ($razemSztuka * ($bonus['wartosc'] / 100)));
                                    $zestawItem = [
                                        'nazwa' => join('<br/>', $zestawSklad),
                                        'ilosc' => $zestawIlosc,
                                        'cena_z_rabatem' => $razemSztuka,
                                        'cena_za_sztuke' => $razemSztuka,
                                        'towar_id' => $zestawTowarId,
                                        'zestaw_id' => $zestawId,
                                        'zestaw_key' => $zestawKey,
                                        'zestaw_nazwa' => (!empty($bonus['zestaw_nazwa']) ? $bonus['zestaw_nazwa'] : $this->Translation->get('labels.Zestaw')),
                                        'zdjecia'=>$zestawImages
                                    ];
                                    echo $this->element('default/order/cart_item', ['zi' => 1, 'zestawKey' => $zestawKey, 'zestawItems' => $zestawItems, 'itemKey' => $bonusKey, 'item' => $zestawItem, 'lp' => $lp]);

//                                     echo $this->element('default/order/cart_item_zestaw_bonus', ['zi' => $zi, 'zestawKey' => $zestawKey, 'zestawItems' => $zestawItems, 'itemKey' => $bonusKey, 'item' => $bonus, 'lp' => $lp,'zestawWartosc'=>$zestawWartosc]);
                                    $lp++;
                                }
                            }
                        }
                        $totalNetto = $cartItems['total_netto'];
                        ?>
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-12 text-right">
                            <button type="button" class="btn btn-outline-secondary btn-sm remove-all-from-cart" confirm-btn-yes="<?= $this->Translation->get('labels.Yes') ?>" confirm-btn-no="<?= $this->Translation->get('labels.No') ?>" confirm-message="<?= $this->Translation->get('koszyk.CzyNapewnoChceszUsunacWszystkoZKoszyka') ?>"><i class="far fa-trash-alt"></i> <?= $this->Translation->get('koszyk.UsunWszystkie') ?></button>
                    </div>
                </div>
                <div class="row mt-20">
                    <div class="col-12 col-md-12 text-right">
                <label class="fw-semi-bold fs-14 xs-cart-hidden"><?=$this->Translation->get('koszyk.KodRabatowy')?></label>
                <input class="kod-rabatowy" type="text" placeholder="<?=$this->Translation->get('koszyk.WpiszKodRabatowy')?>"/>
                <button class="kod-rabatowy-btn" type="button"><?=$this->Translation->get('labels.OK')?></button>
                    </div>
                </div>
             <?php 
             /*   <div class="row mt-20">
                    <div class="col-12 col-md-12">
                        <?=$this->Translation->get('koszyk.kartaPodarunkowaInfo',['form'=>$this->element('default/karta_kod_form')],true)?>
                    </div>
                </div> */ ?>
            </div>
            <div class="col-12 col-lg-5 md-border-1-left-ab">
                <div class="cartSummarySmallHead"><?= $this->Translation->get('koszyk.Podsumowanie') ?></div>
                <div id="orderFormContainer">
                <?= $this->element('default/order/rodzaje_platnosci', ['totalBrutto' => $totalBrutto]) ?>
                </div>
                <input type="hidden" id="totalBruttonIn" value="<?= $totalBrutto ?>"/>
            </div>
        </div>
    <?php
             if(!empty($aktywneKody)){
                 ?>
                <div class="row mt-20">
                    <div class="col-12 col-md-12">
                        <div class="pageHead border-1-bottom-e7"><?=t('koszyk.aktywneKody')?></div>
                        <div>
                            <?php foreach ($aktywneKody as $kodItem){
                                echo $this->element('default/active_kod',['kod'=>$kodItem]);
                            } ?>
                        </div>
                    </div>
                </div>
                <?php
             } ?>
    </div>
<?php if(!empty($polecane) && $polecane->count()>0){?>
<div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="koszykPolecaneHead"><?= $this->Translation->get('koszyk.polecaneProdukty') ?></div>
                    </div>
                </div>
                <div class="row mt-15">
                    <?php
                    foreach ($polecane as $polecany) {
                        ?>
                        <div class="col-12 col-md-4 col-sm-6 col-lg-3 item-list">
                            <?= $this->element('default/item_min', ['towar' => $polecany, 'showPegi' => false,'addAsTr'=>true]) ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
    <?php
}
} else {
    ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-warning">
                <?= $this->Translation->get('koszyk.noItemsInCart', [], true) ?>
            </div>
        </div>
    </div>
    </div>
<?php }
?>
    <script type="text/javascript">
    kosztyKraje=<?= (!empty($kosztyKraje)? json_encode($kosztyKraje):'new Object()')?>;
    kosztyKrajeDef=<?php  $_kd=\Cake\Core\Configure::read('wysylka.krajKosztDodatkowy'); echo (!empty($_kd)?$_kd:'0')?>;
    krajDef='<?= \Cake\Core\Configure::read('wysylka.krajPodstawowy')?>';
    </script>
    <div id="reloadPage">
        <div>
           <div class="sk-circle">
  <div class="sk-circle1 sk-child"></div>
  <div class="sk-circle2 sk-child"></div>
  <div class="sk-circle3 sk-child"></div>
  <div class="sk-circle4 sk-child"></div>
  <div class="sk-circle5 sk-child"></div>
  <div class="sk-circle6 sk-child"></div>
  <div class="sk-circle7 sk-child"></div>
  <div class="sk-circle8 sk-child"></div>
  <div class="sk-circle9 sk-child"></div>
  <div class="sk-circle10 sk-child"></div>
  <div class="sk-circle11 sk-child"></div>
  <div class="sk-circle12 sk-child"></div>
</div>
        </div>
    </div>