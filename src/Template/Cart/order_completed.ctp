<div class="container mt-50">
    <div class="row justify-content-center">
        <div class="col col-lg-6 col-md-8">
            <div class="alert alert-success">
                <?= $this->Translation->get('koszyk.orderCompletedInfoNumber{0}Value{1}', [(!empty($zamowienie->numer) ? $zamowienie->numer : $zamowienie->id), $this->Txt->cena($zamowienie->wartosc_razem)], true) ?>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col col-lg-6 col-md-8">
            <?php
            $platnoscData = $zamowienie->rodzaj_platnosci->toArray();
            $platnoscData['nr_zamowienia'] = (!empty($zamowienie->numer) ? $zamowienie->numer : $zamowienie->id);
            $platnoscData['kwota'] = $this->Txt->cena($zamowienie->wartosc_razem);
            if (!empty($zamowienie->wysylka)) {
                foreach ($zamowienie->wysylka->toArray() as $wysylkaField => $wysylka) {
                    $platnoscData['wysylka_' . $wysylkaField] = $wysylka;
                }
            }
            if (!empty($zamowienie->platnosc)) {
                $platnoscData['zaplac'] = $this->element('default/eplatnosci/' . $zamowienie->platnosc[0]->rodzaj_platnosci_id);
            }
            echo $this->Translation->get('platnosci_' . $zamowienie->rodzaj_platnosci->id . '.SposobPlatnosciInfo', $platnoscData, true);
            ?>
        </div>
    </div>
</div>
<?php 
echo $this->element('default/order/data_layer');
if(!empty($zamowienie->zgoda)){
    echo Cake\Core\Configure::read('script.zamowienieZgoda');
}
if(!empty($zamowienie->zgoda2)){
    echo Cake\Core\Configure::read('script.zamowienieZgoda2');
}?>
