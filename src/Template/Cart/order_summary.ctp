<?php
$priceType = \Cake\Core\Configure::read('ceny.rodzaj');
$totalBrutto = 0;
$lp = 1;
?>
<div class="container">
    <div class="row mb-30">
        <div class="col-12">
            <h1 class="pageHead"><?= $this->Translation->get('koszyk.orderSummaryHeader') ?></h1>
        </div>
    </div>
    <div class="row mb-30">
        <div class="col-12 col-md-4">
            <h3 class="fs-16 fw-semi-bold"><?= $this->Translation->get('koszyk.SkladaszZamowienieJako') ?></h3>
            <div>
                <?= $this->Txt->printUserInfo($zamowienie) ?>
            </div>
        </div>
        <div class="col-12 col-md-4">
            <h3 class="fs-16 fw-semi-bold"><?= $this->Translation->get('koszyk.DaneDoWysylki') ?></h3>
            <div>
                <?= $this->Txt->printUserInfo($zamowienie, '<br/>', 'wysylka_') ?><br/>
                <?= $this->Txt->printAddres($zamowienie, '<br/>', 'wysylka_') ?>
            </div>
        </div>
        <?php
        if (!empty($zamowienie['faktura'])) {
            ?>
            <div class="col-12 col-md-4">
                <h3 class="fs-16 fw-semi-bold"><?= $this->Translation->get('koszyk.DaneDoFaktury') ?></h3>
                <div>
                    <?= $this->Txt->printUserInfo($zamowienie, '<br/>', 'faktura_') ?><br/>
                    <?= $this->Txt->printAddres($zamowienie, '<br/>', 'faktura_') ?>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
    <?php if (!empty($zamowienie['zamowienie_towar'])) { ?>
        <div class="row">
            <div class="col-lg-12">
                <h3 class="pageHead"><?= $this->Translation->get('koszyk.ZawartoscTwojegoKoszyka') ?></h3>
                <table class="table koszyk-table order-sumary">
                    <thead>
                        <tr class="xs-cart-hidden">
                            <th class="lp"><?= $this->Translation->get('koszyk.thLp') ?></th>
                            <th colspan="2" class="text-left"><?= $this->Translation->get('koszyk.thName') ?></th>
                            <th class="text-left"><?= $this->Translation->get('koszyk.thCode') ?></th>
                            <th class="text-center"><?= $this->Translation->get('koszyk.thDostepnosc') ?></th>
                            <th class="text-center"><?= $this->Translation->get('koszyk.thPriceByItem') ?></th>
                            <th class="text-center"><?= $this->Translation->get('koszyk.thOrderedQuantity') ?></th>
                            <th class="text-right"><?= $this->Translation->get('koszyk.thPriceTogether') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $rabat = 0;
                        if (!empty($userRabat)) {
                            if ($userRabat['typ'] == 'flat') {
                                $rabat = $userRabat['flat'];
                            } elseif ($userRabat['typ'] == 'dist') {
                                if (!empty($cartItems['rabat'])) {
                                    $rabat = $cartItems['rabat'];
                                }
                            }
                        }
                        $zestawyInCart = [];
                        if (!empty($zamowienie['zamowienie_towar'])) {
                            foreach ($zamowienie['zamowienie_towar'] as $itemKey => $item) {
                                if (!empty($item['zestaw_id'])) {
                                    $zestawyInCart[$item['zestaw_key']][$itemKey] = $item;
                                    continue;
                                }
                                echo $this->element('default/order/cart_item', ['itemKey' => $itemKey, 'item' => $item, 'lp' => $lp++, 'noEdit' => true]);
                            }
                            if (!empty($zestawyInCart)) {
                                foreach ($zestawyInCart as $zestawKey => $zestawItems) {
                                    $zi = 1;
                                    $bonus=[];
                                    $bonusKey=null;
                                    $zestawWartosc=0;
                                    foreach ($zestawItems as $zestawItemKey => $zestawItem) {

                                         if(!empty($zestawItem['zestaw_bonus'])){
                                            $bonus=$zestawItem;
                                            $bonusKey=$zestawItemKey;
                                            if($zestawItem['typ']=='kwota'){
                                                $rabatKwotaZestaw+=($zestawItem['wartosc']*$zestawItem['zestaw_ilosc']);
                                            }
                                          continue;
                                        }else{
                                        $zestawWartosc+=(1 * $zestawItem['ilosc_w_kartonie']) * $zestawItem['cena_podstawowa'];
                                            echo $this->element('default/order/cart_item', ['zi' => $zi, 'zestawKey' => $zestawKey, 'zestawItems' => $zestawItems, 'itemKey' => $zestawItemKey, 'item' => $zestawItem, 'lp' => $lp, 'noEdit' => true]);
                                        }
                                        $zi++;
                                    }
                                    if(!empty($zestawBonusy[$zestawKey])){
                                        echo $this->element('default/order/cart_item_zestaw_bonus', ['zi' => $zi, 'zestawKey' => $zestawKey, 'zestawItems' => $zestawItems, 'itemKey' => $bonusKey, 'item' => $zestawBonusy[$zestawKey], 'lp' => $lp,'zestawWartosc'=>$zestawWartosc, 'noEdit' => true]);
                                    }
                                     $lp++;
                                }
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="col-12 col-md-5">
                <?php
                if (!empty($zamowienie['uwagi'])) {
                    ?>
                    <h3 class="fs-16 fw-semi-bold"><?= $this->Translation->get('koszyk.TwojeUwagi') ?></h3>
                    <div class="like-textarea">
                        <?= nl2br($zamowienie['uwagi']) ?>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="col-12 col-md-7 text-right">
                <div class="text-right no-warp totalNetto">
                    <span><?= $this->Translation->get('koszyk.wartoscProduktowNetto') ?>:</span>
                    <span id="cartTotalNetto"><?= $this->Txt->cena($zamowienie['wartosc_produktow_netto']) ?></span>
                </div>
                <div class="text-right no-warp totalBrutto">
                    <span><?= $this->Translation->get('koszyk.wartoscProduktowBrutto') ?>:</span>
                    <span id="cartTotalBrutto"><?= $this->Txt->cena($zamowienie['wartosc_produktow']) ?></span>
                </div>
                <div class="text-right fs-18 fw-semi-bold pd-r-15">
                    <?= $zamowienie['platnosc_wysylka'] ?>: <?= $this->Txt->cena($zamowienie['wysylka_koszt']) ?>
                </div>
                <?php 
                if(!empty($zamowienie['rabat_zestaw'])){
                    ?>
                <div class="text-right no-warp rabatKwotaZestaw">
                    <span><?= $this->Translation->get('koszyk.rabatZaZestaw') ?>:</span>
                    <span id="cartTotalBrutto">-<?= $this->Txt->cena($zamowienie['rabat_zestaw']) ?></span>
                </div>
                <?php
                }
                if(!empty($rabatKodHtml)){
                    echo $rabatKodHtml;
                }
                ?>
            </div>
        </div>
        <div class="row justify-content-center align-items-end">
            <div class="col-12 col-md-6 text-left">
                <a href="<?= \Cake\Routing\Router::url(['controller' => 'Cart', 'action' => 'index']) ?>" class="btn btn-outline-success btn-md"><i class="fa fa-chevron-left"></i> <?= $this->Translation->get('koszyk.cartContinueShopping') ?></a>
            </div>
            <div class="col-12 col-md-6 text-right">
                <hr style="max-width: 400px;margin-right:  0;"/>
                <div class="sumary-total-to-pay fs-18 fw-semi-bold pd-r-15">
                    <?php 
                    $totalToPay=$zamowienie['wartosc_razem']-$zamowienie['rabat_zestaw'];
                    if(!empty($zamowienie['kod_rabatowy']) && !empty($zamowienie['kod_rabatowy_wartosc'])){
                        if($zamowienie['kod_rabatowy_typ']=='kwota'){
                            $totalToPay=$totalToPay-$zamowienie['kod_rabatowy_wartosc'];
                        }else{
                            $totalToPay=$totalToPay-($totalToPay*($zamowienie['kod_rabatowy_wartosc']/100));
                        }
                    }
                    ?>
                    <?= $this->Translation->get('koszyk.totalToPay{0}', [$this->Html->tag('span', $this->Txt->cena($totalToPay), ['class' => 'fs-24 fw-bold'])]) ?>

                </div>
                <div class="text-right pd-r-15"><a href="<?= \Cake\Routing\Router::url(['action'=>'submitOrder'])?>" id="submit-order-a" class="btn btn-color-1"><?= $this->Translation->get('koszyk.zlozZamowienie') ?><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></a></div>

            </div>
        </div>
    </div>
    <?php
}
?>
<div class="submitInProgres">
    <div>
        <div class="sk-cube-grid">
  <div class="sk-cube sk-cube1"></div>
  <div class="sk-cube sk-cube2"></div>
  <div class="sk-cube sk-cube3"></div>
  <div class="sk-cube sk-cube4"></div>
  <div class="sk-cube sk-cube5"></div>
  <div class="sk-cube sk-cube6"></div>
  <div class="sk-cube sk-cube7"></div>
  <div class="sk-cube sk-cube8"></div>
  <div class="sk-cube sk-cube9"></div>
</div>
    </div>
    <div>
        <?=$this->Translation->get('success.ZamowienieJestWTrakciePrzetwarzania',[],true)?>
    </div>
</div>