<div class="row">
    <div class="col-12">
        <div class="card border-none">
            <h4 class="card-header fs-14 fw-semi-bold">
                <?= $this->Translation->get('uzytkownik.orderListHeader') ?>
            </h4>
            <div class="card-body padding-left-0 padding-right-0">
                 <?php
                if (!empty($zamowienia) && $zamowienia->count() > 0) {
                    ?>
                    <div>
                        <?=$this->element('user/orders_table',['orders'=>$zamowienia])?>
                    </div>
                    <?php
                } else {
                    ?>
                    <div>
                        <?= $this->Translation->get('uzytkownik.homeBrakZlozonychZamowien') ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>
<?=$this->element('default/paginator')?>