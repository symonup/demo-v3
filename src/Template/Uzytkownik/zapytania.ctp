<div class="row">
    <div class="col-12">
        <div class="card border-none">
            <h4 class="card-header fs-14 fw-semi-bold">
                <?= $this->Translation->get('uzytkownik.zapytaniaListHeader') ?>
                <a class="pull-right fs-13 fw-regular fc-black" href="<?= \Cake\Routing\Router::url(['action' => 'zapytanieAdd']) ?>"><?= $this->Translation->get('labels.noweZapytanie') ?></a>
            </h4>
            <div class="card-body padding-left-0 padding-right-0">
                 <?php
                if (!empty($zapytania) && $zapytania->count() > 0) {
                    ?>
                    <div>
                        <?=$this->element('user/zapytania_table',['zapytania'=>$zapytania])?>
                    </div>
                    <?php
                } else {
                    ?>
                    <div>
                        <?= $this->Translation->get('uzytkownik.zapytaniaListZlozonychZapytan') ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>
<?=$this->element('default/paginator')?>