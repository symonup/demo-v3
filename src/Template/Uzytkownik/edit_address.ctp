<div class="card">
    <h4 class="card-header">
        <?= $this->Translation->get('uzytkownik.editAddressHeader') ?>
        <a href="<?= Cake\Routing\Router::url(['action' => 'addressList']) ?>" class="btn btn-sm btn-success pull-right"><i class="fa fa-chevron-left"></i> <?= $this->Translation->get('uzytkownik.returnToAddressList') ?></a>
    </h4>

    <div class="card-body">
        <?php
        echo $this->Form->create($uzytkownikAdres,['id'=>'wysylka-form']);
        echo $this->element('user/address_form', ['typ' => 1]);
        echo $this->Form->end();
        ?>
    </div>
</div>