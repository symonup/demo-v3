<div class="row justify-content-start mb-20">
    <div class="col-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-12">
                <h1 class="fs-18 fw-semi-bold"><?= $this->Translation->get('uzytkownik.homeHeader') ?></h1>
                <div class="fs-14 fw-regular"><?= $this->Translation->get('uzytkownik.homeInfo', $user, true) ?></div>
            </div>
        </div>
        <div class="row mt-20 justify-content-start">
            <div class="col-12 col-md-12 col-lg-6">
                <div class="card border-none">
                    <h4 class="card-header fs-14 fw-semi-bold">
                        <?= $this->Translation->get('uzytkownik.homeDaneUzytkownika') ?>
                        <a class="pull-right fs-13 fw-regular fc-black" href="<?= \Cake\Routing\Router::url(['action' => 'info']) ?>"><?= $this->Translation->get('labels.edytuj') ?></a>
                    </h4>
                    <div class="card-body card-body-bordred">
                        <?= $uzytkownik['imie'] ?> <?= $uzytkownik['nazwisko'] ?><br/>
                        <?= $uzytkownik['email'] ?>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-12 col-lg-6">
                <div class="card border-none">
                    <h4 class="card-header fs-14 fw-semi-bold">
                        <?= $this->Translation->get('uzytkownik.homeSubstrykcjaNewslettera') ?>
                        <a class="pull-right fs-13 fw-regular fc-black" href="<?= \Cake\Routing\Router::url(['action' => 'info']) ?>"><?= $this->Translation->get('labels.edytuj') ?></a>
                    </h4>
                    <div class="card-body card-body-bordred">
                        <?= (!empty($uzytkownik['newsletter']) ? $this->Translation->get('uzytkownik.JestesZapisanyNaNewsletter') : $this->Translation->get('uzytkownik.NieJestesZapisanyNaNewsletter')) ?><br/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-20 justify-content-start">
            <div class="col-12 col-md-12 col-lg-6">
                <div class="card border-none">
                    <h4 class="card-header fs-14 fw-semi-bold">
                        <?= $this->Translation->get('uzytkownik.homeDomyslnyAdresWysylki') ?>
                        <a class="pull-right fs-13 fw-regular fc-black" href="<?= \Cake\Routing\Router::url(['action' => (!empty($uzytkownik->uzytkownik_adres_wysylka) ? 'editAddress' : 'addAddress'), (!empty($uzytkownik->uzytkownik_adres_wysylka) ? $uzytkownik->uzytkownik_adres_wysylka[0]->id : null)]) ?>"><?= $this->Translation->get('labels.' . (!empty($uzytkownik->uzytkownik_adres_wysylka) ? 'edytuj' : 'dodaj')) ?></a>
                    </h4>
                    <div class="card-body card-body-bordred">
                        <?php
                        if (!empty($uzytkownik->uzytkownik_adres_wysylka)) {
                            echo $this->Txt->printUserInfo($uzytkownik->uzytkownik_adres_wysylka[0]->toArray(), '<br/>') . '<br/>';
                            echo $this->Txt->printAddres($uzytkownik->uzytkownik_adres_wysylka[0]->toArray(), '<br/>');
                        } else {
                            echo $this->Translation->get('uzytkownik.NieZostalPodanyDomyslnyAdresWysylki');
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-12 col-lg-6">
                <div class="card border-none">
                    <h4 class="card-header fs-14 fw-semi-bold">
                        <?= $this->Translation->get('uzytkownik.homeDomyslnyAdresRachunku') ?>
                        <a class="pull-right fs-13 fw-regular fc-black" href="<?= \Cake\Routing\Router::url(['action' => 'invoiceAddress']) ?>"><?= $this->Translation->get('labels.' . (!empty($uzytkownik->uzytkownik_faktura) ? 'edytuj' : 'dodaj')) ?></a>
                    </h4>
                    <div class="card-body card-body-bordred">
                        <?php
                        if (!empty($uzytkownik->uzytkownik_faktura)) {
                            echo $this->Txt->printUserInfo($uzytkownik->uzytkownik_faktura->toArray(), '<br/>') . '<br/>';
                            echo $this->Txt->printAddres($uzytkownik->uzytkownik_faktura->toArray(), '<br/>');
                        } else {
                            echo $this->Translation->get('uzytkownik.NieZostalPodanyDomyslnyAdresRachunku');
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <?php /*  <div class="col-12 col-md-12 col-lg-5 md-border-2-left sm-border-2-top">
        <h2><?= $this->Translation->get('uzytkownik.programPartnerskiHeader') ?></h2>
        <div><?= $this->Translation->get('uzytkownik.programPartnerskiInfo', $user, true) ?></div>
        <div class="userAcount-bonus"><?= $this->Translation->get('uzytkownik.programPartnerskiTwojeZlotowki') ?><div class="opinia-label" id="bonus_value"><?= (empty($user['bonusy_suma']) ? '0' : round($user['bonusy_suma'], 2)) ?></div><span><?= Cake\Core\Configure::read('currencysSymbol.' . Cake\Core\Configure::read('currencyDefault')) ?></span></div>
        <hr/>
        <h2><?= $this->Translation->get('kartaPodarunkowa.uzytkownikHeader') ?></h2>
        <div><?= $this->Translation->get('kartaPodarunkowa.uzytkownikInfo', $user, true) ?></div>
        <?=$this->element('default/karta_kod_form')?>
    </div> */ ?>
</div>