<div class="card">
            <h4 class="card-header">
<?=$this->Translation->get('uzytkownik.cartsListHeader')?>
            </h4>
        
  <div class="card-body">
    <?php 
    if(!empty($allCarts)){
        ?>
      <table class="table table-condensed table-hover">
          <thead>
              <tr>
                  <th class="text-center"><?=$this->Translation->get('uzytkownik.cartsListCreateDateTh')?></th>
                  <th class="text-center"><?=$this->Translation->get('uzytkownik.cartsListTotalValueTh')?></th>
                  <th class="text-center"><?=$this->Translation->get('uzytkownik.cartsListLastUpdateTh')?></th>
                  <th class="text-center"><?=$this->Translation->get('uzytkownik.cartsListActiveTh')?></th>
                  <th></th>
              </tr>
          </thead>
          <tbody>
              <?php
        foreach ($allCarts as $cart){
          ?>
              <tr>
                  <td class="text-center"><?=$cart['created']?></td>
                  <td class="text-center"><?=$this->Txt->cena($cart['items']['total_brutto'])?></td>
                  <td class="text-center"><?=$cart['modified']?></td>
                  <td class="text-center"><?=(!empty($cart['active'])?$this->Html->tag('span',$this->Html->tag('i','',['class'=>'fa fa-check']),['class'=>'cart-list-is-active','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Translation->get('uzytkownik.cartListThisCartIsActive')]):'')?></td>
                  <td class="text-right">
                      <?= $this->Html->link('<i class="fa fa-angle-double-down"></i>', 'javascript:void(0);',['escape'=>false,'class'=>'btn btn-outline-primary btn-sm cart-list-show-detalis','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Translation->get('labels.showDetalis')]) ?>
                      <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'activateCart', $cart['id']],['escape'=>false,'class'=>'btn btn-outline-dark btn-sm','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Translation->get('uzytkownik.CartListContinueOrder')]) ?>
                    <?= $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'deleteCart', $cart['id']], ['confirm-btn-yes'=>$this->Translation->get('labels.Yes'),'confirm-btn-no'=>$this->Translation->get('labels.No'),'confirm-message' => $this->Translation->get('uzytkownik.areYouSureYouWantToDeleteCart'),'escape'=>false,'class'=>'btn btn-outline-danger btn-sm confirm-link','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Translation->get('labels.Delete')]) ?>
               </td>
              </tr>
              <tr class="cart-list-subtable">
                  <td colspan="5">
          <div>
              <table class="table">
                  <thead>
                  <th><?=$this->Translation->get('uzytkownik.cartListItemNameTh')?></th>
                  <th><?=$this->Translation->get('uzytkownik.cartListItemCodeTh')?></th>
                  <th class="text-center"><?=$this->Translation->get('uzytkownik.cartListItemPriceTh')?></th>
                  <th class="text-center"><?=$this->Translation->get('uzytkownik.cartListItemQuantityTh')?></th>
                  <th class="text-center"><?=$this->Translation->get('uzytkownik.cartListItemTotalValueTh')?></th>
                  </thead>
                  <tbody>
              <?php
          foreach ($cart['items']['items'] as $cartItem){
              ?>
                      <tr>
                          <td><?=$cartItem['nazwa']?></td>
                          <td><?=(!empty($cartItem['kod'])?$cartItem['kod']:'')?></td>
                          <td class="text-center"><?=$this->Txt->cena($cartItem['cena_za_sztuke_brutto'])?></td>
                          <td class="text-center"><?=($cartItem['ilosc']*$cartItem['ilosc_w_kartonie'])?></td>
                          <td class="text-center"><?=$this->Txt->cena($cartItem['cena_razem_brutto'])?></td>
                      </tr>
              <?php
          }
          ?>
                  </tbody>
              </table>
                  </div>
                  </td>
          </tr>
          <?php
        }
        ?>
          </tbody>
      </table>
      
      <?php
    }
    else{
        ?>
      <div class="row justify-content-center">
          <div class="col-lg-8 col-md-10">
              <div class="alert alert-info"><?=$this->Translation->get('uzytkownik.noCartsInfo',[],true)?></div>
          </div>
      </div>
      <?php
    }
    ?>
  </div>
</div>