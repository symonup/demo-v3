<div class="card">
    <h4 class="card-header">
        <?= $this->Translation->get('uzytkownik.orderDetalisHeader') ?>
        <a href="<?= Cake\Routing\Router::url(['action' => 'orders']) ?>" class="btn btn-sm btn-success pull-right"><i class="fa fa-chevron-left"></i> <?= $this->Translation->get('uzytkownik.returnToOrdersList') ?></a>
    </h4>

    <div class="card-body">
        <div class="row">
            <div class="col-md-4">
                <h5><?= $this->Translation->get('uzytkownik.orderDetalis') ?></h5>
                <ul class="no-point">
                    <li><?= $this->Translation->get('uzytkownik.orderDetalisOrderNumber{0}', [(!empty($zamowienie->numer)?$zamowienie->numer:$zamowienie->id)]) ?></li>
                    <li><?= $this->Translation->get('uzytkownik.orderDetalisOrderDate{0}', [$zamowienie->data->format('Y-m-d H:i')]) ?></li>
                    <li><?= $this->Translation->get('uzytkownik.orderDetalisOrderState{0}', [$this->Translation->get('statusy_zamowien.'.$zamowienie->status)]) ?></li>
                    <li><?= $this->Translation->get('uzytkownik.orderDetalisOrderTotalValue{0}', [$this->Txt->cena($zamowienie->wartosc_razem, (!empty($zamowienie->walutum) ? $zamowienie->walutum->symbol : null))]) ?></li>
                    <li><?= $this->Translation->get('uzytkownik.orderDetalisOrderComments{0}', [nl2br($zamowienie->uwagi)]) ?></li>
                    <?php if(!empty($zamowienie->bonus_suma)){
                        ?>
                    <li><?= $this->Translation->get('uzytkownik.orderDetalisBonusSuma{0}', [$this->Txt->cena($zamowienie->bonus_suma,$zamowienie->waluta_symbol,true)]) ?></li>
                    <?php
                    }?>
                </ul>
            </div>
            <div class="col-md-4">
                <h5><?= $this->Translation->get('uzytkownik.orderDetalisDeliveryData') ?></h5>
                <div>
                    <?= $this->Txt->printUserInfo($zamowienie->toArray(), '<br/>', 'wysylka_') ?>
                </div>
                <div>
                    <?= (!empty($zamowienie->paczkomat)?$this->Translation->get('uzytkownik.orderDetalisPaczkomat{0}',[$this->Html->tag('div',$zamowienie->paczkomat_info)]):$this->Txt->printAddres($zamowienie->toArray(), '<br/>', 'wysylka_')) ?>
                </div>
            </div>
            <div class="col-md-4">
                <?php if(!empty($zamowienie->faktura)){?>
                <h5><?= $this->Translation->get('uzytkownik.orderDetalisInvoiceData') ?></h5>
                <div>
                    <?= $this->Txt->printUserInfo($zamowienie->toArray(), '<br/>', 'faktura_') ?>
                </div>
                <div>
                    <?= $this->Txt->printAddres($zamowienie->toArray(), '<br/>', 'faktura_') ?>
                </div>
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12"><h5><?=$this->Translation->get('uzytkownik.orderDetalisOrderedProducts')?></h5></div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th><?=$this->Translation->get('uzytkownik.orderDetalisProductListNameTh')?></th>
                            <th class="xs-hidden"><?=$this->Translation->get('uzytkownik.orderDetalisProductListCodeTh')?></th>
                            <th class="text-center xs-hidden"><?=$this->Translation->get('uzytkownik.orderDetalisProductListPriceTh')?></th>
                            <th class="text-center"><?=$this->Translation->get('uzytkownik.orderDetalisProductListQuantityTh')?></th>
                            <th class="text-center"><?=$this->Translation->get('uzytkownik.orderDetalisProductListTotalValueTh')?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        foreach ($zamowienie->zamowienie_towar as $towar){
                            ?>
                        <tr>
                            <td><?=$towar->nazwa?></td>
                            <td class="xs-hidden"><?=$towar->kod?></td>
                            <td class="text-center xs-hidden"><?=$this->Txt->cena($towar->cena_za_sztuke_brutto,(!empty($zamowienie->waluta_symbol)?$zamowienie->waluta_symbol:null))?></td>
                            <td class="text-center"><?=$towar->ilosc.' '.$towar->jednostka_str?></td>
                            <td class="text-center"><?=$this->Txt->cena($towar->cena_razem_brutto,(!empty($zamowienie->waluta_symbol)?$zamowienie->waluta_symbol:null))?></td>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        
                        <?php 
                                 if(!empty($zamowienie->kod_rabatowy) && !empty($zamowienie->kod_rabatowy_wartosc)){
                                    ?>
                                <tr>
                                    <th colspan="<?=($mobile?'2':'4')?>" class="text-right"><?=$this->Translation->get('uzytkownik.orderDetalisProductListKodRabatowy{0}',[$zamowienie->kod_rabatowy])?></th>
                                    <th class="text-center"><?=(($zamowienie->kod_rabatowy_typ=='kwota')?$this->Txt->cena($zamowienie->kod_rabatowy_wartosc,$zamowienie->waluta_symbol):$zamowienie->kod_rabatowy_wartosc.'%')?></th>
                                </tr>
                                <?php
                                }
                                ?>
                                <tr>
                                    <th colspan="<?=($mobile?'2':'4')?>" class="text-right"><?=$this->Translation->get('uzytkownik.orderDetalisProductListWartoscProduktow')?></th>
                                    <?php /* <th class="text-right" style="white-space: nowrap;"><?=$this->Txt->cena($zamowienie->wartosc_produktow_netto,$zamowienie->waluta_symbol)?></th> */ ?>
                                    <th class="text-center" style="white-space: nowrap;"><?= $this->Txt->cena($zamowienie->wartosc_produktow, $zamowienie->waluta_symbol) ?></th>
                                </tr>
                                <tr>
                                    <th colspan="<?=($mobile?'2':'4')?>" class="text-right"><?= $zamowienie->platnosc_wysylka ?></th>
                                    <th class="text-center" style="white-space: nowrap;"><?= $this->Txt->cena($zamowienie->wysylka_koszt, $zamowienie->waluta_symbol) ?></th>
                                </tr>
                                <?php
                                if (!empty($zamowienie->bonus_uzyty)) {
                                    ?>
                                    <tr>
                                        <th colspan="<?=($mobile?'2':'4')?>" class="text-right"><?=$this->Translation->get('uzytkownik.orderDetalisProductListBonusUzyty')?></th>
                                        <th class="text-center">- <?= $this->Txt->cena($zamowienie->bonus_uzyty, $zamowienie->waluta_symbol, true) ?></th>
                                    </tr>
                                    <?php
                                }
                                ?>
                        <tr>
                            <th colspan="<?=($mobile?'2':'4')?>" class="text-right"><?=$this->Translation->get('uzytkownik.orderDetalisProductListTotalValue')?></th>
                            <th class="text-center"><?=$this->Txt->cena($zamowienie->wartosc_razem, (!empty($zamowienie->walutum) ? $zamowienie->walutum->symbol : null))?></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <?php
        ?>
    </div>
</div>