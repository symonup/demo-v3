<div class="row">
    <div class="col-12">
        <div class="card border-none">
            <h4 class="card-header fs-14 fw-semi-bold">
                <?= $this->Translation->get('uzytkownik.platnosciListHeader') ?>
            </h4>
            <div class="card-body padding-left-0 padding-right-0">
                 <?php
                if (!empty($platnosci) && $platnosci->count() > 0) {
                    ?>
                    <div>
                        <?=$this->element('user/platnosci_table',['platnosci'=>$platnosci])?>
                    </div>
                    <?php
                } else {
                    ?>
                    <div>
                        <?= $this->Translation->get('uzytkownik.platnosciListBrakPlatnosci') ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>
<?=$this->element('default/paginator')?>