<div class="card">
            <h4 class="card-header">
<?=$this->Translation->get('uzytkownik.basicInfoHeader')?>
            </h4>
        
  <div class="card-body">
    <?php 
    echo $this->Form->create($uzytkownik);
    ?>
      <div class="row">
          <div class="col-md-12"><h5 class="card-title"><?=$this->Translation->get('uzytkownik.yourData')?></h5></div>
      </div>
      <div class="row">
          <div class="col-md-6"><?=$this->Form->control('imie',['label'=>$this->Translation->get('uzytkownik.name'),'required'=>true])?></div>
          <div class="col-md-6"><?=$this->Form->control('nazwisko',['label'=>$this->Translation->get('uzytkownik.lastname'),'required'=>true])?></div>
      </div>
      <div class="row">
          <div class="col-md-6"><?=$this->Form->control('email',['disabled'=>true,'label'=>$this->Translation->get('uzytkownik.email'),'required'=>true])?></div>
          <div class="col-md-6"><?=$this->Form->control('telefon',['label'=>$this->Translation->get('uzytkownik.phone'),'required'=>true])?></div>
      </div>
      <div class="row">
          <div class="col-md-6"><?=$this->Form->control('firma',['label'=>$this->Translation->get('uzytkownik.company'),'required'=>($uzytkownik->typ=='b2b')])?></div>
          <div class="col-md-6"><?=$this->Form->control('nip',['label'=>$this->Translation->get('uzytkownik.nip'),'required'=>($uzytkownik->typ=='b2b')])?></div>
      </div>
      <?php 
      if($uzytkownik->typ=='b2b'){
          ?>
      <div class="row">
          <div class="col-md-6"><?=$this->Form->control('firma_ulica',['label'=>$this->Translation->get('uzytkownik.ulica'),'required'=>true])?></div>
          <div class="col-md"><?=$this->Form->control('firma_nr_domu',['label'=>$this->Translation->get('uzytkownik.nrBudynku'),'required'=>true])?></div>
          <div class="col-md"><?=$this->Form->control('firma_nr_lokalu',['label'=>$this->Translation->get('uzytkownik.nrLokalu'),'required'=>false])?></div>
      </div>
      <div class="row">
          <div class="col-md-6"><?=$this->Form->control('firma_kod',['label'=>$this->Translation->get('uzytkownik.kodPocztowy'),'required'=>true])?></div>
          <div class="col-md-6"><?=$this->Form->control('firma_miasto',['label'=>$this->Translation->get('uzytkownik.miasto'),'required'=>true])?></div>
      </div>
      <?php
      }
      ?>
      <div class="row">
          <div class="col-12 col-lg-6 text-left">
              <?= $this->Form->control('newsletter', ['type' => 'checkbox', 'label' => $this->Translation->get('uzytkownik.ChceOtrzymywacNewsletter')]) ?>
        </div>
          <div class="col-12 col-lg-6 text-right">
              <a href="<?= \Cake\Routing\Router::url(['controller'=>'Home','action'=>'resetPassword'])?>" class="btn btn-outline-success btn-small"><?=$this->Translation->get('uzytkownik.changePassword')?></a>
              <button type="submit" class="btn btn-success btn-small"><?=$this->Translation->get('labels.save')?></button>
          </div>
      </div>
    <?php
    echo $this->Form->end();
    ?>
  </div>
</div>