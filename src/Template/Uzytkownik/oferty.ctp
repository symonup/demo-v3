<div class="row">
    <div class="col-12">
        <div class="card border-none">
            <h4 class="card-header fs-14 fw-semi-bold">
                <?= $this->Translation->get('uzytkownik.ofertyListHeader') ?>
            </h4>
            <div class="card-body padding-left-0 padding-right-0">
                 <?php
                if (!empty($oferty) && $oferty->count() > 0) {
                    ?>
                    <div>
                        <?=$this->element('user/oferty_table',['oferty'=>$oferty])?>
                    </div>
                    <?php
                } else {
                    ?>
                    <div>
                        <?= $this->Translation->get('uzytkownik.ofertyListBrakOfert') ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>
<?=$this->element('default/paginator')?>