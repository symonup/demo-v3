<div class="card">
            <h4 class="card-header">
<?=$this->Translation->get('uzytkownik.addressListHeader')?>
                    <a href="<?= Cake\Routing\Router::url(['action'=>'addAddress'])?>" class="btn btn-sm btn-success pull-right"><i class="fa fa-plus"></i> <?=$this->Translation->get('uzytkownik.addAddress')?></a>
            </h4>
        
  <div class="card-body">
    <?php 
    if($adresy->count()>0){
        ?>
      <table class="table table-condensed table-hover">
          <thead>
              <tr>
                  <th colspan="2"><?=$this->Translation->get('uzytkownik.addressListThAddress')?></th>
              </tr>
          </thead>
          <tbody>
              <?php
        foreach ($adresy as $adres){
          ?>
              <tr>
                  <td>
                      <?php
                      $userInfo=$this->Txt->printUserInfo($adres->toArray(),', ');
                      if(!empty($userInfo))
                      {
                          echo $userInfo.'<br/>';
                      }
                      ?>
                      <?=$this->Txt->printAddres($adres->toArray(),', ')?>
                  </td>
                  <td class="text-right">
                      <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'editAddress', $adres->id],['escape'=>false,'class'=>'btn btn-outline-dark btn-sm','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Translation->get('labels.edit')]) ?>
                    <?= $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'deleteAddress', $adres->id], ['confirm-btn-yes'=>$this->Translation->get('labels.Yes'),'confirm-btn-no'=>$this->Translation->get('labels.No'),'confirm-message' => $this->Translation->get('uzytkownik.areYouSureYouWantToDeleteAddress'),'escape'=>false,'class'=>'btn btn-outline-danger btn-sm confirm-link','data-toggle'=>'tooltip', 'data-placement'=>'top','title'=>$this->Translation->get('labels.Delete')]) ?>
               </td>
              </tr>
              <?php
        }
        ?>
          </tbody>
      </table>
      
      <?php
    }
    else{
        ?>
      <div class="row justify-content-center">
          <div class="col-lg-8 col-md-10">
              <div class="alert alert-info"><?=$this->Translation->get('uzytkownik.noAddressesInfo',[],true)?></div>
          </div>
      </div>
      <?php
    }
    ?>
  </div>
</div>