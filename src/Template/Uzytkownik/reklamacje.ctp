<div class="row">
    <div class="col-12">
        <div class="card border-none">
            <h4 class="card-header fs-14 fw-semi-bold">
                <?= $this->Translation->get('uzytkownik.reklamacjeListHeader') ?>
                <a class="pull-right fs-13 fw-regular fc-black" href="<?= \Cake\Routing\Router::url(['controller'=>'Reklamacja','action' => 'add']) ?>"><?= $this->Translation->get('labels.noweReklamacja') ?></a>
            </h4>
            <div class="card-body padding-left-0 padding-right-0">
                 <?php
                if (!empty($reklamacje) && $reklamacje->count() > 0) {
                    ?>
                    <div>
                        <?=$this->element('user/reklamacje_table',['reklamacje'=>$reklamacje])?>
                    </div>
                    <?php
                } else {
                    ?>
                    <div>
                        <?= $this->Translation->get('uzytkownik.reklamacjeListBrakReklamacji') ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>
<?=$this->element('default/paginator')?>