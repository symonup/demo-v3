<div class="card">
            <h4 class="card-header">
<?=$this->Translation->get('uzytkownik.invoiceAddressHeader')?>
            </h4>
        
  <div class="card-body">
    <?php 
    echo $this->Form->create($uzytkownikAdres,['id'=>'faktura-form']);
    ?>
      <?=$this->Form->control('typ',['type'=>'hidden','value'=>2])?>
      <div class="row">
          <div class="col-md-12"><h5 class="card-title"><?=$this->Translation->get('uzytkownik.invoiceData')?></h5></div>
      </div>
      <div class="row">
          <div class="col-md-6"><?=$this->Form->control('imie',['label'=>$this->Translation->get('uzytkownik.name'),'minlength'=>2])?></div>
          <div class="col-md-6"><?=$this->Form->control('nazwisko',['label'=>$this->Translation->get('uzytkownik.lastname'),'minlength'=>2])?></div>
      </div>
      <div class="row">
          <div class="col-md-6"><?=$this->Form->control('firma',['label'=>$this->Translation->get('uzytkownik.company'),'other-req'=>'#nip','minlength'=>2])?></div>
          <div class="col-md-6"><?=$this->Form->control('nip',['label'=>$this->Translation->get('uzytkownik.nip'),'other-req'=>'#firma'])?></div>
      </div>
      <div class="row">
          <div class="col-md-12"><h5 class="card-title"><?=$this->Translation->get('uzytkownik.addressCompanyData')?></h5></div>
      </div>
      <div class="row">
          <div class="col-md-6"><?= $this->Form->control('ulica',['label'=>$this->Translation->get('uzytkownik.street')])?></div>
          <div class="col-md-3"><?= $this->Form->control('nr_domu',['label'=>$this->Translation->get('uzytkownik.homeNumber')])?></div>
          <div class="col-md-3"><?= $this->Form->control('nr_lokalu',['label'=>$this->Translation->get('uzytkownik.premisesNumber')])?></div>
      </div>
      <div class="row">
          <div class="col-md-3"><?= $this->Form->control('kod',['label'=>$this->Translation->get('uzytkownik.postCode')])?></div>
          <div class="col-md-6"><?= $this->Form->control('miasto',['label'=>$this->Translation->get('uzytkownik.city')])?></div>
          <div class="col-md-3"><?= $this->Form->control('kraj',['label'=>$this->Translation->get('uzytkownik.country'),'type'=>'select','options'=>$allCountry,'default'=> Cake\Core\Configure::read('adres.default_country')])?></div>
      </div>
      <div class="row">
          <div class="col-md-9"><?=$this->Form->control('as_new_delivery',['type'=>'checkbox','checked'=>false,'label'=>$this->Translation->get('uzytkownik.saveInvoiceAddressAsNewDeliveryAddress')])?></div>
          <div class="col-md-3 text-right"><?= $this->Form->submit($this->Translation->get('labels.save'),['class'=>'btn btn-success'])?></div>
      </div>
    <?php
    if(!empty($redirect)){
        echo $this->Form->control('redirect',['type'=>'hidden','value'=>$redirect]);
    }
    echo $this->Form->end();
    ?>
  </div>
</div>