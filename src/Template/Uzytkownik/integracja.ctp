<div class="row justify-content-start mb-20">
    <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <h4 class="card-header fs-14 fw-semi-bold">
                        <?= $this->Translation->get('uzytkownik.Integracja') ?>
                    </h4>
                    <div class="card-body card-body-bordred">
                    <?php 
                        if (!empty($uzytkownik->api_token)) {
                            ?>
                        <h5><?=t('uzytkownik.integracjaPlikPelny')?></h5>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th><?=t('uzytkownik.integracjaTyp')?></th>
                                    <th><?=t('uzytkownik.integracjaLink')?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>JSON</td>
                                    <td><?= Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'full', '_ext' => 'json', 'token' => $uzytkownik->api_token, 'prefix' => 'api'], true) ?></td>
                                </tr>
                                <tr>
                                    <td>XML</td>
                                    <td><?= Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'full', '_ext' => 'xml', 'token' => $uzytkownik->api_token, 'prefix' => 'api'], true) ?></td>
                                </tr>
                            </tbody>
                        </table>
                        <h5><?=t('uzytkownik.integracjaPlikMinimalny')?></h5>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th><?=t('uzytkownik.integracjaTyp')?></th>
                                    <th><?=t('uzytkownik.integracjaLink')?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>JSON</td>
                                    <td><?= Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'min', '_ext' => 'json', 'token' => $user['api_token'], 'prefix' => 'api'], true) ?></td>
                                </tr>
                                <tr>
                                    <td>XML</td>
                                    <td><?= Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'min', '_ext' => 'xml', 'token' => $user['api_token'], 'prefix' => 'api'], true) ?></td>
                                </tr>
                            </tbody>
                        </table>
                            <?php
                        }
                    else{
                        echo $this->Html->tag('div',t('uzytkownik.integracjaBrakDostepuInfo',['email'=>c('dane.email'),'telefon'=>c('dane.telefon')],true));
                    }
                    ?>
                    </div>
                </div>
            </div>
</div>