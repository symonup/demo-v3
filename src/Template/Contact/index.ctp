<div class="container contact">
    <div class="row">
        <div class="col-12 col-md-9">
            <div class="row">
                <div class="col-12 col-md-4">
                    <div></div>
                </div>
                <div class="col-12 col-md-4"></div>
                <div class="col-12 col-md-4"></div>
            </div>
            <div class="row">
                <div class="col-12">
                    [%-_kontakt-form_-%]
                </div>
            </div>
        </div>
        <div class="col-12 col-md-3"></div>
    </div>
    <div class="row">
        <div class="col-12">Mapa</div>
    </div>
</div>
    <?php 
    echo $this->Html->tag('div', $this->Html->tag('div', $this->Html->tag('h1', $this->Translation->get('kontakt.contactHeader')), ['class' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12'])
            . $this->Html->tag('div', $this->Html->tag('p', $this->Translation->get('kontakt.contactInfo',[],true)), ['class' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12'])
            . $this->Html->tag('div', $this->Html->tag('hr'), ['class' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12'])
            , ['class' => 'row']);
    echo $this->Form->create() . $this->Html->tag('div', $this->Html->tag('div', $this->Html->tag('div', $this->Form->input('imie', ['type' => 'text', 'label' => $this->Translation->get('kontakt.contactInputNameLabel'), 'class' => 'form-control', 'required' => true])
                            . $this->Form->input('email', ['type' => 'email', 'label' => $this->Translation->get('kontakt.contactInputEmailLabel'), 'class' => 'form-control', 'required' => true])
                            , ['class' => 'col-xs-12 col-sm-12 col-md-4 col-lg-4'])
                    . $this->Html->tag('div', $this->Form->input('tresc', ['type' => 'textarea', 'label' => $this->Translation->get('kontakt.contactInputContentLabel'), 'class' => 'form-control', 'required' => true])
                            , ['class' => 'col-xs-12 col-sm-12 col-md-8 col-lg-8'])
                    , ['class' => 'row'])
            . $this->Html->tag('div', $this->Html->tag('div', $this->Html->tag('div', $this->Captcha->input(), array('class' => 'captcha'))//$this->Recaptcha->display(['data-size'=>'compact'])
                            . $this->Form->button($this->Translation->get('kontakt.contactSend'), ['escape' => false, 'class' => 'btn btn-lg btn-success'])
                            , ['class' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right'])
                    , ['class' => 'row'])
            , []) . $this->Form->end() . $this->Html->tag('hr');
    echo $this->Html->tag('div', $this->Translation->get('kontakt.contactData',[],true), ['class' => 'row']) . $this->Html->tag('hr');
    echo $this->Html->tag('div', $this->Html->tag('div', $this->Html->tag('div', \Cake\Core\Configure::read('kontakt.mapa'), []), ['class' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12'])
            , ['class' => 'row']);
    ?>
</div>