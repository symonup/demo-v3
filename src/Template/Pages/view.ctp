<?php
$tresc = $this->element('pages/' . $locale . '/page_' . $strona->id);
foreach ($blocks as $blockId => $blockItem) {
    $tresc = str_replace($blockId, $blockItem, $tresc);
}
if ($strona->type == 'type_popup') {
    echo $this->element('default/popup_strona', ['nazwa' => $strona->nazwa, 'tresc' => $tresc,'referer'=>$referer]);
} else {
    ?>
<div class="container page-bg-in">
    <div class="row">
        <div class="col-12">
            <?=$tresc?>
        </div>
    </div>
</div>
<?php
}
?>