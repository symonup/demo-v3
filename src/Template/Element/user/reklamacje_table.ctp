<table class="table">
    <thead class="card-thead">
    <th><?= $this->Translation->get('uzytkownik.ZapytanieThNrZapytania') ?></th>
    <th><?= $this->Translation->get('uzytkownik.ZapytanieThData') ?></th>
    <th><?= $this->Translation->get('uzytkownik.ZapytanieThStatus') ?></th>
    <th><?= $this->Translation->get('uzytkownik.ZapytanieThOferta') ?></th>
</thead>
<tbody>

    <?php
    foreach ($zapytania as $item) {
        ?>
        <tr>
            <td><?= $item->nr_zapytania ?></td>
            <td><?= (!empty($item->data) ? $item->data->format('Y-m-d H:i') : '') ?></td>
            <td><?= (!empty($order->data_realizacji) ? $order->data_realizacji->format('Y-m-d H:i') : '') ?></td>
            <td><?= $item->status ?></td>
            <td><?= (!empty($item->oferta) ? $item->oferta->nr_oferty : '') ?></td>
        </tr>
        <?php
    }
    ?>
</tbody>
</table>