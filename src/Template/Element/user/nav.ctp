<div class="user-nav">
    <?php
    $btnDefClass = '';
    $btnActiveClass = 'active';
    $action = mb_strtolower($this->request->params['action']);
    ?>
    <a class="<?= (($action == 'index') ? $btnActiveClass : $btnDefClass) ?>" href="<?= Cake\Routing\Router::url(['action' => 'index']) ?>"><?= $this->Translation->get('uzytkownik_nav.home') ?> <i class="fa fa-caret-down"></i></a>
    <div class="user-sub-nav">
        <a class="<?= (($action == 'orders') ? $btnActiveClass : $btnDefClass) ?>" href="<?= Cake\Routing\Router::url(['action' => 'orders']) ?>"><i class="fa fa-file"></i><?= $this->Translation->get('uzytkownik_nav.orders') ?></a>
        <a class="<?= (($action == 'info') ? $btnActiveClass : $btnDefClass) ?>" href="<?= Cake\Routing\Router::url(['action' => 'info']) ?>"><i class="fa fa-user"></i><?= $this->Translation->get('uzytkownik_nav.info') ?></a>
        <a class="<?= (($action == 'addresslist') ? $btnActiveClass : $btnDefClass) ?>" href="<?= Cake\Routing\Router::url(['action' => 'addressList']) ?>"><i class="fa fa-address-card"></i><?= $this->Translation->get('uzytkownik_nav.addressList') ?></a>
        <a class="<?= (($action == 'invoiceaddress') ? $btnActiveClass : $btnDefClass) ?>" href="<?= Cake\Routing\Router::url(['action' => 'invoiceAddress']) ?>"><i class="fa fa-clipboard-list"></i><?= $this->Translation->get('uzytkownik_nav.invoiceAddress') ?></a>
    <?php 
    if($user['typ']=='b2b'){
        ?>
        <a class="<?= (($action == 'faktury') ? $btnActiveClass : $btnDefClass) ?>" href="<?= Cake\Routing\Router::url(['action' => 'faktury']) ?>"><i class="fas fa-file-invoice"></i><?= $this->Translation->get('uzytkownik_nav.faktury') ?></a>
        <a class="<?= (($action == 'integracja') ? $btnActiveClass : $btnDefClass) ?>" href="<?= Cake\Routing\Router::url(['action' => 'integracja']) ?>"><i class="fas fa-link"></i><?= $this->Translation->get('uzytkownik_nav.integracja') ?></a>
        <?php
    }
    ?>
    </div>
    <a href="<?= Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'index']) ?>"><?= $this->Translation->get('uzytkownik_nav.backToTheShopping') ?> <i class="fa fa-caret-right"></i></a>
    <a href="<?= Cake\Routing\Router::url(['controller' => 'Home', 'action' => 'logout']) ?>"><?= $this->Translation->get('uzytkownik_nav.Wyloguj') ?> <i class="fa fa-caret-right"></i></a>
</div>