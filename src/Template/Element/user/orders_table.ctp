<table class="table">
    <thead class="card-thead">
    <th><?= $this->Translation->get('uzytkownik.ZamowienieThNrZamowienia') ?></th>
    <th class="xs-hidden"><?= $this->Translation->get('uzytkownik.ZamowienieThDataWystawienia') ?></th>
    <th><?= $this->Translation->get('uzytkownik.ZamowienieThStatus') ?></th>
    <th class="xs-hidden"><?= $this->Translation->get('uzytkownik.ZamowienieThPlatnosc') ?></th>
    <th class="xs-hidden"><?= $this->Translation->get('uzytkownik.ZamowienieThWartosc') ?></th>
    <th></th>
</thead>
<tbody>

    <?php
    foreach ($orders as $order) {
        ?>
        <tr>
            <td><?= (!empty($order->numer)?$order->numer:$order->id) ?></td>
            <td class="xs-hidden"><?= (!empty($order->data) ? $order->data->format('Y-m-d H:i') : '') ?></td>
            <td><?= $this->Translation->get('statusy_zamowien.'.$order->status) ?></td>
            <td class="xs-hidden"><?= $order->platnosc_wysylka ?></td>
            <td class="xs-hidden"><?= $this->Txt->cena($order->wartosc_razem,$order->waluta_symbol) ?></td>
            <td class="text-right"><a class="btn btn-sm btn-outline-dark" href="<?= \Cake\Routing\Router::url(['action'=>'orderDetalis',$order->token])?>"><i class="fa fa-eye"></i> <?=$this->Translation->get('uzytkownik.szczegolyZamowieniaLink')?></a></td>
        </tr>
        <?php
    }
    ?>
</tbody>
</table>