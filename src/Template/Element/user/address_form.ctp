<?= $this->Form->control('typ', ['type' => 'hidden', 'value' => $typ]) ?>
<div class="row">
    <div class="col-md-12"><h5 class="card-title"><?= $this->Translation->get('uzytkownik.addressData') ?></h5></div>
</div>
<div class="row">
    <div class="col-md-6"><?= $this->Form->control('imie', ['label' => $this->Translation->get('uzytkownik.name'),'minlength'=>2]) ?></div>
    <div class="col-md-6"><?= $this->Form->control('nazwisko', ['label' => $this->Translation->get('uzytkownik.lastName'),'minlength'=>2]) ?></div>
</div>
<div class="row">
    <div class="col-md-6"><?= $this->Form->control('ulica', ['label' => $this->Translation->get('uzytkownik.street')]) ?></div>
    <div class="col-md-3"><?= $this->Form->control('nr_domu', ['label' => $this->Translation->get('uzytkownik.homeNumber')]) ?></div>
    <div class="col-md-3"><?= $this->Form->control('nr_lokalu', ['label' => $this->Translation->get('uzytkownik.premisesNumber')]) ?></div>
</div>
<div class="row">
    <div class="col-md-3"><?= $this->Form->control('kod', ['label' => $this->Translation->get('uzytkownik.postCode')]) ?></div>
    <div class="col-md-6"><?= $this->Form->control('miasto', ['label' => $this->Translation->get('uzytkownik.city')]) ?></div>
    <div class="col-md-3"><?= $this->Form->control('kraj', ['label' => $this->Translation->get('uzytkownik.country'), 'type' => 'select', 'options' => $allCountry, 'default' => Cake\Core\Configure::read('adres.default_country')]) ?></div>
</div>
<div class="row">
    <div class="col-md-9">
        <?= $this->Form->control('domyslny_wysylka', ['type' => 'checkbox', 'label' => $this->Translation->get('uzytkownik.ustawAdresJakoDomyslny')]) ?>
        <?= $this->Form->control('as_invoice', ['type' => 'checkbox', 'checked' => false, 'label' => $this->Translation->get('uzytkownik.saveDeliveryAddressAsInvoice')]) ?>
    </div>
    <div class="col-md-3 text-right"><?= $this->Form->submit($this->Translation->get('labels.save'), ['class' => 'btn btn-success']) ?></div>
</div>