<div class="html-content"><div class="text-center">
<img src="/files/my_files/kontakt_img.jpg">
</div>
<div class="row justify-content-between">
  <div class="col-12 col-md-auto">
      <table class="kontakt-info">
      <tbody><tr>
        <td style="vertical-align: top;"><i class="flat flaticon-area"></i></td>
        <td>Nazwa firmy<br>ul. ulica 151<br>21-700 Miasto</td>
      </tr>
    </tbody></table>
  </div>
  <div class="col-12 col-md-auto">
    <table class="kontakt-info">
      <tbody><tr>
              <td style="vertical-align: top;"><i class="flat flaticon-pass"></i></td>
        <td>NIP: 600 345 23 23<br>Regon: 400 182 23</td>
      </tr>
    </tbody></table>
  </div>
  <div class="col-12 col-md-auto">
    <table class="kontakt-info">
      <tbody><tr>
        <td style="vertical-align: top;"><i class="flat flaticon-clock"></i></td>
        <td>W dni robocze od: 8:00 do 16:00<br>Po godzinie 16:00 i w dni wolne od pracy również odpisujemy na maile. Odpowiadamy do 2 godzin.</td>
      </tr>
    </tbody></table>
  </div>
</div></div>[%-_blok_4_-%]