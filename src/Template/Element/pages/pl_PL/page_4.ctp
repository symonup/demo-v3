<div class="html-content"><p align="CENTER"><span><b>POLITYKA
PRYWATNOŚCI SKLEPU INTERNETOWEGO</b></span></p><p align="CENTER"><span><b>www.example.pl</b></span></p><p>
<br>
</p><p>
<br>
</p><p align="CENTER"><span><b>§
1</b></span></p><p align="CENTER"><span><b>POSTANOWIENIA
OGÓLNE</b></span></p><ol>
	<li><p align="JUSTIFY">
	<span>Administratorem
	danych osobowych zbieranych za pośrednictwem Sklepu internetowego 
	www.example.pl jest</span><span> NAZWA FIRMY wpisaną
	do Centralnej Ewidencji i Informacji o Działalności Gospodarczej
	Rzeczypospolitej Polskiej prowadzonej przez ministra właściwego do
	spraw gospodarki, miejsce wykonywania działalności oraz adres do
	doręczeń: ADRES, NIP:
	XXXXXXXXXXX, REGON: XXXXXXXXX, adres poczty elektronicznej (e-mail):
	biuro@example.pl, numer telefonu: +48 XXX XXX XXX, </span><span>zwana
	dalej „Administratorem" i będąca jednocześnie
	„Usługodawcą”.</span></p>
	</li><li><p align="JUSTIFY">
	<span>Dane
	osobowe zbierane przez Administratora za pośrednictwem strony
	internetowej są przetwarzane zgodnie z&nbsp;Rozporządzeniem
	Parlamentu Europejskiego i&nbsp;Rady (UE) 2016/679 z&nbsp;dnia 27
	kwietnia 2016 r. w&nbsp;sprawie ochrony osób fizycznych w&nbsp;związku
	z&nbsp;przetwarzaniem danych osobowych i&nbsp;w sprawie swobodnego
	przepływu takich danych oraz uchylenia dyrektywy 95/46/WE (ogólne
	rozporządzenie o&nbsp;ochronie danych), zwane dalej </span><span><b>RODO.</b></span></p>
	</li><li><p align="JUSTIFY">
	<span>Wszelkie
	wyrazy lub wyrażenia pisane w treści niniejszej Polityki
	Prywatności z dużej litery należy rozumieć zgodnie z ich
	definicją zawartą w Regulaminie Sklepu internetowego
	www.example.pl.</span></p>
</li></ol><p>
<br>
</p><p align="CENTER"><span><b>§
2</b></span></p><p align="CENTER"><span><b>RODZAJ
PRZETWARZANYCH DANYCH OSOBOWYCH, CEL I ZAKRES ZBIERANIA DANYCH</b></span></p><ol>
	<li><p align="JUSTIFY">
	<span><b>CEL
	PRZETWARZANIA I PODSTAWA PRAWNA.</b></span><span>
	Administrator przetwarza dane osobowe Usługobiorców Sklepu
	www.example.pl w przypadku:</span></p>
</li></ol><ol type="a">
	<li><p align="JUSTIFY">
	<span>rejestracji
	Konta w&nbsp;Sklepie, w&nbsp;celu utworzenia indywidualnego konta
	i&nbsp;zarządzania tym Kontem, na podstawie art. 6 ust. 1 lit. b
	RODO (realizacja umowy o świadczenie usługi drogą elektroniczną
	zgodnie z&nbsp;Regulaminem Sklepu),</span></p>
	</li><li><p align="JUSTIFY">
	<span>składania
	zamówienia w&nbsp;Sklepie, w&nbsp;celu wykonania umowy sprzedaży,
	na podstawie art. 6 ust. 1 lit. b RODO (realizacja umowy sprzedaży),</span></p>
	</li><li><p align="JUSTIFY">
	<span>zapisania
	się do Newslettera w celu przesyłania informacji handlowych drogą
	elektroniczną. Dane osobowe są przetwarzane po wyrażeniu odrębnej
	zgody, na podstawie art. 6 ust. 1 lit. a) RODO.</span></p>
</li></ol><ol start="2">
	<li><p align="JUSTIFY">
	<span><b>RODZAJ
	PRZETWARZANYCH DANYCH OSOBOWYCH.</b></span><span>
	W przypadku:</span></p>
</li></ol><ol type="a">
	<li><p align="JUSTIFY">
	<span>Konta
	Usługobiorca podaje:</span></p>
</li></ol><ul>
	<li><p align="JUSTIFY">
	<span>Imię
	i nazwisko,</span></p>
	</li><li><p align="JUSTIFY">
	<span>Login,</span></p>
	</li><li><p align="JUSTIFY">
	<span>Adres,</span></p>
	</li><li><p align="JUSTIFY">
	<span>Adres
	e-mail.</span></p>
</li></ul><ol type="a" start="2">
	<li><p align="JUSTIFY">
	<span>Zamówienia
	Usługobiorca podaje:</span></p>
</li></ol><ul>
	<li><p align="JUSTIFY">
	<span>Imię
	i nazwisko,</span></p>
	</li><li><p align="JUSTIFY">
	<span>NIP,</span></p>
	</li><li><p align="JUSTIFY">
	<span>Adres,</span></p>
	</li><li><p align="JUSTIFY">
	<span>Adres
	e-mail,</span></p>
	</li><li><p align="JUSTIFY">
	<span>Numer
	telefonu,</span></p>
</li></ul><ol type="a" start="3">
	<li><p align="JUSTIFY">
	<span>Newslettera
	Usługobiorca podaje</span></p>
</li></ol><ul>
	<li><p align="JUSTIFY">
	<span>Imię
	i nazwisko,</span></p>
	</li><li><p align="JUSTIFY">
	<span>adres
	e-mail.</span></p>
</li></ul><ol start="3">
	<li><p align="JUSTIFY">
	<span><b>OKRES
	ARCHIWIZACJI DANYCH OSOBOWYCH.</b></span><span> Dane
	osobowe Usługobiorców przechowywane są przez Administratora: </span>
	</p>
</li></ol><ol type="a">
	<li><p align="JUSTIFY">
	<span>w
	przypadku, gdy podstawą przetwarzania danych jest wykonanie umowy,
	tak długo, jak jest to niezbędne do wykonania umowy, a&nbsp;po tym
	czasie przez okres odpowiadający okresowi przedawnienia roszczeń.
	Jeżeli przepis szczególny nie stanowi inaczej, termin
	przedawnienia wynosi lat sześć, a&nbsp;dla roszczeń o&nbsp;świadczenia
	okresowe oraz roszczeń związanych z&nbsp;prowadzeniem działalności
	gospodarczej - trzy lata.</span></p>
	</li><li><p align="JUSTIFY">
	<span>w
	przypadku, gdy podstawą przetwarzania danych jest zgoda, tak długo,
	aż zgoda nie zostanie odwołana, a&nbsp;po odwołaniu zgody przez
	okres czasu odpowiadający okresowi przedawnienia roszczeń jakie
	może podnosić Administrator i&nbsp;jakie mogą być podnoszone
	wobec niego. Jeżeli przepis szczególny nie stanowi inaczej, termin
	przedawnienia wynosi lat sześć, a&nbsp;dla roszczeń o&nbsp;świadczenia
	okresowe oraz roszczeń związanych z&nbsp;prowadzeniem działalności
	gospodarczej - trzy lata.</span></p>
</li></ol><ol start="4">
	<li><p align="JUSTIFY">
	<span>Podczas
	korzystania ze Sklepu mogą być pobierane dodatkowe informacje,
	w&nbsp;szczególności: adres IP przypisany do komputera
	Usługobiorcy lub zewnętrzny adres IP dostawcy Internetu, nazwa
	domeny, rodzaj przeglądarki, czas dostępu, typ systemu
	operacyjnego.</span></p>
	</li><li><p align="JUSTIFY">
	<span>Po
	wyrażeniu odrębnej zgody, na podstawie art. 6 ust. 1 lit. a) RODO
	dane mogą być przetwarzane również w celu przesyłania
	informacji handlowych drogą elektroniczną lub wykonywania
	telefonicznych połączeń w celu marketingu bezpośredniego –
	odpowiednio w związku z art. 10 ust. 2 Ustawy z dnia 18 lipca 2002
	roku o świadczeniu usług drogą elektroniczną lub art. 172 ust. 1
	Ustawy z dnia 16 lipca 2004 roku – Prawo Telekomunikacyjne, w tym
	kierowanych w wyniku profilowania, o ile Usługobiorca wyraził
	stosowną zgodę.</span></p>
	</li><li><p align="JUSTIFY">
	<span>Od
	Usługobiorców mogą być także gromadzone dane nawigacyjne, w&nbsp;tym
	informacje o&nbsp;linkach i&nbsp;odnośnikach, w&nbsp;które
	zdecydują się kliknąć lub innych czynnościach, podejmowanych w
	Sklepie. Podstawą prawną tego rodzaju czynności jest prawnie
	uzasadniony interes Administratora (art. 6 ust. 1 lit. f RODO),
	polegający na ułatwieniu korzystania z&nbsp;usług świadczonych
	drogą elektroniczną oraz na poprawie funkcjonalności tych usług.</span></p>
	</li><li><p align="JUSTIFY">
	<span>Podanie
	danych osobowych przez Usługobiorcę jest dobrowolne.</span></p>
	</li><li><p align="JUSTIFY">
	<span>Dane
	osobowe będą przetwarzane także w sposób zautomatyzowany w
	formie profilowania, o ile Usługobiorca wyrazi na to zgodę na
	podstawie art. 6 ust. 1 lit. a) RODO. Konsekwencją profilowania
	będzie przypisanie danej osobie profilu w celu podejmowania
	dotyczących jej decyzji bądź analizy lub przewidywania jej
	preferencji, zachowań i postaw.</span></p>
	</li><li><p align="JUSTIFY">
	<span>Administrator
	dokłada szczególnej staranności w celu ochrony interesów osób,
	których dane dotyczą, a w szczególności zapewnia, że zbierane
	przez niego dane są:</span></p>
</li></ol><ol>
	<ol type="a">
		<li><p align="JUSTIFY">
		<span>przetwarzane
		zgodnie z prawem, </span>
		</p>
		</li><li><p align="JUSTIFY">
		<span>zbierane
		dla oznaczonych, zgodnych z prawem celów i niepoddawane dalszemu
		przetwarzaniu niezgodnemu z tymi celami,</span></p>
		</li><li><p align="JUSTIFY">
		<span>merytorycznie
		poprawne i adekwatne w stosunku do celów, w jakich są
		przetwarzane oraz przechowywane w postaci umożliwiającej
		identyfikację osób, których dotyczą, nie dłużej niż jest to
		niezbędne do osiągnięcia celu przetwarzania.</span></p>
	</li></ol>
</ol><p>
<br>
</p><p align="CENTER"><span><b>§
3</b></span></p><p align="CENTER"><span><b>UDOSTĘPNIENIE
DANYCH OSOBOWYCH</b></span></p><ol>
	<li><p align="JUSTIFY">
	<span>Dane
	osobowe Usługobiorców przekazywane są dostawcom usług, z&nbsp;których
	korzysta Administrator przy prowadzeniu Sklepu. Dostawcy usług,
	którym przekazywane są dane osobowe, w&nbsp;zależności od
	uzgodnień umownych i&nbsp;okoliczności, albo podlegają poleceniom
	Administratora co do celów i&nbsp;sposobów przetwarzania tych
	danych (podmioty przetwarzające) albo samodzielnie określają cele
	i&nbsp;sposoby ich przetwarzania (administratorzy).</span></p>
	</li><li><p align="JUSTIFY">
	Dane
	osobowe Usługobiorców są przechowywane wyłącznie na terenie
	Europejskiego Obszaru Gospodarczego (EOG).</p>
</li></ol><p align="JUSTIFY"><br>
</p><p align="CENTER"><span><b>§
4</b></span></p><p align="CENTER"><span><b>PRAWO
KONTROLI, DOSTĘPU DO TREŚCI WŁASNYCH DANYCH ORAZ ICH POPRAWIANIA</b></span></p><ol>
	<li><p align="JUSTIFY">
	<span>Osoba,
	której dane dotyczą, ma prawo dostępu do treści swoich danych
	osobowych oraz prawo ich sprostowania, usunięcia, ograniczenia
	przetwarzania, prawo do przenoszenia danych, prawo wniesienia
	sprzeciwu, prawo do cofnięcia zgody w dowolnym momencie bez wpływu
	na zgodność z prawem przetwarzania, którego dokonano na podstawie
	zgody przed jej cofnięciem.</span></p>
	</li><li><p align="JUSTIFY">
	<span>Podstawy
	prawne żądania Usługobiorcy: </span>
	</p>
</li></ol><ol type="a">
	<li><p align="JUSTIFY">
	<span><b>Dostęp
	do danych</b></span><span>
	– art. 15 RODO.</span></p>
	</li><li><p align="JUSTIFY">
	<span><b>Sprostowanie
	danych</b></span><span>
	– art. 16 RODO.</span></p>
	</li><li><p align="JUSTIFY">
	<span><b>Usunięcie
	danych (tzw. prawo do bycia zapomnianym) </b></span><span>–
	art. 17 RODO.</span></p>
	</li><li><p align="JUSTIFY">
	<span><b>Ograniczenie
	przetwarzania</b></span><span>
	– art. 18 RODO.</span></p>
	</li><li><p align="JUSTIFY">
	<span><b>Przeniesienie
	danych </b></span><span>–
	art. 20 RODO.</span></p>
	</li><li><p align="JUSTIFY">
	<span><b>Sprzeciw
	</b></span><span>–
	art. 21 RODO</span></p>
	</li><li><p align="JUSTIFY">
	<span><b>Cofnięcie
	zgody </b></span><span>–
	art. 7 ust. 3 RODO.</span></p>
</li></ol><ol start="3">
	<li><p align="JUSTIFY">
	<span>W
	celu realizacji uprawnień, o których mowa w pkt 2 można wysłać
	stosowną wiadomość e-mail na adres:</span>
	<span>.</span></p>
	</li><li><p align="JUSTIFY">
	<span>W
	sytuacji wystąpienia przez Usługobiorcę z&nbsp;uprawnieniem
	wynikającym z&nbsp;powyższych praw, Administrator spełnia żądanie
	albo odmawia jego spełnienia niezwłocznie, nie później jednak
	niż w&nbsp;ciągu miesiąca po jego otrzymaniu. Jeżeli jednak -
	z&nbsp;uwagi na skomplikowany charakter żądania lub liczbę żądań
	– Administrator nie będzie mógł spełnić żądania w&nbsp;ciągu
	miesiąca, spełni je w&nbsp;ciągu kolejnych dwóch miesięcy
	informując Usługobiorcę uprzednio w&nbsp;terminie miesiąca od
	otrzymania żądania - o&nbsp;zamierzonym przedłużeniu terminu
	oraz jego przyczynach.</span></p>
	</li><li><p align="JUSTIFY">
	<span>W
	przypadku stwierdzenia, że przetwarzanie danych osobowych narusza
	przepisy RODO, osoba, której dane dotyczą, ma prawo wnieść
	skargę do Prezesa Urzędu Ochrony Danych Osobowych.</span></p>
</li></ol><p>
<br>
</p><p align="CENTER"><span><b>§
5</b></span></p><p align="CENTER"><span><b>PLIKI
"COOKIES"</b></span></p><ol>
	<li><p align="JUSTIFY">
	<span>Strona
	Administratora&nbsp;</span><span>używa
	plików</span><span>.</span></p>
	</li><li><p align="JUSTIFY">
	<span>Instalacja
	plików „</span><span>”
	jest konieczna do prawidłowego świadczenia usług na stronie
	internetowej Sklepu. W plikach „</span><span znajdują="" się="" informacje="" niezbędne="" do="" prawidłowego="" funkcjonowania="" strony,="" a="" także="" <="" span=""><span>dają one także
	możliwość opracowywania ogólnych statystyk odwiedzin strony
	internetowej.</span></span></p>
	</li><li><p align="JUSTIFY">
	<span>W
	ramach strony stosowane są dwa rodzaje plików „</span><span>”:
	„sesyjne” oraz „stałe”.</span></p>
</li></ol><ol type="a">
	<li><p align="JUSTIFY">
	<span>”
	„sesyjne” są plikami tymczasowymi, które przechowywane są w
	urządzeniu końcowym Usługobiorcy do czasu wylogowania
	(opuszczenia strony). </span>
	</p>
	</li><li><p align="JUSTIFY">
	<span>Stałe”
	pliki „</span><span>”
	przechowywane są w urządzeniu końcowym Usługobiorcy przez czas
	określony w parametrach plików „</span><span>”
	lub do czasu ich usunięcia przez Usługobiorcę.</span></p>
</li></ol><ol start="4">
	<li><p align="JUSTIFY">
	<span>Administrator
	wykorzystuje własne pliki cookies w celu </span><span>lepszego
	poznania sposobu interakcji Usługobiorców w zakresie zawartości
	strony. Pliki gromadzą informacje o sposobie korzystania ze strony
	internetowej przez Usługobiorcę, typie strony, z jakiej
	Usługobiorca został przekierowany oraz liczbie odwiedzin i czasie
	wizyty Usługobiorcy na stronie internetowej. Informacje te nie
	rejestrują konkretnych danych osobowych Usługobiorcy, lecz służą
	do opracowania statystyk korzystania ze strony.</span></p>
	</li><li><p align="JUSTIFY">
	<span>Administrator
	wykorzystuje zewnętrzne pliki cookies</span><span><i>
	</i></span><span>w
	celu </span><span>zbierania ogólnych
	i&nbsp;anonimowych danych statycznych za pośrednictwem narzędzi
	analitycznych Google Analytics (administrator cookies zewnętrznego:
	Google Inc. z&nbsp;siedzibą w&nbsp;USA).</span></p>
	</li><li><p align="JUSTIFY">
	<span>Usługobiorca
	ma prawo zadecydowania w zakresie dostępu plików „</span><span>”
	do swojego komputera poprzez ich uprzedni wybór w oknie swojej
	przeglądarki. </span><span>Szczegółowe
	informacje o możliwości i sposobach obsługi plików „</span><span>”
	dostępne są w ustawieniach oprogramowania (przeglądarki
	internetowej).</span></p>
</li></ol><p>
<br>
</p><p align="CENTER"><span><b>§
6</b></span></p><p align="CENTER"><span><b>POSTANOWIENIA
KOŃCOWE</b></span></p><p>
</p><ol>
	<li><p align="JUSTIFY">
	<span>Administrator
	stosuje środki techniczne i organizacyjne zapewniające ochronę
	przetwarzanych danych osobowych odpowiednią do zagrożeń oraz
	kategorii danych objętych ochroną, a w szczególności zabezpiecza
	dane przed ich udostępnieniem osobom nieupoważnionym, zabraniem
	przez osobę nieuprawnioną, przetwarzaniem z naruszeniem
	obowiązujących przepisów oraz zmianą, utratą, uszkodzeniem lub
	zniszczeniem.</span></p>
	</li><li><p align="JUSTIFY">
	<span>Administrator
	udostępnia odpowiednie środki techniczne zapobiegające
	pozyskiwaniu i modyfikowaniu przez osoby nieuprawnione, danych
	osobowych przesyłanych drogą elektroniczną.</span></p>
	</li><li><p align="JUSTIFY">
	<span>W
	sprawach nieuregulowanych niniejszą Polityką prywatności stosuje
	się odpowiednio </span><span>przepisy
	RODO</span><span>
	oraz inne właściwe przepisy prawa polskiego.</span></p>
</li></ol></div>