<div class="html-content"><div class="container">
  <div class="row">
    <div class="col-12 fs-24 fw-bold">Nasze sklepy stacjonarne<br></div></div>
<div class="row justify-content-start align-items-stretch">
    <div class="col-12 col-md-1 text-center"><div class="bg-color-1 fs-24 fw-bold pd-t-10 md-mw-70">1</div></div>
    <div class="col-12 col-md-2 md-padding-left-0 md-padding-right-0">
        <p>
            ul. Krakowska 31<br>
            34-130 Kalwaria Zebrzydowska
        </p>
        <p class="mb-0">
            <b>Godziny otwarcia:</b>
        </p>
        <ul class="list-style-no mb-0">
            <li>Od poniedziałku do piątku:</li>
            <li>08<sup>00</sup> - 18<sup>00</sup></li>
        </ul>
    </div>
    <div class="col-12 col-md-3 md-w-plus-70">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2571.7130857742018!2d19.680033615290196!3d49.86663397939911!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4716630aa89acd05%3A0xe7c16d16ce7b6f3f!2sKrakowska+31%2C+34-130+Kalwaria+Zebrzydowska!5e0!3m2!1spl!2spl!4v1544786436385" width="100%" frameborder="0" style="border:0" allowfullscreen=""></iframe>
    </div>
    <div class="col-12 col-md-1 text-center"><div class="md-mw-70 bg-color-1 fs-24 fw-bold pd-t-10">2</div></div>
    <div class="col-12 col-md-2 md-padding-left-0 md-padding-right-0">
        <p>
            ul. Ks. J. Popiełuszki 2D<br>
            32-050 Skawina
        </p>
        <p class="mb-0">
            <b>Godziny otwarcia:</b>
        </p>
        <ul class="list-style-no mb-0">
            <li>Od poniedziałku do piątku:</li>
            <li>08<sup>00</sup> - 18<sup>00</sup></li>
            <li>Sobota:</li>
            <li>09<sup>00</sup> - 15<sup>00</sup></li>
        </ul>
    </div>
    <div class="col-12 col-md-3 md-w-plus-70">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2566.0338854505253!2d19.827569715294903!3d49.97322307941233!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47165d8ba336e8b1%3A0x343605fdeef92006!2sksi%C4%99dza+Jerzego+Popie%C5%82uszki+2d%2C+Skawina%2C+Polska%2C+Popie%C5%82uszki+2d%2C+32-050+Skawina!5e0!3m2!1spl!2spl!4v1544787686806" width="100%" frameborder="0" style="border:0" allowfullscreen=""></iframe>&nbsp; &nbsp; &nbsp;</div></div>
</div></div>