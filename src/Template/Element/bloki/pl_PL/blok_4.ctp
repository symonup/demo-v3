<div class="row">
    <div class="col-12 col-md">
        <div class="color-bar color-1">
            <div class="row justify-content-around">
                <div class="col-auto"><i class="flat flaticon-phone"></i></div>
                <div class="col-auto"><a href="tel:+48 517 835 872">+48 517 835 872</a></div>
                <div class="col-auto"><a href="tel:+48 506 337 824">+48 506 337 824</a></div>
            </div>
        </div>
        <div>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2549.151435425481!2d18.66498301514779!3d50.28910100685967!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x471130ff4806a503%3A0xeeb394670db1416a!2sSamatix%20-%20Profesjonalne%20strony%20internetowe!5e0!3m2!1spl!2spl!4v1587367719780!5m2!1spl!2spl" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
    </div>
    <div class="col-12 col-md-4 text-center">
        <div class="color-bar color-2"><i class="flat flaticon-send-1"></i> <a href="mailto:biuro@example.pl">biuro@example.pl</a></div>
        <div>
            <div><img src="/files/my_files/punkt_odbioru.jpg"/></div>
            <div style="background: #116baa; padding: 15px;">
                <div style="color: #89d3ef; font-size: 24px; font-weight: 500; text-align: left;">PUNKT ODBIORU OSOBISTEGO</div>
                <table>
                    <tbody><tr>
                            <td style="vertical-align: bottom;color: #89d3ef;"><i class="flat flaticon-clock-2"></i></td>
                            <td style="color: #fff;text-align: left;padding-left: 10px;line-height: 1.5;padding-bottom: 5px;">
                                Nazwa firmy<br>
                                ul. adres 151<br>
                                21-700 Miasto<br>
                                <span style="color: #89d3ef;">W dni robocze od 8:00 do 16:00</span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
        </div>
    </div>
</div>
<div class="row justify-content-between mt-50">
    <div class="col-12 col-md-4">
        <div class="blue-title">Zwroty, Reklamacje, Serwis</div>
        <p style="font-size: 18px;color: #656565;line-height: normal;">
            W dni robocze od 8:00 do 16:00<br>
            Po godzinie 16:00 i w dni wolne od pracy również odposujemy na maile. Odpowiadamy do 2 godzin.<br>
            Tel.: +48 517 835 872<br>
            E-mail: biuro@example.pl
        </p>
        <div class="blue-title">Adres do zwrotów</div>
        <p style="font-size: 18px;color: #656565;line-height: normal;">
            Nazwa firmy<br>
            ul. adres 151<br>
            21-700 Miasto<br>
            Tel.: +48 111 222 333
        </p>
    </div>
    <div class="col-12 col-md">
        <div class="blue-title">FORMULARZ KONTAKTOWY</div>
        [%-_kontakt-form_-%]
    </div>
</div>
<div class="row">
    <div class="col-12"><div class="blue-title">Nasze konta bankowe</div></div>
    <div class="col-12 col-md"><img src="/img/bank_1.png"/> <span style="font-size: 16px;color: #144a71;">10 1234 1234 1234 1234 1234 1234</span></div>
    <div class="col-12 col-md"><img src="/img/bank_2.png"/> <span style="font-size: 16px;color: #144a71;">10 1234 1234 1234 1234 1234 1234</span></div>
    <div class="col-12 col-md"><img src="/img/bank_3.png"/> <span style="font-size: 16px;color: #144a71;">10 1234 1234 1234 1234 1234 1234</span></div>
    <div class="col-12">
        <p style="color: #838383;font-size: 16px;margin-top: 5px;">W tytule przelewu podaj: NUMER ZAMÓWIENIA, IMIĘ I NAZWISKO.</p>
    </div>
</div>