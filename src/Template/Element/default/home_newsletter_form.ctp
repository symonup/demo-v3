<?php
if ($mobile) {
    ?>
    <form method="post" action="<?= \Cake\Routing\Router::url(['controller' => 'Home', 'action' => 'newsletter']) ?>" id="home-newsletter-form">
        <div class="row">
            <div class="col-12">
                <div class="newsletterInfo"><?= $this->Translation->get('newsletter.newsletterHomeTitle', null, true) ?></div>
            </div>
        </div>
        <div class="row">
            <div class="col-auto">
                <span class="newsletter-home-ico">
                    <i class="fa fa-envelope"></i>
                </span>
            </div>
            <div class="col">
                <div class="newsletterInfo"><?= $this->Translation->get('newsletter.newsletterHomeInfo', [], true) ?></div>
                <input required="required" name="email" type="email" placeholder="<?= $this->Translation->get('newsletter.emailPlaceholder') ?>" class="newsletter-input">

                <button type="submit" class="newsletter-button btn-color-1"><?= $this->Translation->get('newsletter.wyslij') ?></button>

            </div>
        </div>
    </form>
<?php } else {
    ?>
    <form method="post" action="<?= \Cake\Routing\Router::url(['controller' => 'Home', 'action' => 'newsletter']) ?>" id="home-newsletter-form">
        <div class="row">
            <div class="col-12">
                <div class="newsletterInfoTitle"><?= $this->Translation->get('newsletter.newsletterHomeTitle', null, true) ?></div>
            </div>
        </div>
        <div class="row">
            <div class="col-auto">
                <span class="newsletter-home-ico">
                    <i class="fa fa-envelope"></i>
                </span>
            </div>
            <div class="col">
                <div class="newsletterInfo"><?= $this->Translation->get('newsletter.newsletterHomeInfo', [], true) ?></div>
                <input required="required" name="email" type="email" placeholder="<?= $this->Translation->get('newsletter.emailPlaceholder') ?>" class="newsletter-input">

                <button type="submit" class="newsletter-button"><?= $this->Translation->get('newsletter.wyslij') ?></button>

            </div>
        </div>
    </form>
    <?php }
?>