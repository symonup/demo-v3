<?php
if (!empty($allCats)) {
    ?>
    <div class="container-fluid fullCategory">
        <div class="row justify-content-between full-cat-container">
            <?php
            $max = 9;
            foreach ($allCats as $mainCat) {
                ?>
                <div class="col-12 col-md-4 col-lg-3 fullCategoryItem">
                    <div>
                        <a class="cat-img" href="<?= r(['controller' => 'Towar', 'action' => 'index', $mainCat['id'], $this->Txt->friendlyUrl($mainCat['nazwa'])]) ?>">
                            <img src="<?= (!empty($mainCat['zdjecie']) ? $displayPath['kategoria'] . $mainCat['zdjecie'] : '/img/noPhoto.png') ?>"/>
                        </a>
                        <a class="cat-name" href="<?= r(['controller' => 'Towar', 'action' => 'index', $mainCat['id'], $this->Txt->friendlyUrl($mainCat['nazwa'])]) ?>">
                            <?= $mainCat['nazwa'] ?>
                        </a>
                        <?php
                        if (!empty($mainCat['sub'])) {
                            ?>
                            <ul class="fullCategoryItemSub">
                                <?php
                                $i=1;
                                foreach ($mainCat['sub'] as $subCat) {
                                    if($i > $max){
                                    ?>
                                <li><a class="sub-cat-all" href="<?= r(['controller' => 'Towar', 'action' => 'index', $mainCat['id'], $this->Txt->friendlyUrl($mainCat['nazwa'])]) ?>"><i class="fa fa-plus"></i> <?= t('labels.zobaczWszystkie') ?></a></li> 
                                    <?php
                                        break;
                                    }else{
                                    ?>
                                    <li><a class="sub-cat-name" href="<?= r(['controller' => 'Towar', 'action' => 'index', $subCat['id'], $this->Txt->friendlyUrl($subCat['nazwa'])]) ?>"><?= $subCat['nazwa'] ?></a></li> 
                                    <?php
                                    }
                                    $i++;
                                }
                                ?>
                            </ul>
                            <?php
                        }
                        ?>
                    </div>

                </div>
                <?php
            }
            ?>
        </div>
    </div>
    <?php
}

