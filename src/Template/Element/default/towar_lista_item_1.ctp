<?php
if (!empty($towar)) {
    $oldPrice = null;
    $itemPriceDef = $this->Txt->itemPriceDef($towar, false);
        $itemPrice = $this->Txt->itemPrice($towar, false);
    if (empty($count)) {
        $count = 3;
    }
    $col = 12 / $count;
    if ($itemPriceDef > $itemPrice) {
        $oldPrice = $itemPriceDef;
    }
    ?>
    <div class="col-lg-<?= $col ?> col-md-6">
        <a href="<?= \Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'view', $towar->id, $this->Navi->friendlyUrl($towar->nazwa)]) ?>" class="list-item">
            <div class="image">
                <?php
                $imageSrc = '';
                $alt = $towar->nazwa;
                if (!empty($towar->towar_zdjecie)) {
                    if (file_exists($filePath['towar'] . $this->Txt->getKatalog($towar->towar_zdjecie[0]->id) . DS . 'thumb_2_' . $towar->towar_zdjecie[0]->plik)) {
                        $imageSrc = \Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'image', $towar->towar_zdjecie[0]->id, 'thumb_2_' . $towar->towar_zdjecie[0]->plik]);
                    }
                    if (!empty($towar->towar_zdjecie[0]->alt)) {
                        $alt = $towar->towar_zdjecie[0]->alt;
                    }
                }
                if (empty($imageSrc)) {
                    $imageSrc = $displayPath['webroot'] . 'img/noPhoto.png';
                }
                ?>
                <img src="<?= $imageSrc ?>" alt="<?= $alt ?>"/>
            </div>
            <div class="descriptions">
                <div class="name"><?= $towar->nazwa ?></div>
                <div class="description"><?= (!empty($towar->opis2) ? $towar->opis2 : $this->Txt->clearText($towar->opis, 100)) ?></div>
            </div>
            <div class="prices">
                <div class="price"><span class="pricesLabel"><?= $this->Translation->get('towar_lista.price') ?></span><span><?= $this->Txt->cena($itemPrice) . (!empty($oldPrice) ? '<span class="cenaDef">' . $this->Txt->cena($oldPrice) . '</span>' : '') ?></span></div>
               
            </div>
            <div class="detalis">
                <span><?= $this->Translation->get('towar_lista.showDetalis') ?></span>
            </div>
            <?php
            $badge = '';
            if (!empty($towar->promocja)) {
                $badge = 'promotion';
            }
            if (empty($badge) && !empty($towar->wyrozniony)) {
                $badge = 'exposure';
            }
            if (empty($badge) && !empty($towar->wyprzedaz)) {
                $badge = 'sell';
            }
            if (!empty($badge)) {
                echo $this->Html->tag('span', $this->Translation->get('towar_lista.' . $badge), ['class' => 'badge ' . $badge]);
            }
            if (!empty($cartItems) && !empty($cartItems['in_cart_ids']) && key_exists($towar->id, $cartItems['in_cart_ids'])) {
                echo $this->Html->tag('span', $this->Html->tag('i', '', ['class' => 'fa fa-shopping-basket']), ['class' => 'in-cart-badge', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $this->Translation->get('towar_lista.inCartTooltip')]);
            }
            ?>

        </a>
    </div>
<?php } ?>