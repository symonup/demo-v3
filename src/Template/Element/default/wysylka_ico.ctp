<div class="wysylka-ico-parent">
    <?php
    $wysylkaTxt = '';
    if ($towar->ilosc > 0) {
        if (!empty($towar->wysylka_info_dostepny)) {
            $wysylkaTxt = $towar->wysylka_info_dostepny;
        } else {
            $wysylkaTxt = $this->Translation->get('dostepnosc.dostawa24');
        }
    } else {
        if (!empty($towar->wysylka_info_niedostepny)) {
            $wysylkaTxt = $towar->wysylka_info_niedostepny;
        } else {
            $wysylkaTxt = $this->Translation->get('dostepnosc.naZamowienie');
        }
    }
    ?>
    <span class="wysylka-ico <?= (($towar->ilosc > 0) ? 'dostepny' : 'niedostepny') ?>"><span><?= $wysylkaTxt ?></span></span>
</div>