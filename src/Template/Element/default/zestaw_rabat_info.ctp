
    <?php 
    $content='';
    $popOver='';
    if($typ==1){
        $content = $this->Translation->get('zestaw.Rabat{0}',[$this->Html->tag('span',$rabat.'%')]);
        $popOver = $this->Translation->get('zestaw.RabatProcentowyInfo');
    }elseif($typ==2){
        $content = $this->Translation->get('zestaw.RabatNaZakupy{0}',[$this->Html->tag('span',$this->Txt->cena($rabat))]);
        $popOver = $this->Translation->get('zestaw.RabatKwotowyInfo');
    }else{
        $content = $this->Translation->get('zestaw.Bon{0}',[$this->Html->tag('span',$this->Txt->cena($rabat))]);
        $popOver = $this->Translation->get('zestaw.BonInfo');
    }
    ?>
<div class="zestaw-rabat-info">
    <button class="info-ico" type="button" data-toggle="popover" data-placement="right" data-container="body" data-trigger="hover | focus" data-html="true" data-content="<?= str_replace('"', "'", $popOver) ?>"><i class="fa fa-info-circle"></i></button>
    <?=$content?>
</div>