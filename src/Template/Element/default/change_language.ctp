<?php
$routerParams = [
    'controller' => $this->request->params['controller'],
    'action' => $this->request->params['action'],
];
$routerParams += $this->request->params['pass'];
if (!empty($this->request->query)) {
    $routerParams += $this->request->query;
}
unset($routerParams['view']);
?>
<div class="dropdown changeLng">
    <button class="btn btn-outline-success btn-small dropdown-toggle" type="button" id="dropdownLocales" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <img src="<?= $displayPath['lang'] . $active_jezyk->flaga ?>"/>
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownLocales">
        <?php
        foreach ($allLanguages as $lngItem) {
            ?>
            <a class="dropdown-item" onclick="$('#locale').val('<?= $lngItem->symbol ?>');$('#locale').trigger('change');"><img src="<?= $displayPath['lang'] . $lngItem->flaga ?>"/> <?= $lngItem->nazwa ?></a>
            <?php
        }
        ?>
    </div>
</div>
<div style="display: none;">
    <?= $this->Form->create(null, ['url' => ['controller' => 'Home', 'action' => 'changeLocale'], 'type' => 'post']) ?>
    <?= $this->Form->control('redirect', ['type' => 'hidden', 'value' => json_encode($routerParams)]) ?>
    <?= $this->Form->control('locale', ['type' => 'hidden', 'value' => $active_jezyk->symbol, 'onchange' => 'submit();']) ?>
    <?= $this->Form->end() ?>
</div>
