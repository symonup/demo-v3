<?php
if(!empty($item)){
        ?>
<a href="<?=$this->Txt->towarViewUrl($item)?>" title="<?=$item->nazwa?>" class="item-mini">
    <div class="image"><?=$this->Txt->towarZdjecie($item)?></div>
    <div class="name"><?=$item->nazwa?></div>
    <div class="price"><?=$this->Txt->cena($this->Txt->itemPrice($item))?></div>
</a>
<?php } ?>