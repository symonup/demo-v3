<?php
$znakWodny = Cake\Core\Configure::read('zdjecia.znak_wodny');
$defIndex = 0;
?>
    <div>
        <?php
        $imageSrc = '';
        $imageSrcBig = '';
        $alt = $towar->nazwa;
        if (!empty($towar->towar_zdjecie)) {
            if (!empty($towar->towar_zdjecie)) {
                if (file_exists($filePath['towar'] . $this->Txt->getKatalog($towar->towar_zdjecie[$defIndex]->id) . DS . 'thumb_3_' . $towar->towar_zdjecie[$defIndex]->plik)) {
                    $imageSrc = $displayPath['towar'].$this->Txt->getKatalog($towar->towar_zdjecie[$defIndex]->id).'/'.'thumb_3_' . $towar->towar_zdjecie[$defIndex]->plik;//\Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'image', $towar->towar_zdjecie[$defIndex]->id, 'thumb_3_' . $towar->towar_zdjecie[$defIndex]->plik, $znakWodny]);
                    $imageSrcBig = $displayPath['towar'].$this->Txt->getKatalog($towar->towar_zdjecie[$defIndex]->id).'/'.$towar->towar_zdjecie[$defIndex]->plik;//\Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'image', $towar->towar_zdjecie[$defIndex]->id, $towar->towar_zdjecie[$defIndex]->plik, $znakWodny]);
                }else{
                            if(!empty($towar->towar_zdjecie[$defIndex]->remote_url)){
                                $imageSrc=$imageSrcOrgin=$towar->towar_zdjecie[$defIndex]->remote_url;
                }
                        }
                if (!empty($towar->towar_zdjecie[$defIndex]->alt)) {
                    $alt = $towar->towar_zdjecie[$defIndex]->alt;
                }
            }
        }
        if (empty($imageSrc)) {
            $imageSrc = $displayPath['img'] . 'noPhoto.png';
        }
        ?>
        <div class="view-image" id="galeria-image-big" href="<?= $imageSrcBig ?>" data-toggle="lightbox">
            <img src="<?= $imageSrc ?>" alt="<?= $alt ?>" w="<?= !empty($znakWodny) ? 'true' : 'false' ?>"/>
            <div class="galeria-loader">
                <div class="sk-circle-galeria">
                    <div class="sk-circle1 sk-child"></div>
                    <div class="sk-circle2 sk-child"></div>
                    <div class="sk-circle3 sk-child"></div>
                    <div class="sk-circle4 sk-child"></div>
                    <div class="sk-circle5 sk-child"></div>
                    <div class="sk-circle6 sk-child"></div>
                    <div class="sk-circle7 sk-child"></div>
                    <div class="sk-circle8 sk-child"></div>
                    <div class="sk-circle9 sk-child"></div>
                    <div class="sk-circle10 sk-child"></div>
                    <div class="sk-circle11 sk-child"></div>
                    <div class="sk-circle12 sk-child"></div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="view-thumbs">
            <?php
            if (!empty($towar->towar_zdjecie) && count($towar->towar_zdjecie) > 3) {
                ?>
                <div class="view-thumbs-control slide-prev">
                    <a href="javascript:void(0);"><i class="fa fa-angle-left"></i></a>
                </div>
                <?php
            }
            ?>
            <div class="view-thumbs-inner">
                <?php
                if (!empty($towar->towar_zdjecie)) {
                    $ti=0;
                    $allThumbs=[];
                    foreach ($towar->towar_zdjecie as $imgIndex => $towarZdjecie) {
                        if (!empty($towarZdjecie->domyslne)) {
                            $defIndex = $imgIndex;
                        }
                        $imageSrc = '';
                        $imageSrcOrgin = '';
                        $alt = $towar->nazwa;
                        if (file_exists($filePath['towar'] . $this->Txt->getKatalog($towarZdjecie->id) . DS . 'thumb_' . $towarZdjecie->plik)) {
                            $imageSrc = $displayPath['towar'].$this->Txt->getKatalog($towarZdjecie->id).'/'.'thumb_' . $towarZdjecie->plik;//\Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'image', $towarZdjecie->id, 'thumb_' . $towarZdjecie->plik]);
                            $imageSrcOrgin = $displayPath['towar'].$this->Txt->getKatalog($towarZdjecie->id).'/' . $towarZdjecie->plik;//\Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'image', $towarZdjecie->id, $towarZdjecie->plik,$znakWodny]);
                        }else{
                            if(!empty($towarZdjecie->remote_url)){
                                $imageSrc=$imageSrcOrgin=$towarZdjecie->remote_url;
                        }
                        }
                        if (!empty($towarZdjecie->alt)) {
                            $alt = $towarZdjecie->alt;
                        }

                        if (empty($imageSrc)) {
                            $imageSrc = $displayPath['img'] . 'noPhoto.png';
                        }
                        $allThumbs[]=$this->Html->tag('div',$this->Html->image($imageSrc,['src-orgin'=>$imageSrcOrgin,'alt'=>$alt]),['class'=>'view-thumb'.(($ti==0)?' active':''),'version-id'=>(!empty($towarZdjecie->wariant_id) ? $towarZdjecie->wariant_id : false)]);
                        $ti++;
                    }
                }
                if(!empty($allThumbs)){
                    echo join('', $allThumbs);
                }
                ?>
            </div>
            <?php
            if (!empty($towar->towar_zdjecie) && count($towar->towar_zdjecie) > 3) {
                ?>
                <div class="view-thumbs-control slide-next">
                    <a href="javascript:void(0);"><i class="fa fa-angle-right"></i></a>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
<div id="image-preview" style="display: none;">
    <img/>
    <div class="image-preview-loader">
        <div class="spinner-preview">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
    <div class="image-preview-nav">
        <a data-target="prev" href="javascript:void();"><i class="fa fa-chevron-left"></i></a>
        <a data-target="next"  href="javascript:void();"><i class="fa fa-chevron-right"></i></a>
        <a data-target="close"  href="javascript:void();"><i class="fa fa-times"></i></a>
    </div>
</div>