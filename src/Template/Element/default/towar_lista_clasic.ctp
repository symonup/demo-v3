<div class="col-12 list-item-row">
    <?php
    $forceBadge = null;
    if (!empty($listLabel)) {
        switch ($listLabel) {
            case 'promocje':
                $forceBadge = 'promocja';
                break;
            case 'nowosci':
                $forceBadge = 'nowosc';
                break;
            case 'polecane':
                $forceBadge = 'polecany';
                break;
            case 'wyroznione':
                $forceBadge = 'wyrozniony';
                break;
            case 'wyprzedaz':
                $forceBadge = 'wyprzedaz';
                break;
            default:
                break;
        }
    }
    if (!empty($towary)) {
        foreach ($towary as $towar) {
            echo $this->element('default/towar_lista_item_clasic', ['towar' => $towar,'forceBadge'=>$forceBadge]);
        }
    }
    ?>
</div>
<?=(!empty($noPaging)?'':$this->element('default/paginator'))?>