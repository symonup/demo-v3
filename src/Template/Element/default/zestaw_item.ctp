<?php
        $showAllow = true;
        $zawartoscZ = [];
        $produktGlowny = $this->Html->tag('div', $this->Html->tag('div', $this->Txt->towarZdjecie($zestaw->towar), ['class' => 'image'])
                .$this->Html->tag('div', (!empty($zestaw->towar->zestaw_nazwa)?$zestaw->towar->zestaw_nazwa:$zestaw->towar->nazwa), ['class' => 'name'])
                .$this->Html->tag('div', $this->Txt->cena($this->Txt->itemPrice($zestaw->towar)), ['class' => 'price'])
                ,['class'=>'zestaw-item']);
        $_zestaw = '';
        $cena_zestawu = $this->Txt->itemPrice($zestaw->towar);
        $zt_i = 1;
        if ($zestaw->towar->ilosc <= 0) {
            $showAllow = false;
        }
        $zawartoscZ[] = (!empty($zestaw->towar->zestaw_nazwa) ? $zestaw->towar->zestaw_nazwa : $zestaw->towar->nazwa);
        foreach ($zestaw->zestaw_towar as $item) {
            if ($item->towar->ilosc <= 0) {
                $showAllow = false;
            }
            $cena_zestawu += $this->Txt->itemPrice($item->towar);
            $zawartoscZ[] = (!empty($item->towar->zestaw_nazwa) ? $item->towar->zestaw_nazwa : $item->towar->nazwa);

            $_zestaw .= $this->Html->tag('div', $this->Html->tag('div', $this->Txt->towarZdjecie($item->towar), ['class' => 'image'])
                .$this->Html->tag('div', (!empty($item->towar->zestaw_nazwa)?$item->towar->zestaw_nazwa:$item->towar->nazwa), ['class' => 'name'])
                .$this->Html->tag('div', $this->Txt->cena($this->Txt->itemPrice($item->towar)), ['class' => 'price'])
                ,['class'=>'zestaw-item'.(($zt_i == count($zestaw->zestaw_towar)) ? ' last' : '')]);
            $zt_i++;
        }
        $cena_rabat = round($cena_zestawu - (($cena_zestawu * round($zestaw->rabat, 2)) / 100), 2);
        $oszczedzasz = $cena_zestawu - $cena_rabat;
        $nazwa = $this->Html->tag('div', (!empty($zestaw->nazwa) ? $zestaw->nazwa : $this->Translation->get('towar_view.zestaw')), ['class' => 'zestaw-nazwa']);
        $zawartosc = $this->Html->tag('div', $this->Html->tag('div', $this->Translation->get('towar_view.zawartoscZestawu'), ['class' => 'zestaw-zawartosc-head']) . $this->Html->tag('div', join('<br/>', $zawartoscZ), ['class' => 'zestaw-zawartosc-items']), ['class' => 'zestaw-zawartosc']);
        $ceny = $this->Html->tag('div', $this->Txt->cena($cena_zestawu), ['class' => 'zestaw-cena-old']).$this->Html->tag('div', $this->Txt->cena($cena_rabat), ['class' => 'zestaw-cena']);
        $button = '<button zestaw-id="' . $zestaw->id . '" class="add-to-cart zestaw-button" type="button">'.$this->Translation->get('towar_view.zestawAddToCart').'</button>';
        if ($showAllow) {
            ?>
    <div class="zestaw zestaw-list-item">
        <div class="zestaw-items">
            <?= $produktGlowny . $_zestaw ?>
        </div>
        <div class="zestaw_podsumowanie">
            <div class="zestaw-ceny">
                <?=$ceny?>
            </div><div class="zestaw-button">
            <?= $button ?>
            </div>
        </div>
    </div>
<?php
        }
    