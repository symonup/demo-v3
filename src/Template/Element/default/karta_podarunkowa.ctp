<div class="karta-podarunkowa">
    <div class="octagonWrap small">
        <div class="octagon3">
            <div class="nazwa"><?= $karta->nazwa ?></div>
            <div class="row">
                <div class="col-12 col-md-6 padding-right-0">
                    <div class="image">
                        <?=(!empty($karta->zdjecie) ? $this->Html->image($displayPath['karta_podarunkowa'] . $karta->zdjecie,['alt'=>$karta->nazwa]) : '')?>
                    </div>
                </div>
                <div class="col-12 col-md-6 ps-relative pd-b-30">
                    <div class="wartosc"><?= $this->Translation->get('kartaPodarunkowa.wartosc{wartosc}',['wartosc'=>$this->Html->tag('span',$this->Txt->cena($karta->wartosc,null,true))]) ?></div>
            <div class="cena"><?= $this->Translation->get('kartaPodarunkowa.cena{cena}',['cena'=>$this->Html->tag('span',$this->Txt->cena($karta->cena,null,true))]) ?></div>
                </div>
            </div>
            <div class="kup">
                <button type="button" class="btn btn-block btn-sm btn-success add-to-cart-bonus" card-id="<?= $karta->id ?>"><?= $this->Translation->get('kartaPodarunkowa.dodajDoKoszyka') ?></button>
            </div>
        </div>
    </div>
</div>

