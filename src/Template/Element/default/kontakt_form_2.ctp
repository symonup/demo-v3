<form class="kontakt-form-2" method="post" action="<?= Cake\Routing\Router::url(['controller' => 'Contact', 'action' => 'index']) ?>">
    <input type="hidden" name="redirect" value="<?= $referer ?>"/>
    <div class="form-group">
        <input required="required" class="form-control" type="text" name="imie" placeholder="<?= $this->Translation->get('kontaktForm.YourName') ?>"/>
    </div>
    <div class="form-group">
        <input required="required" class="form-control" type="email" name="email" placeholder="<?= $this->Translation->get('kontaktForm.EmailAddress') ?>"/>
    </div>
    <div class="form-group">
        <textarea rows="8" required="required" class="form-control" name="tresc" placeholder="<?= $this->Translation->get('kontaktForm.Message') ?>"></textarea>
    </div>
    <?php /*
    <div class="form-group">
        <div class="kontakt-zgoda">
            <input type="checkbox" required="required" name="zgoda_dane" value="Tak" id="kontakt-zgoda-dane"/><label for="kontakt-zgoda-dane"><?= $this->Translation->get('kontaktForm.zgodaNaPrzetwarzanieDanych') ?></label>
        </div>
    </div> */ ?>
    <div class="form-group text-right">
        <button class="btn btn-dark" type="submit"><?= $this->Translation->get('kontaktForm.Send') ?><i class="fa fa-angle-right" style="margin-left: 15px;"></i></button>
    </div>
</form>