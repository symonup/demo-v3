        <div class="row" id="view-opinia-form">
            <div class="col-12">
                <div class="headerLine"><span><?= t('opinia.opinieHeader') ?></span></div>
            </div>
        </div>
<div class="row">
    <div class="col-12 col-md-12 col-lg-4">
        <div class="opinie-header">
            <div><?= $this->Translation->get('opinia.sredniaOpini') . $this->Html->tag('div', (!empty($towar->opinia_avg)?$towar->opinia_avg:'0'), ['class'=>'opinia-label']) . $this->Html->tag('span', '/' . $count_star) ?></div>
            <div class="mt-20"><?= $this->Translation->get('opinia.iloscOpini') . $this->Html->tag('div', count($towar->towar_opinia), ['class'=>'opinia-label']) ?></div>
        </div>
    </div><?php if(!$mobile) { ?> 
    <div class="col-12 col-md-12 col-lg-8">
        <?=$this->element('default/opinia_form')?>
    </div>
    <?php }else{
        ?>
    <div class="col-12 mt-10 mb-10">
    <button type="button" class="dodaj-opinie-mobile btn btn-block btn-outline-color-1" data-toggle="modal" data-target="#modal-opinia-form">
                            <?= $this->Translation->get('opinia.dodajOpinieMobile') ?> <i class="fa fa-plus"></i>
                        </button>
    </div>
    <?php
    } ?>
</div>
<?php
if (!empty($towar->towar_opinia)) {
    $gwiazdki = '';
    for ($gw_i = 1; $gw_i <= $count_star; $gw_i++) {
        $gwiazdki .= $this->Html->tag('span', '', ['class' => 'fa fa-star star' . (($gw_i <= $towar->opinia_avg) ? ' active' : ''), 'aria-hidden' => 'true']);
    }
    ?>

<div class="row">
    <div class="col-12">
        <div class="headerLine"><span><?=$this->Translation->get('opinia.OpinieUzytkownikow')?></span></div>
    </div>
</div>
    <div class="row">
        <?php
        foreach ($towar->towar_opinia as $opinia) {
            $opinia_gwiazdki = '';
            for ($ogw_i = 1; $ogw_i <= $count_star; $ogw_i++) {
                $opinia_gwiazdki .= $this->Html->tag('span', '', ['class' => 'fa fa-star star' . (($ogw_i <= $opinia->ocena) ? ' active' : ''), 'aria-hidden' => 'true']);
            }
            ?>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="ocena">
                    <div class="opinia-stars">
                        <?= $opinia_gwiazdki ?>
                    </div>
                    <div class="opinia-autor">
                        <?= $this->Translation->get('opinia.Dodana{0}Przez{1}', [$opinia->data->format('Y-m-d'), $this->Html->tag('span',$opinia->autor,['class'=>'fw-bold'])]) ?>
                    </div>
                </div>
                <div class="opinia">
                    <?= $opinia->opinia ?>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
    <?php
}
if($mobile){
    ?>
<div class="modal fade" id="modal-opinia-form" tabindex="-1" role="dialog" aria-labelledby="modal-opinia-formLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-opinia-formLabel"><?= $this->Translation->get('opinia.popUpTitle') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
               <?=$this->element('default/opinia_form')?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= $this->Translation->get('powiadomOCenie.btnAnuluj') ?></button>
            </div>
        </div>
    </div>
</div>
<?php
}
?>
