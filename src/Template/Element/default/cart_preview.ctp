<?php if (!empty($items)) {
    ?>
    <div id="cartPreview">
        <div class="previewItemsContainer">
            <table class="table koszyk-table">
                <tbody>
                    <?php
                    $rabat = 0;
                    $zestawyInCart = [];
                    if (!empty($items)) {
                        foreach ($items as $itemKey => $item) {
                            if (!empty($item['zestaw_id'])) {
                                $zestawyInCart[$item['zestaw_key']][$itemKey] = $item;
                                continue;
                            }
                            echo $this->element('default/order/cart_item', ['itemKey' => $itemKey, 'item' => $item,'inPreview'=>true]);
                            $razem = $item['ilosc'] * (key_exists('cena_z_rabatem', $item)?$item['cena_z_rabatem']:$item['cena_za_sztuke_brutto']);
                        }
                        $rabatKwotaZestaw = 0;
                        if (!empty($zestawyInCart)) {
                            $zestawCount = 0;
                            foreach ($zestawyInCart as $zestawKey => $zestawItems) {
                                $zestawCount++;
                                $zi = 1;
                                $bonus = [];
                                $bonusKey = null;
                                $zestawWartosc = 0;
                                $zestawId = null;
                                $zestawTowarId = null;
                                $zestawIlosc = 0;
                                $razemSztuka = 0;
                                $zestawSklad = [];
                                $zestawImages = [];
                                foreach ($zestawItems as $zestawItemKey => $zestawItem) {
                                    if (key_exists('zdjecie', $zestawItem)) {
                                        $zestawImages[] = $zestawItem['zdjecie'];
                                    }
                                    if (!empty($zestawItem['zestaw_bonus'])) {
                                        $bonus = $zestawItem;
                                        $bonusKey = $zestawItemKey;
                                        if ($zestawItem['typ'] == 'kwota') {
                                            $rabatKwotaZestaw += ($zestawItem['wartosc'] * $zestawItem['zestaw_ilosc']);
                                        }
                                        continue;
                                    } else {
                                        if (empty($zestawId)) {
                                            $zestawId = $zestawItem['zestaw_id'];
                                        }
                                        if (empty($zestawTowarId)) {
                                            $zestawTowarId = $zestawItem['towar_id'];
                                        }
                                        if (empty($zestawIlosc)) {
                                            $zestawIlosc = $zestawItem['ilosc'];
                                        }
                                        $zestawSklad[] = $zestawItem['nazwa'];
                                    }
                                    $razem = $zestawItem['ilosc'] * (key_exists('cena_z_rabatem', $zestawItem)?$zestawItem['cena_z_rabatem']:$zestawItem['cena_za_sztuke_brutto']);
                                    $razemSztuka += (key_exists('cena_z_rabatem', $zestawItem)?$zestawItem['cena_z_rabatem']:$zestawItem['cena_za_sztuke_brutto']);
                                    $zestawWartosc += $zestawItem['cena_podstawowa'];
                                    $zi++;
                                }
                                $zestawItem = [
                                    'nazwa' => join('<br/>', $zestawSklad),
                                    'ilosc' => $zestawIlosc,
                                    'cena_z_rabatem' => $razemSztuka,
                                    'cena_za_sztuke' => $razemSztuka,
                                    'towar_id' => $zestawTowarId,
                                    'zestaw_id' => $zestawId,
                                    'zestaw_key' => $zestawKey,
                                    'zestaw_nazwa' => (!empty($bonus['zestaw_nazwa']) ? $bonus['zestaw_nazwa'] : $this->Translation->get('labels.Zestaw')),
                                    'zdjecia' => $zestawImages
                                ];
                                echo $this->element('default/order/cart_item', ['zi' => 1, 'zestawKey' => $zestawKey, 'zestawItems' => $zestawItems, 'itemKey' => $bonusKey, 'item' => $zestawItem,'inPreview'=>true]);
                            }
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="text-right mt-10 pd-r-10">
            <a href="<?= \Cake\Routing\Router::url(['controller' => 'Cart', 'action' => 'index']) ?>" class="btn btn-color-2 btn-sm"><?= $this->Translation->get('koszyk.przejdzDoKoszyka') ?></a>
        </div>
    </div>
    <?php }
?>