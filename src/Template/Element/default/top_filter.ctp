<?php
$baseRoute = ['controller' => $this->request->params['controller'], 'action' => $this->request->params['action']] + $this->request->params['pass'];
$maxFItems = 3;
$filterKategorie = '';
if (!empty($allCats)) {
    $filterKategorie = $this->Navi->categoriesTreeInput($allCats, ((!empty($filters) && !empty($filters['kategoria']) && !empty($filters['kategoria']['_ids'])) ? $filters['kategoria']['_ids'] : (!empty($kategoria) ? [$kategoria->id => $kategoria->id] : [])), ((!empty($filters) && !empty($filters['preorders'])) ? $filters['preorders'] : null), true);
}
?>
<?= $this->Form->create(null, ['id' => 'filterForm']) ?>
<ul class="towar-lista-filter nice-checkbox">
    <?php
    if (!empty($filterKategorie)) {
        ?>
        <li class="filter-category-container">
            <div class="filterLabel"><?= $this->Translation->get('towar_lista.kategoria') ?></div>
            <div class="filterSelected"><?= (!empty($kategoria) ? $kategoria->nazwa : $this->Translation->get('towar_lista.wszystkieKategorie')) ?></div>
            <div class="filterList">
                <ul class="filter-category">
                    <?= $filterKategorie ?>
                </ul>
            </div>
        </li>
        <?php
    }
    if (!empty($platformy) && count($platformy)>1) {
        $platformaAttrs = [];
        $platformaSelected = [];
        foreach ($platformy as $platformaId => $platformaNazwa) {
            $platformaAttrs[] = $this->Form->control('platforma._ids', ['type' => 'checkbox', 'checked' => ((!empty($filters['platforma']) && !empty($filters['platforma']['_ids'][$platformaId])) ? true : false), 'name' => 'platforma[_ids][]', 'id' => 'filter-platforma-' . $platformaId, 'value' => $platformaId, 'label' => $platformaNazwa]);
            if ((!empty($filters['platforma']) && !empty($filters['platforma']['_ids'][$platformaId]))) {
                $platformaSelected[$platformaId] = $platformaNazwa;
            }
        }
        ?>
        <li>
            <div class="filterLabel"><?= $this->Translation->get('towar_lista.platforma') ?></div>
            <div class="filterSelected"><?= (!empty($platformaSelected) ? $platformaSelected[array_keys($platformaSelected)[0]].((count($platformaSelected)>1)?', +'.(count($platformaSelected)-1):'') : $this->Translation->get('towar_lista.wszystkiePlatformy')) ?></div>
            <div class="filterList">
                <ul class="filter-checxbox">
                    <li><?= join('</li><li>', $platformaAttrs) ?></li>
                </ul>
            </div>
        </li>
        <?php
    }
    if (!empty($gatunkiGier)) {
        $gatunekSelected=[];
        $gatunekAttrs=[];
         foreach ($gatunkiGier as $gatunek) {
                       $gatunekAttrs[]=$this->Form->control('gatunek._ids', ['type' => 'checkbox', 'checked' => ((!empty($filters['gatunek']) && !empty($filters['gatunek']['_ids'][$gatunek->id])) ? true : false), 'name' => 'gatunek[_ids][]', 'id' => 'filter-gatunek-' . $gatunek->id, 'value' => $gatunek->id, 'label' => $gatunek->nazwa]);
                       if(!empty($filters['gatunek']) && !empty($filters['gatunek']['_ids'][$gatunek->id])){
                           $gatunekSelected[$gatunek->id]=$gatunek->nazwa;
                       }
         }
        ?>
        <li>
            <div class="filterLabel"><?= $this->Translation->get('towar_lista.gatunek') ?></div>
            <div class="filterSelected"><?= (!empty($gatunekSelected) ? $gatunekSelected[array_keys($gatunekSelected)[0]].((count($gatunekSelected)>1)?', +'.(count($gatunekSelected)-1):'') : $this->Translation->get('towar_lista.wszystkieGatunki')) ?></div>
            <div class="filterList">
                <ul class="filter-checxbox">
                    <li><?= join('</li><li>', $gatunekAttrs) ?></li>
                </ul>
            </div>
        </li>
        <?php
    }
    $pegiWiek = \Cake\Core\Configure::read('pegi_wiek');
    if(!empty($pegiWiek)){
        $pegiWiekAttr=[];
        ?>
        <li>
            <div class="filterLabel"><?= $this->Translation->get('towar_lista.pegiWiek') ?></div>
            <div class="filter-slider">
                <input type="hidden" id="pegi-range" value="<?=((!empty($filters) && key_exists('age', $filters))?$filters['age']:'3;18')?>" name="age"/>
        <input type="hidden" class="pegi-from"/>
        <input type="hidden" class="pegi-to"/>
            </div>
        </li>
        <?php
//        foreach ($pegiWiek as $wiek => $pegiNazwa){
//            $pegiWiekAttr[]=$this->Form->control('gatunek._ids', ['type' => 'checkbox', 'checked' => ((!empty($filters['gatunek']) && !empty($filters['gatunek']['_ids'][$gatunek->id])) ? true : false), 'name' => 'gatunek[_ids][]', 'id' => 'filter-gatunek-' . $gatunek->id, 'value' => $gatunek->id, 'label' => $gatunek->nazwa]);
//                       if(!empty($filters['gatunek']) && !empty($filters['gatunek']['_ids'][$gatunek->id])){
//                           $gatunekSelected[$gatunek->id]=$gatunek->nazwa;
//                       }
//        }
    }
    $pegiTyp = \Cake\Core\Configure::read('pegi_typ');
    if(!empty($pegiTyp)){
        $pegiTypAttr=[];
        $pegiTypSelected=[];
        foreach ($pegiTyp as $typ => $pegiNazwa){
            $pegiTypAttr[]=$this->Form->control('pegi.types', ['type' => 'checkbox', 'checked' => ((!empty($filters['pegi']) && !empty($filters['pegi']['types'][$typ])) ? true : false), 'name' => 'pegi[types][]', 'id' => 'filter-pegi-type-' . $typ, 'value' => $typ, 'label' => $this->Translation->get('pegi_typ.'.$typ)]);
                       if(!empty($filters['pegi']) && !empty($filters['pegi']['types'][$typ])){
                           $pegiTypSelected[$typ]=$this->Translation->get('pegi_typ.'.$typ);
                       }
        }
        
        ?>
        <li>
            <div class="filterLabel"><?= $this->Translation->get('towar_lista.pegiTyp') ?></div>
            <div class="filterSelected"><?= (!empty($pegiTypSelected) ? $pegiTypSelected[array_keys($pegiTypSelected)[0]].((count($pegiTypSelected)>1)?', +'.(count($pegiTypSelected)-1):'') : $this->Translation->get('towar_lista.wszystkieTypyPegi')) ?></div>
            <div class="filterList">
                <ul class="filter-checxbox">
                    <li><?= join('</li><li>', $pegiTypAttr) ?></li>
                </ul>
            </div>
        </li>
        <?php
    }
    $attrsList = [];
    $attrTypeSelected = [];
    if (!empty($attrsAll)) {
        foreach ($attrsAll as $atrybutTyp) {
            if (!empty($atrybutTyp->atrybut)) {
                $attrsList[$atrybutTyp->id] = ['id' => $atrybutTyp->id, 'nazwa' => $atrybutTyp->nazwa];
                $tmpI = 0;
                foreach ($atrybutTyp->atrybut as $attr) {
                    if (!key_exists($attr->id, $countAttrs) || empty($countAttrs[$attr->id])) {
                        continue;
                    }
                    if (!empty($attr->pole)) {
                        continue;
                    }
                    $tmpI++;
                    if (!empty($filters['attr']) && !empty($filters['attr'][$atrybutTyp->id][$attr->id])) {
                        $attrTypeSelected[$atrybutTyp->id][$attr->id] = $attr->nazwa;
                    }
                    $attrsList[$atrybutTyp->id]['attrs'][$attr->id] = $this->Html->tag('li', $this->Form->control('attr.' . $atrybutTyp->id . '._ids', ['type' => 'checkbox', 'checked' => ((!empty($filters['attr']) && !empty($filters['attr'][$atrybutTyp->id][$attr->id])) ? true : false), 'name' => 'attr[' . $atrybutTyp->id . '][]', 'id' => 'filter-attr-' . $attr->id, 'value' => $attr->id, 'label' => $attr->nazwa]), ['class' => (($tmpI > $maxFItems) ? 'item-more' : '')]);
                }
                if ($tmpI > $maxFItems) {
                    $attrsList[$atrybutTyp->id]['attrs']['more'] = $this->Html->tag('li', $this->Html->tag('span', '<i class="fa fa-plus-square"></i> ' . $this->Translation->get('towar_lista.wiecej'), ['class' => 'filter-more-btn', 'more-txt' => '<i class=\'fa fa-plus-square\'></i> ' . $this->Translation->get('towar_lista.wiecej'), 'less-txt' => '<i class=\'fa fa-minus-square\'></i> ' . $this->Translation->get('towar_lista.mniej')]));
                }
            }
        }
    }
    if (!empty($attrsList)) {
        ?>
        <?php
        foreach ($attrsList as $typeId => $attrItem) {
            if (empty($attrItem['attrs'])) {
                continue;
            }
            $attrUnique = uniqid();
            ?>
            <li>
                <div class="filterLabel"><?= $attrItem['nazwa'] ?></div>
                <div class="filterSelected"><?= ((!empty($attrTypeSelected) && !empty($attrTypeSelected[$typeId])) ? $attrTypeSelected[$typeId][array_keys($attrTypeSelected[$typeId])[0]].((count($attrTypeSelected[$typeId])>1)?', +'.(count($attrTypeSelected[$typeId])-1):'') : $this->Translation->get('towar_lista.pokazWszystkie')) ?></div>
                <div class="filterList">
                    <ul class="filter-checxbox" id="attr-<?= $attrUnique ?>">
                        <?= join('', $attrItem['attrs']) ?>
                    </ul>
                </div>
            </li>
            <?php
        }
        ?>
            <?php 
            $allStats=[
                'dostepny'=>$this->Translation->get('status.dostepny'),
                'preorder'=>$this->Translation->get('status.preorder'),
                'promocja'=>$this->Translation->get('status.promocja'),
                'wyprzedaz'=>$this->Translation->get('status.wyprzedaz'),
                'wyrozniony'=>$this->Translation->get('status.bestseller'),
                'nowosc'=>$this->Translation->get('status.nowosc'),
                'polecany'=>$this->Translation->get('status.polecany')
            ];
            $statusy=[];
            $statusSelected=[];
            foreach ($allStats as $status =>$statusNazwa){
            $statusy[]=$this->Form->control('status', ['type' => 'checkbox', 'checked' => ((!empty($filters['status']) && !empty($filters['status'][$status])) ? true : false), 'name' => 'status[]', 'id' => 'filter-status-' . $status, 'value' => $status, 'label' => $statusNazwa]);
           if(!empty($filters['status']) && !empty($filters['status'][$status])){
               $statusSelected[$status]=$statusNazwa;
           }
            }
            ?>
        <li>
            <div class="filterLabel"><?= $this->Translation->get('towar_lista.Status') ?></div>
            <div class="filterSelected"><?= (!empty($statusSelected) ? $statusSelected[array_keys($statusSelected)[0]].((count($statusSelected)>1)?', +'.(count($statusSelected)-1):'') : $this->Translation->get('towar_lista.pokazWszystkie')) ?></div>
                 <div class="filterList">
                     <ul class="filter-checxbox" id="filter-status">
                         <li><?= join('</li><li>', $statusy) ?></li>
                     </ul>
                 </div>
        </li>
        <li>
            <div class="filterLabel"><?= $this->Translation->get('towar_lista.priceRange') ?></div>
            <div id="price-range-inputs">
                <div><span><?= $this->Translation->get('towar_lista.priceFrom') ?></span><input class="price-from"/></div>
                <span>-</span>
                <div><span><?= $this->Translation->get('towar_lista.priceTo') ?></span><input class="price-to"/></div>
            </div>
            <div class="filter-slider filter-slider-price">
            <input type="hidden" data-prefix="<?= !($currencyOptions['after']) ? $currencySymbol . (!empty($currencyOptions['space']) ? ' ' : '') : '' ?>" data-postfix="<?= ($currencyOptions['after']) ? (!empty($currencyOptions['space']) ? ' ' : '') . $currencySymbol : '' ?>" id="price-range" value="<?= (!empty($filters['price']) ? $filters['price'] : '') ?>" data-max="<?= $maxPrice ?>" data-step="1" name="price"/>
            </div>
        </li>
        <?php
    }
    ?>
    
    <?php
    ?>
</ul>
<div class="row justify-content-center visible-only-xs" id="xsSubmitFilterRow">
        <div class="col-12">
            <button id="xsSubmitFilter" class="btn btn-outline-success btn-block"><?= $this->Translation->get('left.mobileZastosujFiltr') ?></button>
        </div>
    </div>
    
    <?= $this->Form->end() ?>
<span class="btn btn-outline-success btn-sm pull-right" id="filterShowXs"><span class="f-hidden-visible"><i class="fas fa-filter"></i> <?= $this->Translation->get('left.FliterButtonMobile') ?></span><span class="f-open-visible"><i class="fa fa-times"></i></span></span>