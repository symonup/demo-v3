<?php if (!empty($hotDeal)) { ?>
    <div class="hot-deal-limit">
        <div class="info-head"><?= $this->Translation->get('hotDeal.title') ?></div>
        <div class="items-sold"><?= $this->Translation->get('hotDeal.sprzedano{0}sztuk', [$this->Html->tag('span', (!empty($hotDeal->sprzedano) ? $hotDeal->sprzedano : 0))]) ?></div>
    </div>
    <a class="octagonWrap xs-small" href="<?= Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'view', $hotDeal->towar->id, $this->Navi->friendlyUrl($hotDeal->towar->nazwa)]) ?>">
        <div class="octagon3">
            <div class="content"> 
                <div class="hot-deal">
                    <div class="info">
                        <div class="items-left"><?= $this->Translation->get('hotDeal.pozostalo{0}sztuk', [$this->Html->tag('span', $hotDeal->ilosc)]) ?></div>
        
                                    <div class="icon"><?= ((!empty($hotDeal->towar->platforma) && (!empty($hotDeal->towar->platforma->logo) || !empty($hotDeal->towar->platforma->logo_2))) ? $this->Html->image($displayPath['platforma'] . (!empty($hotDeal->towar->platforma->logo_2) ? $hotDeal->towar->platforma->logo_2 : $hotDeal->towar->platforma->logo), ['alt' => $hotDeal->towar->platforma->nazwa]) : '') ?></div>
                    <div class="timer">
                        <div class="timer-label"><?= $this->Translation->get('hotDeal.timerMobileTitle') ?></div>
                        <?= $this->element('default/time_counter', ['endTime' => (!empty($hotDeal->data_do) ? $hotDeal->data_do->format('Y-m-d H:i:00') : date('Y-m-d')), 'timeCounterId' => 'hot-deal-time']) ?>
                    </div>
                    
                            <div class="price"> 
                                <?php
                                $priceRegular = $this->Txt->itemPriceDef($hotDeal->towar, false);
                                $priceSpecial = (!empty($hotDeal->cena) ? $hotDeal->cena : $this->Txt->itemPrice($hotDeal->towar, false));
                                $discount = $priceRegular - $priceSpecial;
                                ?>
                                <div class="regular-price"><span><?= $this->Txt->cena($priceRegular, null, true) ?></span></div>
                                <div class="promotion-price"><?= $this->Txt->cena($priceSpecial, null, true) ?></div>
                            </div>
                            <?php if ($discount > 0) { ?>
                                <div class="oszczedzasz"><?= $this->Translation->get('hotDeal.oszczedzasz{0}', [$this->Html->tag('span', $this->Txt->cena($discount, null, true))]) ?></div>
                            <?php } ?>
                </div>
                    <div class="image">
                        <?= $this->Txt->towarZdjecie($hotDeal->towar, 'thumb_2_') ?>

                    </div>

                </div>
            </div>
        </div>
    </a>

<div class="name"><a href="<?= Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'view', $hotDeal->towar->id, $this->Navi->friendlyUrl($hotDeal->towar->nazwa)]) ?>"><?= (!empty($hotDeal->nazwa) ? $hotDeal->nazwa : $hotDeal->towar->nazwa) ?></a></div>
<?php } 
?>