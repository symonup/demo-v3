<?php
if (!empty($towar)) {
    $oldPrice = null;
    $itemPriceDef = $this->Txt->itemPriceDef($towar);
    $itemPrice = $this->Txt->itemPrice($towar);
    if (empty($count)) {
        $count = 3;
    }
    if (empty($premiera)) {
        $premiera = false;
    }
    if ($itemPriceDef > $itemPrice) {
        $oldPrice = $itemPriceDef;
    }
    if (!empty($hideBadge)) {
        $badge = '';
    } else {
        $badge = '';
        $badgeType = '';
        if (!empty($forceBadge)) {
            $badgeType = $forceBadge;
        } else {
            if (!empty($towar->promocja) && (empty($hideBadgeTyp) || !(array_search('promocja', $hideBadgeTyp) !== false))) {
                $badgeType = 'promocja';
            } elseif (!empty($towar->wyprzedaz) && (empty($hideBadgeTyp) || !(array_search('wyprzedaz', $hideBadgeTyp) !== false))) {
                $badgeType = 'wyprzedaz';
            } elseif (!empty($towar->nowosc) && (empty($hideBadgeTyp) || !(array_search('nowosc', $hideBadgeTyp) !== false))) {
                $badgeType = 'nowosc';
            } elseif (!empty($towar->polecany) && (empty($hideBadgeTyp) || !(array_search('polecany', $hideBadgeTyp) !== false))) {
                $badgeType = 'polecany';
            } elseif (!empty($towar->wyrozniony) && (empty($hideBadgeTyp) || !(array_search('wyrozniony', $hideBadgeTyp) !== false))) {
                $badgeType = 'wyrozniony';
            } elseif (!empty($towar->produkt_tygodnia) && (empty($hideBadgeTyp) || !(array_search('produkt_tygodnia', $hideBadgeTyp) !== false))) {
                $badgeType = 'produkt_tygodnia';
            }
        }
        if (!empty($hideBadgeTyp) && array_search($badgeType, $hideBadgeTyp) !== false) {
            $badgeType = '';
        }
        if (!empty($badgeType)) {
            switch ($badgeType) {
                case 'promocja' : {
                        $badge = $this->Html->tag('span', $this->Translation->get('labels.Promocja'), ['class' => 'badge-label difFont promocja']);
                    } break;
                case 'wyprzedaz' : {
                        $badge = $this->Html->tag('span', $this->Translation->get('labels.Wyprzedaz'), ['class' => 'badge-label difFont wyprzedaz']);
                    } break;
                case 'nowosc' : {
                        $badge = $this->Html->tag('span', $this->Translation->get('labels.Nowosc'), ['class' => 'badge-label difFont nowosc']);
                    } break;
                case 'polecany' : {
                        $badge = $this->Html->tag('span', $this->Translation->get('labels.Polecany'), ['class' => 'badge-label difFont polecany']);
                    } break;
                case 'wyrozniony' : {
                        $badge = $this->Html->tag('span', $this->Translation->get('labels.Wyrozniony'), ['class' => 'badge-label difFont wyrozniony']);
                    } break;
                case 'produkt_tygodnia' : {
                        $badge = $this->Html->tag('span', $this->Translation->get('labels.produktTygodnia'), ['class' => 'badge-label difFont produkt_tygodnia']);
                    } break;
                default : $badge = '';
                    break;
            }
        }
    }
    ?>
    <div class="product item-min">
        <div class="content"> 
                <a href="<?= $this->Txt->towarViewUrl($towar) ?>" class="product-name">
                    <span><?= $towar->nazwa ?></span>
                </a>
            <a href="<?= $this->Txt->towarViewUrl($towar) ?>" class="product-image">
                <?= $this->Txt->towarZdjecie($towar, 'thumb_3_') ?>
            </a>
            <div class="product-footer">
                    <?php
                    if (!empty($oldPrice)) {
                        ?>
                        <span class="old-price"><?= $this->Txt->cena($oldPrice) ?></span>
                        <?php
                    }
                /*  
                 *  <span add-target="<?= Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'addToFavorite', $towar->id]) ?>" remove-target="<?= Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'removeFavorite', $towar->id]) ?>" item-id="<?= $towar->id ?>" class="favorite <?= ((!empty($favorite) && key_exists($towar->id, $favorite)) ? 'active' : '') ?>"><span><?= $this->Translation->get('labels.DodajDoSchowka') ?></span><span class="on-active"><?= $this->Translation->get('labels.UsunZeSchowka') ?></span></span>
                */
                    ?>
                        <div class="price<?= (!empty($oldPrice) ? ' promotion' : '') ?>">
                        <span class="<?= !empty($oldPrice) ? 'promo' : '' ?>"><?= $this->Txt->cena($itemPrice) ?></span>
                    </div>
                    <div class="cart-form add-to-cart-container">
                        <?php
                        $addAction = ['controller' => 'Cart', 'action' => 'add', $towar->id];
                        if (!empty($addAsTr)) {
                            $addAction[] = 1;
                        }
                        ?>
                        <button type="button" item-id="<?= $towar->id ?>" add-action="<?= \Cake\Routing\Router::url($addAction) ?>" version-id="<?= (!empty($firstWersion) ? $firstWersion->id : '') ?>" class="btn btn-danger add-to-cart"><i class="flat flaticon-supermarket"></i></button>
                        <?php
                        ?>

                    </div>
            </div>
        </div>
        <?php // $badge ?>
    </div>
<?php } ?>
