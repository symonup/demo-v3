<?php
if (empty($carouselId)) {
    $carouselId = 'carusel-' . uniqid();
}
$pages = 0;
if (empty($hideBadge)) {
    $hideBadge = false;
}
if (empty($hideBadgeTyp)) {
    $hideBadgeTyp = [];
}
if(empty($premiera)){
    $premiera=false;
}
if (!empty($items)) {
    ?>
    <div id="<?= $carouselId ?>" class="carousel slide carusel-items" data-ride="carousel">
        <div class="carousel-inner">
            <?php
            $pages = count($items);

            foreach ($items as $key => $item) {
                ?>
                <div class="carousel-item <?= ($key == 0 ? 'active' : '') ?>">
                    <div class="row">
                        <div class="col-12"><?= $item ?></div>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
        <?php
        if ($pages > 1) {
            ?>
            <a class="left carousel-control no-shadow" href="#<?= $carouselId ?>" role="button" data-slide="prev">
                <span class="carusel-control-img carusel-control-img-prev" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control no-shadow" href="#<?= $carouselId ?>" role="button" data-slide="next">
                <span class="carusel-control-img carusel-control-img-next" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            <?php
        }
        ?>
    </div>
<?php } ?>