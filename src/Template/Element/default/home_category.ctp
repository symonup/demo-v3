<?php
if (!empty($allCats)) {
    ?>
    <div class="container-fluid homeCategory">
        <div class="row justify-content-center mb-20">
            <div class="col-12">
                <div class="headerLine"><span><?= t('labels.naszeKategorie') ?></span></div>
            </div>
        </div>
            <?php
            $allItemsCat = [];
            foreach ($allCats as $mainCat) {
                if (!empty($mainCat['sub'])) {
                    foreach ($mainCat['sub'] as $subCat) {
                        if (empty($subCat['promuj'])) {
                            continue;
                        }
                        $allItemsCat[] = $this->element('default/home_category_item', ['ikona' => (!empty($subCat['ikona']) ? $displayPath['kategoria_ikony'] . $subCat['ikona'] : null), 'nazwa' => $subCat['nazwa'], 'link' => Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'lista', $subCat['id'], $this->Txt->friendlyUrl($subCat['nazwa'])])]);
                    }
                } else {
                    if (empty($mainCat['promuj'])) {
                        continue;
                    }
                    $allItemsCat[] = $this->element('default/home_category_item', ['ikona' => (!empty($mainCat['ikona']) ? $displayPath['kategoria_ikony'] . $mainCat['ikona'] : null), 'nazwa' => $mainCat['nazwa'], 'link' => Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'lista', $mainCat['id'], $this->Txt->friendlyUrl($mainCat['nazwa'])])]);
                    ?>
                    <?php
                }
            }
            $allCatsParts = array_chunk($allItemsCat, ceil((count($allItemsCat)/2)));
            ?>
        <div class="row justify-content-center home-cat-container">
            <div class="col-6 col-md homeCategoryItem">
                <a href="<?= Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'zestawy']) ?>" class="name">
                        <img src="/img/zestawy.png"/>
                    <span><?= t('labels.kupWZestawieTaniej') ?></span>
                </a>
            </div>
            <?= join('', $allCatsParts[0])?>
        </div>
        <div class="row justify-content-center home-cat-container">
            <div class="col-6 col-md homeCategoryItem">
                <a href="<?= Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'index','prezenty']) ?>" class="name">
                        <img src="/img/prezenty.png"/>\
                    <span><?= t('labels.prezentyNaKazdaOkazje') ?></span>
                </a>
            </div>
            <?= join('', $allCatsParts[1])?>
        </div>
            <?php
            ?>
        </div>
    <?php
}

