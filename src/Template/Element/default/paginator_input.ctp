<?php
$active = (int) $this->Paginator->current();
$all = (int) $this->Paginator->counter([
            'format' => '{{pages}}'
        ]);
if ($all > 1) {
if(!empty($this->request->params['type'])){
    $this->Paginator->options([
    'url' => [
        'controller'=>$this->request->params['controller'],'action'=>$this->request->params['action'],'type'=>$this->request->params['type']
            ]
    ]);
}
$baseUrl = ['controller'=>'Towar','action'=>'index'];
$pass = $this->request->getParam('pass');
if(!empty($pass)){
    foreach ($pass as $passItem){
        $baseUrl[]=$passItem;
    }
}
$urlQuery = $this->request->getQuery();
if(!empty($urlQuery)){
    foreach ($urlQuery as $queryName => $queryVal){
        if($queryName=='page'){
            continue;
        }
        $baseUrl[$queryName]=$queryVal;
    }
}
    ?>
            <ul class="pagination">
                <?php
                $curr = $this->Paginator->current();
                $total = $this->Paginator->total();
                if ($curr > 1 && $total > 2) { ?>
                    <?= $this->Paginator->first(__('1')) ?>
                <?php } if ($curr > 1 && $total > 2) {
                    ?>
                    <li><span>...</span></li>
                <?php }
                if($curr>1){
                echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', ['escape' => false]);
                } 
                if($total > 2){
                    ?>
                    <li><input type="text" data-type="number" data-url="<?= Cake\Routing\Router::url($baseUrl)?>" min="1" max="<?=$total?>" value="<?=$curr?>" class="goToPage"/></li>
                    <?php
                }else{
                ?>
                <li><?= $this->Paginator->numbers(['modulus' => 4]) ?></li>
                <?php
                }
                if($curr<$total) {
                    echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', ['escape' => false]);
                }
                if ($curr < $total && $total > 2) {
                    ?>
                    <li><span>...</span></li>
                    <?php
                }
                if ($curr < $total && $total > 2) {
                    echo $this->Paginator->last((string) $total);
                }
                ?>
            </ul>
    <?php
}
?>
