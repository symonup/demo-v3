<?php
$baseRoute = ['controller' => $this->request->params['controller'], 'action' => $this->request->params['action']] + $this->request->params['pass'];
$maxFItems = 3;
$filterKategorie = '';
if (!empty($kategorieAll)) {
    $filterKategorie = $this->Navi->getAllCatByUl($kategorieAll,20, (!empty($kategoria) ? $kategoria->id : null));
}
$kategoryName = $this->Translation->get('left.Menu');
if(!empty($kategoria)){
    $kategoryName = $kategoria->nazwa;
    $backToUrl = ['controller'=>'Towar','action'=>'index'];
    $backToLabel = t('left.wszystkieKategorie');
    if(!empty($kategoria->parent_kategorium) && !empty($kategoria->parent_kategorium->parent_kategorium)){
        $backToUrl[]=$kategoria->parent_kategorium->parent_kategorium->id;
        $backToUrl[]=$this->Txt->friendlyUrl($kategoria->parent_kategorium->parent_kategorium->nazwa);
        $backToLabel=$kategoria->parent_kategorium->parent_kategorium->nazwa;
//        $kategoryName=$kategoria->parent_kategorium->nazwa;
    }
    elseif(!empty($kategoria->parent_kategorium)){
        $backToUrl[]=$kategoria->parent_kategorium->id;
        $backToUrl[]=$this->Txt->friendlyUrl($kategoria->parent_kategorium->nazwa);
        $backToLabel=$kategoria->parent_kategorium->nazwa;
//        $kategoryName=$kategoria->parent_kategorium->nazwa;
    }
}
?>

<div class="special-head">
    <div class="kategoria-curent-name"><?= $this->Html->tag((!empty($kategoria)?'h1':'span'),$kategoryName) ?><i class="flat flaticon-heart-2"></i></div>
<?php if(!empty($kategoria)){
    ?>
    <div class="kategorie-back-to">
        <?=t('left.cofnijDo{0}',[$this->Html->link($backToLabel,$backToUrl,['escape'=>false])])?>
    </div>
    <?php
} ?>
</div>
<?php
if (!empty($filterKategorie)) {
    ?>
    <div class="filter-category-container">
        <ul class="filter-category">
            <?= $filterKategorie ?>
        </ul>
    </div>
    <?php
}
$hasAttrs=false;
$attrsList = [];
if (!empty($attrsAll)) {
    foreach ($attrsAll as $atrybutTyp) {
        if (!empty($atrybutTyp->atrybut)) {
            $attrsList[$atrybutTyp->id] = ['id' => $atrybutTyp->id, 'nazwa' => $atrybutTyp->nazwa];
            $tmpI = 0;
            foreach ($atrybutTyp->atrybut as $attr) {
                if(!key_exists($attr->id, $countAttrs) || empty($countAttrs[$attr->id])){
                    continue;
                }
                if (!empty($attr->pole)) {
                    continue;
                }
                $tmpI++;
                $attrsList[$atrybutTyp->id]['attrs'][$attr->id] = $this->Html->tag('li', $this->Form->control('attr.' . $atrybutTyp->id . '._ids', ['type' => 'checkbox', 'checked' => ((!empty($filters['attr']) && !empty($filters['attr'][$atrybutTyp->id][$attr->id])) ? true : false), 'name' => 'attr[' . $atrybutTyp->id . '][]', 'id' => 'filter-attr-' . $attr->id, 'value' => $attr->id, 'label' => $attr->nazwa . ((key_exists($attr->id, $countAttrs) && !empty($countAttrs[$attr->id])) ? ' (' . $countAttrs[$attr->id] . $this->Translation->get('labels.szt') . ')' : '')]), ['class' => (($tmpI > $maxFItems) ? 'item-more' : '')]);
           $hasAttrs=true;
                }
            if ($tmpI > $maxFItems) {
                $attrsList[$atrybutTyp->id]['attrs']['more'] = $this->Html->tag('li', $this->Html->tag('span', '<i class="fa fa-plus-square"></i> ' . $this->Translation->get('towar_lista.wiecej'), ['class' => 'filter-more-btn', 'more-txt' => '<i class=\'fa fa-plus-square\'></i> ' . $this->Translation->get('towar_lista.wiecej'), 'less-txt' => '<i class=\'fa fa-minus-square\'></i> ' . $this->Translation->get('towar_lista.mniej')]));
            }
        }
    }
}
?>
<?php
if(!empty($okazje)){
    ?>
    <div class="row justify-content-center">
        <div class="col-12 col-md-11">
        <div>
            <div class="filterLabel"><?= $this->Translation->get('towar_lista.Okazja') ?></div>
            <ul class="filter-checxbox">
            <?php 
foreach ($okazje as $okazjaId => $okazjaName){
    ?>
                <li>
                    <?=$this->Form->control('okazje.'.$okazjaId,['name'=>'okazje['.$okazjaId.']', 'checked' => ((!empty($filters['okazje']) && in_array($okazjaId,$filters['okazje'])) ? true : false),'type'=>'checkbox','value'=>$okazjaId,'label'=>$okazjaName])?>
                </li>
                <?php
}
            ?>
            </ul>
        </div>
        </div>
    </div>

<?php
}
if (!empty($attrsList) && $hasAttrs) {
    ?>
    <div class="row justify-content-center">
    <div class="col-12">
        <div class="special-head"><?= $this->Translation->get('towar_lista.Filtracja') ?></div>
    </div>
        <div class="col-12 col-md-11">
    <?php
    foreach ($attrsList as $typeId => $attrItem) {
        if (empty($attrItem['attrs'])) {
            continue;
        }
        $attrUnique = uniqid();
        ?>
        <div>
            <div class="filterLabel"><?= $attrItem['nazwa'] ?></div>
            <?php
            $finder = '';
            if (key_exists('finder', $attrItem) && !empty($attrItem['finder'])) {
                $finder = 'finder-' . $typeId;
            }
            if (!empty($finder)) {
                ?>
                <div>
                    <input type="text" class="finder" data-target="#attr-<?= $attrUnique ?>" placeholder="<?= $this->Translation->get('towar_lista.' . $finder) ?>"/>
                </div>
            <?php } ?>
            <ul class="filter-checxbox <?= (!empty($finder) ? 'finder-content' : '') ?>" id="attr-<?= $attrUnique ?>">
                <?= join('', $attrItem['attrs']) ?>
            </ul>
        </div>
        <?php
    }
    ?>
        </div>
        
    </div>
    <?php
}
?>
<div class="row justify-content-center visible-only-xs" id="xsSubmitFilterRow">
    <div class="col-12">
        <button id="xsSubmitFilter" class="btn btn-outline-success btn-block"><?=$this->Translation->get('left.mobileZastosujFiltr')?></button>
    </div>
</div>
<?php
if (!empty($filters)) {
    unset($filters['kategoria']);
    if (!empty($filters)) {
        ?>
    <div class="row justify-content-center">
        <div class="col-12 col-md-11">
            <a href="<?= Cake\Routing\Router::url($baseRoute) ?>" class="btn btn-block btn-outline-danger btn-xs"><?= $this->Translation->get('towar_lista.clearFilter') ?></a>
        </div>
    </div>
        <?php
    }
}
if (!empty($kategoria->seo_tekst)) {
    ?>
    <div class="row mb-20">
        <div class="col-12 kat-seo-tekst">
            <p><?= $kategoria->seo_tekst ?></p>
        </div>
    </div>
    <?php
}

if (!empty($produktTygodnia)) {
    ?>
    <div class="row mb-20 justify-content-center hidden-xs">
        <div class="col-12">
            <div class="line-top"></div>
            <div class="special-head text-left"><?=$this->Translation->get('left.ProduktTygodnia')?></div>
        </div>
        <div class="col-12 col-md-11">
            <a href="<?= \Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'view', $produktTygodnia->id, $this->Navi->friendlyUrl($produktTygodnia->nazwa)]) ?>" class="left-nowosc">
                <div class="image">
                    <?= $this->Txt->towarZdjecie($produktTygodnia, 'thumb_2_') ?>
                </div>
                <div class="name"><?= $produktTygodnia->nazwa ?></div>
                <div class="cena"><?= $this->Txt->itemPrice($produktTygodnia, true) ?></div>
                <span class="mt-10 btn btn-danger btn-zobacz"><?= $this->Translation->get('labels.Zobacz') ?> <i class="fa fa-angle-right"></i></span>
            </a>
        </div>
    </div>
    <?php
}
if (!empty($nowosc)) {
    ?>
    <div class="row justify-content-center hidden-xs">
        <div class="col-12">
            <div class="line-top"></div>
            <div class="special-head text-left"><?=$this->Translation->get('left.Nowosc')?></div>
        </div>
        <div class="col-12 col-md-11">
            <a href="<?= \Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'view', $nowosc->id, $this->Navi->friendlyUrl($nowosc->nazwa)]) ?>" class="left-nowosc">
                <div class="image">
                    <?= $this->Txt->towarZdjecie($nowosc, 'thumb_2_') ?>
                </div>
                <div class="name"><?= $nowosc->nazwa ?></div>
                <div class="cena"><?= $this->Txt->itemPrice($nowosc, true) ?></div>
                <span class="mt-10 btn btn-danger btn-zobacz"><?= $this->Translation->get('labels.Zobacz') ?> <i class="fa fa-angle-right"></i></span>
            </a>
        </div>
    </div>
    <?php
}
if (!empty($ostatnie) && count($ostatnie) > 0) {
    $ostatni = $ostatnie[0];
    ?>
    <div class="row justify-content-center hidden-xs">
        <div class="col-12">
            <div class="line-top"></div>
            <div class="special-head text-left"><?=$this->Translation->get('left.OstatnioOgladane')?></div>
        </div>
        <div class="col-12 col-md-11">
            <div id="slider-ostatnie" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <?php
                    foreach ($ostatnie as $key => $ostatni) {
                        ?>
                        <div class="carousel-item <?= (($key == 0) ? 'active' : '') ?>">
                            <a href="<?= \Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'view', $ostatni->id, $this->Navi->friendlyUrl($ostatni->nazwa)]) ?>" class="left-nowosc">
                                <div class="image">
                                    <?= $this->Txt->towarZdjecie($ostatni, 'thumb_2_') ?>
                                </div>
                                <div class="name"><?= $ostatni->nazwa ?></div>
                                <div class="cena"><?= $this->Txt->itemPrice($ostatni, true) ?></div>
                                <span class="mt-10 btn btn-danger btn-zobacz"><?= $this->Translation->get('labels.Zobacz') ?> <i class="fa fa-angle-right"></i></span>
                            </a>
                        </div>
                    <?php } ?>
                    <?php if (count($ostatnie) > 1) { ?>
                        <!-- Controls -->
                        <a class="left carousel-control no-shadow" href="#slider-ostatnie" role="button" data-slide="prev">
                            <span class="carusel-control-img carusel-control-img-prev" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control no-shadow" href="#slider-ostatnie" role="button" data-slide="next">
                            <span class="carusel-control-img carusel-control-img-next" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>
<span class="btn btn-outline-success btn-sm pull-right" id="filterShowXs"><span class="f-hidden-visible"><i class="fas fa-filter"></i> <?= $this->Translation->get('left.FliterButtonMobile') ?></span><span class="f-open-visible"><i class="fa fa-times"></i></span></span>