<div class="home-promo-cat">
    <div class="name"><?= $promoKat['nazwa'] ?></div>
    <div class="image"><img src="<?= (!empty($promoKat['ikona']) ? $displayPath['kategoria_ikony'] . $promoKat['ikona'] : $displayPath['kategoria'] . $promoKat['zdjecie']) ?>" alt="<?= $promoKat['nazwa'] ?>"/></div>
    <div class="row align-items-end">
        <div class="col-12 col-md-8">
            <?php
            if (!empty($promoKat['sub'])) {
                ?>
                <div class="sub-promo">
                    <div class="suibtitle"><?= t('labels.najpopularniejsze') ?>:</div>
                    <?php foreach ($promoKat['sub'] as $subPromo) {
                        ?>
                        <div>
                            <a href="<?= r(['controller' => 'Towar', 'action' => 'index', $subPromo['id'], $this->Navi->friendlyURL($subPromo['nazwa'])]) ?>" class="sub-name"><?= $subPromo['nazwa'] . (!empty($subPromo['sub']) ? ':' : '') ?></a>
                            <?php if (!empty($subPromo['sub'])) { ?>
                                <ul>
                                    <?= $this->Navi->categoriesTree($subPromo['sub']) ?>
                                </ul>
                            <?php } ?>
                        </div>
                        <?php }
                    ?>
                </div>
                <?php
            }
            ?>
        </div>
        <div class="col-12 <?= (!empty($promoKat['sub']) ? 'col-md-4' : 'mt-10') ?>">
            <a class="see-more" href="<?= r(['controller' => 'Towar', 'action' => 'index', $promoKat['id'], $this->Navi->friendlyURL($promoKat['nazwa'])]) ?>"><?= t('labels.zobaczWszystkieProduktyWTejKategorii') ?></a>
        </div>
    </div>
</div>