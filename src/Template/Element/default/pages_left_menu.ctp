<button class="btn btn-block btn-outline-secondary" id="pagesLeftMenuOpen"><?=$this->Translation->get('labels.mobilePagesMenu')?> <i class="fas fa-bars"></i></button>
<ul class="pages-menu">
    <?php
    if ($menuPages->count() > 0) {
        foreach ($menuPages as $menuItem) {
            $active = '';
            if ($this->request->here == $menuItem->url) {
                $active = 'active';
            }
            ?>
            <li>
                <a class="<?= $active ?>" <?= (!empty($menuItem->child_menu) ? 'data-toggle="dropdown"' : '') ?> <?= ((!empty($menuItem->child_menu) && !empty($menuItem->url) && $menuItem->url != '#') ? 'onclick="document.location=\'' . $menuItem->url . '\';return true;"' : '') ?> href="<?= (!empty($menuItem->url) ? $menuItem->url : '#') ?>" target="<?= $menuItem->target ?>" title="<?= (!empty($menuItem->title) ? $menuItem->title : strip_tags($menuItem->nazwa)) ?>"><?= $menuItem->nazwa ?></a>
                <?php if (!empty($menuItem->child_menu)) {
                    ?>
                    <div class="dropdown-menu">
                        <?php
                        foreach ($menuItem->child_menu as $childMenu) {
                            ?>
                            <a class="dropdown-item" href="<?= $childMenu->url ?>" target="<?= $childMenu->target ?>" title="<?= (!empty($childMenu->title) ? $childMenu->title : $childMenu->nazwa) ?>"><i class="fa fa-angle-right"></i> <?= $childMenu->nazwa ?></a>
                            <?php
                        }
                        ?>
                    </div>
                <?php }
                ?>
            </li>
            <?php
        }
    }
    ?>
</ul>