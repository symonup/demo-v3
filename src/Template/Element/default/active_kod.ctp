<div class="kod-item">
    <div class="kod-value">
        <div class="value"><?= (($kod['typ'] == 'procent') ? $kod['rabat'] . '<span>%</span>' : $kod['rabat'] . '<span class="kwota">zł</span>') ?></div>
        <div class="val-name"><?= t('kodyRabatowe.kupon') ?></div>
        <span class="kod-border-item">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </span>
    </div>
    <div class="kod-info">
        <div class="name"><?= t('kodyRabatowe.kodRabatowy{wartosc}{nazwa}', ['wartosc' => $this->Html->tag('span', (($kod['typ'] == 'procent') ? $kod['rabat'] . '%' : $this->Txt->cena($kod['rabat'], null, true))), 'nazwa' => $kod['nazwa']]) ?></div>
        <div class="description"><?= $kod['opis'] ?></div>
        <div class="detalis row">
            <div class="col-12 col-md">
                <table>
                    <tr>
                        <td><?= t('kodyRabatowe.wielkoscZnizki') ?></td>
                        <td><?= (($kod['typ'] == 'procent') ? $kod['rabat'] . '%' : $this->Txt->cena($kod['rabat'], null, true)) ?></td>
                    </tr>
                    <tr>
                        <td><?= t('kodyRabatowe.minimalneZamowienie') ?></td>
                        <td><?= (!empty($kod['min_order']) ? $this->Txt->cena($kod['min_order'], null, true) : t('kodyRabatowe.brakMinimalnejKwoty')) ?></td>
                    </tr>
                    <tr>
                        <td><?= t('kodyRabatowe.promocjaDla') ?></td>
                        <td><?= t('kodyRabatowe.klientTyp_' . $kod['klient_typ']) ?></td>
                    </tr>
                    <tr>
                        <td><?= t('kodyRabatowe.promocjaWygasa') ?></td>
                        <td><?= $this->Txt->printDate($kod['data_end'], 'Y-m-d H:i') ?></td>
                    </tr>
                </table>
            </div>
            <div class="col-12 col-md-auto">
                <a href="<?= r(['controller' => 'Home', 'action' => 'unUseCode', $kod['kod']]) ?>" class="btn btn-outline-dark btn-sm"><?= t('koszyk.usunKod') ?></a>
            </div>
        </div>
        <div class="kod-time">
            <i class="flat flaticon-sports-and-competition"></i> <?= t('kodyRabatowe.kodRabatowyWygasa{data}', ['data' => $this->Txt->dateInfo($kod['data_end'], t('kodyRabatowe.nieaktywny'))]) ?>
        </div>
    </div>
</div>