<div class="row justify-content-center align-items-center blog-item">
    <div class="col-12 col-md-6 description">
        <div class="tytul">
            <?=$artykul->tytul?>
        </div>
        <div class="opis">
            <?= nl2br($artykul->opis)?>
        </div>
        <div class="link">
            
            <div class="row justify-content-between">
                <?php 
                $urlArr=['action'=>'artykul',$artykul->id,$this->Txt->friendlyUrl($artykul->tytul)];
            $fbId = c('facebook.appId');
        if(!empty($fbId)){
            ?>
        <div class="col-auto blog-comments"><i class="flat flaticon-chat"></i> <fb:comments-count href="<?= Cake\Routing\Router::url($urlArr, true) ?>"></fb:comments-count></div>
        <?php
        }
        ?>
        <div class="col text-right">
            <a href="<?= \Cake\Routing\Router::url($urlArr)?>" title="<?=$artykul->tytul?>" class="btn btn-success"><?=$this->Translation->get('blog.CzytajWiecej')?></a>
        </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6 image">
        <?php 
        if(!empty($artykul->zdjecie) && file_exists($filePath['blog'].$artykul->zdjecie)){
            ?>
        <img src="<?=$displayPath['blog'].$artykul->zdjecie?>" alt="<?=$artykul->tytul?>"/>
        <?php
        }
        ?>
    </div>
</div>