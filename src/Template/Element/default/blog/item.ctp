<?php
if(!empty($item)){
    $urlArr=['controller'=>'Blog','action'=>'artykul',$item->id,$this->Txt->friendlyUrl($item->tytul)];
    ?>
<a class="blog-item-min" href="<?= Cake\Routing\Router::url($urlArr)?>">
    <div class="row align-items-end">
        <div class="col blog-data"><?=t('blog.dataDodania{0}',[$this->Txt->printDate($item->data,'d/m.Y H:i')])?></div>
        <?php 
            $fbId = c('facebook.appId');
        if(!empty($fbId)){
            ?>
        <div class="col-auto blog-comments"><i class="flat flaticon-chat"></i> <fb:comments-count href="<?= Cake\Routing\Router::url($urlArr, true) ?>"></fb:comments-count></div>
        <?php
        }
        ?>
    </div>
    <div class="blog-img">
        <img src="<?=$displayPath['blog'].$item->zdjecie?>"/>
    </div>
    <div class="blog-title">
        <?=$item->tytul?>
    </div>
</a>
<?php
}