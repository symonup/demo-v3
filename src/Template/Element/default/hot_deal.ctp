<?php if (!empty($hotDeal)) { ?>
   <a class="produkt-dnia" href="<?= Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'view', $hotDeal->towar->id, $this->Navi->friendlyUrl($hotDeal->towar->nazwa)]) ?>">
        <div class="hot-deal">
            <div class="name"><?= (!empty($hotDeal->nazwa) ? $hotDeal->nazwa : $hotDeal->towar->nazwa) ?></div>
            <div class="info">
                <div class="image">
                    <?= $this->Txt->towarZdjecie($hotDeal->towar, 'thumb_2_') ?>
                </div>
                <div class="price"> 
                    <?php
                    $priceRegular = $this->Txt->itemPriceDef($hotDeal->towar, false);
                    $priceSpecial = (!empty($hotDeal->cena) ? $hotDeal->cena : $this->Txt->itemPrice($hotDeal->towar, false));
                    $discount = round(100 - (($priceSpecial * 100) / $priceRegular));
                    ?>
                    <div class="promotion-price"><?= $this->Txt->cena($priceSpecial, null, true) ?></div>
                    <div class="promotion-rabat"><?= '-'.$discount . '%' ?></div><div class="regular-price"><span><?= $this->Txt->cena($priceRegular, null, true) ?></span></div>
                </div>
            </div>
        </div>
    </a>
<?php }
?>