<?= $this->Form->create(null, ['type' => 'post', 'url' => ['controller'=>'Uzytkownik','action'=>'kartaKod'], 'id' => 'form-karta-kod', 'class' => 'form-inline']) ?>
<?= $this->Form->control('kod', ['type' => 'text', 'required' => true, 'autocomplete' => 'off', 'placeholder' => $this->Translation->get('kartaPodarunkowa.podajKod'), 'short' => 'kod', 'label' => false]) ?>
<?= $this->Form->control('pin', ['type' => 'text', 'required' => true, 'autocomplete' => 'off', 'placeholder' => $this->Translation->get('kartaPodarunkowa.podajPin'), 'short' => 'pin', 'label' => false]) ?>
<button type="submit" class="btn btn-success"><?= $this->Translation->get('kartaPodarunkowa.uzyjKoduBtn') ?></button>
<?=
$this->Form->end()?>