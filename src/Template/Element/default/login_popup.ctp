<div class="modal fade" id="login-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    <ul class="nav nav-pills" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-login-tab" data-toggle="pill" href="#pills-login" role="tab" aria-controls="pills-login" aria-selected="true"><?=$this->Translation->get('loginPage.popUpLogowanie')?></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-register-tab" data-toggle="pill" href="#pills-register" role="tab" aria-controls="pills-register" aria-selected="false"><?=$this->Translation->get('loginPage.popUpRejestracja')?></a>
                        </li>
                    </ul>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row mt-10 mb-20 justify-content-center">
                    <div class="col-8"><img src="/img/logo.png"/></div>
                </div>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-login" role="tabpanel" aria-labelledby="pills-login-tab">
                        <?= $this->Form->create(null, ['class' => 'login-form','url'=>['controller'=>'Home','action'=>'login']]) ?>
                        <div class="row">
                            <div class="col-md-12 nice-checkbox type-2">
                                <?= $this->Form->control('email', ['type' => 'email', 'id' => 'login-email', 'label' => $this->Translation->get('loginPage.emailInputLabel')]) ?>
                                <?= $this->Form->control('password', ['type' => 'password', 'id' => 'login-password', 'required' => true, 'label' => $this->Translation->get('loginPage.passwordInputLabel')]) ?>
                                <?php // $this->Form->control('remember_me', ['type' => 'checkbox', 'required' => false, 'label' => ['text' => $this->Translation->get('loginPage.rememberMeLabel'), 'escape' => false]]) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mt-10 mb-20 text-center">
                                <button type="submit" class="btn btn-sm btn-block btn-color-2 btn-login" name="login"><?= $this->Translation->get('loginPage.signInButton') ?> <i class="flat flaticon-login-square-arrow-button-outline"></i></button>
                            </div>
                            <div class="col-md-12 text-center">
                                <a class="forget-link" href="<?= Cake\Routing\Router::url(['controller' => 'Home', 'action' => 'resetPassword']) ?>"><?= $this->Translation->get('loginPage.resetPasswordLink') ?></a>
                            </div>
                        </div>
                        <?= $this->Form->end() ?>
                    </div>
                    <div class="tab-pane fade" id="pills-register" role="tabpanel" aria-labelledby="pills-register-tab">
                        <?= $this->Form->create($uzytkownik, ['class' => 'register-form open','url'=>['controller'=>'Home','action'=>'login']]) ?>
                        <div class="row">
                            <div class="col-md-12">
                                <?= $this->Form->control('imie', ['type' => 'text', 'label' => $this->Translation->get('loginPage.nameInputLabel')]) ?>
                            </div>
                            <div class="col-md-12">
                                <?= $this->Form->control('nazwisko', ['type' => 'text', 'label' => $this->Translation->get('loginPage.lastNameInputLabel')]) ?>
                            </div>
                            <div class="col-md-12">
                                <?= $this->Form->control('email', ['type' => 'email', 'label' => $this->Translation->get('loginPage.emailInputLabel')]) ?>
                            </div>
                            <div class="col-md-12">
                                <?= $this->Form->control('password', ['type' => 'password', 'id' => 'register-password', 'required' => true, 'label' => $this->Translation->get('loginPage.passwordInputLabel')]) ?>
                            </div>
                            <div class="col-md-12">
                                <?= $this->Form->control('repeat_password', ['type' => 'password', 'required' => true, 'label' => $this->Translation->get('loginPage.repeatPasswordInputLabel')]) ?>
                            </div>
                            <div class="col-md-12 nice-checkbox type-2">
                                <?php $zgodaTekst = $this->Translation->get('loginPage.consentInputLabel');
                                if (!empty($zgodaTekst)) {
                                    echo $this->Form->control('zgoda', ['type' => 'checkbox', 'required' => true, 'label' => ['text' => $this->Translation->get('loginPage.consentInputLabel'), 'escape' => false]]);
                                } ?>
<?= $this->Form->control('regulamin', ['type' => 'checkbox', 'required' => true, 'label' => ['text' => $this->Translation->get('loginPage.regulationsInputLabel'), 'escape' => false]]) ?>
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-sm btn-color-2 btn-login btn-block mt-10" name="register"><?= $this->Translation->get('loginPage.joinButton') ?> <i class="flat flaticon-login-square-arrow-button-outline"></i></button>
                        </div>
<?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>