<nav class="navbar navbar-expand-lg navbar-light bg-light justify-content-between align-items-top">
    <div class="col-auto">
        <a class="navbar-brand" href="<?= Cake\Routing\Router::url(['controller' => 'Home', 'action' => 'index']) ?>"><img src="<?= $displayPath['webroot'] ?>img/logo.png"/></a>
    </div>
    <div class="col text-right">
        <ul class="top-menu">
            <li>
                <div class="goToCart">
                    <a href="<?= \Cake\Routing\Router::url(['controller' => 'Cart', 'action' => 'index']) ?>">
                        <span>
                            <span class="cartLinkLabel"></span> <?= t('header.twojKoszyk') ?>
                            <span class="cartInfo">
                                <?php
                                if (!empty($cartItems) && !empty($cartItems['cart-info'])) {
                                    echo $cartItems['cart-info'];
                                } else {
                                    echo t('header.Ilosc{0}Kwota{1}', ['<span id="cartItemsCount">0</span>', '<span id="cartItemsValue">' . $this->Txt->cena(0) . '</span>']);
                                }
                                ?>
                            </span>
                        </span>
                    </a>
                </div>
            </li>
        </ul>
        <ul class="top-menu">
            <li>
                <a href="<?= Cake\Routing\Router::url(['controller' => 'Uzytkownik', 'action' => 'index']) ?>">
                    <i class="flat flaticon-cart-1"></i>
                </a>
            </li>
            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'index', 'ulubione']) ?>" class="btn-favorite"><i class="flat flaticon-heart"></i></a></li>
            <li>    
                <?php
                if (!empty($user)) {
                    ?>
                    <a href="<?= \Cake\Routing\Router::url(['controller' => 'Uzytkownik', 'action' => 'index']) ?>"><i class="flat flaticon-user"></i></a>
                    <a class="logout" href="<?= \Cake\Routing\Router::url(['controller' => 'Home', 'action' => 'logout']) ?>" data-toggle="tooltip" title="<?= $this->Translation->get('header.Wyloguj') ?>"><i class="fas fa-sign-out-alt"></i></a>
                    <?php
                } else {
                    ?>
                    <a href="<?= \Cake\Routing\Router::url(['controller' => 'Home', 'action' => 'login']) ?>"><i class="flat flaticon-user"></i></a>
                    <?php
                }
                ?>
            </li>
        </ul>
    </div>
</nav>
<div class="container-fluid nav-top mt-10">
    <div class="row justify-content-center align-items-center">
        <div class="col-auto">
            <button class="navbar-toggler mobile-menu-button" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars"></i>
            </button>
        </div>
        <div class="col text-center">
            <a class="fs-20 fw-bold" href="tel:<?= Cake\Core\Configure::read('dane.telefon') ?>"><?= Cake\Core\Configure::read('dane.telefon') ?></a>
        </div>
        <div class="col-auto text-right">
            <a class="gold-link" href="<?=r(['controller'=>'Blog','action'=>'index'])?>"><?=t('header.Blog')?> <i class="fa fa-angle-right"></i></a>
        </div>
    </div>
    <nav class="navbar">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-0 ml-auto">
                <?php
                if ($menu->count() > 0) {
                    ?>
                    <?php
                    foreach ($menu as $menuItem) {
                        if (!empty($menuItem->child_menu)) {
                            ?>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenu" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <?= $menuItem->nazwa ?>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenu">
                                    <?php
                                    foreach ($menuItem->child_menu as $childMenu) {
                                        ?>
                                        <a class="dropdown-item" href="<?= $childMenu->url ?>" target="<?= $childMenu->target ?>" title="<?= (!empty($childMenu->title) ? $childMenu->title : $childMenu->nazwa) ?>"><?= $childMenu->nazwa ?></a>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </li>
                            <?php
                        } else {
                            ?>
                            <li class="nav-item"><a class="nav-link" target="<?= $menuItem->target ?>" href="<?= $menuItem->url ?>" title="<?= (!empty($menuItem->title) ? $menuItem->title : $menuItem->nazwa) ?>"><?= $menuItem->nazwa ?></a></li>
                        <?php } ?> 

                        <?php
                    }
                    ?>
                    <?php
                } else {
                    echo $this->Navi->getAllCatByUlMobile($allCats, 1);
                }
                ?>
            </ul>
        </div>
    </nav>
    <div class="row align-items-center">
        <div class="col-12">
            <?= $this->element('default/search_form') ?>
        </div>
    </div>
</div>