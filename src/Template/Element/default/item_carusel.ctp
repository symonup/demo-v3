<?php
if (!empty($towar)) {
    if(empty($gratis)){
        $gratis=false;
    }
    $oldPrice = null;
    $itemPriceDef = $this->Txt->itemPriceDef($towar);
    $itemPrice = $this->Txt->itemPrice($towar);
    if (empty($count)) {
        $count = 3;
    }
    if (empty($premiera)) {
        $premiera = false;
    }
    if ($itemPriceDef > $itemPrice) {
        $oldPrice = $itemPriceDef;
    }
    if (!empty($hideBadge)) {
        $badge = '';
    } else {
        $badge = '';
        $badgeType = '';
        if (!empty($forceBadge)) {
            $badgeType = $forceBadge;
        } else {
            if (!empty($towar->promocja) && (empty($hideBadgeTyp) || !(array_search('promocja', $hideBadgeTyp) !== false))) {
                $badgeType = 'promocja';
            } elseif (!empty($towar->wyprzedaz) && (empty($hideBadgeTyp) || !(array_search('wyprzedaz', $hideBadgeTyp) !== false))) {
                $badgeType = 'wyprzedaz';
            } elseif (!empty($towar->nowosc) && (empty($hideBadgeTyp) || !(array_search('nowosc', $hideBadgeTyp) !== false))) {
                $badgeType = 'nowosc';
            } elseif (!empty($towar->polecany) && (empty($hideBadgeTyp) || !(array_search('polecany', $hideBadgeTyp) !== false))) {
                $badgeType = 'polecany';
            } elseif (!empty($towar->wyrozniony) && (empty($hideBadgeTyp) || !(array_search('wyrozniony', $hideBadgeTyp) !== false))) {
                $badgeType = 'wyrozniony';
            } elseif (!empty($towar->produkt_tygodnia) && (empty($hideBadgeTyp) || !(array_search('produkt_tygodnia', $hideBadgeTyp) !== false))) {
                $badgeType = 'produkt_tygodnia';
            }
        }
        if (!empty($hideBadgeTyp) && array_search($badgeType, $hideBadgeTyp) !== false) {
            $badgeType = '';
        }
        if (!empty($badgeType)) {
            switch ($badgeType) {
                case 'promocja' : {
                        $badge = $this->Html->tag('span', $this->Translation->get('labels.Promocja'), ['class' => 'badge-label difFont promocja']);
                    } break;
                case 'wyprzedaz' : {
                        $badge = $this->Html->tag('span', $this->Translation->get('labels.Wyprzedaz'), ['class' => 'badge-label difFont wyprzedaz']);
                    } break;
                case 'nowosc' : {
                        $badge = $this->Html->tag('span', $this->Translation->get('labels.Nowosc'), ['class' => 'badge-label difFont nowosc']);
                    } break;
                case 'polecany' : {
                        $badge = $this->Html->tag('span', $this->Translation->get('labels.Polecany'), ['class' => 'badge-label difFont polecany']);
                    } break;
                case 'wyrozniony' : {
                        $badge = $this->Html->tag('span', $this->Translation->get('labels.Wyrozniony'), ['class' => 'badge-label difFont wyrozniony']);
                    } break;
                case 'produkt_tygodnia' : {
                        $badge = $this->Html->tag('span', $this->Translation->get('labels.produktTygodnia'), ['class' => 'badge-label difFont produkt_tygodnia']);
                    } break;
                default : $badge = '';
                    break;
            }
        }
        $showHotBadge = Cake\Core\Configure::read('hot_deal.showBadge');
        if (!empty($towar->hot_deal) && !empty($showHotBadge)) {
            $badge = $this->Html->tag('div', $this->Translation->get('labels.hotDealBadge{0}', [$this->element('default/time_counter', ['hLabel' => $this->Translation->get('timeCounter.godz'), 'mLabel' => $this->Translation->get('timeCounter.min'), 'sLabel' => $this->Translation->get('timeCounter.sek'), 'endTime' => (!empty($towar->hot_deal->data_do) ? $towar->hot_deal->data_do->format('Y-m-d H:i:00') : date('Y-m-d'))])]), ['class' => 'badge-label difFont hot_deal_badge']);
        }
    }
    ?>
    <div class="product item-min item-min-2">
            <div class="product-image">
                <?= $this->Txt->towarZdjecie($towar, 'thumb_2_') ?>
                <div class="hover-actions">
                    <a href="<?= $this->Txt->towarViewUrl($towar) ?>">
                        <i class="flat flaticon-info"></i>
                        <span><?= t('towar_lista.zobaczWiecej') ?></span>
                    </a>
                    <span add-target="<?= Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'addToFavorite', $towar->id]) ?>" remove-target="<?= Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'removeFavorite', $towar->id]) ?>" item-id="<?= $towar->id ?>" class="favorite <?= ((!empty($favorite) && key_exists($towar->id, $favorite)) ? 'active' : '') ?>">
                        <i class="flat flaticon-heart"></i>
                        <span class="text-active"><?= $this->Translation->get('labels.UsunZeSchowka') ?></span><span class="text-unactive"><?= $this->Translation->get('labels.DodajDoSchowka') ?></span>
                    </span>
                        <?php
                        $addAction = ['controller' => 'Cart', 'action' => 'add', $towar->id];
                        if (!empty($addAsTr)) {
                            $addAction[] = 1;
                        }
                        ?>
                        <button type="button" <?=($gratis?'as-gratis="true"':'')?> item-id="<?= $towar->id ?>" add-action="<?= \Cake\Routing\Router::url($addAction) ?>" version-id="<?= (!empty($firstWersion) ? $firstWersion->id : '') ?>" class="btn btn-danger add-to-cart">
                            <i class="flat flaticon-cart"></i>
                            <span><?= $this->Translation->get('towar_lista.addToCartShort') ?> </span>
                        </button>
                        <?php
                        ?>
                </div>
            </div>
            <a href="<?= $this->Txt->towarViewUrl($towar) ?>" class="product-name">
                <span><?= $towar->nazwa ?></span>
            </a>
            <div class="product-footer">
                <div class="price">
                    <?php if($gratis){
                    echo t('labels.od{0}',[$this->Txt->cena($towar->gratis_wartosc)]);    
                    }else{
                    echo $this->Txt->cena($itemPrice); 
                    
                    } ?>
                </div>
            </div>
    </div>
<?php } ?>
