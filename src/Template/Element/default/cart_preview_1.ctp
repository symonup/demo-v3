<?php if(!empty($items)){
  ?>
<div id="cartPreview">
    <div class="previewItemsContainer">
        <div class="previewItemsTable">
    <?php 
    $cartZestawy=[];
    foreach ($items as $itemKey => $item){
        if(!empty($item['zestaw_bonus'])){
            continue;
        }
        if(!empty($item['zestaw_key'])){
            $cartZestawy[$item['zestaw_key']][$itemKey]=$item;
            continue;
        }
        $wersjaImgSrc = $displayPath['img'] . 'noPhoto.png';
        $wersjaAlt = $item['nazwa'];
        if (!empty($item['zdjecie'])) {
            if (file_exists($filePath['towar'] . $this->Txt->getKatalog($item['zdjecie']['id']) . DS . 'thumb_' . $item['zdjecie']['plik'])) {
                $wersjaImgSrc = \Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'image', $item['zdjecie']['id'], 'thumb_' . $item['zdjecie']['plik']]);
            }
        }
        ?>
    <div class="item-preview">
        <div class="image"><img src="<?= $wersjaImgSrc ?>" alt="<?= $wersjaAlt ?>"/></div>
        <div class="name"><?=$item['nazwa']?></div>
        <div class="ilosc">
            <?php
                if (!empty($item['zestaw_id'])) {
                    if ($zi == 1) {
                        ?>
                        <input type="text" value="<?= $item['ilosc'] ?>" class="cart-ordered-box item-ilosc-input" zestaw-key="<?= $zestawKey ?>" item-id="<?= $item['towar_id'] ?>" version-id="" item-price="<?= $item['cena_za_sztuke'] ?>" in-box="1"/>

                        <?php
                    }
                } else {
                    ?>
                    <input type="text" value="<?= $item['ilosc'] ?>" class="cart-ordered-box item-ilosc-input" item-key="<?= $itemKey ?>" item-id="<?= $item['towar_id'] ?>" version-id="<?= (!empty($item['wariant_id']) ? $item['wariant_id'] : '') ?>" item-price="<?= $item['cena_za_sztuke'] ?>" in-box="1"/>
                    <?php
                }
                ?>
        </div>
        <div class="cena"><?=$this->Txt->cena($item['cena_razem_'.\Cake\Core\Configure::read('ceny.rodzaj')])?></div>
    </div>
    <?php
    } 
    ?>
        </div>
      <?php  if(!empty($cartZestawy)){
        ?>
            <div class="preview-zestaw-container">
            <?php
        foreach ($cartZestawy as $zestawKey => $zestawItems){
            ?>
            <div class="preview-zestaw">
            <?php
            $zestawIlosc=1;
            $zestawWartosc=0;
            $zi=1;
            foreach ($zestawItems as $itemKey => $item){
                $zestawIlosc=$item['ilosc'];
                $zestawWartosc+=$item['cena_razem_'.\Cake\Core\Configure::read('ceny.rodzaj')];
                $wersjaImgSrc = $displayPath['img'] . 'noPhoto.png';
        $wersjaAlt = $item['nazwa'];
        if (!empty($item['zdjecie'])) {
            if (file_exists($filePath['towar'] . $this->Txt->getKatalog($item['zdjecie']['id']) . DS . 'thumb_' . $item['zdjecie']['plik'])) {
                $wersjaImgSrc = \Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'image', $item['zdjecie']['id'], 'thumb_' . $item['zdjecie']['plik']]);
            }
        }
                ?>
                <div class="preview-z-item <?=(($zi== count($zestawItems))?'last':'')?>" title="<?=$item['nazwa']?>" data-toggle="tooltip">
                    <div class="image"><img src="<?= $wersjaImgSrc ?>" alt="<?= $wersjaAlt ?>"/></div>
                </div>
                <?php
                $zi++;
            }
            ?>
                <div class="ilosc"><?=$zestawIlosc?> <?=$this->Translation->get('labels.szt')?></div>
                <div class="cena"><?=$this->Txt->cena($zestawWartosc)?></div>
            </div>
                <?php
        }
        ?>
            </div>
                <?php
    }
    ?>
    </div>
    <div class="text-right mt-10 pd-r-10">
        <span class="btn btn-color-2 btn-sm"><?=$this->Translation->get('koszyk.przejdzDoKoszyka')?></span>
    </div>
</div>
<?php
} ?>