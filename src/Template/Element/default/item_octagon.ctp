<?php
if (!empty($towar)) {
    $oldPrice = null;
    $itemPriceDef = $this->Txt->itemPriceDef($towar);
    $itemPrice = $this->Txt->itemPrice($towar);
    if (empty($count)) {
        $count = 3;
    }
    if (empty($premiera)) {
        $premiera = false;
    }
    if ($itemPriceDef > $itemPrice) {
        $oldPrice = $itemPriceDef;
    }
    if (!empty($hideBadge)) {
        $badge = '';
    } else {
        $badge = '';
        $badgeType = '';
        if (!empty($forceBadge)) {
            $badgeType = $forceBadge;
        } else {
            if (!empty($towar->promocja) && (empty($hideBadgeTyp) || !(array_search('promocja', $hideBadgeTyp) !== false))) {
                $badgeType = 'promocja';
            } elseif (!empty($towar->wyprzedaz) && (empty($hideBadgeTyp) || !(array_search('wyprzedaz', $hideBadgeTyp) !== false))) {
                $badgeType = 'wyprzedaz';
            } elseif (!empty($towar->nowosc) && (empty($hideBadgeTyp) || !(array_search('nowosc', $hideBadgeTyp) !== false))) {
                $badgeType = 'nowosc';
            } elseif (!empty($towar->polecany) && (empty($hideBadgeTyp) || !(array_search('polecany', $hideBadgeTyp) !== false))) {
                $badgeType = 'polecany';
            } elseif (!empty($towar->wyrozniony) && (empty($hideBadgeTyp) || !(array_search('wyrozniony', $hideBadgeTyp) !== false))) {
                $badgeType = 'wyrozniony';
            } elseif (!empty($towar->produkt_tygodnia) && (empty($hideBadgeTyp) || !(array_search('produkt_tygodnia', $hideBadgeTyp) !== false))) {
                $badgeType = 'produkt_tygodnia';
            }
        }
        if (!empty($hideBadgeTyp) && array_search($badgeType, $hideBadgeTyp) !== false) {
            $badgeType = '';
        }
        if (!empty($badgeType)) {
            switch ($badgeType) {
                case 'promocja' : {
                        $badge = $this->Html->tag('span', $this->Translation->get('labels.Promocja'), ['class' => 'badge-label difFont promocja']);
                    } break;
                case 'wyprzedaz' : {
                        $badge = $this->Html->tag('span', $this->Translation->get('labels.Wyprzedaz'), ['class' => 'badge-label difFont wyprzedaz']);
                    } break;
                case 'nowosc' : {
                        $badge = $this->Html->tag('span', $this->Translation->get('labels.Nowosc'), ['class' => 'badge-label difFont nowosc']);
                    } break;
                case 'polecany' : {
                        $badge = $this->Html->tag('span', $this->Translation->get('labels.Polecany'), ['class' => 'badge-label difFont polecany']);
                    } break;
                case 'wyrozniony' : {
                        $badge = $this->Html->tag('span', $this->Translation->get('labels.Wyrozniony'), ['class' => 'badge-label difFont wyrozniony']);
                    } break;
                case 'produkt_tygodnia' : {
                        $badge = $this->Html->tag('span', $this->Translation->get('labels.produktTygodnia'), ['class' => 'badge-label difFont produkt_tygodnia']);
                    } break;
                default : $badge = '';
                    break;
            }
        }
        $showHotBadge = Cake\Core\Configure::read('hot_deal.showBadge');
        if (!empty($towar->hot_deal) && !empty($showHotBadge)) {
            $badge = $this->Html->tag('div', $this->Translation->get('labels.hotDealBadge{0}', [$this->element('default/time_counter', ['hLabel' => $this->Translation->get('timeCounter.godz'), 'mLabel' => $this->Translation->get('timeCounter.min'), 'sLabel' => $this->Translation->get('timeCounter.sek'), 'endTime' => (!empty($towar->hot_deal->data_do) ? $towar->hot_deal->data_do->format('Y-m-d H:i:00') : date('Y-m-d'))])]), ['class' => 'badge-label difFont hot_deal_badge']);
        }
        if (!empty($premiera) && !empty($towar->data_premiery)) {
            if (strtotime('+ 4 days') > strtotime($towar->data_premiery->format('Y-m-d 00:00:00')) && time() < strtotime($towar->data_premiery->format('Y-m-d 00:00:00'))) {
                $badge = $this->Html->tag('div', $this->Translation->get('labels.premieraZa{0}', [$this->element('default/time_counter', ['hLabel' => $this->Translation->get('timeCounter.godz'), 'mLabel' => $this->Translation->get('timeCounter.min'), 'sLabel' => $this->Translation->get('timeCounter.sek'), 'endTime' => $towar->data_premiery->format('Y-m-d 00:00:00')])]), ['class' => 'badge-label difFont hot_deal_badge']);
            }
        }
    }
    ?>
    <div class="product <?= (!empty($premiera) ? 'product-premiera' : '') ?>">
        <?php
        if ($premiera && $mobile) {
            ?>
            <div class="premiera-info">
                <div>
                    <?= $this->Translation->get('towar_lista.dataPremiery{0}', $this->Html->tag('span', (!empty($towar->data_premiery) ? $towar->data_premiery->format('Y-m-d') : $this->Translation->get('labels.Nieznana')))) ?>
                </div>
            </div>
            <?php
        }
        ?>
        <div class="octagonWrap small">
            <div class="octagon3">
                <div class="content"> 
                    <div class="product-name">
                        <span><?= $towar->nazwa ?></span>
                    </div>
                    <div class="product-content">
                        <a href="<?= $this->Txt->towarViewUrl($towar) ?>" class="product-image">
                            <?= $this->Txt->towarZdjecie($towar, 'thumb_') ?>
                            <div class="base-params">
                                <div class="param-1"><span add-target="<?= Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'addToFavorite', $towar->id]) ?>" remove-target="<?= Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'removeFavorite', $towar->id]) ?>" item-id="<?= $towar->id ?>" class="favorite <?= ((!empty($favorite) && key_exists($towar->id, $favorite)) ? 'active' : '') ?>"><i class="far fa-heart" data-toggle="tooltip" title="<?= $this->Translation->get('labels.DodajDoSchowka') ?>"></i><i class="on-active fa fa-heart" data-toggle="tooltip" title="<?= $this->Translation->get('labels.UsunZeSchowka') ?>"></i></span></div>
                                <div class="param-2"><?= ((!empty($towar->platforma) && !empty($towar->platforma->logo)) ? $this->Html->image($displayPath['platforma'] . $towar->platforma->logo, ['alt' => $towar->platforma->nazwa]) : '') ?></div>
                            </div>
                        </a>
                        <div class="product-actions">
                            <div class="cart-form add-to-cart-container">
                                <?php
                                $addAction = ['controller' => 'Cart', 'action' => 'add', $towar->id];
                                if (!empty($addAsTr)) {
                                    $addAction[] = 1;
                                }
                                if (!empty($towar->data_premiery) && strtotime($towar->data_premiery->format('Y-m-d')) > strtotime(date('Y-m-d'))) {
                                    if (!empty($towar->preorder) && $towar->preorder_limit > 0 && (
                                            (empty($towar->preorder_od) && empty($towar->preorder_do)) || (empty($towar->preorder_do) && !empty($towar->preorder_od) && strtotime(date('Y-m-d H:i')) >= strtotime($towar->preorder_od->format('Y-m-d H:i'))) || (empty($towar->preorder_od) && !empty($towar->preorder_do) && strtotime(date('Y-m-d H:i')) <= strtotime($towar->preorder_do->format('Y-m-d H:i'))) || (!empty($towar->preorder_do) && !empty($towar->preorder_od) && strtotime(date('Y-m-d H:i')) >= strtotime($towar->preorder_od->format('Y-m-d H:i')) && strtotime(date('Y-m-d H:i')) <= strtotime($towar->preorder_do->format('Y-m-d H:i')))
                                            )
                                    ) {
                                        if ($towar->ilosc <= 0) {
                                            ?>
                                            <span class="itemOctagonBrak"><i class="fa fa-ban"></i><span><?= $this->Translation->get('towar_lista.chwilowyBrakTowaru') ?></span></span>
                                            <?php
                                        } else {
                                            ?>
                                            <button type="button" item-id="<?= $towar->id ?>" add-action="<?= \Cake\Routing\Router::url($addAction) ?>" version-id="<?= (!empty($firstWersion) ? $firstWersion->id : '') ?>" class="btn btn-danger add-to-cart"><?= $this->Translation->get('towar_lista.addToCart') ?></button>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <span class="order-not-allow" data-toggle="tooltip" title="<?= $this->Translation->get('towar_lista.produktNiedostepnyWSprzedazyInfo') ?>"></span>
                                        <?php
                                    }
                                } else {
                                    if ($towar->ilosc <= 0) {
                                        ?>
                                        <span class="itemOctagonBrak"><i class="fa fa-ban"></i><span><?= $this->Translation->get('towar_lista.chwilowyBrakTowaru') ?></span></span>
                                    <?php } else {
                                        ?>
                                        <button type="button" item-id="<?= $towar->id ?>" add-action="<?= \Cake\Routing\Router::url($addAction) ?>" version-id="<?= (!empty($firstWersion) ? $firstWersion->id : '') ?>" class="btn btn-danger add-to-cart"><?= $this->Translation->get('towar_lista.addToCart') ?></button>
                                        <?php
                                    }
                                }
                                ?>

                            </div>
                            <a href="<?= $this->Txt->towarViewUrl($towar) ?>" class="list-item">
                                <i class="fa fa-info"></i>
                                <div><?= $this->Translation->get('towar_lista.sprawdzSzczegoly') ?></div>
                            </a>
                            <?php if (!empty($towar->bonus)) {
                                ?>
                                <div class="bonus-zlotowki">
                                    <div class="text"><?= $this->Translation->get('towar_lista.bonusoweZlotowki') ?></div>
                                    <div class="value"><?= $this->Txt->cena($towar->bonus->wartosc, null, true) ?></div>
                                </div>
                            <?php }
                            ?>
                        </div>
                    </div>
                    <div class="product-footer">
                        <div class="price<?= (!empty($oldPrice) ? ' promotion' : '') ?>">
                            <span class="<?= !empty($oldPrice) ? 'promo' : '' ?>"><?= $this->Txt->cena($itemPrice) ?></span>
                            <?php
                            if (!empty($oldPrice)) {
                                ?>
                                <span class="old-price"><?= $this->Txt->cena($oldPrice) ?></span>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if (!empty($premiera) && !$mobile) {
            ?>
            <div class="premiera-info">
                <div>
                    <?= $this->Translation->get('towar_lista.dataPremiery{0}', $this->Html->tag('span', (!empty($towar->data_premiery) ? $towar->data_premiery->format('Y-m-d') : $this->Translation->get('labels.Nieznana')))) ?>
                </div>
                <?php
                if (!empty($towar->data_premiery) && strtotime($towar->data_premiery->format('Y-m_d H:i:s')) > time() && $towar->ilosc > 0) {
                    
                } else {
                    ?>
                    <a href="<?= Cake\Routing\Router::url(['controller' => 'Home', 'action' => 'powiadomODostepnosci', $towar->id]) ?>" modal-target="#modal-powiadom-o-dostepnosci" class="getPupUpLink"><?= $this->Translation->get('towar_lista.powiadomMnieODostepnosci') ?> <i class="fa fa-angle-right"></i></a>
                    <?php
                }
                ?>
            </div>
            <?php
        } elseif ($premiera && $mobile) {
            ?>
            <?php
            if (!empty($towar->data_premiery) && strtotime($towar->data_premiery->format('Y-m_d H:i:s')) > time() && $towar->ilosc > 0) {
                
            } else {
                ?>
                <div class="premiera-info">
                    <a href="<?= Cake\Routing\Router::url(['controller' => 'Home', 'action' => 'powiadomODostepnosci', $towar->id]) ?>" modal-target="#modal-powiadom-o-dostepnosci" class="getPupUpLink"><?= $this->Translation->get('towar_lista.powiadomMnieODostepnosci') ?> <i class="fa fa-angle-right"></i></a>
                </div>
                <?php
            }
            ?>
            <?php
        }
        ?>
        <?= $badge ?>
    </div>
<?php } ?>
