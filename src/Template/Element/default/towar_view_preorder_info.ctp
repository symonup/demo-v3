<div class="col-12 col-lg-5 text-right lg-mb--22">
    <div class="licznik-wysylki">
        <div>
            <div class="licznik-wysylki-ico">
                <i class="fas fa-box-open fa-3x v-align-m mr-10"></i> <?= $this->Translation->get('towar_view.preorderDostepnyZa') ?>
            </div>
            <?= $this->element('default/time_counter', ['endTime' => $towar->preorder_od->format('Y-m-d H:i:s'),'allowDays'=>true, 'timeCounterId' => 'wysylka-time']) ?>
        </div>
        <div class="licznik-wysylki-info padding-left-0"><?= $this->Translation->get('towar_view.preorderDostepnyZaInfo', ['od'=>$towar->preorder_od->format('Y-m-d H:i'),'do'=>$towar->preorder_do->format('Y-m-d H:i')], true) ?></div>
    </div>
</div>