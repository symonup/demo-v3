
<?php
$forceBadge = null;
if (!empty($listLabel)) {
    switch ($listLabel) {
        case 'promocje':
            $forceBadge = 'promocja';
            break;
        case 'nowosci':
            $forceBadge = 'nowosc';
            break;
        case 'polecane':
            $forceBadge = 'polecany';
            break;
        case 'wyroznione':
            $forceBadge = 'wyrozniony';
            break;
        case 'wyprzedaz':
            $forceBadge = 'wyprzedaz';
            break;
        default:
            break;
    }
}
if (!empty($towary)) {
    ?>
    <div class="row mt-30">
        <?php
        foreach ($towary as $towar) {
            ?>

            <div class="col-12 col-md-3 col-sm-6">
                <?= $this->element('default/item_carusel', ['towar' => $towar, 'forceBadge' => $forceBadge, 'showPegi' => false,'hideBadge'=>(!empty($hideBadge)?hideBadge:false), 'premiera' => (!empty($premiera) ? $premiera : false),'gratis'=>(!empty($listLabel) && $listLabel=='gratisy')]) ?>
            </div>
            <?php
        }
        ?>
    </div>
    <div class="row">
        <div class="col-12 text-right"><?= (!empty($noPaging) ? '' : $this->element('default/paginator_input')) ?></div>
    </div>
    <?php
}
?>