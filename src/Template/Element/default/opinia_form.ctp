<?php
$nowa_gwiazdki = '';
for ($ngw_i = 1; $ngw_i <= $count_star; $ngw_i++) {
    $nowa_gwiazdki .= $this->Html->tag('span', '', ['class' => 'fa fa-star star', 'aria-hidden' => 'true', 'ocena' => $ngw_i]);
}
echo $this->Form->create('towar_opinia', ['url' => Cake\Routing\Router::url(['controller' => 'towar', 'action' => 'opinia', $towar->id]), 'class' => 'form-default', 'id' => 'nowa_opinia']);
?>
<div class="row align-items-end">
    <div class="col-12 col-lg-9">
        <?php
        echo $this->Form->input('autor', ['type' => 'text', 'required' => true, 'class' => 'form-control', 'label' => $this->Translation->get('opinia.yourNick'), 'templates' => ['formGroup' => '<div>' . $this->Translation->get('opinia.rateTheProduct') . '</div><div id="new_stars">' . $nowa_gwiazdki . '</div>{{label}}{{input}}']]);
        echo $this->Form->input('ocena', ['type' => 'hidden', 'value' => $count_star]);
        echo $this->Form->input('opinia', ['label' => $this->Translation->get('opinia.Opinion'), 'required' => true, 'class' => 'form-control', 'type' => 'textarea', 'templates' => ['inputContainer' => '<div class="form-group textarea">{{content}}</div>']]);
        ?>
    </div>
    <div class="col-12 col-lg-3"><?= $this->Form->submit($this->Translation->get('opinia.addOpinion'), ['class' => 'btn btn-lg btn-primary btn-color-1 xs-btn-block']) ?></div>
</div>
<?php
echo $this->Form->end();
?>
