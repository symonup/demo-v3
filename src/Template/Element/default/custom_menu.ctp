<ul class="top-menu-big-nav">
                    <?php
                    if ($menu->count() > 0) {
                        foreach ($menu as $menuItem) {
                            if ($menuItem->typ == 'produkty') {
                                ?>
                                <li><a href="<?= (!empty($menuItem->url) ? $menuItem->url : '#') ?>"  title="<?= (!empty($menuItem->title) ? $menuItem->title : strip_tags($menuItem->nazwa)) ?>"><?= $menuItem->nazwa ?></a>
                                    <div class="sub-menu">
                                        <ul class="product-sub-menu">
                                            <?= $this->Navi->getAllCatByUl($allCats, 2) ?>
                                        </ul>
                                    </div>
                                </li>
                                <?php
                            } else {
                                ?>
                                <li>
                                    <a <?= (!empty($menuItem->child_menu) ? 'data-toggle="dropdown"' : '') ?> <?= ((!empty($menuItem->child_menu) && !empty($menuItem->url) && $menuItem->url != '#') ? 'onclick="document.location=\'' . $menuItem->url . '\';return true;"' : '') ?> href="<?= (!empty($menuItem->url) ? $menuItem->url : '#') ?>" target="<?= $menuItem->target ?>" title="<?= (!empty($menuItem->title) ? $menuItem->title : strip_tags($menuItem->nazwa)) ?>"><?= $menuItem->nazwa ?></a>
                                    <?php if (!empty($menuItem->child_menu)) {
                                        ?>
                                        <div class="dropdown-menu">
                                            <?php
                                            foreach ($menuItem->child_menu as $childMenu) {
                                                ?>
                                                <a class="dropdown-item" href="<?= $childMenu->url ?>" target="<?= $childMenu->target ?>" title="<?= (!empty($childMenu->title) ? $childMenu->title : $childMenu->nazwa) ?>"><i class="fa fa-angle-right"></i> <?= $childMenu->nazwa ?></a>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    <?php }
                                    ?>
                                </li>
                                <?php
                            }
                        }
                    }
                    ?>
                </ul>