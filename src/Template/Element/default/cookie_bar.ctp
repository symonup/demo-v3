<div id="cookieBar">
    <div>
        <span class="cookieClose"><i class="fa fa-times"></i></span>
        <div><?= $this->Translation->get('cookie.cookieInfo') ?></div>
        <div class="text-center mt-10">
            <button type="button" class="btn btn-color-1 confirmCookie" data-target="<?= \Cake\Routing\Router::url(['controller'=>'Home','action'=>'confirmCookie'])?>"><?= $this->Translation->get('cookie.cookieButton') ?></button>
        </div>
    </div>
</div>