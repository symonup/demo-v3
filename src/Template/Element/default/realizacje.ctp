<div class="container-fluid mt-40">
    <div class="row">
        <div class="col-12">
            <div class="fc-grey fs-18 fw-regular text-uppercase"><?=t('labels.naszeRealizacje')?></div>
        </div>
    </div>
        <div class="row justify-content-center">
            <div class="col-12 col-md-12 overflow_hidden">
                <div class="scene">
                    <div class="cl-carousel">
                        <div class="carousel__cell"><a href="#r1"><img src="/img/karuzela/1_min.png"/></a></div>
                        <div class="carousel__cell"><img src="/img/karuzela/2_min.png"/></div>
                        <div class="carousel__cell"><img src="/img/karuzela/3_min.png"/></div>
                        <div class="carousel__cell"><img src="/img/karuzela/2_min.png"/></div>
                        <div class="carousel__cell"><img src="/img/karuzela/3_min.png"/></div>
                        <div class="carousel__cell"><img src="/img/karuzela/2_min.png"/></div>
                        <div class="carousel__cell"><img src="/img/karuzela/3_min.png"/></div>
                        <div class="carousel__cell"><img src="/img/karuzela/2_min.png"/></div>
                        <div class="carousel__cell"><img src="/img/karuzela/3_min.png"/></div>
                        <div class="carousel__cell"><img src="/img/karuzela/2_min.png"/></div>
                        <div class="carousel__cell"><img src="/img/karuzela/3_min.png"/></div>
                        <div class="carousel__cell"><img src="/img/karuzela/2_min.png"/></div>
                        <div class="carousel__cell"><img src="/img/karuzela/3_min.png"/></div>
                        <div class="carousel__cell"><img src="/img/karuzela/2_min.png"/></div>
                        <div class="carousel__cell"><img src="/img/karuzela/3_min.png"/></div>
                        <div class="carousel__cell"><img src="/img/karuzela/2_min.png"/></div>
                    </div>
                </div>
                <div class="carousel-options">
                    <button class="previous-button"><i class="flat flaticon-chevron"></i></button>
                        <button class="next-button"><i class="flat flaticon-chevron"></i></button>
                </div>

            </div>
        </div>
</div>