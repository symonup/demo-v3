<?php
if (!empty($towar)) {
    $oldPrice = null;
    $itemPriceDef = $this->Txt->itemPriceDef($towar);
    $itemPrice = $this->Txt->itemPrice($towar);
    if (empty($count)) {
        $count = 3;
    }
    $col = 12 / $count;
    if ($itemPriceDef > $itemPrice) {
        $oldPrice = $itemPriceDef;
    }
    $badge = '';
    $badgeType = '';
    if (!empty($forceBadge)) {
        $badgeType = $forceBadge;
    } else {
        if (!empty($towar->promocja)) {
            $badgeType = 'promocja';
        } elseif (!empty($towar->wyprzedaz)) {
            $badgeType = 'wyprzedaz';
        } elseif (!empty($towar->nowosc)) {
            $badgeType = 'nowosc';
        } elseif (!empty($towar->polecany)) {
            $badgeType = 'polecany';
        } elseif (!empty($towar->wyrozniony)) {
            $badgeType = 'wyrozniony';
        }
    }
    if (!empty($badgeType)) {
        switch ($badgeType) {
            case 'promocja' : {
                    $badge = $this->Html->tag('span', $this->Translation->get('labels.Promocja'), ['class' => 'badge-label difFont promocja']);
                } break;
            case 'wyprzedaz' : {
                    $badge = $this->Html->tag('span', $this->Translation->get('labels.Wyprzedaz'), ['class' => 'badge-label difFont wyprzedaz']);
                } break;
            case 'nowosc' : {
                    $badge = $this->Html->tag('span', $this->Translation->get('labels.Nowosc'), ['class' => 'badge-label difFont nowosc']);
                } break;
            case 'polecany' : {
                    $badge = $this->Html->tag('span', $this->Translation->get('labels.Polecany'), ['class' => 'badge-label difFont polecany']);
                } break;
            case 'wyrozniony' : {
                    $badge = $this->Html->tag('span', $this->Translation->get('labels.Wyrozniony'), ['class' => 'badge-label difFont wyrozniony']);
                } break;
            default : $badge = '';
                break;
        }
    }
    ?>
    <div class="list-item-container special-item col-lg-<?= $col ?> col-md-6 <?= (!empty($revers) ? 'revers' : '') ?>">
        <a href="<?= \Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'view', $towar->id, $this->Navi->friendlyUrl($towar->nazwa)]) ?>" class="list-item">
            <div class="image">
                <?php
                $imageSrc = '';
                $alt = $towar->nazwa;
                if (!empty($towar->towar_zdjecie)) {
                    if (file_exists($filePath['towar'] . $this->Txt->getKatalog($towar->towar_zdjecie[0]->id) . DS . 'thumb_2_' . $towar->towar_zdjecie[0]->plik)) {
                        $imageSrc = \Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'image', $towar->towar_zdjecie[0]->id, 'thumb_2_' . $towar->towar_zdjecie[0]->plik]);
                    }
                    if (!empty($towar->towar_zdjecie[0]->alt)) {
                        $alt = $towar->towar_zdjecie[0]->alt;
                    }
                }
                if (empty($imageSrc)) {
                    $imageSrc = $displayPath['webroot'] . 'img/noPhoto.png';
                }
                ?>
                <img src="<?= $imageSrc ?>" alt="<?= $alt ?>"/>
            </div>
            <div class="prices">
                <div class="info">
                    <div class="wysylka-ico-parent">
                        <?php 
                        $wysylkaTxt='';
                        if($towar->ilosc > 0){
                            if(!empty($towar->wysylka_info_dostepny)){
                                $wysylkaTxt=$towar->wysylka_info_dostepny;
                            }else{
                                $wysylkaTxt=$this->Translation->get('dostepnosc.dostawa24');
                            }
                        }else{
                            if(!empty($towar->wysylka_info_niedostepny)){
                                $wysylkaTxt=$towar->wysylka_info_niedostepny;
                            }else{
                                $wysylkaTxt=$this->Translation->get('dostepnosc.naZamowienie');
                            }
                        }
                        ?>
                        <span class="wysylka-ico <?= (($towar->ilosc > 0) ? 'dostepny' : 'niedostepny') ?>"><span><?= $wysylkaTxt ?></span></span>
                    </div>
                    <div class="kod"><?= $towar->kod ?></div>
                </div>
                <div class="name"><?= $this->Txt->cutText($towar->nazwa, 22, '[...]') ?></div>
                <div class="price">
                    <span class="<?= !empty($oldPrice) ? 'promo' : '' ?>"><?= $this->Txt->cena($itemPrice) ?></span>
                    <?php
                    if (!empty($oldPrice)) {
                        ?>
                        <span class="old-price"><?= $this->Txt->cena($oldPrice) ?></span>
                        <?php
                    }
                    $dostepnoscTooltip='WiecejNiz{0}';
                    if(empty($towar->max_ilosc)){
                        if($towar->ilosc<=0){
                            $dostepnoscTooltip='BrakNaStanie';
                        }else{
                        $dostepnoscTooltip='{0}szt';
                        }
                    }elseif($towar->ilosc<$towar->max_ilosc){
                        if($towar->ilosc>0){
                            $dostepnoscTooltip='MniejNiz{0}';
                        }else{
                            $dostepnoscTooltip='BrakNaStanie';
                        }
                    }
                    ?>
                        <span data-toggle="tooltip" data-placement="top" title="<?=$this->Translation->get('dostepnosc.'.$dostepnoscTooltip,[(($towar->ilosc<0)?0:$towar->ilosc)])?>" class="dostepnosc-bar <?=((!empty($towar->max_ilosc) && $towar->ilosc > $towar->max_ilosc)?'max':(($towar->ilosc<=0)?'min':'mid'))?>"><span></span><span></span><span></span></span>
                </div>

            </div>
            <?= $badge ?>
        </a>
    </div>
<?php } ?>