<?php
$baseRoute = ['controller' => $this->request->params['controller'], 'action' => $this->request->params['action']] + $this->request->params['pass'];
$maxFItems = 3;
$filterKategorie = '';
if (!empty($kategorieAll)) {
    $filterKategorie = $this->Navi->getAllCatByUl($kategorieAll,20, (!empty($kategoria) ? $kategoria->id : null),null,'zestawy');
}
$kategoryName = $this->Translation->get('left.Menu');
if(!empty($kategoria)){
    $kategoryName = $kategoria->nazwa;
    $backToUrl = ['controller'=>'Towar','action'=>'zestawy'];
    $backToLabel = t('left.wszystkieKategorie');
    if(!empty($kategoria->parent_kategorium) && !empty($kategoria->parent_kategorium->parent_kategorium)){
        $backToUrl[]=$kategoria->parent_kategorium->parent_kategorium->id;
        $backToUrl[]=$this->Txt->friendlyUrl($kategoria->parent_kategorium->parent_kategorium->nazwa);
        $backToLabel=$kategoria->parent_kategorium->parent_kategorium->nazwa;
//        $kategoryName=$kategoria->parent_kategorium->nazwa;
    }
    elseif(!empty($kategoria->parent_kategorium)){
        $backToUrl[]=$kategoria->parent_kategorium->id;
        $backToUrl[]=$this->Txt->friendlyUrl($kategoria->parent_kategorium->nazwa);
        $backToLabel=$kategoria->parent_kategorium->nazwa;
//        $kategoryName=$kategoria->parent_kategorium->nazwa;
    }
}
?>
<div class="special-head">
    <div class="kategoria-curent-name"><?= $this->Html->tag((!empty($kategoria)?'h1':'span'),$kategoryName) ?><i class="flat flaticon-heart-2"></i></div>
<?php if(!empty($kategoria)){
    ?>
    <div class="kategorie-back-to">
        <?=t('left.cofnijDo{0}',[$this->Html->link($backToLabel,$backToUrl,['escape'=>false])])?>
    </div>
    <?php
} ?>
</div>
<?php
if (!empty($filterKategorie)) {
    ?>
    <div class="filter-category-container">
        <ul class="filter-category">
            <?= $filterKategorie ?>
        </ul>
    </div>
    <?php
}

