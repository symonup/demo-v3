<div class="top-bar">
    <div class="container-fluid">
        <div class="row align-items-start">
            <div class="col-12 col-md-auto col-xxl-4 nav-logo-container">
                <a href="<?= Cake\Routing\Router::url(['controller' => 'Home', 'action' => 'index']) ?>"><img src="<?= $displayPath['webroot'] ?>img/logo.png"/></a>
            </div>
            <div class="col-12 col-sm-12 col-md text-left fs-18">
                <div class="row justify-content-end mt-20 nav-top-info">
                    <div class="col-auto">
                        <div class="in-head-value">
                            <i class="flat flaticon-clock"></i>
                            <span>
                                <span><?=t('header.zamowDo{0}',[c('dostawa.maxTime')])?></span>
                                <div class="nav-time-count">
                                    <span><?=t('header.wysylkaZa')?></span>
                                    <?php
                            $wsTime=$this->Txt->deliveryDay();
                                    ?>
                                    <?= $this->element('default/time_counter', ['timeCounterId' => 'nav-ws-counter', 'endTime' => $wsTime, 'hideLabels' => true]) ?>
                                </div>
                            </span>
                        </div>
                    </div>
                    <?php 
                    $darmowaLimit = c('wysylka.limit');
                    if(!empty($darmowaLimit)){
                        $brakuje = $darmowaLimit;
                        if(!empty($cartItems) && !empty($cartItems['total_z_rabatem'])){
                            $brakuje = $brakuje - $cartItems['total_z_rabatem'];
                        }
                        if($brakuje < 0){
                            $brakuje = 0;
                        }
                    ?>
                    <div class="col-auto">
                        <div class="in-head-value">
                            <i class="flat flaticon-percentage"></i>
                            <span>
                                <span><?=t('header.doDarmowejWysylkiBrakuje')?></span>
                                <div id="nav-free-delivery"><?= $this->Txt->cena($brakuje) ?></div>
                            </span>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="col-auto col-xxl-3 text-right">
                        <div class="goToCart">
                            <a href="<?= \Cake\Routing\Router::url(['controller' => 'Cart', 'action' => 'index']) ?>">
                                <span class="cartLinkLabel"></span>
                                <span>
                                    <?= t('header.twojKoszyk') ?>
                                    <span class="cartInfo">
                                        <?php
                                        if (!empty($cartItems) && !empty($cartItems['cart-info'])) {
                                            echo $cartItems['cart-info'];
                                        } else {
                                            echo t('header.Ilosc{0}Kwota{1}', ['<span id="cartItemsCount">0</span>', '<span id="cartItemsValue">' . $this->Txt->cena(0) . '</span>']);
                                        }
                                        ?>
                                    </span>
                                </span>
                            </a>
                            <?php
                            if (!empty($cartItems)) {
                                echo $cartItems['preview'];
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-end">
                    <div class="col-12 col-md col-head-search">
                        <?= $this->element('default/search_form') ?>
                    </div>
                    <div class="col-12 col-md-auto col-xxl-auto">
                        <div class="infolinia-link">
                            <i class="flat flaticon-whatsapp-logo-variant"></i>
                            <span>
                                <a href="tel:<?= Cake\Core\Configure::read('dane.telefon') ?>"><?= Cake\Core\Configure::read('dane.telefon') ?></a>
                                <a href="tel:<?= Cake\Core\Configure::read('dane.telefon2') ?>"><?= Cake\Core\Configure::read('dane.telefon2') ?></a>
                            </span>
                        </div>
                        <div class="infolinia-link">
                            <i class="flat flaticon-email"></i>
                            <span>
                                <a href="mailto:<?= Cake\Core\Configure::read('dane.email') ?>"><?= Cake\Core\Configure::read('dane.email') ?></a>
                            </span>
                        </div>
                    </div>
                    <div class="col-12 col-md-auto">
                        <ul class="top-menu">
                            <li class="custom-link">
                                <a href="<?= \Cake\Routing\Router::url(['controller' => 'Uzytkownik', 'action' => 'index']) ?>">
                                    <i class="flat flaticon-cart-1"></i>
                                    <span><?=t('header.panelHurtowy')?></span>
                                </a>
                            </li>
                            <li class="custom-link">
                                <a href="<?= r(['controller' => 'Towar', 'action' => 'index','ulubione']) ?>">
                                    <i class="flat flaticon-heart"></i>
                                    <span><?=t('header.listaZyczen')?></span>
                                </a>
                            </li>
                            <li class="custom-link link-last">    
                                <?php
                                if (!empty($user)) {
                                    ?>
                                    <a href="<?= \Cake\Routing\Router::url(['controller' => 'Uzytkownik', 'action' => 'index']) ?>">
                                        <i class="flat flaticon-user"></i>
                                    <span><?=t('header.mojeKonto')?></span>
                                    </a>
                                    <a class="logout" href="<?= \Cake\Routing\Router::url(['controller' => 'Home', 'action' => 'logout']) ?>" data-toggle="tooltip" title="<?= $this->Translation->get('header.Wyloguj') ?>"><i class="fas fa-sign-out-alt"></i></a>
                                    <?php
                                } else {
                                    ?>
                                    <a  href="<?= \Cake\Routing\Router::url(['controller' => 'Home', 'action' => 'login']) ?>">
                                        <i class="flat flaticon-user"></i>
                                        <span><?=t('header.mojeKonto')?></span>
                                    </a>
                                    <?php
                                }
                                ?>
                            </li>
                            <li>
                                    <a class="gold-link" href="<?=r(['controller'=>'Blog','action'=>'index'])?>"><?=t('header.Blog')?> <i class="fa fa-angle-right"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row top-custom-menu justify-content-between">
                        <?php
                        if ($menu->count() > 0) {
                ?>
                <?php
                foreach ($menu as $menuItem) {
                    ?>
                    <div class="col-12 col-md-auto top-menu-item">
                        <div class="<?= (!empty($menuItem->child_menu) ? 'dropdown' : '') ?>">
                            <a class="top-menu-link <?= (!empty($menuItem->child_menu) ? 'dropdown-toggle' : '') ?>" <?= (!empty($menuItem->child_menu) ? 'data-toggle="dropdown"' : '') ?> <?= ((!empty($menuItem->child_menu) && !empty($menuItem->url) && $menuItem->url != '#') ? 'onclick="document.location=\'' . $menuItem->url . '\';return true;"' : '') ?> href="<?= (!empty($menuItem->url) ? $menuItem->url : '#') ?>" target="<?= $menuItem->target ?>" title="<?= (!empty($menuItem->title) ? $menuItem->title : $menuItem->nazwa) ?>"><?= $menuItem->nazwa ?></a>
                            <?php if (!empty($menuItem->child_menu)) {
                                ?>
                                <div class="dropdown-menu">
                                    <?php
                                    foreach ($menuItem->child_menu as $childMenu) {
                                        ?>
                                        <a class="dropdown-item" href="<?= $childMenu->url ?>" target="<?= $childMenu->target ?>" title="<?= (!empty($childMenu->title) ? $childMenu->title : $childMenu->nazwa) ?>"><?= $childMenu->nazwa ?></a>
                                        <?php
                                    }
                                    ?>
                                </div>
                            <?php }
                            ?>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <?php
            }
                        ?>
                </div>

            </div>
        </div>
    </div>
</div>