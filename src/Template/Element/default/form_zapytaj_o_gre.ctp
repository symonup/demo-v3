<?php 
?><?=$this->Form->create(null,['url'=>['controller'=>'Home','action'=>'zapytanie'],'class'=>'form-zapytaj-o-gre'])?>
<div class="row">
    <div class="col-12 col-md-5">
        <div class="op-w-55">
            <img src="/img/question.png"/>
        </div>
    </div>
    <div class="col-12 col-md-7 md-padding-left-0">
        <div class="zapytaj-o-gre-info">
            <?=$this->Translation->get('zapytajOGre.infoTop',[],true)?>
        </div>
        <?=$this->Form->control('platforma',['type'=>'select','options'=>$platformy,'empty'=>$this->Translation->get('zapytajOGre.labelPlatformaDowolna'),'default'=>'inna','label'=>$this->Translation->get('zapytajOGre.labelPlatforma')])?>
        <?=$this->Form->control('tresc',['type'=>'textarea','required'=>true,'rows'=>3,'placeholder'=>$this->Translation->get('zapytajOGre.placeholderUwagi'),'label'=>$this->Translation->get('zapytajOGre.labelUwagi')])?>
    </div>
</div>
<div class="row mt-15">
    <div class="col-12 col-md-4">
         <?=$this->Form->control('imie',['label'=>false,'required'=>true,'type'=>'text','placeholder'=>$this->Translation->get('zapytajOGre.placeholderImie')])?>
    </div>
    <div class="col-12 col-md-4">
         <?=$this->Form->control('email',['label'=>false,'required'=>true,'type'=>'email','placeholder'=>$this->Translation->get('zapytajOGre.placeholderEmail')])?>
    </div>
    <div class="col-12 col-md-4">
         <?=$this->Form->control('telefon',['label'=>false,'type'=>'text','placeholder'=>$this->Translation->get('zapytajOGre.placeholderTelefon')])?>
    </div>
</div>
<div class="row">
    <?php /*<div class="col-12 col-md-7 padding-right-0 text-center">
         <?=$this->Form->control('zgoda_dane',['required'=>true,'type'=>'checkbox','id'=>'zgoda-zapytaj-o-gre','label'=>$this->Translation->get('zapytajOGre.zgodaDaneLabel')])?>
    </div> */ ?>
    <div class="col-12 col-md-12 text-center">
        <button type="submit" class="btn btn-link"><?=$this->Translation->get('zapytajOGre.wyslij')?></button>
    </div>
</div>
<?=$this->Form->end()?>