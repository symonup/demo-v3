<div class="row align-items-end justify-content-between home-kontakt-items">
    <div class="col-6 col-md-auto">
        <a href="<?= Cake\Routing\Router::url(['controller'=>'Home','action'=>'getTalk'])?>" class="call-back getPupUpLink" modal-target="#get-talk-modal">
            <div>
            <i class="flat flaticon-woman-with-headset"></i>
            <span><?=t('homeKontakt.maszPytania')?>
                <span><?=t('homeKontakt.zamowRozmowe')?></span>
            </span>
            </div>
            <div><?=t('homeKontakt.oddzwonimyDoCiebie')?> <i class="flat flaticon-telephone"></i></div>
        </a>
    </div>
    <div class="col-6 col-md-auto col-lg-auto call-us">
        <span><?=t('homeKontakt.zadzwonDoNas')?></span>
        <a href="tel:<?=c('dane.telefon')?>"><?=c('dane.telefon')?></a>
        <div>
            <span><?=t('homeKontakt.lubSkorzystajZFormularza')?></span>
            <a href="/kontakt#k-form" class="btn btn-primary btn-sm"><?=t('homeKontakt.formularzKontaktowy')?> <i class="fa fa-angle-right"></i></a>
        </div>
    </div>
    <div class="col-12 col-md col-lg col-xxl-4">
        <?=$this->element('default/newsletter_form')?>
    </div>
</div>