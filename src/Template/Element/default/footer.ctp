<footer>
        <div class="container-fluid footer">
            <div class="container-fluid">
                <div class="row justify-content-between">
                    <div class="col-12 col-md-12 col-lg-2 col-xxl-auto footer-logo align-self-center">
                        <a href="<?=r(['controller'=>'Home','action'=>'index'])?>"><img src="/img/logo_mini.png"/></a>
                    </div>
                    <div class="col-12 col-md-12 col-lg-auto col-xxl">
                        <?=c('dane.stopka_dane')?>
                        <div class="mt-10">
                            <a class="gold-link" href="<?=r(['controller'=>'Blog','action'=>'index'])?>"><?=t('footer.Blog')?> <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
               <?php /*     <div class="col-12 <?=(!$mobile?'col-md':'')?>">
                        <div class="row  justify-content-around">
                    <?php */
                    $footerGroups = [];
                    if (!empty($footerLinks) && $footerLinks->count() > 0) {
                        foreach ($footerLinks as $footerLink) {
                            if(!empty($footerLink->strona)){
                                if(!empty($footerLink->strona->ukryta)){
                                    continue;
                        }
                                $footerGroups[$footerLink->grupa][] = '<li><a href="' . \Cake\Routing\Router::url(['controller' => 'Pages', 'action' => 'view', $footerLink->strona->id, $this->Txt->friendlyUrl($footerLink->strona->nazwa)]) . '" link-type="' . $footerLink->strona->type . '" title="' . (!empty($footerLink->strona->seo_tytul) ? $footerLink->strona->seo_tytul : $footerLink->strona->nazwa) . '" '.(!empty($footerLink->nofollow)?'rel="nofollow"':'').'>' . $footerLink->strona->nazwa . '</a></li>';
                    }
                    elseif($footerLink->url=='[%hot-deal-link%]'){
                        if(!empty($hotDealLink)){
                            $footerGroups[$footerLink->grupa][] = '<li><a href="' . $hotDealLink . '" title="' . (!empty($footerLink->title) ? $footerLink->title : $footerLink->label) . '" target="' . $footerLink->target . '"  '.(!empty($footerLink->nofollow)?'rel="nofollow"':'').'>' . $footerLink->label . '</a></li>';
                        }
                    }
                            else $footerGroups[$footerLink->grupa][] = '<li><a href="' . $footerLink->url . '" title="' . (!empty($footerLink->title) ? $footerLink->title : $footerLink->label) . '" target="' . $footerLink->target . '"  '.(!empty($footerLink->nofollow)?'rel="nofollow"':'').'>' . $footerLink->label . '</a></li>';
                        }
                    }

//                    if (!empty($pagesFooterLinks) && $pagesFooterLinks->count() > 0) {
//                        foreach ($pagesFooterLinks as $page) {
//                            $footerGroups[1][] = '<li><i class="fa fa-caret-right"></i> <a href="' . \Cake\Routing\Router::url(['controller' => 'Pages', 'action' => 'view', $page->id, $this->Txt->friendlyUrl($page->nazwa)]) . '" link-type="' . $page->type . '" title="' . (!empty($page->seo_tytul) ? $page->seo_tytul : $page->nazwa) . '">' . $page->nazwa . '</a></li>';
//                        }
//                    }
ksort($footerGroups);
                    if (!empty($footerGroups)) {
                        foreach ($footerGroups as $grupa => $footerGroupLinks) {
                            ?>
                            <div class="col-12 <?=(!$mobile?'col-md':'')?> col-xxl">
                                <div class="footer-head difFont"><?= t('footer.NazwaGrupy_' . $grupa) ?></div>
                                <ul class="footer-links">
                                    <?= join('', $footerGroupLinks) ?>
                                </ul>
                            </div>
                            <?php
                        }
                    }
               /*     ?>
                </div>
                    </div>
                    <?php */
                        ?>
                    <div class="col-12 <?=(!$mobile?'col-md-auto':'')?> col-xxl-auto footer-kontakt">
                        <div><?= t('footer.Kontakt') ?></div>
                        <div><?= t('footer.kontaktInfo',['telefon'=>c('dane.telefon'),'email'=>c('mail.domyslny_email')],true) ?></div>
                        <?php //$this->element('default/newsletter_form') ?>
                    </div>
                    <?php
                    
                    ?>
                    
                </div>
            </div>
        </div>
        <div class="container-fluid copyright">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-12  <?=(!$mobile?'col-md-6':'')?> text-left">
                        <?= t('footer.{0}copyRight', [date('Y')]) ?>
                    </div>
                    <div class="col-12  <?=(!$mobile?'col-md-6':'')?> text-right">
                        <?= t('footer.realizacja')?> <a href="https://samatix.pl/" target="_blank">Samatix</a>
                    </div>
                </div>
            </div>
        </div>
</footer>
<?php
if(!empty($showCookieBar)){
   echo $this->element('default/cookie_bar');
}
?>