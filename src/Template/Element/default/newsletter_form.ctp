<?php 
if($mobile){
?>
<form method="post" action="<?= \Cake\Routing\Router::url(['controller'=>'Home','action'=>'newsletter'])?>" id="newsletter-form">
    <div class="newsletterInfo"><?=$this->Translation->get('newsletter.newsletterInfo',[],true)?></div>
    <span class="newsletter-ico"><i class="flat flaticon-send"></i></span>
    <input required="required" name="email" type="email" placeholder="<?=$this->Translation->get('newsletter.emailPlaceholder')?>" class="newsletter-input">
    <?php /* <div class="newsletter-zgoda">
        <input required="required" type="checkbox" name="zgoda_dane" value="Tak" id="newsletter-zgoda-dane"/><label for="newsletter-zgoda-dane"><?=$this->Translation->get('newsletter.zgodaNaPrzetwarzanieDanych')?></label>
    </div>
    <div class="newsletter-zgoda">
        <input required="required" type="checkbox" name="zgoda_polityka_prywatnosci" value="Tak" id="newsletter-zgoda-polityka-prywatnosci"/><label for="newsletter-zgoda-polityka-prywatnosci"><?=$this->Translation->get('newsletter.zapoznalemSieZPolitykaPrywantosci')?></label>
    </div> */ ?>
    <button type="submit" class="newsletter-button btn-color-1"><?=$this->Translation->get('newsletter.wyslij')?></button>
</form>
<?php }else{
    ?>
<form method="post" action="<?= \Cake\Routing\Router::url(['controller'=>'Home','action'=>'newsletter'])?>" id="newsletter-form">
    <div class="newsletterInfo"><?=$this->Translation->get('newsletter.newsletterInfo',[],true)?></div>
    <span class="newsletter-ico"><i class="flat flaticon-send"></i></span>
    <input required="required" name="email" type="email" placeholder="<?=$this->Translation->get('newsletter.emailPlaceholder')?>" class="newsletter-input">
    <button type="submit" class="newsletter-button btn btn-primary"><?=$this->Translation->get('newsletter.wyslij')?></button>
</form>
<?php
} ?>