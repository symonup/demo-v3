<?php
if (!empty($zestawy) && $zestawy->count() > 0) {
    $zestawyItems = [];
    foreach ($zestawy as $zestaw) {
        $showAllow = true;
        $zawartoscZ = [];
        $produktGlowny = $this->Html->tag('div', $this->Html->tag('div', $this->Txt->towarZdjecie($zestaw->towar), ['class' => 'image'])
                .$this->Html->tag('div', (!empty($zestaw->towar->zestaw_nazwa)?$zestaw->towar->zestaw_nazwa:$zestaw->towar->nazwa), ['class' => 'name'])
                .$this->Html->tag('div', $this->Txt->cena($this->Txt->itemPrice($zestaw->towar)), ['class' => 'price'])
                ,['class'=>'zestaw-item']);
        $_zestaw = '';
        $cena_zestawu = $this->Txt->itemPrice($zestaw->towar);
        $zt_i = 1;
        if ($zestaw->towar->ilosc <= 0) {
            $showAllow = false;
        }
        $zawartoscZ[] = (!empty($zestaw->towar->zestaw_nazwa) ? $zestaw->towar->zestaw_nazwa : $zestaw->towar->nazwa);
        foreach ($zestaw->zestaw_towar as $item) {
            if ($item->towar->ilosc <= 0) {
                $showAllow = false;
            }
            $cena_zestawu += $this->Txt->itemPrice($item->towar);
            $zawartoscZ[] = (!empty($item->towar->zestaw_nazwa) ? $item->towar->zestaw_nazwa : $item->towar->nazwa);

            $_zestaw .= $this->Html->tag('div', $this->Html->tag('div', $this->Txt->towarZdjecie($item->towar), ['class' => 'image'])
                .$this->Html->tag('div', (!empty($item->towar->zestaw_nazwa)?$item->towar->zestaw_nazwa:$item->towar->nazwa), ['class' => 'name'])
                .$this->Html->tag('div', $this->Txt->cena($this->Txt->itemPrice($item->towar)), ['class' => 'price'])
                ,['class'=>'zestaw-item'.(($zt_i == count($zestaw->zestaw_towar)) ? ' last' : '')]);
            $zt_i++;
        }
        $cena_rabat = round($cena_zestawu - (($cena_zestawu * round($zestaw->rabat, 2)) / 100), 2);
        $oszczedzasz = $cena_zestawu - $cena_rabat;
        $nazwa = $this->Html->tag('div', (!empty($zestaw->nazwa) ? $zestaw->nazwa : $this->Translation->get('towar_view.zestaw')), ['class' => 'zestaw-nazwa']);
        $zawartosc = $this->Html->tag('div', $this->Html->tag('div', $this->Translation->get('towar_view.zawartoscZestawu'), ['class' => 'zestaw-zawartosc-head']) . $this->Html->tag('div', join('<br/>', $zawartoscZ), ['class' => 'zestaw-zawartosc-items']), ['class' => 'zestaw-zawartosc']);
        $ceny = $this->Html->tag('div', $this->Txt->cena($cena_zestawu), ['class' => 'zestaw-cena-old']).$this->Html->tag('div', $this->Txt->cena($cena_rabat), ['class' => 'zestaw-cena']);
        $button = '<input type="text" value="1" class="item-ilosc-input zestaw-ilosc" zestaw-id="' . $zestaw->id . '" autocomplete="off"><button zestaw-id="' . $zestaw->id . '" class="add-to-cart zestaw-button" type="button">'.$this->Translation->get('towar_view.zestawAddToCart').'</button>';
        if ($showAllow) {
            $zestawyItems[] = $this->Html->tag('div', $this->Html->tag('div', $this->Html->tag('div', $produktGlowny . $_zestaw, ['class' => 'zestaw-items']) . $this->Html->tag('div', $ceny.$button, ['class' => 'zestaw_podsumowanie']), ['class' => 'zestaw']), ['class' => 'carousel-item' . (empty($zestawyItems) ? ' active' : '')]);
        }
    }
    if (count($zestawyItems) > 0) {
        ?>
<div class="col-12 col-md col-lg-auto lg-auto-max-667 col-xxl">
    <div id="view-zestaw-s" class="box-bg-grey">
        <div class="row align-items-center">
            <div class="col-12 col-md-auto"><div class="textHeadOne"><i class="flat flaticon-percent"></i> <?= $this->Translation->get('towar_view.wZestawieTaniej') ?></div></div>
            <div class="col-12 col-md text-right">
                  <?php if (count($zestawyItems) > 1) {
                    ?>
                    <a class="btn btn-color-1" href="#carouselZestaw" role="button" data-slide="prev">
                        <i class="flat flaticon-left-direction-arrow"></i>
                    </a>
                    <a class="btn btn-color-1" href="#carouselZestaw" role="button" data-slide="next">
                        <i class="flat flaticon-right-thin-chevron"></i>
                    </a>
                <?php }
                ?>
                <a href="<?=r(['controller'=>'Towar','action'=>'zestawy'])?>" class="btn <?=($mobile?'btn-block':'')?> btn-color-1"><?=t('towar_view.zobaczInneZestawy')?> <i class="flat flaticon-right-thin-chevron"></i></a>
            </div>
        </div>
            <div id="carouselZestaw" class="carousel slide carusel-items" data-ride="carousel">
                <div class="carousel-inner">
                    <?= join('', $zestawyItems) ?>
                </div>
            </div>
        </div>
</div>
        <?php
    }
}
?>
