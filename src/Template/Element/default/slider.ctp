<?php 
if(empty($id)){
    $id= uniqid('carousel');
}
?><div id="<?= $id ?>" class="carousel slide custom-slide-control" data-ride="carousel">
<?php if(!empty($textTop)){
    echo $this->Html->tag('span',$textTop,['class'=>'tekst-top']);
} ?>
    <div class="carousel-inner">
        <?php
        foreach ($caruselPages as $key => $page) {
            ?>
            <div class="carousel-item <?= (($key == 0) ? 'active' : '') ?>">
                <?= $page ?>
            </div>
            <?php
        }
        ?>
    </div>
    <?php if (count($caruselPages) > 1) { ?>
        <a class="carousel-control-prev" href="#<?= $id ?>" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#<?= $id ?>" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    <?php } 
    if(!empty($textBottom)){
    echo $this->Html->tag('span',$textBottom,['class'=>'tekst-bottom']);
} ?>
</div>