
<?php
if (!empty($zestawy)) {
    ?>
    <div class="row mt-30">
        <?php
        foreach ($zestawy as $zestaw) {
            ?>

            <div class="col-12 col-md-6 mb-20">
                <?= $this->element('default/zestaw_item', ['zestaw' => $zestaw]) ?>
            </div>
            <?php
        }
        ?>
    </div>
    <div class="row">
        <div class="col-12 text-right"><?= (!empty($noPaging) ? '' : $this->element('default/paginator_input')) ?></div>
    </div>
    <?php
}
?>