<?php
if(empty($count)){
    $count=6;
}
if(!empty($items)){
    ?>
<div class="row list-item-row">
<?php
$i=1;
    foreach ($items as $item){
        if(!empty($max) && $i>$max){
            break;
        }
        echo $this->element('default/towar_lista_item_bordered',['towar'=>$item,'count'=>$count,'forceBadge'=>(!empty($forceBadge)?$forceBadge:null)]);
        $i++;
    }
    ?>
</div>
<?php
}

