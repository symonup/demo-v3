<form class="kontakt-form" method="post" action="<?= Cake\Routing\Router::url(['controller' => 'Contact', 'action' => 'index']) ?>">
    <div class="row justify-content-end align-items-end" style="margin-bottom: 20px;">
        <div class="col-12 col-md-4">
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <input placeholder="<?= $this->Translation->get('kontaktForm.YourName') ?>" value="<?= ((!empty($kontaktFormData) && key_exists('imie', $kontaktFormData)) ? $kontaktFormData['imie'] : '') ?>" class="form-control" required="required" type="text" name="imie"/>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <input placeholder="<?= $this->Translation->get('kontaktForm.Surname') ?>" value="<?= ((!empty($kontaktFormData) && key_exists('nazwisko', $kontaktFormData)) ? $kontaktFormData['nazwisko'] : '') ?>" class="form-control" type="text" name="nazwisko"/>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <input placeholder="<?= $this->Translation->get('kontaktForm.NazwaFirmy') ?>" value="<?= ((!empty($kontaktFormData) && key_exists('firma', $kontaktFormData)) ? $kontaktFormData['firma'] : '') ?>" class="form-control" type="text" name="firma"/>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <input placeholder="<?= $this->Translation->get('kontaktForm.Phone') ?>" value="<?= ((!empty($kontaktFormData) && key_exists('telefon', $kontaktFormData)) ? $kontaktFormData['telefon'] : '') ?>" class="form-control" type="text" name="telefon"/>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group" style="margin-bottom: 0;">
                        <input placeholder="<?= $this->Translation->get('kontaktForm.EmailAddress') ?>" value="<?= ((!empty($kontaktFormData) && key_exists('email', $kontaktFormData)) ? $kontaktFormData['email'] : '') ?>" class="form-control" required="required" type="email" name="email"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-8">
            <div class="row">
                <div class="col-12">
                    <div class="form-group" style="margin-bottom: 0;">
                        <textarea style="height: 229.89px;" placeholder="<?= $this->Translation->get('kontaktForm.Message') ?>" rows="9" class="form-control" required="required" name="tresc"><?= ((!empty($kontaktFormData) && key_exists('tresc', $kontaktFormData)) ? $kontaktFormData['tresc'] : '') ?></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
            <div class="row justify-content-end align-items-center">
                <div class="col-12 col-md-auto md-padding-right-0 text-right captcha-container">
                    <?= $this->Captcha->input('Contact') ?>
                </div>
        <div class="col-12 col-md-auto">
            <button style="border-radius: 10px;" type="submit" class="btn btn-success"><?= $this->Translation->get('kontaktForm.Send') ?><i class="flat flaticon-send-1 kontakt-send-ico"></i></button>
        </div>
            </div>
</form>