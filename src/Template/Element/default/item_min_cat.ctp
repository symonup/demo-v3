<?php
if (!empty($towar)) {
    $oldPrice = null;
    $rabatPrec=null;
    $itemPriceDef = $this->Txt->itemPriceDef($towar);
    $itemPrice = $this->Txt->itemPrice($towar);
    $itemPriceNetto = $this->Txt->itemPrice($towar,false,'netto');
    if (empty($count)) {
        $count = 3;
    }
    if (empty($premiera)) {
        $premiera = false;
    }
    if ($itemPriceDef > $itemPrice) {
        $oldPrice = $itemPriceDef;
    }
    if(!empty($oldPrice)){
        $rabatPrec=round(100 - (($itemPrice*100)/$oldPrice));
    }
    ?>
    <div class="product item-min-cat">
        <a href="<?= $this->Txt->towarViewUrl($towar) ?>" class="content"> 
            <div class="product-image">
                <?= $this->Txt->towarZdjecie($towar, 'thumb_2_') ?>
            </div>
            <div class="product-content">
                <div class="product-name">
                    <span><?= $towar->nazwa ?></span>
                </div>
                <div class="price">
                    <?php
                    if (!empty($oldPrice)) {
                        if(!empty($rabatPrec)){
                            ?>
                    <span class="rabat-badge">-<?=$rabatPrec?>%</span>
                    <?php
                        }
                        ?>
                        <span class="old-price"><?= $this->Txt->cena($oldPrice) ?></span>
                        <?php
                    }
                    ?>
                </div>
                <div class="price">
                    <div class="<?= (!empty($oldPrice) ? ' promotion' : '') ?>">
                        <span class="<?= !empty($oldPrice) ? 'promo' : '' ?>"><?= $this->Txt->cena($itemPrice) ?></span>
                    </div>
                    <?php 
                    if(!empty($itemPriceNetto)){
                        ?>
                    <div class="price-netto">
                        <span><?= t('labels.netto{0}',[$this->Txt->cena($itemPriceNetto)]) ?></span>
                    </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </a>
    </div>
<?php } ?>
