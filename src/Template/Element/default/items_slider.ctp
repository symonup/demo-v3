<?php
if (empty($sliderId)) {
    $sliderId = uniqid();
}
if (empty($perPage)) {
    $perPage = 2;
}
if(12 % $perPage > 0){
    $cols = '';
}else{
    $cols = '-'.(12 / $perPage);
}
?>
<div id="<?= $sliderId ?>" class="carousel slide carusel-items" data-ride="carousel">
    <div class="carousel-inner">

        <?php
        $allItems = [];
        foreach ($items as $item) {
            $allItems[] = $this->element('default/item_carusel', ['towar' => $item, 'hideBadge' => false, 'hideBadgeTyp' => ['wyrozniony']]);
        }
        $bPages = count($allItems);
        if (!$mobile) {
            $itemPages = array_chunk($allItems, $perPage);
            $bPages = count($itemPages);
            foreach ($itemPages as $key => $towar_items) {
                if(count($towar_items)<$perPage){
                    $tmpCol='-3';
                }else{
                    $tmpCol=$cols;
                }
                ?>
                <div class="carousel-item <?= ($key == 0 ? 'active' : '') ?>">
                    <div class="row">
                        <div class="col-12 col-md<?= $tmpCol ?>"><?= join('</div><div class="col-12 col-md' . $tmpCol . '">', $towar_items) ?></div>
                    </div>
                </div>
                <?php
            }
        } else {
            foreach ($allItems as $key => $towar_items) {
                ?>
                <div class="carousel-item <?= ($key == 0 ? 'active' : '') ?>">
                    <div class="row">
                        <div class="col-12"><?= (is_array($towar_items)?join('</div><div class="col-12 col-md-6">', $towar_items):$towar_items) ?></div>
                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>
    <?php
    if ($bPages > 1) {
        ?>
        <a class="left carousel-control no-shadow" href="#<?= $sliderId ?>" role="button" data-slide="prev">
            <span class="carusel-control-img carusel-control-img-prev" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control no-shadow" href="#<?= $sliderId ?>" role="button" data-slide="next">
            <span class="carusel-control-img carusel-control-img-next" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        <?php
    }
    ?>
</div>