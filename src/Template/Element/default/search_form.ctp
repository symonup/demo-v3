<form class="search" ajax-action="<?= \Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'searchRq']) ?>" action="<?= \Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'index']) ?>">
    <input type="text" required="required" autocomplete="off" name="search" value="<?= (!empty($searchQuery) ? $searchQuery : '') ?>" class="search-input" placeholder="<?= $this->Translation->get('header.czegoSzukasz') ?>"/>
    <button type="submit"><i class="flat flaticon-search-1"></i></button>
</form>
<div class="promo-text"><?=t('header.promoText')?></div>