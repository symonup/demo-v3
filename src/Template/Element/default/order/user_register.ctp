<div class="row justify-content-center">
    <div class="col-12">
        <h4 class="pageHead"><?= $this->Translation->get('koszyk.ZalogujSieAbyZlozycZamowienieSzybciej') ?></h4>
        <hr/>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <h5 class="text-center"><?= $this->Translation->get('koszyk.signInHeader') ?></h5>
        <?= $this->Form->create(null, ['class' => 'login-form', 'url' => \Cake\Routing\Router::url(['controller' => 'Home', 'action' => 'login', 'order'])]) ?>
        <div class="row">
            <div class="col-md-12">
                <?= $this->Form->control('email', ['type' => 'email', 'id' => 'login-email', 'label' => $this->Translation->get('loginPage.emailInputLabel')]) ?>
                <?= $this->Form->control('password', ['type' => 'password', 'id' => 'login-password', 'required' => true, 'label' => $this->Translation->get('loginPage.passwordInputLabel')]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 text-left">
                <a href="<?= Cake\Routing\Router::url(['controller' => 'Home', 'action' => 'resetPassword']) ?>"><?= $this->Translation->get('loginPage.resetPasswordLink') ?></a>
            </div>
            <div class="col-md-6 text-right">
                <button type="submit" class="btn btn-outline-success" name="login"><?= $this->Translation->get('loginPage.signInButton') ?></button>
            </div>

        </div>
        <?= $this->Form->end() ?>
        <div class="login-more-info">
            <?= $this->Translation->get('koszyk.LoginMoreInfo', [], true) ?>
        </div>
    </div>
    <div class="col-md-8">
        <h5 class="text-center"><?= $this->Translation->get('koszyk.joinHeader') ?></h5>

        <?= $this->Form->create($uzytkownik, ['class' => 'register-form', 'url' => \Cake\Routing\Router::url(['controller' => 'Home', 'action' => 'login', 'order'])]) ?>
        <div class="row">
            <div class="col-md-6">
                <?= $this->Form->control('imie', ['type' => 'text','minlength'=>2,'id'=>'register-imie', 'label' => $this->Translation->get('loginPage.nameInputLabel')]) ?>
            </div>
            <div class="col-md-6">
                <?= $this->Form->control('nazwisko', ['type' => 'text','minlength'=>2,'id'=>'register-nazwisko', 'label' => $this->Translation->get('loginPage.lastNameInputLabel')]) ?>
            </div>
            <div class="col-md-6">
                <?= $this->Form->control('firma', ['type' => 'text','minlength'=>2,'id'=>'register-firma','other-req'=>'#register-nip', 'label' => $this->Translation->get('loginPage.companyInputLabel')]) ?>
            </div>
            <div class="col-md-6">
                <?= $this->Form->control('nip', ['type' => 'text','id'=>'register-nip','other-req'=>'#register-firma', 'label' => $this->Translation->get('loginPage.nipInputLabel')]) ?>
            </div>
            <div class="col-md-6">
                <?= $this->Form->control('telefon', ['type' => 'text','id'=>'register-telefon', 'data-type' => 'phone', 'label' => $this->Translation->get('loginPage.phoneInputLabel')]) ?>

            </div>
            <div class="col-md-6">
                <?= $this->Form->control('email', ['type' => 'email','id'=>'register-email', 'label' => $this->Translation->get('loginPage.emailInputLabel')]) ?>
            </div>
            <div class="col-md-6">
                <?= $this->Form->control('password', ['type' => 'password','id'=>'register-password', 'required' => true, 'label' => $this->Translation->get('loginPage.passwordInputLabel')]) ?>
            </div>
            <div class="col-md-6">
                <?= $this->Form->control('repeat_password', ['type' => 'password','id'=>'register-repeat-password', 'required' => true, 'label' => $this->Translation->get('loginPage.repeatPasswordInputLabel')]) ?>
            </div>
        </div>
        <div class="text-right">
            <button type="submit" class="btn btn-outline-success" name="register"><?= $this->Translation->get('loginPage.joinButton') ?></button>
        </div>
        <?= $this->Form->end() ?>
    </div>
    <div class="col-12">
        <hr/>
    </div>
</div>