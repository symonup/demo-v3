<div class="row">
    <div class="col-12">
        <div id="chceFvCheck">
            <h5><?= $this->Translation->get('koszyk.invoiceData') ?></h5>
            <div class="form-group radio">
                <input type="radio" <?= ((empty($zamowienie['dane_do_fv']) || $zamowienie['dane_do_fv'] == 'takie_same') ? 'checked="checked"' : '') ?> name="dane_do_fv" id="dane_do_fv_1" value="takie_same"/>
                <label for="dane_do_fv_1"><?= $this->Translation->get('zamowienie.DaneDoFvTakieSameJakDoWysylki') ?></label>
            </div>
            <div class="form-group radio">
                <input type="radio" <?= ((!empty($zamowienie) && $zamowienie['dane_do_fv'] == 'inne') ? 'checked="checked"' : '') ?> name="dane_do_fv" id="dane_do_fv_2" value="inne"/>
                <label for="dane_do_fv_2"><?= $this->Translation->get('zamowienie.InneDaneDoFv') ?></label>
            </div>
        </div>
    </div>
</div>
<div class="row fv-other">
    <div class="col-md-6"><?= $this->Form->control('faktura_imie', ['label' => $this->Translation->get('uzytkownik.name'),'minlength'=>2, 'tmp-req' => 'true']) ?></div>
    <div class="col-md-6"><?= $this->Form->control('faktura_nazwisko', ['label' => $this->Translation->get('uzytkownik.lastname'),'minlength'=>2, 'tmp-req' => 'true']) ?></div>
</div>
<div class="row fv-other">
    <div class="col-md-6"><?= $this->Form->control('faktura_firma', ['label' => $this->Translation->get('uzytkownik.company'), 'tmp-req' => 'true']) ?></div>
    <div class="col-md-6"><?= $this->Form->control('faktura_nip', ['label' => $this->Translation->get('uzytkownik.nip'), 'tmp-req' => 'true']) ?></div>
</div>
<div class="row fv-other">
    <div class="col-md-6"><?= $this->Form->control('faktura_ulica', ['label' => $this->Translation->get('uzytkownik.street'), 'tmp-req' => 'true']) ?></div>
    <div class="col-md-3"><?= $this->Form->control('faktura_nr_domu', ['label' => $this->Translation->get('uzytkownik.homeNumber'), 'tmp-req' => 'true']) ?></div>
    <div class="col-md-3"><?= $this->Form->control('faktura_nr_lokalu', ['label' => $this->Translation->get('uzytkownik.premisesNumber')]) ?></div>
</div>
<div class="row fv-other">
    <div class="col-md-3"><?= $this->Form->control('faktura_kod', ['label' => $this->Translation->get('uzytkownik.postCode'), 'tmp-req' => 'true']) ?></div>
    <div class="col-md-6"><?= $this->Form->control('faktura_miasto', ['label' => $this->Translation->get('uzytkownik.city'), 'tmp-req' => 'true']) ?></div>
    <div class="col-md-3"><?= $this->Form->control('faktura_kraj', ['label' => $this->Translation->get('uzytkownik.country'), 'tmp-req' => 'true', 'type' => 'select', 'options' => $allCountry, 'default' => Cake\Core\Configure::read('adres.default_country')]) ?></div>
</div>