<div class="order-form-container form-mini <?= (!empty($_GET['order_open']) ? 'open' : '') ?>">
    <?php
//    if (empty($user)) {
//        echo $this->element('default/order/user_register');
//    }
    echo $this->Form->create($zamowienie, ['url' => ['action' => 'orderSummary'], 'id' => 'cart-form', 'on-change-action' => Cake\Routing\Router::url(['controller' => 'Cart', 'action' => 'setOrderForm'])]);
//    if (!empty($user)) {
//        echo $this->element('default/order/form_user', ['totalBrutto' => $totalBrutto]);
//    } else {

 /*   if (!empty($user) && !empty($user['bonusy_suma'])) {
        ?>
        <div class="row">
            <div class="col-12 nice-checkbox type-2 cart-bonus">
                <div class="cartSummaryHead"><?= $this->Translation->get('koszyk.TwojeBonusoweZlotowki') ?></div>
                <p class="bonus-info"><?= $this->Translation->get('koszyk.bonusoweZlotowkiInfo') ?></p>
                <div class="bonus-label"><?= $this->Translation->get('koszyk.TwojeBonusoweZlotowkiIlosc{0}', $this->Html->tag('span', $this->Txt->cena($user['bonusy_suma'], null, true))) ?></div>
                <?= $this->Form->control('bonusowe_zlotowki', ['type' => 'checkbox', 'label' => $this->Translation->get('koszyk.chceWykorzystacBonusoweZlotowki'), 'value' => $user['bonusy_suma']]) ?>
            </div>
        </div>
        <?php
    } */
    ?>
                        <div id="produkty-sum"><?=$rabatKodHtml?></div>
    <?php

    echo $this->element('default/order/form', ['totalBrutto' => $totalBrutto]);
    echo $this->Form->control('paczkomat',['type'=>'hidden']);
    echo $this->Form->control('paczkomat_info',['type'=>'hidden']);
//    }
    ?>
    <div class="row">
        <div class="col-12 col-md-7 col-xxl-6 rodzajePlatnosciContainer">
            <div class="cartSummaryHead"><?= $this->Translation->get('koszyk.Platnosc') ?></div>
            <?= $platnosciUl ?>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="cartSummaryHead"><?= $this->Translation->get('koszyk.CommentsToOrder') ?></div>
            <?= $this->Form->control('uwagi', ['type' => 'textarea', 'label' => false, 'rows' => 2]) ?>
        </div>
        <div class="col-12">
            <div class="order-checkboxes text-left nice-checkbox type-2">
                <?= $this->Form->control('regulamin', ['required' => true, 'type' => 'checkbox','checked'=>false, 'label' => ['text' => $this->Translation->get('koszyk.IAcceptTheTermsAndConditions'), 'escape' => false]]) ?>
                <?php $zgodaTekst = $this->Translation->get('koszyk.IAgreeToTheProcessingOfPersonalData'); if(!empty($zgodaTekst)){echo $this->Form->control('zgoda', ['required' => false, 'type' => 'checkbox', 'label' => ['text' => $zgodaTekst, 'escape' => false ,'class'=>'koszyk-zgoda']]);} ?>
                <?php $zgodaTekst2 = $this->Translation->get('koszyk.IAgreeToTheProcessingOfPersonalData2'); if(!empty($zgodaTekst2)){echo $this->Form->control('zgoda2', ['required' => false, 'type' => 'checkbox', 'label' => ['text' => $zgodaTekst2, 'escape' => false,'class'=>'koszyk-zgoda']]);} ?>
            </div>
            <div class="row align-items-end mt-20">
                <div class="col-12 col-md-6 text-left">
                    <div class="sumary-total-to-pay cartSummaryHead">
                        <?= $this->Translation->get('koszyk.totalToPay{0}', [$this->Html->tag('span', $this->Txt->cena($totalBrutto), ['id' => 'cart-total-to-pay'])]) ?>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <button id="cart-submit" class="btn btn-block btn-lg btn-success" type="submit"><?= $this->Translation->get('koszyk.submitOrder') ?></button>
                </div>
            </div>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>