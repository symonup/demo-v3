<?php 
?>
<tr class="cart-item zestaw-bonus <?= (!empty($item['zestaw_id']) ?'zestaw':'')?><?=((!empty($zi) && $zi==1)?' first':'')?><?=(((!empty($zestawItems) && $zi==count($zestawItems) && empty($noEdit)) || (!empty($zestawItems) && !empty($noEdit)))?' last':'')?>" <?= (!empty($item['zestaw_id']) ? ' zestaw-key="'.$item['zestaw_key'].'"' : '') ?> item-key="<?= $itemKey ?>" <?= (!empty($item['zestaw_id']) ? 'zestaw-id="' . $item['zestaw_id'] . '"' : '') ?>>
    <td class="lp"></td>
    <td class="bonus-name" colspan="<?=(!empty($noEdit)?'6':'7')?>"><?=$item['nazwa']?></td>
    <td class="text-right cartItemBrutto">
        <?php 
        $oldPrice=$zestawWartosc*$item['zestaw_ilosc'];
        $zestawValue=$zestawWartosc;
            $wartosc_zestawu=$zestawWartosc;
        if($item['zestaw_ilosc']>1){
            $zestawValue=0;
            if($item['typ']=='procent'){
                $wartosc_zestawu=$zestawWartosc-($zestawWartosc*($item['wartosc']/100));
            }
            for($tmpI=1;$tmpI<=$item['zestaw_ilosc'];$tmpI++){
                $zestawValue+=$wartosc_zestawu;
            }
        }else{
             if($item['typ']=='procent'){
                $wartosc_zestawu=$zestawWartosc-($zestawWartosc*($item['wartosc']/100));
            }
            $zestawValue=$wartosc_zestawu;
        }
        if($oldPrice > $zestawValue){
            ?>
        <span class="old-price"><?=$this->Txt->cena($oldPrice)?></span>
        <?php
        }
        ?>
        <?=$this->Txt->cena($zestawValue)?>
    </td>
</tr>