<?php 
if($mobile){
    ?>
<tr class="cart-item" <?= (!empty($item['zestaw_id']) ? ' zestaw-key="' . $item['zestaw_key'] . '"' : '') ?> item-key="<?= $itemKey ?>" <?= (!empty($item['zestaw_id']) ? 'zestaw-id="' . $item['zestaw_id'] . '"' : '') ?>>
<?php if (!empty($item['zestaw_id'])) {
        ?>
        <td class="text-left name" colspan="3"><div class="zestawName"><?= $item['zestaw_nazwa'] ?></div><?= $item['nazwa'] ?></td>
        <?php
    } elseif(!empty($item['karta_podarunkowa'])){
        ?>
        <td class="text-left name" colspan="3"><span><?= $item['nazwa'] ?></span></td>
    <?php
    }else {
        ?>
        <td class="text-left name" colspan="3"><a href="<?= \Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'view', $item['towar_id'], $this->Navi->friendlyUrl($item['nazwa'])]) ?>"><?= $item['nazwa'].(!empty($item['platforma'])?' ('.$item['platforma'].')':'') ?></a></td>
    <?php }
    ?>
</tr>
    <?php
}
?>
<tr class="cart-item" <?= (!empty($item['zestaw_id']) ? ' zestaw-key="' . $item['zestaw_key'] . '"' : '') ?> item-key="<?= $itemKey ?>" <?= (!empty($item['zestaw_id']) ? 'zestaw-id="' . $item['zestaw_id'] . '"' : '') ?>>
    <td class="cart-image <?= (!empty($item['zestaw_key']) ? 'image-zestaw' : '') ?>">
        <?php
        if (!empty($item['zestaw_key'])) {
            $zestawImages = [];
            if (!empty($item['zdjecia'])) {
                foreach ($item['zdjecia'] as $zdjecie) {
                    if (file_exists($filePath['towar'] . $this->Txt->getKatalog($zdjecie['id']) . DS . 'thumb_' . $zdjecie['plik'])) {
                        if (!empty($zdjecie['alt'])) {
                            $alt = $zdjecie['alt'];
                        }
                        $zImg = $this->Txt->towarZdjecieSrc($zdjecie['id'], $zdjecie['plik'], 'thumb_');
                        $zestawImages[] = $this->Html->image($zImg, ['alt' => false]);
                    }
                }
            }
            if (!empty($zestawImages)) {
                echo join('', $zestawImages);
            } else {
                echo $this->Html->image($displayPath['img'] . 'noPhoto.png', ['alt' => false]);
            }
        } else {
            $wersjaImgSrc = $displayPath['img'] . 'noPhoto.png';
            $alt = $item['nazwa'];
            if (!empty($item['zdjecie'])) {
                if(!empty($item['karta_podarunkowa']) && file_exists($filePath['karta_podarunkowa'] . $item['zdjecie'])){
                    $wersjaImgSrc = $displayPath['karta_podarunkowa'] . $item['zdjecie'];
                }
                elseif (file_exists($filePath['towar'] . $this->Txt->getKatalog($item['zdjecie']['id']) . DS . 'thumb_' . $item['zdjecie']['plik'])) {
                    if (!empty($item['zdjecie']['alt'])) {
                        $alt = $item['zdjecie']['alt'];
                    }
                    $wersjaImgSrc = $this->Txt->towarZdjecieSrc($item['zdjecie']['id'], $item['zdjecie']['plik'], 'thumb_');
                }
            }
            echo $this->Html->image($wersjaImgSrc, ['alt' => $alt]);
        }
        ?>
        <?php
        if($mobile){
            if (!empty($item['zestaw_id'])) {
                if ($zi == 1) {
                    if (!empty($inPreview)) {
                        ?>
            <span zestaw-key="<?= $zestawKey ?>" class="remove-from-cart"><i class="far fa-trash-alt"></i></span>
                        <?php
                    } else {
                        ?>
                        <span zestaw-key="<?= $zestawKey ?>" class="remove-from-cart" confirm-btn-yes="<?= $this->Translation->get('labels.Yes') ?>" confirm-btn-no="<?= $this->Translation->get('labels.No') ?>" confirm-message="<?= $this->Translation->get('koszyk.CzyNaPewnoChceszUsunacZestawZKoszyka') ?>"><?= $this->Translation->get('koszyk.removeZestaw') ?></span>
                        <?php
                    }
                }
            } else {
                if (!empty($inPreview)) {
                    ?>
                    <span item-key="<?= $itemKey ?>" class="remove-from-cart"><i class="far fa-trash-alt"></i></span>
                    <?php
                } else {
                    ?>
                    <span item-key="<?= $itemKey ?>" class="remove-from-cart" confirm-btn-yes="<?= $this->Translation->get('labels.Yes') ?>" confirm-btn-no="<?= $this->Translation->get('labels.No') ?>" confirm-message="<?= $this->Translation->get('koszyk.AreYouSureYouWantToDeleteTheItem') ?>"><?= $this->Translation->get('koszyk.removeItem') ?></span>
                    <?php
                }
            }
            }
            ?>
    </td>
    <?php if(!$mobile){ if (!empty($item['zestaw_id'])) {
        ?>
        <td class="text-left name"><div class="zestawName"><?= $item['zestaw_nazwa'] ?></div><?= $item['nazwa'] ?></td>
        <?php
    } elseif(!empty($item['karta_podarunkowa'])){
        ?>
        <td class="text-left name"><span><?= $item['nazwa'] ?></span></td>
    <?php
    }else {
        ?>
        <td class="text-left name"><a href="<?= \Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'view', $item['towar_id'], $this->Navi->friendlyUrl($item['nazwa'])]) ?>"><?= $item['nazwa'].(!empty($item['platforma'])?' ('.$item['platforma'].')':'') ?></a></td>
    <?php }
    }
    ?>
    <td class="text-center tdIlosc">
        <?php
        if (!empty($noEdit) || !empty($item['gratis'])) {
            echo $item['ilosc'].(!empty($item['jednostka_str'])?' '.$item['jednostka_str']:'');
        } else {
            ?>
            <div class="view-ilosc">
                <?php
                if (!empty($item['zestaw_id'])) {
                    if ($zi == 1) {
                        ?>
                        <input type="text" value="<?= $item['ilosc'] ?>" class="cart-ordered-box item-ilosc-input" zestaw-key="<?= $zestawKey ?>" item-id="<?= $item['towar_id'] ?>" version-id="" item-price="<?= $item['cena_za_sztuke'] ?>" in-box="1"/>

                        <?php
                    }
                } else {
                    ?>
                        <input type="text" value="<?= $item['ilosc'] ?>" class="cart-ordered-box item-ilosc-input" item-key="<?= $itemKey ?>" item-id="<?= (!empty($item['karta_podarunkowa'])?$item['karta_podarunkowa_id']:$item['towar_id']) ?>" version-id="<?= (!empty($item['wariant_id']) ? $item['wariant_id'] : '') ?>" item-price="<?= $item['cena_za_sztuke'] ?>" in-box="1"/>
                    <?php
                }
                ?>
            </div>
            <?php if (empty($inPreview)) { ?><span class="cart-label"><?= $this->Translation->get('labels.ilosc') ?></span><?php } ?>
        <?php } ?>
    </td>
    <?php
    $razem = $item['ilosc'] * (key_exists('cena_z_rabatem', $item) ? $item['cena_z_rabatem'] : $item['cena_za_sztuke_brutto']);
    ?>
    <td class="text-center">
        <div class="cartItemBrutto"><?= (!empty($item['gratis'])?t('koszyk.gratis'):$this->Txt->cena($razem)) ?></div>
        <?php if (empty($inPreview) && empty($item['gratis'])) { ?><span class="cart-label"><?= $this->Translation->get('labels.cena') ?></span><?php } ?>
    </td>
    <?php
    if (!empty($noEdit)) {
        
    } elseif(!$mobile) {
        ?>
        <td class="tdTrash text-right">  <?php
            if (!empty($item['zestaw_id'])) {
                if ($zi == 1) {
                    if (!empty($inPreview)) {
                        ?>
            <span zestaw-key="<?= $zestawKey ?>" class="remove-from-cart"><i class="far fa-trash-alt"></i></span>
                        <?php
                    } else {
                        ?>
                        <span zestaw-key="<?= $zestawKey ?>" class="remove-from-cart" confirm-btn-yes="<?= $this->Translation->get('labels.Yes') ?>" confirm-btn-no="<?= $this->Translation->get('labels.No') ?>" confirm-message="<?= $this->Translation->get('koszyk.CzyNaPewnoChceszUsunacZestawZKoszyka') ?>"><?= $this->Translation->get('koszyk.removeZestaw') ?></span>
                        <?php
                    }
                }
            } else {
                if (!empty($inPreview)) {
                    ?>
                    <span item-key="<?= $itemKey ?>" class="remove-from-cart"><i class="far fa-trash-alt"></i></span>
                    <?php
                } else {
                    ?>
                    <span item-key="<?= $itemKey ?>" class="remove-from-cart" confirm-btn-yes="<?= $this->Translation->get('labels.Yes') ?>" confirm-btn-no="<?= $this->Translation->get('labels.No') ?>" confirm-message="<?= $this->Translation->get('koszyk.AreYouSureYouWantToDeleteTheItem') ?>"><?= $this->Translation->get('koszyk.removeItem') ?></span>
                    <?php
                }
            }
            ?></td>
        <?php
    }
    ?>
</tr>