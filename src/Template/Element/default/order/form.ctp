<div class="row justify-content-center">
    <div class="col-12 nice-checkbox type-2">
        <div class="cartSummaryHead hide-on-odbior"><?= $this->Translation->get('koszyk.deliveryAddress') ?></div>
        <div class="cartSummaryHead show-on-odbior hidden"><?= $this->Translation->get('koszyk.twojeDane') ?></div>
        <?= $this->element('default/order/base_wysylka_form_order') ?>
        <?= $this->Form->control('faktura', ['label' => $this->Translation->get('zamowienie.ChceOtrzymacFakture'), 'type' => 'checkbox']) ?>
    </div>
    <div class="col-12">
        <?= $this->element('default/order/base_faktura_form_order') ?>
    </div>
</div>