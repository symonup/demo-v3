<?php 
$invoiceData='';
if (!empty($uzytkownik->uzytkownik_adres)) {
            $invoiceData = $this->Txt->printUserInfo($uzytkownik->uzytkownik_adres[0]->toArray(), '<br/>');
            $invoiceData .= (!empty($invoiceData) ? '<br/>' : '') . $this->Txt->printAddres($uzytkownik->uzytkownik_adres[0]->toArray(), '<br/>');
        }
?>
<div class="row justify-content-center">
    <div class="col-12 col-lg-<?=((!empty($deliveryAdressOpitons) && !empty($invoiceData))?4:8)?>">
        <h5><?= $this->Translation->get('koszyk.deliveryAddress') ?></h5>
        <?php
        if (empty($deliveryAdressOpitons)) {
            echo $this->element('default/order/base_wysylka_form');
        } else {
            echo $this->Form->control('adres_wysylki', ['label' => false, 'type' => 'select', 'options' => $deliveryAdressOpitons]);
            $i = 0;
            foreach ($deliveryAdressLabels as $addressId => $adresLabel) {
                ?>
                <div class="address-label <?= (($i > 0) ? 'hidden' : '') ?>" address-id="<?= $addressId ?>">
                    <?= $adresLabel ?>
                </div>
                <?php
                $i++;
            }
        }
        ?>
        <?= $this->Form->control('faktura', ['label' => $this->Translation->get('zamowienie.ChceOtrzymacFakture'), 'type' => 'checkbox']) ?>
    </div>
    <div class="col-12 col-lg-<?=((!empty($deliveryAdressOpitons) && !empty($invoiceData))?4:8)?>">
        <?php
        if (!empty($invoiceData)) {
            ?>
        <div id="chceFvCheck">
            <h5><?= $this->Translation->get('koszyk.invoiceData') ?></h5>
            <div class="invoice-data-label"><?= $invoiceData ?></div>
        </div>
            <?php
        } else {
            echo $this->element('default/order/base_faktura_form');
        }
        ?>
    </div>
</div>
