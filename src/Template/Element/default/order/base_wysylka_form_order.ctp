<div class="row">
    <div class="col-12 md-child-cols-3">
     <?= $this->Form->control('imie', ['label'=>false,'placeholder' => $this->Translation->get('uzytkownik.name'),'minlength'=>2]) ?>
    <?= $this->Form->control('nazwisko', ['label'=>false,'placeholder' => $this->Translation->get('uzytkownik.lastName'),'minlength'=>2]) ?>
    <?= $this->Form->control('telefon', ['label'=>false,'type' => 'text','required'=>true, 'data-type' => 'phone', 'placeholder' => $this->Translation->get('uzytkownik.phone')]) ?>
    </div>
     
</div>
<div class="row hide-on-odbior">
    <div class="col-12 md-child-cols-3">
    <?= $this->Form->control('wysylka_kod', ['label'=>false,'placeholder' => $this->Translation->get('uzytkownik.postCode'),'disable-on-odbior'=>'true','paczkomat-not-required'=>'true','required'=>true]) ?>
    <?= $this->Form->control('wysylka_miasto', ['label'=>false,'placeholder' => $this->Translation->get('uzytkownik.city'),'disable-on-odbior'=>'true','paczkomat-not-required'=>'true','required'=>true]) ?>
    <?= $this->Form->control('wysylka_ulica', ['label'=>false,'placeholder' => $this->Translation->get('uzytkownik.Adres'),'disable-on-odbior'=>'true','paczkomat-not-required'=>'true','required'=>true]) ?>
    </div>
</div>
<div class="row">
    <div class="col-12 md-child-cols-3">
    <?= $this->Form->control('wysylka_firma', ['label'=>false,'placeholder' => $this->Translation->get('uzytkownik.Firma'),'required'=>false]) ?>
    <?= $this->Form->control('email', ['label'=>false,'placeholder' => $this->Translation->get('uzytkownik.email'),'readonly'=>(!empty($user))]) ?>
    <?= $this->Form->control('wysylka_kraj', ['label'=>false,'empty' => $this->Translation->get('uzytkownik.country'),'hide-on-odbior'=>'true','disable-on-odbior'=>'true', 'type' => 'select','paczkomat-not-required'=>'true', 'options' => Cake\Core\Configure::read('kraje'), 'default' => Cake\Core\Configure::read('adres.default_country'),'required'=>true]) ?>
    </div>
</div>