<?php
$defFormTemplate = [
    'inputContainer' => '<div class="form-group {{type}}{{required}}">{{content}}</div>',
    'inputContainerError' => '<div class="form-group {{type}}{{required}} error">{{content}}</div>',
    'formGroup' => '{{label}}{{error}}{{input}}',
    'error' => '<span class="error invalid-feedback">{{content}}</span>',
    'submitContainer' => '<div class="form-group submit">{{content}}</div>',
    'input' => '<input type="{{type}}" class="form-control" name="{{name}}"{{attrs}}/>',
    'textarea' => '<textarea class="form-control" name="{{name}}"{{attrs}}>{{value}}</textarea>',
    'select' => '<select class="form-control" name="{{name}}"{{attrs}}>{{content}}</select>',
    'selectMultiple' => '<select class="form-control" name="{{name}}[]" multiple="multiple"{{attrs}}>{{content}}</select>',
    'file' => '<div class="file-add"><input type="file" name="{{name}}"{{attrs}}><button type="button" class="btn btn-sm btn-info"><i class="fa fa-plus"></i> _FILE_ADD_LABEL_</button><div class="upload-file-preview"><img src="_FILE_SRC_"/></div></div>',
    'checkbox' => '<input type="checkbox" name="{{name}}" value="{{value}}"{{attrs}}>',
    'checkboxFormGroup' => '{{label}}',
    'checkboxWrapper' => '<div class="checkbox">{{label}}</div>',
    'nestingLabel' => '{{hidden}}{{input}}<label{{attrs}}>{{text}}</label>'
];
$this->Form->setTemplates($defFormTemplate);
$wysylkiaList = '';
$min_w_price = -1;
$min_w_id = 0;
foreach ($wysylkiPrice as $wId => $wPrice) {
    if ($min_w_price < 0) {
        $min_w_price = $wPrice;
        $min_w_id = $wId;
    }
    if ($wPrice < $min_w_price) {
        $min_w_price = $wPrice;
        $min_w_id = $wId;
    }
}
$wysylkaKosztDotatkowy = [];
$paczkomat = null;
$paczkomatSelected = false;
$punktyOdbioru = [];
foreach ($wysylki as $wysylka) {
    $class = '';
    if (empty($min_w_id)) {
        $min_w_id = $wysylka->id;
    }
    if (!empty($wysylka->koszt_dodatkowy)) {
        $wysylkaKosztDotatkowy[$wysylka->id] = $wysylka->koszt_dodatkowy;
    }
    foreach ($wysylka->wysylka_platnosc as $wysylka_platnosc) {
        $class .= (!empty($class) ? ' ' : '') . 'platnosc-' . $wysylka_platnosc->rodzaj_platnosci_id;
    }
    $wChecked = ((count($wysylki) == 1) ? true : ((empty($platnoscChecked)) ? (($min_w_id == $wysylka->id) ? true : false) : (($platnoscChecked['wysylka'] == $wysylka->id) ? true : false)));
    if (!empty($wysylka->paczkomat)) {
        $paczkomat = true;
        if ($wChecked) {
            $paczkomatSelected = $wysylka->id;
        }
    }
    if (!empty($wysylka->punkty_odbioru)) {
        foreach ($wysylka->punkty_odbioru as $punktOdbioru) {
            $punktyOdbioru[$wysylka->id][$punktOdbioru->id] = $this->Html->tag('div', $this->Html->tag('input','', ['name'=>'punkt_odbioru_id','type' => 'radio', 'id' => 'punkt-odbioru-' . $wysylka->id . '-' . $punktOdbioru->id, 'value' => $punktOdbioru->id]).$this->Html->tag('label','<div class="adres">' . nl2br($punktOdbioru->adres) . '</div>',['for'=>'punkt-odbioru-' . $wysylka->id . '-' . $punktOdbioru->id]), ['class' => 'punkt_odbioru nice-radiobutton']);
        }
    }
    $kosztyKraj=(!empty($wysylka->elektroniczna)?true:(!empty($wysylka->koszty_kraj)));
    $wysylkiaList .= $this->Html->tag('li', $this->Html->tag('input', '', ['disable-for-kraj'=>(!$kosztyKraj?'true':null),'type' => 'radio','e-wysylka'=>(!empty($wysylka->elektroniczna)?'true':'false'), 'paczkomat' => (!empty($wysylka->paczkomat) ? 'true' : 'false'), 'value' => $wysylka->id, 'checked' => $wChecked, 'price' => (!empty($wysylkiPrice[$wysylka->id]) ? $wysylkiPrice[$wysylka->id] : 0), 'def-price' => (!empty($wysylkiPrice[$wysylka->id]) ? $wysylkiPrice[$wysylka->id] : 0), 'name' => 'wysylka', 'id' => 'wysylka-' . $wysylka->id]) . $this->Html->tag('label', $wysylka->nazwa . ' ' . $this->Html->tag('span', $this->Txt->cena((!empty($wysylkiPrice[$wysylka->id])?$wysylkiPrice[$wysylka->id]:0), null, true), ['class' => 'platnosc-cena-label']), ['for' => 'wysylka-' . $wysylka->id])
            , ['class' => $class.(!$kosztyKraj?' hide-for-kraj':''), 'id' => 'li_wysylka-' . $wysylka->id]);
}
$wysylkaUl = $this->Html->tag('ul', $wysylkiaList, ['class' => 'dostawa nice-radiobutton']);
$platnosciList = '';
$selectedW = !empty($platnoscChecked) ? $platnoscChecked['wysylka'] : $min_w_id;
$min_p_price = -1;
$min_p_id = 0;
foreach ($rodzaje_platnosci as $rodzaj_platnosci) {
    if ($min_p_price < 0) {
        $min_p_price = $rodzaj_platnosci->koszt_dodatkowy;
        $min_p_id = $rodzaj_platnosci->id;
    }
    if ($rodzaj_platnosci->koszt_dodatkowy < $min_p_price) {
        $min_p_price = $rodzaj_platnosci->koszt_dodatkowy;
        $min_p_id = $rodzaj_platnosci->id;
    }
}
$prowizje = [];
foreach ($rodzaje_platnosci as $rodzaj_platnosci) {
    $prowizje[$rodzaj_platnosci->id] = (empty($rodzaj_platnosci->prowizja) ? 0 : $rodzaj_platnosci->prowizja / 100);
    $class = '';
    foreach ($rodzaj_platnosci->wysylka_platnosc as $wysylka_platnosc) {
        $class .= (!empty($class) ? ' ' : '') . 'wysylka-' . $wysylka_platnosc->wysylka_id;
    }
    $disabledKraje=(!empty($rodzaj_platnosci->disabled_kraj));
    $platnosciList .= $this->Html->tag('li', $this->Html->tag('input', '', ['disable-for-kraj'=>($disabledKraje?'true':null),'type' => 'radio', 'value' => $rodzaj_platnosci->id, 'checked' => ((empty($platnoscChecked)) ? (($min_p_id == $rodzaj_platnosci->id) ? true : false) : (($platnoscChecked['rodzaj'] == $rodzaj_platnosci->id) ? true : false)), 'koszt-dodatkowy' => ((!empty($rodzaj_platnosci->koszt_dodatkowy)) ? $rodzaj_platnosci->koszt_dodatkowy : 0), 'darmowa-wysylka' => $rodzaj_platnosci->darmowa_wysylka, 'price' => ((!empty($rodzaj_platnosci->koszt_dodatkowy) && key_exists($selectedW, $wysylkaKosztDotatkowy)) ? $wysylkaKosztDotatkowy[$selectedW] : 0), 'name' => 'platnosc', 'id' => 'platnosc-' . $rodzaj_platnosci->id]) . $this->Html->tag('label', $rodzaj_platnosci->nazwa . $this->Html->tag('span', $this->Txt->cena(((!empty($rodzaj_platnosci->koszt_dodatkowy) && key_exists($selectedW, $wysylkaKosztDotatkowy)) ? $wysylkaKosztDotatkowy[$selectedW] : 0), null, true), ['class' => 'platnosc-cena-label']), ['for' => 'platnosc-' . $rodzaj_platnosci->id])
            , ['class' => $class.($disabledKraje?' hide-for-kraj':''), 'id' => 'li_platnosc-' . $rodzaj_platnosci->id]);
}
$platnosciUl = $this->Html->tag('ul', $platnosciList, ['class' => 'platnosc nice-radiobutton']);
?>
<div class="row">
    <div class="col-12 col-md-6 rodzajePlatnosciContainer">
        <div class="cartSummaryHead"><?= $this->Translation->get('koszyk.Wysylka') ?></div>
        <?= $wysylkaUl ?>
    </div><?php if (!empty($paczkomat) || !empty($punktyOdbioru)) { ?>
        <div class="col-12 col-md-6">
            <?php if (!empty($paczkomat)) { ?>
                <div id="paczkomat-container" class="<?= (!empty($paczkomatSelected) ? 'open' : '') ?>">
                    <div class="cartSummaryHead"><?= $this->Translation->get('koszyk.wybierzPaczkomat') ?></div>
                    <div id="paczkomat-name"><?php
                        if (!empty($zamowienie) && !empty($zamowienie['paczkomat'])) {
                            echo $zamowienie['paczkomat_info'];
                        }
                        ?></div>
                    <button type="button" class="btn btn-sm btn-link" href="<?= Cake\Routing\Router::url(['controller' => 'Cart', 'action' => 'getPaczkomaty']) ?>" id="get-inpost-map"><?= $this->Translation->get('koszyk.wybierzPaczkomatLink') ?></button>
                </div>
            <?php
            }
            if (!empty($punktyOdbioru)) {
                ?>
                <div id="punkty-odbioru-container">
                    <div class="cartSummaryHead"><?=$this->Translation->get('koszyk.punktOdbioru')?></div>
                    <?php 
                                    foreach ($punktyOdbioru as $punktWysylkaId => $punktyOptions){
                                        echo $this->Html->tag('div', join('', $punktyOptions),['id'=>'punkt-container-'.$punktWysylkaId,'class'=>'punkt-container'.(($selectedW==$punktWysylkaId)?' open':'')]);
                                    }
                    ?>
                </div>
        <?php
    }
    ?>
        </div><?php } ?>
    <div class="col-12">
<?= $this->element('default/order/order_form', ['totalBrutto' => $totalBrutto, 'platnosciUl' => $platnosciUl]) ?>
    </div>
</div>
<script type="text/javascript">
    kosztyDodatkowe =<?php echo json_encode($wysylkaKosztDotatkowy); ?>;
</script>