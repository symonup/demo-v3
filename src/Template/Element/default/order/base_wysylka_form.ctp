<div class="row">
    <div class="col-md-6"><?= $this->Form->control('imie', ['label' => $this->Translation->get('uzytkownik.name'),'minlength'=>2,'value'=>(!empty($zamowienie['imie'])?$zamowienie['imie']:(!empty($user)?$user['imie']:''))]) ?></div>
    <div class="col-md-6"><?= $this->Form->control('nazwisko', ['label' => $this->Translation->get('uzytkownik.lastName'),'minlength'=>2,'value'=>(!empty($zamowienie['nazwisko'])?$zamowienie['nazwisko']:(!empty($user)?$user['nazwisko']:''))]) ?></div>
</div>
<div class="row">
    <div class="col-md-6">
        <?= $this->Form->control('telefon', ['type' => 'text','value'=>(!empty($zamowienie['telefon'])?$zamowienie['telefon']:(!empty($user)?$user['telefon']:'')), 'data-type' => 'phone', 'label' => $this->Translation->get('uzytkownik.phone')]) ?>
    </div>
    <div class="col-md-6"><?= $this->Form->control('email', ['label' => $this->Translation->get('uzytkownik.email'),'value'=>(!empty($user)?$user['email']:(!empty($zamowienie['email'])?$zamowienie['email']:'')),'readonly'=>(!empty($user))]) ?></div>
</div>
<div class="row">
    <div class="col-md-6"><?= $this->Form->control('wysylka_ulica', ['label' => $this->Translation->get('uzytkownik.street'),'required'=>true]) ?></div>
    <div class="col-md-3"><?= $this->Form->control('wysylka_nr_domu', ['label' => $this->Translation->get('uzytkownik.homeNumber'),'required'=>true]) ?></div>
    <div class="col-md-3"><?= $this->Form->control('wysylka_nr_lokalu', ['label' => $this->Translation->get('uzytkownik.premisesNumber')]) ?></div>
</div>
<div class="row">
    <div class="col-md-3"><?= $this->Form->control('wysylka_kod', ['label' => $this->Translation->get('uzytkownik.postCode'),'required'=>true]) ?></div>
    <div class="col-md-6"><?= $this->Form->control('wysylka_miasto', ['label' => $this->Translation->get('uzytkownik.city'),'required'=>true]) ?></div>
    <div class="col-md-3"><?= $this->Form->control('wysylka_kraj', ['label' => $this->Translation->get('uzytkownik.country'), 'type' => 'select', 'options' => $allCountry, 'default' => Cake\Core\Configure::read('adres.default_country'),'required'=>true]) ?></div>
</div>