<?php
$active = (int) $this->Paginator->current();
$all = (int) $this->Paginator->counter([
            'format' => '{{pages}}'
        ]);
if ($all > 1) {
if(!empty($this->request->params['type'])){
    $this->Paginator->options([
    'url' => [
        'controller'=>$this->request->params['controller'],'action'=>$this->request->params['action'],'type'=>$this->request->params['type']
            ]
    ]);
}
    ?>
            <ul class="pagination">
                <?php
                $curr = $this->Paginator->current();
                $total = $this->Paginator->total();
                if($curr>1){
                echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', ['escape' => false]);
                } 
                if ($curr > 3 && $total > 5) { ?>
                    <?= $this->Paginator->first(__('1')) ?>
                <?php } if ($curr > 4 && $total > 5) {
                    ?>
                    <li><span>...</span></li>
                <?php }
                ?>
                <li><?= $this->Paginator->numbers(['modulus' => 4]) ?></li>
                <?php
                if ($curr < ($total - 3) && $total > 5) {
                    ?>
                    <li><span>...</span></li>
                    <?php
                }
                if ($curr < ($total - 2) && $total > 5) {
                    echo $this->Paginator->last((string) $total);
                }
                if($curr<$total) {
                    echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', ['escape' => false]);
                }
                ?>
            </ul>
    <?php
}
?>
