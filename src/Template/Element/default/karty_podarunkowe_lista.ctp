<div class="row">
    <?php
    if (!$mobile) {
        foreach ($kartyPodarunkowe as $karta) {
            ?>
            <div class="col-12 col-md-6 col-lg-3">
                <?= $this->element('default/karta_podarunkowa', ['karta' => $karta]) ?>
            </div>
        <?php
        }
    } else {
        $kartyItems = [];
        foreach ($kartyPodarunkowe as $karta) {
            $kartyItems[] = $this->element('default/karta_podarunkowa', ['karta' => $karta]);
        }
        ?><div class="col-12">
        <?= $this->element('default/custom_slider', ['items' => $kartyItems]) ?>
        </div>
<?php } ?>
</div>