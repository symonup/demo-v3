<?php
if(!$mobile){
if (!empty($banner)) {
    $bannerId = 'banner-' . $banner->id;
    if (!empty($banner->banner_slides)) {
        ?>
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-12 col-md-12 col-lg-12 parent-banner">
                    <div id="<?= $bannerId ?>" class="carousel slide banner-container" data-ride="carousel" style="<?= (!empty($banner->css) ? str_replace(["\r", "\n", "\t"], ['', '', ''], $banner->css) : '') ?>">
                        <?php
                        $i = 0;
                        $bannerNames = [];
                        foreach ($banner->banner_slides as $index => $slide) {
                            if(empty($liveEdit) && !empty($slide->is_linked) && !empty($slide->link)){
                                $bannerItems[] = '<a def-width="' . ($slide->width) . '" def-height="' . ($slide->height) . '" href="'.$slide->link.'" title="'.$slide->title.'" target="'.$slide->target.'" class="carousel-item ' . (($i == 0) ? ' active' : '') . '" style="height: ' . ($slide->height) . 'px; ' . (($slide->type == 'file') ? "background-image: url('/files/banner_slides/" . $slide->file . "');" : '') . ' ' . (!empty($slide->css) ? str_replace(["\r", "\n", "\t"], ['', '', ''], $slide->css) : '') . '">
                                ' . (!empty($liveEdit) ? '<img src="/img/blank_pixel.png" liveEdit="true" width="100%" height="' . $slide->height . '"/>' : '') . '
                                ' . $this->Banner->elements($slide) . '
                            </a>';
                            }else{
                            $bannerItems[] = '<div def-width="' . ($slide->width) . '" def-height="' . ($slide->height) . '" class="carousel-item ' . (($i == 0) ? ' active' : '') . '" style="height: ' . ($slide->height) . 'px; ' . (($slide->type == 'file') ? "background-image: url('/files/banner_slides/" . $slide->file . "');" : '') . ' ' . (!empty($slide->css) ? str_replace(["\r", "\n", "\t"], ['', '', ''], $slide->css) : '') . '">
                                ' . (!empty($liveEdit) ? '<img src="/img/blank_pixel.png" liveEdit="true" width="100%" height="' . $slide->height . '"/>' : '') . '
                                ' . $this->Banner->elements($slide) . '
                            </div>';
                            }
                            $bannerNames[$i] = $slide->title;
                            $i++;
                        }
                        ?>
                        <?php if (count($bannerNames) > 1) { ?>
                            <ol class="carousel-indicators">
                                <?php
                                foreach ($bannerNames as $iN => $slideName) {
                                    ?>
                                <li data-target="#<?= $bannerId ?>" data-slide-to="<?= $iN ?>" class="<?= (($iN == 0) ? 'active' : '') ?>"><?php /*<span><?=$slideName?></span>*/ ?></li>
                                    <?php
                                }
                                ?>
                            </ol>
                        <?php } ?>
                        <div class="carousel-inner" role="listbox">
                            <?= join('', $bannerItems) ?>
                        </div>

                        <?php if (count($banner->banner_slides) > 1) { ?>
                            <!-- Controls -->
                            <a class="left carousel-control no-shadow" href="#<?= $bannerId ?>" role="button" data-slide="prev">
                                <span class="carusel-control-img carusel-control-img-prev" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control no-shadow" href="#<?= $bannerId ?>" role="button" data-slide="next">
                                <span class="carusel-control-img carusel-control-img-next" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}
}
?>
