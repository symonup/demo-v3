<?php
$htmlHours = '<span>0</span><span>0</span>';
$htmlMinutes = '<span>0</span><span>0</span>';
$htmlSeconds = '<span>0</span><span>0</span>';
$htmlDays='';
if (strtotime($endTime) > time()) {
    $dateDiff = $this->Txt->datetimeDiff(date('Y-m-d H:i:s'), $endTime);
    $seconds = (string) $dateDiff->sec;
    $minutes = (string) $dateDiff->min;
    $hours = (string) $dateDiff->hour;
    $days = (string) $dateDiff->day;
    if ($days > 4 && !empty($allowDays)) {
        for($di=0;$di<strlen($days);$di++){
            $htmlDays .= '<span>' . $days[$di] . '</span>';
        }
    }elseif($days>0){
        $dayHours = $days * 24;
        $hours = (string) ($hours + $dayHours);
    }
    if ($hours > 0) {
        if (strlen($hours) > 1) {
            if (strlen($hours) > 2) {
                $htmlHours = '<span>' . $hours[0] . '</span><span>' . $hours[1] . '</span><span>' . $hours[2] . '</span>';
            } else {
                $htmlHours = '<span>' . $hours[0] . '</span><span>' . $hours[1] . '</span>';
            }
        } else {
            $htmlHours = '<span>0</span><span>' . $hours . '</span>';
        }
    }
    if ($minutes > 0) {
        if (strlen($minutes) > 1) {
            $htmlMinutes = '<span>' . $minutes[0] . '</span><span>' . $minutes[1] . '</span>';
        } else {
            $htmlMinutes = '<span>0</span><span>' . $minutes . '</span>';
        }
    }
    if ($seconds > 0) {
        if (strlen($seconds) > 1) {
            $htmlSeconds = '<span>' . $seconds[0] . '</span><span>' . $seconds[0] . '</span>';
        } else {
            $htmlSeconds = '<span>0</span><span>' . $seconds . '</span>';
        }
    }
}
$dLabel=(!empty($dLabel)?$dLabel:$this->Translation->get('timeCounter.dni'));
$hLabel=(!empty($hLabel)?$hLabel:$this->Translation->get('timeCounter.godzin'));
$mLabel=(!empty($mLabel)?$mLabel:$this->Translation->get('timeCounter.minut'));
$sLabel=(!empty($sLabel)?$sLabel:$this->Translation->get('timeCounter.sekund'));
$counterItems=[];
if(!empty($htmlDays)){
    $counterItems[]=$this->Html->tag('div',$htmlDays.(!empty($hideLabels)?'':$this->Html->tag('div',$dLabel,['class'=>'time-label'])),['class'=>'days']);
}
$counterItems[]=$this->Html->tag('div',$htmlHours.(!empty($hideLabels)?'':$this->Html->tag('div',$hLabel,['class'=>'time-label'])),['class'=>'hours']);
$counterItems[]=$this->Html->tag('div',$htmlMinutes.(!empty($hideLabels)?'':$this->Html->tag('div',$mLabel,['class'=>'time-label'])),['class'=>'minutes']);
$counterItems[]=$this->Html->tag('div',$htmlSeconds.(!empty($hideLabels)?'':$this->Html->tag('div',$sLabel,['class'=>'time-label'])),['class'=>'seconds']);
?>
<div id="<?= !empty($timeCounterId) ? $timeCounterId : uniqid() ?>" <?=(!empty($pageReload)?'page-reload="'.$pageReload.'"':'')?> allow-days="<?=(!empty($allowDays)?'true':'false')?>" class="time-counter" count-to="<?= $endTime ?>" hide-labels="<?=(!empty($hideLabels)?'true':'false')?>" label-d="<?=$dLabel?>" label-h="<?=$hLabel?>" label-m="<?=$mLabel?>" label-s="<?=$sLabel?>"><?= join('', $counterItems)?></div>