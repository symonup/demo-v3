<div class="col-12 col-lg-5 text-right lg-mb--22">
    <div class="licznik-wysylki">
        <div>
            <div class="licznik-wysylki-ico">
                <i class="fas fa-3x fa-birthday-cake v-align-m mr-10"></i> <?= $this->Translation->get('towar_view.premieraZa') ?>
            </div>
            <?= $this->element('default/time_counter', ['endTime' => $towar->data_premiery->format('Y-m-d 00:00:00'),'allowDays'=>true, 'timeCounterId' => 'wysylka-time']) ?>
        </div>
        <div class="licznik-wysylki-info padding-left-0"><?= $this->Translation->get('towar_view.premieraZaInfo', ['data_premiery'=>$towar->data_premiery->format('Y-m-d')], true) ?></div>
    </div>
</div>