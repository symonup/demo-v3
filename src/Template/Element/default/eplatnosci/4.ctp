<?php
$allInVal = [
    'p24_merchant_id' => $zamowienie->platnosc[0]->rodzaj_platnosci->konfig2,
    'p24_pos_id' => $zamowienie->platnosc[0]->rodzaj_platnosci->konfig3,
    'p24_session_id' => $zamowienie->platnosc[0]->token,
    'p24_amount' => ($zamowienie->platnosc[0]->kwota * 100),
    'p24_currency' => (!empty($zamowienie->platnosc[0]->waluta) ? $zamowienie->platnosc[0]->waluta->symbol : 'PLN'),
    'p24_description' => $zamowienie->platnosc[0]->opis_lng,
    'p24_email' => $zamowienie->email,
    'p24_client' => $zamowienie->imie . ' ' . $zamowienie->nazwisko,
    'p24_country' => 'pl',
    'p24_url_return' => Cake\Routing\Router::url(['controller' => 'Cart', 'action' => 'orderPay', $zamowienie->platnosc[0]->token], true),
    'p24_url_status' => Cake\Routing\Router::url(['controller' => 'Cart', 'action' => 'payment', 4, $zamowienie->platnosc[0]->token], true),
    'p24_api_version' => $zamowienie->platnosc[0]->rodzaj_platnosci->konfig4
];
?>
<form id="dotpayForm" <?= (!empty($zamowienie->platnosc[0]->rodzaj_platnosci->auto_submit) ? 'class="form-auto-submit"' : '') ?> method="post" action="<?= (!empty($zamowienie->platnosc[0]->rodzaj_platnosci->test) ? 'https://sandbox.przelewy24.pl/trnDirect' : 'https://secure.przelewy24.pl/trnRegister') ?>">
    <?php
    foreach ($allInVal as $name => $value) {
        echo '<input type="hidden" value="' . $value . '" name="' . $name . '"/>';
    }
    ?>
    <input type="hidden" value="<?= hash('md5', $allInVal['p24_session_id'] . '|' . $allInVal['p24_merchant_id'] . '|' . $allInVal['p24_amount'] . '|' . $allInVal['p24_currency'] . '|' . $zamowienie->platnosc[0]->rodzaj_platnosci->konfig1) ?>" name="p24_sign"/>
    <button type="submit" class="btn btn-lg btn-danger btn-zobacz"><?= $this->Translation->get('platnosc.Zaplac') ?></button>
</form>