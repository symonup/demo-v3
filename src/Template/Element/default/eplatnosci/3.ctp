<?php 
$allInVal=[
'api_version'=>$zamowienie->platnosc[0]->rodzaj_platnosci->konfig3,
'lang'=>(!empty($zamowienie->lng)?(($zamowienie->lng!='pl_PL')?'en':'pl'):'pl'),
'id'=>$zamowienie->platnosc[0]->rodzaj_platnosci->konfig2,
'amount'=>round($zamowienie->platnosc[0]->kwota,2),
'currency'=>(!empty($zamowienie->platnosc[0]->waluta)?$zamowienie->platnosc[0]->waluta->symbol:'PLN'),
'description'=>$zamowienie->platnosc[0]->opis_lng,
'control'=>$zamowienie->platnosc[0]->token,
//'channel'=>$zamowienie->dotpay,
'URL'=>Cake\Routing\Router::url(['controller'=>'Cart','action'=>'orderPay',$zamowienie->platnosc[0]->token],true),
'type'=>'0',
'URLC'=>Cake\Routing\Router::url(['controller'=>'Cart','action'=>'payment',3,$zamowienie->platnosc[0]->token],true),
'firstname'=>$zamowienie->imie,
'lastname'=>$zamowienie->nazwisko,
'email'=>$zamowienie->email,
//'street'=>$zamowienie->dostawa_ulica,
//'street_n1'=>$zamowienie->dostawa_nr_domu,
//    'street_n2'=>$zamowienie->dostawa_nr_lokalu,
//'city'=>$zamowienie->dostawa_miasto,
//'postcode'=>$zamowienie->dostawa_kod,
//'phone'=>$zamowienie->telefon
        ]; 
if(empty($zamowienie->dotpay)){
    unset($allInVal['channel']);
}
?>
<form id="dotpayForm" class="text-center<?=(!empty($zamowienie->platnosc[0]->rodzaj_platnosci->auto_submit)?' form-auto-submit':'') ?>" method="post" action="<?=(!empty($zamowienie->platnosc[0]->rodzaj_platnosci->test)?'https://ssl.dotpay.pl/test_payment/':'https://ssl.dotpay.pl/t2/')?>">
<?php 
foreach ($allInVal as $name => $value){
    echo '<input type="hidden" value="'.$value.'" name="'.$name.'"/>';
}
?>
<input type="hidden" value="<?= hash('sha256', $zamowienie->platnosc[0]->rodzaj_platnosci->konfig1.join('',$allInVal))?>" name="chk"/>
<button type="submit" class="btn btn-lg btn-danger btn-zobacz"><?=$this->Translation->get('platnosc.Zaplac')?></button>
</form>