<?php
$allInVal = [
    'customerIp' => $_SERVER['REMOTE_ADDR'],
    'extOrderId'=>$zamowienie->platnosc[0]->token,
    'merchantPosId' => $zamowienie->platnosc[0]->rodzaj_platnosci->konfig1,
    'description' => $zamowienie->platnosc[0]->opis_lng,
    'totalAmount' => ($zamowienie->platnosc[0]->kwota * 100),
    'currencyCode' => (!empty($zamowienie->platnosc[0]->waluta) ? $zamowienie->platnosc[0]->waluta->symbol : 'PLN'),
    'continueUrl' => Cake\Routing\Router::url(['controller' => 'Cart', 'action' => 'orderPay', $zamowienie->platnosc[0]->token], true),
    'notifyUrl' => Cake\Routing\Router::url(['controller' => 'Cart', 'action' => 'payment', 4, $zamowienie->platnosc[0]->token], true)
];
?>
<form id="dotpayForm" <?= (!empty($zamowienie->platnosc[0]->rodzaj_platnosci->auto_submit) ? 'class="form-auto-submit"' : '') ?> method="post" action="<?= (!empty($zamowienie->platnosc[0]->rodzaj_platnosci->test) ? '' : '') ?>">
    <?php
    foreach ($allInVal as $name => $value) {
        echo '<input type="hidden" value="' . $value . '" name="' . $name . '"/>';
    }
    ksort($allInVal,SORT_STRING);
    $valuesToSign=[];
    foreach ($allInVal as $key => $value){
        $valuesToSign[]= $key.'='.urlencode($value);
    }
    $sign= hash('SHA-256',join('&', $valuesToSign).'&'.$zamowienie->platnosc[0]->rodzaj_platnosci->konfig4);
    ?>
    <input type="hidden" value="sender=<?=$zamowienie->platnosc[0]->rodzaj_platnosci->konfig1?>;algorithm=SHA-256;signature=<?= $sign ?>" name="OpenPayu-Signature"/>
    <button type="submit" class="btn btn-lg btn-danger btn-zobacz"><?= $this->Translation->get('platnosc.Zaplac') ?></button>
</form>