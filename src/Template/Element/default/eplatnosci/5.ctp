<?php
$allInVal = [
    'customerIp' => $_SERVER['REMOTE_ADDR'],
    'extOrderId'=>$zamowienie->platnosc[0]->token.'|'.time(),
    'merchantPosId' => $zamowienie->platnosc[0]->rodzaj_platnosci->konfig1,
    'description' => $zamowienie->platnosc[0]->opis_lng,
    'totalAmount' => ($zamowienie->platnosc[0]->kwota * 100),
    'currencyCode' => (!empty($zamowienie->platnosc[0]->waluta) ? $zamowienie->platnosc[0]->waluta->symbol : 'PLN'),
    'shippingMethods[0].country' => 'PL',
            'shippingMethods[0].price' => $zamowienie->koszt_przesylki * 100,
            'shippingMethods[0].name' => $zamowienie->wysylka->nazwa,
            'buyer.email' => $zamowienie->email,
            'buyer.firstName' => $zamowienie->imie,
            'buyer.lastName' => $zamowienie->nazwisko,
];
if(!empty($zamowienie->zamowienie_towar)){
    foreach ($zamowienie->zamowienie_towar as $produktKey => $produkt){
        $allInVal['products['.$produktKey.'].name']=$produkt->nazwa;
        $allInVal['products['.$produktKey.'].unitPrice']=($produkt->cena_za_sztuke_brutto*100);
        $allInVal['products['.$produktKey.'].quantity']=$produkt->ilosc;
    }
}
    $allInVal['continueUrl'] = Cake\Routing\Router::url(['controller' => 'Cart', 'action' => 'orderPay', $zamowienie->platnosc[0]->token], true);
    $allInVal['notifyUrl'] = Cake\Routing\Router::url(['controller' => 'Cart', 'action' => 'payment', 5, $zamowienie->platnosc[0]->token], true);
?>
<form id="paymentForm" <?= (!empty($zamowienie->platnosc[0]->rodzaj_platnosci->auto_submit) ? 'class="form-auto-submit"' : '') ?> method="post" action="<?= (!empty($zamowienie->platnosc[0]->rodzaj_platnosci->test) ? 'https://secure.snd.payu.com/api/v2_1/orders' : 'https://secure.payu.com/api/v2_1/orders') ?>">
    <?php
    foreach ($allInVal as $name => $value) {
        echo '<input type="hidden" value="' . $value . '" name="' . $name . '"/>';
    }
    ksort($allInVal,SORT_STRING);
    $valuesToSign=[];
    foreach ($allInVal as $key => $value){
        $valuesToSign[]= $key.'='.urlencode($value);
    }
    $sign= hash('sha256',join('&', $valuesToSign).'&'.$zamowienie->platnosc[0]->rodzaj_platnosci->konfig4);
    ?>
    <input type="hidden" value="sender=<?=$zamowienie->platnosc[0]->rodzaj_platnosci->konfig1?>;algorithm=SHA-256;signature=<?= $sign ?>" name="OpenPayu-Signature"/>
    <button type="submit" class="btn btn-lg btn-danger btn-zobacz"><?= $this->Translation->get('platnosc.Zaplac') ?></button>
</form>