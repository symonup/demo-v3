<style type="text/css">
    .debugView{
        position: fixed;
        right: 5px;
        top: 5px;
        z-index: 500000000;
        background: #FF0000;
        color: #fff;
        font-weight: 700;
        padding: 10px;
    }
</style>
    <div class="debugView">
        <div class="d-xl-none d-lg-none d-md-none d-sm-none d-xs-block">XS</div>
        <div class="d-xl-none d-lg-none d-md-none d-sm-block d-xs-none">SM</div>
        <div class="d-xl-none d-lg-none d-md-block d-sm-none d-xs-none">MD</div>
        <div class="d-xl-none d-lg-block d-md-none d-sm-none d-xs-none">LG</div>
        <div class="d-xl-block d-lg-none d-md-none d-sm-none d-xs-none">XL</div>
    </div>