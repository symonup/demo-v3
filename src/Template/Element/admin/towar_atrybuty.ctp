<script type="text/javascript">
var towarId=<?php echo (!empty($towar->id)?$towar->id:0); ?>;
</script>
<?php
$grups=[];
$typeGroups=[];
$parentsNames=[];
$rozwinBtn='<span class="btn btn-xs btn-default attr-rozwin-liste"><i class="caret"></i></span>';
if(!empty($allAttrs)){
    foreach($allAttrs as $atrybutTyp){
        $attrWariant='<div class="wariant-label text-right"><input type="radio" name="towar_atrybut[attr_wariant]" '.((!empty($selectedWariantAttrType) && $selectedWariantAttrType==$atrybutTyp->id)?'checked="checked"':'').' id="attr-wariant-'.$atrybutTyp->id.'" value="'.$atrybutTyp->id.'" /><span class="radio-unselect" target="#attr-wariant-'.$atrybutTyp->id.'">'.$this->Txt->printAdmin(__('Admin | odznacz')).'</span> <label for="attr-wariant-'.$atrybutTyp->id.'">Użyj tego atrybutu jako wariantu</label></div>';
        $attrs=[];
        if(!empty($atrybutTyp->atrybut_typ_parent)){
            if(!empty($atrybutTyp->atrybut)){
            foreach ($atrybutTyp->atrybut as $attr){
                $subAttrsPodrzedne=[];
                if(!empty($attr->atrybut_podrzedne)){
                    foreach ($attr->atrybut_podrzedne as $attrPodrz){
                     $subAttrsPodrzedne[]=$this->Html->tag('li',$this->Form->input('towar_atrybut.atrybut_podrzedne_id.'.$attr->id.'.'.$attrPodrz->id,['type'=>'checkbox','class'=>'sub-attr-input','attr-id'=>$attr->id,'checked'=>((!empty($selected_atrybut_podrzedne) && key_exists($attrPodrz->id,$selected_atrybut_podrzedne))?true:false),'value'=>$attrPodrz->id,'label'=>$attrPodrz->nazwa]));   
                    }
                }
                $attrs[]=$this->Html->tag('li',$this->Form->input('towar_atrybut.atrybut_typ.'.$attr->atrybut_typ_id.'.'.$attr->id,['type'=>'hidden','value'=>$attr->id]).$this->Form->input('towar_atrybut.atrybut_id.'.$attr->id,['type'=>'checkbox','class'=>'attr-input','attr-id'=>$attr->id,'checked'=>((!empty($selected_atrybut) && key_exists($attr->id,$selected_atrybut))?true:false),'value'=>$attr->id,'label'=>$attr->nazwa]).(!empty($attr->pole)?$this->Form->input('towar_atrybut.wartosc.'.$attr->id,['type'=>'text','label'=>false,'value'=>(!empty($selected_atrybut) && key_exists($attr->id,$selected_atrybut)?$selected_atrybut[$attr->id]:''),'placeholder'=>__('Wartość liczbowa'),'data-type'=>'float']):'').(!empty($subAttrsPodrzedne)?(!empty($attr->pod_tytul)?'<div><label class="attr_pod">'.$attr->pod_tytul.'</label></div>':'').$this->Html->tag('ul', join('', $subAttrsPodrzedne)):''));
            }
        }
        $parentsNames[$atrybutTyp->atrybut_typ_parent->id]=$atrybutTyp->atrybut_typ_parent->nazwa;
        if(!empty($attrs)){
        $typeGroups[$atrybutTyp->atrybut_typ_parent->id][]=$this->Html->tag('li',$this->Html->tag('label',$atrybutTyp->nazwa).$this->Html->tag('ul', join('', $attrs)));
        
        }
        }
        else{
        if(!empty($atrybutTyp->atrybut)){
            foreach ($atrybutTyp->atrybut as $attr){
                $subAttrsPodrzedne=[];
                if(!empty($attr->atrybut_podrzedne)){
                    foreach ($attr->atrybut_podrzedne as $attrPodrz){
                     $subAttrsPodrzedne[]=$this->Html->tag('li',$this->Form->input('towar_atrybut.atrybut_podrzedne_id.'.$attr->id.'.'.$attrPodrz->id,['type'=>'checkbox','class'=>'sub-attr-input','attr-id'=>$attr->id,'checked'=>((!empty($selected_atrybut_podrzedne) && key_exists($attrPodrz->id,$selected_atrybut_podrzedne))?true:false),'value'=>$attrPodrz->id,'label'=>$attrPodrz->nazwa]));   
                    }
                }
                $attrs[]=$this->Html->tag('li',$this->Form->input('towar_atrybut.atrybut_typ.'.$attr->atrybut_typ_id.'.'.$attr->id,['type'=>'hidden','value'=>$attr->id]).$this->Form->input('towar_atrybut.atrybut_id.'.$attr->id,['type'=>'checkbox','class'=>'attr-input','attr-id'=>$attr->id,'checked'=>((!empty($selected_atrybut) && key_exists($attr->id,$selected_atrybut))?true:false),'value'=>$attr->id,'label'=>$attr->nazwa]).(!empty($attr->pole)?$this->Form->input('towar_atrybut.wartosc.'.$attr->id,['type'=>'text','label'=>false,'value'=>(!empty($selected_atrybut) && key_exists($attr->id,$selected_atrybut)?$selected_atrybut[$attr->id]:''),'placeholder'=>__('Wartość liczbowa'),'data-type'=>'float']):'').(!empty($subAttrsPodrzedne)?(!empty($attr->pod_tytul)?'<div><label class="attr_pod">'.$attr->pod_tytul.'</label></div>':'').$this->Html->tag('ul', join('', $subAttrsPodrzedne)):''));
            }
        }
        if(!empty($attrs)){
            $grups[]=$this->Html->tag('li',$this->Html->tag('label',$atrybutTyp->nazwa,['class'=>'attr-rozwin-liste']).$rozwinBtn.$attrWariant.$this->Html->tag('ul', join('', $attrs)));
        }
        }
    }
    if(!empty($typeGroups)){
        foreach ($typeGroups as $typeId => $groupAttr){
        $grups[]=$this->Html->tag('li',$this->Html->tag('label',$parentsNames[$typeId],['class'=>'attr-rozwin-liste']).$rozwinBtn.$this->Html->tag('ul', join('',$groupAttr)));
    }
    }
}
//var_dump($grups);
?>
<div class="greyArea">
    <h3>Atrybuty</h3>
    <p>Zaznacz tylko te atrybuty które pasują do tego produktu.</p>
    <div class="searchAttr">
<?php
if(!empty($grups)){
    foreach ($grups as $itemGroup){
        echo $this->Html->tag('ul',$itemGroup);
    }
}else{
    ?>
        <div class="alert alert-info">
            Brak atrybutów w bazie danych.
        </div>
        <?php
}
?>
    </div>
</div>
      