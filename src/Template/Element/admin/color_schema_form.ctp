<div class="row" item-key="<?= $i ?>">
    <div class="col-xs-12 col-sm-5"><?= $this->Form->control('colors.' . $i . '.key', ['type' => 'text', 'value' => $key]) ?></div>
    <div class="col-xs-12 col-sm-5"><?= $this->Form->control('colors.' . $i . '.color', ['type' => 'text', 'value' => $color]) ?></div>
    <div class="col-xs-12 col-sm-2 text-right"><button type="button" item-key="<?= $i ?>" class="removeLayoutSchemaColorInputs btn btn-danger btn-sm" confirm-message="<?=$this->Txt->printAdmin(__('Admin | Czy na pewno chcesz usunąć zmienną? Upewnij się że nie występuje ona w schemacie.'))?>"><i class="fa fa-trash"></i></button></div>
</div>