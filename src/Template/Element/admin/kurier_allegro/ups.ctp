<?php
$telefon = (!empty($zamowienie->wysylka_telefon)?$zamowienie->wysylka_telefon:$zamowienie->kupujacy_telefon);
$nazwa = $zamowienie->wysylka_imie . ' ' . $zamowienie->wysylka_nazwisko;
$nazwaF = $nazwa;
$adres = ucwords(mb_strtolower($zamowienie->wysylka_ulica)) . ' ' . $zamowienie->wysylka_nr_domu . (!empty($zamowienie->wysylka_nr_lokalu) ? '/' . $zamowienie->wysylka_nr_lokalu : '');
$kod = str_replace('-', '', $zamowienie->wysylka_kod);
$miasto = ucwords(mb_strtolower($zamowienie->wysylka_miasto));
echo $this->Form->create('Ups', array('url' => \Cake\Routing\Router::url(array('controller'=>'Zamowienie','action' => 'kurierXml', 'allegro_ups_'.date('ymd_His') . '_' . $zamowienie->id.'.xml', 'ups'))));
echo $this->Html->tag('h3', 'UPS - plik xml');
echo '<div class="shipCont col-xs-12 col-sm-12 col-md-4 col-lg-4">';
echo $this->Html->tag('h3', 'Informacje o odbiorcy');
echo $this->Form->input('OpenShipments.attr.xmlns', array('type' => 'hidden', 'value' => 'x-schema:OpenShipments.xdr'));
echo $this->Form->input('OpenShipments.OpenShipment.attr.ProcessStatus', array('type' => 'hidden', 'value' => ''));
echo $this->Form->input('OpenShipments.OpenShipment.attr.ShipmentOption', array('type' => 'hidden', 'value' => 'SC'));

//echo $this->Form->input('OpenShipments.OpenShipment.ShipFrom.CompanyOrName', array('type' => 'hidden', 'value' => \Cake\Core\Configure::read('ups.CompanyOrName')));
//echo $this->Form->input('OpenShipments.OpenShipment.ShipFrom.Attention', array('type' => 'hidden', 'value' => 'Shipper'));
//echo $this->Form->input('OpenShipments.OpenShipment.ShipFrom.Address1', array('type' => 'hidden', 'value' => \Cake\Core\Configure::read('ups.Address1')));
//echo $this->Form->input('OpenShipments.OpenShipment.ShipFrom.CountryTerritory', array('type' => 'hidden', 'value' => \Cake\Core\Configure::read('ups.CountryTerritory')));
//echo $this->Form->input('OpenShipments.OpenShipment.ShipFrom.PostalCode', array('type' => 'hidden', 'value' => \Cake\Core\Configure::read('ups.PostalCode')));
//echo $this->Form->input('OpenShipments.OpenShipment.ShipFrom.CityOrTown', array('type' => 'hidden', 'value' => \Cake\Core\Configure::read('ups.CityOrTown')));
//echo $this->Form->input('OpenShipments.OpenShipment.ShipFrom.StateProvinceCounty', array('type' => 'hidden', 'value' => \Cake\Core\Configure::read('ups.StateProvinceCounty')));
//echo $this->Form->input('OpenShipments.OpenShipment.ShipFrom.Telephone', array('type' => 'hidden', 'value' => \Cake\Core\Configure::read('ups.Telephone')));
//echo $this->Form->input('OpenShipments.OpenShipment.ShipFrom.UpsAccountNumber', array('type' => 'hidden', 'value' => \Cake\Core\Configure::read('ups.UpsAccountNumber')));

echo $this->Form->input('OpenShipments.OpenShipment.ShipTo.CompanyOrName', array('label' => 'Nazwa odbiorcy','class'=>'form-control', 'value' => $nazwaF));
echo $this->Form->input('OpenShipments.OpenShipment.ShipTo.Attention', array('label' => 'Attention','class'=>'form-control', 'value' => $nazwa));
echo $this->Form->input('OpenShipments.OpenShipment.ShipTo.Address1', array('label' => 'Adres','class'=>'form-control', 'value' => $adres));
echo $this->Form->input('OpenShipments.OpenShipment.ShipTo.CountryTerritory', array('label' => 'Kraj','class'=>'form-control', 'options' => \Cake\Core\Configure::read('kraje'), 'value' => $zamowienie->wysylka_kraj));
echo $this->Form->input('OpenShipments.OpenShipment.ShipTo.PostalCode', array('label' => 'Kod pocztowy','class'=>'form-control', 'value' => $kod));
echo $this->Form->input('OpenShipments.OpenShipment.ShipTo.CityOrTown', array('label' => 'Miasto','class'=>'form-control', 'value' => $miasto));
echo $this->Form->input('OpenShipments.OpenShipment.ShipTo.Telephone', array('label' => 'Telefon','class'=>'form-control', 'value' => $telefon));
//echo $this->Form->input('OpenShipments.OpenShipment.ShipTo.ReceiverUpsAccountNumber', array('label' => 'Identyfikator odbiorcy UPS','class'=>'form-control', 'value' => \Cake\Core\Configure::read('ups.ReceiverUpsAccountNumber')));
echo '</div>';
echo '<div class="shipCont col-xs-12 col-sm-12 col-md-4 col-lg-4">';
echo $this->Html->tag('h3', 'Informacje o przesyłce');
echo $this->Form->input('OpenShipments.OpenShipment.ShipmentInformation.ProfileName', array('label' => 'Profile name','class'=>'form-control','value'=>\Cake\Core\Configure::read('ups.ProfileName')));
echo $this->Form->input('OpenShipments.OpenShipment.ShipmentInformation.ServiceType', array('label' => 'Typ przesyłki','class'=>'form-control','value'=>'ST', 'options' => \Cake\Core\Configure::read('ups_opt_ServiceType')));
echo $this->Form->input('OpenShipments.OpenShipment.ShipmentInformation.PackageType', array('label' => 'Typ paczki','class'=>'form-control','value'=>'CP', 'options' => \Cake\Core\Configure::read('ups_opt_PackageType')));
echo $this->Form->input('OpenShipments.OpenShipment.ShipmentInformation.NumberOfPackages', array('label' => 'Ilość paczek','class'=>'form-control', 'value'=>1));

echo $this->Form->input('OpenShipments.OpenShipment.ShipmentInformation.ShipmentActualWeight', array('label' => 'Waga paczki','class'=>'form-control'));
echo $this->Form->input('OpenShipments.OpenShipment.ShipmentInformation.AdditionalHandling.NumberOfAdditionalHandling', array('label' => 'Dodatkowa obsługa','type'=>'number','default'=>'','class'=>'form-control', 'value'=>''));

echo $this->Form->input('OpenShipments.OpenShipment.ShipmentInformation.Reference1', array('label' => 'Reference1','class'=>'form-control'));
echo $this->Form->input('OpenShipments.OpenShipment.ShipmentInformation.Reference2', array('label' => 'Reference2','class'=>'form-control'));
//echo $this->Form->input('OpenShipments.OpenShipment.ShipmentInformation.DescriptionOfGoods', array('label' => 'Ogólny opis paczki','class'=>'form-control','type'=>'textarea'));
//        echo $this->Form->input('ShipmentInformation.GoodsNotInFreeCirculation',array('label'=>'Wysokość','options'=>Configure::read('ups_opt_GoodsNotInFreeCirculation')));
//echo $this->Form->input('OpenShipments.OpenShipment.ShipmentInformation.BillTransportationTo', array('label' => 'Koszt pokrywa','class'=>'form-control', 'options' => \Cake\Core\Configure::read('ups_opt_BillTransportationTo')));
echo $this->Form->input('OpenShipments.OpenShipment.ShipmentInformation.BillingOption', array('label' => 'Koszt pokrywa','class'=>'form-control','value'=>'PP', 'options' => \Cake\Core\Configure::read('ups_opt_BillingOption')));
if($zamowienie->payment_type == 'CASH_ON_DELIVERY')
{
echo $this->Form->input('OpenShipments.OpenShipment.ShipmentInformation.COD.CashOnly', array('type'=>'hidden','value' => '1','class'=>'form-control'));
echo $this->Form->input('OpenShipments.OpenShipment.ShipmentInformation.COD.Amount', array('label' => 'Wartość pobrania','class'=>'form-control','value'=>$zamowienie->wartosc_razem));
}
//
//echo $this->Form->input('OpenShipments.OpenShipment.ShipmentInformation.BillDutyTaxTo', array('label' => 'Podatek pokrywa','class'=>'form-control', 'options' => \Cake\Core\Configure::read('ups_opt_BillDutyTaxTo')));
//echo $this->Form->input('OpenShipments.OpenShipment.ShipmentInformation.SplitDutyAndTax', array('label' => 'Podziel koszty (koszt/podatek)','class'=>'form-control', 'options' => \Cake\Core\Configure::read('ups_opt_SplitDutyAndTax')));
echo '</div>';
echo '<div class="shipCont col-xs-12 col-sm-12 col-md-4 col-lg-4">';
echo $this->Html->tag('h3', 'Informacje dodatkowe');
echo $this->Form->input('OpenShipments.OpenShipment.ShipmentInformation.QVNOption.0.QVNRecipientAndNotificationTypes.EMailAddress', array('label' => 'Email odbiorcy','value'=>$zamowienie->kupujacy_email,'class'=>'form-control'));
echo $this->Form->input('OpenShipments.OpenShipment.ShipmentInformation.QVNOption.0.QVNRecipientAndNotificationTypes.Ship', array('type'=>'hidden','value'=>1));
echo $this->Form->input('OpenShipments.OpenShipment.ShipmentInformation.QVNOption.1.QVNRecipientAndNotificationTypes.EMailAddress', array('label' => 'Email nadawcy','value'=>\Cake\Core\Configure::read('ups.email'),'class'=>'form-control'));
echo $this->Form->input('OpenShipments.OpenShipment.ShipmentInformation.QVNOption.1.QVNRecipientAndNotificationTypes.Exception', array('type'=>'hidden','value'=>1));
echo $this->Form->input('OpenShipments.OpenShipment.ShipmentInformation.QVNOption.SubjectLine', array('label' => 'Subject Line','value'=>0,'class'=>'form-control'));
echo $this->Form->input('OpenShipments.OpenShipment.ShipmentInformation.QVNOption.Memo', array('label' => 'Memo','value'=>'','class'=>'form-control'));
echo $this->Form->input('OpenShipments.OpenShipment.ShipmentInformation.ReturnOfDocument', array('label' => 'Return of document','value'=>'N','options'=>['N'=>__('Nie'),'Y'=>__('Tak')],'class'=>'form-control'));

//echo $this->Form->input('OpenShipments.OpenShipment.Package.Weight', array('label' => 'Waga paczki','class'=>'form-control'));
//echo $this->Form->input('OpenShipments.OpenShipment.Package.Length', array('label' => 'Długość','class'=>'form-control'));
//echo $this->Form->input('OpenShipments.OpenShipment.Package.Width', array('label' => 'Szerokość','class'=>'form-control'));
//echo $this->Form->input('OpenShipments.OpenShipment.Package.Height', array('label' => 'Wysokość','class'=>'form-control'));
//echo $this->Form->input('OpenShipments.OpenShipment.Package.Reference1', array('label' => 'Reference1','class'=>'form-control'));
//echo $this->Form->input('OpenShipments.OpenShipment.Package.Reference2', array('label' => 'Reference2','class'=>'form-control'));
//echo $this->Form->input('OpenShipments.OpenShipment.Package.Reference3', array('label' => 'Reference3','class'=>'form-control'));
//echo $this->Form->input('OpenShipments.OpenShipment.Package.Reference4', array('label' => 'Reference4','class'=>'form-control'));
//echo $this->Form->input('OpenShipments.OpenShipment.Package.Reference5', array('label' => 'Reference5','class'=>'form-control'));
echo '</div>';
echo $this->Html->tag('div', $this->Html->tag('button', '<span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> ' . __('Pobierz'), ['class' => 'btn btn-success btn-sm', 'type' => 'submit', 'escape' => false]), ['class' => 'submit text-right col-xs-12']);
echo $this->Form->end();
?>
