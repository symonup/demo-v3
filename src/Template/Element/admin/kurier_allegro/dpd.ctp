<?php
//\Cake\Core\Configure::read('siodemka.nazwisko_nadawcy')
$telefon=trim(str_replace(['+',' ','.','-'],['','','',''],(!empty($zamowienie->wysylka_telefon)?$zamowienie->wysylka_telefon:$zamowienie->kupujacy_telefon)));
if(strlen($telefon)==9){
    $telefon='48'.$telefon;
}
$allowKraj= \Cake\Core\Configure::read('dpd.allow_kraj');
if(!empty($allowKraj)){
    $csvLabels=['Nazwa odbiorcy','Adres','Kod pocztowy','Kraj','Miasto','Telefon','E-mail','Ilość paczek','Waga','Wartość ubezpieczenia','Wartość pobrania','Uwagi','Nr ref 1','Nr ref 2'];
}else{
    $csvLabels=['Nazwa odbiorcy','Adres','Kod pocztowy','Miasto','Telefon','E-mail','Ilość paczek','Waga','Wartość ubezpieczenia','Wartość pobrania','Uwagi','Nr ref 1','Nr ref 2'];
}
$labelsI=0;
$csvPos=0;
$nazwa = $zamowienie->wysylka_imie . ' ' . $zamowienie->wysylka_nazwisko;
use Cake\Routing\Router;
echo $this->Form->create(NULL, array('url' => Router::url(array('controller' => 'Zamowienie', 'action' => 'kurier_csv', $zamowienie->id,'allegro_dpd'))));
echo $this->Html->tag('h3', 'DPD - plik csv');
echo '<div class="shipCont col-xs-12 col-sm-12 col-md-4 col-lg-4"><h3>Informacje o odbiorcy</h3>';
echo $this->Form->input('csv.'.$csvPos, array('label'=>$csvLabels[$labelsI],'value'=>$nazwa));
echo $this->Form->input('labels.'.$csvPos++, array('type' => 'hidden','value'=>$csvLabels[$labelsI++]));
echo $this->Form->input('csv.'.$csvPos, array('label'=>$csvLabels[$labelsI],'value'=>$zamowienie->wysylka_ulica.' '.$zamowienie->wysylka_nr_domu.(!empty($zamowienie->wysylka_nr_lokalu)?'/'.$zamowienie->wysylka_nr_lokalu:'')));
echo $this->Form->input('labels.'.$csvPos++, array('type' => 'hidden','value'=>$csvLabels[$labelsI++]));
echo $this->Form->input('csv.'.$csvPos, array('label'=>$csvLabels[$labelsI],'value'=> $zamowienie->wysylka_kod));
echo $this->Form->input('labels.'.$csvPos++, array('type' => 'hidden','value'=>$csvLabels[$labelsI++]));
if(!empty($allowKraj)){
echo $this->Form->input('csv.'.$csvPos, array('label'=>$csvLabels[$labelsI],'default'=> 'PL','type'=>'text','value'=>$zamowienie->wysylka_kraj));
echo $this->Form->input('labels.'.$csvPos++, array('type' => 'hidden','value'=>$csvLabels[$labelsI++]));
}
echo $this->Form->input('csv.'.$csvPos, array('label'=>$csvLabels[$labelsI],'value'=>$zamowienie->wysylka_miasto));
echo $this->Form->input('labels.'.$csvPos++, array('type' => 'hidden','value'=>$csvLabels[$labelsI++]));
echo $this->Form->input('csv.'.$csvPos, array('label'=>$csvLabels[$labelsI],'value'=>$telefon));
echo $this->Form->input('labels.'.$csvPos++, array('type' => 'hidden','value'=>$csvLabels[$labelsI++]));
echo $this->Form->input('csv.'.$csvPos, array('label'=>$csvLabels[$labelsI],'value'=>$zamowienie->kupujacy_email));
echo $this->Form->input('labels.'.$csvPos++, array('type' => 'hidden','value'=>$csvLabels[$labelsI++]));
echo '</div>';
echo '<div class="shipCont col-xs-12 col-sm-12 col-md-4 col-lg-4"><h3>Informacje o przesyłce</h3>';
echo $this->Form->input('csv.'.$csvPos, array('type'=>'text','label'=>$csvLabels[$labelsI],'value'=>''));
echo $this->Form->input('labels.'.$csvPos++, array('type' => 'hidden','value'=>$csvLabels[$labelsI++]));
echo $this->Form->input('csv.'.$csvPos, array('type'=>'text','label'=>$csvLabels[$labelsI],'value'=>''));
echo $this->Form->input('labels.'.$csvPos++, array('type' => 'hidden','value'=>$csvLabels[$labelsI++]));
echo $this->Form->input('csv.'.$csvPos, array('label'=>$csvLabels[$labelsI],'value'=>($zamowienie->wartosc_razem)));
echo $this->Form->input('labels.'.$csvPos++, array('type' => 'hidden','value'=>$csvLabels[$labelsI++]));
echo $this->Form->input('csv.'.$csvPos, array('label'=>$csvLabels[$labelsI],'value'=>(($zamowienie->payment_type == 'CASH_ON_DELIVERY') ? ($zamowienie->wartosc_razem) : '')));
echo $this->Form->input('labels.'.$csvPos++, array('type' => 'hidden','value'=>$csvLabels[$labelsI++]));
echo $this->Form->input('csv.'.$csvPos, array('type'=>'text','label'=>$csvLabels[$labelsI],'value'=>''));
echo $this->Form->input('labels.'.$csvPos++, array('type' => 'hidden','value'=>$csvLabels[$labelsI++]));
echo $this->Form->input('csv.'.$csvPos, array('type'=>'text','label'=>$csvLabels[$labelsI],'value'=>''));
echo $this->Form->input('labels.'.$csvPos++, array('type' => 'hidden','value'=>$csvLabels[$labelsI++]));
echo $this->Form->input('csv.'.$csvPos, array('type'=>'text','label'=>$csvLabels[$labelsI],'value'=>''));
echo $this->Form->input('labels.'.$csvPos++, array('type' => 'hidden','value'=>$csvLabels[$labelsI++]));
echo '</div>';
echo $this->Html->tag('div', $this->Html->tag('button', '<span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> ' . __('Pobierz'), ['class' => 'btn btn-success btn-sm', 'type' => 'submit', 'escape' => false]), ['class' => 'submit text-right col-xs-12']);
echo $this->Form->end();
?>
<script type="text/javascript">
    
</script>


