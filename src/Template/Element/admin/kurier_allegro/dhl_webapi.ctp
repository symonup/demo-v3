<?= $this->Html->script(array('/layout-admin/build/js/dhl.js')) ?>
<?php
$oldData = null;
if (!empty($_GET['dhl_edit']) && !empty($zamowienie->zamowienie_dhl)) {
    foreach ($zamowienie->zamowienie_dhl as $tmpData) {
        if ($tmpData['shipmentId'] == $_GET['dhl_edit']) {
            $oldData = json_decode($tmpData['requestData'], true);
            break;
        }
    }
}
$nazwa = $zamowienie->wysylka_imie . ' ' . $zamowienie->wysylka_nazwisko;
echo $this->Form->create('LIST', array('class' => 'form-default', 'style' => 'display: table; width: 100%; table-layout: fixed;', 'url' => \Cake\Routing\Router::url(array('controller' => 'Zamowienie', 'action' => ((!empty($_GET['dhl_edit']) && !empty($oldData)) ? 'dhl_edit' : 'dhl_add'), ((!empty($_GET['dhl_edit']) && !empty($oldData)) ? $_GET['dhl_edit'] : $zamowienie->id), 'a'))));
echo $this->Html->tag('h3', 'DHL - webApi');
echo '<div class="shipCont col-xs-12 col-sm-12 col-md-4 col-lg-4"><h3>Informacje o odbiorcy</h3>';
echo $this->Form->input('receiver.name', array('label' => 'Nazwa odbiorcy:', 'class' => 'form-control', 'type' => 'text', 'value' => (!empty($oldData) ? $oldData['receiver']['name'] : $nazwa)));
echo $this->Form->input('receiver.postalCode', array('label' => 'Kod:', 'class' => 'form-control', 'type' => 'text', 'value' => (!empty($oldData) ? $oldData['receiver']['postalCode'] : str_replace('-', '', $zamowienie->wysylka_kod))));
echo $this->Form->input('receiver.city', array('label' => 'Miasto:', 'class' => 'form-control', 'type' => 'text', 'value' => (!empty($oldData) ? $oldData['receiver']['city'] : $zamowienie->wysylka_miasto)));
echo $this->Form->input('receiver.street', array('label' => 'Ulica:', 'class' => 'form-control', 'type' => 'text', 'value' => (!empty($oldData) ? $oldData['receiver']['street'] : $zamowienie->wysylka_ulica)));
echo $this->Form->input('receiver.houseNumber', array('label' => 'Nr domu:', 'required' => true, 'class' => 'form-control', 'type' => 'text', 'value' => (!empty($oldData) ? $oldData['receiver']['houseNumber'] : $zamowienie->wysylka_nr_domu)));
echo $this->Form->input('receiver.apartmentNumber', array('label' => 'Nr lokalu:', 'class' => 'form-control', 'type' => 'text', 'value' => (!empty($oldData) ? $oldData['receiver']['apartmentNumber'] : $zamowienie->wysylka_nr_lokalu)));
echo $this->Form->input('receiver.country', array('label' => 'Kraj:', 'class' => 'form-control', 'type' => 'text', 'value' => (!empty($oldData) ? $oldData['receiver']['country'] : (!empty($zamowienie->wysylka_kraj) ? $zamowienie->wysylka_kraj : 'PL'))));
echo $this->Form->input('receiver.contactPerson', array('label' => 'Osoba kontaktowa:', 'type' => 'text', 'value' => (!empty($oldData) ? $oldData['receiver']['contactPerson'] : $zamowienie->wysylka_imie . ' ' . $zamowienie->wysylka_nazwisko)));
echo $this->Form->input('receiver.contactPhone', array('label' => 'Telefon:', 'type' => 'text', 'value' => (!empty($oldData) ? $oldData['receiver']['contactPhone'] : (!empty($zamowienie->wysylka_telefon)?$zamowienie->wysylka_telefon:$zamowienie->kupujacy_telefon))));
echo $this->Form->input('receiver.contactEmail', array('label' => 'Email:', 'type' => 'text', 'value' => (!empty($oldData) ? $oldData['receiver']['contactEmail'] : $zamowienie->kupujacy_email)));
echo '</div>';
echo '<div class="shipCont col-xs-12 col-sm-12 col-md-4 col-lg-4"><h3>Informacje o przesyłce</h3>';
echo $this->Form->input('pieceList.0.type', ['type' => 'select', 'id' => 'dhl-rodzaj-paczki', 'options' => ['ENVELOPE' => 'List', 'PACKAGE' => 'Paczka', 'PALLET' => 'Paleta'], 'value' => (!empty($oldData) ? $oldData['pieceList'][0]['type'] : 'PACKAGE'), 'label' => 'Rodzaj paczki']);
echo $this->Form->input('pieceList.0.width', ['type' => 'number', 'value' => (!empty($oldData) ? $oldData['pieceList'][0]['width'] : ''), 'class' => 'dhl-rodzaj-paczki-required', 'label' => ['escape'=>false,'text'=>'Szerokość w centymetrach<span class="label-more-info">wymagana gdy typ inny niż "List"</span>']]);
echo $this->Form->input('pieceList.0.height', ['type' => 'number', 'value' => (!empty($oldData) ? $oldData['pieceList'][0]['height'] : ''), 'class' => 'dhl-rodzaj-paczki-required', 'label' => ['escape'=>false,'text'=>'Wysokość w centymetrach<span class="label-more-info">wymagana gdy typ inny niż "List"</span>']]);
echo $this->Form->input('pieceList.0.length', ['type' => 'number', 'value' => (!empty($oldData) ? $oldData['pieceList'][0]['length'] : ''), 'class' => 'dhl-rodzaj-paczki-required', 'label' => ['escape'=>false,'text'=>'Długość w centymetrach<span class="label-more-info">wymagana gdy typ inny niż "List"</span>']]);
echo $this->Form->input('pieceList.0.weight', ['type' => 'number', 'value' => (!empty($oldData) ? $oldData['pieceList'][0]['weight'] : ''), 'class' => 'dhl-rodzaj-paczki-required', 'label' => ['escape'=>false,'text'=>'Waga w kilogramach<span class="label-more-info">wymagana gdy typ inny niż "List"</span>']]);
echo $this->Form->input('pieceList.0.quantity', ['label' => 'Ilość paczek', 'type' => 'number', 'value' => (!empty($oldData) ? $oldData['pieceList'][0]['quantity'] : 1)]);
echo $this->Form->input('pieceList.0.nonStandard', ['type' => 'select', 'options' => [0 => 'Nie', 1 => 'Tak'], 'value' => (!empty($oldData) ? $oldData['pieceList'][0]['nonStandard'] : 0), 'label' => ['escape'=>false,'text'=>'Czy paczka jest niestandardowa<span class="label-more-info">(wg definicji z cennika)</span>']]);
echo $this->Form->input('pieceList.0.euroReturn', ['type' => 'select', 'options' => [0 => 'Nie', 1 => 'Tak'], 'value' => (!empty($oldData) ? $oldData['pieceList'][0]['euroReturn'] : 0), 'label' => ['escape'=>false,'text'=>'Czy palety euro do zwrotu<span class="label-more-info">(można wybrać tylko przy type = "PALLET")</span>']]);
echo '</div>';
echo '<div class="shipCont col-xs-12 col-sm-12 col-md-4 col-lg-4"><h3>Informacje dodatkowe</h3>';
echo $this->Form->input('service.product', array('label' => 'Typ produktu', 'value' => (!empty($oldData) ? $oldData['service']['product'] : 'AH'), 'class' => 'form-control', 'type' => 'select', 'options' => array('AH' => 'Standardowa dostawa', '09' => 'Dostawa do godz. 9 dnia następnego', '12' => 'Dostawa do godz. 12 dnia następnego', 'EK' => 'przesyłka Connect', 'PI' => 'przesyłka International')));
echo $this->Form->input('service.deliveryEvening', array('label' => 'Doręczenie wieczorne', 'value' => (!empty($oldData) ? $oldData['service']['deliveryEvening'] : 0), 'class' => 'form-control', 'type' => 'select', 'options' => array(0 => 'Nie', 1 => 'Tak')));
echo $this->Form->input('service.deliveryOnSaturday', array('label' => 'Doręczenie w sobotę', 'value' => (!empty($oldData) ? $oldData['service']['deliveryOnSaturday'] : 0), 'class' => 'form-control', 'type' => 'select', 'options' => array(0 => 'Nie', 1 => 'Tak')));
echo $this->Form->input('service.pickupOnSaturday', array('label' => 'Nadanie w sobotę', 'value' => (!empty($oldData) ? $oldData['service']['pickupOnSaturday'] : 0), 'class' => 'form-control', 'type' => 'select', 'options' => array(0 => 'Nie', 1 => 'Tak')));
echo $this->Form->input('service.collectOnDelivery', array('label' => 'Opłata za pobraniem', 'class' => 'form-control', 'type' => 'select', 'value' => (!empty($oldData) ? $oldData['service']['collectOnDelivery'] : (($zamowienie->payment_type == 'CASH_ON_DELIVERY') ? 1 : 0)), 'options' => array(0 => 'Nie', 1 => 'Tak')));
echo $this->Form->input('service.collectOnDeliveryValue', array('label' => 'Wartość pobrania', 'class' => 'form-control', 'type' => 'text', 'value' => (!empty($oldData) ? $oldData['service']['collectOnDeliveryValue'] : (($zamowienie->payment_type == 'CASH_ON_DELIVERY') ? $zamowienie->wartosc_razem : '0'))));
echo $this->Form->input('service.collectOnDeliveryForm', array('type' => 'hidden', 'value' => 'BANK_TRANSFER'));
//echo $this->Form->input('service.collectOnDeliveryReference', array('type' => 'hidden','value'=>''));
echo $this->Form->input('service.insurance', array('type' => 'select', 'options' => [0 => 'Nie', 1 => 'Tak'], 'value' => (!empty($oldData) ? $oldData['service']['insurance'] : (($zamowienie->payment_type == 'CASH_ON_DELIVERY') ? 1 : 0)), 'label' => 'Ubezpieczenie:'));
echo $this->Form->input('service.insuranceValue', array('type' => 'text', 'data-type' => 'float', 'value' => (!empty($oldData) ? $oldData['service']['insuranceValue'] : (($zamowienie->payment_type == 'CASH_ON_DELIVERY') ? $zamowienie->wartosc_razem : '')), 'label' => 'Wartość ubezpieczenia:'));
//echo $this->Form->input('service.returnOnDelivery', array('type' => 'hidden','value'=>''));
//echo $this->Form->input('service.returnOnDeliveryReference', array('type' => 'hidden','value'=>''));
//echo $this->Form->input('service.proofOfDelivery', array('type' => 'hidden','value'=>''));
//echo $this->Form->input('service.selfCollect', array('type' => 'select','options'=>[0=>'Nie',1=>'Tak'],'value'=>0,'label'=>'Odbiuór własny:'));
//echo $this->Form->input('service.deliveryToNeighbour', array('type' => 'select','options'=>[0=>'Nie',1=>'Tak'],'value'=>0,'label'=>'Doręczenie do sąsiada:'));
echo $this->Form->input('service.predeliveryInformation', array('type' => 'select', 'options' => [0 => 'Nie', 1 => 'Tak'], 'value' => (!empty($oldData) ? $oldData['service']['predeliveryInformation'] : 0), 'label' => 'Informacja przed doręczeniem:'));
echo $this->Form->input('service.preaviso', array('type' => 'select', 'options' => [0 => 'Nie', 1 => 'Tak'], 'value' => (!empty($oldData) ? $oldData['service']['preaviso'] : 0), 'label' => 'Informacja dla odbiorcy o przyjęciu paczki do przesłania:'));

echo $this->Form->input('payment.payerType', array('type' => 'select', 'options' => ['RECEIVER' => 'Odbiorca', 'SHIPPER' => 'Nadawca', 'USER' => 'Przesyłka typu "trzecia strona" (jeśli umożliwia to przydzielony numer SAP)'], 'value' => (!empty($oldData) ? $oldData['payment']['payerType'] : (($zamowienie->payment_type == 'CASH_ON_DELIVERY') ? 'RECEIVER' : 'SHIPPER')), 'label' => 'Płatność za usługę transportową'));
echo $this->Form->input('payment.paymentMethod', array('type' => 'select', 'options' => ['CASH' => 'Gotówka', 'BANK_TRANSFER' => 'Przelew'], 'value' => (!empty($oldData) ? $oldData['payment']['paymentMethod'] : (($zamowienie->payment_type == 'CASH_ON_DELIVERY') ? 'CASH' : 'BANK_TRANSFER')), 'label' => 'Sposób płatności'));

echo $this->Form->input('content', array('type' => 'text', 'value' => (!empty($oldData) ? $oldData['content'] : 'Płyty DVD'), 'max' => 30, 'label' => 'Opis zawartości', 'required' => true));
echo $this->Form->input('shipmentDate', ['autocomplete' => 'off', 'value' => (!empty($oldData) ? $oldData['shipmentDate'] : date('Y-m-d')), 'label' => ['text' => __('Data nadania') . '<span class="label-more-info">(Data nadania musi być zgodna z datą zamówienia kuriera)</span>', 'escape' => false], 'type' => 'text', 'datepicker' => 'date', 'class' => 'datepicker', 'required' => true]);
echo '</div>';
if (!empty($_GET['dhl_edit']) && !empty($oldData)) {
    echo $this->Html->tag('div', $this->Html->tag('button', '<span class="glyphicon glyphicon-send" aria-hidden="true"></span> ' . __('Aktualizuj przesyłkę'), ['class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('Czy na pewno chcesz zaktualizować przesyłkę? Aktualizacja nadaje nowy numer przesyłki.');", 'type' => 'submit', 'escape' => false]), ['class' => 'submit text-right']);
} else {
    echo $this->Html->tag('div', $this->Html->tag('button', '<span class="glyphicon glyphicon-send" aria-hidden="true"></span> ' . __('Dodaj przesyłkę'), ['class' => 'btn btn-success btn-sm', 'type' => 'submit', 'escape' => false]), ['class' => 'submit text-right']);
}
echo $this->Form->end();
if (!empty($zamowienie->zamowienie_dhl)) {
    ?>
    <h3>Wygenerowane przesyłki:</h3>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th><?= __('Data nadania') ?></th>
                <th><?= __('Numer przesyłki') ?></th>
                <th><?= __('Opis zawartości') ?></th>
                <th><?= __('Nr zlecenia') ?></th>
                <th><?= __('List przewozowy') ?></th>
            </tr>
        </thead>
        <tbody>

            <?php
            foreach ($zamowienie->zamowienie_dhl as $ship) {
                ?>
                <tr>
                    <td><?= (!empty($ship->shipmentDate) ? $ship->shipmentDate->format('Y-m-d') : '') ?></td>
                    <td><?= $ship->shipmentId ?></td>
                    <td><?= nl2br($ship->opis) ?></td>
                    <td><?= $ship->orderId ?></td>
                    <td>
                        <?= $this->Html->link('<span class="glyphicon glyphicon-print" aria-hidden="true"></span>', ['action' => 'dhl_get_label', $ship->shipmentId], ['escape' => false, 'target' => '_blank', 'class' => 'btn btn-primary btn-xs', 'data-toggle' => 'tooltip', 'title' => __('Pobierz list przewozowy')]) ?>
                        <?php
                        if (empty($ship->orderId)) {
                            echo $this->Html->link('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', ['action' => 'view', $zamowienie->id, 'k_tab' => 'dhlWebApi', 'dhl_edit' => $ship->shipmentId], ['escape' => false, 'class' => 'btn btn-warning btn-xs', 'data-toggle' => 'tooltip', 'title' => __('Edytuj przesyłkę')]);
                            echo ' ' . $this->Html->link('<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>', ['action' => 'dhl_delete_shipment', $ship->shipmentId], ['escape' => false, 'onclick' => "return confirm('Czy na pewno chcesz usunąć przesyłkę?');", 'class' => 'btn btn-danger btn-xs', 'data-toggle' => 'tooltip', 'title' => __('Usuń przesyłkę')]);
                        }
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <?php
}
?>
