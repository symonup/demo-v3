<?php
$actSimilar='';
if(!empty($towar) && !empty($towar->polecane))
{
    foreach ($towar->polecane as $podobny)
    {
        $actSimilar.='<li>'.((!empty($podobny->towar_zdjecie) && file_exists($filePath['towar'].'thumb_'.$podobny->towar_zdjecie[0]->plik))?'<img style="max-width: 50px; float: left;" src="'.$displayPath['towar'].'thumb_'.$podobny->towar_zdjecie[0]->plik.'"/>':'').$podobny->nazwa.'<input type="hidden" value="'.$podobny->id.'" name="polecane[_ids][]"/></li>';
    }
}
echo $this->Html->tag('div',
        '<input type="hidden" name="polecane[_ids][]"/>'.
                $this->Html->tag('h3',__('Produkty powiązane/polecane (Inni kupili także)').$this->Txt->helpInfo('towar','produkty_polecane'))
         .$this->Form->input('search',['type'=>'text','data-target'=> Cake\Routing\Router::url(['action'=>'findSimilar']).'/','label'=>$this->Txt->printAdmin(__('Admin | Wyszukaj po nazwie lub kodzie produktu.')),'id'=>'polecaneSearch','class'=>'form-control','div'=>['class'=>'form-group input text'], 'templates' => ['formGroup' => '{{label}}{{input}}<div class="searching"></div>']])
         .$this->Html->tag('div',$this->Html->tag('h5',$this->Txt->printAdmin(__('Admin | Wyniki wyszukiwania'))).$this->Html->tag('ul',' ',['class'=>'similar_product_sort polecane_search_results']),['class'=>'col-xs-12 col-sm-5 col-md-5 col-lg-5'])
                
         .$this->Html->tag('div',$this->Html->tag('h5',__('Actions')).$this->Html->tag('div','<span data-toggle="tooltip" data-placement="top" title="'.__('Add selected to recomend').'" class="glyphicon glyphicon-step-forward" data-action="move-to-similar" data-content-result="polecane_search_results" data-content="polecane_products" aria-hidden="true"></span><br/>
            <span data-toggle="tooltip" data-placement="top" title="'.__('Remove selected from recomend').'" class="glyphicon glyphicon-step-backward" aria-hidden="true" data-action="move-to-result" data-content-result="polecane_search_results" data-content="polecane_products"></span><br/>
            <span data-toggle="tooltip" data-placement="top" title="'.__('Add all to recomend').'" class="glyphicon glyphicon-fast-forward" aria-hidden="true" data-action="move-all-to-similar" data-content-result="polecane_search_results" data-content="polecane_products"></span><br/>
            <span data-toggle="tooltip" data-placement="top" title="'.__('Remove all from recomend').'" class="glyphicon glyphicon-fast-backward" aria-hidden="true" data-action="move-all-to-result" data-content-result="polecane_search_results" data-content="polecane_products"></span><br/>
            <span data-toggle="tooltip" data-placement="top" title="'.__('Remove selected from recomend').'" class="glyphicon glyphicon-trash" aria-hidden="true" data-action="delete-similar" data-content-result="polecane_search_results" data-content="polecane_products"></span><br/>
            <span data-toggle="tooltip" data-placement="top" title="'.__('Full size').'" class="glyphicon glyphicon-fullscreen" aria-hidden="true" data-action="full-size" data-content-result="polecane_search_results" data-content="polecane_products"></span>',['class'=>'similar_action_content']),['class'=>'col-xs-12 col-sm-2 col-md-2 col-lg-2'])
         .$this->Html->tag('div',$this->Html->tag('h5',__('Polecane')).$this->Html->tag('ul',(!empty($actSimilar)?$actSimilar:' '),['class'=>'similar_product_sort polecane_products']),['class'=>'col-xs-12 col-sm-5 col-md-5 col-lg-5'])
                ,
    ['class'=>'greyArea']);
?>
