<?php
$start = 1;
$last = null;
$first = null;
if (!empty($pages) && $pages > 1) {
    if ($pages > 5) {
        $max = 3;
        $last = $pages;
        if ($page > 3) {
            $first = 1;
            $start = ($page - 2);
            if(($pages-$page)<4){
                $start=$pages-3;
                $last=null;
            }
        } else {
            $start = 1;
        }
    } else {
        $max = ($pages-1);
        $start = 1;
    }
    ?>
    <ul class="pagination">
        <?php if ($page > 1) { ?>
            <li class="prev"><a rel="prev" href="<?= (!empty($url) ? $url . '?page=' . ($page - 1) : '?page=' . ($page - 1)) ?>">Poprzednia</a></li>
            <?php
        }
        if (!empty($first)) {
            ?>
            <li class="next"><a rel="next" href="<?= (!empty($url) ? $url . '?page=' . $first : '?page=' . $last) ?>"><?= $first ?></a></li> 
            <li class="sep"><a href="">...</a> </li>  
            <?php
        }
        for ($i = $start; $i <= ($start + $max); $i++) {
            ?>
            <li class="<?= (($i == $page) ? 'active' : '') ?>"><a href="<?= (!empty($url) ? $url . '?page=' . $i : '?page=' . $i) ?>"><?= $i ?></a></li>

            <?php
        }
        if (!empty($last)) {
            ?>
            <li class="sep"><a href="">...</a> </li>
            <li class="next"><a rel="next" href="<?= (!empty($url) ? $url . '?page=' . $last : '?page=' . $last) ?>"><?= $last ?></a></li>   
            <?php
        }
        if ($page < $pages) {
            ?>   
            <li class="next"><a rel="next" href="<?= (!empty($url) ? $url . '?page=' . ($page + 1) : '?page=' . ($page + 1)) ?>">Następna</a></li>   
            <?php } ?>
    </ul>
    <?php
}

