<div class="multiple-checkbox-list-container">
    <div class="row">
        <div class="col-md-6">
            <input class="form-control multi-select-finder input-sm" data-target="#towarKategorie" placeholder="szukaj..."/>
        </div>
        <div class="col-md-6 text-right">
            <button class="btn btn-primary btn-sm showAll" type="button" data-target="#towarKategorie" data-toggle="tooltip" title="<?= $this->Txt->printAdmin(__('Admin | Rozwiń wszystkie')) ?>"><span class="glyphicon glyphicon-chevron-down"></span></button>
            <button class="btn btn-info btn-sm hideAll" type="button" data-target="#towarKategorie" data-toggle="tooltip" title="<?= $this->Txt->printAdmin(__('Admin | Zwiń wszystkie')) ?>"><span class="glyphicon glyphicon-chevron-up"></span></button>
            <button class="btn btn-warning btn-sm selectAll" type="button" data-target="#towarKategorie" data-toggle="tooltip" title="<?= $this->Txt->printAdmin(__('Admin | Zaznacz wszystkie')) ?>"><span class="glyphicon glyphicon-check"></span></button>
            <button class="btn btn-danger btn-sm unselectAll" type="button" data-target="#towarKategorie" data-toggle="tooltip" title="<?= $this->Txt->printAdmin(__('Admin | Odznacz wszystkie')) ?>"><span class="glyphicon glyphicon-ban-circle"></span></button>
        </div>
    </div>
    <?php
    echo $this->Html->tag('ul', $this->Navi->categoriesTreeInputAdmin($katArr, $kategoriaSelected), ['class' => 'multiple-checkbox-list', 'id' => 'towarKategorie']);
    ?>
</div>