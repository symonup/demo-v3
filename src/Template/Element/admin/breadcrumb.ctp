<?php
if (!empty($crumbs)) {
    ?>
    <ol class="breadcrumb">
        <?php
        foreach ($crumbs as $crumb) {
            echo $this->Html->tag('li', $this->Html->link($crumb['name'], $crumb['url']));
        }
        ?>
    </ol>
    <?php
}
?>