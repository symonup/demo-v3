<?php
$actSimilar='';
if(!empty($towar) && !empty($towar->artykul))
{
    foreach ($towar->artykul as $podobny)
    {
        $actSimilar.='<li>'.((!empty($podobny->zdjecie_1) && file_exists($filePath['artykul'].$podobny->zdjecie_1))?'<img style="max-width: 50px; float: left;" src="'.$displayPath['artykul'].$podobny->zdjecie_1.'"/>':'').$podobny->tytul.'<input type="hidden" value="'.$podobny->id.'" name="artykul[_ids][]"/></li>';
    }
}
echo $this->Html->tag('div',
                $this->Html->tag('h3',__('Artykuly'))
         .$this->Form->input('search',['type'=>'text','label'=>__('Wpisz tytuł artykułu'),'data-target'=> Cake\Routing\Router::url(['action'=>'findArtykul']).'/','id'=>'towarArtykulSearch','class'=>'form-control','div'=>['class'=>'form-group input text'], 'templates' => ['formGroup' => '{{label}}{{input}}<div class="artykul_searching"></div>']])
         .$this->Html->tag('div',$this->Html->tag('h5',__('Wyniki wyszukiwania')).$this->Html->tag('ul',' ',['class'=>'similar_product_sort artykul_search_results']),['class'=>'col-xs-12 col-sm-5 col-md-5 col-lg-5'])
                
         .$this->Html->tag('div',$this->Html->tag('h5',$this->Txt->printAdmin(__('Admin | Actions'))).$this->Html->tag('div','<span data-toggle="tooltip" data-placement="top" title="'.$this->Txt->printAdmin(__('Admin | Dodaj zaznaczone')).'" class="glyphicon glyphicon-step-forward" data-action="move-to-similar" data-content-result="artykul_search_results" data-content="artykul_products" aria-hidden="true"></span><br/>
            <span data-toggle="tooltip" data-placement="top" title="'.$this->Txt->printAdmin(__('Admin | Usuń zaznaczone')).'" class="glyphicon glyphicon-step-backward" aria-hidden="true" data-action="move-to-result" data-content-result="artykul_search_results" data-content="artykul_products"></span><br/>
            <span data-toggle="tooltip" data-placement="top" title="'.$this->Txt->printAdmin(__('Admin | Dodaj wszystkie')).'" class="glyphicon glyphicon-fast-forward" aria-hidden="true" data-action="move-all-to-similar" data-content-result="artykul_search_results" data-content="artykul_products"></span><br/>
            <span data-toggle="tooltip" data-placement="top" title="'.$this->Txt->printAdmin(__('Admin | Usuń wszystkie')).'" class="glyphicon glyphicon-fast-backward" aria-hidden="true" data-action="move-all-to-result" data-content-result="artykul_search_results" data-content="artykul_products"></span><br/>
            <span data-toggle="tooltip" data-placement="top" title="'.$this->Txt->printAdmin(__('Admin | Usuń zaznaczone')).'" class="glyphicon glyphicon-trash" aria-hidden="true" data-action="delete-similar" data-content-result="artykul_search_results" data-content="artykul_products"></span><br/>
            <span data-toggle="tooltip" data-placement="top" title="'.$this->Txt->printAdmin(__('Admin | Pokaż wszystkie')).'" class="glyphicon glyphicon-fullscreen" aria-hidden="true" data-action="full-size" data-content-result="artykul_search_results" data-content="artykul_products"></span>',['class'=>'similar_action_content']),['class'=>'col-xs-12 col-sm-2 col-md-2 col-lg-2'])
         .$this->Html->tag('div',$this->Html->tag('h5',__('Artykuly')).$this->Html->tag('ul',(!empty($actSimilar)?$actSimilar:' '),['class'=>'similar_product_sort artykul_products']),['class'=>'col-xs-12 col-sm-5 col-md-5 col-lg-5'])
                ,
    ['class'=>'greyArea']);
?>
