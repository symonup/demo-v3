<div class="row">
    <div class="col-lg-12">
        <hr/>
    </div>
</div>
<div class="row">
    <div class="col-lg-6 col-md-8 col-md-offset-2 col-lg-offset-3">
        <div id="tips-slider" class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
                <!-- Controls -->
                <div class="tips-controls">
                    <i class="fa fa-lightbulb-o fa-2x"></i>
            <?php if (count($tips) > 1) { ?>
                    <a class="left carousel-control" href="#tips-slider" role="button" data-slide="prev">
                        <i class="fa fa-chevron-left"></i> <?=$this->Txt->printAdmin(__('Tips | Poprzednia wskazówka'),'Tips | ')?>
                    </a>
                    <a class="right carousel-control" href="#tips-slider" role="button" data-slide="next">
                        <?=$this->Txt->printAdmin(__('Tips | Następna wskazówka'),'Tips | ')?> <i class="fa fa-chevron-right"></i>
                    </a>
            <?php } ?>
                </div>
            <div class="carousel-inner" role="listbox">
                <?php
                $i = 0;
                foreach ($tips as $tip) {
                    echo $this->Html->tag('div', $tip, ['class' => 'item' . (($i == 0) ? ' active' : '')]);
                    $i++;
                }
                ?>
            </div>
        </div>

    </div>
</div>