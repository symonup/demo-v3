<?php

if (!empty($zestaw)) {
    
    $produktGlowny = $this->Html->tag('div', $this->Html->tag('div', (!empty($zestaw->towar->towar_zdjecie)?$this->Html->image(str_replace('/b2b/', '/', $displayPath['towar']).$this->Txt->getKatalog($zestaw->towar->towar_zdjecie[0]->id).'/thumb_'.$zestaw->towar->towar_zdjecie[0]->plik,['alt'=>(!empty($zestaw->towar->towar_zdjecie[0]->alt)?$zestaw->towar->towar_zdjecie[0]->alt:$zestaw->towar->nazwa)]):NO_PHOTO))
    .$this->Html->tag('div',
            $zestaw->towar->nazwa.' '.
            $this->Html->tag('span',(!empty($zestaw->towar->towar_cena_bazowa)?$this->Txt->cena ((!empty($zestaw->towar->promocja) && ($zestaw->towar->towar_cena_bazowa->cena_promocja>0))?$zestaw->towar->towar_cena_bazowa->cena_promocja:$zestaw->towar->towar_cena_bazowa->cena_sprzedazy):''),['class'=>'cena']))
            ,['class'=>'image']);
        $_zestaw = '';
        if(!empty($zestaw->towar->towar_cena_bazowa)){
        $cena_zestawu = (!empty($zestaw->towar->promocja) && ($zestaw->towar->towar_cena_bazowa->cena_promocja>0))?$zestaw->towar->towar_cena_bazowa->cena_promocja:$zestaw->towar->towar_cena_bazowa->cena_sprzedazy;
        }
        else{
            $cena_zestawu=0;
        }
        $zt_i=1;
        foreach ($zestaw->zestaw_towar as $item) {
            $wymiana = $this->Html->tag('div',$item->towar->nazwa.' '.$this->Html->tag('span',(!empty($item->towar->towar_cena_bazowa)?$this->Txt->cena((!empty($item->towar->promocja) && ($item->towar->towar_cena_bazowa->cena_promocja_brutto>0))?$item->towar->towar_cena_bazowa->cena_promocja_brutto:$item->towar->towar_cena_bazowa->cena_sprzedazy):''),['class'=>'cena']));
            if(!empty($item->towar->towar_cena_bazowa)){
            $cena_zestawu+=(!empty($item->towar->promocja) && ($item->towar->towar_cena_bazowa->cena_promocja_brutto>0))?$item->towar_cena_bazowa->cena_promocja_brutto:$item->towar->towar_cena_bazowa->cena_sprzedazy;
            }
            $rabat = '';
            
            $_zestaw.=$this->Html->tag('div', $this->Html->tag('div', (!empty($item->towar->towar_zdjecie)?$this->Html->image(str_replace('/b2b/', '/', $displayPath['towar']).$this->Txt->getKatalog($item->towar->towar_zdjecie[0]->id).'/thumb_'.$item->towar->towar_zdjecie[0]->plik,['alt'=>(!empty($item->towar->towar_zdjecie[0]->alt)?$item->towar->towar_zdjecie[0]->alt:$item->towar->nazwa)]):NO_PHOTO)).$wymiana
            ,['class'=>'image'.(($zt_i==count($zestaw->zestaw_towar))?' last':'')]);
            $zt_i++;
        }
        $cena_rabat = round($cena_zestawu - (($cena_zestawu * round($zestaw->rabat, 2)) / 100), 2);
        $rabat = '';
        $ceny = '';
     echo $this->Html->tag('div', $produktGlowny . $_zestaw . $this->Html->tag('div',$rabat . $ceny,['class'=>'zestaw_podsumowanie']), ['class' => 'zestaw']);
    
}
?>
