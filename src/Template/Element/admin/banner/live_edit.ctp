<?php $this->start('css');   
 echo $this->Html->css(['banner.css?'.uniqid()]) ;
          $this->end();
  ?>
<?=$this->element('default/banner',['liveEdit'=>null])?>

<?php $this->start('bodyJs'); ?>
<script type="text/javascript">
        var ctLink = '';
        var banerId =<?= $banner->id ?>;
        var imgWidth = $('[liveEdit=true]').width();
        var imgHeight = $('[liveEdit=true]').height();
        var jc;
        $(document).ready(function() {
            imgWidth = $('[liveEdit=true]').width();
            imgHeight = $('[liveEdit=true]').height();
             jc = $('[liveEdit=true]').Jcrop({
                onSelect: showCoords,
                onChange: showCoords,
                bgColor: '',
//                boxHeight: 100,
//                boxWidth: 20,
//                aspectRatio:16/9
            });

<?php
if (!empty($baner->banner_slides)) {
    foreach ($banner->banner_slides[0]['banner_slides_elements'] as $key => $link) {
        $element=$this->Banner->element($link);
        if(!empty($element)){
            echo '$(\'#slide-element-'.$link->banner_slides_id.'-'.$link->id.'\').appendTo($(\'.jcrop-holder\'));';
        }
    }
}
?>
        });
        function myRound(val){
            return Math.round((val*100))/100;
        }
        function showCoords(c)
        {
            $('#crW').html(c.w);
            $('#crH').html(c.h);
            $('#crT').html(c.y);
            $('#crL').html(c.x);
            var left = (c.x-$('.slideElementContainer').offset().left);//((c.x * 100) / imgWidth);
            var top = c.y;//(c.y * 100) / imgHeight;
            var width = c.w;//(c.w * 100) / imgWidth;
            var height = c.h;//(c.h * 100) / imgHeight;
            $('#banner-slides-elements-' + ctLink + '-pos-left').val(myRound(left)).trigger('change');
            $('#banner-slides-elements-' + ctLink + '-pos-top').val(myRound(top)).trigger('change');
            $('#banner-slides-elements-' + ctLink + '-width').val(width).trigger('change');
            $('#banner-slides-elements-' + ctLink + '-height').val(height).trigger('change');
//            if ($('.baner_tekst_' + ctLink + '[lng=' + activeLng + ']')[0] !== undefined)
//            {
//                if ($('#newLink_' + ctLink + '_' + activeLng) && $('#newLink_' + ctLink + '_' + activeLng).length > 0)
//                {
//                    var newLink = $('#newLink_' + ctLink + '_' + activeLng);
//                    newLink.attr('style', 'position:absolute; width:' + c.w + 'px;height:' + c.h + 'px; top:' + c.y + 'px; left:' + c.x + 'px;');
//
//                }
//                else {
//                    var newLink = $('<div class="linkArea" id="newLink_' + ctLink + '_' + activeLng + '" style="position:absolute; width:' + c.w + 'px;height:' + c.h + 'px; top:' + c.y + 'px; left:' + c.x + 'px;"><span ' + (($('.link_style_' + ctLink + '[lng=' + activeLng + ']').val() != '') ? 'style="' + $('.link_style_' + ctLink + '[lng=' + activeLng + ']').val() + '"' : '') + '>' + (($('.baner_tekst_' + ctLink + '[lng=' + activeLng + ']').val() != '') ? $('.baner_tekst_' + ctLink).val() : 'Link ' + ctLink + '[lng=' + activeLng + ']') + '</span></div>');
//                    newLink.appendTo($('.jcrop-holder'));
//                }
//            }
        }
        $(document).on('change','[slide-element-cord-left]',function(){
            $($(this).attr('slide-element-cord-left')).css('left',$(this).val()+'px');
        });
        $(document).on('change','[slide-element-cord-top]',function(){
            $($(this).attr('slide-element-cord-top')).css('top',$(this).val()+'px');
        });
        $(document).on('change','[slide-element-cord-width]',function(){
            $($(this).attr('slide-element-cord-width')).css('width',$(this).val()+'px');
        });
        $(document).on('change','[slide-element-cord-height]',function(){
            $($(this).attr('slide-element-cord-height')).css('height',$(this).val()+'px');
        });
        function editLink(linkId)
        {
            ctLink = linkId;
        }
        function deleteLink(linkId)
        {
            if (confirm('Czy napewno chcesz usunąć link?'))
            {
                $('#newLink_' + linkId).remove();
                $('#linkDiv_' + linkId).remove();
            }
            else
                return false;
        }
</script>
<?php $this->end()?>