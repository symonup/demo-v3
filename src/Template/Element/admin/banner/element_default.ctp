<?php 
if(!empty($itemElement)){
    $slideElementNum=(int) $itemElement['number'];
    $slideElementLp=($slideElementNum + 1);
}
else{
    $slideElementNum='slideelementnumber';
    $slideElementLp='slideelementnumberplus1';
}
?>
<div class="panel panel-default panel-slide-element" id="slide-element-container-<?=$slideElementNum?>" pos="<?=$slideElementNum?>">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#slide-elements-container" href="#slide-element-<?=$slideElementNum?>" aria-expanded="true" class="" label="<?= $this->Txt->printAdmin(__('Admin | Element')) ?>"><?= $this->Txt->printAdmin(__('Admin | Element {0}', [$slideElementLp])) ?></a>
            <button confirm="<?=$this->Txt->printAdmin(__('Admin | Czy na pewno chcesz usunąć element slajdu'))?>" type="button" data-toggle="tooltip" data-placement="top" title="<?= $this->Txt->printAdmin(__('Admin | Usuń')) ?>" class="btn btn-danger btn-xs remove-slide-element pull-right"><i class="fa fa-remove"></i></button>
        </h4>
    </div>
    <div id="slide-element-<?=$slideElementNum?>" class="panel-collapse collapse in" aria-expanded="true" style="">
        <div class="panel-body">
            <?php
            if(!empty($itemElement) && !empty($itemElement['item'])){
                echo $this->Form->control('banner_slides_elements.'.$slideElementNum.'.id',['type'=>'hidden','value'=>$itemElement['item']['id']]);
            }
            echo $this->Form->control('banner_slides_elements.'.$slideElementNum.'.banner_slides_id',['type'=>'hidden','value'=>$bannerSlideId]);
            echo $this->Form->control('banner_slides_elements.'.$slideElementNum.'.type', ['type' => 'hidden','value'=>'html']);
            echo $this->Form->control('banner_slides_elements.'.$slideElementNum.'.is_linked', ['type' => 'checkbox','value'=>1,'checked'=>((!empty($itemElement) && !empty($itemElement['item']))?$itemElement['item']['is_linked']:false), 'label' => $this->Txt->printAdmin(__('Admin | Element podlinkowany')), 'class' => 'slide-is-linked']);

            $slideElementsTypesArray = [
                'link'=>[
                    $this->Form->control('banner_slides_elements.'.$slideElementNum.'.link', ['class' => 'form-control', 'label' => $this->Txt->printAdmin(__('Admin | Link url')),'value'=>((!empty($itemElement) && !empty($itemElement['item']))?$itemElement['item']['link']:'')]),
                    $this->Form->control('banner_slides_elements.'.$slideElementNum.'.title', ['class' => 'form-control', 'label' => $this->Txt->printAdmin(__('Admin | Link title')),'value'=>((!empty($itemElement) && !empty($itemElement['item']))?$itemElement['item']['title']:'')]),
                    $this->Form->control('banner_slides_elements.'.$slideElementNum.'.target', ['class' => 'form-control', 'label' => $this->Txt->printAdmin(__('Admin | Link target')),'value'=>((!empty($itemElement) && !empty($itemElement['item']))?$itemElement['item']['target']:''), 'type' => 'select', 'options' => ['_self' => $this->Txt->printAdmin(__('Admin | Aktualna strona')), '_blank' => $this->Txt->printAdmin(__('Admin | Nowa karta'))], 'default' => '_self']),
                
                ],
                'html' => [
                    $this->Form->control('banner_slides_elements.'.$slideElementNum.'.position', ['class' => 'form-control', 'type' => 'hidden', 'value'=>'absolute']),
                    $this->Form->control('banner_slides_elements.'.$slideElementNum.'.text', ['class' => 'form-control', 'type' => 'textarea', 'label' => $this->Txt->printAdmin(__('Admin | Html')), 'code-mirror-html' => ((!empty($itemElement) && !empty($itemElement['item']))?'true':'false'),'value'=>((!empty($itemElement) && !empty($itemElement['item']))?$itemElement['item']['text']:'')]),
                    ]
            ];
            foreach ($slideElementsTypesArray as $elementType => $inputs) {
                if (!empty($inputs)) {
                    echo $this->Html->tag('div', join('', $inputs), ['class' => 'banner-slide-element-type', 'data-type' => $elementType]);
                }
            }
            $unique= uniqid();
            echo $this->Form->control('banner_slides_elements.'.$slideElementNum.'.pos_left', ['type' => 'hidden','slide-element-cord-left'=>'#slide-element-'.$bannerSlideId.'-'.((!empty($itemElement) && !empty($itemElement['item']))?$itemElement['item']['id']:$unique),'value'=>((!empty($itemElement) && !empty($itemElement['item']))?$itemElement['item']['pos_left']:'')]);
            echo $this->Form->control('banner_slides_elements.'.$slideElementNum.'.pos_top', ['type' => 'hidden','slide-element-cord-top'=>'#slide-element-'.$bannerSlideId.'-'.((!empty($itemElement) && !empty($itemElement['item']))?$itemElement['item']['id']:$unique),'value'=>((!empty($itemElement) && !empty($itemElement['item']))?$itemElement['item']['pos_top']:'')]);
            echo $this->Form->control('banner_slides_elements.'.$slideElementNum.'._width', ['type' => 'hidden','slide-element-cord-width'=>'#slide-element-'.$bannerSlideId.'-'.((!empty($itemElement) && !empty($itemElement['item']))?$itemElement['item']['id']:$unique),'value'=>((!empty($itemElement) && !empty($itemElement['item']))?$itemElement['item']['_width']:'')]);
            echo $this->Form->control('banner_slides_elements.'.$slideElementNum.'._height', ['type' => 'hidden','slide-element-cord-height'=>'#slide-element-'.$bannerSlideId.'-'.((!empty($itemElement) && !empty($itemElement['item']))?$itemElement['item']['id']:$unique),'value'=>((!empty($itemElement) && !empty($itemElement['item']))?$itemElement['item']['_height']:'')]);
            
            echo $this->Form->control('banner_slides_elements.'.$slideElementNum.'.css', ['class' => 'form-control','type'=>'textarea','value'=>((!empty($itemElement) && !empty($itemElement['item']))?$itemElement['item']['css']:''), 'label' => $this->Txt->printAdmin(__('Admin | Css')), 'code-mirror-css' => ((!empty($itemElement) && !empty($itemElement['item']))?'true':'false')]);
            ?>
        </div>
    </div>
</div>
