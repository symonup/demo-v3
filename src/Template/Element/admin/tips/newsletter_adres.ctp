<div class="tip-head">
    <?= $this->Txt->printAdmin(__('Tips | Adresy do newslettera.'),'Tips | ')?>
</div>
<div class="tip-content">
    <p>
        <?= $this->Txt->printAdmin(__('Tips | Na {0} do newslettera znajdują się adresy wszystkich osób które zapisały się na newsletter. Adresy można usuwać ale nie można ich edytować.',[$this->Html->link(__('Tips | liście adresów'),['controller'=>'NewsletterAdres','action'=>'index'])]),'Tips | ')?>
    </p>
</div>