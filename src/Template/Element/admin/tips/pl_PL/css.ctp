<div class="tip-head">
    Własne style css strony?
</div>
<div class="tip-content">
    <p>
       Własne style css służą do modyfikacji wyglądu Twojej strony. Własne style wczytywane są na końcu więc mają pierszeństwo przed domyślnymi stylami. W tej sekcji możesz całkowicie wpłynąć na wygląd Twojej strony zmieniając jej kolory, czcionki itp.
    </p>
    <p>
        Przykład dla zmiany koloru tła na czerwone:
    <pre>
body{
    background-color: red;
}
    </pre>        
    </p>
</div>