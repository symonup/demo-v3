<div class="tip-head">
    Jak zarządzać stopką?
</div>
<div class="tip-content">
    <p>
        Stopka składa się z kilku elementów:<br/>
    <ul>
        <li>Danych firmy</li>
        <li>Informacji kontaktowych</li>
        <li>Linków do stron</li>
        <li>Linków do portali społecznościowych</li>
    </ul>
    </p>
    <p>
        Dane firmy i informacje kontaktowe należy edytować w <a href="<?= Cake\Routing\Router::url(['controller'=>'Konfiguracja','action'=>'index'])?>">konfiguracji</a>
    </p>
    <p>
        Listę linków do stron tworzymy w <a href="<?= Cake\Routing\Router::url(['controller'=>'FooterLink','action'=>'index'])?>">Linki w stopce</a> oraz przy każdej stronie można zaznaczyć czy link do tej strony ma wyświetlać się w stopce, wówczas link do strony wyświetlany jest niezależnie od tego czy został dodany w linkach stopki.
    </p>
    <p>
        Linki do portali społecznościowych należy edytować w <a href="<?= Cake\Routing\Router::url(['controller'=>'Konfiguracja','action'=>'index'])?>">konfiguracji</a>. Jeżeli nie chcesz wyświetlać jakiegoś linku pozostaw jego adres pusty.
    </p>
</div>