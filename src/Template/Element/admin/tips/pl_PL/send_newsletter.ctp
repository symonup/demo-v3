<div class="tip-head">
    <?=$this->Txt->printAdmin(__('Tips | Jak wysłać newsletter'),'Tips | ')?>
</div>
<div class="tip-content">
    <p class="text-left">
        <?=$this->Txt->printAdmin(__('Tips | Aby wysłać newsletter uruchom crona wywołującego ten link: {0}',[$this->Html->tag('b',\Cake\Routing\Router::url(['controller' => 'Newsletter', 'action' => 'cron', \Cake\Core\Configure::read('newsletter.token'), 'prefix' => false], true))]),'Tips | ')?>
    </p>
    <p>
        <?=$this->Txt->printAdmin(__('Tips | Token identyfikacyjny możesz zmienić w {0}',[$this->Html->link(__('Tips | konfoguracji'),['controller' => 'Konfiguracja', 'action' => 'index'])]),'Tips | ')?>
    </p>
</div>