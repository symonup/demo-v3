<div class="tip-head">
    Czym są bloki?
</div>
<div class="tip-content">
    <p>
    Bloki to elementy HTML które wchodzą w skład Twojej strony. Każda strona, którą tworzysz lub edytujesz może składać się z wielu bloków. Bloki są przydatne zwłaszcza kiedy na wielu stronach umieszczasz takie same elementy, wówczas tworzysz blok i dodajesz go do swoich stron. Kiedy chcesz coś zmienić edytujesz tylko blok a zmiana jest automatycznie widoczna na wszystkich stronach na których ten blok został dodany.
    </p>
    <p>
        Istnieją bloki stałe których nie można edytować z poziomu CMS. Są to bloki np. formularzy kontaktowych. Bloki stałe możesz również wykożystać w tworzonych przez Ciebie blokach wstawiając w kod HTML odpowiedni zaczep.
    </p>
    <p>
        Aby dodać nowe lub edytować istniejące bloki przejdź do <a href="<?= Cake\Routing\Router::url(['controller'=>'Blok','action'=>'index'])?>">listy bloków</a>
    </p>
</div>