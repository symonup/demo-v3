<div class="tip-head">
    Jak zarządzać menu?
</div>
<div class="tip-content">
    <p>
        W menu możesz dodawać i edytować linki. Linki mogą zawierać elementy podrzędne. Tworząc link musisz podać etykietę widoczną na stronie i adres url gdzie link ma prowadzić.
    </p>
</div>