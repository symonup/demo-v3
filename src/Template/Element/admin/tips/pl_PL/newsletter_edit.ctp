<div class="tip-head">
    <?= $this->Txt->printAdmin(__('Tips | Zmienne jakich możesz użyć w szablonie. Zmienne są zamieniane z danymi z systemu np. Imię i Nazwisko klienta'), 'Tips | ') ?>
</div>
<div class="tip-content">
    <p>
        <?php
        $viribles = [
            '[%preview-url%]' => $this->Txt->printAdmin(__('Tips | Link do podglądu newslettera na stronie.'), 'Tips | '),
            '[%unsubscribe-url%]' => $this->Txt->printAdmin(__('Tips | Link do wypisania się z newslettera. (Dostępny tylko dla newsletterów skierowanych do osób zapisanych przez stronę www)'), 'Tips | '),
            '[%name%]' => $this->Txt->printAdmin(__('Tips | Imię klienta lub pracownika (Dostępne tylko dla newsletterów skierowanych do klientów lub pracowników)'), 'Tips | ')
        ];
        $tds = [];
        if (!empty($viribles)) {
            foreach ($viribles as $key => $desc) {
                $tds[] = $this->Html->tag('tr', $this->Html->tag('td', $key,['class'=>'tip-virible']) . $this->Html->tag('td', $desc));
            }
        }
        if (!empty($tds)) {
            ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <td class="tip-virible"><?= $this->Txt->printAdmin(__('Tips | Zmienna'), 'Tips | ') ?></td>
                    <td><?= $this->Txt->printAdmin(__('Tips | Opis'), 'Tips | ') ?></td>
                </tr>
            </thead>
            <tbody>
                <?= join('', $tds) ?>
            </tbody>
        </table>
        <?php
    }
    ?>
</p>
</div>