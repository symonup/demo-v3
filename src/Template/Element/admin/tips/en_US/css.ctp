<div class="tip-head">
    Own page css styles.
</div>
<div class="tip-content">
    <p>
       Your own css styles are used to modify the appearance of your website. Custom styles are loaded at the end so they have priority over default styles. In this section you can completely influence the appearance of your website by changing its colors, fonts etc.
    </p>
    <p>
        Example for changing the background color to red:
    <pre>
body{
    background-color: red;
}
    </pre>        
    </p>
</div>