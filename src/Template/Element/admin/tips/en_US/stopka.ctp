<div class="tip-head">
    How to manage the footer?
</div>
<div class="tip-content">
    <p>
        The foot consists of several elements:<br/>
    <ul>
        <li>Company data</li>
        <li>Contact information</li>
        <li>Links to pages</li>
        <li>Links to social networks</li>
    </ul>
    </p>
    <p>
        Company data and contact information should be edited in <a href="<?= Cake\Routing\Router::url(['controller'=>'Konfiguracja','action'=>'index'])?>">configuration</a>
    </p>
    <p>
        The list of links to the pages we create in <a href="<?= Cake\Routing\Router::url(['controller'=>'FooterLink','action'=>'index'])?>">"Footer links"</a> and on each page you can select whether the link to this page is displayed in the footer, then the link to the page is displayed regardless of whether it was added in the footer links.
    </p>
    <p>
        Links to social networks should be edited in <a href="<?= Cake\Routing\Router::url(['controller'=>'Konfiguracja','action'=>'index'])?>">configuration</a>. If you do not want to display a link, leave it empty.
    </p>
</div>