<div class="tip-head">
    Configuration
</div>
<div class="tip-content">
    <p>
        <a href="<?= Cake\Routing\Router::url(['controller'=>'Konfiguracja','action'=>'index'])?>">In the configuration</a> , we make basic settings for the website.
        </p>
        <p>
            Company's data:<br/>
        <ul>
            <li>Company name - The name of your company is displayed, among others, in the footer.</li>
            <li>Telephone - The contact phone is displayed, among others, in the footer.</li>
            <li>Primary email address - Email address to which inquiries from contact forms will come.</li>
            <li>Company data - Company address data is displayed in the footer.</li>
        </ul>
        </p>
        <p>
            SMTP settings:<br/>
        These are the settings necessary to send e-mails from the contact form and the newsletter. Configure the settings according to your email operator's instructions.
        </p>
        <p>
            SEO settings - these are the default settings, they will be displayed when the pages do not have their own SEO settings.
        </p>
        <p>
            Links to social networks - Here you need to define adesses of pages on social networks in which your company is active. If the address is empty, the link will not be displayed on the page.
        </p>
        <p>
            Newsletter:
        <ul>
            <li>Security token - the code that secures the address of sending the newsletter, it prevents the launch of the newsletter sending people who do not know this code.</li>
            <li>Limit - the limit of addresses for sending mailings after one call of the newsletter. This is an anti-spam protection.</li>
        </ul>
        </p>
        <p>
            Scripts:<br/>
            In scripts, you can place various kinds of remarketing scripts, widgets, plugins.<br/>
            Scripts from the head section are read just before closing the head tag.<br/>
            Scripts from the body section are read just before closing the body tag.<br/>
        </p>
</div>