<div class="tip-head">
    How do you send a newsletter?
</div>
<div class="tip-content">
    <p class="text-left">
        To send the newsletter, launch the cron calling this link: <b><?=\Cake\Routing\Router::url(['controller' => 'Newsletter', 'action' => 'cron', \Cake\Core\Configure::read('newsletter.token'), 'prefix' => false], true)?></b>
    </p>
    <p>
        You can change the identification token in <a href="<?= \Cake\Routing\Router::url(['controller' => 'Konfiguracja', 'action' => 'index'])?>">confoguration</a>
    </p>
</div>