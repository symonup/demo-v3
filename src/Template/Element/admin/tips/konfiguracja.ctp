<div class="tip-head">
    Konfiguracja
</div>
<div class="tip-content">
    <p>
        W <a href="<?= Cake\Routing\Router::url(['controller'=>'Konfiguracja','action'=>'index'])?>">konfiguracji</a> robimy podstawowe ustawienia dla strony.
        </p>
        <p>
            Dane firmy:<br/>
        <ul>
            <li>Nazwa firmy - Nazwa Twojej firmy wyświetla się między innymi w stopce.</li>
            <li>Telefon - Telefon kontaktowy wyświetla się między innymi w stopce.</li>
            <li>Główny adres e-mail - Adres email na który będą przychodziły zapytania z formularzy kontaktowych.</li>
            <li>Dane firmy - Dane adresowe firmy wyświetlają się w stopce.</li>
        </ul>
        </p>
        <p>
            Ustawienia SMTP:<br/>
        Są to ustawienia niezbędne do wysyłki maili z formularza kontaktowego oraz newslettera. Skonfiguruj ustawienia zgodnie z zaleceniami Twojego operatora poczty.
        </p>
        <p>
            Ustawienia SEO - są to domyślne ustawieia, będą wyświetlane w momencie kiedy strony nie mają podanych własnych ustawień SEO.
        </p>
        <p>
            Linki do portali społecznościowych - Tutaj należy zdefiniować adesy stron w portalach społecznościowych w których Twoja firma jest aktywna. Jeżeli adres zostanie pusty link nie będzie wyświetlał się na stronie.
        </p>
        <p>
            Newsletter:
        <ul>
            <li>Token zabezpieczający - kod który zabezpiecza adres wysyłki newslettera, uniemożliwia to uruchomienie wysyłki newslettera osobom nie znającym tego kodu.</li>
            <li>Limit - limit adresów do wysyłki mailingu za jednym wywołaniem wysyłki newslettera. Jest to zabezpieczenie antyspamowe.</li>
        </ul>
        </p>
        <p>
            Skrypty:<br/>
            W skryptach możesz umieścić różnego rodzaju skrypty remarketingowe, widgety, wtyczki.<br/>
            Skrypty z sekcji head wczytywane są tuż przed zamknięciem znacznika head.<br/>
            Skrypty z sekcji body wczytywane są tuż przed zamknięciem znacznika body.<br/>
        </p>
</div>