<div class="tip-head">
    How to create websites?
</div>
<div class="tip-content">
    <p>
        To create a new page, go to <a href="<?= Cake\Routing\Router::url(['controller'=>'Strona','action'=>'index'])?>">the list of pages</a><br/>
        Then <a href="<?= Cake\Routing\Router::url(['controller'=>'Strona','action'=>'add'])?>">add a new page</a>
    </p>
    <p>
        By creating page content you can use predefined elements (blocks and banners) or add a new HTML element and create your own content.<br/>
        Before saving the page, you can see her look by clicking the preview. Clicking on the preview does not save the page but generates its preview.<br/>
        You can easily manage the order of elements on the page by moving them to the right places.
    </p>
</div>