<div class="tip-head">
    How to create banners?
</div>
<div class="tip-content">
    <p>
        To create or edit a banner, go to <a href="<?= Cake\Routing\Router::url(['controller'=>'Banner','action'=>'index'])?>">the list of banners</a><br/>
        Then select the banner to edit or add a new banner <a href="<?= Cake\Routing\Router::url(['controller'=>'Banner','action'=>'add'])?>">add a new banner</a>
    </p>
    <p>
        First you create the banner base, give it a name (to make it easier to identify when inserting on the page) and optionally define the css style.
    </p>
    <p>
        The next step is to create slides.<br/>
        Each banner can consist of multiple slides, each slide is added and edited separately.<br/>
        In the first step, add the slide background, specify its height and optionally enter the css parameters.<br/>
    </p>
    <p>
        After saving the slide base, you can go to create slide elements. Slide elements are HTML elements that you can insert and deploy anywhere on the slide.<br/>
        Each slide element is independent, it can be placed anywhere and have their own css styles.<br/>
        Slide elements should be placed on the preview, marking the area on which the element is to be found.<br/>
        Slide elements can be HTML tags as well as plain text.
    </p>
</div>