<div class="tip-head">
Variables that you can use in the template. Variables are exchanged with data from the system, eg. Name and surname of the customer
</div>
<div class="tip-content">
    <p>
        <?php 
        $viribles=[
            'przypomnienie_nastepna_usluga'=>[
                '[%service-date%]' => $this->Txt->printAdmin(__('Tips | Data wykonania usługi'),'Tips | '),
                        '[%service-time%]' => $this->Txt->printAdmin(__('Tips | Godzina rozpoczęcia pracy'),'Tips | '),
                        '[%customer-name%]' => $this->Txt->printAdmin(__('Tips | Imię klienta'),'Tips | '),
                        '[%customer-lastname%]' => $this->Txt->printAdmin(__('Tips | Nazwisko klienta'),'Tips | ')
            ],
            'informacja_o_ostatniej_usludze'=>[
                '[%last-service-list-tomorow%]' => $this->Txt->printAdmin(__('Tips | Lista klientów z ostatnim terminem na jutro'),'Tips | '),
                        '[%last-service-list-today%]' => $this->Txt->printAdmin(__('Tips | Lista klientów z ostatnim terminem na dzisiaj'),'Tips | ')
            ]
        ];
        $tds=[];
        if(!empty($templateName) && !empty($viribles[$templateName])){
            foreach ($viribles[$templateName] as $key => $desc){
                $tds[]=$this->Html->tag('tr',$this->Html->tag('td',$key,['class'=>'tip-virible']).$this->Html->tag('td',$desc));
            }
        }
        if(!empty($tds)){
            ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <td class="tip-virible"><?=$this->Txt->printAdmin(__('Tips | Zmienna'),'Tips | ')?></td>
                <td><?=$this->Txt->printAdmin(__('Tips | Opis'),'Tips | ')?></td>
            </tr>
        </thead>
        <tbody>
            <?= join('',$tds)?>
        </tbody>
    </table>
        <?php
        }
        ?>
        </p>
</div>