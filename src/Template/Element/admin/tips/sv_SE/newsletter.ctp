<div class="tip-head">
    How to create newsletters?
</div>
<div class="tip-content">
    <p>
        Creating a newsletter is very simple. When you go to <a href="<?= \Cake\Routing\Router::url(['controller'=>'NewsletterAdres','action'=>'index'])?>">the list of newsletters</a>, add a new newsletter. Newsletters are created in the form of HTML.
    </p>
</div>