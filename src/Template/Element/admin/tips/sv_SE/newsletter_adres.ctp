<div class="tip-head">
    Addresses to the newsletter.
</div>
<div class="tip-content">
    <p>
        <a href="<?= \Cake\Routing\Router::url(['controller'=>'NewsletterAdres','action'=>'index'])?>">The list of addresses</a> to the newsletter contains addresses of all people who subscribed to the newsletter. Addresses can be deleted but can not be edited.
    </p>
</div>