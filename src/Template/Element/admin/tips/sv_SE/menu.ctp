<div class="tip-head">
    How to manage the menu?
</div>
<div class="tip-content">
    <p>
        On the menu, you can add and edit links. Links may contain child elements. When creating a link, you must provide the label on the page and the address of the url where the link is to lead.
    </p>
</div>