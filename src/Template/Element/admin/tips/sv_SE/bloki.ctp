<div class="tip-head">
    What are blocks?
</div>
<div class="tip-content">
    <p>
    Blocks are HTML elements that are part of your site. Each page you create or edit can consist of multiple blocks. Blocks are useful especially when you place the same elements on many pages, then you create a block and add it to your pages. When you want to change something you edit only the block and the change is automatically visible on all pages where this block has been added.
    </p>
    <p>
        There are fixed blocks that can not be edited from the CMS level. These are blocks of, for example, contact forms. You can also use fixed blocks in the blocks you create by inserting the appropriate anchor into the HTML code.
    </p>
    <p>
        To add new or edit existing blocks, go to <a href="<?= Cake\Routing\Router::url(['controller'=>'Blok','action'=>'index'])?>">the list of blocks</a>
    </p>
</div>