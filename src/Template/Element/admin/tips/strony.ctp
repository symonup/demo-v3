<div class="tip-head">
    Jak tworzyć strony?
</div>
<div class="tip-content">
    <p>
        Aby stworzyć nową stronę przejdź do <a href="<?= Cake\Routing\Router::url(['controller'=>'Strona','action'=>'index'])?>">listy stron</a><br/>
        Następnie <a href="<?= Cake\Routing\Router::url(['controller'=>'Strona','action'=>'add'])?>">dodaj nową stronę</a>
    </p>
    <p>
        Tworząc treść strony możesz kożystać z predefiniowanych elementów (bloków i banerów) lub dodać nowy element HTML i stworzyć własne treści.<br/>
        Przed zapisaniem strony możesz podejrzeć jej wygląg klikając w podgląd. Kliknięcie w podgląd nie zapisuje strony tylko generuje jej podgląd.<br/>
        W łatwy sposób możesz zarządzać kolejnością elementów na stronie przesuwając je w odpowiednie miejsca.
    </p>
</div>