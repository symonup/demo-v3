<div class="tip-head">
    Jak tworzyć banery?
</div>
<div class="tip-content">
    <p>
        Aby stworzyć lub edytować baner przejdź do <a href="<?= Cake\Routing\Router::url(['controller'=>'Banner','action'=>'index'])?>">listy banerów</a><br/>
        Następnie wybierz baner do edycji lub <a href="<?= Cake\Routing\Router::url(['controller'=>'Banner','action'=>'add'])?>">dodaj nowy baner</a>
    </p>
    <p>
        W pierwszej kolejności tworzysz podstawę banera, nadając mu nazwę (aby ułatwić identyfikację przy wstawianiu na stronę) i opcjonalnie określasz style css.
    </p>
    <p>
        Kolejnym etapem jest tworzenie slajdów.<br/>
        Każdy baner może składać się z wielu slajdów, każdy slajd dodaje i edytuje się osobno.<br/>
        W pierwszym kroku należy dodać tło slajdu, określić jego wysokość i opcjonalnie podać parametry css.<br/>
    </p>
    <p>
        Po zapisaniu podstawy slajdu, możesz przejść do tworzenia elementów slajdów. Elementy slajdu są to elementy HTML które możesz wstawiać i rozmieszczać w dowolnym miejscu na slajdzie.<br/>
        Każdy element slajdu jest niezależny, może być umieszczony w dowolnym miejscu i posiadać własne style css.<br/>
        Elementy slajdów należy rozmieszczać na podglądzie, zaznaczając obszar na który element ma się znaleźć.<br/>
        Elementy slajdu mogą być znacznikami HTML jak również zwykłym tekstem.
    </p>
</div>