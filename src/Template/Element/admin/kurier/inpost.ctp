<?php
//\Cake\Core\Configure::read('siodemka.nazwisko_nadawcy')
$telefon=trim(str_replace(['+',' ','.','-'],['','','',''],$zamowienie->telefon));


$csvLabels=['e-mail','telefon','rozmiar','paczkomat','numer_referencyjny','ubezpieczenie','za_pobraniem','imie_i_nazwisko','nazwa_firmy','ulica','kod_pocztowy','miejscowosc','typ_przesylki'];

$labelsI=0;
$csvPos=0;
$imie_nazwisko=$zamowienie->imie.' '.$zamowienie->nazwisko;
$firma = (!empty($zamowienie->firma)?$zamowienie->firma:'');
$ulica = $zamowienie->wysylka_ulica.(!empty($zamowienie->wysylka_nr_domu)?' '.$zamowienie->wysylka_nr_domu.(!empty($zamowienie->wysylka_nr_lokalu)?'/'.$zamowienie->wysylka_nr_lokalu:''):'');
$email = $zamowienie->email;
$kodPocztowy=$zamowienie->wysylka_kod;
$kraj = $zamowienie->wysylka_kraj;
$miasto = $zamowienie->wysylka_miasto;
$paczkomat = $zamowienie->paczkomat;
$nrRef = $zamowienie->numer;
$ubezpieczenie = round($zamowienie->wartosc_produktow,2);
$zaPobraniem = (($zamowienie->rodzaj_platnosci_id==1)?round($zamowienie->wartosc_razem,2):'');

use Cake\Routing\Router;
echo $this->Form->create(NULL, array('url' => Router::url(array('controller' => 'Zamowienie', 'action' => 'kurier_csv', $zamowienie->id,'inpost'))));
echo $this->Html->tag('h3', 'DPD - plik csv');
echo '<div class="shipCont col-xs-12 col-sm-12 col-md-4 col-lg-4"><h3>Informacje o przesyłce</h3>';
echo $this->Form->input('csv.'.$csvPos, array('label'=>$csvLabels[$labelsI],'value'=>$email));
echo $this->Form->input('labels.'.$csvPos++, array('type' => 'hidden','value'=>$csvLabels[$labelsI++]));
echo $this->Form->input('csv.'.$csvPos, array('label'=>$csvLabels[$labelsI],'value'=>$telefon));
echo $this->Form->input('labels.'.$csvPos++, array('type' => 'hidden','value'=>$csvLabels[$labelsI++]));
echo $this->Form->input('csv.'.$csvPos, array('label'=>$csvLabels[$labelsI],'type'=>'select','options'=>['A'=>'A (8 x 38 x 64 cm)','B'=>'B (19 x 38 x 64 cm)','C'=>'C (41 x 38 x 64 cm)'],'value'=> 'A'));
echo $this->Form->input('labels.'.$csvPos++, array('type' => 'hidden','value'=>$csvLabels[$labelsI++]));
echo $this->Form->input('csv.'.$csvPos, array('label'=>$csvLabels[$labelsI],'value'=> $paczkomat));
echo $this->Form->input('labels.'.$csvPos++, array('type' => 'hidden','value'=>$csvLabels[$labelsI++]));
echo $this->Form->input('csv.'.$csvPos, array('label'=>$csvLabels[$labelsI],'type'=>'text','value'=>$nrRef));
echo $this->Form->input('labels.'.$csvPos++, array('type' => 'hidden','value'=>$csvLabels[$labelsI++]));
echo $this->Form->input('csv.'.$csvPos, array('label'=>$csvLabels[$labelsI],'value'=>$ubezpieczenie));
echo $this->Form->input('labels.'.$csvPos++, array('type' => 'hidden','value'=>$csvLabels[$labelsI++]));
echo $this->Form->input('csv.'.$csvPos, array('label'=>$csvLabels[$labelsI],'value'=>$zaPobraniem));
echo $this->Form->input('labels.'.$csvPos++, array('type' => 'hidden','value'=>$csvLabels[$labelsI++]));
echo '</div>';
echo '<div class="shipCont col-xs-12 col-sm-12 col-md-4 col-lg-4"><h3>Informacje o odbiorcy</h3>';
echo $this->Form->input('csv.'.$csvPos, array('label'=>$csvLabels[$labelsI],'value'=>$imie_nazwisko));
echo $this->Form->input('labels.'.$csvPos++, array('type' => 'hidden','value'=>$csvLabels[$labelsI++]));
echo $this->Form->input('csv.'.$csvPos, array('type'=>'text','label'=>$csvLabels[$labelsI],'value'=>$firma));
echo $this->Form->input('labels.'.$csvPos++, array('type' => 'hidden','value'=>$csvLabels[$labelsI++]));
echo $this->Form->input('csv.'.$csvPos, array('type'=>'text','label'=>$csvLabels[$labelsI],'value'=>$ulica));
echo $this->Form->input('labels.'.$csvPos++, array('type' => 'hidden','value'=>$csvLabels[$labelsI++]));
echo $this->Form->input('csv.'.$csvPos, array('label'=>$csvLabels[$labelsI],'value'=>$kodPocztowy));
echo $this->Form->input('labels.'.$csvPos++, array('type' => 'hidden','value'=>$csvLabels[$labelsI++]));
echo $this->Form->input('csv.'.$csvPos, array('label'=>$csvLabels[$labelsI],'value'=>$miasto));
echo $this->Form->input('labels.'.$csvPos++, array('type' => 'hidden','value'=>$csvLabels[$labelsI++]));
echo $this->Form->input('csv.'.$csvPos, array('type'=>'select','options'=>['paczkomaty'=>'Paczkomat','kurier'=>'Kurier'],'label'=>$csvLabels[$labelsI],'value'=>(!empty($paczkomat)?'paczkomaty':'kurier')));
echo $this->Form->input('labels.'.$csvPos++, array('type' => 'hidden','value'=>$csvLabels[$labelsI++]));
echo '</div>';
echo $this->Html->tag('div', $this->Html->tag('button', '<span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> ' . __('Pobierz'), ['class' => 'btn btn-success btn-sm', 'type' => 'submit', 'escape' => false]), ['class' => 'submit text-right col-xs-12']);
echo $this->Form->end();
?>
<script type="text/javascript">
    
</script>


