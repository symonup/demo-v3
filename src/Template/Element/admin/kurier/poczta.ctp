<?php
$daneForm = array('Odbiorca' => array(
        'nazwa' => trim($zamowienie->imie) . ' ' . trim($zamowienie->nazwisko),
        'nazwa2' => $zamowienie->firma,
        'ulica' => $zamowienie->wysylka_ulica,
        'dom' => $zamowienie->wysylka_nr_domu,
        'lokal' => !empty($zamowienie->wysylka_nr_lokalu) ? $zamowienie->wysylka_nr_lokalu : '',
        'miasto' => $zamowienie->wysylka_miasto,
        'kod' => str_replace('-', '', $zamowienie->wysylka_kod)
        ),
    'Wartosc'=>array(
        'wartosc_przesylki'=>str_replace('.','',($zamowienie->wartosc_produktow/100)),
        'wartosc_razem'=>str_replace('.','',($zamowienie->wartosc_razem/100))
    ));

$pocztaOpt=array('845'=>'Przesyłka polecona',
'846'=>'Paczka pocztowa',
'848'=>'Przesyłka pobraniowa',
'840'=>'Przesyłka listowa zwykła',
'850'=>'E-PRZESYŁKA',
'812'=>'POCZTEX');
echo $this->Form->input('RodzajPrzesylki',array('options'=>$pocztaOpt,'id'=>'pocztaOpt','empty'=>'Wybierz rodzaj przesyłki'));
echo $this->Html->tag('div','&nbsp;',array('id'=>'pocztaContent'));


?>
<script type="text/javascript">
$('#pocztaOpt').change(function(){
    $.ajax({
        type:'post',
        url:'/admin/wysylka/poczta_form/<?php echo $zamowienie->id; ?>',
        data:{
            typ:$('#pocztaOpt').val(),
            dane: <?php echo json_encode($daneForm); ?>
        },
        success: function(msg){
            $('#pocztaContent').html(msg);
        }
    });
});
$('#NadawcaAdminSzczegolyForm').submit(function(){
if($('#pocztaOpt').val()===''); return false;
});
</script>