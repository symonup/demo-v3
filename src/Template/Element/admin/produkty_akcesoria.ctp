<?php
$actSimilar='';
if(!empty($towar) && !empty($towar->akcesoria))
{
    foreach ($towar->akcesoria as $podobny)
    {
        $actSimilar.='<li>'.((!empty($podobny->towar_zdjecie) && file_exists($filePath['towar'].'thumb_'.$podobny->towar_zdjecie[0]->plik))?'<img style="max-width: 50px; float: left;" src="'.$displayPath['towar'].'thumb_'.$podobny->towar_zdjecie[0]->plik.'"/>':'').$podobny->nazwa.'<input type="hidden" value="'.$podobny->id.'" name="akcesoria[_ids][]"/></li>';
    }
}
echo $this->Html->tag('div',
        '<input type="hidden" name="akcesoria[_ids][]"/>'.
                $this->Html->tag('h3',$this->Txt->printAdmin(__('Admin | Zamienniki')))
         .$this->Form->input('search',['type'=>'text','data-target'=> Cake\Routing\Router::url(['action'=>'findSimilar']).'/','label'=>$this->Txt->printAdmin(__('Admin | Wyszukaj po nazwie lub kodzie produktu.')),'id'=>'akcesoriaSearch','class'=>'form-control','div'=>['class'=>'form-group input text'], 'templates' => ['formGroup' => '{{label}}{{input}}<div class="searching"></div>']])
         .$this->Html->tag('div',$this->Html->tag('h5',$this->Txt->printAdmin(__('Admin | Wyniki wyszukiwania'))).$this->Html->tag('ul',' ',['class'=>'similar_product_sort akcesoria_search_results']),['class'=>'col-xs-12 col-sm-5 col-md-5 col-lg-5'])
                
         .$this->Html->tag('div',$this->Html->tag('h5',$this->Txt->printAdmin(__('Admin | Actions'))).$this->Html->tag('div','<span data-toggle="tooltip" data-placement="top" title="'.$this->Txt->printAdmin(__('Admin | Dodaj zaznaczone produkty')).'" class="glyphicon glyphicon-step-forward" data-action="move-to-similar" data-content-result="akcesoria_search_results" data-content="akcesoria_products" aria-hidden="true"></span><br/>
            <span data-toggle="tooltip" data-placement="top" title="'.$this->Txt->printAdmin(__('Admin | Usuń zaznaczone produkty')).'" class="glyphicon glyphicon-step-backward" aria-hidden="true" data-action="move-to-result" data-content-result="akcesoria_search_results" data-content="akcesoria_products"></span><br/>
            <span data-toggle="tooltip" data-placement="top" title="'.$this->Txt->printAdmin(__('Admin | Dodaj wszystkie')).'" class="glyphicon glyphicon-fast-forward" aria-hidden="true" data-action="move-all-to-similar" data-content-result="akcesoria_search_results" data-content="akcesoria_products"></span><br/>
            <span data-toggle="tooltip" data-placement="top" title="'.$this->Txt->printAdmin(__('Admin | Usuń wszystkie')).'" class="glyphicon glyphicon-fast-backward" aria-hidden="true" data-action="move-all-to-result" data-content-result="akcesoria_search_results" data-content="akcesoria_products"></span><br/>
            <span data-toggle="tooltip" data-placement="top" title="'.$this->Txt->printAdmin(__('Admin | Usuń zaznaczone')).'" class="glyphicon glyphicon-trash" aria-hidden="true" data-action="delete-similar" data-content-result="akcesoria_search_results" data-content="akcesoria_products"></span><br/>
            <span data-toggle="tooltip" data-placement="top" title="'.$this->Txt->printAdmin(__('Admin | Pokaż wszystkie')).'" class="glyphicon glyphicon-fullscreen" aria-hidden="true" data-action="full-size" data-content-result="akcesoria_search_results" data-content="akcesoria_products"></span>',['class'=>'similar_action_content']),['class'=>'col-xs-12 col-sm-2 col-md-2 col-lg-2'])
         .$this->Html->tag('div',$this->Html->tag('h5',$this->Txt->printAdmin(__('Admin | Zamienniki'))).$this->Html->tag('ul',(!empty($actSimilar)?$actSimilar:' '),['class'=>'similar_product_sort akcesoria_products']),['class'=>'col-xs-12 col-sm-5 col-md-5 col-lg-5'])
                ,
    ['class'=>'greyArea']);
?>
