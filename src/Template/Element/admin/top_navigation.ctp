<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-2x fa-user"></i> <?=(!empty($user)?(((!empty($user['imie']) && !empty($user['nazwisko']))?$user['imie'].' '.$user['nazwisko']:$user['login'])):'')?>
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li><a href="<?= Cake\Routing\Router::url(['controller'=>'Index','action'=>'logout'])?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                    </ul>
                </li>
                <li role="presentation" data-toggle="tooltip" data-placement="bottom" title="<?=$this->Txt->printAdmin(__('Admin | Przejdź do strony'))?>"><a href="/" target="_blank"><i class="fa fa-external-link-square"></i></a></li>
                <li role="presentation" class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-envelope-o"></i>
                        <span id="alerts-count" class="badge bg-green"></span>
                    </a>
                    <ul id="alerts-menu" class="dropdown-menu list-unstyled msg_list" role="menu">
                        <li><span><?=$this->Txt->printAdmin(__('Admin | Brak wiadomości'))?></span></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>