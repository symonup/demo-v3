<tr>
    <td><?= $this->Number->format($konfiguracja->id) ?></td>
    <td><?= $this->Txt->printAdmin(__('Config | ' . $konfiguracja->label)) ?></td>
    <td><?php
        if ($konfiguracja->typ == 'script') {
            echo $this->Form->control('wartosc', ['value' => $konfiguracja->wartosc, 'type' => 'textarea', 'disabled' => true, 'label' => false, 'style' => 'width:580px;', 'class' => 'form-control']);
        } elseif ($konfiguracja->rodzaj == 'checkbox') {
            echo $this->Txt->printBool($konfiguracja->wartosc);
        } elseif ($konfiguracja->rodzaj == 'textarea' && empty($konfiguracja->html)) {
            echo nl2br($konfiguracja->wartosc);
        } elseif ($konfiguracja->rodzaj == 'password') {
            echo '********';
        } elseif ($konfiguracja->rodzaj == 'select_multiple') {
            $wartosci = explode(',', $konfiguracja->wartosc);
            if (!empty($konfiguracja->options)) {
                $tmpOpt = explode('|', $konfiguracja->options);
                $options = [];
                foreach ($tmpOpt as $optionArr) {
                    $tmpOption = explode('=', $optionArr);
                    $options[$tmpOption[0]] = $this->Txt->printAdmin(__('Admin | ' . (key_exists(1, $tmpOption) ? $tmpOption[1] : $tmpOption[0])));
                }
                foreach ($wartosci as &$wartosc) {
                    if (key_exists($wartosc, $options)) {
                        $wartosc = $options[$wartosc];
                    }
                }
            }
            echo '<span class="cfg-multiple-value">' . join('</span> <span class="cfg-multiple-value">', $wartosci) . '</span>';
        } else {
            echo $konfiguracja->wartosc;
        }
        ?></td>
    <td class="actions">
        <?= $this->Html->link('<i class="fa fa-pencil"></i>', ['action' => 'edit', $konfiguracja->id], ['escape' => false, 'class' => 'btn btn-default btn-xs', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $this->Txt->printAdmin(__('Admin | Edytuj'))]) ?>
    </td>
</tr>