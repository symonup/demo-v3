<?php if ($descItemType == 'text') { ?>
    <div class="row">
        <div class="col-xs-12">
            <textarea><?= ((!empty($descValue) && !empty($descValue['text'])) ? $descValue['text'] : '') ?></textarea>
        </div>
    </div>
<?php } if ($descItemType == 'image') { ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="image-desceription"><?= ((!empty($descValue) && !empty($descValue['images'])) ? $descValue['images'][0] : '<i class="fa fa-image"></i>') ?></div>
        </div>
    </div>
<?php } if ($descItemType == 'text-image') { ?>
    <div class="row">
        <div class="col-xs-6">
            <textarea><?= ((!empty($descValue) && !empty($descValue['text'])) ? $descValue['text'] : '') ?></textarea>
        </div>
        <div class="col-xs-6">
            <div class="image-desceription"><?= ((!empty($descValue) && !empty($descValue['images'])) ? $descValue['images'][0] : '<i class="fa fa-image"></i>') ?></div>
        </div>
    </div>
<?php } if ($descItemType == 'image-text') { ?>
    <div class="row">
        <div class="col-xs-6">
            <div class="image-desceription"><?= ((!empty($descValue) && !empty($descValue['images'])) ? $descValue['images'][0] : '<i class="fa fa-image"></i>') ?></div>
        </div>
        <div class="col-xs-6">
            <textarea><?= ((!empty($descValue) && !empty($descValue['text'])) ? $descValue['text'] : '') ?></textarea>
        </div>
    </div>
<?php } if ($descItemType == 'image-image') { ?>
    <div class="row">
        <div class="col-xs-6">
            <div class="image-desceription"><?= ((!empty($descValue) && !empty($descValue['images'])) ? $descValue['images'][0] : '<i class="fa fa-image"></i>') ?></div>
        </div>
        <div class="col-xs-6">
            <div class="image-desceription"><?= ((!empty($descValue) && !empty($descValue['images']) && count($descValue['images']) > 1) ? $descValue['images'][1] : '<i class="fa fa-image"></i>') ?></div>
        </div>
    </div>
<?php } ?>