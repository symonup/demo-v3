<div class="modal fade" id="allegro-kategorie-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?=$this->Txt->printAdmin(__('Admin | Wybierz kategorię allegro'))?></h4>
      </div>
      <div class="modal-body" id="allegro-kategorie-options-container">
        <?=$this->element('admin/allegro/category_options',['options'=>$options,'parent'=>0,'key'=>0])?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->Txt->printAdmin(__('Admin | Anuluj'))?></button>
        <button type="button" class="btn btn-primary" id="allegro-confirm-select-cat"><?=$this->Txt->printAdmin(__('Admin | Wybierz kategorię'))?></button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->