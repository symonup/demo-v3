<div class="modal fade" id="allegro-powiaz-modal" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?=$this->Txt->printAdmin(__('Admin | Wybierz produkt do powiązania'))?></h4>
      </div>
        <form method="post">
      <div class="modal-body" id="">
          <select name="towar_id" class="form-control select2-remote" data-target="<?= Cake\Routing\Router::url(['controller'=>'Towar','action'=>'findToSelect2'])?>"></select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->Txt->printAdmin(__('Admin | Anuluj'))?></button>
        <button type="submit" class="btn btn-primary"><?=$this->Txt->printAdmin(__('Admin | Powiąż aukcje z produktem'))?></button>
      </div>
        </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->