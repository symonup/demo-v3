<div class="modal fade" id="allegro-images-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?=$this->Txt->printAdmin(__('Admin | Wstaw zdjęcie do opisu'))?></h4>
      </div>
      <div class="modal-body">
        <?php 
        foreach ($allImagesResults as $imageId => $allegroImage){
            ?>
          <div class="select-allegro-image">
              <img src="<?=$allegroImage['location']?>"/>
              <input type="hidden" value="<?=$allegroImage['location']?>"/>
          </div>
          <?php
        }
        ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->Txt->printAdmin(__('Admin | Anuluj'))?></button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->