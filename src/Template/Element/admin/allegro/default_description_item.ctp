            <?php 
            if(!empty($descValue)){
            $arrValues=[];
            $tmpType=[];
            foreach ($descValue as $descItem){
                $tmpType[]=mb_strtolower($descItem['type']);
                if(mb_strtolower($descItem['type'])=='text'){
                    $arrValues['text']=$descItem['content'];
                }
                if(mb_strtolower($descItem['type'])=='image'){
                    $arrValues['images'][]='<img src="'.$descItem['url'].'"/><input type="hidden" value="'.$descItem['url'].'"/>';
                }
            }
            $descItemType=join('-',$tmpType);
           $descValue = $arrValues;
        }
        else{
            if(empty($descItemType)){
                $descItemType='text';
            }
            $descValue = [];
        }
            ?><div class="description-item" key="<?= $key ?>" type="<?=$descItemType?>">
    <div class="header">
        <button type="button" class="description-item-type btn btn-xs btn-default <?=(($descItemType=='text')?'active':'')?>" data-type="text"><i class="fa fa-align-center"></i></button>
        <button type="button" class="description-item-type btn btn-xs btn-default <?=(($descItemType=='image')?'active':'')?>" data-type="image"><i class="fa fa-image"></i></button>
        <button type="button" class="description-item-type btn btn-xs btn-default <?=(($descItemType=='text-image')?'active':'')?>" data-type="text-image"><i class="fa fa-align-center"></i> <i class="fa fa-image"></i></button>
        <button type="button" class="description-item-type btn btn-xs btn-default <?=(($descItemType=='image-text')?'active':'')?>" data-type="image-text"><i class="fa fa-image"></i> <i class="fa fa-align-center"></i></button>
        <button type="button" class="description-item-type btn btn-xs btn-default <?=(($descItemType=='image-image')?'active':'')?>" data-type="image-image"><i class="fa fa-image"></i> <i class="fa fa-image"></i></button>
        <div class="pull-right">
            <button type="button" confirm-message="<?= $this->Txt->printAdmin(__('Admin | Czy na pewno chcesz usunąć element opisu? Wprowadzona treść zostanie usunięta.')) ?>" class="remove btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
            <button type="button" class="copy btn btn-xs btn-default"><i class="fa fa-copy"></i></button>
            <button type="button" class="move btn btn-xs btn-default" data-target="up"><i class="fa fa-arrow-up"></i></button>
            <button type="button" class="move btn btn-xs btn-default" data-target="down"><i class="fa fa-arrow-down"></i></button>
        </div>
    </div>
    <div class="content">
        <?php
        switch ($descItemType) {
            case 'text': {
                    if (!empty($defaultDesc)) {
                        $descValue['text'] = trim(preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/i", '<$1$2>', $defaultDesc));
                    }
                }break;
            case 'image': {
                    
                }break;
            case 'text-image': {
                    if (!empty($defaultDesc)) {
                        $descValue['text'] = trim(preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/i", '<$1$2>', $defaultDesc));
                    }
                }break;
            case 'image-text': {
                    if (!empty($defaultDesc)) {
                        $descValue['text'] = trim(preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/i", '<$1$2>', $defaultDesc));
                    }
                }break;
            case 'image-image': {
                    
                }break;
            default: {
                    if (!empty($defaultDesc)) {
                        $descValue['text'] = trim(preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/i", '<$1$2>', $defaultDesc));
                    }
                }break;
        }
        ?>
<?= $this->element('admin/allegro/description_items', ['descItemType' => $descItemType, 'descValue' => $descValue]) ?>
    </div>
</div>