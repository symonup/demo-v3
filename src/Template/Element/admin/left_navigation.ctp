<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="<?= Cake\Routing\Router::url(['controller' => 'Index', 'action' => 'index']) ?>" class="site_title"><i class="fa fa-laptop"></i> <span><?= Cake\Core\Configure::read('admin.nazwa_serwisu') ?></span></a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <div class="profile_pic">
                <img src="<?= $displayPath['webroot'] ?>layout-admin/build/images/user.png" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span><?= $this->Txt->printAdmin(__('Admin | Witaj')) ?>,</span>
                <h2><?= (!empty($user) ? (((!empty($user['imie']) && !empty($user['nazwisko'])) ? $user['imie'] . ' ' . $user['nazwisko'] : $user['login'])) : '') ?></h2>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                    <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Index', 'action' => 'index']) ?>"><i class="fa fa-home"></i> <?= $this->Txt->printAdmin(__('Admin | Home')) ?></a></li>
                    <li><a><i class="fa fa-cubes"></i> <?= $this->Txt->printAdmin(__('Admin | Katalog')) ?> <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'index']) ?>"><b><?= $this->Txt->printAdmin(__('Admin | Produkty')) ?></b></a></li>
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Kategoria', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Admin | Kategorie')) ?></a></li>
                            <?php /*  <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Gatunek', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Admin | Gatunki')) ?></a></li> */ ?>
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Producent', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Admin | Producenci')) ?></a></li>
                            <?php /*  <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Platforma', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Admin | Platformy')) ?></a></li> */ ?>
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'AtrybutTyp', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Admin | Atrybuty i warianty')) ?></a></li>
                            <?php /*  <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Wersja', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Admin | Wersje')) ?></a></li> */ ?>
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Zestaw', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Admin | Zestawy')) ?></a></li>
                            <?php /*  <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Bonus', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Admin | Bonusowe złotówki')) ?></a></li> */ ?>
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'porownywarki']) ?>"><?= $this->Txt->printAdmin(__('Admin | Porównywarki')) ?></a></li>
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Wiek', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Admin | Przedziały wiekowe')) ?></a></li>
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Certyfikat', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Admin | Certyfikaty')) ?></a></li>
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Opakowanie', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Admin | Opakowania')) ?></a></li>
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Okazje', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Admin | Okazje (prezenty)')) ?></a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-amazon"></i> <?= $this->Txt->printAdmin(__('Admin | Allegro')) ?> <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Allegro', 'action' => 'index']) ?>"><b><?= $this->Txt->printAdmin(__('Admin | Aukcje')) ?></b></a></li>
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Allegro', 'action' => 'orders']) ?>"><?= $this->Txt->printAdmin(__('Admin | Zamówienia')) ?></a></li>
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'AllegroKonto', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Admin | Konfiguracja')) ?></a></li>
                        </ul>
                    </li>
                    <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Zamowienie', 'action' => 'index']) ?>"><i class="fa fa-shopping-cart"></i> <?= $this->Txt->printAdmin(__('Admin | Zamówienia')) ?></a></li>
                    <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Powiadomienia', 'action' => 'index']) ?>"><i class="fa fa-envelope"></i> <?= $this->Txt->printAdmin(__('Admin | Zapytania')) ?> <span class="pull-right label label-success" id="alerts-count-menu"><?= $zapytaniaCountAll ?></span></a></li>
                    <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Zamowienie', 'action' => 'dhl']) ?>"><i class="fa fa-truck"></i> <?= $this->Txt->printAdmin(__('Admin | DHL zlecenia')) ?></a></li>
                    <li><a href="<?= Cake\Routing\Router::url(['controller' => 'KodyRabatowe', 'action' => 'index']) ?>"><i class="fa fa-tags"></i> <?= $this->Txt->printAdmin(__('Admin | Kody rabatowe')) ?></a></li>
                    <?php /*      <li><a href="<?= Cake\Routing\Router::url(['controller' => 'KartaPodarunkowa', 'action' => 'index']) ?>"><i class="fa fa-credit-card"></i> <?= $this->Txt->printAdmin(__('Admin | Karty podarunkowe')) ?></a></li> */ ?>
                    <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Koszyk', 'action' => 'index']) ?>"><i class="fa fa-shopping-basket"></i> <?= $this->Txt->printAdmin(__('Admin | Koszyki')) ?></a></li>
                    
                    <li><a><i class="fa fa-users"></i> <?= $this->Txt->printAdmin(__('Admin | Klienci')) ?> <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Uzytkownik', 'action' => 'index', 'b2c']) ?>"><b><?= $this->Txt->printAdmin(__('Admin | Indywidualni')) ?></b></a></li>
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Uzytkownik', 'action' => 'index', 'b2b']) ?>"><?= $this->Txt->printAdmin(__('Admin | Hurtowi')) ?></a></li>
                        </ul>
                    </li>
                    <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Newsletter', 'action' => 'index']) ?>"><i class="fa fa-envelope"></i> <?= $this->Txt->printAdmin(__('Admin | Newsletter')) ?></a></li>
                    <li><a href="<?= Cake\Routing\Router::url(['controller' => 'NewsletterAdres', 'action' => 'index']) ?>"><i class="fa fa-envelope"></i> <?= $this->Txt->printAdmin(__('Admin | Newsletter adresy')) ?></a></li>
                    <li><a href="<?= Cake\Routing\Router::url(['controller' => 'TowarOpinia', 'action' => 'index']) ?>"><i class="fa fa-star-half-o"></i> <?= $this->Txt->printAdmin(__('Admin | Opinie')) ?></a></li> 

                    <li><a><i class="fa fa-clone"></i> <?= $this->Txt->printAdmin(__('Nav | Elementy strony'), 'Nav | ') ?> <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Strona', 'action' => 'stats']) ?>"><?= $this->Txt->printAdmin(__('Nav | Statystyki'), 'Nav | ') ?></a></li>
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Menu', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Nav | Menu'), 'Nav | ') ?></a></li>
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Banner', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Nav | Banery'), 'Nav | ') ?></a></li>
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Blok', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Nav | Bloki'), 'Nav | ') ?></a></li>
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Strona', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Nav | Strony'), 'Nav | ') ?></a></li>
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'FooterLink', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Nav | Linki w stopce'), 'Nav | ') ?></a></li>
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Konfiguracja', 'action' => 'css']) ?>"><?= $this->Txt->printAdmin(__('Nav | Style CSS'), 'Nav | ') ?></a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-cog"></i> <?= $this->Txt->printAdmin(__('Admin | Ustawienia')) ?> <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Konfiguracja', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Admin | Konfiguracja')) ?></a></li>
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Konfiguracja', 'action' => 'kalendarz']) ?>"><?= $this->Txt->printAdmin(__('Admin | Kalendarz dostaw')) ?></a></li>

                            <?php /*  <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Waluta', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Admin | Waluty')) ?></a></li>
                              <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Vat', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Admin | Vat')) ?></a></li>
                             */ ?>  <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Jednostka', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Admin | Jednostki')) ?></a></li>
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'RodzajPlatnosci', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Admin | Rodzaje płatności')) ?></a></li>
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Wysylka', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Admin | Rodzaje wysyłki')) ?></a></li>
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'PunktyOdbioru', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Admin | Punkty odbioru')) ?></a></li>
                            <?php /*  <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Jezyk', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Admin | Języki')) ?></a></li>
                             */ ?>    <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Jezyk', 'action' => 'translations']) ?>"><?= $this->Txt->printAdmin(__('Admin | Słowniki')) ?></a></li>
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Kategoria', 'action' => 'mapowanie']) ?>"><?= $this->Txt->printAdmin(__('Admin | Mapowanie kategorii do hurtowni')) ?></a></li>
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Szablon', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Admin | Szablony email')) ?></a></li>
                            <li><a href="<?= Cake\Routing\Router::url(['controller' => 'Administrator', 'action' => 'index']) ?>"><?= $this->Txt->printAdmin(__('Admin | Administratorzy')) ?></a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="<?= $this->Txt->printAdmin(__('Admin | Wyloguj')) ?>" href="<?= Cake\Routing\Router::url(['controller' => 'Index', 'action' => 'logout']) ?>">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>