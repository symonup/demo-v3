<?php 
$actSimilar='';
if(!empty($zestaw_towar) && !empty($zestaw_towar->zestaw_towar_wymiana))
{
    foreach ($zestaw_towar->zestaw_towar_wymiana as $podobny)
    {
        $actSimilar.='<li>'.((!empty($simPicture[$podobny->podobny_id]) && file_exists($filePath['towar'].'thumb_'.$simPicture[$podobny->podobny_id]))?'<img style="max-width: 50px; float: left;" src="/files/images/thumb_'.$simPicture[$podobny->podobny_id].'"/>':'').$podobny->towar->nazwa.'<input type="hidden" value="'.$podobny->towar->id.'" name="zestaw_towar_wymiana['.$towar->id.'][towar_id][]"/></li>';
    }
}
echo $this->Html->tag('div',
                $this->Html->tag('h3',__('Towary do wymiany'))
         .$this->Form->input('search',['type'=>'text','label'=>__('enter product name or code'),'class'=>'form-control','wymiana-search'=>'true','div'=>['class'=>'form-group input text'], 'templates' => ['formGroup' => '{{label}}{{input}}<div class="wymiana_searching"></div>']])
         .$this->Html->tag('div',$this->Html->tag('h5',__('Search result')).$this->Html->tag('ul',' ',['class'=>'similar_product_sort similar_search_results_'.$towar->id]),['class'=>'col-xs-12 col-sm-5 col-md-5 col-lg-5'])
                
         .$this->Html->tag('div',$this->Html->tag('h5',__('Actions')).$this->Html->tag('div','<span data-toggle="tooltip" data-placement="top" title="'.__('Add selected to similar').'" class="glyphicon glyphicon-step-forward" data-action="move-to-similar" data-content-result="similar_search_results_'.$towar->id.'" data-content="similar_products_'.$towar->id.'" aria-hidden="true"></span><br/>
            <span data-toggle="tooltip" data-placement="top" title="'.__('Remove selected from similar').'" class="glyphicon glyphicon-step-backward" aria-hidden="true" data-action="move-to-result" data-content-result="similar_search_results_'.$towar->id.'" data-content="similar_products_'.$towar->id.'"></span><br/>
            <span data-toggle="tooltip" data-placement="top" title="'.__('Add all to similar').'" class="glyphicon glyphicon-fast-forward" aria-hidden="true" data-action="move-all-to-similar" data-content-result="similar_search_results_'.$towar->id.'" data-content="similar_products_'.$towar->id.'"></span><br/>
            <span data-toggle="tooltip" data-placement="top" title="'.__('Remove all from similar').'" class="glyphicon glyphicon-fast-backward" aria-hidden="true" data-action="move-all-to-result" data-content-result="similar_search_results_'.$towar->id.'" data-content="similar_products_'.$towar->id.'"></span><br/>
            <span data-toggle="tooltip" data-placement="top" title="'.__('Remove selected from similar').'" class="glyphicon glyphicon-trash" aria-hidden="true" data-action="delete-similar" data-content-result="similar_search_results_'.$towar->id.'" data-content="similar_products_'.$towar->id.'"></span><br/>
            <span data-toggle="tooltip" data-placement="top" title="'.__('Full size').'" class="glyphicon glyphicon-fullscreen" aria-hidden="true" data-action="full-size" data-content-result="similar_search_results_'.$towar->id.'" data-content="similar_products_'.$towar->id.'"></span>',['class'=>'similar_action_content']),['class'=>'col-xs-12 col-sm-2 col-md-2 col-lg-2'])
         .$this->Html->tag('div',$this->Html->tag('h5',__('Similar products')).$this->Html->tag('ul',(!empty($actSimilar)?$actSimilar:' '),['class'=>'similar_product_sort similar_products_'.$towar->id]),['class'=>'col-xs-12 col-sm-5 col-md-5 col-lg-5'])
                ,
    ['class'=>'greyArea zestaw_wymiana_lista','style'=>'display:none;']);
?>
