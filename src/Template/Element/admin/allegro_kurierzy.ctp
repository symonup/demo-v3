<!-- Nav tabs -->
<?php 
$active='dpd';
if(!empty($_GET['k_tab'])){
    $active=$_GET['k_tab'];
}
?>
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="<?=((empty($active) || $active=='dpd')?'active':'')?>"><a href="#dpd" aria-controls="dpd" role="tab" data-toggle="tab">DPD</a></li>
    <li role="presentation" class="<?=((!empty($active) && $active=='ups')?'active':'')?>"><a href="#ups" aria-controls="ups" role="tab" data-toggle="tab">UPS</a></li>
    <li role="presentation" class="<?=((!empty($active) && $active=='dhlWebApi')?'active':'')?>"><a href="#dhlWebApi" aria-controls="dhlWebApi" role="tab" data-toggle="tab">DHL webApi</a></li>
 <li role="presentation"><a href="#inpost" aria-controls="inpost" role="tab" data-toggle="tab">Inpost</a></li>
   <li role="presentation"><a href="#poczta" aria-controls="poczta" role="tab" data-toggle="tab">Poczta Polska</a></li> 
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane <?=((empty($active) || $active=='dpd')?'active':'')?>" id="dpd">
        <?php 
        echo $this->element('admin/kurier_allegro/dpd');
        ?>
    </div>
    <div role="tabpanel" class="tab-pane <?=((!empty($active) && $active=='ups')?'active':'')?>" id="ups">
        <?php 
        echo $this->element('admin/kurier_allegro/ups');
        ?>
    </div>
    <div role="tabpanel" class="tab-pane <?=((!empty($active) && $active=='dhlWebApi')?'active':'')?>" id="dhlWebApi">
        <?php 
        echo $this->element('admin/kurier_allegro/dhl_webapi');
        ?>
    </div>
      <div role="tabpanel" class="tab-pane" id="inpost">
        <?php 
        echo $this->element('admin/kurier_allegro/inpost');
        ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="poczta">
        <?php 
        echo $this->element('admin/kurier_allegro/poczta');
        ?>
    </div> 
  </div>
