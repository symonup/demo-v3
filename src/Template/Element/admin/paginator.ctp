<div class="paginator row">
    <div class="col-xs-12 col-md-6">
    <p><?= $this->Paginator->counter($this->Txt->printAdmin(__('Admin | Strona {{page}} / {{pages}}, wszystkich pozycji {{count}}, wyświetlone pozycje od {{start}} do {{end}}'))) ?></p>
    </div>
    <div class="col-xs-12 col-md-6 text-right">
    <ul class="pagination">
        <?= $this->Paginator->first(__('<< first')) ?>
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <li><?= $this->Paginator->numbers(['modulus'=>4]) ?></li>
        <?= $this->Paginator->next(__('next') . ' >') ?>
        <?= $this->Paginator->last(__('last >>')) ?>
    </ul>
    </div>
</div>