<div class="paginator paginator-top">
    <ul class="pagination">
        <?php 
        $curr=$this->Paginator->current();
        $total=$this->Paginator->total();?>
        <?= $this->Paginator->prev('<',['escale'=>false]) ?>
        <?php if($curr>2 && $total>3){ ?>
        <?= $this->Paginator->first(__('1')) ?>
        <?php } if($curr>3 && $total>3){
            ?>
        <li><span>...</span></li>
        <?php
        } ?>
        <li><?= $this->Paginator->numbers(['modulus'=>2]) ?></li>
        <?php 
        if($curr<($total-2) && $total>3){
            ?>
        <li><span>...</span></li>
        <?php
        } 
        if($curr<($total-1) && $total > 3){
            echo $this->Paginator->last((string) $total);
        }
        ?>
        <?= $this->Paginator->next('>',['escale'=>false]) ?>
    </ul>
</div>