<div class="container-fluid mt-40">
            <div class="row">
                <div class="col-12 kat-header">
                    <span><?= $this->Translation->get('towar_lista.Kategoria') ?></span><h1><?= $kategoria->nazwa ?></h1>
                </div>
            </div>
            <?php if(!empty($kategoria->naglowek)){ ?>
            <div class="row">
                <div class="col-12 text-justify fs-16 fc-black lh-19 ">
                    <?= $kategoria->naglowek ?>
                </div>
            </div>
            <?php } ?>
            <div class="row sub-kat-list justify-content-between">
                <?php
                $itemI = 1;
                foreach ($podkategorie as $subKat) {
                    ?>
                <div class="col-12 col-md-6 col-lg-5 col-xxl-6 sub-kat-item">
                    <div class="row">
                        <div class="col-12 col-xxl-5">
                            <a href="<?= \Cake\Routing\Router::url(['action' => 'index', $subKat->id, $this->Navi->friendlyUrl($subKat->nazwa)]) ?>" title="<?= $subKat->nazwa ?>" class="image">
                            <?= ((!empty($subKat->zdjecie) && file_exists($filePath['kategoria'] . $subKat->zdjecie)) ? $this->Html->image($displayPath['kategoria'] . $subKat->zdjecie, ['alt' => $subKat->nazwa]) : NO_PHOTO) ?>
                        </a>
                        </div>
                        <div class="col-12 col-xxl-7 xxl-padding-left-0">
                            <a href="<?= \Cake\Routing\Router::url(['action' => 'index', $subKat->id, $this->Navi->friendlyUrl($subKat->nazwa)]) ?>" title="<?= $subKat->nazwa ?>" class="name">
                            <?= $subKat->nazwa ?>
                        </a>
                            <div>
                                <?php if(!empty($subKat->child_kategoria)){
                                    ?>
                                <ul class="kat-sub-childs">
                                <?php
     foreach ($subKat->child_kategoria as $child){
         echo $this->Html->tag('li',$this->Html->link($child->nazwa,['action'=>'index',$child->id,$this->Txt->friendlyUrl($child->nazwa)],['escape'=>false]));
     }
                                    ?>
                                </ul>
                                <?php
                                } ?>
                            </div>
                        </div>
                    </div>
                </div>
                    <?php
                    $itemI++;
                }
                ?>
    </div>
</div>