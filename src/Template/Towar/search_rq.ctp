<div id="rq-results">
<?php
if(!empty($towary) && $towary->count()>0){
    foreach ($towary as $towar){
        ?>
    <a class="search-item-result" href="<?= $this->Txt->towarViewUrl($towar)?>"><?=$towar->nazwa?></a>
    <?php
    }
}else{
    ?>
    <div class="empty-results"><?=$this->Translation->get('search.emptyResults')?></div>
    <?php
}
?>
</div>