<?php
$routerParams = [
    'controller' => $this->request->params['controller'],
    'action' => $this->request->params['action'],
];
$routerParams += $this->request->params['pass'];
$baseRoute = [];
if (!empty($this->request->query)) {
    $routerParams += $this->request->query;
}
unset($routerParams['view']);
?>
<div class="container <?= (!empty($listLabel) ? 'list-' . $listLabel : '') ?>">
    <?= $this->Form->create(null, ['id' => 'filterForm']) ?>
    <div class="row">
        <div class="col-12 col-md-3">
            <div class="filterPriceLabel"><?= $this->Translation->get('towar_lista.priceRange') ?></div>
            <div id="price-range-inputs">
                <div><span><?= $this->Translation->get('towar_lista.priceFrom') ?></span><?= !($currencyOptions['after']) ? $this->Html->tag('span', $currencySymbol . (!empty($currencyOptions['space']) ? ' ' : ''), ['class' => 'currencySymbol']) : '' ?><input class="price-from"/><?= ($currencyOptions['after']) ? $this->Html->tag('span', (!empty($currencyOptions['space']) ? ' ' : '') . $currencySymbol, ['class' => 'currencySymbol']) : '' ?></div>
                <span>-</span>
                <div><span><?= $this->Translation->get('towar_lista.priceTo') ?></span><?= !($currencyOptions['after']) ? $this->Html->tag('span', $currencySymbol . (!empty($currencyOptions['space']) ? ' ' : ''), ['class' => 'currencySymbol']) : '' ?><input class="price-to"/><?= ($currencyOptions['after']) ? $this->Html->tag('span', (!empty($currencyOptions['space']) ? ' ' : '') . $currencySymbol, ['class' => 'currencySymbol']) : '' ?></div>
            </div>
            <input type="hidden" data-prefix="<?= !($currencyOptions['after']) ? $currencySymbol . (!empty($currencyOptions['space']) ? ' ' : '') : '' ?>" data-postfix="<?= ($currencyOptions['after']) ? (!empty($currencyOptions['space']) ? ' ' : '') . $currencySymbol : '' ?>" id="price-range" value="<?= (!empty($filters['price']) ? $filters['price'] : '') ?>" data-max="<?= $maxPrice ?>" data-step="1" name="price"/>

        </div>
        <div class="col-12 col-md-3 col-lg-2">
            <?= $this->Form->control('wiek', ['type' => 'select','value'=>((!empty($filters) && key_exists('wiek', $filters))?$filters['wiek']:''), 'options' => $wiek, 'empty' => t('labels.wszystkie'), 'label' => t('towar_lista.wiek')]) ?>
        </div>
        <div class="col-12 col-md-3 col-lg-2">
            <?= $this->Form->control('plec', ['type' => 'select','value'=>((!empty($filters) && key_exists('plec', $filters))?$filters['plec']:''), 'options' => ['boy' => t('plec.chlopiec'), 'girl' => t('plec.dziewczynka')], 'empty' => t('plec.wszystkie'), 'label' => t('towar_lista.plec')]) ?>
        </div> 
        <div class="col-12">
            <hr/>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-3 filter-container">
            <?= $this->element('default/towar_lista_left') ?>
        </div>
        <div class="col-12 col-md-9">

            <?php
            $preorders = false;
//    if(!empty($listLabel) && $listLabel=='wyprzedaz'){
//        echo $this->element('default/lista_strefa');
//    }
//    if(!empty($listLabel) && $listLabel=='preorders'){
//        echo $this->element('default/lista_preorders');
//        $preorders=true;
//    }
            ?>

            <div class="row mt-10 align-items-center">
                <div class="col-12 col-lg-auto list-sort">
                    <?= $this->Form->input('sort', ['label' => $this->Translation->get('towar_lista.wyswietlanie'), 'value' => $sortKey, 'onchange' => 'submit();', 'options' => ['id_desc' => $this->Translation->get('towar_lista.sortByDefault'), 'nazwa_asc' => $this->Translation->get('towar_lista.sortByNameAsc'), 'nazwa_desc' => $this->Translation->get('towar_lista.sortByNameDesc'), 'cena_asc' => $this->Translation->get('towar_lista.sortPriceAsc'), 'cena_desc' => $this->Translation->get('towar_lista.sortByPriceDesc')]]) ?>

                </div>
                <div class="col-auto">
                    <?php
                    if (!empty($filters)) {
                        unset($filters['kategoria']);
                        unset($filters['preorders']);
                        if (!empty($filters) && !empty($_GET)) {
                            ?>
                            <a href="<?= Cake\Routing\Router::url($baseRoute) ?>" class="btn btn-outline-danger btn-sm"><?= $this->Translation->get('towar_lista.clearFilter') ?></a>

                            <?php
                        }
                    }
                    ?>
                </div>
                <div class="col text-right">
                    <?= $this->element('default/paginator_input') ?>
                </div>
            </div>
            <?php if (!empty($kategoria) || (!empty($listLabel) && $listLabel != 'wyprzedaz' && $listLabel != 'preorders') || !empty($searchQuery) || !empty($listaGatunek)) { ?>
                <div class="row">
                    <div class="col-12 kat-header pageHead">
                        <?php
                        if (!empty($listLabel)) {
                $rqCode = (!empty($_GET['code'])?$_GET['code']:null);
                if(!empty($rqCode)){
                            ?>
                            <h1 class=""><?= $this->Translation->get('towar_lista.listaHead_' . $listLabel.'Kod{0}',[$rqCode]) ?></h1>
                            <?php
                }else{
                            ?>
                            <h1 class=""><?= $this->Translation->get('towar_lista.listaHead_' . $listLabel) ?></h1>
                            <?php
                }
                        } elseif (!empty($searchQuery)) {
                            $baseRoute = ['controller' => $this->request->params['controller'], 'action' => $this->request->params['action']] + $this->request->params['pass'];
                            ?>
                            <span><?= $this->Translation->get('towar_lista.wynikiWyszukiwaniaDlaFrazy') ?></span> <h1><?= $searchQuery ?></h1>
                            <a href="<?= \Cake\Routing\Router::url($baseRoute) ?>" class="btn btn-outline-danger btn-sm pull-right"><?= str_replace('wyczyscWyniki', '', $this->Translation->get('towar_lista.wyczyscWyniki')) ?> <i class="fa fa-times"></i></a>
                        <?php }
                        ?>
                    </div>
                </div>
                <?php if (!empty($kategoria->naglowek)) { ?>
                    <div class="row">
                        <div class="col-12 text-justify fs-16 fc-black lh-19 no-padding">
                            <?= $kategoria->naglowek ?>
                        </div>
                    </div>
                    <?php
                }
            } elseif (empty($listLabel)) {
                $rqCode = (!empty($_GET['code'])?$_GET['code']:null);
                if(!empty($rqCode)){
                ?>
                <div class="row">
                    <div class="col-12 kat-header pageHead">
                        <?= $this->Translation->get('towar_lista.listaHead_produktyKod{0}',[$rqCode]) ?>
                    </div>
                </div>
                <?php
                }else{
                ?>
                <div class="row">
                    <div class="col-12 kat-header pageHead">
                        <?= $this->Translation->get('towar_lista.listaHead_wszystkieProdukty') ?>
                    </div>
                </div>
                <?php
                }
            }
            ?>
            <?php
            if (!($towary->count() > 0)) {
                echo $this->element('default/towar_lista_not_found');
            } else {
                if (empty($premiery)) {
                    $premiery = false;
                }
                echo $this->element('default/towar_lista', ['towary' => $towary, 'premiera' => ($preorders || $premiery), 'hideBadge' => (($mobile && ($preorders || $premiery)) ? true : false)]);
            }
            ?>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>