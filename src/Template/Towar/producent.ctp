<?= $this->element('default/breadcrumb') ?>
<div class="container mt-40">
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12 kat-header difFont no-padding">
                    <span><?= $this->Translation->get('producent.OfertaProducenta') ?></span><h1><?= $producent->nazwa ?></h1>
                </div>
            </div>
            <?php if(!empty($producent->naglowek)){ ?>
            <div class="row">
                <div class="col-12 text-justify fs-16 fc-black lh-19 no-padding">
                    <?= $producent->naglowek ?>
                </div>
            </div>
            <?php } ?>
            <div class="row justify-content-center sub-kat-list producent">
                <?php
                $itemI = 1;
                foreach ($allKat as $kat) {
                    ?>
                    <a class="col-12 col-md-3 sub-kat-item item-<?= $itemI ?>" href="<?= \Cake\Routing\Router::url(['action' => 'index', $kat['id'], $this->Navi->friendlyUrl($kat['nazwa']),'producent'=>[$producent->id]]) ?>" title="<?= $kat['nazwa'] ?>">
                        <div class="image">
                            <?= ((!empty($kat['zdjecie']) && file_exists($filePath['kategoria'] . $kat['zdjecie'])) ? $this->Html->image($displayPath['kategoria'] . $kat['zdjecie'], ['alt' => $kat['nazwa']]) : NO_PHOTO) ?>
                        </div>
                        <div class="name">
                            <?= $kat['nazwa'] ?>
                        </div>
                    </a>
                    <?php
                    if ($itemI == 4) {
                        $itemI = 1;
                    } else {
                        $itemI++;
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>