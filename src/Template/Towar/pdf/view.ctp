<?php
$oldPrice = null;
$rabatPrec = null;
$itemPriceDef = $this->Txt->itemPriceDef($towar);
$itemPrice = $this->Txt->itemPrice($towar, false, 'brutto');
$itemPriceNetto = $this->Txt->itemPrice($towar, false, 'netto');
if ($itemPriceDef > $itemPrice) {
    $oldPrice = $itemPriceDef;
}
if (!empty($oldPrice)) {
    $rabatPrec = round(100 - (($itemPrice * 100) / $oldPrice));
}
$gwiazdki = '';
if (!empty($towar->towar_opinia)) {
    for ($gw_i = 1; $gw_i <= $count_star; $gw_i++) {
        $gwiazdki .= $this->Html->tag('span', '', ['class' => 'fa fa-star star' . (($gw_i <= $towar->opinia_avg) ? ' active' : ''), 'aria-hidden' => 'true']);
    }
} else {
    for ($gw_i = 1; $gw_i <= $count_star; $gw_i++) {
        $gwiazdki .= $this->Html->tag('span', '', ['class' => 'fa fa-star star', 'aria-hidden' => 'true']);
    }
}
$badges = [
    'promocja',
    'wyprzedaz',
    'nowosc',
    'wyrozniony'
];
$badge = [];
foreach ($badges as $badgeName) {
    if (!empty($towar[$badgeName])) {
        $badge[] = $this->element('default/towar_view_badge', ['type' => $badgeName]);
    }
}
$baseParams = [];
$maxAttr = ($mobile ? 30 : 1);
if (!empty($towar->atrybut)) {
    foreach ($towar->atrybut as $atrybut) {
        if (!empty($atrybut->pole)) {
            if (count($baseParams) == $maxAttr && !key_exists($atrybut->nazwa, $baseParams)) {
                continue;
            }
            $baseParams[$atrybut->nazwa][] = $atrybut->towar_atrybut[0]->wartosc;
        } else {
            if (count($baseParams) == $maxAttr && !key_exists($atrybut->atrybut_typ->nazwa, $baseParams)) {
                continue;
            }
            $baseParams[$atrybut->atrybut_typ->nazwa][] = $atrybut->nazwa;
        }
    }
}
$firstWersion = null;
if ($wersje->count() > 0) {
    $firstWersion = $wersje->toArray()[0];
}
$wysylkaEndTime = $this->Txt->deliveryDay();
$dateDiff = $this->Txt->datetimeDiff(date('Y-m-d H:i:s'), $wysylkaEndTime);
$hoursLeft = $dateDiff->hour;
$gatunki = [];
if (!empty($towar->gatunek)) {
    foreach ($towar->gatunek as $gatunek) {
        $gatunki[$gatunek->id] = $gatunek->nazwa;
    }
}
$warianty = [];
$wariantIlosc = [];
$kolory = [];
$koloryHex = [];
$rozmiary = [];
if (!empty($towar->towar_wariant)) {
    foreach ($towar->towar_wariant as $towarWariant) {
        if (empty($towarWariant->ilosc)) {
            continue;
        }
        $warianty['kolor'][$towarWariant->kolor_id][$towarWariant->rozmiar_id] = $towarWariant->id;
        $wariantIlosc[$towarWariant->id] = $towarWariant->ilosc;
        $kolory[$towarWariant->kolor_id] = $towarWariant->kolor->nazwa;
        if (!empty($towarWariant->kolor->hex)) {
            $koloryHex[$towarWariant->kolor_id] = $towarWariant->kolor->hex;
        }
        $rozmiary[$towarWariant->rozmiar_id] = $towarWariant->rozmiar->nazwa;
    }
}
$optionsKolor = [];
$optionsRozmiar = [];
if (!empty($warianty)) {
    foreach ($warianty['kolor'] as $kolorId => $wariantRozmiary) {
        $allRozmiarByKolor = [];
        foreach ($wariantRozmiary as $rozmiarId => $wariantId) {
            $optionsRozmiar[$wariantId] = ['value' => $wariantId, 'text' => $rozmiary[$rozmiarId], 'ilosc' => $wariantIlosc[$wariantId], 'kolor-id' => $kolorId];
            $allRozmiarByKolor[$rozmiarId] = $rozmiarId;
        }
        $optionsKolor[$kolorId] = ['value' => $kolorId, 'text' => $kolory[$kolorId], 'rozmiar-id' => join(',', $allRozmiarByKolor), 'style' => (!empty($koloryHex[$kolorId]) ? 'background-color:' . $koloryHex[$kolorId] . ';' : null)];
    }
}
?>
<div class="container item-view">
    <div class="row towar-view-top">
        <div class="col-12 <?= (!$mobile ? 'col-lg-5' : '') ?> pd-b-20 ps-relative towar-view-top-left">
            <?= ($mobile ? $this->element('default/towar_galeria_poziom') : $this->element('default/towar_galeria_poziom')) ?>

            <?php if ($mobile) { ?>
                <div class="ps-relative">
                    <span add-target="<?= Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'addToFavorite', $towar->id]) ?>" remove-target="<?= Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'removeFavorite', $towar->id]) ?>" class="favorite <?= ((!empty($favorite) && key_exists($towar->id, $favorite)) ? 'active' : '') ?>"><i class="far fa-heart" data-toggle="tooltip" title="<?= $this->Translation->get('labels.DodajDoSchowka') ?>"></i><i class="on-active fa fa-heart" data-toggle="tooltip" title="<?= $this->Translation->get('labels.UsunZeSchowka') ?>"></i></span>
                    <div class="view-price-mobile">
                        <?= $this->Txt->cena($itemPrice) ?>
                    </div>
                </div>
                <?php
                if ($wersje->count() > 0) {
                    ?>
                    <div class="view-wersja">
                        <span class="version-name"><?= $firstWersion->atrybut->atrybut_typ->nazwa ?>:</span>
                        <div class="btn-group">
                            <button type="button" class="btn btn-outline-dark dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="version-label"><?= $firstWersion->atrybut->nazwa ?></span>
                            </button>
                            <div class="dropdown-menu">
                                <?php
                                foreach ($wersje as $wersja) {
                                    ?>
                                    <a class="dropdown-item towar-wersja" href="javascript:setItemVersion(<?= $wersja->id ?>,'<?= $wersja->atrybut->nazwa ?>');" version-id="<?= $wersja->id ?>"><?= $wersja->atrybut->nazwa ?></a>

                                    <?php
                                }
                                ?> 
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <a href="javascript:void(0);" class="powiadom-o-cenie" data-toggle="modal" data-target="#modal-powiadom-o-cenie">
                    <?= $this->Translation->get('towar_view.powiadomKiedyCenaSpadnie') ?><i class="fa fa-angle-right"></i>
                </a>
                <?php
                $addToCartAllow = true;
                ?>
                <div class="add-to-cart-container mt-10">
                    <?php
                    if ($addToCartAllow) {
                        ?>
                        <div class="view-ilosc">
                            <input type="text" item-price="<?= $itemPrice ?>" placeholder="1" class="item-ilosc-input" item-id="<?= $towar->id ?>" version-id="<?= (!empty($firstWersion) ? $firstWersion->id : '') ?>"/>
                        </div>
                        <?php
                    }
                    if (!empty($towar->data_premiery) && strtotime($towar->data_premiery->format('Y-m-d')) > strtotime(date('Y-m-d'))) {
                        if (!empty($towar->preorder) && $towar->preorder_limit > 0 && (
                                (empty($towar->preorder_od) && empty($towar->preorder_do)) || (empty($towar->preorder_do) && !empty($towar->preorder_od) && strtotime(date('Y-m-d H:i')) >= strtotime($towar->preorder_od->format('Y-m-d H:i'))) || (empty($towar->preorder_od) && !empty($towar->preorder_do) && strtotime(date('Y-m-d H:i')) <= strtotime($towar->preorder_do->format('Y-m-d H:i'))) || (!empty($towar->preorder_do) && !empty($towar->preorder_od) && strtotime(date('Y-m-d H:i')) >= strtotime($towar->preorder_od->format('Y-m-d H:i')) && strtotime(date('Y-m-d H:i')) <= strtotime($towar->preorder_do->format('Y-m-d H:i')))
                                )
                        ) {
                            ?>
                            <button version-id="<?= (!empty($firstWersion) ? $firstWersion->id : '') ?>" error-message="<?= $this->Translation->get('errors.selectVariant') ?>" type="button" item-id="<?= $towar->id ?>" class="btn btn-success btn-block add-to-cart"  add-action="<?= \Cake\Routing\Router::url(['controller' => 'Cart', 'action' => 'add', $towar->id]) ?>"><?= $this->Translation->get('towar_view.addToCartMobile') ?><i class="fa fa-spinner reload-active"></i><i class="fa reload-unactive fa-angle-right"></i></button>
                            <?php
                        } else {
                            ?>
                            <span class="order-not-allow" data-toggle="tooltip" title="<?= $this->Translation->get('towar_lista.produktNiedostepnyWSprzedazyInfo') ?>"><?= $this->Translation->get('towar_view.addToCartNotAllow') ?></span>
                            <?php
                        }
                    } else {
                        ?>
                        <button version-id="<?= (!empty($firstWersion) ? $firstWersion->id : '') ?>" error-message="<?= $this->Translation->get('errors.selectVariant') ?>" type="button" item-id="<?= $towar->id ?>" class="btn btn-success btn-block add-to-cart"  add-action="<?= \Cake\Routing\Router::url(['controller' => 'Cart', 'action' => 'add', $towar->id]) ?>"><?= $this->Translation->get('towar_view.addToCartMobile') ?><i class="fa fa-spinner reload-active"></i><i class="fa reload-unactive fa-angle-right"></i></button>
                        <?php
                    }
                    ?>
                </div>
                <?php
            }
            ?>
        </div>
        <div class="col-12 <?= (!$mobile ? 'col-lg-7' : '') ?> pd-b-20 ps-relative towar-view-top-right">
            <div class="row">
                <?php if (!$mobile) { ?>
                    <div class="col">
                        <div class="row">
                            <div class="col-12">
                                <h1 class="view-productName"><?= $towar->nazwa ?></h1>
                            </div>
                            <?php if (!empty($towar->kod)) { ?>
                                <div class="col-12 col-auto"><div class="view-param-kod"><?= $this->Translation->get('towar_view.kodProdukty{0}', [$this->Html->tag('span', $towar->kod)]) ?></div></div>
                            <?php } if (!empty($towar->ean)) { ?>
                                <div class="col-12 col-auto"><div class="view-param-kod"><?= $this->Translation->get('towar_view.kodEAN{0}', [$this->Html->tag('span', $towar->ean)]) ?></div></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-auto"><a href="#"><i class="fa fa-print"></i></a></div>
                    <div class="col-12">
                        <div class="view-price-head">
                            <div class="price">
                                <?php
                                if (!empty($oldPrice)) {
                                    if (!empty($rabatPrec)) {
                                        ?>
                                        <span class="rabat-badge">-<?= $rabatPrec ?>%</span>
                                        <?php
                                    }
                                    ?>
                                    <span class="old-price"><?= $this->Txt->cena($oldPrice) ?></span>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="price">
                                <div class="<?= (!empty($oldPrice) ? ' promotion' : '') ?>">
                                    <span class="price-val <?= !empty($oldPrice) ? 'promo' : '' ?>"><?= $this->Txt->cena($itemPrice) ?></span>
                                </div>
                                <?php
                                if (!empty($itemPriceNetto)) {
                                    ?>
                                    <div class="price-netto">
                                        <span><?= t('labels.netto{0}', [$this->Txt->cena($itemPriceNetto)]) ?></span>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                        <div class="view-price mt-20 mb-20">
                            <div class="view-param">
                                <i class="flat flaticon-chronometer"></i><?= t('towar_view.Dostepnosc{0}', [(($towar->ilosc > 0) ? '24h' : '14 dni')]) ?>
                            </div>
                            <?php if (!empty($towar->producent)) { ?>
                                <div class="view-param">
                                    <i class="flat flaticon-work"></i><?= t('towar_view.Producent{0}', [$towar->producent->nazwa]) ?>
                                </div>
                                <?php
                            }
                            $kosztWs = -1;
                            $dostawaMess = '';
                            if (!empty($wysylki)) {
                                foreach ($wysylki as $wsId => $wysylkaKoszt) {
                                    if ($kosztWs < 0 || $wysylkaKoszt < $kosztWs) {
                                        $kosztWs = $wysylkaKoszt;
                                    }
                                }
                                if ($wysylkaKoszt <= 0) {
                                    $dostawaMess = $this->Translation->get('towar_view.dostawaGratis');
                                } else {
                                    $dostawaMess = $this->Translation->get('wysylka.od{0}', [$this->Txt->cena($wysylkaKoszt)]);
                                }
                            } else {
                                $dostawaMess = $this->Translation->get('towar_view.dostawaGratis');
                            }
                            ?>

                            <div class="view-param">
                                <i class="flat flaticon-delivery-packages-on-a-trolley"></i><?= t('towar_view.Dostawa{0}', [$dostawaMess]) ?> <a href="/pages/view/6/dostawa" link-type="type_popup" title="Dostawa"><?= t('towar_view.przeczytajZasadyDostawy') ?> <i class="fa fa-angle-right"></i></a>
                            </div>
                            <?php
                            if ($wersje->count() > 0) {
                                ?>
                                <div class="view-wersja">
                                    <span class="version-name"><?= $firstWersion->atrybut->atrybut_typ->nazwa ?>:</span>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-outline-dark dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="version-label"><?= $firstWersion->atrybut->nazwa ?></span>
                                        </button>
                                        <div class="dropdown-menu">
                                            <?php
                                            foreach ($wersje as $wersja) {
                                                ?>
                                                <a class="dropdown-item towar-wersja" href="javascript:setItemVersion(<?= $wersja->id ?>,'<?= $wersja->atrybut->nazwa ?>');" version-id="<?= $wersja->id ?>"><?= $wersja->atrybut->nazwa ?></a>

                                                <?php
                                            }
                                            ?> 
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="text-right br-b-1">
                                <a href="<?= Cake\Routing\Router::url(['controller' => 'Home', 'action' => 'powiadomODostepnosci', $towar->id, 't' => 1]) ?>" modal-target="#modal-powiadom-o-dostepnosci" class="getPupUpLink">
                                    <i class="fas fa-comment-alt"></i> <?= $this->Translation->get('towar_view.dostepnoscInfo') ?> <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                            <div class="cart-form">
                                <?php if (!empty($warianty)) {
                                    ?>
                                    <div class="row" id="wariant-selector">
                                        <?php
                                        if (!empty($optionsKolor)) {
                                            ?>
                                            <div class="col-12 col-md-6">
                                                <?= $this->Form->control('kolor', ['type' => 'select', 'options' => $optionsKolor, 'id' => 'towar-view-kolor', 'label' => $this->Translation->get('towar_view.kolor')]) ?>
                                            </div>
                                            <?php
                                        }
                                        if (!empty($optionsRozmiar)) {
                                            ?>
                                            <div class="col-12 col-md-6">
                                                <?= $this->Form->control('wariant_id', ['type' => 'select', 'options' => $optionsRozmiar, 'id' => 'towar-view-rozmiar', 'label' => $this->Translation->get('towar_view.rozmiar')]) ?>
                                            </div>
                                        <?php }
                                        ?>
                                    </div>
                                <?php }
                                ?>
                                <div class="row align-items-end add-to-cart-container">
                                    <div class="col-auto text-right padding-right-0 fs-16 lh-normal"><?= $this->Translation->get('labels.ilosc') ?></div>
                                    <div class="col-auto padding-right-0"><div class="view-ilosc">
                                            <input type="text" item-price="<?= $itemPrice ?>" placeholder="1" class="item-ilosc-input" item-id="<?= $towar->id ?>" version-id="<?= (!empty($firstWersion) ? $firstWersion->id : '') ?>"/>
                                        </div></div>
                                    <div class="col padding-left-0">    
                                        <button version-id="<?= (!empty($firstWersion) ? $firstWersion->id : '') ?>" error-message="<?= $this->Translation->get('errors.selectVariant') ?>" type="button" item-id="<?= $towar->id ?>" class="btn btn-success btn-block add-to-cart"  add-action="<?= \Cake\Routing\Router::url(['controller' => 'Cart', 'action' => 'add', $towar->id]) ?>"><i class="fa fa-spinner reload-active"></i><i class="fa reload-unactive fa-shopping-cart"></i><?= $this->Translation->get('towar_view.addToCart') ?></button>
                                    </div>
                                        <div><span add-target="<?= Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'addToFavorite', $towar->id]) ?>" remove-target="<?= Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'removeFavorite', $towar->id]) ?>" class="favorite <?= ((!empty($favorite) && key_exists($towar->id, $favorite)) ? 'active' : '') ?>"><i class="far fa-heart" data-toggle="tooltip" title="<?= $this->Translation->get('labels.DodajDoSchowka') ?>"></i><i class="on-active fa fa-heart" data-toggle="tooltip" title="<?= $this->Translation->get('labels.UsunZeSchowka') ?>"></i></span></div>
                                
                                </div>
                            </div>
                            <?php ?>
                        </div>



                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="row mt-10">
        <div class="col-lg-12">
            <?php
            $opisNav = [];
            $opisTabs = [];
            $allParams = [];
            if (!$mobile && !empty($towar->atrybut)) {
                foreach ($towar->atrybut as $atrybut) {
                    if (!empty($atrybut->pole)) {
                        $allParams[$atrybut->nazwa][] = $atrybut->towar_atrybut[0]->wartosc;
                    } else {
                        $allParams[$atrybut->atrybut_typ->nazwa][] = $atrybut->nazwa;
                    }
                }
            }
            $params = '';
            if (!empty($allParams)) {
                $params = $this->element('default/towar_opis_cechy', ['params' => $allParams]);
            }
            if (!empty($towar->opis) || !empty($params) || ($mobile && !empty($towar->opis2) && trim($towar->opis2) != '<p><br></p>')) {
                echo $this->Html->tag('div', $this->Html->tag('div', $this->Translation->get('towar_view.OpisProduktu'), ['class' => 'head-opis']) . (($mobile && !empty($towar->opis2) && trim($towar->opis2) != '<p><br></p>') ? $towar->opis2 : (!empty($towar->opis) ? $towar->opis : '')) . $params, ['class' => '']);
            }
            ?>
        </div>
    </div>
    <?php
    if ($mobile) {

        if (!empty($zestaw->count() > 0)) {
            echo $this->Html->tag('div', $this->Html->tag('div', $this->Translation->get('towar_view.wZestawieTaniej'), ['class' => 'textHeadOne' . ($mobile ? ' mt-10' : '')]) . $this->element('default/towar_zestaw', ['zestawy' => $zestaw]), ['class' => '']);
        }
    }
    ?>
</div>
<?php
if (!empty($towar->polecane)) {
    ?>
    <div class="bg-grey mt-20">
        <div class="container-fluid produkty-powiazane xs-padding-0">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <span class="item-badge home-nowosci-badge"><?= $this->Translation->get('towar_view.otherProducts') ?></span>
                    </div>
                </div>
                <?php
                if (!$mobile) {
                    ?>
                    <div class="row mt-15">
                        <?php
                        foreach ($towar->polecane as $polecany) {
                            ?>
                            <div class="col-12 col-md-4 col-sm-6">
                                <?= $this->element('default/item_min', ['towar' => $polecany, 'showPegi' => false]) ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                } else {
                    echo $this->element('default/items_mobile_carusel', ['items' => $towar->polecane, 'hideBadge' => false, 'showPegi' => false]);
                }
                ?>
            </div>
        </div>
    </div>
<?php } /*
  ?>
  <div class="container">
  <div class="row">
  <div class="col-12">
  <div class="opinie-content">
  <div class="row">
  <div class="col-12 opinie-head" id="opinie">
  <?= $this->Translation->get('towar_view.tabOpinion') ?>
  </div>
  </div>
  <?= $this->element('default/towar_view_opinie') ?>
  </div>
  </div>
  </div>
  </div> */ /* ?>
  <div class="modal fade" id="modal-powiadom-o-cenie" tabindex="-1" role="dialog" aria-labelledby="modal-powiadom-o-cenieLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
  <div class="modal-content">
  <div class="modal-header">
  <h5 class="modal-title" id="modal-powiadom-o-cenieLabel"><?= $this->Translation->get('powiadomOCenie.modalTitle') ?></h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
  </button>
  </div>
  <?= $this->Form->create($powiadomienie, ['url' => ['controller' => 'Towar', 'action' => 'powiadomOCenie', $towar->id]]) ?>
  <div class="modal-body">
  <div><?= $this->Translation->get('powiadomOCenie.modalInfo{nazwa}{data_premiery}', ['nazwa' => $towar->nazwa, 'data_premiery' => $this->Txt->printDate($towar->data_premiery, 'Y-m-d', $this->Translation->get('labels.Nieznana'))], true) ?></div>
  <?= $this->Form->control('imie', ['type' => 'text', 'label' => false, 'placeholder' => $this->Translation->get('powiadomOCenie.imiePlaceholder')]) ?>
  <?= $this->Form->control('email', ['type' => 'email', 'label' => false, 'placeholder' => $this->Translation->get('powiadomOCenie.emailPlaceholder')]) ?>
  <?php /*<div class="nice-checkbox">
  <?= $this->Form->control('zgoda', ['type' => 'checkbox', 'required' => true, 'id' => 'zgoda-powiadomienie', 'label' => ['text' => $this->Translation->get('powiadomOCenie.zgodaLabel'), 'escape' => false]]) ?>
  </div> */ /* ?>
  </div>
  <div class="modal-footer">
  <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= $this->Translation->get('powiadomOCenie.btnAnuluj') ?></button>
  <button type="submit" class="btn btn-primary btn-color-1"><?= $this->Translation->get('powiadomOCenie.btnZapisz') ?></button>
  </div>
  <?= $this->Form->end() ?>
  </div>
  </div>
  </div> */
?>