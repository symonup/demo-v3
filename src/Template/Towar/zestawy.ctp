<?php
$routerParams = [
    'controller' => $this->request->params['controller'],
    'action' => $this->request->params['action'],
];
$routerParams += $this->request->params['pass'];
$baseRoute = [];
if (!empty($this->request->query)) {
    $routerParams += $this->request->query;
}
unset($routerParams['view']);
?>
<div class="container">
    <div class="row">
        <div class="col-12 col-md-3">
            <?=$this->element('default/zestawy_left')?>
        </div>
        <div class="col-12 col-md-9">
            
            <div class="row mt-10 align-items-center">
                <div class="col text-right">
                    <?= $this->element('default/paginator_input') ?>
                </div>
            </div>
            <?php 
            $zestawOpis = t('zestawy_lista.naglowekOpis',[],true);
            if(!empty($zestawOpis) && $zestawOpis!='naglowekOpis' && empty($_GET['page'])){
                ?>
                <div class="row">
                    <div class="col-12">
                        <div><?= $zestawOpis ?></div>
                    </div>
                </div>
            <?php
            }
            ?>
            <?php
            if (!($zestawy->count() > 0)) {
                echo $this->element('default/zestawy_lista_not_found');
            } else {
                if (empty($premiery)) {
                    $premiery = false;
                }
                echo $this->element('default/zestawy_lista', ['zestawy' => $zestawy]);
            }
            ?>
        </div>
    </div>
</div>