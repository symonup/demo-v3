<?php
$searchKat = [];
foreach ($mainCatsDef as $mainKatItem) {
    $searchKat[$mainKatItem['id']] = $mainKatItem['nazwa'];
}
$oldPrice = null;
$rabatPrec = null;
$itemPriceDef = $this->Txt->itemPriceDef($towar);
$itemPrice = $this->Txt->itemPrice($towar, false, 'brutto');
$itemPriceNetto = false;
if ($b2b) {
    $itemPriceNetto = $this->Txt->itemPrice($towar, false, 'netto');
}
if ($itemPriceDef > $itemPrice) {
    $oldPrice = $itemPriceDef;
}
if (!empty($oldPrice)) {
    $rabatPrec = round(100 - (($itemPrice * 100) / $oldPrice));
}
$gwiazdki = '';
if (!empty($towar->towar_opinia)) {
    for ($gw_i = 1; $gw_i <= $count_star; $gw_i++) {
        $gwiazdki .= $this->Html->tag('span', '', ['class' => 'fa fa-star star' . (($gw_i <= $towar->opinia_avg) ? ' active' : ''), 'aria-hidden' => 'true']);
    }
} else {
    for ($gw_i = 1; $gw_i <= $count_star; $gw_i++) {
        $gwiazdki .= $this->Html->tag('span', '', ['class' => 'fa fa-star star', 'aria-hidden' => 'true']);
    }
}
$badges = [
    'promocja',
    'wyprzedaz',
    'nowosc',
    'wyrozniony'
];
$badge = [];
foreach ($badges as $badgeName) {
    if (!empty($towar[$badgeName])) {
        $badge[] = $this->element('default/towar_view_badge', ['type' => $badgeName]);
    }
}
$baseParams = [];
$maxAttr = 8;
if (!empty($towar->kod)) {
    $baseParams[t('towar_view.param_kod')][] = $towar->kod;
}
if (!empty($towar->producent)) {
    $baseParams[t('towar_view.param_producent')][] = $towar->producent->nazwa;
}
if (!empty($towar->ean)) {
    $baseParams[t('towar_view.param_ean')][] = $towar->ean;
}
if (!empty($towar->opakowanie)) {
    $baseParams[t('towar_view.param_opakowanie')][] = $towar->opakowanie->nazwa;
}
if (!empty($towar->wymiary)) {
    $baseParams[t('towar_view.param_wymiary')][] = $towar->wymiary;
}
if (!empty($towar->wymiary_opakowania)) {
    $baseParams[t('towar_view.param_wymiary_opakowania')][] = $towar->wymiary_opakowania;
}
if (!empty($towar->waga_kg)) {
    $baseParams[t('towar_view.param_waga_kg')][] = $towar->waga_kg;
}
if (!empty($towar->baterie)) {
    $baseParams[t('towar_view.param_baterie')][] = $towar->baterie;
}
if (!empty($towar->atrybut)) {
    foreach ($towar->atrybut as $atrybut) {
        if (!empty($atrybut->pole)) {
            if (count($baseParams) == $maxAttr && !key_exists($atrybut->nazwa, $baseParams)) {
                continue;
            }
            $baseParams[$atrybut->nazwa][] = $atrybut->towar_atrybut[0]->wartosc;
        } else {
            if (count($baseParams) == $maxAttr && !key_exists($atrybut->atrybut_typ->nazwa, $baseParams)) {
                continue;
            }
            $baseParams[$atrybut->atrybut_typ->nazwa][] = $atrybut->nazwa;
        }
    }
}
$firstWersion = null;
if ($wersje->count() > 0) {
    $firstWersion = $wersje->toArray()[0];
}
$wysylkaEndTime = $this->Txt->deliveryDay();
$dateDiff = $this->Txt->datetimeDiff(date('Y-m-d H:i:s'), $wysylkaEndTime);
$hoursLeft = $dateDiff->hour;
$gatunki = [];
if (!empty($towar->gatunek)) {
    foreach ($towar->gatunek as $gatunek) {
        $gatunki[$gatunek->id] = $gatunek->nazwa;
    }
}
$warianty = [];
$wariantIlosc = [];
$kolory = [];
$koloryHex = [];
$rozmiary = [];
if (!empty($towar->towar_wariant)) {
    foreach ($towar->towar_wariant as $towarWariant) {
        if (empty($towarWariant->ilosc)) {
            continue;
        }
        $warianty['kolor'][$towarWariant->kolor_id][$towarWariant->rozmiar_id] = $towarWariant->id;
        $wariantIlosc[$towarWariant->id] = $towarWariant->ilosc;
        $kolory[$towarWariant->kolor_id] = $towarWariant->kolor->nazwa;
        if (!empty($towarWariant->kolor->hex)) {
            $koloryHex[$towarWariant->kolor_id] = $towarWariant->kolor->hex;
        }
        $rozmiary[$towarWariant->rozmiar_id] = $towarWariant->rozmiar->nazwa;
    }
}
$optionsKolor = [];
$optionsRozmiar = [];
if (!empty($warianty)) {
    foreach ($warianty['kolor'] as $kolorId => $wariantRozmiary) {
        $allRozmiarByKolor = [];
        foreach ($wariantRozmiary as $rozmiarId => $wariantId) {
            $optionsRozmiar[$wariantId] = ['value' => $wariantId, 'text' => $rozmiary[$rozmiarId], 'ilosc' => $wariantIlosc[$wariantId], 'kolor-id' => $kolorId];
            $allRozmiarByKolor[$rozmiarId] = $rozmiarId;
        }
        $optionsKolor[$kolorId] = ['value' => $kolorId, 'text' => $kolory[$kolorId], 'rozmiar-id' => join(',', $allRozmiarByKolor), 'style' => (!empty($koloryHex[$kolorId]) ? 'background-color:' . $koloryHex[$kolorId] . ';' : null)];
    }
}
$opinia_gwiazdki = '';
for ($ogw_i = 1; $ogw_i <= $count_star; $ogw_i++) {
    $opinia_gwiazdki .= $this->Html->tag('span', '', ['class' => 'fa fa-star star' . (($ogw_i <= $towar->opinia_avg) ? ' active' : '')]);
}
?>

<div class="container-fluid item-view">
    <div class="row align-items-center">
        <div class="col-12 col-md-12 col-lg <?=(!$mobile?'form-group-mb-0':'')?>">
            <?= $this->element('default/search_box', ['searchKat' => $searchKat]) ?>
        </div>
        <div class="col-12 col-md-12 col-lg-4 text-right">
          <?php //  <a href="#" class="view-links-top"><i class="flat flaticon-like"></i> <?= t('towar_view.polecZnajomemu') </a>?>
            <?php $fbId = c('facebook.appId');
            if(!empty($fbId)){
                ?>
            <div class="fb-send-to-messenger" 
  messenger_app_id="320295312261912" 
  page_id="105066617793596" 
  data-ref="<?= \Cake\Routing\Router::url('/',true)?>" 
  color="white" 
  size="standard">
</div>
            <?php
            }
            ?>
            <a target="_blank" href="<?= r(['controller' => 'Towar', 'action' => 'view', $towar->id, '_ext' => 'pdf']) ?>" class="view-links-top"><i class="flat flaticon-print"></i> <?= t('towar_view.drukuj') ?></a>
            <span add-target="<?= Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'addToFavorite', $towar->id]) ?>" remove-target="<?= Cake\Routing\Router::url(['controller' => 'Towar', 'action' => 'removeFavorite', $towar->id]) ?>" class="view-links-top favorite <?= ((!empty($favorite) && key_exists($towar->id, $favorite)) ? 'active' : '') ?>"><i class="far fa-heart"></i><i class="on-active fa fa-heart"></i><span class="text-unactive"><?= t('labels.DodajDoSchowka') ?></span><span class="text-active"><?= t('labels.UsunZeSchowka') ?></span></span>

        </div>
        <div class="col-12"><hr/></div>
    </div>
    <div class="row towar-view-top">
        <div class="col-12 col-lg-6 pd-b-20 ps-relative towar-view-top-left">
            <div class="row">
                <div class="col-12">
                    <h1 class="view-productName"><?= $towar->nazwa ?></h1>
                    <div class="view-productOpinia">
                        <div class="avg-stars"><?= $opinia_gwiazdki ?></div>
                        <a href="#view-opinia-form">(<?= count($towar->towar_opinia) ?>)</a>
                        <a href="#view-opinia-form"><?= t('towar_view.dodajOpinie') ?></a>
                    </div>
                </div>
                <?php if (!empty($towar->kod)) { ?>
                    <div class="col-12 col-md-auto"><div class="view-param-kod"><?= t('towar_view.kodProdukty{0}', [$this->Html->tag('span', $towar->kod)]) ?></div></div>
                <?php } if (!empty($towar->ean)) { ?>
                    <div class="col-12 col-md-auto"><div class="view-param-kod"><?= t('towar_view.kodEAN{0}', [$this->Html->tag('span', $towar->ean)]) ?></div></div>
                <?php } ?>
            </div>
            <?= $this->element('default/towar_galeria'.($mobile?'_poziom':'')) ?>
            <?php
            if (!empty($towar->polecane)) {
                ?>
                <div class="similarHeader"><?= t('towar_view.otherProducts') ?></div>
                <?php
                echo $this->element('default/items_slider_mini', ['items' => $towar->polecane, 'sliderId' => 'viewPolecaneBottomSlider', 'hideBadge' => false, 'perPage' => 3]);
                ?>
            <?php }
            ?>
        </div>
        <div class="col-12 col-lg-auto pd-b-20 ps-relative towar-view-top-center">
            <div class="view-productBaseParams">
                <div class="text-center">
                    <span class="view-param-label"><?= t('towar_view.plec') ?></span>
                    <div class="view-gander">
                        <span class="girl <?= ((empty($towar->plec) || $towar->plec == 'girl') ? 'active' : '') ?>"><i class="flat flaticon-smiling-girl"></i></span>
                        <span class="boy <?= ((empty($towar->plec) || $towar->plec == 'boy') ? 'active' : '') ?>"><i class="flat flaticon-boy-broad-smile"></i></span>
                    </div>
                </div>
                <?php if (!empty($towar->wiek)) {
                    ?>
                    <div class="text-center">
                        <span class="view-param-label"><?= t('towar_view.wiek') ?></span>
                        <div class="view-param-value">
                            <?= $towar->wiek->nazwa ?>
                        </div>
                    </div>
                <?php }
                ?>
                <div class="text-center">
                    <span class="view-param-label-2"><?= t('towar_view.wysylka') ?></span>
                    <div class="view-param-value">
                        <?php
                        if ($towar->ilosc > 0) {
                            echo t('towar_view.wysylka24h');
                        } else {
                            echo t('towar_view.wysylkaNaZamowienie');
                        }
                        ?>
                    </div>
                </div>
                <?php
                if (!empty($towar->certyfikat)) {
                    $certyfikaty = [];
                    foreach ($towar->certyfikat as $certyfikat) {
                        $certyfikaty[] = '<span' . (!empty($certyfikat->nazwa) ? ' data-toggle="tooltip" title="' . str_replace('"', '', $certyfikat->nazwa) . '"' : '') . '>' . $certyfikat->skrot . '</span>';
                    }
                    ?>
                    <div class="text-center">
                        <span class="view-param-label-2"><?= t('towar_view.certyfikaty') ?></span>
                        <div class="view-param-value">
                            <?= join(', ', $certyfikaty) ?>
                        </div>
                    </div>
                    <?php
                }
                if (!empty($towar->gwarancja)) {
                    ?>
                    <div class="text-center">
                        <span class="view-param-label-2"><?= t('towar_view.gwarancja') ?></span>
                        <div class="view-param-value">
                            <?= $towar->gwarancja ?>
                        </div>
                    </div>
                    <?php
                }
                if (!empty($towar->zwrot)) {
                    ?>
                    <div class="text-center">
                        <span class="view-param-label-2"><?= t('towar_view.zwrotDo') ?></span>
                        <div class="view-param-value">
                            <?= $towar->zwrot ?>
                        </div>
                    </div>
                    <?php
                }
                if (!empty($towar->opakowanie)) {
                    ?>
                    <div class="text-center">
                        <span class="view-param-label-2"><?= t('towar_view.opakowanie') ?></span>
                        <div class="view-param-value">
                            <?= $towar->opakowanie->nazwa ?>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
        <div class="col-12 col-lg pd-b-20 ps-relative towar-view-top-right">
            <div class="row">
                <div class="col-12">
                    <div class="view-time-to-send">
                        <i class="flat flaticon-delivery-truck"></i>
                        <span>Zamów w ciągu</span>
                        <div class="view-time-count">
                            <?php
                            $wsTime = $this->Txt->deliveryDay();
                            ?>
                            <?= $this->element('default/time_counter', ['timeCounterId' => 'view-ws-counter', 'endTime' => $wsTime, 'hideLabels' => false]) ?>
                        </div>
                        <span>
                            <?php
                            if (strtotime($wsTime) > $this->Txt->printDate(new DateTime('+ 1 day'), 'Y-m-d 23:59:59', '', true)) {
                                echo t('towar_view.wyslemyW{0}', [$this->Txt->getDayName($wsTime, true)]);
                            } elseif (strtotime($wsTime) > $this->Txt->printDate(new DateTime(), 'Y-m-d ' . c('dostawa.maxTime') . ':00', '', true)) {
                                echo t('towar_view.wyslemyJutro');
                            } else {
                                echo t('towar_view.wyslemyDzisiaj');
                            }
                            ?>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="cart-form">
                        <?php if(!empty($towarSprzedane) && $towarSprzedane>0){
                            ?>
                        <div class="items-sold" sold="<?=$towarSprzedane?>"><?=t('towar_view.sprzedanych{0}',[$this->Html->tag('span',$this->Txt->odmiana($towarSprzedane,[t('odmiany.sztuka'),t('odmiany.sztuki'),t('odmiany.sztuk')],false))])?></div>
                        <?php
                        }?>
                        <?php if (!empty($warianty)) {
                            ?>
                            <div class="row" id="wariant-selector">
                                <?php
                                if (!empty($optionsKolor)) {
                                    ?>
                                    <div class="col-12 col-md-6">
                                        <?= $this->Form->control('kolor', ['type' => 'select', 'options' => $optionsKolor, 'id' => 'towar-view-kolor', 'label' => t('towar_view.kolor')]) ?>
                                    </div>
                                    <?php
                                }
                                if (!empty($optionsRozmiar)) {
                                    ?>
                                    <div class="col-12 col-md-6">
                                        <?= $this->Form->control('wariant_id', ['type' => 'select', 'options' => $optionsRozmiar, 'id' => 'towar-view-rozmiar', 'label' => t('towar_view.rozmiar')]) ?>
                                    </div>
                                <?php }
                                ?>
                            </div>
                        <?php }
                        ?>

                        <div class="row align-items-center add-to-cart-container justify-content-end">
                            <div class="col md-padding-right-0">

                                <div class="view-price-head">
                                    <div class="price">
                                        <?php if ($b2b && !empty($itemPriceNetto)) {
                                            ?>
                                            <div class="<?= (!empty($oldPrice) ? ' promotion' : '') ?>">
                                                <span class="price-val <?= !empty($oldPrice) ? 'promo' : '' ?>"><span class="price-type-label"><?= t('labels.netto') ?></span><?= $this->Txt->cena($itemPriceNetto) ?></span>
                                            </div>
                                            <div class="price-netto">
                                                <span><span class="price-type-label"><?= t('labels.brutto') ?></span><?= $this->Txt->cena($itemPrice) ?></span>
                                            </div>
                                            <?php
                                        } else {
                                            ?>
                                            <div class="<?= (!empty($oldPrice) ? ' promotion' : '') ?>">
                                                <span class="price-val <?= !empty($oldPrice) ? 'promo' : '' ?>"><?= $this->Txt->cena($itemPrice) ?></span>
                                            </div>
                                            <?php
                                            if (!empty($itemPriceNetto)) {
                                                ?>
                                                <div class="price-netto">
                                                    <span><?= t('labels.netto{0}', [$this->Txt->cena($itemPriceNetto)]) ?></span>
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>

                                    </div>
                                    <?php
                                    if (!empty($oldPrice)) {
                                        ?>
                                        <div class="price">
                                            <?php
                                            if (!empty($rabatPrec)) {
                                                ?>
                                                <span class="rabat-badge">-<?= $rabatPrec ?>%</span>
                                                <?php
                                            }
                                            ?>
                                            <span class="old-price"><?= $this->Txt->cena($oldPrice) ?></span>
                                            <?php ?>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-12 col-md-auto <?=(!$mobile?'padding-left-0':'mt-10')?>">
                                <div class="view-ilosc">
                                    <input type="text" item-price="<?= $itemPrice ?>" placeholder="1" class="item-ilosc-input towar-view-item-ilosc" item-id="<?= $towar->id ?>" version-id="<?= (!empty($firstWersion) ? $firstWersion->id : '') ?>"/>
                                </div>
                                <button version-id="<?= (!empty($firstWersion) ? $firstWersion->id : '') ?>" error-message="<?= t('errors.selectVariant') ?>" type="button" item-id="<?= $towar->id ?>" class="btn add-to-cart"  add-action="<?= \Cake\Routing\Router::url(['controller' => 'Cart', 'action' => 'add', $towar->id]) ?>"><?= t('towar_view.addToCart') ?><i class="flat flaticon-cart"></i></button>
                            <?php if(!empty($allowAsGratis)){
                                ?>
                                <div class="add-as-gratis-in-view">
                                <button as-gratis="true" version-id="<?= (!empty($firstWersion) ? $firstWersion->id : '') ?>" error-message="<?= t('errors.selectVariant') ?>" type="button" item-id="<?= $towar->id ?>" class="btn add-to-cart"  add-action="<?= \Cake\Routing\Router::url(['controller' => 'Cart', 'action' => 'add', $towar->id]) ?>"><?= t('towar_view.addAsGratis') ?></button>
                                </div>
                                <?php
                            } ?>
                            </div>
                        </div>

                    </div>
                    <?php if ($b2b) {
                        ?>
                        <div class="row">
                            <div class="col-12">
                                <div class="item-count-label"><?= t('towar_view.iloscSztWKartonie{0}', [$this->Html->tag('span', $this->Txt->odmiana((!empty($towar->ilosc_w_kartonie) ? $towar->ilosc_w_kartonie : 1), [t('odmiany.sztuka'), t('odmiany.sztuki'), t('odmiany.sztuk')]))]) ?></div>
                                <div class="item-count-label"><?= t('towar_view.aktualnyStanMagazynowy{0}', [$this->Html->tag('span', $this->Txt->odmiana(((!empty($towar->ilosc) && $towar->ilosc > 0) ? $towar->ilosc : 0), [t('odmiany.sztuka'), t('odmiany.sztuki'), t('odmiany.sztuk')]))]) ?></div>
                            </div>
                        </div>
                    <?php } else {
                         //   <div class="row justify-content-between">
                            $allIcbLinks = [];
                            if (!empty($zestaw->count() > 0)) {
                                $allIcbLinks[]='<a class="link-icb" href="#view-zestaw-s"><i class="flat flaticon-percent"></i><span>'. t('towar_view.kupWZestawieTaniej') .'</span><i class="flat flaticon-right-thin-chevron"></i></a>';
                            }
                            $allIcbLinks[]='<a class="link-icb" href="'. c('links.baterieInfo') .'"><i class="flat flaticon-power"></i><span>'. t('towar_view.potrzebujeszBaterii') .'</span><i class="flat flaticon-right-thin-chevron"></i></a>';
                            $allIcbLinks[]='<a class="link-icb" href="#view-gratis-s"><i class="flat flaticon-gift"></i><span>'. t('towar_view.gratisDoZamowieniaPowyzej') .'</span><i class="flat flaticon-right-thin-chevron"></i></a>';
                            if (!empty($towar->punkty)) {
                                $allIcbLinks[]='<a class="link-icb" href="'. c('links.punktyDoZamowieniaInfo') .'"><i class="flat flaticon-business-and-finance"></i><span>'. t('towar_view.punktyDoZamowienia', ['punkty' => $towar->punkty]) .'</span><i class="flat flaticon-right-thin-chevron"></i></a>';
                            }
                            $icbParts= array_chunk($allIcbLinks, 2);
                            foreach ($icbParts as $icbLinks){
                                ?>
                            <div class="row justify-content-between">
                                <div class="col-12 col-md-auto"><?=join('</div><div class="col-12 col-md-auto">',$icbLinks)?></div>
                            </div>
                            <?php 
                            }
                            ?>
                            <?php /* if (!empty($zestaw->count() > 0)) { ?>
                                <div class="col-12 col-md-auto col-xxl-6">
                                    <a class="link-icb" href="#view-zestaw-s"><i class="flat flaticon-percent"></i> <?= t('towar_view.kupWZestawieTaniej') ?> <i class="flat flaticon-right-thin-chevron"></i></a>
                                </div>
                            <?php } ?>
                            <div class="col-12 col-md-auto col-xxl-6">
                                <a class="link-icb" href="#view-gratis-s"><i class="flat flaticon-gift"></i> <?= t('towar_view.gratisDoZamowieniaPowyzej') ?> <i class="flat flaticon-right-thin-chevron"></i></a>
                            </div>
                            <div class="col-12 col-md-auto col-xxl-6">
                                <a class="link-icb" href="<?= c('links.baterieInfo') ?>"><i class="flat flaticon-power"></i> <?= t('towar_view.potrzebujeszBaterii') ?> <i class="flat flaticon-right-thin-chevron"></i></a>
                            </div>
                            <?php if (!empty($towar->punkty)) {
                                ?>
                                <div class="col-12 col-md-auto col-xxl-6">
                                    <a class="link-icb" href="<?= c('links.punktyDoZamowieniaInfo') ?>"><i class="flat flaticon-business-and-finance"></i><span><?= t('towar_view.punktyDoZamowienia', ['punkty' => $towar->punkty]) ?></span><i class="flat flaticon-right-thin-chevron"></i></a>
                                </div>
                            <?php } 
                        </div>*/
                            ?>
                    <?php } ?>
                    <hr/>
                    <div class="view-price mt-20 mb-20">
                        <div class="row justify-content-between">
                            <div class="col-12 col-md-auto">
                                <?php
                                $kosztWs = -1;
                                if (!empty($wysylki)) {
                                    foreach ($wysylki as $wsId => $wysylkaKoszt) {
                                        if ($kosztWs < 0 || $wysylkaKoszt < $kosztWs) {
                                            $kosztWs = $wysylkaKoszt;
                                        }
                                    }
                                    if ($wysylkaKoszt <= 0) {
                                        ?>
                                        <div class="view-dostawa-item"><i class="flat flat-big flaticon-delivery-truck"></i><span><?= t('towar_view.dostawaGratis') ?></span><i class="flat flaticon-right-thin-chevron"></i></div>
                                        <?php
                                    } else {
                                        ?>
                                        <div class="view-dostawa-item"><i class="flat flat-big flaticon-delivery-truck"></i><span><?= t('wysylka.od{0}', [$this->Txt->cena($wysylkaKoszt)]) ?></span><i class="flat flaticon-right-thin-chevron"></i></div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <div class="view-dostawa-item"><i class="flat flat-big flaticon-delivery-truck"></i><span><?= t('towar_view.dostawaGratis') ?></span><i class="flat flaticon-right-thin-chevron"></i></div>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="col-12 col-md-auto">
                                <a href="<?= c('links.odbiorOsobistyInfo') ?>" target="_blank">
                                    <i class="flat flat-big flaticon-area"></i>
                                    <div class="view-lokalizacja"><?= t('towar_view.Lokalizacja', [], true) ?></div>
                                    <i class="flat flaticon-right-thin-chevron"></i>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-12"><hr/></div>
            </div>
            <?php if (!empty($baseParams)) {
                ?>
                <div class="row justify-content-between">
                    <?php
                    $printBaseParams = array_chunk($baseParams, ceil(count($baseParams) / 2), true);
                    foreach ($printBaseParams as $printBaseParamsItems) {
                        ?>
                        <div class="col-12 col-md-6">
                            <?php
                            foreach ($printBaseParamsItems as $baseParamName => $baseParamValue) {
                                ?>
                                <div class="param-name"><?= $baseParamName ?>: <span class="param-value"><?= join(', ', $baseParamValue) ?></span></div>
                            <?php }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            <?php }
            ?>
            <div class="row">
                <div class="col-12"><hr/></div>
                <div class="col-12 col-md">
                    <div class="view-zapytanie zapytanie-info">
                        <i class="flat flaticon-talk"></i>
                        <div>
                            <div><?= t('towar_view.maszPytania') ?></div>
                            <div><?= t('towar_view.zadzwonDoNas{telefon}', ['telefon' => $this->Html->link(c('dane.telefon'), 'tel:' . c('dane.telefon'))]) ?></div>
                            <a href="<?= Cake\Routing\Router::url(['controller' => 'Home', 'action' => 'powiadomODostepnosci', $towar->id]) ?>" modal-target="#modal-powiadom-o-dostepnosci" class="getPupUpLink">
                                <?= t('towar_view.zapytanieInfo') ?>         
                            </a>
                        </div>
                    </div>
                </div>
                <?php if ($towar->ilosc <= 0) {
                    ?>
                    <div class="col-12 col-md">
                        <div class="view-zapytanie zapytanie-dostepnosc">
                            <i class="flat flaticon-ban-circle-symbol"></i>
                            <div>
                                <div><?= t('towar_view.dostepnosc') ?></div>
                                <div><?= t('towar_view.produktChwilowoNiedostepny') ?></div>
                                <a href="<?= Cake\Routing\Router::url(['controller' => 'Home', 'action' => 'powiadomODostepnosci', $towar->id, 't' => 1]) ?>" modal-target="#modal-powiadom-o-dostepnosci" class="getPupUpLink">
                                    <?= $this->Html->tag('div', t('towar_view.dostepnoscInfo')) ?>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php }
                ?>

            </div>
        </div>
    </div>
    <?php if (!$b2b) { ?>
        <div class="row child-cols-mb-10 justify-content-md-around justify-content-xxl-between">
            <?php
            if (!empty($zestaw->count() > 0)) {
                echo $this->element('default/towar_zestaw', ['zestawy' => $zestaw]);
            }
            ?>
            <div class="col-12 col-md-auto col-lg-5 col-xxl-auto">
                <div class="box-bg-grey" id="view-gratis-s">
                    <div class="row align-items-center">
                        <div class="col-12 col-md-auto"><div class="textHeadOne"><i flat-badge="<?= t('labels.gratis') ?>" class="flat flaticon-label-commercial-circular-filled-tool"></i> <?= t('towar_view.gratisDoZamowienia') ?></div></div>
                        <div class="col-12 col-md text-right">
                            <a href="<?= r(['controller' => 'Towar', 'action' => 'index', 'gratisy']) ?>" class="btn <?=($mobile?'btn-block':'')?> btn-color-1"><?= t('towar_view.zobaczGratisy') ?> <i class="flat flaticon-right-thin-chevron"></i></a>
                        </div>
                    </div>
                    <div class="textOne"><?= t('towar_view.gratisInfo') ?></div>
                    <div class="row">
                        <?php
                        if (!empty($gratisy) && $gratisy->count() > 0) {
                            foreach ($gratisy as $gratis) {
                                $addAction = ['controller' => 'Cart', 'action' => 'add', $gratis->id];
                                ?>
                                <div class="col-6 col-md-4">
                                    <div class="gratis-item">
                                        <?= $this->Txt->towarZdjecie($gratis, 'thumb_') ?>
                                        <div class="hover-item">
                                            <div class="gratis-name"><?=$gratis->nazwa?></div>
                                            <div class="gratis-min"><?=t('towar_view.zamowienieOd{0}',[$this->Txt->cena($gratis->gratis_wartosc)])?></div>
                                            <div class="gratis-button">
                                                <button type="button" as-gratis="true" item-id="<?= $gratis->id ?>" add-action="<?= \Cake\Routing\Router::url($addAction) ?>" class="btn btn-danger add-to-cart">
                                                    <i class="flat flaticon-cart"></i>
                                                    <span><?= $this->Translation->get('towar_lista.addToCartShort') ?> </span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        } else {
                            ?>
                            <div class="col-md">
                                <div class="gratis-item">
                                    <img src="/img/logo_mini.png"/>
                                </div>
                            </div>
                            <div class="col-md">
                                <div class="gratis-item">
                                    <img src="/img/logo_mini.png"/>
                                </div>
                            </div>
                            <div class="col-md">
                                <div class="gratis-item">
                                    <img src="/img/logo_mini.png"/>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-auto col-lg-auto col-xxl-auto">
                <div class="box-bg-grey">
                    <div class="textHeadOne"><i class="flat flaticon-gift"></i> <?= t('towar_view.prezentNaKazdaOkazje') ?></div>
                    <div class="textOne"><?= t('towar_view.prezentyInfo', [], true) ?></div>
                    <div class="text-center">    
                        <a href="<?= r(['controller' => 'Towar', 'action' => 'index','prezenty']) ?>" class="btn <?=($mobile?'btn-block':'')?> btn-color-1"><?= t('towar_view.pokazPrezenty') ?> <i class="flat flaticon-right-thin-chevron"></i></a>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="row mt-10">
        <div class="col-lg-12">
            <?php
            $opisNav = [];
            $opisTabs = [];
            $allParams = [];
            if (!empty($towar->atrybut)) {
                foreach ($towar->atrybut as $atrybut) {
                    if (!empty($atrybut->pole)) {
                        $allParams[$atrybut->nazwa][] = $atrybut->towar_atrybut[0]->wartosc;
                    } else {
                        $allParams[$atrybut->atrybut_typ->nazwa][] = $atrybut->nazwa;
                    }
                }
            }
            $params = '';
            if (!empty($allParams)) {
                $params = $this->element('default/towar_opis_cechy', ['params' => $allParams]);
            }
            if (!empty($towar->opis) || !empty($params)) {
                echo $this->Html->tag('div', $this->Html->tag('div', t('towar_view.OpisProduktu'), ['class' => 'head-opis']) . (!empty($towar->opis) ? $towar->opis : '') . $params, ['class' => '']);
            }
            ?>
        </div>
    </div>
</div>
    <?php
if (!empty($towar->podobne)) {
    ?>
    <div class="container-fluid produkty-powiazane">
        <div class="row">
            <div class="col-12">
                <div class="headerLine"><span><?= t('towar_view.inniPrzegladaliTakze') ?></span></div>
            </div>
        </div>
        <div class="row mt-15">
            <?php
            echo $this->element('default/items_slider', ['items' => $towar->podobne, 'sliderId' => 'viewOgladaneBottomSlider', 'hideBadge' => false, 'perPage' => 5]);
            ?>
        </div>
    </div>
<?php }
?>
<div class="container-fluid">
    <?= $this->element('default/towar_view_opinie') ?>
</div>
<?php
if (!empty($ostatnie) && $ostatnie->count() > 0) {
    ?>
    <div class="container-fluid produkty-powiazane">
        <div class="row">
            <div class="col-12">
                <div class="headerLine"><span><?= t('towar_view.ostatnioOgloadane') ?></span></div>
            </div>
        </div>
        <div class="row mt-15">
            <?php
            echo $this->element('default/items_slider', ['items' => $ostatnie, 'sliderId' => 'viewOgladaneBottomSlider', 'hideBadge' => false, 'perPage' => 5]);
            ?>
        </div>
    </div>
<?php }
?>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId            : '320295312261912',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v6.0'
    });
  };
</script>
<script async defer src="https://connect.facebook.net/pl_PL/sdk.js"></script>