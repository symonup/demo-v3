<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?= $cakeDescription ?>:
            <?= $this->fetch('title') ?>
        </title>
        <?= $this->Html->meta('icon') ?>
        <?= $this->Html->css($styleShets) ?>
    <?=$this->Html->script($headScripts)?>
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
    </head>
    <body class="<?=$this->request->params['action']?>">
    <script type="text/javascript">
    var alertMessages=new Object();
    var basePath='<?=$basePath?>';
    </script>
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-4">
                <img src="/img/w_budowie.png"/>
            </div>
            <div class="col-12 col-md-6">
                <h1>Strona w przygotowaniu</h1>
                <div>
                    <hr/>
                </div>
                <div class="text-right">
                    <a href="https://samatix.pl/" target="_blank">Samatix</a>
                </div>
            </div>
        </div>
    </div>
        <?= $this->Html->script($scripts) ?>
    </body>
</html>
