<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= ((!empty($seoData) && !empty($seoData['title']))?$seoData['title']:$this->fetch('title')) ?>
    </title>
    <meta name="robots" content="noindex, follow">
    <?= $this->Html->meta('icon') ?>
<?php 
$styleShets[]='/css/landingpage.css?'. uniqid();
?>
    <?=$this->Html->css($styleShets)?>
    <?=$this->Html->script($headScripts)?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <script type="text/javascript">
    var alertMessages=new Object();
    var basePath='<?=$basePath?>';
    </script>
    <?=$this->element('landingpage/navbar')?>
    <?php 
    $addClass='mb-50';
    if(mb_strtolower($this->request->params['controller'])=='pages' && !empty($strona)){
        $addClass=$strona->type;
    }
    ?>
    <div class="container-fluid no-padding <?=$addClass?>">

      
    <?= $this->Flash->render() ?>
       <div style="color: #ffffff;"><?=$this->Translation->get('landingPage.tekst',[],true)?></div>
<?=$this->element('default/time_counter',['endTime'=>'2019-04-01 10:00:00','pageReload'=>'self'])?>

    </div>
    <?=$this->element('landingpage/footer')?>
<?=$this->Html->script($scripts)?>
</body>
</html>
