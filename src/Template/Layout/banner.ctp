<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <?= $this->Html->css($styleShets) ?>
    <?=$this->Html->script($headScripts)?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
    </head>
    <body class="<?=(!empty($strona)?$strona->type:'')?>" style="padding-bottom: 0;">
    <script type="text/javascript">
    var alertMessages=new Object();
    var basePath='<?=$basePath?>';
    </script>
        <?= $this->fetch('content') ?>
        <?= $this->Html->script($scripts) ?>
    </body>
</html>
