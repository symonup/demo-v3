<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= ((!empty($seoData) && !empty($seoData['title']))?$seoData['title']:$this->fetch('title')) ?>
    </title>
    <meta name="robots" content="noindex, follow">
    <?= $this->Html->meta('icon') ?>

    <?=$this->Html->css($styleShets)?>
    <?=$this->Html->script($headScripts)?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body class="<?=(!empty($mobile)?'mobile':'desktop')?>">
    <script type="text/javascript">
    var alertMessages=new Object();
    var basePath='<?=$basePath?>';
    </script>
    <?=$this->element('default/navbar'.(!empty($mobile)?'_mobile':''))?>

    <div class="container-fluid">

      
    <?= $this->Flash->render() ?>
        <div class="container">
    <div class="row mt-40 xs-mt-10">
        <div class="xs-padding-0 col-md-3 col-lg-2">
            <button class="btn btn-block btn-outline-secondary visible-only-xs" id="userMenuShowBtn"><?= $this->Translation->get('uzytkownik_nav.yourMenu') ?> <i class="fa fa-bars pull-right"></i></button>
            <?=$this->element('user/nav')?>
        </div>
        <div class="xs-padding-0 col-md-9 col-lg-10">
            <div class="user-content"><?= $this->fetch('content') ?></div>
        </div>
    </div>
</div>
        

    </div>
    <?=$this->element('default/footer')?>
<?=$this->Html->script($scripts)?>
</body>
</html>
