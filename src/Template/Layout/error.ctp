<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= ((!empty($seoData) && !empty($seoData['title']))?$seoData['title']:$this->fetch('title')) ?>
    </title>
    <meta name="robots" content="noindex, follow">
    <?= $this->Html->meta('icon') ?>
<?php 
$styleShets[]='/css/landingpage.css?'. uniqid();
?>
    <?=$this->Html->css($styleShets)?>
    <?=$this->Html->script($headScripts)?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <script type="text/javascript">
    var alertMessages=new Object();
    var basePath='<?=$basePath?>';
    </script>
    <?=$this->element('error/navbar')?>
    <?php 
    $addClass='mb-50';
    if(mb_strtolower($this->request->params['controller'])=='pages' && !empty($strona)){
        $addClass=$strona->type;
    }
    ?>
    <div class="container-fluid no-padding <?=$addClass?>">

      
    <?= $this->Flash->render() ?>
                    <?= $this->fetch('content') ?>

    </div>
    <?=$this->element('error/footer')?>
<?=$this->Html->script($scripts)?>
</body>
</html>

            
       
