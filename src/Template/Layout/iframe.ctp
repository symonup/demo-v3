<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?=$this->Html->css($styleShets)?>
    <?=$this->Html->script($headScripts)?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body class="iframe-body nav-md">
    <script type="text/javascript">
    var alertMessages=new Object();
    var basePath='<?=$basePath?>';
    var lastPage=<?=(!empty($lastPage)?json_encode($lastPage):'null')?>;
    </script>
    <div class="container body">
      <div class="main_container">
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" style="margin-left: 0; background: transparent;" role="main">
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>
        </div>
      </div>
    </div>
    <?=$this->Html->script($scripts)?>
</body>
</html>
