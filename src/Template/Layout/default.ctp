<?php 
$crtlAct=mb_strtolower($this->request->params['controller'].'_'.$this->request->params['action'].(!empty($listLabel)?'_'.$listLabel:''));
?><!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?= ((!empty($seoData) && !empty($seoData['title'])) ? $seoData['title'] : $this->fetch('title')) ?>
        </title>
        <?php
        if (!empty($seoData['keywords'])) {
            echo '<meta name="Keywords" content="' . str_replace('"', '', $seoData['keywords']) . '"/>';
        }
        if (!empty($seoData['description'])) {
            echo '<meta name="Description" content="' . str_replace('"', '', $seoData['description']) . '"/>';
        }
        if (!empty($seoData['noIndex'])) {
            echo '<meta name="robots" content="noindex, nofollow"/>';
        }
        ?>
        <?= $this->Html->meta('icon') ?>

        <?= $this->Html->css($styleShets) ?>
        <?= $this->Html->script($headScripts) ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
        <?= \Cake\Core\Configure::read('script.head')?>
    </head>
    <body class="<?=$browser?> <?=$crtlAct?> <?= (!empty($singleSite) ? 'single-site' : '') ?> <?= (!empty($mobile) ? 'mobile' : 'desktop') ?> <?=(!empty($devMobile)?'devMobile':'devDesktop')?>">
        <script type="text/javascript">
            var alertMessages = new Object();
            var basePath = '<?= $basePath ?>';
        </script>
        <?= $this->element('default/navbar' . (!empty($mobile) ? '_mobile' : '')) ?>
        <?php
        $addClass = 'mb-50';
        if (mb_strtolower($this->request->params['controller']) == 'pages' && !empty($strona)) {
            $addClass = $strona->type;
        }
        ?>
        <div class="root-content <?= $addClass ?>">
            <?= $this->element('default/breadcrumb') ?>
                    <?= $this->Flash->render() ?>
                    <?= $this->fetch('content') ?>
        </div>
        <?= $this->element('default/footer') ?>
        <?= $this->Html->script($scripts) ?>
        <?php $fbId = c('facebook.appId');
        if(!empty($fbId)){
            ?>
        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v6.0&appId=<?=$fbId?>&autoLogAppEvents=1"></script>
        <?php
        }
        ?>
        <?= \Cake\Core\Configure::read('script.body')?>
    </body>
</html>
