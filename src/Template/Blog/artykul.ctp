<div class="container blog">
    <div class="row">
        <div class="col-12">
            <div class="row justify-content-center align-items-center blog-item">
                <div class="col-12 col-md-6 image">
                    <?php
                    if (!empty($artykul->zdjecie) && file_exists($filePath['blog'] . $artykul->zdjecie)) {
                        ?>
                        <img src="<?= $displayPath['blog'] . $artykul->zdjecie ?>" alt="<?= $artykul->tytul ?>"/>
                        <?php
                    }
                    ?>
                </div>
                <div class="col-12 col-md-6 description">
                    <div class="tytul">
                        <?= $artykul->tytul ?>
                    </div>
                    <div class="opis">
                        <?= nl2br($artykul->opis) ?>
                    </div>
                    <div class="link text-right">
                        <a href="#content" title="<?= $artykul->tytul ?>" class="btn btn-sm btn-success"><?= $this->Translation->get('blog.CzytajWiecej') ?></a>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center align-items-center blog-content">
                <div class="col-12 col-xxl-10 tresc" id="content">
                    <?= $artykul->tresc ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-xxl-10 text-left">
                    <a href="<?= \Cake\Routing\Router::url(['action' => 'index']) ?>" class="btn btn-success"><i class="fa fa-chevron-circle-left"></i> <?= $this->Translation->get('blog.WrocDoListyArtykulow') ?></a>
                </div>
            </div>
            <?php
            $fbId = c('facebook.appId');
            if (!empty($fbId)) {
                ?>
                <hr/>
                <div class="row">
                    <div class="col-12">
                        <div class="fb-comments" data-href="<?= Cake\Routing\Router::url(null, true) ?>" data-numposts="10" data-width="100%"></div>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>