<div class="container blog">
    <div class="row">
    <div class="col-12">
    <?php
    $i = 0;
    if(!empty($artykuly) && $artykuly->count()>0){
    foreach ($artykuly as $artykul) {
        if($mobile){
            echo $this->element('default/blog/item_left', ['artykul' => $artykul]);
        }else{
        if (($i % 2) == 0) {
            echo $this->element('default/blog/item_left', ['artykul' => $artykul]);
        } else {
            echo $this->element('default/blog/item_right', ['artykul' => $artykul]);
        }
        }
        $i++;
    }
    $active = (int) $this->Paginator->current();
    $all = (int) $this->Paginator->counter([
                'format' => '{{pages}}'
    ]);
    if ($all > 1) {
        ?>
        <div class="row">
            <div class="col-12">
                <div class="paginator">
                <ul class="pagination">
        <?php 
        $curr=$this->Paginator->current();
        $total=$this->Paginator->total();
        if($curr>1) echo $this->Paginator->prev('<',['escale'=>false]) ;
       if($curr>2 && $total>3){ ?>
        <?= $this->Paginator->first(__('1')) ?>
        <?php } if($curr>3 && $total>3){
            ?>
        <li><span>...</span></li>
        <?php
        } ?>
        <?= $this->Paginator->numbers(['modulus'=>2]) ?>
        <?php 
        if($curr<($total-2) && $total>3){
            ?>
        <li><span>...</span></li>
        <?php
        } 
        if($curr<($total-1) && $total > 3){
            echo $this->Paginator->last((string) $total);
        }
        if($curr!=$total) echo $this->Paginator->next('>',['escale'=>false]); ?>
    </ul>
                </div>
            </div>
        </div>
        <?php
    }
    }else{
        ?><div class="alert alert-warning"><?=$this->Translation->get('errors.BrakArtykulowNaBlogu',[],true)?></div><?php
    }
    ?>
    </div>
</div>
</div>