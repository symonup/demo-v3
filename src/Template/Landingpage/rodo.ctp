<?php 
if(empty($rodo)){
    if($token=='success'){
        ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-6">
            <div class="alert alert-success"><p>Dziękujemy za uzupełnienie danych.</p></div>
        </div>
    </div>
</div>
<?php
    }else{
    ?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-6">
            <div class="alert alert-danger"><p>Podany adres jest nieprawidłowy. Nie możemy zidentyfikować Twojego adresu email. Sprawdź czy adres w przeglądarce zgadza się z linkiem otrzymanym w wiadomości email.</p></div>
        </div>
    </div>
</div>
<?php
    }
}else{
?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-8">
            <div class="alert alert-info">
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus porta erat pretium lacus eleifend, eu congue nunc scelerisque. Aenean dignissim urna sem, ac varius mi porttitor id. Nullam posuere pellentesque erat, ac congue nunc rhoncus nec. Praesent vitae nisi dui. Ut iaculis bibendum metus, porta tempor est commodo rhoncus. Fusce bibendum, justo id iaculis feugiat, nibh neque venenatis odio, sed ultricies nunc felis in mi. Morbi id convallis nisi. Nam ac turpis tellus. Vivamus ac ante sed lorem pretium semper. Integer vulputate molestie leo at egestas.
            </p>
            </div>
        </div>
    </div>
            <?=$this->Form->create($rodo)?>
    <div class="row justify-content-center">
        <div class="col-12 col-lg-8">
            <div class="rodo-item">
            <?=$this->Form->control('zgoda_1',['type'=>'checkbox','label'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus porta erat pretium lacus eleifend, eu congue nunc scelerisque. Aenean dignissim urna sem, ac varius mi porttitor id.'])?>
            </div>   
            <div class="rodo-item"> 
        <?=$this->Form->control('zgoda_2',['type'=>'checkbox','label'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus porta erat pretium lacus eleifend, eu congue nunc scelerisque. Aenean dignissim urna sem, ac varius mi porttitor id.'])?>
          </div>   
            <div class="rodo-item"> 
          <?=$this->Form->control('zgoda_3',['type'=>'checkbox','label'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus porta erat pretium lacus eleifend, eu congue nunc scelerisque. Aenean dignissim urna sem, ac varius mi porttitor id.'])?>
          </div>   
            <div class="rodo-item"> 
          <?=$this->Form->control('zgoda_4',['type'=>'checkbox','label'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus porta erat pretium lacus eleifend, eu congue nunc scelerisque. Aenean dignissim urna sem, ac varius mi porttitor id.'])?>
         </div>   
            <div class="rodo-item"> 
           <?=$this->Form->control('zgoda_5',['type'=>'checkbox','label'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus porta erat pretium lacus eleifend, eu congue nunc scelerisque. Aenean dignissim urna sem, ac varius mi porttitor id.'])?>
          </div>   
            <div class="rodo-item"> 
          <?=$this->Form->control('zgoda_6',['type'=>'checkbox','label'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus porta erat pretium lacus eleifend, eu congue nunc scelerisque. Aenean dignissim urna sem, ac varius mi porttitor id.'])?>
          </div>   
            <div class="rodo-item"> 
          <?=$this->Form->control('zgoda_7',['type'=>'checkbox','label'=>['text'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus porta erat pretium lacus eleifend, eu congue nunc scelerisque. Aenean dignissim urna sem, ac varius mi porttitor id.'.$this->Form->control('telefon',['type'=>'text','label'=>false,'placeholder'=>'Numer telefonu','style'=>'max-width:200px;']),'escape'=>false]])?>
          
        </div>   
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-12 col-lg-4">
            <button type="submit" class="btn btn-success btn-lg btn-block">Potwierdzam</button>
        </div>
    </div>
        <?=$this->Form->end();?>
</div>
<?php } ?>