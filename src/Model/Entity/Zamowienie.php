<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Zamowienie Entity
 *
 * @property int $id
 * @property int $uzytkownik_id
 * @property string $lng
 * @property int $waluta_id
 * @property string $imie
 * @property string $nazwisko
 * @property string $email
 * @property string $firma
 * @property string $nip
 * @property string $adres_faktury
 * @property string $adres_wysylki
 * @property string $wysylka_ulica
 * @property string $wysylka_nr_domu
 * @property string $wysylka_nr_lokalu
 * @property string $wysylka_kod
 * @property string $wysylka_miasto
 * @property string $wysylka_imie
 * @property string $wysylka_nazwisko
 * @property string $wysylka_firma
 * @property string $wysylka_nip
 * @property string $wysylka_telefon
 * @property string $wysylka_email
 * @property string $wysylka_kraj
 * @property string $faktura_imie
 * @property string $faktura_nazwisko
 * @property string $faktura_firma
 * @property string $faktura_nip
 * @property string $faktura_ulica
 * @property string $faktura_nr_domu
 * @property string $faktura_nr_lokalu
 * @property string $faktura_kod
 * @property string $faktura_miasto
 * @property string $faktura_kraj
 * @property string $telefon
 * @property float $wartosc_produktow
 * @property float $wartosc_vat
 * @property float $wartosc_razem
 * @property float $wartosc_razem_netto
 * @property string $uwagi
 * @property string $notatki
 * @property string $status
 * @property \Cake\I18n\FrozenTime $data
 * @property int $rabat
 * @property int $telefoniczne
 * @property string $status_log
 * @property string $nr_fv
 * @property int $wycena
 * @property int $wycena_wyslana
 * @property string $notatka_status
 * @property float $prowizja
 * @property string $domena
 *
 * @property \App\Model\Entity\Uzytkownik $uzytkownik
 * @property \App\Model\Entity\Walutum $walutum
 * @property \App\Model\Entity\Wysylka $wysylka
 * @property \App\Model\Entity\RodzajPlatnosci $rodzaj_platnosci
 * @property \App\Model\Entity\FakturaPliki[] $faktura_pliki
 * @property \App\Model\Entity\Platnosc[] $platnosc
 * @property \App\Model\Entity\ZamowienieTowar[] $zamowienie_towar
 */
class Zamowienie extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
