<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * WariantCecha Entity
 *
 * @property int $id
 * @property int $towar_id
 * @property string $cecha
 * @property string $typ
 *
 * @property \App\Model\Entity\Towar $towar
 * @property \App\Model\Entity\WariantCechaWartosc[] $wariant_cecha_wartosc
 */
class WariantCecha extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
