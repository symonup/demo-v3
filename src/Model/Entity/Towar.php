<?php

namespace App\Model\Entity;

use Cake\ORM\Behavior\Translate\TranslateTrait;
use Cake\ORM\Entity;

/**
 * Towar Entity
 *
 * @property int $id
 * @property string $kod
 * @property int $ilosc
 * @property int $producent_id
 * @property int $ukryty
 * @property int $promocja
 * @property int $polecany
 * @property int $wyprzedaz
 * @property int $wyrozniony
 * @property int $nowosc
 * @property \Cake\I18n\FrozenTime $best_of_the_week
 * @property string $gwarancja_producenta
 * @property bool $bez_konfekcji
 * @property string $nazwa
 * @property string $opis
 * @property string $opis2
 * @property string $hint_cena
 * @property string $seo_tytul
 * @property string $seo_slowa
 * @property string $seo_opis
 * @property int $kolor_id
 * @property int $wzor_id
 * @property int $opinia_avg
 * @property string $zestaw_nazwa
 * @property bool $wyklucz_z_gm
 * @property int $wariant
 * @property int $na_zamowienie
 * @property string $subiekt_kod
 * @property string $ean
 * @property string $symbole
 * @property int $kolekcja_id
 *
 * @property \App\Model\Entity\Producent $producent
 * @property \App\Model\Entity\Kolor $kolor
 * @property \App\Model\Entity\Wzor $wzor
 * @property \App\Model\Entity\Kolekcja $kolekcja
 * @property \App\Model\Entity\AllegroAukcja[] $allegro_aukcja
 * @property \App\Model\Entity\KategoriaPromocja[] $kategoria_promocja
 * @property \App\Model\Entity\TowarAkcesorium[] $towar_akcesoria
 * @property \App\Model\Entity\TowarCena[] $towar_cena
 * @property \App\Model\Entity\TowarOpinium[] $towar_opinia
 * @property \App\Model\Entity\TowarPodobne[] $towar_podobne
 * @property \App\Model\Entity\TowarPolecane[] $towar_polecane
 * @property \App\Model\Entity\TowarUzytkownikRabat[] $towar_uzytkownik_rabat
 * @property \App\Model\Entity\TowarWariant[] $towar_wariant
 * @property \App\Model\Entity\TowarZdjecie[] $towar_zdjecie
 * @property \App\Model\Entity\WariantCecha[] $wariant_cecha
 * @property \App\Model\Entity\WariantCechaWartosc[] $wariant_cecha_wartosc
 * @property \App\Model\Entity\Zestaw[] $zestaw
 * @property \App\Model\Entity\ZestawTowarWymiana[] $zestaw_towar_wymiana
 * @property \App\Model\Entity\AllegroZamowienie[] $allegro_zamowienie
 * @property \App\Model\Entity\Artykul[] $artykul
 * @property \App\Model\Entity\Atrybut[] $atrybut
 * @property \App\Model\Entity\Film[] $film
 * @property \App\Model\Entity\I18n[] $i18n
 * @property \App\Model\Entity\Kategorium[] $kategoria
 * @property \App\Model\Entity\Przeznaczenie[] $przeznaczenie
 * @property \App\Model\Entity\Zamowienie[] $zamowienie
 */
class Towar extends Entity {

    use TranslateTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
    protected $_virtual = ['price','price_type','price_vat'];

    protected function _getPrice() {
        if(!empty($this->towar_cena_default)) {
            if (!empty($this->promocja) && $this->towar_cena_default->cena_promocja > 0) {
                return $this->towar_cena_default->cena_promocja;
            }
            return $this->towar_cena_default->cena_sprzedazy;
        }
    }
    protected function _getPriceType() {
        if(!empty($this->towar_cena_default)) {
            return $this->towar_cena_default->rodzaj;
        }
    }
    protected function _getPriceVat() {
        if(!empty($this->towar_cena_default) && !empty($this->towar_cena_default->vat)) {
            return $this->towar_cena_default->vat->stawka;
        }
    }

}
