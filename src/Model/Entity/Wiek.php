<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Wiek Entity
 *
 * @property int $id
 * @property string $nazwa
 * @property int $miesiace_od
 * @property int $miesiace_do
 * @property int $lata_od
 * @property int $lata_do
 * @property int $kolejnosc
 *
 * @property \App\Model\Entity\Towar[] $towar
 */
class Wiek extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
