<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TowarZdjecie Entity
 *
 * @property int $id
 * @property int $towar_id
 * @property string $plik
 * @property int $kolejnosc
 * @property bool $domyslne
 * @property bool $techniczny
 * @property string $alt
 * @property int $znak_wodny
 * @property bool $ukryte
 * @property string $orginal
 *
 * @property \App\Model\Entity\Towar $towar
 */
class TowarZdjecie extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
