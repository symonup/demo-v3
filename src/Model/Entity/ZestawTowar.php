<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ZestawTowar Entity
 *
 * @property int $id
 * @property int $towar_id
 * @property int $zestaw_id
 * @property int $wymiana
 * @property int $kategoria_id
 *
 * @property \App\Model\Entity\Towar $towar
 * @property \App\Model\Entity\Zestaw $zestaw
 * @property \App\Model\Entity\Kategoria $kategoria
 * @property \App\Model\Entity\ZestawTowarWymiana[] $zestaw_towar_wymiana
 */
class ZestawTowar extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
