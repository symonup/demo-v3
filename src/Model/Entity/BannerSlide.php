<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * BannerSlide Entity
 *
 * @property int $id
 * @property int $banner_id
 * @property string $type
 * @property string $file
 * @property bool $is_linked
 * @property string $link
 * @property string $title
 * @property string $target
 * @property int $height
 * @property string $css
 *
 * @property \App\Model\Entity\Banner $banner
 */
class BannerSlide extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
