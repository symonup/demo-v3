<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Wysylka Entity
 *
 * @property int $id
 * @property string $nazwa
 * @property int $vat_id
 * @property float $koszt_netto
 * @property float $koszt_brutto
 * @property float $koszt_dodatkowy
 * @property string $opis
 * @property bool $aktywna
 * @property string $obrazek
 * @property int $duzy_gabaryt
 *
 * @property \App\Model\Entity\Vat $vat
 * @property \App\Model\Entity\WysylkaKoszt[] $wysylka_koszt
 * @property \App\Model\Entity\Zamowienie[] $zamowienie
 * @property \App\Model\Entity\Platnosc[] $platnosc
 * @property \App\Model\Entity\WysylkaNazwaTranslation $nazwa_translation
 * @property \App\Model\Entity\WysylkaI18n[] $_i18n
 */
class Wysylka extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
