<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Walutum Entity
 *
 * @property int $id
 * @property string $symbol
 * @property string $nazwa
 * @property float $kurs
 * @property int $domyslna
 * @property string $separator_d
 * @property string $separator_t
 * @property int $symbol_przed
 */
class Walutum extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
