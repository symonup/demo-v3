<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * WariantCechaWartosc Entity
 *
 * @property int $id
 * @property int $wariant_cecha_id
 * @property int $towar_id
 * @property float $wartosc_float
 * @property string $wartosc_text
 * @property int $kolor_id
 * @property int $wzor_id
 *
 * @property \App\Model\Entity\WariantCecha $wariant_cecha
 * @property \App\Model\Entity\Towar $towar
 * @property \App\Model\Entity\Kolor $kolor
 * @property \App\Model\Entity\Wzor $wzor
 */
class WariantCechaWartosc extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
