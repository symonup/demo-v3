<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Zapytanie Entity
 *
 * @property int $id
 * @property string $nr_zgloszenia
 * @property int $uzytkownik_id
 * @property string $imie
 * @property string $email
 * @property bool $zgoda_dane
 * @property string $telefon
 * @property int $platforma
 * @property int $nosnik
 * @property string $tresc
 * @property \Cake\I18n\FrozenTime $data_dodania
 * @property string $odpowiedz
 * @property \Cake\I18n\FrozenTime $data_wyslania
 *
 * @property \App\Model\Entity\Uzytkownik $uzytkownik
 */
class Zapytanie extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
