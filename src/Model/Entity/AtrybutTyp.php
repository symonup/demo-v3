<?php
namespace App\Model\Entity;

use Cake\ORM\Behavior\Translate\TranslateTrait;
use Cake\ORM\Entity;

/**
 * AtrybutTyp Entity
 *
 * @property int $id
 * @property string $nazwa
 * @property int $kolejnosc
 * @property int $atrybut_typ_parent_id
 *
 * @property \App\Model\Entity\AtrybutTypParent $atrybut_typ_parent
 * @property \App\Model\Entity\Atrybut[] $atrybut
 * @property \App\Model\Entity\I18n[] $i18n
 */
class AtrybutTyp extends Entity
{
use TranslateTrait;
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
