<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * WyszukiwarkaBox Entity
 *
 * @property int $id
 * @property int $wyszukiwarka_id
 * @property int $nr_kolumny
 * @property string $tytul
 * @property int $kategoria
 * @property string $atrybut
 *
 * @property \App\Model\Entity\Wyszukiwarka $wyszukiwarka
 */
class WyszukiwarkaBox extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
