<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Powiadomienium Entity
 *
 * @property int $id
 * @property string $typ
 * @property int $uzytkownik_id
 * @property string $imie
 * @property string $email
 * @property int $zgoda
 * @property int $towar_id
 * @property float $cena
 * @property \Cake\I18n\FrozenTime $data_dodania
 * @property bool $wyslane
 * @property \Cake\I18n\FrozenTime $data_wyslania
 *
 * @property \App\Model\Entity\Uzytkownik $uzytkownik
 * @property \App\Model\Entity\Towar $towar
 */
class Powiadomienium extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
