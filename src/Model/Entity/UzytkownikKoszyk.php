<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UzytkownikKoszyk Entity
 *
 * @property int $id
 * @property int $uzytkownik_id
 * @property string $items
 * @property bool $active
 * @property \Cake\I18n\FrozenTime $create_date
 * @property \Cake\I18n\FrozenTime $last_update
 *
 * @property \App\Model\Entity\Uzytkownik $uzytkownik
 */
class UzytkownikKoszyk extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
