<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * KoszykTowar Entity
 *
 * @property int $id
 * @property int $koszyk_id
 * @property int $towar_id
 * @property string $koszyk_key
 * @property int $dodany
 * @property int $usuniety
 * @property \Cake\I18n\Time $data_dodania
 * @property \Cake\I18n\Time $data_usuniecia
 * @property int $ilosc
 * @property float $cena
 * @property string $notatki
 *
 * @property \App\Model\Entity\Towar $towar
 * @property \App\Model\Entity\Koszyk $koszyk
 */
class KoszykTowar extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
