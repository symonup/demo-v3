<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Vat Entity
 *
 * @property int $id
 * @property int $stawka
 * @property string $nazwa
 * @property bool $zwolniony
 *
 * @property \App\Model\Entity\TowarCena[] $towar_cena
 * @property \App\Model\Entity\Wysylka[] $wysylka
 */
class Vat extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
