<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AllegroAukcja Entity
 *
 * @property int $id
 * @property int $towar_id
 * @property int $szablon
 * @property string $item_id
 * @property string $item_info
 * @property int $item_is_allegro_standard
 * @property int $sprzedane
 *
 * @property \App\Model\Entity\Towar $towar
 * @property \App\Model\Entity\Item $item
 * @property \App\Model\Entity\AllegroAukcjaPrzesylka[] $allegro_aukcja_przesylka
 * @property \App\Model\Entity\AllegroAukcjaWariant[] $allegro_aukcja_wariant
 * @property \App\Model\Entity\AllegroPolaKategorium[] $allegro_pola_kategoria
 */
class AllegroAukcja extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
