<?php
namespace App\Model\Entity;

use Cake\ORM\Behavior\Translate\TranslateTrait;
use Cake\ORM\Entity;

/**
 * Szablon Entity
 *
 * @property int $id
 * @property string $nazwa
 * @property string $label
 * @property string $temat
 * @property string $tresc
 * @property string $opis
 *
 * @property \App\Model\Entity\I18n[] $i18n
 */
class Szablon extends Entity
{
use TranslateTrait;
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
