<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AllegroZamowienieTowar Entity
 *
 * @property int $id
 * @property int $towar_id
 * @property int $allegro_zamowienie_id
 * @property string $item_id
 * @property string $nazwa
 * @property float $cena_podstawowa_za_sztuke
 * @property string $waluta
 * @property float $cena_za_sztuke
 * @property int $ilosc
 * @property \Cake\I18n\FrozenTime $data_zakupu
 * @property string $buy_id
 *
 * @property \App\Model\Entity\AllegroZamowienie $allegro_zamowienie
 * @property \App\Model\Entity\Oferta $oferta
 * @property \App\Model\Entity\Zakup $zakup
 */
class AllegroZamowienieTowar extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
