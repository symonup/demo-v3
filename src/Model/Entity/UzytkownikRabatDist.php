<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UzytkownikRabatDist Entity
 *
 * @property int $id
 * @property int $uzytkownik_id
 * @property float $kwota_zamowienia
 * @property float $wartosc_rabatu
 *
 * @property \App\Model\Entity\Uzytkownik $uzytkownik
 */
class UzytkownikRabatDist extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
