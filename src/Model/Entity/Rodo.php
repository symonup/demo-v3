<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Rodo Entity
 *
 * @property int $id
 * @property string $email
 * @property string $token
 * @property \Cake\I18n\FrozenTime $data_zatwierdzenia
 * @property int $zgoda_1
 * @property int $zgoda_2
 * @property int $zgoda_3
 * @property int $zgoda_4
 * @property int $zgoda_5
 * @property int $zgoda_6
 * @property int $zgoda_7
 * @property int $zgoda_8
 * @property string $telefon
 * @property int $wyslany
 * @property \Cake\I18n\FrozenTime $data_wyslania
 */
class Rodo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'token'
    ];
}
