<?php
namespace App\Model\Entity;

use Cake\ORM\Behavior\Translate\TranslateTrait;
use Cake\ORM\Entity;

/**
 * Sezon Entity
 *
 * @property int $id
 * @property string $nazwa
 * @property int $kolejnosc
 * @property int $ukryty
 * @property string $seo_tytul
 * @property string $seo_opis
 * @property string $seo_slowa
 * @property string $naglowek
 * @property string $menu_tekst_naglowek
 * @property string $menu_tekst
 *
 * @property \App\Model\Entity\Kategorium[] $kategoria
 * @property \App\Model\Entity\I18n[] $i18n
 */
class Sezon extends Entity
{
use TranslateTrait;
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
