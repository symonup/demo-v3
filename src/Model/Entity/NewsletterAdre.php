<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * NewsletterAdre Entity
 *
 * @property int $id
 * @property int $jezyk_id
 * @property string $email
 * @property bool $potwierdzony
 * @property string $token
 * @property \Cake\I18n\FrozenTime $data
 * @property int $wyslany
 * @property int $status
 *
 * @property \App\Model\Entity\Jezyk $jezyk
 */
class NewsletterAdre extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'token'
    ];
}
