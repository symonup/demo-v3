<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FakturaPozycja Entity
 *
 * @property int $id
 * @property int $faktura_id
 * @property int $lp
 * @property string $nazwa
 * @property int $typ_towaru
 * @property string $symbol
 * @property bool $rabat_procentowy
 * @property bool $rabat_od_ceny
 * @property bool $rabat_pozycja
 * @property bool $rabat_blokada
 * @property float $rabat_wartosc
 * @property float $rabat_procent
 * @property string $jednostka
 * @property float $ilosc_jednostka
 * @property float $ilosc_magazyn
 * @property float $cena_magazyn
 * @property float $cena_netto
 * @property float $cena_brutto
 * @property float $stawka_vat
 * @property float $wartosc_netto
 * @property float $wartosc_vat
 * @property float $wartosc_brutto
 * @property float $koszt
 * @property string $opis_uslugi
 * @property string $nazwa_uslugi
 *
 * @property \App\Model\Entity\Faktura $faktura
 */
class FakturaPozycja extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
