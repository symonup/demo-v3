<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * KartaPodarunkowaKod Entity
 *
 * @property int $id
 * @property int $karta_podarunkowa_id
 * @property int $zamowienie_id
 * @property int $uzytkownik_id
 * @property string $kod
 * @property float $wartosc
 * @property \Cake\I18n\FrozenTime $data_dodania
 * @property \Cake\I18n\FrozenTime $data_uzycia
 * @property \Cake\I18n\FrozenTime $data_waznosci
 * @property bool $aktywny
 * @property bool $uzyty
 *
 * @property \App\Model\Entity\KartaPodarunkowa $karta_podarunkowa
 * @property \App\Model\Entity\Zamowienie $zamowienie
 * @property \App\Model\Entity\Uzytkownik $uzytkownik
 */
class KartaPodarunkowaKod extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
