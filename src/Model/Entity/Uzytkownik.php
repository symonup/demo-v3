<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Uzytkownik Entity
 *
 * @property int $id
 * @property string $password
 * @property string $email
 * @property string $telefon
 * @property string $imie
 * @property string $nazwisko
 * @property bool $newsletter
 * @property string $rabat
 * @property string $token
 * @property \Cake\I18n\FrozenTime $token_data
 * @property int $waluta_id
 * @property int $administrator_id
 * @property \Cake\I18n\FrozenTime $data_rejestracji
 * @property bool $aktywny
 * @property string $firma
 * @property string $nip
 *
 * @property \App\Model\Entity\Walutum $walutum
 * @property \App\Model\Entity\Administrator $administrator
 * @property \App\Model\Entity\Platnosc[] $platnosc
 * @property \App\Model\Entity\TowarUzytkownikRabat[] $towar_uzytkownik_rabat
 * @property \App\Model\Entity\UzytkownikAdre[] $uzytkownik_adres
 * @property \App\Model\Entity\Zamowienie[] $zamowienie
 */
class Uzytkownik extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
    protected function _setPassword($value) {
        $hasher = new DefaultPasswordHasher();
        return $hasher->hash($value);
    }
}
