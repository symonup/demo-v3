<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * KodyRabatoweProducent Entity
 *
 * @property int $id
 * @property int $producent_id
 * @property int $kody_rabatowe_id
 *
 * @property \App\Model\Entity\Producent $producent
 * @property \App\Model\Entity\KodyRabatowe $kody_rabatowe
 */
class KodyRabatoweProducent extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
