<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Kolor Entity
 *
 * @property int $id
 * @property string $nazwa
 * @property string $hex
 *
 * @property \App\Model\Entity\Towar[] $towar
 * @property \App\Model\Entity\WariantCechaWartosc[] $wariant_cecha_wartosc
 * @property \App\Model\Entity\KolorNazwaTranslation $nazwa_translation
 * @property \App\Model\Entity\KolorI18n[] $_i18n
 */
class Kolor extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
