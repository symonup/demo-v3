<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Opinium Entity
 *
 * @property int $id
 * @property string $tresc
 * @property string $podpis
 * @property bool $ukryta
 * @property int $kolejnosc
 *
 * @property \App\Model\Entity\Towar[] $towar
 */
class Opinium extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
