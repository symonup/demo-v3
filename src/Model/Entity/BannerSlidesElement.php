<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * BannerSlidesElement Entity
 *
 * @property int $id
 * @property int $banner_slides_id
 * @property string $type
 * @property string $file
 * @property string $text
 * @property string $title
 * @property bool $tooltip
 * @property string $pos_left
 * @property string $pos_top
 * @property string $position
 * @property string $css
 * @property string $link
 * @property string $target
 * @property int $is_linked
 * @property int $_width
 * @property int $_height
 *
 * @property \App\Model\Entity\BannerSlide $banner_slide
 */
class BannerSlidesElement extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
