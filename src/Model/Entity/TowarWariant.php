<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TowarWariant Entity
 *
 * @property int $id
 * @property int $towar_id
 * @property int $rozmiar_id
 * @property int $kolor_id
 * @property int $ilosc
 * @property string $ean
 *
 * @property \App\Model\Entity\Towar $towar
 * @property \App\Model\Entity\Rozmiar $rozmiar
 * @property \App\Model\Entity\Kolor $kolor
 */
class TowarWariant extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
