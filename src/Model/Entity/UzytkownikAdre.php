<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UzytkownikAdre Entity
 *
 * @property int $id
 * @property int $uzytkownik_id
 * @property string $nazwa_adresu
 * @property string $imie
 * @property string $nazwisko
 * @property string $firma
 * @property string $nip
 * @property string $telefon
 * @property string $ulica
 * @property string $nr_domu
 * @property string $nr_lokalu
 * @property string $kod
 * @property string $miasto
 * @property int $domyslny_wysylka
 * @property int $domyslny_faktura
 * @property int $typ
 *
 * @property \App\Model\Entity\Uzytkownik $uzytkownik
 */
class UzytkownikAdre extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
