<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Faktura Entity
 *
 * @property int $id
 * @property int $zamowienie_id
 * @property string $typ
 * @property int $status
 * @property int $status_fiskalny
 * @property string $id_dokumentu
 * @property string $numer_dostawcy
 * @property string $rozszerzenie_numeru
 * @property string $numer_dokumentu
 * @property string $numer_do_korekty
 * @property \Cake\I18n\FrozenDate $data_wystawienia_do_korekty
 * @property string $numer_zamowienia
 * @property string $magazyn_docelowy_mm
 * @property string $kod_kontrahenta
 * @property string $nazwa_skrocona
 * @property string $nazwa_pelna
 * @property string $miasto
 * @property string $kod_pocztowy
 * @property string $adres
 * @property string $nip
 * @property string $kategoria_dokumentu
 * @property string $podtytul_kategorii
 * @property string $miejsce_wystawienia
 * @property \Cake\I18n\FrozenDate $data_wystawienia
 * @property \Cake\I18n\FrozenDate $data_sprzedazy
 * @property \Cake\I18n\FrozenDate $data_otrzymania
 * @property int $ilosc_pozycji
 * @property bool $wg_cen_netto
 * @property string $aktywna_cena
 * @property float $wartosc_netto
 * @property float $wartosc_vat
 * @property float $wartosc_brutto
 * @property float $koszt
 * @property string $rabat_nazwa
 * @property float $rabat_procent
 * @property string $forma_platnosci
 * @property \Cake\I18n\FrozenDate $termin_platnosci
 * @property float $kwota_zaplacona_przy_odbiorze
 * @property float $wartosc_do_zaplaty
 * @property int $zaokraglenie_do_zaplaty
 * @property int $zaokraglenie_vat
 * @property bool $automatyczne_przeliczanie
 * @property int $statusy_specjalne
 * @property string $wystawiajacy
 * @property string $odbiorca
 * @property string $podstawa_wydania
 * @property float $wartosc_opakowan
 * @property float $wartosc_zwroconych_opakowan
 * @property string $waluta
 * @property float $kurs_waluty
 * @property string $uwagi
 * @property string $komentarz
 * @property string $podtytul
 * @property string $empty_data
 * @property bool $import_dokumentu
 * @property bool $eksportowy
 * @property int $rodzaj_transakcji
 * @property string $platnosc_karta_nazwa
 * @property float $platnosc_karta_kwota
 * @property string $platnosc_kredyt_nazwa
 * @property float $platnosc_kredyt_kwota
 * @property string $panstwo
 * @property string $prefix_ue
 * @property bool $czy_ue
 * @property string $token
 * @property string $sklep_nazwa
 * @property string $sklep_miasto
 * @property string $sklep_kod_pocztowy
 * @property string $sklep_adres
 * @property string $sklep_nip
 *
 * @property \App\Model\Entity\Zamowienie $zamowienie
 * @property \App\Model\Entity\FakturaPozycja[] $faktura_pozycja
 */
class Faktura extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'token'
    ];
}
