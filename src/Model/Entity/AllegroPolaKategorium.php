<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AllegroPolaKategorium Entity
 *
 * @property int $id
 * @property int $allegro_aukcja_id
 * @property int $fid
 * @property string $value
 * @property int $type
 * @property int $field_type
 *
 * @property \App\Model\Entity\AllegroAukcja $allegro_aukcja
 */
class AllegroPolaKategorium extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
