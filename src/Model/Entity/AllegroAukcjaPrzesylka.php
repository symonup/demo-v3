<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AllegroAukcjaPrzesylka Entity
 *
 * @property int $id
 * @property int $allegro_aukcja_id
 * @property int $fid
 * @property float $pierwsza_sztuka
 * @property float $kolejna_sztuka
 * @property int $ilosc_w_paczce
 *
 * @property \App\Model\Entity\AllegroAukcja $allegro_aukcja
 */
class AllegroAukcjaPrzesylka extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
