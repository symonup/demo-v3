<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * KartaPodarunkowa Entity
 *
 * @property int $id
 * @property string $nazwa
 * @property float $wartosc
 * @property float $cena
 * @property \Cake\I18n\FrozenTime $data_dodania
 * @property bool $aktywna
 *
 * @property \App\Model\Entity\KartaPodarunkowaKod[] $karta_podarunkowa_kod
 */
class KartaPodarunkowa extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
