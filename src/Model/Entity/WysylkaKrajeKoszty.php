<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * WysylkaKrajeKoszty Entity
 *
 * @property int $id
 * @property int $wysylka_id
 * @property string $kraj
 * @property float $koszt
 *
 * @property \App\Model\Entity\Wysylka $wysylka
 */
class WysylkaKrajeKoszty extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
