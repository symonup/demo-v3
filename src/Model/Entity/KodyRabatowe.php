<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * KodyRabatowe Entity
 *
 * @property int $id
 * @property string $nazwa
 * @property string $kod
 * @property float $rabat
 * @property \Cake\I18n\FrozenTime $data_start
 * @property \Cake\I18n\FrozenTime $data_end
 * @property bool $zablokuj
 * @property \Cake\I18n\FrozenTime $data_dodania
 */
class KodyRabatowe extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
