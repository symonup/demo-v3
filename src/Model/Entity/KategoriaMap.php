<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * KategoriaMap Entity
 *
 * @property int $id
 * @property int $kategoria_id
 * @property string $hurtownia
 * @property string $nazwa
 *
 * @property \App\Model\Entity\Kategorium $kategorium
 */
class KategoriaMap extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
