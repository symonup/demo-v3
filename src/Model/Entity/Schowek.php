<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Schowek Entity
 *
 * @property int $id
 * @property int $uzytkownik_id
 * @property int $towar_id
 * @property \Cake\I18n\FrozenTime $data
 *
 * @property \App\Model\Entity\Uzytkownik $uzytkownik
 * @property \App\Model\Entity\Towar $towar
 */
class Schowek extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
