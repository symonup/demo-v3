<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TowarAtrybut Entity
 *
 * @property int $id
 * @property int $atrybut_id
 * @property int $towar_id
 * @property float $wartosc
 * @property int $kategoria_id
 * @property int $atrybut_podrzedne_id
 * @property bool $wariant
 *
 * @property \App\Model\Entity\Atrybut $atrybut
 * @property \App\Model\Entity\Towar $towar
 * @property \App\Model\Entity\Kategoria $kategoria
 * @property \App\Model\Entity\AtrybutPodrzedne $atrybut_podrzedne
 */
class TowarAtrybut extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
