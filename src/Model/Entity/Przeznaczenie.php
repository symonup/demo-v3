<?php
namespace App\Model\Entity;

use Cake\ORM\Behavior\Translate\TranslateTrait;
use Cake\ORM\Entity;

/**
 * Przeznaczenie Entity
 *
 * @property int $id
 * @property string $nazwa
 * @property int $ikona_id
 * @property int $kolejnosc
 * @property int $ukryte
 *
 * @property \App\Model\Entity\Ikona $ikona
 * @property \App\Model\Entity\I18n[] $i18n
 * @property \App\Model\Entity\Towar[] $towar
 */
class Przeznaczenie extends Entity
{
use TranslateTrait;
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
