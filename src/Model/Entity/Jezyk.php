<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Jezyk Entity
 *
 * @property int $id
 * @property string $nazwa
 * @property string $locale
 * @property string $symbol
 * @property int $waluta_id
 * @property int $domyslny
 * @property int $aktywny
 * @property string $flaga
 * @property string $domena
 *
 * @property \App\Model\Entity\Walutum $walutum
 * @property \App\Model\Entity\NewsletterAdre[] $newsletter_adres
 */
class Jezyk extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
