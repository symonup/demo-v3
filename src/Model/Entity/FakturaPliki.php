<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FakturaPliki Entity
 *
 * @property int $id
 * @property int $zamowienie_id
 * @property int $allegro_zamowienie_id
 * @property string $plik
 * @property \Cake\I18n\FrozenTime $data
 *
 * @property \App\Model\Entity\Zamowienie $zamowienie
 * @property \App\Model\Entity\AllegroZamowienie $allegro_zamowienie
 */
class FakturaPliki extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
