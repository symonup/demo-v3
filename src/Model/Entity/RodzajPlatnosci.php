<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RodzajPlatnosci Entity
 *
 * @property int $id
 * @property string $nazwa
 * @property string $nazwa_wyswietlana
 * @property bool $platnosc_elektroniczna
 * @property bool $pobranie
 * @property string $opis
 * @property string $konfig1
 * @property string $konfig2
 * @property string $konfig3
 * @property string $konfig4
 * @property string $waluta
 * @property bool $aktywna
 * @property int $koszt_dodatkowy
 * @property float $prowizja
 * @property float $darmowa_wysylka
 *
 * @property \App\Model\Entity\Platnosc[] $platnosc
 * @property \App\Model\Entity\WysylkaPlatnosc[] $wysylka_platnosc
 * @property \App\Model\Entity\Zamowienie[] $zamowienie
 * @property \App\Model\Entity\RodzajPlatnosciNazwaTranslation $nazwa_translation
 * @property \App\Model\Entity\RodzajPlatnosciI18n[] $_i18n
 */
class RodzajPlatnosci extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
