<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * WysylkaKoszt Entity
 *
 * @property int $id
 * @property int $wysylka_id
 * @property int $od
 * @property float $koszt_netto
 * @property float $koszt_brutto
 *
 * @property \App\Model\Entity\Wysylka $wysylka
 */
class WysylkaKoszt extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
