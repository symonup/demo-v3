<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ZamowienieDhl Entity
 *
 * @property int $id
 * @property int $zamowienie_id
 * @property int $allegro_zamowienie_id
 * @property string $shipmentId
 * @property \Cake\I18n\Time $shipmentDate
 * @property string $labelType
 * @property string $labelName
 * @property string $labelData
 * @property string $labelMimeType
 * @property string $orderId
 * @property \Cake\I18n\Time $data_dodania
 * @property string $opis
 * @property \Cake\I18n\Time $godzina_od
 * @property \Cake\I18n\Time $godzina_do
 * @property string $type
 * @property string $uwagi
 * @property string $requestData
 *
 * @property \App\Model\Entity\Zamowienie $zamowienie
 * @property \App\Model\Entity\AllegroZamowienie $allegro_zamowienie
 */
class ZamowienieDhl extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
