<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ZamowienieTowar Entity
 *
 * @property int $id
 * @property int $zamowienie_id
 * @property int $ilosc
 * @property int $ilosc_w_kartonie
 * @property float $cena_za_sztuke_netto
 * @property float $cena_za_sztuke_brutto
 * @property float $cena_razem_netto
 * @property float $cena_razem_brutto
 * @property string $kod
 * @property string $nazwa
 * @property int $towar_id
 * @property int $wariant_id
 * @property int $jednostka_id
 * @property int $vat_id
 * @property float $vat_stawka
 *
 * @property \App\Model\Entity\Zamowienie $zamowienie
 * @property \App\Model\Entity\Towar $towar
 * @property \App\Model\Entity\AllegroOferta $allegro_oferta
 * @property \App\Model\Entity\AllegroZakup $allegro_zakup
 * @property \App\Model\Entity\Zestaw $zestaw
 */
class ZamowienieTowar extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
