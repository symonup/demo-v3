<?php
namespace App\Model\Entity;

use Cake\ORM\Behavior\Translate\TranslateTrait;
use Cake\ORM\Entity;

/**
 * Atrybut Entity
 *
 * @property int $id
 * @property int $atrybut_typ_id
 * @property int $kategoria_id
 * @property string $nazwa
 * @property int $pole
 * @property int $kolejnosc
 * @property string $pod_tytul
 *
 * @property \App\Model\Entity\AtrybutTyp $atrybut_typ
 * @property \App\Model\Entity\Kategorium[] $kategoria
 * @property \App\Model\Entity\AtrybutPodrzedne[] $atrybut_podrzedne
 * @property \App\Model\Entity\I18n[] $i18n
 * @property \App\Model\Entity\Towar[] $towar
 */
class Atrybut extends Entity
{
use TranslateTrait;
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
