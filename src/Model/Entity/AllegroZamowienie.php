<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AllegroZamowienie Entity
 *
 * @property int $id
 * @property int $allegro_konto_id
 * @property string $order_id
 * @property string $kupujacy_id
 * @property string $kupujacy_email
 * @property string $kupujacy_login
 * @property bool $kupujacy_guest
 * @property string $kupujacy_telefon
 * @property string $payment_id
 * @property string $payment_type
 * @property string $payment_provider
 * @property \Cake\I18n\FrozenTime $payment_finished_at
 * @property float $payment_wplata_kwota
 * @property string $payment_wplata_waluta
 * @property string $allegro_status
 * @property string $allegro_wysylka_id
 * @property string $allegro_wysylka_nazwa
 * @property float $allegro_wysylka_koszt
 * @property string $allegro_wysylka_waluta
 * @property bool $allegro_wysylka_smart
 * @property bool $faktura
 * @property float $wartosc_razem
 * @property string $waluta
 * @property string $wysylka_imie
 * @property string $wysylka_nazwisko
 * @property string $wysylka_ulica
 * @property string $wysylka_miasto
 * @property string $wysylka_kod
 * @property string $wysylka_kraj
 * @property string $wysylka_telefon
 * @property string $paczkomat_id
 * @property string $paczkomat_nazwa
 * @property string $paczkomat_opis
 * @property string $paczkomat_adres
 * @property string $faktura_ulica
 * @property string $faktura_miasto
 * @property string $faktura_kod
 * @property string $faktura_kraj
 * @property string $faktura_firma
 * @property string $faktura_nip
 * @property \Cake\I18n\FrozenTime $data_zakupu
 * @property string $status
 * @property string $uwagi
 * @property string $notatki
 * @property string $nr_listu_przewozowego
 * @property string $kurier_typ
 *
 * @property \App\Model\Entity\Transakcja $transakcja
 * @property \App\Model\Entity\Kupujacy $kupujacy
 * @property \App\Model\Entity\RodzajDostawy $rodzaj_dostawy
 * @property \App\Model\Entity\Zamowienie[] $zamowienie
 * @property \App\Model\Entity\FakturaPliki[] $faktura_pliki
 * @property \App\Model\Entity\Towar[] $towar
 */
class AllegroZamowienie extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
