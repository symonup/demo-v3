<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CallBack Entity
 *
 * @property int $id
 * @property string $telefon
 * @property \Cake\I18n\FrozenTime $data
 * @property int $status
 * @property string $notatki
 * @property \Cake\I18n\FrozenTime $data_status
 * @property int $administrator_id
 * @property string $administrator_dane
 *
 * @property \App\Model\Entity\Administrator $administrator
 */
class CallBack extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
