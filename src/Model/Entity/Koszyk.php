<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Koszyk Entity
 *
 * @property int $id
 * @property string $session_id
 * @property int $uzytkownik_id
 * @property \Cake\I18n\Time $data
 * @property \Cake\I18n\Time $last_update
 * @property int $zamowienie_id
 * @property bool $aktywny
 *
 * @property \App\Model\Entity\Uzytkownik $uzytkownik
 * @property \App\Model\Entity\Zamowienie $zamowienie
 * @property \App\Model\Entity\KoszykTowar[] $koszyk_towar
 */
class Koszyk extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
