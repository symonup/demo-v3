<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Strona Entity
 *
 * @property int $id
 * @property int $menu_dolne
 * @property int $ukryta
 * @property \Cake\I18n\FrozenTime $data
 * @property string $nazwa
 * @property string $tresc
 * @property string $seo_tytul
 * @property string $seo_slowa
 * @property string $seo_opis
 * @property bool $allow_all
 * @property string $type
 */
class Strona extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
