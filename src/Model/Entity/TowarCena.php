<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TowarCena Entity
 *
 * @property int $id
 * @property int $towar_id
 * @property float $waga
 * @property float $cena_netto
 * @property float $cena_brutto
 * @property float $cena_sprzedazy_netto
 * @property float $cena_sprzedazy
 * @property float $cena_promocja_netto
 * @property float $cena_promocja_brutto
 * @property int $jednostka_id
 * @property float $wartosc_minimalna
 * @property int $vat_id
 * @property int $wartosc_minimalna_count
 *
 * @property \App\Model\Entity\Towar $towar
 * @property \App\Model\Entity\Jednostka $jednostka
 * @property \App\Model\Entity\Vat $vat
 */
class TowarCena extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
