<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AllegroKonto Entity
 *
 * @property int $id
 * @property string $restClientId
 * @property string $restClientSecret
 * @property string $restApiKey
 * @property string $restRedirectUri
 * @property string $soapApiKey
 * @property string $token
 * @property int $defaultCategory
 * @property string $defaultCategoryPath
 * @property string $defaultCategoryPathId
 * @property string $name
 * @property bool $isDefault
 */
class AllegroKonto extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'token'
    ];
}
