<?php
namespace App\Model\Entity;

use Cake\ORM\Behavior\Translate\TranslateTrait;
use Cake\ORM\Entity;

/**
 * HotDeal Entity
 *
 * @property int $id
 * @property int $towar_id
 * @property float $cena
 * @property string $nazwa
 * @property \Cake\I18n\FrozenTime $data_od
 * @property \Cake\I18n\FrozenTime $data_do
 * @property int $ilosc
 * @property int $sprzedano
 * @property bool $aktywna
 *
 * @property \App\Model\Entity\Towar $towar
 */
class HotDeal extends Entity
{

    use TranslateTrait;
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
