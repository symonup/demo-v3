<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TowarOpinium Entity
 *
 * @property int $id
 * @property int $towar_id
 * @property \Cake\I18n\FrozenTime $data
 * @property string $autor
 * @property int $ocena
 * @property string $opinia
 * @property string $plusy
 * @property string $minusy
 * @property string $jezyk
 * @property int $status
 *
 * @property \App\Model\Entity\Towar $towar
 */
class TowarOpinium extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
