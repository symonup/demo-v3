<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Platnosc Entity
 *
 * @property int $id
 * @property int $uzytkownik_id
 * @property int $zamowienie_id
 * @property int $rodzaj_platnosci_id
 * @property int $waluta_id
 * @property float $kwota
 * @property string $opis
 * @property string $opis_lng
 * @property string $token
 * @property string $token2
 * @property string $status
 * @property int $status2
 * @property string $blad
 * @property \Cake\I18n\FrozenTime $data
 * @property \Cake\I18n\FrozenTime $data_rozpoczecia
 * @property bool $doplata
 *
 * @property \App\Model\Entity\Uzytkownik $uzytkownik
 * @property \App\Model\Entity\Zamowienie $zamowienie
 * @property \App\Model\Entity\RodzajPlatnosci $rodzaj_platnosci
 * @property \App\Model\Entity\Walutum $walutum
 * @property \App\Model\Entity\Wysylka[] $wysylka
 */
class Platnosc extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'token'
    ];
}
