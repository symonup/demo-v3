<?php
namespace App\Model\Entity;

use Cake\ORM\Behavior\Translate\TranslateTrait;
use Cake\ORM\Entity;

/**
 * Artykul Entity
 *
 * @property int $id
 * @property string $tytul
 * @property string $pod_tytul
 * @property string $tresc
 * @property string $zdjecie_1
 * @property string $zdjecie_2
 * @property \Cake\I18n\FrozenTime $data
 * @property bool $ukryty
 * @property int $kolejnosc
 * @property string $seo_slowa
 * @property string $seo_tytul
 * @property string $seo_opis
 *
 * @property \App\Model\Entity\I18n[] $i18n
 * @property \App\Model\Entity\Towar[] $towar
 */
class Artykul extends Entity
{
use TranslateTrait;
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
