<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Miasto Entity
 *
 * @property int $id
 * @property string $nazwa
 * @property bool $ukryte
 * @property float $szerokosc
 * @property float $dlugosc
 * @property string $rodzaj
 * @property string $label_position
 * @property int $cords_n_stopnie
 * @property int $cords_n_minuty
 * @property int $cords_e_stopnie
 * @property int $cords_e_minuty
 */
class Miasto extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
