<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Kategorium Entity
 *
 * @property int $id
 * @property int $parent_id
 * @property int $ukryta
 * @property int $kolejnosc
 * @property \Cake\I18n\FrozenDate $data
 * @property int $wyswietl_miniatury
 * @property bool $konfekcja
 * @property string $zdjecie
 * @property string $nazwa
 * @property string $sciezka
 * @property string $naglowek
 * @property string $seo_tytul
 * @property string $seo_opis
 * @property string $seo_slowa
 * @property string $tytul_strony
 * @property bool $menu_top
 * @property string $menu_tekst_naglowek
 * @property string $menu_tekst
 * @property string $tagi
 * @property int $lft
 * @property int $rght
 * @property string $nazwa_producent
 * @property int $google_kategoria_id
 * @property bool $google_merchant
 * @property string $nazwa_presta
 *
 * @property \App\Model\Entity\ParentKategorium $parent_kategorium
 * @property \App\Model\Entity\GoogleKategorie $google_kategorie
 * @property \App\Model\Entity\ChildKategorium[] $child_kategoria
 * @property \App\Model\Entity\Atrybut[] $atrybut
 * @property \App\Model\Entity\Baner[] $baner
 * @property \App\Model\Entity\I18n[] $i18n
 * @property \App\Model\Entity\Sezon[] $sezon
 * @property \App\Model\Entity\Towar[] $towar
 */
class Kategorium extends Entity
{
use \Cake\ORM\Behavior\Translate\TranslateTrait;
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
        '_translations'=>true
    ];
}
