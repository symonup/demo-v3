<?php

namespace App\Model\Behavior;

use Translation\Translation;
use Cake\ORM\Behavior\TranslateBehavior AS CakeTranslateBehavior;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use ArrayObject;

class TranslateBehavior extends CakeTranslateBehavior {

    public $Translation;

    public function initialize(array $config) {
        $this->Translation = new Translation();
        parent::initialize($config);
    }

    public function get($key, $param = []) {
        $result = $this->Translation->get($key, $param);
        $this->Translation->saveFiles();
        return $result;
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options) {
        if (!empty($data['_translations'])) {
            foreach ($data['_translations'] as $lang => $dataLang) {
                foreach ($dataLang as $field => $value) {
                    if (empty($data[$field])) {
                        $data[$field] = $value;
                    }
                }
            }
        }
    }

//    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options) {
//        $translation = $entity->_translations;
//        unset($entity['_translations']);
//        if (!empty($translation)) {
//            foreach ($translation as $lang => $data) {
//                if (is_array($data)) {
//                    $entity->translation($lang)->set($data);
//                }
//            }
//        }
//        parent::beforeSave($event, $entity, $options);
//    }

}
