<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * KartaPodarunkowa Model
 *
 * @property \App\Model\Table\KartaPodarunkowaKodTable|\Cake\ORM\Association\HasMany $KartaPodarunkowaKod
 *
 * @method \App\Model\Entity\KartaPodarunkowa get($primaryKey, $options = [])
 * @method \App\Model\Entity\KartaPodarunkowa newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\KartaPodarunkowa[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\KartaPodarunkowa|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\KartaPodarunkowa patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\KartaPodarunkowa[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\KartaPodarunkowa findOrCreate($search, callable $callback = null, $options = [])
 */
class KartaPodarunkowaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('karta_podarunkowa');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('KartaPodarunkowaKod', [
            'foreignKey' => 'karta_podarunkowa_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->numeric('wartosc')
            ->requirePresence('wartosc', 'create')
            ->notEmpty('wartosc');

        $validator
            ->numeric('cena')
            ->requirePresence('cena', 'create')
            ->notEmpty('cena');

        $validator
            ->dateTime('data_dodania')
            ->requirePresence('data_dodania', 'create')
            ->notEmpty('data_dodania');

        $validator
            ->boolean('aktywna')
            ->requirePresence('aktywna', 'create')
            ->notEmpty('aktywna');
        
        $validator
                ->allowEmpty('zdjecie');

        return $validator;
    }
}
