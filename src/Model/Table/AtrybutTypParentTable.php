<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AtrybutTypParent Model
 *
 * @property \App\Model\Table\AtrybutTypTable|\Cake\ORM\Association\HasMany $AtrybutTyp
 * @property \App\Model\Table\I18nTable|\Cake\ORM\Association\BelongsToMany $I18n
 *
 * @method \App\Model\Entity\AtrybutTypParent get($primaryKey, $options = [])
 * @method \App\Model\Entity\AtrybutTypParent newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AtrybutTypParent[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AtrybutTypParent|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AtrybutTypParent patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AtrybutTypParent[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AtrybutTypParent findOrCreate($search, callable $callback = null, $options = [])
 */
class AtrybutTypParentTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('atrybut_typ_parent');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('AtrybutTyp', [
            'foreignKey' => 'atrybut_typ_parent_id'
        ]);
        $this->addBehavior('Translate', ['fields' => ['nazwa','pod_tytul'], 'translationTable' => 'AtrybutTypParentI18n']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->allowEmpty('pod_tytul');

        return $validator;
    }
}
