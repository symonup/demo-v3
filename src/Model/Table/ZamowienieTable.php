<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Zamowienie Model
 *
 * @property \App\Model\Table\UzytkownikTable|\Cake\ORM\Association\BelongsTo $Uzytkownik
 * @property \App\Model\Table\WalutaTable|\Cake\ORM\Association\BelongsTo $Waluta
 * @property |\Cake\ORM\Association\HasMany $AllegroZamowienie
 * @property \App\Model\Table\FakturaPlikiTable|\Cake\ORM\Association\HasMany $FakturaPliki
 * @property \App\Model\Table\PlatnoscTable|\Cake\ORM\Association\HasMany $Platnosc
 * @property |\Cake\ORM\Association\BelongsToMany $Towar
 *
 * @method \App\Model\Entity\Zamowienie get($primaryKey, $options = [])
 * @method \App\Model\Entity\Zamowienie newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Zamowienie[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Zamowienie|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Zamowienie patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Zamowienie[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Zamowienie findOrCreate($search, callable $callback = null, $options = [])
 */
class ZamowienieTable extends Table {

    public $Translation;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('zamowienie');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Uzytkownik', [
            'foreignKey' => 'uzytkownik_id'
        ]);
        $this->belongsTo('Administrator', [
            'foreignKey' => 'administrator_id'
        ]);
        $this->belongsTo('Waluta', [
            'foreignKey' => 'waluta_id'
        ]);
        $this->hasMany('FakturaPliki', [
            'foreignKey' => 'zamowienie_id'
        ]);
        $this->hasMany('Platnosc', [
            'foreignKey' => 'zamowienie_id'
        ]);
        $this->hasMany('ZamowienieTowar', [
            'foreignKey' => 'zamowienie_id'
        ]);
        $this->hasMany('KodyRabatowe', [
            'foreignKey' => 'zamowienie_id'
        ]);
        $this->hasMany('ZamowienieDhl', [
            'foreignKey' => 'zamowienie_id'
        ]);
        $this->belongsTo('PunktyOdbioru', [
            'foreignKey' => 'punkty_odbioru_id'
        ]);
        $this->belongsToMany('Towar', [
            'foreignKey' => 'zamowienie_id',
            'targetForeignKey' => 'towar_id',
            'joinTable' => 'zamowienie_towar'
        ]);
        $this->belongsTo('RodzajPlatnosci',[
            'foreignKey' => 'rodzaj_platnosci_id'
        ]);
        $this->belongsTo('Wysylka',[
            'foreignKey' => 'wysylka_id'
        ]);
        $this->hasMany('KartaPodarunkowaKod', [
            'foreignKey' => 'zamowienie_id'
        ]);
        $this->hasMany('FakturaSubiekt', [
            'className'=>'Faktura',
            'foreignKey' => 'zamowienie_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');
        $validator
                ->allowEmpty('uzytkownik_id');
        $validator
                ->allowEmpty('numer');
        $validator
                ->allowEmpty('waluta_id');
        $validator
                ->allowEmpty('waluta_symbol');
        $validator
                ->allowEmpty('lng');
        $validator
                ->requirePresence('imie', 'create')
                ->notEmpty('imie');
        $validator
                ->requirePresence('nazwisko', 'create')
                ->notEmpty('nazwisko');
        $validator
                ->email('email')
                ->requirePresence('email', 'create')
                ->notEmpty('email');
        $validator
                ->allowEmpty('firma');
        $validator
                ->allowEmpty('nip');
        $validator
                ->allowEmpty('adres_faktury');
        $validator
                ->allowEmpty('adres_wysylki');
        $validator
                ->allowEmpty('rodzaj_platnosci_id');
        $validator
                ->allowEmpty('wysylka_id');
        $validator
                ->allowEmpty('platnosc_wysylka');
        $validator
                ->allowEmpty('wysylka_ulica');

        $validator
                ->allowEmpty('wysylka_nr_domu');

        $validator
                ->allowEmpty('wysylka_nr_lokalu');

        $validator
                ->allowEmpty('wysylka_kod');

        $validator
                ->allowEmpty('wysylka_miasto');

        $validator
                ->allowEmpty('wysylka_imie');

        $validator
                ->allowEmpty('wysylka_nazwisko');

        $validator
                ->allowEmpty('wysylka_firma');

        $validator
                ->allowEmpty('wysylka_nip');

        $validator
                ->allowEmpty('wysylka_telefon');

        $validator
                ->allowEmpty('wysylka_email');

        $validator
                ->allowEmpty('wysylka_kraj');

        $validator
                ->allowEmpty('faktura_imie');

        $validator
                ->allowEmpty('faktura_nazwisko');

        $validator
                ->allowEmpty('faktura_firma');

        $validator
                ->allowEmpty('faktura_nip');

        $validator
                ->allowEmpty('faktura_ulica');

        $validator
                ->allowEmpty('faktura_nr_domu');

        $validator
                ->allowEmpty('faktura_nr_lokalu');

        $validator
                ->allowEmpty('faktura_kod');

        $validator
                ->allowEmpty('faktura_miasto');

        $validator
                ->allowEmpty('faktura_kraj');

        $validator
                ->allowEmpty('telefon');

        $validator
                ->numeric('wartosc_produktow')
                ->requirePresence('wartosc_produktow', 'create')
                ->notEmpty('wartosc_produktow');
        $validator
                ->numeric('wartosc_produktow_netto')
                ->allowEmpty('wartosc_produktow_netto');
        $validator
                ->allowEmpty('wysylka_koszt');
        $validator
                ->numeric('wartosc_vat')
                ->allowEmpty('wartosc_vat');

        $validator
                ->numeric('wartosc_razem')
                ->requirePresence('wartosc_razem', 'create')
                ->notEmpty('wartosc_razem');

        $validator
                ->numeric('wartosc_razem_netto')
                ->allowEmpty('wartosc_razem_netto');

        $validator
                ->allowEmpty('uwagi');

        $validator
                ->allowEmpty('notatki');

        $validator
                ->requirePresence('status', 'create')
                ->notEmpty('status');

        $validator
                ->dateTime('data')
                ->requirePresence('data', 'create')
                ->notEmpty('data');

        $validator
                ->dateTime('data_realizacji')
                ->allowEmpty('data_realizacji');

        $validator
                ->allowEmpty('rabat');
        $validator
                ->allowEmpty('status_log');
        $validator
                ->allowEmpty('nr_fv');
        $validator
                ->allowEmpty('notatka_status');
        $validator
                ->allowEmpty('token');
        $validator
                ->date('termin_wysylki')
                ->allowEmpty('termin_wysylki');
        $validator
                ->allowEmpty('nr_listu_przewozowego');
        $validator
                ->allowEmpty('kurier_typ');
        $validator
                ->allowEmpty('rabat_zestaw');
        $validator
                ->allowEmpty('kod_rabatowy');
        $validator
                ->allowEmpty('kod_rabatowy_typ');
        $validator
                ->allowEmpty('kod_rabatowy_wartosc');
        $validator
                ->boolean('telefoniczne')
                ->allowEmpty('telefoniczne');
        $validator
                ->allowEmpty('bonus_suma');
        $validator
                ->allowEmpty('paczkomat');
        $validator
                ->allowEmpty('paczkomat_info');
        $validator
                ->boolean('faktura')
                ->allowEmpty('faktura');
        $validator
                ->allowEmpty('bonus_przyznany');
        $validator
                ->boolean('preorder')
                ->allowEmpty('preorder');
        $validator
                ->date('data_premiery')
                ->allowEmpty('data_premiery');
        $validator
                ->allowEmpty('platforma');
        $validator
                ->allowEmpty('hot_deal');
        
        $validator
                ->allowEmpty('punkty_odbioru_id');
        $validator
                ->allowEmpty('punkty_odbioru_info');
        $validator
                ->boolean('regulamin')
                ->allowEmpty('regulamin');
        $validator
                ->boolean('zgoda')
                ->allowEmpty('zgoda');
        $validator
                ->boolean('zgoda2')
                ->allowEmpty('zgoda2');

        $validator
                ->allowEmpty('wysylka_long_koszt');
        
        return $validator;
    }

    public function validationRegulamin($validator) {
        $validator
                ->requirePresence('zgoda', 'create', t('validations.consentIsRequired'))
                ->notEmpty('zgoda', t('validations.consentIsRequired'));
        $validator
                ->requirePresence('regulamin', 'create', t('validations.regulationsIsRequired'))
                ->notEmpty('regulamin', t('validations.regulationsIsRequired'))
        ;
        return $validator;
    }

    public function validationOrder($validator) {
        $this->validationDefault($validator);
//        $this->validationRegulamin($validator);
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['uzytkownik_id'], 'Uzytkownik'));
        $rules->add($rules->existsIn(['waluta_id'], 'Waluta'));

        return $rules;
    }

    public function getNumber($date) {
        if (empty($date)) {
            return false;
        }
        $daysInMonth = date('t', strtotime($date));
        $from = date('Y-m-01 00:00:00', strtotime($date));
        $to = date('Y-m-' . $daysInMonth . ' 23:59:59', strtotime($date));
        $count = $this->find('all', ['conditions' => ['Zamowienie.data >=' => $from, 'Zamowienie.data <=' => $to]])->count();
        $addZero = '';
        if (strlen($count) < 5) {
            for ($i = 5; $i > strlen($count); $i--) {
                $addZero .= '0';
            }
        }
        return date('ym', strtotime($date)) . $addZero . $count;
    }

    private function newOrderNumber($len=8){
        $txt = new \App\View\Helper\TxtHelper(new \Cake\View\View());
        $orderNumber=$txt->randomOrderNumber($len);
        if($this->find('all')->where(['Zamowienie.numer'=>$orderNumber])->count()>0){
            return $this->newOrderNumber($len);
        } else {
            return $orderNumber;
        }
    }
}
