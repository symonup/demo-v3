<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Opinia Model
 *
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\BelongsToMany $Towar
 *
 * @method \App\Model\Entity\Opinium get($primaryKey, $options = [])
 * @method \App\Model\Entity\Opinium newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Opinium[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Opinium|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Opinium patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Opinium[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Opinium findOrCreate($search, callable $callback = null, $options = [])
 */
class OpiniaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('opinia');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('tresc', 'create')
            ->notEmpty('tresc');

        $validator
            ->requirePresence('podpis', 'create')
            ->notEmpty('podpis');

        $validator
            ->boolean('ukryta')
            ->allowEmpty('ukryta');

        $validator
            ->integer('kolejnosc')
            ->allowEmpty('kolejnosc');

        return $validator;
    }
}
