<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Artykul Model
 *
 * @property \App\Model\Table\I18nTable|\Cake\ORM\Association\BelongsToMany $I18n
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\BelongsToMany $Towar
 *
 * @method \App\Model\Entity\Artykul get($primaryKey, $options = [])
 * @method \App\Model\Entity\Artykul newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Artykul[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Artykul|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Artykul patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Artykul[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Artykul findOrCreate($search, callable $callback = null, $options = [])
 */
class ArtykulTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('artykul');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsToMany('Towar', [
            'foreignKey' => 'artykul_id',
            'targetForeignKey' => 'towar_id',
            'joinTable' => 'towar_artykul'
        ]);
        $this->addBehavior('Translate', ['fields' => ['tytul','pod_tytul','tresc','seo_tytul','seo_slowa','seo_opis'], 'translationTable' => 'ArtykulI18n']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('tytul', 'create')
            ->notEmpty('tytul');

        $validator
            ->allowEmpty('pod_tytul');

        $validator
            ->requirePresence('tresc', 'create')
            ->notEmpty('tresc');

        $validator
            ->requirePresence('zdjecie_1', 'create')
            ->notEmpty('zdjecie_1');

        $validator
            ->allowEmpty('zdjecie_2');

        $validator
            ->dateTime('data')
            ->requirePresence('data', 'create')
            ->notEmpty('data');

        $validator
            ->boolean('ukryty')
            ->requirePresence('ukryty', 'create')
            ->notEmpty('ukryty');

        $validator
            ->integer('kolejnosc')
            ->requirePresence('kolejnosc', 'create')
            ->notEmpty('kolejnosc');

        $validator
            ->allowEmpty('seo_slowa');

        $validator
            ->allowEmpty('seo_tytul');

        $validator
            ->allowEmpty('seo_opis');

        return $validator;
    }
}
