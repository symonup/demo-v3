<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * KoszykTowar Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Koszyks
 * @property \Cake\ORM\Association\BelongsTo $Towars
 *
 * @method \App\Model\Entity\KoszykTowar get($primaryKey, $options = [])
 * @method \App\Model\Entity\KoszykTowar newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\KoszykTowar[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\KoszykTowar|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\KoszykTowar patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\KoszykTowar[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\KoszykTowar findOrCreate($search, callable $callback = null)
 */
class KoszykTowarTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('koszyk_towar');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Koszyk', [
            'foreignKey' => 'koszyk_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Towar', [
            'foreignKey' => 'towar_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('koszyk_key');

        $validator
            ->integer('dodany')
            ->allowEmpty('dodany');

        $validator
            ->integer('usuniety')
            ->allowEmpty('usuniety');

        $validator
            ->dateTime('data_dodania')
            ->allowEmpty('data_dodania');

        $validator
            ->dateTime('data_usuniecia')
            ->allowEmpty('data_usuniecia');

        $validator
            ->integer('ilosc')
            ->allowEmpty('ilosc');

        $validator
            ->numeric('cena')
            ->allowEmpty('cena');

        $validator
            ->allowEmpty('notatki');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['koszyk_id'], 'Koszyk'));
        $rules->add($rules->existsIn(['towar_id'], 'Towar'));

        return $rules;
    }
}
