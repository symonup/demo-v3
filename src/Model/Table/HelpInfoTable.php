<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * HelpInfo Model
 *
 * @method \App\Model\Entity\HelpInfo get($primaryKey, $options = [])
 * @method \App\Model\Entity\HelpInfo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\HelpInfo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\HelpInfo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\HelpInfo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\HelpInfo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\HelpInfo findOrCreate($search, callable $callback = null, $options = [])
 */
class HelpInfoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('help_info');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->requirePresence('field', 'create')
            ->notEmpty('field');

        $validator
            ->allowEmpty('info');

        return $validator;
    }
}
