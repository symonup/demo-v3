<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Przeznaczenie Model
 *
 * @property \App\Model\Table\IkonaTable|\Cake\ORM\Association\BelongsTo $Ikona
 * @property \App\Model\Table\I18nTable|\Cake\ORM\Association\BelongsToMany $I18n
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\BelongsToMany $Towar
 *
 * @method \App\Model\Entity\Przeznaczenie get($primaryKey, $options = [])
 * @method \App\Model\Entity\Przeznaczenie newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Przeznaczenie[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Przeznaczenie|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Przeznaczenie patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Przeznaczenie[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Przeznaczenie findOrCreate($search, callable $callback = null, $options = [])
 */
class PrzeznaczenieTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('przeznaczenie');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Ikona', [
            'foreignKey' => 'ikona_id'
        ]);
        $this->belongsToMany('Towar', [
            'foreignKey' => 'przeznaczenie_id',
            'targetForeignKey' => 'towar_id',
            'joinTable' => 'towar_przeznaczenie'
        ]);
        $this->addBehavior('Translate', ['fields' => ['nazwa'], 'translationTable' => 'PrzeznaczenieI18n']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->integer('kolejnosc')
            ->requirePresence('kolejnosc', 'create')
            ->notEmpty('kolejnosc');

        $validator
            ->integer('ukryte')
            ->allowEmpty('ukryte');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['ikona_id'], 'Ikona'));

        return $rules;
    }
}
