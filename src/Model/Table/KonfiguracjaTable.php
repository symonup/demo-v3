<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Core\Configure;
/**
 * Konfiguracja Model
 *
 * @method \App\Model\Entity\Konfiguracja get($primaryKey, $options = [])
 * @method \App\Model\Entity\Konfiguracja newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Konfiguracja[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Konfiguracja|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Konfiguracja patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Konfiguracja[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Konfiguracja findOrCreate($search, callable $callback = null, $options = [])
 */
class KonfiguracjaTable extends Table
{
public $pobierz=false;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('konfiguracja');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Translate', ['fields' => ['wartosc'], 'translationTable' => 'KonfiguracjaI18n']);
        $itemToAdd = [
      'wysylka.krajPodstawowy' => ['typ' => 'wysylka', 'nazwa' => 'krajPodstawowy', 'wartosc' => 'PL', 'rodzaj' => 'select','html'=>0, 'label' => 'Podstawowy kraj wysyłki'],
      'wysylka.krajKosztDodatkowy' => ['typ' => 'wysylka', 'nazwa' => 'krajKosztDodatkowy', 'wartosc' => '50', 'rodzaj' => 'number','html'=>0, 'label' => 'Koszt dodatkowy dla innych krajów niż podstawowy'],
      'wysylka.limit' => ['typ' => 'wysylka', 'nazwa' => 'limit', 'wartosc' => '500', 'rodzaj' => 'number','html'=>0, 'label' => 'Minimalna wartość zamówienia dla darmowej wysyłki'],
      'wysylka.stawkaVat' => ['typ' => 'wysylka', 'nazwa' => 'stawkaVat', 'wartosc' => '23', 'rodzaj' => 'number','html'=>0, 'label' => 'Stawka VAT kosztów wysyłki'],
      'seoHome.title' => ['typ' => 'seoHome', 'nazwa' => 'title', 'wartosc' => '', 'rodzaj' => 'text','html'=>0, 'label' => 'Tytuł strony głównej'],
      'seoHome.keywords' => ['typ' => 'seoHome', 'nazwa' => 'keywords', 'wartosc' => '', 'rodzaj' => 'text','html'=>0, 'label' => 'Słowa kluczowe strony głównej'],
      'seoHome.description' => ['typ' => 'seoHome', 'nazwa' => 'description', 'wartosc' => '', 'rodzaj' => 'text','html'=>0, 'label' => 'Opis strony głównej'],
      'seoLogin.title' => ['typ' => 'seoLogin', 'nazwa' => 'title', 'wartosc' => '', 'rodzaj' => 'text','html'=>0, 'label' => 'Tytuł strony logowania i rejestracji'],
      'seoLogin.keywords' => ['typ' => 'seoLogin', 'nazwa' => 'keywords', 'wartosc' => '', 'rodzaj' => 'text','html'=>0, 'label' => 'Słowa kluczowe strony logowania i rejestracji'],
      'seoLogin.description' => ['typ' => 'seoLogin', 'nazwa' => 'description', 'wartosc' => '', 'rodzaj' => 'text','html'=>0, 'label' => 'Opis strony logowania i rejestracji'],
      'seoPassword.title' => ['typ' => 'seoPassword', 'nazwa' => 'title', 'wartosc' => '', 'rodzaj' => 'text','html'=>0, 'label' => 'Tytuł strony restartu hasła'],
      'seoPassword.keywords' => ['typ' => 'seoPassword', 'nazwa' => 'keywords', 'wartosc' => '', 'rodzaj' => 'text','html'=>0, 'label' => 'Słowa kluczowe strony restartu hasła'],
      'seoPassword.description' => ['typ' => 'seoPassword', 'nazwa' => 'description', 'wartosc' => '', 'rodzaj' => 'text','html'=>0, 'label' => 'Opis strony restartu hasła'],
      'dane.stopka_dane' => ['typ' => 'dane', 'nazwa' => 'stopka_dane', 'wartosc' => '', 'rodzaj' => 'textarea','html'=>1, 'label' => 'Dane firmy w stopce'],
      'dane.telefon2' => ['typ' => 'dane', 'nazwa' => 'telefon2', 'wartosc' => '+48 343 232 234', 'rodzaj' => 'text','html'=>0, 'label' => 'Drugi telefon'],
      'dane.email' => ['typ' => 'dane', 'nazwa' => 'email', 'wartosc' => 'biuro@example.pl', 'rodzaj' => 'text','html'=>0, 'label' => 'Email kontaktowy'],
      'links.homePromo1' => ['typ' => 'links', 'nazwa' => 'homePromo1', 'wartosc' => '#', 'rodzaj' => 'text','html'=>0, 'label' => 'Link na stronie głównej w menu obok banera - pozycja 1'],
      'links.homePromo2' => ['typ' => 'links', 'nazwa' => 'homePromo2', 'wartosc' => '#', 'rodzaj' => 'text','html'=>0, 'label' => 'Link na stronie głównej w menu obok banera - pozycja 2'],
      'links.homePromo3' => ['typ' => 'links', 'nazwa' => 'homePromo3', 'wartosc' => '#', 'rodzaj' => 'text','html'=>0, 'label' => 'Link na stronie głównej w menu obok banera - pozycja 3'],
      'gratis.wartosc_min' => ['typ' => 'gratis', 'nazwa' => 'wartosc_min', 'wartosc' => '100', 'rodzaj' => 'number','html'=>0, 'label' => 'Minimalna wartość zamówienia aby otrzymać gratis'],
       'facebook.appId'=>['typ' => 'facebook', 'nazwa' => 'appId', 'wartosc' => '2168791899846908', 'rodzaj' => 'text','html'=>0, 'label' => 'ID aplikacji facebook'],
       'subiekt.miesjce_wystawienia'=>['typ' => 'subiekt', 'nazwa' => 'miesjce_wystawienia', 'wartosc' => 'Wrocław', 'rodzaj' => 'text','html'=>0, 'label' => 'Miejsce wystawienia faktur'],
       'subiekt.id_programu'=>['typ' => 'subiekt', 'nazwa' => 'id_programu', 'wartosc' => 'GT', 'rodzaj' => 'text','html'=>0, 'label' => 'Identyfikator programu'],
       'subiekt.id_nadawcy'=>['typ' => 'subiekt', 'nazwa' => 'id_nadawcy', 'wartosc' => 'Subiekt GT', 'rodzaj' => 'text','html'=>0, 'label' => 'Identyfikator nadawcy'],
       'subiekt.nazwa_krotka'=>['typ' => 'subiekt', 'nazwa' => 'nazwa_krotka', 'wartosc' => 'Demo', 'rodzaj' => 'text','html'=>0, 'label' => 'Nazwa skrócona firmy'],
       'subiekt.nazwa_pelna'=>['typ' => 'subiekt', 'nazwa' => 'nazwa_pelna', 'wartosc' => 'Firma przykładowa systemu InsERT GT', 'rodzaj' => 'text','html'=>0, 'label' => 'Pełna nazwa firmy'],
       'subiekt.miasto'=>['typ' => 'subiekt', 'nazwa' => 'miasto', 'wartosc' => 'Wrocław', 'rodzaj' => 'text','html'=>0, 'label' => 'Miasto'],
       'subiekt.kod_pocztowy'=>['typ' => 'subiekt', 'nazwa' => 'kod_pocztowy', 'wartosc' => '54-445', 'rodzaj' => 'text','html'=>0, 'label' => 'Kod pocztowy'],
       'subiekt.adres'=>['typ' => 'subiekt', 'nazwa' => 'adres', 'wartosc' => 'Bławatkowa 25/3', 'rodzaj' => 'text','html'=>0, 'label' => 'Adres'],
       'subiekt.nip'=>['typ' => 'subiekt', 'nazwa' => 'nip', 'wartosc' => '111-111-11-11', 'rodzaj' => 'text','html'=>0, 'label' => 'NIP'],
       'subiekt.nip_ue'=>['typ' => 'subiekt', 'nazwa' => 'nip_ue', 'wartosc' => '0', 'rodzaj' => 'text','html'=>0, 'label' => 'NIP UE'],
       'subiekt.vat_ue'=>['typ' => 'subiekt', 'nazwa' => 'vat_ue', 'wartosc' => '0', 'rodzaj' => 'checkbox','html'=>0, 'label' => 'Zarejestrowany jako VAT UE'],
       'subiekt.podtytul_fv'=>['typ' => 'subiekt', 'nazwa' => 'podtytul_fv', 'wartosc' => 'Zamówienie nr: {numer}', 'rodzaj' => 'text','html'=>0, 'label' => 'Podtytuł zamówienia do subiekta zmienna {numer}'],
       'faktura.platnoscGotowka'=>['typ' => 'faktura', 'nazwa' => 'platnoscGotowka', 'wartosc' => 'Gotówka', 'rodzaj' => 'text','html'=>0, 'label' => 'Nazwa płatności gotówkowej na fakturze'],
       'faktura.platnoscKarta'=>['typ' => 'faktura', 'nazwa' => 'platnoscKarta', 'wartosc' => 'Karta płatnicza', 'rodzaj' => 'text','html'=>0, 'label' => 'Nazwa płatności kartą na fakturze'],
       'faktura.platnoscPrzelew'=>['typ' => 'faktura', 'nazwa' => 'platnoscPrzelew', 'wartosc' => 'Przelew', 'rodzaj' => 'text','html'=>0, 'label' => 'Nazwa płatności przelewem na fakturze'],
            ];
        if (!$this->pobierz) {
            foreach ($this->find('all') as $item) {
                Configure::write($item['typ'] . '.' . $item['nazwa'], $item['wartosc']);
                if (key_exists($item['typ'] . '.' . $item['nazwa'], $itemToAdd))
                    unset($itemToAdd[$item['typ'] . '.' . $item['nazwa']]);
            }
            if (!empty($itemToAdd)) {
                foreach ($itemToAdd as $newCfgItem) {
                    if ($this->save($this->newEntity($newCfgItem))) {
                        Configure::write($newCfgItem['typ'] . '.' . $newCfgItem['nazwa'], $newCfgItem['wartosc']);
                    }
                }
            }
            $this->pobierz = true;
        }
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->allowEmpty('wartosc');

        $validator
            ->requirePresence('typ', 'create')
            ->notEmpty('typ');

        $validator
            ->requirePresence('rodzaj', 'create')
            ->notEmpty('rodzaj');

        $validator
            ->boolean('html')
            ->allowEmpty('html');

        $validator
            ->allowEmpty('label');

        $validator
            ->allowEmpty('options');

        return $validator;
    }
    
    public function getValue($name) {
        return Configure::read($name);
    }
    public function getDomain()
    {
        return $_SERVER['SERVER_NAME'];
        
    }
    public function getKonfiguracjaArray()
    {
        $cfgs=$this->find('all');
        $returnArray=array();
        foreach($cfgs as $cfg)
        {
            $returnArray[$cfg['typ']][]=$cfg;
        }
        return $returnArray;
    }
}
