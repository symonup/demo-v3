<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Routing Model
 *
 * @method \App\Model\Entity\Routing get($primaryKey, $options = [])
 * @method \App\Model\Entity\Routing newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Routing[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Routing|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Routing patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Routing[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Routing findOrCreate($search, callable $callback = null, $options = [])
 */
class RoutingTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('routing');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('controller', 'create')
            ->notEmpty('controller');

        $validator
            ->requirePresence('action', 'create')
            ->notEmpty('action');

        $validator
            ->allowEmpty('pass');

        $validator
            ->requirePresence('domena', 'create')
            ->notEmpty('domena');

        $validator
            ->allowEmpty('maska');

        return $validator;
    }
}
