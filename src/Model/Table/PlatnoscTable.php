<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Platnosc Model
 *
 * @property \App\Model\Table\UzytkownikTable|\Cake\ORM\Association\BelongsTo $Uzytkownik
 * @property \App\Model\Table\ZamowienieTable|\Cake\ORM\Association\BelongsTo $Zamowienie
 * @property \App\Model\Table\RodzajPlatnosciTable|\Cake\ORM\Association\BelongsTo $RodzajPlatnosci
 * @property \App\Model\Table\WalutaTable|\Cake\ORM\Association\BelongsTo $Waluta
 * @property \App\Model\Table\WysylkaTable|\Cake\ORM\Association\BelongsToMany $Wysylka
 *
 * @method \App\Model\Entity\Platnosc get($primaryKey, $options = [])
 * @method \App\Model\Entity\Platnosc newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Platnosc[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Platnosc|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Platnosc patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Platnosc[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Platnosc findOrCreate($search, callable $callback = null, $options = [])
 */
class PlatnoscTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('platnosc');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Uzytkownik', [
            'foreignKey' => 'uzytkownik_id'
        ]);
        $this->belongsTo('Zamowienie', [
            'foreignKey' => 'zamowienie_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('RodzajPlatnosci', [
            'foreignKey' => 'rodzaj_platnosci_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Waluta', [
            'foreignKey' => 'waluta_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Wysylka', [
            'foreignKey' => 'platnosc_id',
            'targetForeignKey' => 'wysylka_id',
            'joinTable' => 'wysylka_platnosc'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->numeric('kwota')
            ->requirePresence('kwota', 'create')
            ->notEmpty('kwota');

        $validator
            ->allowEmpty('opis');

        $validator
            ->allowEmpty('opis_lng');

        $validator
            ->requirePresence('token', 'create')
            ->notEmpty('token');

        $validator
            ->requirePresence('token2', 'create')
            ->notEmpty('token2');

        $validator
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->integer('status2')
            ->allowEmpty('status2');

        $validator
            ->allowEmpty('blad');

        $validator
            ->dateTime('data')
            ->requirePresence('data', 'create')
            ->notEmpty('data');

        $validator
            ->dateTime('data_rozpoczecia')
            ->allowEmpty('data_rozpoczecia');

        $validator
            ->boolean('doplata')
            ->allowEmpty('doplata');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['uzytkownik_id'], 'Uzytkownik'));
        $rules->add($rules->existsIn(['zamowienie_id'], 'Zamowienie'));
        $rules->add($rules->existsIn(['rodzaj_platnosci_id'], 'RodzajPlatnosci'));
        $rules->add($rules->existsIn(['waluta_id'], 'Waluta'));

        return $rules;
    }
}
