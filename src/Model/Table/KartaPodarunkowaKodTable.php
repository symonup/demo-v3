<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * KartaPodarunkowaKod Model
 *
 * @property \App\Model\Table\KartaPodarunkowaTable|\Cake\ORM\Association\BelongsTo $KartaPodarunkowa
 * @property \App\Model\Table\ZamowienieTable|\Cake\ORM\Association\BelongsTo $Zamowienie
 * @property \App\Model\Table\UzytkownikTable|\Cake\ORM\Association\BelongsTo $Uzytkownik
 *
 * @method \App\Model\Entity\KartaPodarunkowaKod get($primaryKey, $options = [])
 * @method \App\Model\Entity\KartaPodarunkowaKod newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\KartaPodarunkowaKod[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\KartaPodarunkowaKod|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\KartaPodarunkowaKod patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\KartaPodarunkowaKod[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\KartaPodarunkowaKod findOrCreate($search, callable $callback = null, $options = [])
 */
class KartaPodarunkowaKodTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('karta_podarunkowa_kod');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('KartaPodarunkowa', [
            'foreignKey' => 'karta_podarunkowa_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Zamowienie', [
            'foreignKey' => 'zamowienie_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Uzytkownik', [
            'foreignKey' => 'uzytkownik_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('kod', 'create')
            ->notEmpty('kod')
            ->add('kod', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);
        $validator
                ->allowEmpty('pin');
        $validator
            ->numeric('wartosc')
            ->requirePresence('wartosc', 'create')
            ->notEmpty('wartosc');

        $validator
            ->dateTime('data_dodania')
            ->requirePresence('data_dodania', 'create')
            ->notEmpty('data_dodania');

        $validator
            ->dateTime('data_uzycia')
            ->allowEmpty('data_uzycia');

        $validator
            ->dateTime('data_waznosci')
            ->allowEmpty('data_waznosci');

        $validator
            ->boolean('aktywny')
            ->allowEmpty('aktywny');

        $validator
            ->boolean('uzyty')
            ->allowEmpty('uzyty');

        $validator
            ->boolean('wyslany')
            ->allowEmpty('wyslany');

        $validator
            ->dateTime('data_wyslania')
            ->allowEmpty('data_wyslania');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['kod']));
        $rules->add($rules->existsIn(['karta_podarunkowa_id'], 'KartaPodarunkowa'));
        $rules->add($rules->existsIn(['zamowienie_id'], 'Zamowienie'));
        $rules->add($rules->existsIn(['uzytkownik_id'], 'Uzytkownik'));

        return $rules;
    }
}
