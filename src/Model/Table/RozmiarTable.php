<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Rozmiar Model
 *
 * @property \App\Model\Table\TowarWariantTable|\Cake\ORM\Association\HasMany $TowarWariant
 *
 * @method \App\Model\Entity\Rozmiar get($primaryKey, $options = [])
 * @method \App\Model\Entity\Rozmiar newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Rozmiar[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Rozmiar|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Rozmiar patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Rozmiar[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Rozmiar findOrCreate($search, callable $callback = null, $options = [])
 */
class RozmiarTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('rozmiar');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('TowarWariant', [
            'foreignKey' => 'rozmiar_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->integer('kolejnosc')
            ->allowEmpty('kolejnosc');

        return $validator;
    }
}
