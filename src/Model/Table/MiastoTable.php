<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Miasto Model
 *
 * @method \App\Model\Entity\Miasto get($primaryKey, $options = [])
 * @method \App\Model\Entity\Miasto newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Miasto[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Miasto|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Miasto patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Miasto[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Miasto findOrCreate($search, callable $callback = null, $options = [])
 */
class MiastoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('miasto');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->boolean('ukryte')
            ->allowEmpty('ukryte');

        $validator
            ->numeric('szerokosc')
            ->requirePresence('szerokosc', 'create')
            ->notEmpty('szerokosc');

        $validator
            ->numeric('dlugosc')
            ->requirePresence('dlugosc', 'create')
            ->notEmpty('dlugosc');

        $validator
            ->allowEmpty('rodzaj');

        $validator
            ->allowEmpty('label_position');

        $validator
            ->integer('cords_n_stopnie')
            ->requirePresence('cords_n_stopnie', 'create')
            ->notEmpty('cords_n_stopnie');

        $validator
            ->integer('cords_n_minuty')
            ->requirePresence('cords_n_minuty', 'create')
            ->notEmpty('cords_n_minuty');

        $validator
            ->integer('cords_e_stopnie')
            ->requirePresence('cords_e_stopnie', 'create')
            ->notEmpty('cords_e_stopnie');

        $validator
            ->integer('cords_e_minuty')
            ->requirePresence('cords_e_minuty', 'create')
            ->notEmpty('cords_e_minuty');

        return $validator;
    }
}
