<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Atrybut Model
 *
 * @property \App\Model\Table\AtrybutTypsTable|\Cake\ORM\Association\BelongsTo $AtrybutTyps
 * @property \App\Model\Table\KategoriasTable|\Cake\ORM\Association\BelongsTo $Kategorias
 * @property \App\Model\Table\AtrybutPodrzedneTable|\Cake\ORM\Association\HasMany $AtrybutPodrzedne
 * @property \App\Model\Table\I18nTable|\Cake\ORM\Association\BelongsToMany $I18n
 * @property \App\Model\Table\KategoriaTable|\Cake\ORM\Association\BelongsToMany $Kategoria
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\BelongsToMany $Towar
 *
 * @method \App\Model\Entity\Atrybut get($primaryKey, $options = [])
 * @method \App\Model\Entity\Atrybut newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Atrybut[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Atrybut|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Atrybut patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Atrybut[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Atrybut findOrCreate($search, callable $callback = null, $options = [])
 */
class AtrybutTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('atrybut');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('AtrybutTyp', [
            'foreignKey' => 'atrybut_typ_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('AtrybutPodrzedne', [
            'foreignKey' => 'atrybut_id'
        ]);
        $this->belongsToMany('Towar', [
            'foreignKey' => 'atrybut_id',
            'targetForeignKey' => 'towar_id',
            'joinTable' => 'towar_atrybut'
        ]);
        $this->hasMany('TowarAtrybut', [
            'foreignKey' => 'atrybut_id'
        ]);
        $this->addBehavior('Translate', ['fields' => ['nazwa','pod_tytul'], 'translationTable' => 'AtrybutI18n']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->integer('pole')
            ->allowEmpty('pole');

        $validator
            ->integer('kolejnosc')
            ->allowEmpty('kolejnosc');

        $validator
            ->allowEmpty('pod_tytul');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['atrybut_typ_id'], 'AtrybutTyp'));

        return $rules;
    }
    
}
