<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Producent Model
 *
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\HasMany $Towar
 * @property \App\Model\Table\I18nTable|\Cake\ORM\Association\BelongsToMany $I18n
 *
 * @method \App\Model\Entity\Producent get($primaryKey, $options = [])
 * @method \App\Model\Entity\Producent newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Producent[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Producent|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Producent patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Producent[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Producent findOrCreate($search, callable $callback = null, $options = [])
 */
class ProducentTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('producent');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Towar', [
            'foreignKey' => 'producent_id'
        ]);
        $this->addBehavior('Translate', ['fields' => ['gwarancja','naglowek'], 'translationTable' => 'ProducentI18n']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->integer('ukryty')
            ->allowEmpty('ukryty');

        $validator
            ->integer('kolejnosc')
            ->requirePresence('kolejnosc', 'create')
            ->notEmpty('kolejnosc');

        $validator
            ->allowEmpty('logo');

        $validator
            ->integer('na_zamowienie')
            ->allowEmpty('na_zamowienie');

        $validator
            ->allowEmpty('gwarancja');

        $validator
            ->allowEmpty('www');
        $validator
            ->allowEmpty('naglowek');

        return $validator;
    }
}
