<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AllegroKonto Model
 *
 * @method \App\Model\Entity\AllegroKonto get($primaryKey, $options = [])
 * @method \App\Model\Entity\AllegroKonto newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AllegroKonto[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AllegroKonto|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AllegroKonto patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AllegroKonto[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AllegroKonto findOrCreate($search, callable $callback = null, $options = [])
 */
class AllegroKontoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('allegro_konto');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('userId', 'create')
            ->notEmpty('userId');
        
        $validator
            ->requirePresence('restClientId', 'create')
            ->notEmpty('restClientId');

        $validator
            ->allowEmpty('restClientSecret');

        $validator
            ->allowEmpty('restApiKey');

        $validator
            ->requirePresence('restRedirectUri', 'create')
            ->notEmpty('restRedirectUri');

        $validator
            ->requirePresence('soapApiKey', 'create')
            ->notEmpty('soapApiKey');

        $validator
            ->allowEmpty('token');

        $validator
            ->integer('defaultCategory')
            ->allowEmpty('defaultCategory');

        $validator
            ->allowEmpty('defaultCategoryPath');

        $validator
            ->allowEmpty('defaultCategoryPathId');

        $validator
            ->allowEmpty('name');

        $validator
            ->boolean('isDefault')
            ->allowEmpty('isDefault');
        
        $validator
                ->dateTime('token_expire')
                ->allowEmpty('token_expire');

        $validator
            ->allowEmpty('devClientId');

        $validator
            ->allowEmpty('devClientSecret');

        $validator
            ->allowEmpty('device_code');

        $validator
            ->allowEmpty('user_code');
        
        $validator
            ->allowEmpty('api_interval');
        
        $validator
            ->allowEmpty('kraj');
        
        $validator
            ->allowEmpty('wojewodztwo');
        
        $validator
            ->allowEmpty('miasto');
        
        $validator
            ->allowEmpty('kod_pocztowy');
        $validator
                ->allowEmpty('automaticRenew');

        return $validator;
    }
}
