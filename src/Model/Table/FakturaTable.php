<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Faktura Model
 *
 * @property \App\Model\Table\ZamowienieTable|\Cake\ORM\Association\BelongsTo $Zamowienie
 * @property \App\Model\Table\FakturaPozycjaTable|\Cake\ORM\Association\HasMany $FakturaPozycja
 *
 * @method \App\Model\Entity\Faktura get($primaryKey, $options = [])
 * @method \App\Model\Entity\Faktura newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Faktura[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Faktura|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Faktura patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Faktura[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Faktura findOrCreate($search, callable $callback = null, $options = [])
 */
class FakturaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('faktura');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Zamowienie', [
            'foreignKey' => 'zamowienie_id'
        ]);
        $this->hasMany('FakturaPozycja', [
            'foreignKey' => 'faktura_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('typ');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        $validator
            ->integer('status_fiskalny')
            ->allowEmpty('status_fiskalny');

        $validator
            ->allowEmpty('id_dokumentu');

        $validator
            ->allowEmpty('numer_dostawcy');

        $validator
            ->allowEmpty('rozszerzenie_numeru');

        $validator
            ->allowEmpty('numer_dokumentu');

        $validator
            ->allowEmpty('numer_do_korekty');

        $validator
            ->date('data_wystawienia_do_korekty')
            ->allowEmpty('data_wystawienia_do_korekty');

        $validator
            ->allowEmpty('numer_zamowienia');

        $validator
            ->allowEmpty('magazyn_docelowy_mm');

        $validator
            ->allowEmpty('kod_kontrahenta');

        $validator
            ->allowEmpty('nazwa_skrocona');

        $validator
            ->allowEmpty('nazwa_pelna');

        $validator
            ->allowEmpty('miasto');

        $validator
            ->allowEmpty('kod_pocztowy');

        $validator
            ->allowEmpty('adres');

        $validator
            ->allowEmpty('nip');

        $validator
            ->allowEmpty('kategoria_dokumentu');

        $validator
            ->allowEmpty('podtytul_kategorii');

        $validator
            ->allowEmpty('miejsce_wystawienia');

        $validator
            ->date('data_wystawienia')
            ->allowEmpty('data_wystawienia');

        $validator
            ->date('data_sprzedazy')
            ->allowEmpty('data_sprzedazy');

        $validator
            ->date('data_otrzymania')
            ->allowEmpty('data_otrzymania');

        $validator
            ->integer('ilosc_pozycji')
            ->allowEmpty('ilosc_pozycji');

        $validator
            ->boolean('wg_cen_netto')
            ->allowEmpty('wg_cen_netto');

        $validator
            ->allowEmpty('aktywna_cena');

        $validator
            ->numeric('wartosc_netto')
            ->allowEmpty('wartosc_netto');

        $validator
            ->numeric('wartosc_vat')
            ->allowEmpty('wartosc_vat');

        $validator
            ->numeric('wartosc_brutto')
            ->allowEmpty('wartosc_brutto');

        $validator
            ->numeric('koszt')
            ->allowEmpty('koszt');

        $validator
            ->allowEmpty('rabat_nazwa');

        $validator
            ->numeric('rabat_procent')
            ->allowEmpty('rabat_procent');

        $validator
            ->allowEmpty('forma_platnosci');

        $validator
            ->date('termin_platnosci')
            ->allowEmpty('termin_platnosci');

        $validator
            ->numeric('kwota_zaplacona_przy_odbiorze')
            ->allowEmpty('kwota_zaplacona_przy_odbiorze');

        $validator
            ->numeric('wartosc_do_zaplaty')
            ->allowEmpty('wartosc_do_zaplaty');

        $validator
            ->integer('zaokraglenie_do_zaplaty')
            ->allowEmpty('zaokraglenie_do_zaplaty');

        $validator
            ->integer('zaokraglenie_vat')
            ->allowEmpty('zaokraglenie_vat');

        $validator
            ->boolean('automatyczne_przeliczanie')
            ->allowEmpty('automatyczne_przeliczanie');

        $validator
            ->integer('statusy_specjalne')
            ->allowEmpty('statusy_specjalne');

        $validator
            ->allowEmpty('wystawiajacy');

        $validator
            ->allowEmpty('odbiorca');

        $validator
            ->allowEmpty('podstawa_wydania');

        $validator
            ->numeric('wartosc_opakowan')
            ->allowEmpty('wartosc_opakowan');

        $validator
            ->numeric('wartosc_zwroconych_opakowan')
            ->allowEmpty('wartosc_zwroconych_opakowan');

        $validator
            ->allowEmpty('waluta');

        $validator
            ->numeric('kurs_waluty')
            ->allowEmpty('kurs_waluty');

        $validator
            ->allowEmpty('uwagi');

        $validator
            ->allowEmpty('komentarz');

        $validator
            ->allowEmpty('podtytul');

        $validator
            ->allowEmpty('empty_data');

        $validator
            ->boolean('import_dokumentu')
            ->allowEmpty('import_dokumentu');

        $validator
            ->boolean('eksportowy')
            ->allowEmpty('eksportowy');

        $validator
            ->integer('rodzaj_transakcji')
            ->allowEmpty('rodzaj_transakcji');

        $validator
            ->allowEmpty('platnosc_karta_nazwa');

        $validator
            ->numeric('platnosc_karta_kwota')
            ->allowEmpty('platnosc_karta_kwota');

        $validator
            ->allowEmpty('platnosc_kredyt_nazwa');

        $validator
            ->numeric('platnosc_kredyt_kwota')
            ->allowEmpty('platnosc_kredyt_kwota');

        $validator
            ->allowEmpty('panstwo');

        $validator
            ->allowEmpty('prefix_ue');

        $validator
            ->boolean('czy_ue')
            ->allowEmpty('czy_ue');

        $validator
            ->allowEmpty('token');

        $validator
            ->allowEmpty('sklep_nazwa');

        $validator
            ->allowEmpty('sklep_miasto');

        $validator
            ->allowEmpty('sklep_kod_pocztowy');

        $validator
            ->allowEmpty('sklep_adres');

        $validator
            ->allowEmpty('sklep_nip');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['zamowienie_id'], 'Zamowienie'));

        return $rules;
    }
}
