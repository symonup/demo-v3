<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * NewsletterAdres Model
 *
 * @method \App\Model\Entity\NewsletterAdre get($primaryKey, $options = [])
 * @method \App\Model\Entity\NewsletterAdre newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\NewsletterAdre[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\NewsletterAdre|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\NewsletterAdre patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\NewsletterAdre[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\NewsletterAdre findOrCreate($search, callable $callback = null, $options = [])
 */
class NewsletterAdresTable extends Table
{

    public $Translation;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('newsletter_adres');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->boolean('potwierdzony')
            ->allowEmpty('potwierdzony');

        $validator
            ->allowEmpty('token');

        $validator
            ->dateTime('data')
            ->allowEmpty('data');

        $validator
            ->integer('wyslany')
            ->allowEmpty('wyslany');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        $validator
            ->allowEmpty('locale');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email'],__('Email exist.')));

        return $rules;
    }
}
