<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WyszukiwarkaBox Model
 *
 * @property \App\Model\Table\WyszukiwarkaTable|\Cake\ORM\Association\BelongsTo $Wyszukiwarka
 *
 * @method \App\Model\Entity\WyszukiwarkaBox get($primaryKey, $options = [])
 * @method \App\Model\Entity\WyszukiwarkaBox newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\WyszukiwarkaBox[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\WyszukiwarkaBox|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\WyszukiwarkaBox patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\WyszukiwarkaBox[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\WyszukiwarkaBox findOrCreate($search, callable $callback = null, $options = [])
 */
class WyszukiwarkaBoxTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('wyszukiwarka_box');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Wyszukiwarka', [
            'foreignKey' => 'wyszukiwarka_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('nr_kolumny')
            ->requirePresence('nr_kolumny', 'create')
            ->notEmpty('nr_kolumny');

        $validator
            ->allowEmpty('tytul');

        $validator
            ->integer('kategoria')
            ->allowEmpty('kategoria');

        $validator
            ->requirePresence('atrybut', 'create')
            ->notEmpty('atrybut');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['wyszukiwarka_id'], 'Wyszukiwarka'));

        return $rules;
    }
}
