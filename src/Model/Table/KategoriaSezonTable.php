<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * KategoriaSezon Model
 *
 * @property \App\Model\Table\SezonTable|\Cake\ORM\Association\BelongsTo $Sezon
 * @property \App\Model\Table\KategoriaTable|\Cake\ORM\Association\BelongsTo $Kategoria
 *
 * @method \App\Model\Entity\KategoriaSezon get($primaryKey, $options = [])
 * @method \App\Model\Entity\KategoriaSezon newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\KategoriaSezon[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\KategoriaSezon|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\KategoriaSezon patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\KategoriaSezon[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\KategoriaSezon findOrCreate($search, callable $callback = null, $options = [])
 */
class KategoriaSezonTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('kategoria_sezon');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Sezon', [
            'foreignKey' => 'sezon_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Kategoria', [
            'foreignKey' => 'kategoria_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['sezon_id'], 'Sezon'));
        $rules->add($rules->existsIn(['kategoria_id'], 'Kategoria'));

        return $rules;
    }
}
