<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AllegroZamowienie Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $AllegroKonto
 * @property |\Cake\ORM\Association\BelongsTo $Orders
 * @property \App\Model\Table\KupujaciesTable|\Cake\ORM\Association\BelongsTo $Kupujacies
 * @property |\Cake\ORM\Association\BelongsTo $Payments
 * @property |\Cake\ORM\Association\BelongsTo $AllegroWysylkas
 * @property |\Cake\ORM\Association\BelongsTo $Paczkomats
 * @property |\Cake\ORM\Association\HasMany $ZamowienieDhl
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\BelongsToMany $Towar
 *
 * @method \App\Model\Entity\AllegroZamowienie get($primaryKey, $options = [])
 * @method \App\Model\Entity\AllegroZamowienie newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AllegroZamowienie[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AllegroZamowienie|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AllegroZamowienie patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AllegroZamowienie[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AllegroZamowienie findOrCreate($search, callable $callback = null, $options = [])
 */
class AllegroZamowienieTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('allegro_zamowienie');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('AllegroKonto', [
            'foreignKey' => 'allegro_konto_id'
        ]);
        $this->hasMany('ZamowienieDhl', [
            'foreignKey' => 'allegro_zamowienie_id'
        ]);
        $this->hasMany('AllegroZamowienieTowar', [
            'foreignKey' => 'allegro_zamowienie_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('kupujacy_email');

        $validator
            ->allowEmpty('kupujacy_login');

        $validator
            ->boolean('kupujacy_guest')
            ->allowEmpty('kupujacy_guest');

        $validator
            ->allowEmpty('kupujacy_telefon');

        $validator
            ->allowEmpty('payment_type');

        $validator
            ->allowEmpty('payment_provider');

        $validator
            ->dateTime('payment_finished_at')
            ->allowEmpty('payment_finished_at');

        $validator
            ->numeric('payment_wplata_kwota')
            ->allowEmpty('payment_wplata_kwota');

        $validator
            ->allowEmpty('payment_wplata_waluta');

        $validator
            ->allowEmpty('allegro_status');

        $validator
            ->allowEmpty('allegro_wysylka_nazwa');

        $validator
            ->numeric('allegro_wysylka_koszt')
            ->allowEmpty('allegro_wysylka_koszt');

        $validator
            ->allowEmpty('allegro_wysylka_waluta');

        $validator
            ->boolean('allegro_wysylka_smart')
            ->allowEmpty('allegro_wysylka_smart');

        $validator
            ->boolean('faktura')
            ->allowEmpty('faktura');

        $validator
            ->numeric('wartosc_razem')
            ->allowEmpty('wartosc_razem');

        $validator
            ->allowEmpty('waluta');

        $validator
            ->allowEmpty('wysylka_imie');

        $validator
            ->allowEmpty('wysylka_nazwisko');

        $validator
            ->allowEmpty('wysylka_ulica');

        $validator
            ->allowEmpty('wysylka_miasto');

        $validator
            ->allowEmpty('wysylka_kod');

        $validator
            ->allowEmpty('wysylka_kraj');

        $validator
            ->allowEmpty('wysylka_telefon');

        $validator
            ->allowEmpty('paczkomat_nazwa');

        $validator
            ->allowEmpty('paczkomat_opis');

        $validator
            ->allowEmpty('paczkomat_adres');

        $validator
            ->allowEmpty('faktura_ulica');

        $validator
            ->allowEmpty('faktura_miasto');

        $validator
            ->allowEmpty('faktura_kod');

        $validator
            ->allowEmpty('faktura_kraj');

        $validator
            ->allowEmpty('faktura_firma');

        $validator
            ->allowEmpty('faktura_nip');

        $validator
            ->dateTime('data_zakupu')
            ->allowEmpty('data_zakupu');

        $validator
            ->allowEmpty('status');

        $validator
            ->allowEmpty('uwagi');

        $validator
            ->allowEmpty('notatki');

        $validator
            ->allowEmpty('nr_listu_przewozowego');

        $validator
            ->allowEmpty('kurier_typ');

        $validator
            ->dateTime('get_date')
            ->allowEmpty('get_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['allegro_konto_id'], 'AllegroKonto'));

        return $rules;
    }
}
