<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UzytkownikAdres Model
 *
 * @property \App\Model\Table\UzytkownikTable|\Cake\ORM\Association\BelongsTo $Uzytkownik
 *
 * @method \App\Model\Entity\UzytkownikAdre get($primaryKey, $options = [])
 * @method \App\Model\Entity\UzytkownikAdre newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UzytkownikAdre[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UzytkownikAdre|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UzytkownikAdre patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UzytkownikAdre[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UzytkownikAdre findOrCreate($search, callable $callback = null, $options = [])
 */
class UzytkownikAdresTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('uzytkownik_adres');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Uzytkownik', [
            'foreignKey' => 'uzytkownik_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('nazwa_adresu');

        $validator
            ->allowEmpty('imie');

        $validator
            ->allowEmpty('nazwisko');

        $validator
            ->allowEmpty('firma');

        $validator
            ->allowEmpty('nip');

        $validator
            ->allowEmpty('telefon');

        $validator
            ->requirePresence('ulica', 'create')
            ->notEmpty('ulica');

        $validator
            ->allowEmpty('nr_domu');

        $validator
            ->allowEmpty('nr_lokalu');

        $validator
            ->allowEmpty('kod');

        $validator
            ->requirePresence('miasto', 'create')
            ->notEmpty('miasto');

        $validator
            ->boolean('domyslny_wysylka')
            ->allowEmpty('domyslny_wysylka');

        $validator
            ->boolean('domyslny_faktura')
            ->allowEmpty('domyslny_faktura');

        $validator
            ->integer('typ')
            ->requirePresence('typ', 'create')
            ->notEmpty('typ');
        
        $validator
            ->allowEmpty('email');
        $validator
            ->allowEmpty('kraj');
        $validator
            ->allowEmpty('kierunkowy');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['uzytkownik_id'], 'Uzytkownik'));

        return $rules;
    }
}
