<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Kategoria Model
 *
 * @property \App\Model\Table\KategoriaTable|\Cake\ORM\Association\BelongsTo $ParentKategoria
 * @property \App\Model\Table\KategoriaTable|\Cake\ORM\Association\HasMany $ChildKategoria
 * @property \App\Model\Table\AtrybutTable|\Cake\ORM\Association\BelongsToMany $Atrybut
 * @property \App\Model\Table\BanerTable|\Cake\ORM\Association\BelongsToMany $Baner
 * @property \App\Model\Table\I18nTable|\Cake\ORM\Association\BelongsToMany $I18n
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\BelongsToMany $Towar
 *
 * @method \App\Model\Entity\Kategorium get($primaryKey, $options = [])
 * @method \App\Model\Entity\Kategorium newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Kategorium[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Kategorium|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Kategorium patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Kategorium[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Kategorium findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TreeBehavior
 */
class KategoriaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public $crumb = [];
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('kategoria');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Translate', ['fields' => ['nazwa', 'naglowek','opis_short','seo_tekst', 'seo_tytul', 'seo_slowa', 'seo_opis'], 'translationTable' => 'KategoriaI18n']);
        $this->addBehavior('Tree', [
            'recoverOrder' => ['kolejnosc' => 'DESC'],
        ]);
        
        $this->belongsTo('ParentKategoria', [
            'className' => 'Kategoria',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('ChildKategoria', [
            'className' => 'Kategoria',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('KategoriaMap', [
            'foreignKey' => 'kategoria_id'
        ]);
        $this->belongsToMany('Towar', [
            'foreignKey' => 'kategoria_id',
            'targetForeignKey' => 'towar_id',
            'joinTable' => 'towar_kategoria'
        ]);
        $this->belongsTo('Promocja', [
            'className'=>'Towar',
            'foreignKey' => 'towar_promocja_id'
        ]);
        $this->belongsTo('GoogleKategorie',[
            'foreignKey' => 'google_kategoria_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('ukryta')
            ->requirePresence('ukryta', 'create')
            ->allowEmpty('ukryta');

        $validator
            ->integer('kolejnosc')
            ->allowEmpty('kolejnosc');

        $validator
            ->date('data',['Y-m-d','Y-m-d H:i:s','H:i:s'])
            ->allowEmpty('data');

        $validator
            ->boolean('wyswietl_miniatury')
            ->allowEmpty('wyswietl_miniatury');
        $validator
            ->boolean('promuj')
            ->allowEmpty('promuj');

        $validator
            ->boolean('konfekcja')
            ->allowEmpty('konfekcja');

        $validator
            ->allowEmpty('zdjecie');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->allowEmpty('sciezka');

        $validator
            ->allowEmpty('naglowek');

        $validator
            ->allowEmpty('seo_tytul');

        $validator
            ->allowEmpty('seo_opis');

        $validator
            ->allowEmpty('seo_slowa');

        $validator
            ->allowEmpty('tytul_strony');

        $validator
            ->boolean('menu_top')
            ->allowEmpty('menu_top');

        $validator
            ->allowEmpty('menu_tekst_naglowek');

        $validator
            ->allowEmpty('menu_tekst');

        $validator
            ->allowEmpty('tagi');

        $validator
            ->allowEmpty('nazwa_producent');

        $validator
            ->allowEmpty('ikona');
        $validator
                ->allowEmpty('seo_tekst');
        $validator
                ->boolean('preorder')
            ->allowEmpty('preorder');
        $validator
                ->allowEmpty('google_kategoria_id');
        $validator
                ->allowEmpty('icon_class');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['parent_id'], 'ParentKategoria'));

        return $rules;
    }
    
    public function getCategoryTreeAll($parent_id = null) {
        $returnArray = [];
        $conditions = ['(Kategoria.preorder = 0 OR Kategoria.preorder IS NULL)'];
            if (empty($parent_id))
                $conditions['Kategoria.parent_id IS'] = NULL;
            else
                $conditions['Kategoria.parent_id'] = $parent_id;
        
        $mainCategories = $this->find('all', ['conditions' => $conditions, 'order' => 'Kategoria.kolejnosc DESC']);
        if (empty($mainCategories))
            return false;
        foreach ($mainCategories as $mainCategory) {
            $returnArray[$mainCategory->id] = [
                'id' => $mainCategory->id,
                'nazwa' => $mainCategory->nazwa,
                'zdjecie' => $mainCategory->zdjecie,
                'ikona' => $mainCategory->ikona,
                'icon_class' => $mainCategory->icon_class,
                'preorder' => $mainCategory->preorder,
                'sub' => $this->getCategoryTreeAll($mainCategory->id)
            ];
        }
        return $returnArray;
    }
    
    public function getCategoryTree($parent_id = null,$promowane=false) {
        $returnArray = [];
        $conditions = [];
            if (empty($parent_id))
                $conditions['Kategoria.parent_id IS'] = NULL;
            else
                $conditions['Kategoria.parent_id'] = $parent_id;
            $conditions['Kategoria.ukryta']=0;
            if($promowane){
                $conditions['Kategoria.promuj']=1;
            }
        
        $mainCategories = $this->find('all', ['conditions' => $conditions, 'order' => 'Kategoria.kolejnosc DESC']);
        if (empty($mainCategories))
            return false;
        foreach ($mainCategories as $mainCategory) {
            $returnArray[$mainCategory->id] = [
                'id' => $mainCategory->id,
                'nazwa' => $mainCategory->nazwa,
                'opis_short' => $mainCategory->opis_short,
                'seo_tytul' => $mainCategory->seo_tytul,
                'zdjecie' => $mainCategory->zdjecie,
                'ikona' => $mainCategory->ikona,
                'icon_class' => $mainCategory->icon_class,
                'promuj' => $mainCategory->promuj,
                'sub' => $this->getCategoryTree($mainCategory->id,$promowane)
            ];
        }
        return $returnArray;
    }
    private function getPreorders(){
        $preorderPlatforms = \Cake\Core\Configure::read('preorderPlatforms');
        $platforms=[];
        if(!empty($preorderPlatforms)){
        foreach ($preorderPlatforms as $platformaId => $platformName){
            $platforms[$platformaId]=['id'=>$platformaId,'nazwa'=>$platformName];
        }
        }
        return $platforms;
    }

    public $allIds=[];
    public function getAllIds($parent_id = null) {
        if(!empty($parent_id)){
            $this->allIds[$parent_id]=$parent_id;
        }
        $kategorie=$this->find('all')->where(['parent_id'=>$parent_id]);
        if($kategorie->count()>0){
            foreach ($kategorie as $kat){
                $this->getAllIds($kat->id);
            }
        }
        return $this->allIds;
    }
    public function getCrumb($kategoria_id, $routArr = [], $allowCat = [],$action='index') {

        if (empty($kategoria_id))
            return false;
        if (!empty($allowCat) && !key_exists($kategoria_id, $allowCat))
            return false;
        $kategoria = $this->find('all',['conditions'=>['Kategoria.id'=>$kategoria_id]])->first();
        if (empty($kategoria))
            return false;
        if (!empty($routArr))
            $this->crumb[$kategoria->nazwa] = \Cake\Routing\Router::url(array_merge($routArr, [$kategoria->id, $this->friendlyURL($kategoria->nazwa)]));
        else
            $this->crumb[$kategoria->nazwa] = ['controller' => 'Towar', 'action' => $action, $kategoria->id, $this->friendlyURL($kategoria->nazwa)];
        $this->getCrumb($kategoria->parent_id, $routArr, $allowCat,$action);
        return $this->crumb;
    }
    public function friendlyURL($url) {
        $tabela = array(
            //WIN
            "\xb9" => "a", "\xa5" => "A", "\xe6" => "c", "\xc6" => "C",
            "\xea" => "e", "\xca" => "E", "\xb3" => "l", "\xa3" => "L",
            "\xf3" => "o", "\xd3" => "O", "\x9c" => "s", "\x8c" => "S",
            "\x9f" => "z", "\xaf" => "Z", "\xbf" => "z", "\xac" => "Z",
            "\xf1" => "n", "\xd1" => "N",
            //UTF
            "\xc4\x85" => "a", "\xc4\x84" => "A", "\xc4\x87" => "c", "\xc4\x86" => "C",
            "\xc4\x99" => "e", "\xc4\x98" => "E", "\xc5\x82" => "l", "\xc5\x81" => "L",
            "\xc3\xb3" => "o", "\xc3\x93" => "O", "\xc5\x9b" => "s", "\xc5\x9a" => "S",
            "\xc5\xbc" => "z", "\xc5\xbb" => "Z", "\xc5\xba" => "z", "\xc5\xb9" => "Z",
            "\xc5\x84" => "n", "\xc5\x83" => "N",
            //ISO
            "\xb1" => "a", "\xa1" => "A", "\xe6" => "c", "\xc6" => "C",
            "\xea" => "e", "\xca" => "E", "\xb3" => "l", "\xa3" => "L",
            "\xf3" => "o", "\xd3" => "O", "\xb6" => "s", "\xa6" => "S",
            "\xbc" => "z", "\xac" => "Z", "\xbf" => "z", "\xaf" => "Z",
            "\xf1" => "n", "\xd1" => "N",
            //I to co nie potrzebne
            " " => "-", "$" => "-", "!" => "-", "@" => "-", "#" => "-", "%" => "-", "/" => "-","[" => "","]" => "","(" => "-",")" => "-");

        return strtolower(strtr($url, $tabela));
    }
}
