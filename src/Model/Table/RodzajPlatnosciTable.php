<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RodzajPlatnosci Model
 *
 * @property \App\Model\Table\PlatnoscTable|\Cake\ORM\Association\HasMany $Platnosc
 * @property \App\Model\Table\WysylkaPlatnoscTable|\Cake\ORM\Association\HasMany $WysylkaPlatnosc
 *
 * @method \App\Model\Entity\RodzajPlatnosci get($primaryKey, $options = [])
 * @method \App\Model\Entity\RodzajPlatnosci newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\RodzajPlatnosci[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RodzajPlatnosci|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RodzajPlatnosci patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RodzajPlatnosci[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\RodzajPlatnosci findOrCreate($search, callable $callback = null, $options = [])
 */
class RodzajPlatnosciTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('rodzaj_platnosci');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Platnosc', [
            'foreignKey' => 'rodzaj_platnosci_id'
        ]);
        $this->hasMany('WysylkaPlatnosc', [
            'foreignKey' => 'rodzaj_platnosci_id'
        ]);
        $this->belongsToMany('Wysylka', [
             'foreignKey' => 'rodzaj_platnosci_id',
            'targetForeignKey' => 'wysylka_id',
            'joinTable' => 'wysylka_platnosc'
        ]);
        $this->addBehavior('Translate', ['fields' => ['nazwa'], 'translationTable' => 'RodzajPlatnosciI18n']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->requirePresence('nazwa_wyswietlana', 'create')
            ->notEmpty('nazwa_wyswietlana');

        $validator
            ->boolean('platnosc_elektroniczna')
            ->requirePresence('platnosc_elektroniczna', 'create')
            ->notEmpty('platnosc_elektroniczna');

        $validator
            ->boolean('pobranie')
            ->requirePresence('pobranie', 'create')
            ->notEmpty('pobranie');

        $validator
            ->allowEmpty('opis');

        $validator
            ->allowEmpty('konfig1');

        $validator
            ->allowEmpty('konfig2');

        $validator
            ->allowEmpty('konfig3');

        $validator
            ->allowEmpty('konfig4');

        $validator
            ->requirePresence('waluta', 'create')
            ->notEmpty('waluta');

        $validator
            ->boolean('aktywna')
            ->requirePresence('aktywna', 'create')
            ->notEmpty('aktywna');

        $validator
            ->boolean('koszt_dodatkowy')
            ->allowEmpty('koszt_dodatkowy');

        $validator
            ->numeric('prowizja')
            ->allowEmpty('prowizja');

        $validator
            ->numeric('darmowa_wysylka')
            ->allowEmpty('darmowa_wysylka');

        $validator
            ->allowEmpty('ikona');
        
        $validator
                ->allowEmpty('kolejnosc');
        $validator
                ->allowEmpty('test');
        $validator
                ->boolean('auto_submit')
                ->allowEmpty('auto_submit');
        $validator
                ->boolean('disabled_kraj')
                ->allowEmpty('disabled_kraj');

        return $validator;
    }
}
