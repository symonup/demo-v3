<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TowarOpinia Model
 *
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\BelongsTo $Towar
 *
 * @method \App\Model\Entity\TowarOpinium get($primaryKey, $options = [])
 * @method \App\Model\Entity\TowarOpinium newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TowarOpinium[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TowarOpinium|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TowarOpinium patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TowarOpinium[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TowarOpinium findOrCreate($search, callable $callback = null, $options = [])
 */
class TowarOpiniaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('towar_opinia');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Towar', [
            'foreignKey' => 'towar_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('data')
            ->requirePresence('data', 'create')
            ->notEmpty('data');

        $validator
            ->requirePresence('autor', 'create')
            ->notEmpty('autor');

        $validator
            ->integer('ocena')
            ->requirePresence('ocena', 'create')
            ->notEmpty('ocena');

        $validator
            ->requirePresence('opinia', 'create')
            ->notEmpty('opinia');

        $validator
            ->allowEmpty('plusy');

        $validator
            ->allowEmpty('minusy');

        $validator
            ->requirePresence('jezyk', 'create')
            ->notEmpty('jezyk');

        $validator
            ->integer('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['towar_id'], 'Towar'));

        return $rules;
    }
}
