<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ZamowienieDhl Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Zamowienie
 * @property \Cake\ORM\Association\BelongsTo $AllegroZamowienie
 *
 * @method \App\Model\Entity\ZamowienieDhl get($primaryKey, $options = [])
 * @method \App\Model\Entity\ZamowienieDhl newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ZamowienieDhl[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ZamowienieDhl|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ZamowienieDhl patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ZamowienieDhl[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ZamowienieDhl findOrCreate($search, callable $callback = null)
 */
class ZamowienieDhlTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('zamowienie_dhl');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Zamowienie', [
            'foreignKey' => 'zamowienie_id'
        ]);
        $this->belongsTo('AllegroZamowienie', [
            'foreignKey' => 'allegro_zamowienie_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {

        $validator
            ->allowEmpty('shipmentId');

        $validator
            ->add('shipmentDate', 'valid', ['rule' => 'date'])
            ->requirePresence('shipmentDate', 'create')
            ->notEmpty('shipmentDate');

        $validator
            ->allowEmpty('labelType');

        $validator
            ->allowEmpty('labelName');

        $validator
            ->allowEmpty('labelData');

        $validator
            ->allowEmpty('labelMimeType');

        $validator
            ->allowEmpty('orderId');

        $validator
            ->add('data_dodania', 'valid', ['rule' => 'datetime'])
            ->allowEmpty('data_dodania');

        $validator
            ->allowEmpty('opis');

        $validator
            ->add('godzina_od', 'valid', ['rule' => 'time'])
            ->allowEmpty('godzina_od');

        $validator
            ->add('godzina_do', 'valid', ['rule' => 'time'])
            ->allowEmpty('godzina_do');

        $validator
            ->allowEmpty('type');

        $validator
            ->allowEmpty('uwagi');

        $validator
            ->allowEmpty('requestData');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['zamowienie_id'], 'Zamowienie'));
        $rules->add($rules->existsIn(['allegro_zamowienie_id'], 'AllegroZamowienie'));

        return $rules;
    }
}
