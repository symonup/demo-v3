<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Waluta Model
 *
 * @method \App\Model\Entity\Walutum get($primaryKey, $options = [])
 * @method \App\Model\Entity\Walutum newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Walutum[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Walutum|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Walutum patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Walutum[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Walutum findOrCreate($search, callable $callback = null, $options = [])
 */
class WalutaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('waluta');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('symbol', 'create')
            ->notEmpty('symbol');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->numeric('kurs')
            ->requirePresence('kurs', 'create')
            ->notEmpty('kurs');

        $validator
            ->boolean('domyslna')
            ->allowEmpty('domyslna');

        $validator
            ->requirePresence('separator_d', 'create')
            ->notEmpty('separator_d');

        $validator
            ->requirePresence('separator_t', 'create')
            ->notEmpty('separator_t');

        $validator
            ->boolean('symbol_przed')
            ->allowEmpty('symbol_przed');
        $validator
            ->boolean('space')
            ->allowEmpty('space');

        return $validator;
    }
}
