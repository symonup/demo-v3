<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Okazje Model
 *
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\BelongsToMany $Towar
 *
 * @method \App\Model\Entity\Okazje get($primaryKey, $options = [])
 * @method \App\Model\Entity\Okazje newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Okazje[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Okazje|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Okazje patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Okazje[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Okazje findOrCreate($search, callable $callback = null, $options = [])
 */
class OkazjeTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('okazje');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsToMany('Towar', [
            'foreignKey' => 'okazje_id',
            'targetForeignKey' => 'towar_id',
            'joinTable' => 'okazje_towar'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->integer('kolejnosc')
            ->allowEmpty('kolejnosc');

        return $validator;
    }
}
