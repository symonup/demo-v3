<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PunktyOdbioru Model
 *
 * @property \App\Model\Table\WysylkaTable|\Cake\ORM\Association\BelongsToMany $Wysylka
 *
 * @method \App\Model\Entity\PunktyOdbioru get($primaryKey, $options = [])
 * @method \App\Model\Entity\PunktyOdbioru newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PunktyOdbioru[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PunktyOdbioru|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PunktyOdbioru patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PunktyOdbioru[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PunktyOdbioru findOrCreate($search, callable $callback = null, $options = [])
 */
class PunktyOdbioruTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('punkty_odbioru');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->hasMany('Zamowienie',[
            'foreignKey' => 'punkty_odbioru_id'
        ]);
        $this->belongsToMany('Wysylka', [
            'foreignKey' => 'punkty_odbioru_id',
            'targetForeignKey' => 'wysylka_id',
            'joinTable' => 'wysylka_punkty_odbioru'
        ]);
        $this->addBehavior('Translate', ['fields' => ['nazwa','adres','info'], 'translationTable' => 'RodzajPlatnosciI18n']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->allowEmpty('adres');

        $validator
            ->allowEmpty('mapa');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->allowEmpty('telefon');

        $validator
            ->allowEmpty('info');

        $validator
            ->boolean('aktywny')
            ->allowEmpty('aktywny');

        return $validator;
    }

}
