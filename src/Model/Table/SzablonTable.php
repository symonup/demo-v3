<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Szablon Model
 *
 * @property \App\Model\Table\I18nTable|\Cake\ORM\Association\BelongsToMany $I18n
 *
 * @method \App\Model\Entity\Szablon get($primaryKey, $options = [])
 * @method \App\Model\Entity\Szablon newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Szablon[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Szablon|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Szablon patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Szablon[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Szablon findOrCreate($search, callable $callback = null, $options = [])
 */
class SzablonTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('szablon');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Translate', ['fields' => ['temat', 'tresc'], 'translationTable' => 'SzablonI18n']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->requirePresence('label', 'create')
            ->notEmpty('label');

        $validator
            ->requirePresence('temat', 'create')
            ->notEmpty('temat');

        $validator
            ->requirePresence('tresc', 'create')
            ->notEmpty('tresc');

        $validator
            ->requirePresence('opis', 'create')
            ->notEmpty('opis');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));

        return $rules;
    }
}
