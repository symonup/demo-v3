<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TowarUzytkownikRabat Model
 *
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\BelongsTo $Towar
 * @property \App\Model\Table\UzytkownikTable|\Cake\ORM\Association\BelongsTo $Uzytkownik
 *
 * @method \App\Model\Entity\TowarUzytkownikRabat get($primaryKey, $options = [])
 * @method \App\Model\Entity\TowarUzytkownikRabat newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TowarUzytkownikRabat[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TowarUzytkownikRabat|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TowarUzytkownikRabat patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TowarUzytkownikRabat[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TowarUzytkownikRabat findOrCreate($search, callable $callback = null, $options = [])
 */
class TowarUzytkownikRabatTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('towar_uzytkownik_rabat');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Towar', [
            'foreignKey' => 'towar_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Uzytkownik', [
            'foreignKey' => 'uzytkownik_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('rabat')
            ->requirePresence('rabat', 'create')
            ->notEmpty('rabat');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['towar_id'], 'Towar'));
        $rules->add($rules->existsIn(['uzytkownik_id'], 'Uzytkownik'));

        return $rules;
    }
}
