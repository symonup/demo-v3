<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Wysylka Model
 *
 * @property \App\Model\Table\VatsTable|\Cake\ORM\Association\BelongsTo $Vats
 * @property \App\Model\Table\WysylkaKosztTable|\Cake\ORM\Association\HasMany $WysylkaKoszt
 * @property \App\Model\Table\PlatnoscTable|\Cake\ORM\Association\BelongsToMany $Platnosc
 *
 * @method \App\Model\Entity\Wysylka get($primaryKey, $options = [])
 * @method \App\Model\Entity\Wysylka newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Wysylka[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Wysylka|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Wysylka patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Wysylka[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Wysylka findOrCreate($search, callable $callback = null, $options = [])
 */
class WysylkaTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('wysylka');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Vat', [
            'foreignKey' => 'vat_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('WysylkaKoszt', [
            'foreignKey' => 'wysylka_id'
        ]);
        $this->hasMany('WysylkaKrajeKoszty', [
            'foreignKey' => 'wysylka_id'
        ]);
        $this->hasMany('WysylkaPlatnosc', [
            'foreignKey' => 'wysylka_id'
        ]);
        $this->belongsToMany('RodzajPlatnosci', [
            'foreignKey' => 'wysylka_id',
            'targetForeignKey' => 'rodzaj_platnosci_id',
            'joinTable' => 'wysylka_platnosc'
        ]);

        $this->belongsToMany('PunktyOdbioru', [
            'foreignKey' => 'wysylka_id',
            'targetForeignKey' => 'punkty_odbioru_id',
            'joinTable' => 'wysylka_punkty_odbioru'
        ]);
        $this->addBehavior('Translate', ['fields' => ['nazwa', 'opis', 'komunikat'], 'translationTable' => 'RodzajPlatnosciI18n']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->requirePresence('nazwa', 'create')
                ->notEmpty('nazwa');

        $validator
                ->numeric('koszt_netto')
                ->allowEmpty('koszt_netto');

        $validator
                ->numeric('koszt_brutto')
                ->allowEmpty('koszt_brutto');

        $validator
                ->numeric('koszt_dodatkowy')
                ->allowEmpty('koszt_dodatkowy');

        $validator
                ->allowEmpty('opis');

        $validator
                ->boolean('aktywna')
                ->requirePresence('aktywna', 'create')
                ->notEmpty('aktywna');

        $validator
                ->allowEmpty('obrazek');

        $validator
                ->integer('duzy_gabaryt')
                ->allowEmpty('duzy_gabaryt');

        $validator
                ->boolean('paczkomat')
                ->allowEmpty('paczkomat');

        $validator
                ->allowEmpty('komunikat');
        $validator
                ->boolean('elektroniczna')
                ->allowEmpty('elektroniczna');
        $validator
                ->boolean('odbior_osobisty')
                ->allowEmpty('odbior_osobisty');
        $validator
                ->boolean('koszty_kraj')
                ->allowEmpty('koszty_kraj');
        $validator
                ->boolean('is_long')
                ->allowEmpty('is_long');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['vat_id'], 'Vat'));

        return $rules;
    }

    public function getPrice($waga = 0, $wartoscKoszyka = 0, $retKoszt = false) {
        $wysylki = $this->find('all', ['contain' => ['WysylkaKoszt' => ['conditions' => ['WysylkaKoszt.od <=' => $waga], 'sort' => ['WysylkaKoszt.od' => 'desc']]], 'conditions' => ['Wysylka.aktywna' => 1]]);
        $returnArray = [];
        $koszt = 0;
        $free = false;
        $darmowaLimit = c('wysylka.limit');
        if (!empty($darmowaLimit)) {
            if ($wartoscKoszyka >= $darmowaLimit) {
                $free = true;
            }
        }
        foreach ($wysylki as $wysylka) {
            if ($free) {
                $koszt = 0;
            } else {
                if (!empty($wysylka->wysylka_koszt)) {
                    $koszt = $wysylka->wysylka_koszt[0]['koszt_brutto'];
                } else {
                    $koszt = $wysylka->koszt_brutto;
                }
            }
            $returnArray[$wysylka->id] = $koszt;
        }
        if ($retKoszt) {
            return $koszt;
        } else {
            return $returnArray;
        }
    }

}
