<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AllegroAukcjaWariant Model
 *
 * @property \App\Model\Table\AllegroAukcjaTable|\Cake\ORM\Association\BelongsTo $AllegroAukcja
 *
 * @method \App\Model\Entity\AllegroAukcjaWariant get($primaryKey, $options = [])
 * @method \App\Model\Entity\AllegroAukcjaWariant newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AllegroAukcjaWariant[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AllegroAukcjaWariant|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AllegroAukcjaWariant patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AllegroAukcjaWariant[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AllegroAukcjaWariant findOrCreate($search, callable $callback = null, $options = [])
 */
class AllegroAukcjaWariantTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('allegro_aukcja_wariant');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('AllegroAukcja', [
            'foreignKey' => 'allegro_aukcja_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('fid')
            ->requirePresence('fid', 'create')
            ->notEmpty('fid');

        $validator
            ->requirePresence('mask', 'create')
            ->notEmpty('mask');

        $validator
            ->integer('quantity')
            ->requirePresence('quantity', 'create')
            ->notEmpty('quantity');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['allegro_aukcja_id'], 'AllegroAukcja'));

        return $rules;
    }
}
