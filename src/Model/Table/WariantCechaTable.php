<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WariantCecha Model
 *
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\BelongsTo $Towar
 * @property \App\Model\Table\WariantCechaWartoscTable|\Cake\ORM\Association\HasMany $WariantCechaWartosc
 *
 * @method \App\Model\Entity\WariantCecha get($primaryKey, $options = [])
 * @method \App\Model\Entity\WariantCecha newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\WariantCecha[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\WariantCecha|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\WariantCecha patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\WariantCecha[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\WariantCecha findOrCreate($search, callable $callback = null, $options = [])
 */
class WariantCechaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('wariant_cecha');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Towar', [
            'foreignKey' => 'towar_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('WariantCechaWartosc', [
            'foreignKey' => 'wariant_cecha_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('cecha', 'create')
            ->notEmpty('cecha');

        $validator
            ->requirePresence('typ', 'create')
            ->notEmpty('typ');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['towar_id'], 'Towar'));

        return $rules;
    }
}
