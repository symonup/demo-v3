<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * HotDeal Model
 *
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\BelongsTo $Towar
 *
 * @method \App\Model\Entity\HotDeal get($primaryKey, $options = [])
 * @method \App\Model\Entity\HotDeal newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\HotDeal[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\HotDeal|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\HotDeal patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\HotDeal[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\HotDeal findOrCreate($search, callable $callback = null, $options = [])
 */
class HotDealTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('hot_deal');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Towar', [
            'foreignKey' => 'towar_id',
            'joinType' => 'INNER'
        ]);
        $this->addBehavior('Translate', ['fields' => ['nazwa'], 'translationTable' => 'HotDealI18n']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->numeric('cena')
            ->allowEmpty('cena');

        $validator
            ->allowEmpty('nazwa');

        $validator
            ->dateTime('data_od')
            ->allowEmpty('data_od');

        $validator
            ->dateTime('data_do')
            ->requirePresence('data_do', 'create')
            ->notEmpty('data_do');

        $validator
            ->integer('ilosc')
            ->allowEmpty('ilosc');

        $validator
            ->integer('sprzedano')
            ->allowEmpty('sprzedano');

        $validator
            ->boolean('aktywna')
            ->requirePresence('aktywna', 'create')
            ->notEmpty('aktywna');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['towar_id'], 'Towar'));

        return $rules;
    }
}
