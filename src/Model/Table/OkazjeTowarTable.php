<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OkazjeTowar Model
 *
 * @property \App\Model\Table\OkazjeTable|\Cake\ORM\Association\BelongsTo $Okazje
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\BelongsTo $Towar
 *
 * @method \App\Model\Entity\OkazjeTowar get($primaryKey, $options = [])
 * @method \App\Model\Entity\OkazjeTowar newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OkazjeTowar[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OkazjeTowar|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OkazjeTowar patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OkazjeTowar[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OkazjeTowar findOrCreate($search, callable $callback = null, $options = [])
 */
class OkazjeTowarTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('okazje_towar');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Okazje', [
            'foreignKey' => 'okazje_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Towar', [
            'foreignKey' => 'towar_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['okazje_id'], 'Okazje'));
        $rules->add($rules->existsIn(['towar_id'], 'Towar'));

        return $rules;
    }
}
