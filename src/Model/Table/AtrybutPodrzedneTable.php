<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AtrybutPodrzedne Model
 *
 * @property \App\Model\Table\AtrybutTable|\Cake\ORM\Association\BelongsTo $Atrybut
 * @property \App\Model\Table\TowarAtrybutTable|\Cake\ORM\Association\HasMany $TowarAtrybut
 *
 * @method \App\Model\Entity\AtrybutPodrzedne get($primaryKey, $options = [])
 * @method \App\Model\Entity\AtrybutPodrzedne newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AtrybutPodrzedne[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AtrybutPodrzedne|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AtrybutPodrzedne patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AtrybutPodrzedne[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AtrybutPodrzedne findOrCreate($search, callable $callback = null, $options = [])
 */
class AtrybutPodrzedneTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('atrybut_podrzedne');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Atrybut', [
            'foreignKey' => 'atrybut_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('TowarAtrybut', [
            'foreignKey' => 'atrybut_podrzedne_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['atrybut_id'], 'Atrybut'));

        return $rules;
    }
}
