<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Platforma Model
 *
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\HasMany $Towar
 *
 * @method \App\Model\Entity\Platforma get($primaryKey, $options = [])
 * @method \App\Model\Entity\Platforma newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Platforma[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Platforma|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Platforma patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Platforma[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Platforma findOrCreate($search, callable $callback = null, $options = [])
 */
class PlatformaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('platforma');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Towar', [
            'foreignKey' => 'platforma_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->allowEmpty('logo');

        $validator
            ->allowEmpty('logo_2');

        $validator
                ->boolean('preorder')
            ->allowEmpty('preorder');

        return $validator;
    }
}
