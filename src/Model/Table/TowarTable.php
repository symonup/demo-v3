<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Towar Model
 *
 * @property \App\Model\Table\ProducentsTable|\Cake\ORM\Association\BelongsTo $Producents
 * @property \App\Model\Table\KolorsTable|\Cake\ORM\Association\BelongsTo $Kolors
 * @property \App\Model\Table\WzorsTable|\Cake\ORM\Association\BelongsTo $Wzors
 * @property \App\Model\Table\KolekcjasTable|\Cake\ORM\Association\BelongsTo $Kolekcjas
 * @property \App\Model\Table\AllegroAukcjaTable|\Cake\ORM\Association\HasMany $AllegroAukcja
 * @property \App\Model\Table\KategoriaPromocjaTable|\Cake\ORM\Association\HasMany $KategoriaPromocja
 * @property \App\Model\Table\TowarAkcesoriaTable|\Cake\ORM\Association\HasMany $TowarAkcesoria
 * @property \App\Model\Table\TowarCenaTable|\Cake\ORM\Association\HasMany $TowarCena
 * @property \App\Model\Table\TowarOpiniaTable|\Cake\ORM\Association\HasMany $TowarOpinia
 * @property \App\Model\Table\TowarPodobneTable|\Cake\ORM\Association\HasMany $TowarPodobne
 * @property \App\Model\Table\TowarPolecaneTable|\Cake\ORM\Association\HasMany $TowarPolecane
 * @property \App\Model\Table\TowarUzytkownikRabatTable|\Cake\ORM\Association\HasMany $TowarUzytkownikRabat
 * @property \App\Model\Table\TowarWariantTable|\Cake\ORM\Association\HasMany $TowarWariant
 * @property \App\Model\Table\TowarZdjecieTable|\Cake\ORM\Association\HasMany $TowarZdjecie
 * @property \App\Model\Table\WariantCechaTable|\Cake\ORM\Association\HasMany $WariantCecha
 * @property \App\Model\Table\WariantCechaWartoscTable|\Cake\ORM\Association\HasMany $WariantCechaWartosc
 * @property \App\Model\Table\ZestawTable|\Cake\ORM\Association\HasMany $Zestaw
 * @property \App\Model\Table\ZestawTowarWymianaTable|\Cake\ORM\Association\HasMany $ZestawTowarWymiana
 * @property \App\Model\Table\AllegroZamowienieTable|\Cake\ORM\Association\BelongsToMany $AllegroZamowienie
 * @property \App\Model\Table\AtrybutTable|\Cake\ORM\Association\BelongsToMany $Atrybut
 * @property \App\Model\Table\FilmTable|\Cake\ORM\Association\BelongsToMany $Film
 * @property \App\Model\Table\I18nTable|\Cake\ORM\Association\BelongsToMany $I18n
 * @property \App\Model\Table\KategoriaTable|\Cake\ORM\Association\BelongsToMany $Kategoria
 * @property \App\Model\Table\ZamowienieTable|\Cake\ORM\Association\BelongsToMany $Zamowienie
 * @property \App\Model\Table\ZestawTable|\Cake\ORM\Association\BelongsToMany $Zestaw
 *
 * @method \App\Model\Entity\Towar get($primaryKey, $options = [])
 * @method \App\Model\Entity\Towar newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Towar[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Towar|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Towar patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Towar[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Towar findOrCreate($search, callable $callback = null, $options = [])
 */
class TowarTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('towar');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Producent', [
            'foreignKey' => 'producent_id'
        ]);
        $this->belongsTo('Platforma', [
            'foreignKey' => 'platforma_id'
        ]);
        $this->belongsTo('Wersja', [
            'foreignKey' => 'wersja_id'
        ]);
        $this->belongsTo('Kolor', [
            'foreignKey' => 'kolor_id'
        ]);
        $this->belongsTo('Wzor', [
            'foreignKey' => 'wzor_id'
        ]);
        $this->belongsTo('Bonus', [
            'foreignKey' => 'bonus_id'
        ]);
        $this->belongsTo('Kolekcja', [
            'foreignKey' => 'kolekcja_id'
        ]);
        $this->hasMany('AllegroAukcja', [
            'foreignKey' => 'towar_id'
        ]);
        $this->hasMany('KategoriaPromocja', [
            'foreignKey' => 'towar_id'
        ]);
        $this->hasMany('TowarAkcesoria', [
            'foreignKey' => 'towar_id'
        ]);
        $this->hasMany('TowarCena', [
            'foreignKey' => 'towar_id'
        ]);
        $this->hasOne('TowarCenaDefault', [
            'className'=>'TowarCena',
            'foreignKey' => 'towar_id',
            'conditions'=>['TowarCenaDefault.uzytkownik_id IS'=>null]
        ]);
        $this->hasMany('TowarOpinia', [
            'foreignKey' => 'towar_id'
        ]);
        $this->hasMany('TowarPodobne', [
            'foreignKey' => 'towar_id'
        ]);
        $this->hasMany('TowarPolecane', [
            'foreignKey' => 'towar_id'
        ]);
        $this->hasMany('TowarKategoria', [
            'foreignKey' => 'towar_id'
        ]);
        $this->hasMany('TowarGatunek', [
            'foreignKey' => 'towar_id'
        ]);
        $this->hasMany('TowarUzytkownikRabat', [
            'foreignKey' => 'towar_id'
        ]);
        $this->hasMany('TowarWariant', [
            'foreignKey' => 'towar_id'
        ]);
        $this->hasMany('TowarAtrybut', [
            'foreignKey' => 'towar_id'
        ]);
        $this->hasMany('TowarZdjecie', [
            'foreignKey' => 'towar_id'
        ]);
        $this->hasOne('Zdjecie', [
            'className'=>'TowarZdjecie',
            'foreignKey' => 'towar_id',
            'order'=>['Zdjecie.domyslne'=>'desc','Zdjecie.kolejnosc'=>'desc'],
            'conditions'=>['Zdjecie.domyslne'=>1]
        ]);
        $this->hasMany('WariantCecha', [
            'foreignKey' => 'towar_id'
        ]);
        $this->hasMany('WariantCechaWartosc', [
            'foreignKey' => 'towar_id'
        ]);
        $this->hasMany('Zestaw', [
            'foreignKey' => 'towar_id'
        ]);
        $this->hasMany('ZestawTowarWymiana', [
            'foreignKey' => 'towar_id'
        ]);
        $this->belongsToMany('Akcesoria', [
            'foreignKey' => 'towar_id',
            'targetForeignKey' => 'podobny_id',
            'joinTable' => 'towar_akcesoria'
        ]);
        $this->belongsToMany('Podobne', [
            'foreignKey' => 'towar_id',
            'targetForeignKey' => 'podobny_id',
            'joinTable' => 'towar_podobne'
        ]);
        $this->belongsToMany('Polecane', [
            'foreignKey' => 'towar_id',
            'targetForeignKey' => 'podobny_id',
            'joinTable' => 'towar_polecane'
        ]);
        $this->belongsToMany('Atrybut', [
            'foreignKey' => 'towar_id',
            'targetForeignKey' => 'atrybut_id',
            'joinTable' => 'towar_atrybut'
        ]);
        $this->hasMany('TowarFilm', [
            'foreignKey' => 'towar_id',
        ]);
        $this->belongsToMany('Kategoria', [
            'foreignKey' => 'towar_id',
            'targetForeignKey' => 'kategoria_id',
            'joinTable' => 'towar_kategoria'
        ]);
        $this->belongsToMany('Gatunek', [
            'foreignKey' => 'towar_id',
            'targetForeignKey' => 'gatunek_id',
            'joinTable' => 'towar_gatunek'
        ]);
        $this->belongsToMany('Zamowienie', [
            'foreignKey' => 'towar_id',
            'targetForeignKey' => 'zamowienie_id',
            'joinTable' => 'zamowienie_towar'
        ]);
        $this->belongsToMany('Zestaw', [
            'foreignKey' => 'towar_id',
            'targetForeignKey' => 'zestaw_id',
            'joinTable' => 'zestaw_towar'
        ]);
        $this->hasOne('HotDeal', [
            'foreignKey' => 'towar_id',
            'conditions'=>['HotDeal.ilosc >'=>0,'HotDeal.aktywna'=>1,'HotDeal.data_do >='=>date('Y-m-d H:i:s'),'(HotDeal.data_od IS NULL OR HotDeal.data_od <= \''.date('Y-m-d H:i:s').'\')']
        ]);
        $this->hasOne('HotDealEdit', [
            'className'=>'HotDeal',
            'foreignKey' => 'towar_id',
        ]);
        $this->belongsToMany('Certyfikat', [
            'foreignKey' => 'towar_id',
            'targetForeignKey' => 'certyfikat_id',
            'joinTable' => 'towar_certyfikat'
        ]);
        $this->belongsTo('Wiek', [
            'foreignKey' => 'wiek_id'
        ]);
        $this->belongsTo('Opakowanie', [
            'foreignKey' => 'opakowanie_id'
        ]);
        $this->belongsToMany('Okazje', [
            'foreignKey' => 'towar_id',
            'targetForeignKey' => 'okazje_id',
            'joinTable' => 'okazje_towar'
        ]);
        $this->addBehavior('Translate', ['fields' => ['nazwa', 'opis', 'opis2', 'seo_tytul', 'seo_slowa', 'seo_opis', 'zestaw_nazwa'], 'translationTable' => 'TowarI18n']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->allowEmpty('kod');

        $validator
                ->integer('ilosc')
                ->requirePresence('ilosc', 'create')
                ->notEmpty('ilosc');

        $validator
                ->boolean('ukryty')
                ->allowEmpty('ukryty');

        $validator
                ->boolean('promocja')
                ->allowEmpty('promocja');

        $validator
                ->boolean('polecany')
                ->allowEmpty('polecany');

        $validator
                ->boolean('wyprzedaz')
                ->allowEmpty('wyprzedaz');

        $validator
                ->boolean('wyrozniony')
                ->allowEmpty('wyrozniony');

        $validator
                ->boolean('nowosc')
                ->allowEmpty('nowosc');

        $validator
                ->dateTime('best_of_the_week')
                ->allowEmpty('best_of_the_week');

        $validator
                ->allowEmpty('gwarancja_producenta');

        $validator
                ->boolean('bez_konfekcji')
                ->allowEmpty('bez_konfekcji');

        $validator
                ->requirePresence('nazwa', 'create')
                ->notEmpty('nazwa');

        $validator
                ->allowEmpty('opis');

        $validator
                ->allowEmpty('opis2');

        $validator
                ->allowEmpty('hint_cena');

        $validator
                ->allowEmpty('seo_tytul');

        $validator
                ->allowEmpty('seo_slowa');

        $validator
                ->allowEmpty('seo_opis');

        $validator
                ->integer('opinia_avg')
                ->allowEmpty('opinia_avg');

        $validator
                ->allowEmpty('zestaw_nazwa');

        $validator
                ->boolean('wyklucz_z_gm')
                ->allowEmpty('wyklucz_z_gm');

        $validator
                ->integer('wariant')
                ->allowEmpty('wariant');

        $validator
                ->integer('na_zamowienie')
                ->allowEmpty('na_zamowienie');

        $validator
                ->allowEmpty('subiekt_kod');

        $validator
                ->allowEmpty('ean');

        $validator
                ->allowEmpty('tagi');

        $validator
                ->integer('ilosc_w_kartonie')
                ->notEmpty('ilosc_w_kartonie');
        $validator
                ->numeric('waga_kartonu')
                ->notEmpty('waga_kartonu');
        $validator
                ->numeric('objetosc_kartonu')
                ->notEmpty('objetosc_kartonu');
        $validator
                ->integer('kolejnosc')
                ->allowEmpty('kolejnosc');

        $validator
                ->boolean('mix')
                ->allowEmpty('mix');
        $validator
                ->numeric('waga')
                ->allowEmpty('waga');
        $validator
                ->numeric('opinia_avg')
                ->allowEmpty('opinia_avg');
        $validator
                ->integer('max_ilosc')
                ->allowEmpty('max_ilosc');
        $validator
                ->allowEmpty('wysylka_info_dostepny');
        $validator
                ->allowEmpty('wysylka_info_niedostepny');
        $validator
                ->boolean('produkt_tygodnia')
                ->allowEmpty('produkt_tygodnia');
        $validator
                ->allowEmpty('nazwa_promocja');
        $validator
                ->dateTime('data_utworzenia')
                ->allowEmpty('data_utworzenia');
        $validator
                ->dateTime('data_modyfikacji')
                ->allowEmpty('data_modyfikacji');
        $validator
                ->dateTime('data_pobrania')
                ->allowEmpty('data_pobrania');
        $validator
                ->allowEmpty('pegi_wiek');
        $validator
                ->allowEmpty('pegi_typ');
        $validator
                ->date('data_premiery')
                ->allowEmpty('data_premiery');
        $validator
                ->boolean('preorder')
                ->allowEmpty('preorder');
        $validator
                ->dateTime('preorder_od')
                ->allowEmpty('preorder_od');
        $validator
                ->dateTime('preorder_do')
                ->allowEmpty('preorder_do');
        $validator
                ->allowEmpty('preorder_limit');
        
        $validator
                ->boolean('p_skapiec')
                ->allowEmpty('p_skapiec');
        
        $validator
                ->boolean('p_google')
                ->allowEmpty('p_google');
        
        $validator
                ->boolean('p_ceneo')
                ->allowEmpty('p_ceneo');
        $validator
                ->allowEmpty('ilosc_log');
        $validator
                ->allowEmpty('hurtownia');
        $validator
                ->dateTime('get_date')
                ->allowEmpty('get_date');
        $validator
                ->allowEmpty('hurt_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['producent_id'], 'Producent'));
        $rules->add($rules->existsIn(['platforma_id'], 'Platforma'));
        $rules->add($rules->existsIn(['wersja_id'], 'Wersja'));
        $rules->add($rules->existsIn(['kolor_id'], 'Kolor'));
        $rules->add($rules->existsIn(['wzor_id'], 'Wzor'));
        $rules->add($rules->existsIn(['kolekcja_id'], 'Kolekcja'));

        return $rules;
    }

    public function getWariantsToCart($user, $itemId, $wariantyIds) {
       
            $towarCenaCond = [];
        
        $item = $this->find('all', [
                    'conditions' => ['Towar.id' => $itemId, 'Towar.ukryty' => 0],
                    'contain' => [
                        'TowarWariant' => ['conditions'=>['TowarWariant.id IN'=>$wariantyIds],'Kolor','Rozmiar'],
//                        'TowarAtrybut' => ['conditions' => ['TowarAtrybut.id IN' => $wariantyIds], 'TowarZdjecie' => ['sort' => ['TowarZdjecie.kolejnosc' => 'desc']], 'Atrybut' => ['AtrybutPodrzedne'], 'AtrybutPodrzedne'],
                        'TowarZdjecie' => ['conditions' => ['TowarZdjecie.domyslne' => 1]],
                        'HotDeal',
//                        'Bonus',
//                        'Platforma',
                        'Kategoria',
                        'TowarCenaDefault' => ['Vat', 'Waluta', 'Jednostka', 'conditions' => $towarCenaCond],
                    ]
                ])->first();
        return $item;
    }

    public function getCategoryNameByTowarId($towarId,$categoryId=null){
        if(empty($towarId)){
            return '';
        }
        $item = $this->find('all',['contain'=>['Kategoria']])->where(['Towar.id'=>$towarId])->first();
        if(!empty($item)){
            if(!empty($item->kategoria)){
                $categoryName = '';
                foreach ($item->kategoria as $itemKat){
                    $categoryName=$itemKat->nazwa;
                    if(empty($categoryId)){
                        break;
                    }elseif($categoryId==$itemKat->id){
                        break;
                    }
                }
                return $categoryName;
            }else{
                return '';
            }
        }else{
            return '';
        }
    }
}
