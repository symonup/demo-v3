<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ZamowienieTowar Model
 *
 * @property \App\Model\Table\ZamowienieTable|\Cake\ORM\Association\BelongsTo $Zamowienie
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\BelongsTo $Towar
 * @property |\Cake\ORM\Association\BelongsTo $TowarAtrybut
 * @property |\Cake\ORM\Association\BelongsTo $Jednostka
 * @property |\Cake\ORM\Association\BelongsTo $Vat
 *
 * @method \App\Model\Entity\ZamowienieTowar get($primaryKey, $options = [])
 * @method \App\Model\Entity\ZamowienieTowar newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ZamowienieTowar[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ZamowienieTowar|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ZamowienieTowar patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ZamowienieTowar[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ZamowienieTowar findOrCreate($search, callable $callback = null, $options = [])
 */
class ZamowienieTowarTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('zamowienie_towar');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Zamowienie', [
            'foreignKey' => 'zamowienie_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Towar', [
            'foreignKey' => 'towar_id'
        ]);
        $this->belongsTo('TowarAtrybut', [
            'foreignKey' => 'wariant_id'
        ]);
        $this->belongsTo('Jednostka', [
            'foreignKey' => 'jednostka_id'
        ]);
        $this->belongsTo('Vat', [
            'foreignKey' => 'vat_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('ilosc')
            ->requirePresence('ilosc', 'create')
            ->notEmpty('ilosc');

        $validator
            ->integer('ilosc_w_kartonie')
            ->allowEmpty('ilosc_w_kartonie');

        $validator
            ->numeric('cena_za_sztuke_netto')
            ->requirePresence('cena_za_sztuke_netto', 'create')
            ->notEmpty('cena_za_sztuke_netto');

        $validator
            ->numeric('cena_za_sztuke_brutto')
            ->requirePresence('cena_za_sztuke_brutto', 'create')
            ->notEmpty('cena_za_sztuke_brutto');

        $validator
            ->numeric('cena_razem_netto')
            ->requirePresence('cena_razem_netto', 'create')
            ->notEmpty('cena_razem_netto');

        $validator
            ->numeric('cena_razem_brutto')
            ->requirePresence('cena_razem_brutto', 'create')
            ->notEmpty('cena_razem_brutto');

        $validator
            ->allowEmpty('kod');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->numeric('vat_stawka')
            ->allowEmpty('vat_stawka');
        $validator
            ->allowEmpty('jednostka_str');
        $validator
            ->numeric('objetosc_kartonu')
            ->allowEmpty('objetosc_kartonu');
        $validator
            ->numeric('waga_kartonu')
            ->allowEmpty('waga_kartonu');
        $validator
                ->numeric('rabat')
                ->allowEmpty('rabat');
        $validator
            ->numeric('cena_podstawowa')
            ->allowEmpty('cena_podstawowa');
        $validator
            ->numeric('cena_za_sztuke_netto')
            ->allowEmpty('cena_za_sztuke_netto');
        $validator
            ->numeric('cena_za_sztuke_brutto')
            ->allowEmpty('cena_za_sztuke_brutto');
        $validator
                ->boolean('mix')
                ->allowEmpty('mix');
        $validator
                ->numeric('cena_z_rabatem')
                ->allowEmpty('cena_z_rabatem');
        $validator
                ->numeric('cena_razem_z_rabatem')
                ->allowEmpty('cena_razem_z_rabatem');
        $validator
                ->allowEmpty('bonusowe_zlotowki');
        $validator
                ->allowEmpty('karta_podarunkowa_id');
        $validator
                ->allowEmpty('karta_podarunkowa_wartosc');
        $validator
                ->allowEmpty('karta_podarunkowa_kod');
        $validator
                ->allowEmpty('hurtownia');
        $validator
                ->boolean('ws_long')
                ->allowEmpty('ws_long');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['zamowienie_id'], 'Zamowienie'));
        $rules->add($rules->existsIn(['towar_id'], 'Towar'));
        $rules->add($rules->existsIn(['wariant_id'], 'TowarAtrybut'));
        $rules->add($rules->existsIn(['jednostka_id'], 'Jednostka'));
        $rules->add($rules->existsIn(['vat_id'], 'Vat'));

        return $rules;
    }
}
