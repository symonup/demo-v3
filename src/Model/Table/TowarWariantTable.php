<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TowarWariant Model
 *
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\BelongsTo $Towar
 * @property \App\Model\Table\RozmiarTable|\Cake\ORM\Association\BelongsTo $Rozmiar
 * @property \App\Model\Table\KolorTable|\Cake\ORM\Association\BelongsTo $Kolor
 *
 * @method \App\Model\Entity\TowarWariant get($primaryKey, $options = [])
 * @method \App\Model\Entity\TowarWariant newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TowarWariant[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TowarWariant|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TowarWariant patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TowarWariant[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TowarWariant findOrCreate($search, callable $callback = null, $options = [])
 */
class TowarWariantTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('towar_wariant');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Towar', [
            'foreignKey' => 'towar_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Rozmiar', [
            'foreignKey' => 'rozmiar_id'
        ]);
        $this->belongsTo('Kolor', [
            'foreignKey' => 'kolor_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('ilosc')
            ->allowEmpty('ilosc');

        $validator
            ->allowEmpty('ean');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['towar_id'], 'Towar'));
        $rules->add($rules->existsIn(['rozmiar_id'], 'Rozmiar'));
        $rules->add($rules->existsIn(['kolor_id'], 'Kolor'));

        return $rules;
    }
}
