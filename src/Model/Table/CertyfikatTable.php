<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Certyfikat Model
 *
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\BelongsToMany $Towar
 *
 * @method \App\Model\Entity\Certyfikat get($primaryKey, $options = [])
 * @method \App\Model\Entity\Certyfikat newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Certyfikat[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Certyfikat|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Certyfikat patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Certyfikat[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Certyfikat findOrCreate($search, callable $callback = null, $options = [])
 */
class CertyfikatTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('certyfikat');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsToMany('Towar', [
            'foreignKey' => 'certyfikat_id',
            'targetForeignKey' => 'towar_id',
            'joinTable' => 'towar_certyfikat'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('nazwa');

        $validator
            ->requirePresence('skrot', 'create')
            ->notEmpty('skrot');

        return $validator;
    }
}
