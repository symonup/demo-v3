<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * KategoriaMap Model
 *
 * @property \App\Model\Table\KategoriaTable|\Cake\ORM\Association\BelongsTo $Kategoria
 *
 * @method \App\Model\Entity\KategoriaMap get($primaryKey, $options = [])
 * @method \App\Model\Entity\KategoriaMap newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\KategoriaMap[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\KategoriaMap|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\KategoriaMap patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\KategoriaMap[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\KategoriaMap findOrCreate($search, callable $callback = null, $options = [])
 */
class KategoriaMapTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('kategoria_map');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Kategoria', [
            'foreignKey' => 'kategoria_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('hurtownia', 'create')
            ->notEmpty('hurtownia');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['kategoria_id'], 'Kategoria'));

        return $rules;
    }
}
