<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * KodyRabatoweProducent Model
 *
 * @property \App\Model\Table\ProducentTable|\Cake\ORM\Association\BelongsTo $Producent
 * @property \App\Model\Table\KodyRabatoweTable|\Cake\ORM\Association\BelongsTo $KodyRabatowe
 *
 * @method \App\Model\Entity\KodyRabatoweProducent get($primaryKey, $options = [])
 * @method \App\Model\Entity\KodyRabatoweProducent newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\KodyRabatoweProducent[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\KodyRabatoweProducent|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\KodyRabatoweProducent patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\KodyRabatoweProducent[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\KodyRabatoweProducent findOrCreate($search, callable $callback = null, $options = [])
 */
class KodyRabatoweProducentTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('kody_rabatowe_producent');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Producent', [
            'foreignKey' => 'producent_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('KodyRabatowe', [
            'foreignKey' => 'kody_rabatowe_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['producent_id'], 'Producent'));
        $rules->add($rules->existsIn(['kody_rabatowe_id'], 'KodyRabatowe'));

        return $rules;
    }
}
