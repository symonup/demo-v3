<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Blok Model
 *
 * @method \App\Model\Entity\Blok get($primaryKey, $options = [])
 * @method \App\Model\Entity\Blok newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Blok[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Blok|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Blok patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Blok[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Blok findOrCreate($search, callable $callback = null, $options = [])
 */
class BlokTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('blok');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->boolean('ukryty')
            ->requirePresence('ukryty', 'create')
            ->notEmpty('ukryty');

        $validator
            ->requirePresence('tresc', 'create')
            ->notEmpty('tresc');
        $validator
            ->notEmpty('locale');

        return $validator;
    }
}
