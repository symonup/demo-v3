<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TowarZdjecie Model
 *
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\BelongsTo $Towar
 *
 * @method \App\Model\Entity\TowarZdjecie get($primaryKey, $options = [])
 * @method \App\Model\Entity\TowarZdjecie newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TowarZdjecie[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TowarZdjecie|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TowarZdjecie patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TowarZdjecie[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TowarZdjecie findOrCreate($search, callable $callback = null, $options = [])
 */
class TowarZdjecieTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('towar_zdjecie');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Towar', [
            'foreignKey' => 'towar_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('plik');

        $validator
            ->integer('kolejnosc')
            ->allowEmpty('kolejnosc');

        $validator
            ->boolean('domyslne')
            ->allowEmpty('domyslne');

        $validator
            ->boolean('techniczny')
            ->allowEmpty('techniczny');

        $validator
            ->allowEmpty('alt');

        $validator
            ->integer('znak_wodny')
            ->allowEmpty('znak_wodny');

        $validator
            ->boolean('ukryte')
            ->allowEmpty('ukryte');

        $validator
            ->allowEmpty('orginal');
        $validator
            ->allowEmpty('wariant_id');
        $validator
            ->allowEmpty('remote_url');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['towar_id'], 'Towar'));

        return $rules;
    }
}
