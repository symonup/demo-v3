<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UzytkownikKoszyk Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $Uzytkowniks
 *
 * @method \App\Model\Entity\UzytkownikKoszyk get($primaryKey, $options = [])
 * @method \App\Model\Entity\UzytkownikKoszyk newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UzytkownikKoszyk[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UzytkownikKoszyk|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UzytkownikKoszyk patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UzytkownikKoszyk[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UzytkownikKoszyk findOrCreate($search, callable $callback = null, $options = [])
 */
class UzytkownikKoszykTable extends Table {

    private $Txt;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('uzytkownik_koszyk');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Uzytkownik', [
            'foreignKey' => 'uzytkownik_id',
            'joinType' => 'INNER'
        ]);
        $View = new \Cake\View\View();
        $this->Txt = new \App\View\Helper\TxtHelper($View);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->allowEmpty('items');

        $validator
                ->boolean('active')
                ->allowEmpty('active');

        $validator
                ->dateTime('create_date')
                ->allowEmpty('create_date');

        $validator
                ->dateTime('last_update')
                ->allowEmpty('last_update');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['uzytkownik_id'], 'Uzytkownik'));

        return $rules;
    }

    public function prepareCart($item = null, $warianty = [], $rabat = null,$gratis=false) {
        if (empty($item)) {
            return [];
        }
        $itemsToAdd = [];
        $specialPrice=false;
        $priceDefault = round($this->Txt->itemPriceDef($item),2);
        $priceByItem=round($this->Txt->itemPrice($item),2);
        $itemPrice=$this->Txt->itemPriceArr($item);
        if(empty($itemPrice)){
            return false;
        }
        if(!empty($rabat)){
            $priceDefault=$priceByItem;
            $priceByItem=round($priceByItem-($priceByItem * ($rabat/100)),2);
        }
        if($gratis){
            $priceDefault=0;
            $priceByItem=0;
        }
        if (!empty($item->towar_wariant)) {
            foreach ($item->towar_wariant as $towarWariant) {
                $ilosc = $warianty[$item->id . '_' . $towarWariant->id];
                if (empty($ilosc) || $ilosc == '0' || empty($towarWariant->ilosc)) {
                    continue;
                }
                $priceRazem = round($priceByItem * $ilosc, 2);
                $itemsToAdd[($gratis?'gratis':$item->id . '_' . $towarWariant->id)] = [
                    'ilosc' => $ilosc,
                    'cena_za_sztuke' => $priceByItem,
                    'cena_podstawowa' => $priceDefault,
                    'cena_za_sztuke_netto' => $this->Txt->cenaVat($priceByItem, $itemPrice['vat'], \Cake\Core\Configure::read('ceny.rodzaj'), 'netto', false),
                    'cena_za_sztuke_brutto' => $this->Txt->cenaVat($priceByItem, $itemPrice['vat'], \Cake\Core\Configure::read('ceny.rodzaj'), 'brutto', false),
                    'cena_razem' => $priceRazem,
                    'cena_razem_netto' => $this->Txt->cenaVat($priceRazem, $itemPrice['vat'], \Cake\Core\Configure::read('ceny.rodzaj'), 'netto', false),
                    'cena_razem_brutto' => $this->Txt->cenaVat($priceRazem, $itemPrice['vat'], \Cake\Core\Configure::read('ceny.rodzaj'), 'brutto', false),
                    'kod' => $item->kod,
                    'nazwa' => $item->nazwa . (!empty($towarWariant->kolor)?' '.$towarWariant->kolor->nazwa:'').(!empty($towarWariant->rozmiar)?' '.$towarWariant->rozmiar->nazwa:''),
                    'towar_id' => $item->id,
                    'wariant_id' => $towarWariant->id,
                    'jednostka_id' => $itemPrice['jednostka']['id'],
                    'jednostka_str' => (!empty($itemPrice['jednostka']['id']) ? $itemPrice['jednostka']['jednostka']['jednostka'] : ''),
                    'vat_id' => $itemPrice['vat_id'],
                    'vat_stawka' => $itemPrice['vat'],
                    'bonusowe_zlotowki'=>(!empty($item->bonus)?$item->bonus->wartosc:0),
                    'waga' => $item->waga,
                    'w_promocji'=>((!empty($item->promocja) || !empty($item->wyrozniony) || !empty($item->wyprzedaz) || $specialPrice)?true:false),
                    'towar_ilosc'=>$item->ilosc,
                    'producent_id'=>$item->producent_id,
                    'kategoria_id'=>(!empty($item->kategoria)?$item->kategoria[0]->id:null),
                    'hot_deal'=>(!empty($item->hot_deal)?$item->hot_deal->ilosc:null),
                    'hurtownia'=>$item->hurtownia,
                    'gratis'=>$gratis,
                    'gratis_wartosc'=>($gratis?$item->gratis_wartosc:null)
                ];
                $zdjecie = [];
                
                    if (!empty($item->towar_zdjecie)) {
                        $zdjecie = ['id' => $item->towar_zdjecie[0]->id, 'plik' => $item->towar_zdjecie[0]->plik];
                    }
                
                if (!empty($zdjecie)) {
                    $itemsToAdd[($gratis?'gratis':$item->id . '_' . $towarWariant->id)]['zdjecie'] = $zdjecie;
                }
            }
        } else {
            $ilosc = $warianty[$item->id];
            if (!empty($ilosc) && $ilosc != '0') {
                $priceRazem = round($priceByItem * $ilosc, 2);
                $itemsToAdd[($gratis?'gratis':$item->id)] = [
                    'ilosc' => $ilosc,
                    'cena_za_sztuke' => $priceByItem,
                    'cena_podstawowa' => $priceDefault,
                    'cena_za_sztuke_netto' => $this->Txt->cenaVat($priceByItem, $itemPrice['vat'], \Cake\Core\Configure::read('ceny.rodzaj'), 'netto', false),
                    'cena_za_sztuke_brutto' => $this->Txt->cenaVat($priceByItem, $itemPrice['vat'], \Cake\Core\Configure::read('ceny.rodzaj'), 'brutto', false),
                    'cena_razem' => $priceRazem,
                    'cena_razem_netto' => $this->Txt->cenaVat($priceRazem, $itemPrice['vat'], \Cake\Core\Configure::read('ceny.rodzaj'), 'netto', false),
                    'cena_razem_brutto' => $this->Txt->cenaVat($priceRazem, $itemPrice['vat'], \Cake\Core\Configure::read('ceny.rodzaj'), 'brutto', false),
                    'kod' => $item->kod,
                    'nazwa' => $item->nazwa,
                    'towar_id' => $item->id,
                    'jednostka_id' => $itemPrice['jednostka']['id'],
                    'jednostka_str' => (!empty($itemPrice['jednostka']['id']) ? $itemPrice['jednostka']['jednostka']['jednostka'] : ''),
                    'vat_id' => $itemPrice['vat_id'],
                    'vat_stawka' => $itemPrice['vat'],
                    'bonusowe_zlotowki'=>(!empty($item->bonus)?$item->bonus->wartosc:0),
                    'waga' => $item->waga,
                    'w_promocji'=>((!empty($item->promocja) || !empty($item->wyrozniony) || !empty($item->wyprzedaz) || $specialPrice)?true:false),
                    'towar_ilosc'=>$item->ilosc,
                    'producent_id'=>$item->producent_id,
                    'kategoria_id'=>(!empty($item->kategoria)?$item->kategoria[0]->id:null),
                    'hot_deal'=>(!empty($item->hot_deal)?$item->hot_deal->ilosc:null),
                    'hurtownia'=>$item->hurtownia,
                    'gratis'=>$gratis,
                    'gratis_wartosc'=>($gratis?$item->gratis_wartosc:null)
                ];
                if (!empty($item->towar_zdjecie)) {
                    $itemsToAdd[($gratis?'gratis':$item->id)]['zdjecie'] = ['id' => $item->towar_zdjecie[0]->id, 'plik' => $item->towar_zdjecie[0]->plik];
                }
            }
        }
        return $itemsToAdd;
    }

    public function prepareCartBonus($item = null,$ilosc=1) {
        if (empty($item)) {
            return [];
        }
        $itemsToAdd = [];
        $priceDefault = round($item->cena,2);
        $priceByItem=round($item->cena,2);
        
            if (!empty($ilosc) && $ilosc != '0') {
                $priceRazem = round($priceByItem * $ilosc, 2);
                $itemsToAdd['card_'.$item->id] = [
                    'ilosc' => $ilosc,
                    'cena_za_sztuke' => $priceByItem,
                    'cena_podstawowa' => $priceDefault,
                    'cena_za_sztuke_netto' => $priceByItem,
                    'cena_za_sztuke_brutto' => $priceByItem,
                    'cena_razem' => $priceRazem,
                    'cena_razem_netto' => $priceRazem,
                    'cena_razem_brutto' => $priceRazem,
                    'nazwa' => $item->nazwa,
                    'karta_podarunkowa_id' => $item->id,
                    'karta_podarunkowa_wartosc' => $item->wartosc,
                    'jednostka_id' => null,
                    'jednostka_str' => 'szt.',
                    'vat_id' => null,
                    'vat_stawka' => 0,
                    'karta_podarunkowa'=>true
                ];
                if (!empty($item->zdjecie)) {
                    $itemsToAdd['card_'.$item->id]['zdjecie'] = $item->zdjecie;
                }
            }
        return $itemsToAdd;
    }
}
