<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BannerSlidesElements Model
 *
 * @property \App\Model\Table\BannerSlidesTable|\Cake\ORM\Association\BelongsTo $BannerSlides
 *
 * @method \App\Model\Entity\BannerSlidesElement get($primaryKey, $options = [])
 * @method \App\Model\Entity\BannerSlidesElement newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\BannerSlidesElement[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\BannerSlidesElement|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BannerSlidesElement patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\BannerSlidesElement[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\BannerSlidesElement findOrCreate($search, callable $callback = null, $options = [])
 */
class BannerSlidesElementsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('banner_slides_elements');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->belongsTo('BannerSlides', [
            'foreignKey' => 'banner_slides_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->allowEmpty('file');

        $validator
            ->allowEmpty('text');

        $validator
            ->allowEmpty('title');

        $validator
            ->boolean('tooltip')
            ->allowEmpty('tooltip');

        $validator
            ->allowEmpty('pos_left');

        $validator
            ->allowEmpty('pos_top');

        $validator
            ->allowEmpty('position');

        $validator
            ->allowEmpty('css');

        $validator
            ->allowEmpty('link');

        $validator
            ->allowEmpty('target');

        $validator
            ->integer('is_linked')
            ->allowEmpty('is_linked');

        $validator
            ->integer('_width')
            ->allowEmpty('_width');

        $validator
            ->integer('_height')
            ->allowEmpty('_height');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['banner_slides_id'], 'BannerSlides'));

        return $rules;
    }
}
