<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Bonus Model
 *
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\HasMany $Towar
 *
 * @method \App\Model\Entity\Bonus get($primaryKey, $options = [])
 * @method \App\Model\Entity\Bonus newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Bonus[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Bonus|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Bonus patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Bonus[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Bonus findOrCreate($search, callable $callback = null, $options = [])
 */
class BonusTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('bonus');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Towar', [
            'foreignKey' => 'bonus_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->numeric('wartosc')
            ->requirePresence('wartosc', 'create')
            ->notEmpty('wartosc');

        return $validator;
    }
}
