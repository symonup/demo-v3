<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Koszyk Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Sessions
 * @property \Cake\ORM\Association\BelongsTo $Uzytkowniks
 * @property \Cake\ORM\Association\BelongsTo $Zamowienies
 * @property \Cake\ORM\Association\BelongsToMany $Towar
 *
 * @method \App\Model\Entity\Koszyk get($primaryKey, $options = [])
 * @method \App\Model\Entity\Koszyk newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Koszyk[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Koszyk|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Koszyk patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Koszyk[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Koszyk findOrCreate($search, callable $callback = null)
 */
class KoszykTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('koszyk');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Uzytkownik', [
            'foreignKey' => 'uzytkownik_id'
        ]);
        $this->belongsTo('Zamowienie', [
            'foreignKey' => 'zamowienie_id'
        ]);
        $this->hasMany('KoszykTowar', [
            'foreignKey' => 'koszyk_id'
        ]);
        $this->hasMany('KoszykTowarUsuniete', [
            'foreignKey' => 'koszyk_id',
            'className'=>'KoszykTowar',
            'conditions'=>['usuniety'=>1]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('data')
            ->requirePresence('data', 'create')
            ->notEmpty('data');

        $validator
            ->dateTime('last_update')
            ->allowEmpty('last_update');

        $validator
            ->boolean('aktywny')
            ->allowEmpty('aktywny');
        $validator
            ->allowEmpty('domena');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['uzytkownik_id'], 'Uzytkownik'));
        $rules->add($rules->existsIn(['zamowienie_id'], 'Zamowienie'));

        return $rules;
    }
}
