<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TowarCena Model
 *
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\BelongsTo $Towar
 * @property \App\Model\Table\JednostkaTable|\Cake\ORM\Association\BelongsTo $Jednostka
 * @property \App\Model\Table\VatTable|\Cake\ORM\Association\BelongsTo $Vat
 *
 * @method \App\Model\Entity\TowarCena get($primaryKey, $options = [])
 * @method \App\Model\Entity\TowarCena newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TowarCena[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TowarCena|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TowarCena patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TowarCena[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TowarCena findOrCreate($search, callable $callback = null, $options = [])
 */
class TowarCenaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('towar_cena');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Towar', [
            'foreignKey' => 'towar_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Uzytkownik', [
            'foreignKey' => 'uzytkownik_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Waluta', [
            'foreignKey' => 'waluta_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Jednostka', [
            'foreignKey' => 'jednostka_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Vat', [
            'foreignKey' => 'vat_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->numeric('waga')
            ->allowEmpty('waga');

        $validator
            ->decimal('cena_katalogowa')
            ->allowEmpty('cena_katalogowa');

        $validator
            ->decimal('cena_sprzedazy')
            ->requirePresence('cena_sprzedazy', 'create')
            ->notEmpty('cena_sprzedazy');

        $validator
            ->decimal('cena_promocja')
            ->allowEmpty('cena_promocja');

        $validator
            ->numeric('wartosc_minimalna')
            ->allowEmpty('wartosc_minimalna');

        $validator
            ->integer('wartosc_minimalna_count')
            ->allowEmpty('wartosc_minimalna_count');

        $validator
            ->allowEmpty('rodzaj');
        $validator
            ->allowEmpty('waluta_id');
        $validator
                ->allowEmpty('uzytkownik_id');
        $validator
                ->boolean('aktywna')
                ->allowEmpty('aktywna');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['towar_id'], 'Towar'));
        $rules->add($rules->existsIn(['jednostka_id'], 'Jednostka'));
        $rules->add($rules->existsIn(['vat_id'], 'Vat'));
        $rules->add($rules->existsIn(['waluta_id'], 'Waluta'));

        return $rules;
    }
}
