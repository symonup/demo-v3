<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Rodo Model
 *
 * @method \App\Model\Entity\Rodo get($primaryKey, $options = [])
 * @method \App\Model\Entity\Rodo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Rodo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Rodo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Rodo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Rodo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Rodo findOrCreate($search, callable $callback = null, $options = [])
 */
class RodoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('rodo');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->allowEmpty('token');

        $validator
            ->dateTime('data_zatwierdzenia')
            ->allowEmpty('data_zatwierdzenia');

        $validator
            ->integer('zgoda_1')
            ->allowEmpty('zgoda_1');

        $validator
            ->integer('zgoda_2')
            ->allowEmpty('zgoda_2');

        $validator
            ->integer('zgoda_3')
            ->allowEmpty('zgoda_3');

        $validator
            ->integer('zgoda_4')
            ->allowEmpty('zgoda_4');

        $validator
            ->integer('zgoda_5')
            ->allowEmpty('zgoda_5');

        $validator
            ->integer('zgoda_6')
            ->allowEmpty('zgoda_6');

        $validator
            ->integer('zgoda_7')
            ->allowEmpty('zgoda_7');

        $validator
            ->integer('zgoda_8')
            ->allowEmpty('zgoda_8');

        $validator
            ->allowEmpty('telefon');

        $validator
            ->integer('wyslany')
            ->allowEmpty('wyslany');

        $validator
            ->dateTime('data_wyslania')
            ->allowEmpty('data_wyslania');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
}
