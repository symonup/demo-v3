<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Vat Model
 *
 * @property \App\Model\Table\TowarCenaTable|\Cake\ORM\Association\HasMany $TowarCena
 * @property \App\Model\Table\WysylkaTable|\Cake\ORM\Association\HasMany $Wysylka
 *
 * @method \App\Model\Entity\Vat get($primaryKey, $options = [])
 * @method \App\Model\Entity\Vat newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Vat[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Vat|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Vat patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Vat[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Vat findOrCreate($search, callable $callback = null, $options = [])
 */
class VatTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('vat');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('TowarCena', [
            'foreignKey' => 'vat_id'
        ]);
        $this->hasMany('Wysylka', [
            'foreignKey' => 'vat_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('stawka')
            ->requirePresence('stawka', 'create')
            ->notEmpty('stawka');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->boolean('zwolniony')
            ->allowEmpty('zwolniony');

        return $validator;
    }
    public function createNew($stawka=''){
        if(empty($label)){
            return null;
        }
        $new=$this->newEntity(['stawka'=>$stawka,'nazwa'=>$stawka.'%','zwolniony'=>0]);
        if($this->save($new)){
            return $new->id;
        }else{
            return null;
        }
    }
}
