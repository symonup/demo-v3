<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WysylkaKrajeKoszty Model
 *
 * @property \App\Model\Table\WysylkaTable|\Cake\ORM\Association\BelongsTo $Wysylka
 *
 * @method \App\Model\Entity\WysylkaKrajeKoszty get($primaryKey, $options = [])
 * @method \App\Model\Entity\WysylkaKrajeKoszty newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\WysylkaKrajeKoszty[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\WysylkaKrajeKoszty|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\WysylkaKrajeKoszty patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\WysylkaKrajeKoszty[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\WysylkaKrajeKoszty findOrCreate($search, callable $callback = null, $options = [])
 */
class WysylkaKrajeKosztyTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('wysylka_kraje_koszty');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Wysylka', [
            'foreignKey' => 'wysylka_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('kraj', 'create')
            ->notEmpty('kraj');

        $validator
            ->numeric('koszt')
            ->requirePresence('koszt', 'create')
            ->notEmpty('koszt');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['wysylka_id'], 'Wysylka'));

        return $rules;
    }
}
