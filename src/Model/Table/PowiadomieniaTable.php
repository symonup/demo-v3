<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Powiadomienia Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $Uzytkownik
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\BelongsTo $Towar
 *
 * @method \App\Model\Entity\Powiadomienium get($primaryKey, $options = [])
 * @method \App\Model\Entity\Powiadomienium newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Powiadomienium[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Powiadomienium|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Powiadomienium patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Powiadomienium[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Powiadomienium findOrCreate($search, callable $callback = null, $options = [])
 */
class PowiadomieniaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('powiadomienia');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Uzytkownik', [
            'foreignKey' => 'uzytkownik_id'
        ]);
        $this->belongsTo('Towar', [
            'foreignKey' => 'towar_id'
        ]);
        $this->belongsTo('PunktyOdbioru', [
            'foreignKey' => 'punkt_odbioru'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('typ', 'create')
            ->notEmpty('typ');

        $validator
            ->allowEmpty('imie');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->integer('zgoda')
            ->allowEmpty('zgoda');

        $validator
            ->numeric('cena')
            ->allowEmpty('cena');

        $validator
            ->dateTime('data_dodania')
            ->requirePresence('data_dodania', 'create')
            ->notEmpty('data_dodania');

        $validator
            ->boolean('wyslane')
            ->allowEmpty('wyslane');

        $validator
            ->dateTime('data_wyslania')
            ->allowEmpty('data_wyslania');

        $validator
            ->allowEmpty('tresc');

        $validator
            ->allowEmpty('punkt_odbioru');

        $validator
            ->allowEmpty('dostepny');

        $validator
            ->allowEmpty('odpowiedz');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['uzytkownik_id'], 'Uzytkownik'));
        $rules->add($rules->existsIn(['towar_id'], 'Towar'));

        return $rules;
    }
}
