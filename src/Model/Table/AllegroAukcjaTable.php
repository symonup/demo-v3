<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AllegroAukcja Model
 *
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\BelongsTo $Towar
 * @property \App\Model\Table\ItemsTable|\Cake\ORM\Association\BelongsTo $Items
 * @property \App\Model\Table\AllegroAukcjaPrzesylkaTable|\Cake\ORM\Association\HasMany $AllegroAukcjaPrzesylka
 * @property \App\Model\Table\AllegroAukcjaWariantTable|\Cake\ORM\Association\HasMany $AllegroAukcjaWariant
 * @property \App\Model\Table\AllegroPolaKategoriaTable|\Cake\ORM\Association\HasMany $AllegroPolaKategoria
 *
 * @method \App\Model\Entity\AllegroAukcja get($primaryKey, $options = [])
 * @method \App\Model\Entity\AllegroAukcja newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AllegroAukcja[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AllegroAukcja|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AllegroAukcja patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AllegroAukcja[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AllegroAukcja findOrCreate($search, callable $callback = null, $options = [])
 */
class AllegroAukcjaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('allegro_aukcja');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Towar', [
            'foreignKey' => 'towar_id'
        ]);
        $this->belongsTo('AllegroKonto', [
            'foreignKey' => 'allegro_konto_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');
        
        $validator
            ->allowEmpty('allegro_konto_id');

        $validator
            ->integer('szablon')
            ->allowEmpty('szablon');

        $validator
            ->allowEmpty('item_info');

        $validator
            ->integer('item_is_allegro_standard')
            ->allowEmpty('item_is_allegro_standard');

        $validator
            ->integer('sprzedane')
            ->allowEmpty('sprzedane');
        
        $validator
            ->allowEmpty('name');
        
        $validator
            ->allowEmpty('category_id');
        
        $validator
            ->allowEmpty('validation');
        
        $validator
                ->allowEmpty('iloscToUpdate');
        
        $validator
                ->allowEmpty('priceToUpdate');
        
        $validator
                ->allowEmpty('iloscQueryId');
        
        $validator
                ->allowEmpty('iloscQueryStatus');
        
        $validator
                ->allowEmpty('cenaQueryId');
        
        $validator
                ->allowEmpty('cenaQueryStatus');
        
        $validator
                ->allowEmpty('statusQueryId');
        
        $validator
                ->allowEmpty('statusQueryStatus');
        $validator
                ->boolean('renew')
                ->allowEmpty('renew');
        
        $validator
                ->allowEmpty('iloscQueryStatusLog');
        
        $validator
                ->boolean('removed')
                ->allowEmpty('removed');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['towar_id'], 'Towar'));

        return $rules;
    }
}
