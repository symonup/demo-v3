<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AtrybutTyp Model
 *
 * @property \App\Model\Table\AtrybutTypParentTable|\Cake\ORM\Association\BelongsTo $AtrybutTypParent
 * @property \App\Model\Table\AtrybutTable|\Cake\ORM\Association\HasMany $Atrybut
 * @property \App\Model\Table\I18nTable|\Cake\ORM\Association\BelongsToMany $I18n
 *
 * @method \App\Model\Entity\AtrybutTyp get($primaryKey, $options = [])
 * @method \App\Model\Entity\AtrybutTyp newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AtrybutTyp[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AtrybutTyp|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AtrybutTyp patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AtrybutTyp[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AtrybutTyp findOrCreate($search, callable $callback = null, $options = [])
 */
class AtrybutTypTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('atrybut_typ');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('AtrybutTypParent', [
            'foreignKey' => 'atrybut_typ_parent_id'
        ]);
        $this->hasMany('Atrybut', [
            'foreignKey' => 'atrybut_typ_id'
        ]);
        $this->addBehavior('Translate', ['fields' => ['nazwa'], 'translationTable' => 'AtrybutTypI18n']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->integer('kolejnosc')
            ->allowEmpty('kolejnosc');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['atrybut_typ_parent_id'], 'AtrybutTypParent'));

        return $rules;
    }
}
