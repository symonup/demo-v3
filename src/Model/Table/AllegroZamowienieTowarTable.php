<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AllegroZamowienieTowar Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $Towars
 * @property \App\Model\Table\AllegroZamowienieTable|\Cake\ORM\Association\BelongsTo $AllegroZamowienie
 * @property |\Cake\ORM\Association\BelongsTo $Items
 * @property |\Cake\ORM\Association\BelongsTo $Buys
 *
 * @method \App\Model\Entity\AllegroZamowienieTowar get($primaryKey, $options = [])
 * @method \App\Model\Entity\AllegroZamowienieTowar newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AllegroZamowienieTowar[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AllegroZamowienieTowar|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AllegroZamowienieTowar patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AllegroZamowienieTowar[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AllegroZamowienieTowar findOrCreate($search, callable $callback = null, $options = [])
 */
class AllegroZamowienieTowarTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('allegro_zamowienie_towar');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Towar', [
            'foreignKey' => 'towar_id'
        ]);
        $this->belongsTo('AllegroZamowienie', [
            'foreignKey' => 'allegro_zamowienie_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('nazwa');

        $validator
            ->numeric('cena_podstawowa_za_sztuke')
            ->allowEmpty('cena_podstawowa_za_sztuke');

        $validator
            ->allowEmpty('waluta');

        $validator
            ->numeric('cena_za_sztuke')
            ->allowEmpty('cena_za_sztuke');

        $validator
            ->integer('ilosc')
            ->allowEmpty('ilosc');

        $validator
            ->dateTime('data_zakupu')
            ->allowEmpty('data_zakupu');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['allegro_zamowienie_id'], 'AllegroZamowienie'));

        return $rules;
    }
}
