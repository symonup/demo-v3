<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * KodyRabatowe Model
 *
 * @method \App\Model\Entity\KodyRabatowe get($primaryKey, $options = [])
 * @method \App\Model\Entity\KodyRabatowe newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\KodyRabatowe[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\KodyRabatowe|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\KodyRabatowe patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\KodyRabatowe[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\KodyRabatowe findOrCreate($search, callable $callback = null, $options = [])
 */
class KodyRabatoweTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('kody_rabatowe');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->belongsTo('Zamowienie', [
            'foreignKey' => 'zamowienie_id'
        ]);
        $this->hasMany('KodyRabatoweKategoria', [
            'foreignKey' => 'kody_rabatowe_id'
        ]);
        $this->hasMany('KodyRabatoweProducent', [
            'foreignKey' => 'kody_rabatowe_id'
        ]);
        $this->belongsToMany('Kategoria', [
            'foreignKey' => 'kody_rabatowe_id',
            'targetForeignKey' => 'kategoria_id',
            'joinTable' => 'kody_rabatowe_kategoria'
        ]);
        $this->belongsToMany('Producent', [
            'foreignKey' => 'kody_rabatowe_id',
            'targetForeignKey' => 'producent_id',
            'joinTable' => 'kody_rabatowe_producent'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->requirePresence('kod', 'create')
            ->notEmpty('kod')
            ->add('kod', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->numeric('rabat')
            ->requirePresence('rabat', 'create')
            ->notEmpty('rabat');

        $validator
            ->dateTime('data_start')
            ->allowEmpty('data_start');

        $validator
            ->dateTime('data_end')
            ->allowEmpty('data_end');

        $validator
            ->boolean('zablokuj')
            ->allowEmpty('zablokuj');

        $validator
            ->dateTime('data_dodania')
            ->allowEmpty('data_dodania');
        
        $validator
                ->allowEmpty('typ');
        
        $validator
                ->allowEmpty('zamowienie_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['kod']));

        return $rules;
    }
}
