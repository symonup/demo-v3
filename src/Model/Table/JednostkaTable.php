<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Jednostka Model
 *
 * @property \App\Model\Table\TowarCenaTable|\Cake\ORM\Association\HasMany $TowarCena
 * @property \App\Model\Table\I18nTable|\Cake\ORM\Association\BelongsToMany $I18n
 *
 * @method \App\Model\Entity\Jednostka get($primaryKey, $options = [])
 * @method \App\Model\Entity\Jednostka newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Jednostka[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Jednostka|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Jednostka patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Jednostka[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Jednostka findOrCreate($search, callable $callback = null, $options = [])
 */
class JednostkaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('jednostka');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('TowarCena', [
            'foreignKey' => 'jednostka_id'
        ]);
         $this->addBehavior('Translate', ['fields' => ['jednostka'], 'translationTable' => 'JednostkaI18n']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('jednostka', 'create')
            ->notEmpty('jednostka')
            ->add('jednostka', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }

    public function createNew($label=''){
        if(empty($label)){
            return null;
        }
        $new=$this->newEntity(['jednostka'=>$label]);
        if($this->save($new)){
            return $new->id;
        }else{
            return null;
        }
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['jednostka']));

        return $rules;
    }
}
