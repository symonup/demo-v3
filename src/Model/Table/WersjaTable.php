<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Wersja Model
 *
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\HasMany $Towar
 *
 * @method \App\Model\Entity\Wersja get($primaryKey, $options = [])
 * @method \App\Model\Entity\Wersja newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Wersja[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Wersja|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Wersja patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Wersja[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Wersja findOrCreate($search, callable $callback = null, $options = [])
 */
class WersjaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('wersja');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Towar', [
            'foreignKey' => 'wersja_id'
        ]);
        $this->addBehavior('Translate', ['fields' => ['nazwa'], 'translationTable' => 'GatunekI18n']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        return $validator;
    }
}
