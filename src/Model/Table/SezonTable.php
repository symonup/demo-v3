<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Sezon Model
 *
 * @property \App\Model\Table\KategoriaTable|\Cake\ORM\Association\BelongsToMany $Kategoria
 * @property \App\Model\Table\I18nTable|\Cake\ORM\Association\BelongsToMany $I18n
 *
 * @method \App\Model\Entity\Sezon get($primaryKey, $options = [])
 * @method \App\Model\Entity\Sezon newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Sezon[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Sezon|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Sezon patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Sezon[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Sezon findOrCreate($search, callable $callback = null, $options = [])
 */
class SezonTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('sezon');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsToMany('Kategoria', [
            'foreignKey' => 'sezon_id',
            'targetForeignKey' => 'kategorium_id',
            'joinTable' => 'kategoria_sezon'
        ]);
        $this->addBehavior('Translate', ['fields' => ['nazwa','seo_tytul','seo_opis','seo_slowa','naglowek','menu_tekst_naglowek','menu_tekst'], 'translationTable' => 'SezonI18n']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->integer('kolejnosc')
            ->allowEmpty('kolejnosc');

        $validator
            ->integer('ukryty')
            ->allowEmpty('ukryty');

        $validator
            ->allowEmpty('seo_tytul');

        $validator
            ->allowEmpty('seo_opis');

        $validator
            ->allowEmpty('seo_slowa');

        $validator
            ->allowEmpty('naglowek');

        $validator
            ->allowEmpty('menu_tekst_naglowek');

        $validator
            ->allowEmpty('menu_tekst');

        return $validator;
    }
}
