<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TowarAtrybut Model
 *
 * @property \App\Model\Table\AtrybutTable|\Cake\ORM\Association\BelongsTo $Atrybut
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\BelongsTo $Towar
 * @property \App\Model\Table\KategoriasTable|\Cake\ORM\Association\BelongsTo $Kategorias
 * @property \App\Model\Table\AtrybutPodrzedneTable|\Cake\ORM\Association\BelongsTo $AtrybutPodrzedne
 *
 * @method \App\Model\Entity\TowarAtrybut get($primaryKey, $options = [])
 * @method \App\Model\Entity\TowarAtrybut newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TowarAtrybut[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TowarAtrybut|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TowarAtrybut patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TowarAtrybut[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TowarAtrybut findOrCreate($search, callable $callback = null, $options = [])
 */
class TowarAtrybutTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('towar_atrybut');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Atrybut', [
            'foreignKey' => 'atrybut_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Towar', [
            'foreignKey' => 'towar_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('AtrybutPodrzedne', [
            'foreignKey' => 'atrybut_podrzedne_id'
        ]);
        $this->hasMany('TowarZdjecie', [
            'foreignKey' => 'wariant_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->numeric('wartosc')
            ->allowEmpty('wartosc');

        $validator
            ->boolean('wariant')
            ->allowEmpty('wariant');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['atrybut_id'], 'Atrybut'));
        $rules->add($rules->existsIn(['towar_id'], 'Towar'));
        $rules->add($rules->existsIn(['atrybut_podrzedne_id'], 'AtrybutPodrzedne'));

        return $rules;
    }
}
