<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Jezyk Model
 *
 * @property \App\Model\Table\WalutaTable|\Cake\ORM\Association\BelongsTo $Waluta
 * @property \App\Model\Table\NewsletterAdresTable|\Cake\ORM\Association\HasMany $NewsletterAdres
 *
 * @method \App\Model\Entity\Jezyk get($primaryKey, $options = [])
 * @method \App\Model\Entity\Jezyk newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Jezyk[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Jezyk|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Jezyk patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Jezyk[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Jezyk findOrCreate($search, callable $callback = null, $options = [])
 */
class JezykTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('jezyk');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Waluta', [
            'foreignKey' => 'waluta_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('NewsletterAdres', [
            'foreignKey' => 'jezyk_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->requirePresence('locale', 'create')
            ->notEmpty('locale');

        $validator
            ->requirePresence('symbol', 'create')
            ->notEmpty('symbol');

        $validator
            ->integer('domyslny')
            ->allowEmpty('domyslny');

        $validator
            ->integer('aktywny')
            ->allowEmpty('aktywny');

        $validator
            ->allowEmpty('flaga');

        $validator
            ->allowEmpty('domena');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['waluta_id'], 'Waluta'));

        return $rules;
    }
}
