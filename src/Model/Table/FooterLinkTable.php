<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FooterLink Model
 *
 * @method \App\Model\Entity\FooterLink get($primaryKey, $options = [])
 * @method \App\Model\Entity\FooterLink newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\FooterLink[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\FooterLink|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FooterLink patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\FooterLink[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\FooterLink findOrCreate($search, callable $callback = null, $options = [])
 */
class FooterLinkTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('footer_link');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');
        $this->belongsTo('Strona',[
            'foreignKey'=>'strona_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('url', 'create')
            ->notEmpty('url');

        $validator
            ->requirePresence('label', 'create')
            ->notEmpty('label');

        $validator
            ->allowEmpty('title');

        $validator
            ->allowEmpty('target');

        $validator
            ->integer('kolejnosc')
            ->allowEmpty('kolejnosc');
        $validator
            ->integer('grupa')
            ->allowEmpty('grupa');

        $validator
            ->allowEmpty('strona_id');

        $validator
                ->boolean('nofollow')
            ->allowEmpty('nofollow');

        return $validator;
    }
}
