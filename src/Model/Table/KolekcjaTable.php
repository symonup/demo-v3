<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Kolekcja Model
 *
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\HasMany $Towar
 * @property \App\Model\Table\I18nTable|\Cake\ORM\Association\BelongsToMany $I18n
 *
 * @method \App\Model\Entity\Kolekcja get($primaryKey, $options = [])
 * @method \App\Model\Entity\Kolekcja newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Kolekcja[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Kolekcja|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Kolekcja patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Kolekcja[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Kolekcja findOrCreate($search, callable $callback = null, $options = [])
 */
class KolekcjaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('kolekcja');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Towar', [
            'foreignKey' => 'kolekcja_id'
        ]);
        $this->addBehavior('Translate', ['fields' => ['nazwa'], 'translationTable' => 'KolekcjaI18n']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->integer('kolejnosc')
            ->allowEmpty('kolejnosc');

        $validator
            ->boolean('ukryta')
            ->allowEmpty('ukryta');

        return $validator;
    }
}
