<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CallBack Model
 *
 * @property \App\Model\Table\AdministratorTable|\Cake\ORM\Association\BelongsTo $Administrator
 *
 * @method \App\Model\Entity\CallBack get($primaryKey, $options = [])
 * @method \App\Model\Entity\CallBack newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CallBack[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CallBack|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CallBack patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CallBack[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CallBack findOrCreate($search, callable $callback = null, $options = [])
 */
class CallBackTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('call_back');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Administrator', [
            'foreignKey' => 'administrator_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('telefon', 'create')
            ->notEmpty('telefon');

        $validator
            ->dateTime('data')
            ->requirePresence('data', 'create')
            ->notEmpty('data');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        $validator
            ->allowEmpty('notatki');

        $validator
            ->dateTime('data_status')
            ->allowEmpty('data_status');

        $validator
            ->allowEmpty('administrator_dane');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['administrator_id'], 'Administrator'));

        return $rules;
    }
}
