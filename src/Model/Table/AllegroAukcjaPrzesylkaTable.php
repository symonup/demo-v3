<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AllegroAukcjaPrzesylka Model
 *
 * @property \App\Model\Table\AllegroAukcjaTable|\Cake\ORM\Association\BelongsTo $AllegroAukcja
 *
 * @method \App\Model\Entity\AllegroAukcjaPrzesylka get($primaryKey, $options = [])
 * @method \App\Model\Entity\AllegroAukcjaPrzesylka newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AllegroAukcjaPrzesylka[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AllegroAukcjaPrzesylka|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AllegroAukcjaPrzesylka patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AllegroAukcjaPrzesylka[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AllegroAukcjaPrzesylka findOrCreate($search, callable $callback = null, $options = [])
 */
class AllegroAukcjaPrzesylkaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('allegro_aukcja_przesylka');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('AllegroAukcja', [
            'foreignKey' => 'allegro_aukcja_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->integer('fid')
            ->requirePresence('fid', 'create')
            ->notEmpty('fid');

        $validator
            ->numeric('pierwsza_sztuka')
            ->requirePresence('pierwsza_sztuka', 'create')
            ->notEmpty('pierwsza_sztuka');

        $validator
            ->numeric('kolejna_sztuka')
            ->allowEmpty('kolejna_sztuka');

        $validator
            ->integer('ilosc_w_paczce')
            ->allowEmpty('ilosc_w_paczce');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['allegro_aukcja_id'], 'AllegroAukcja'));

        return $rules;
    }
}
