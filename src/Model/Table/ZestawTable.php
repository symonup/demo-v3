<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Zestaw Model
 *
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\BelongsTo $Towar
 * @property \App\Model\Table\ZamowienieTowarTable|\Cake\ORM\Association\HasMany $ZamowienieTowar
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\BelongsToMany $Towar
 *
 * @method \App\Model\Entity\Zestaw get($primaryKey, $options = [])
 * @method \App\Model\Entity\Zestaw newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Zestaw[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Zestaw|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Zestaw patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Zestaw[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Zestaw findOrCreate($search, callable $callback = null, $options = [])
 */
class ZestawTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('zestaw');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Towar', [
            'foreignKey' => 'towar_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('ZamowienieTowar', [
            'foreignKey' => 'zestaw_id'
        ]);
        $this->hasMany('ZestawTowar', [
            'foreignKey' => 'zestaw_id'
        ]);
//        $this->belongsToMany('Towar', [
//            'foreignKey' => 'zestaw_id',
//            'targetForeignKey' => 'towar_id',
//            'joinTable' => 'zestaw_towar'
//        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->numeric('rabat')
            ->requirePresence('rabat', 'create')
            ->notEmpty('rabat');

        $validator
            ->integer('kolejnosc')
            ->allowEmpty('kolejnosc');
        $validator
            ->integer('typ')
            ->notEmpty('typ');
        $validator
                ->allowEmpty('nazwa');
        $validator
                ->allowEmpty('token');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['towar_id'], 'Towar'));

        return $rules;
    }
}
