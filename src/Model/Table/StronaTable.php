<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Strona Model
 *
 * @method \App\Model\Entity\Strona get($primaryKey, $options = [])
 * @method \App\Model\Entity\Strona newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Strona[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Strona|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Strona patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Strona[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Strona findOrCreate($search, callable $callback = null, $options = [])
 */
class StronaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('strona');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->boolean('menu_dolne')
            ->allowEmpty('menu_dolne');

        $validator
            ->boolean('ukryta')
            ->allowEmpty('ukryta');

        $validator
            ->dateTime('data')
            ->allowEmpty('data');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->allowEmpty('tresc');

        $validator
            ->allowEmpty('seo_tytul');

        $validator
            ->allowEmpty('seo_slowa');

        $validator
            ->allowEmpty('seo_opis');

        $validator
            ->boolean('allow_all')
            ->allowEmpty('allow_all');

        $validator
            ->allowEmpty('type');
        $validator
            ->notEmpty('locale');

        return $validator;
    }
}
