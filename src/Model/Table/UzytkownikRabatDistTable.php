<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UzytkownikRabatDist Model
 *
 * @property \App\Model\Table\UzytkowniksTable|\Cake\ORM\Association\BelongsTo $Uzytkowniks
 *
 * @method \App\Model\Entity\UzytkownikRabatDist get($primaryKey, $options = [])
 * @method \App\Model\Entity\UzytkownikRabatDist newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UzytkownikRabatDist[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UzytkownikRabatDist|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UzytkownikRabatDist patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UzytkownikRabatDist[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UzytkownikRabatDist findOrCreate($search, callable $callback = null, $options = [])
 */
class UzytkownikRabatDistTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('uzytkownik_rabat_dist');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Uzytkownik', [
            'foreignKey' => 'uzytkownik_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->numeric('kwota_zamowienia')
            ->requirePresence('kwota_zamowienia', 'create')
            ->notEmpty('kwota_zamowienia');

        $validator
            ->numeric('wartosc_rabatu')
            ->requirePresence('wartosc_rabatu', 'create')
            ->notEmpty('wartosc_rabatu');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['uzytkownik_id'], 'Uzytkownik'));

        return $rules;
    }
}
