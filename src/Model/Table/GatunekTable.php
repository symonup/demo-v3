<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Gatunek Model
 *
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\BelongsToMany $Towar
 *
 * @method \App\Model\Entity\Gatunek get($primaryKey, $options = [])
 * @method \App\Model\Entity\Gatunek newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Gatunek[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Gatunek|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Gatunek patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Gatunek[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Gatunek findOrCreate($search, callable $callback = null, $options = [])
 */
class GatunekTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('gatunek');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsToMany('Towar', [
            'foreignKey' => 'gatunek_id',
            'targetForeignKey' => 'towar_id',
            'joinTable' => 'towar_gatunek'
        ]);
        $this->addBehavior('Translate', ['fields' => ['nazwa','nazwa_2'], 'translationTable' => 'GatunekI18n']);
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->allowEmpty('zdjecie');

        $validator
            ->integer('kolejnosc')
            ->allowEmpty('kolejnosc');

        $validator
            ->boolean('ukryty')
            ->allowEmpty('ukryty');

        $validator
            ->boolean('glowna')
            ->allowEmpty('glowna');

        $validator
            ->allowEmpty('mask');

        $validator
            ->allowEmpty('nazwa_2');

        return $validator;
    }
}
