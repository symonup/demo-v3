<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Zapytanie Model
 *
 * @property \App\Model\Table\UzytkownikTable|\Cake\ORM\Association\BelongsTo $Uzytkownik
 *
 * @method \App\Model\Entity\Zapytanie get($primaryKey, $options = [])
 * @method \App\Model\Entity\Zapytanie newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Zapytanie[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Zapytanie|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Zapytanie patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Zapytanie[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Zapytanie findOrCreate($search, callable $callback = null, $options = [])
 */
class ZapytanieTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('zapytanie');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Uzytkownik', [
            'foreignKey' => 'uzytkownik_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('nr_zgloszenia');

        $validator
            ->allowEmpty('imie');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->boolean('zgoda_dane')
            ->requirePresence('zgoda_dane', 'create')
            ->notEmpty('zgoda_dane');

        $validator
            ->allowEmpty('telefon');

        $validator
            ->integer('platforma')
            ->allowEmpty('platforma');

        $validator
            ->integer('nosnik')
            ->allowEmpty('nosnik');

        $validator
            ->requirePresence('tresc', 'create')
            ->notEmpty('tresc');

        $validator
            ->dateTime('data_dodania')
            ->requirePresence('data_dodania', 'create')
            ->notEmpty('data_dodania');

        $validator
            ->allowEmpty('odpowiedz');

        $validator
            ->dateTime('data_wyslania')
            ->allowEmpty('data_wyslania');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['uzytkownik_id'], 'Uzytkownik'));

        return $rules;
    }
}
