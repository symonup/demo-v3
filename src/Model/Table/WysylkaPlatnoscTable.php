<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WysylkaPlatnosc Model
 *
 * @property \App\Model\Table\WysylkaTable|\Cake\ORM\Association\BelongsTo $Wysylka
 * @property \App\Model\Table\RodzajPlatnosciTable|\Cake\ORM\Association\BelongsTo $RodzajPlatnosci
 *
 * @method \App\Model\Entity\WysylkaPlatnosc get($primaryKey, $options = [])
 * @method \App\Model\Entity\WysylkaPlatnosc newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\WysylkaPlatnosc[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\WysylkaPlatnosc|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\WysylkaPlatnosc patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\WysylkaPlatnosc[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\WysylkaPlatnosc findOrCreate($search, callable $callback = null, $options = [])
 */
class WysylkaPlatnoscTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('wysylka_platnosc');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Wysylka', [
            'foreignKey' => 'wysylka_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('RodzajPlatnosci', [
            'foreignKey' => 'rodzaj_platnosci_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['wysylka_id'], 'Wysylka'));
        $rules->add($rules->existsIn(['rodzaj_platnosci_id'], 'RodzajPlatnosci'));

        return $rules;
    }
}
