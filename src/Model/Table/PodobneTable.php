<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class PodobneTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('towar');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('TowarZdjecie', [
            'foreignKey' => 'towar_id'
        ]);
        $this->belongsToMany('Kategoria', [
            'foreignKey' => 'towar_id',
            'targetForeignKey' => 'kategoria_id',
            'joinTable' => 'towar_kategoria'
        ]);
        $this->belongsTo('Platforma', [
            'foreignKey' => 'platforma_id'
        ]);
        $this->hasOne('TowarCenaDefault', [
            'className'=>'TowarCena',
            'foreignKey' => 'towar_id',
            'conditions'=>['TowarCenaDefault.uzytkownik_id IS'=>null]
        ]);
        
        $this->addBehavior('Translate', ['fields' => ['nazwa','opis','opis2','hint_cena','seo_tytul','seo_slowa','seo_opis','zestaw_nazwa'], 'translationTable' => 'TowarI18n']);
    }
}
