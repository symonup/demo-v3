<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ZestawTowar Model
 *
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\BelongsTo $Towar
 * @property \App\Model\Table\ZestawTable|\Cake\ORM\Association\BelongsTo $Zestaw
 * @property \App\Model\Table\KategoriasTable|\Cake\ORM\Association\BelongsTo $Kategorias
 *
 * @method \App\Model\Entity\ZestawTowar get($primaryKey, $options = [])
 * @method \App\Model\Entity\ZestawTowar newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ZestawTowar[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ZestawTowar|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ZestawTowar patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ZestawTowar[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ZestawTowar findOrCreate($search, callable $callback = null, $options = [])
 */
class ZestawTowarTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('zestaw_towar');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Towar', [
            'foreignKey' => 'towar_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Zestaw', [
            'foreignKey' => 'zestaw_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Kategoria', [
            'foreignKey' => 'kategoria_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('wymiana')
            ->allowEmpty('wymiana');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['towar_id'], 'Towar'));
        $rules->add($rules->existsIn(['zestaw_id'], 'Zestaw'));
        $rules->add($rules->existsIn(['kategoria_id'], 'Kategoria'));

        return $rules;
    }
}
