<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FakturaPozycja Model
 *
 * @property \App\Model\Table\FakturaTable|\Cake\ORM\Association\BelongsTo $Faktura
 *
 * @method \App\Model\Entity\FakturaPozycja get($primaryKey, $options = [])
 * @method \App\Model\Entity\FakturaPozycja newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\FakturaPozycja[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\FakturaPozycja|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FakturaPozycja patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\FakturaPozycja[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\FakturaPozycja findOrCreate($search, callable $callback = null, $options = [])
 */
class FakturaPozycjaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('faktura_pozycja');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Faktura', [
            'foreignKey' => 'faktura_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('lp')
            ->allowEmpty('lp');

        $validator
            ->allowEmpty('nazwa');

        $validator
            ->integer('typ_towaru')
            ->allowEmpty('typ_towaru');

        $validator
            ->allowEmpty('symbol');

        $validator
            ->boolean('rabat_procentowy')
            ->allowEmpty('rabat_procentowy');

        $validator
            ->boolean('rabat_od_ceny')
            ->allowEmpty('rabat_od_ceny');

        $validator
            ->boolean('rabat_pozycja')
            ->allowEmpty('rabat_pozycja');

        $validator
            ->boolean('rabat_blokada')
            ->allowEmpty('rabat_blokada');

        $validator
            ->numeric('rabat_wartosc')
            ->allowEmpty('rabat_wartosc');

        $validator
            ->numeric('rabat_procent')
            ->allowEmpty('rabat_procent');

        $validator
            ->allowEmpty('jednostka');

        $validator
            ->numeric('ilosc_jednostka')
            ->allowEmpty('ilosc_jednostka');

        $validator
            ->numeric('ilosc_magazyn')
            ->allowEmpty('ilosc_magazyn');

        $validator
            ->numeric('cena_magazyn')
            ->allowEmpty('cena_magazyn');

        $validator
            ->numeric('cena_netto')
            ->allowEmpty('cena_netto');

        $validator
            ->numeric('cena_brutto')
            ->allowEmpty('cena_brutto');

        $validator
            ->numeric('stawka_vat')
            ->allowEmpty('stawka_vat');

        $validator
            ->numeric('wartosc_netto')
            ->allowEmpty('wartosc_netto');

        $validator
            ->numeric('wartosc_vat')
            ->allowEmpty('wartosc_vat');

        $validator
            ->numeric('wartosc_brutto')
            ->allowEmpty('wartosc_brutto');

        $validator
            ->numeric('koszt')
            ->allowEmpty('koszt');

        $validator
            ->allowEmpty('opis_uslugi');

        $validator
            ->allowEmpty('nazwa_uslugi');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['faktura_id'], 'Faktura'));

        return $rules;
    }
}
