<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Ikona Model
 *
 * @property \App\Model\Table\PrzeznaczenieTable|\Cake\ORM\Association\HasMany $Przeznaczenie
 *
 * @method \App\Model\Entity\Ikona get($primaryKey, $options = [])
 * @method \App\Model\Entity\Ikona newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Ikona[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Ikona|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ikona patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Ikona[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Ikona findOrCreate($search, callable $callback = null, $options = [])
 */
class IkonaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ikona');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Przeznaczenie', [
            'foreignKey' => 'ikona_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->requirePresence('ikona', 'create')
            ->notEmpty('ikona');

        $validator
            ->allowEmpty('hover');

        return $validator;
    }
}
