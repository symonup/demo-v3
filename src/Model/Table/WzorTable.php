<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Wzor Model
 *
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\HasMany $Towar
 * @property \App\Model\Table\WariantCechaWartoscTable|\Cake\ORM\Association\HasMany $WariantCechaWartosc
 * @property \App\Model\Table\I18nTable|\Cake\ORM\Association\BelongsToMany $I18n
 *
 * @method \App\Model\Entity\Wzor get($primaryKey, $options = [])
 * @method \App\Model\Entity\Wzor newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Wzor[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Wzor|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Wzor patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Wzor[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Wzor findOrCreate($search, callable $callback = null, $options = [])
 */
class WzorTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('wzor');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Towar', [
            'foreignKey' => 'wzor_id'
        ]);
        $this->hasMany('WariantCechaWartosc', [
            'foreignKey' => 'wzor_id'
        ]);
        $this->addBehavior('Translate', ['fields' => ['nazwa'], 'translationTable' => 'WzorI18n']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->requirePresence('plik', 'create')
            ->notEmpty('plik');

        return $validator;
    }
}
