<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Opakowanie Model
 *
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\HasMany $Towar
 *
 * @method \App\Model\Entity\Opakowanie get($primaryKey, $options = [])
 * @method \App\Model\Entity\Opakowanie newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Opakowanie[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Opakowanie|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Opakowanie patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Opakowanie[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Opakowanie findOrCreate($search, callable $callback = null, $options = [])
 */
class OpakowanieTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('opakowanie');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Towar', [
            'foreignKey' => 'opakowanie_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        return $validator;
    }
}
