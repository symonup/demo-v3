<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Wiek Model
 *
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\HasMany $Towar
 *
 * @method \App\Model\Entity\Wiek get($primaryKey, $options = [])
 * @method \App\Model\Entity\Wiek newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Wiek[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Wiek|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Wiek patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Wiek[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Wiek findOrCreate($search, callable $callback = null, $options = [])
 */
class WiekTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('wiek');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Towar', [
            'foreignKey' => 'wiek_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nazwa', 'create')
            ->notEmpty('nazwa');

        $validator
            ->integer('miesiace_od')
            ->allowEmpty('miesiace_od');

        $validator
            ->integer('miesiace_do')
            ->allowEmpty('miesiace_do');

        $validator
            ->integer('lata_od')
            ->allowEmpty('lata_od');

        $validator
            ->integer('lata_do')
            ->allowEmpty('lata_do');

        $validator
            ->integer('kolejnosc')
            ->allowEmpty('kolejnosc');

        return $validator;
    }
}
