<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FakturaPliki Model
 *
 * @property \App\Model\Table\ZamowienieTable|\Cake\ORM\Association\BelongsTo $Zamowienie
 * @property \App\Model\Table\AllegroZamowienieTable|\Cake\ORM\Association\BelongsTo $AllegroZamowienie
 *
 * @method \App\Model\Entity\FakturaPliki get($primaryKey, $options = [])
 * @method \App\Model\Entity\FakturaPliki newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\FakturaPliki[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\FakturaPliki|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FakturaPliki patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\FakturaPliki[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\FakturaPliki findOrCreate($search, callable $callback = null, $options = [])
 */
class FakturaPlikiTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('faktura_pliki');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Zamowienie', [
            'foreignKey' => 'zamowienie_id'
        ]);
        $this->belongsTo('AllegroZamowienie', [
            'foreignKey' => 'allegro_zamowienie_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('plik', 'create')
            ->notEmpty('plik');

        $validator
            ->dateTime('data')
            ->requirePresence('data', 'create')
            ->notEmpty('data');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['zamowienie_id'], 'Zamowienie'));
        $rules->add($rules->existsIn(['allegro_zamowienie_id'], 'AllegroZamowienie'));

        return $rules;
    }
}
