<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WariantCechaWartosc Model
 *
 * @property \App\Model\Table\WariantCechaTable|\Cake\ORM\Association\BelongsTo $WariantCecha
 * @property \App\Model\Table\TowarTable|\Cake\ORM\Association\BelongsTo $Towar
 * @property \App\Model\Table\KolorsTable|\Cake\ORM\Association\BelongsTo $Kolors
 * @property \App\Model\Table\WzorsTable|\Cake\ORM\Association\BelongsTo $Wzors
 *
 * @method \App\Model\Entity\WariantCechaWartosc get($primaryKey, $options = [])
 * @method \App\Model\Entity\WariantCechaWartosc newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\WariantCechaWartosc[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\WariantCechaWartosc|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\WariantCechaWartosc patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\WariantCechaWartosc[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\WariantCechaWartosc findOrCreate($search, callable $callback = null, $options = [])
 */
class WariantCechaWartoscTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('wariant_cecha_wartosc');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('WariantCecha', [
            'foreignKey' => 'wariant_cecha_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Towar', [
            'foreignKey' => 'towar_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Kolors', [
            'foreignKey' => 'kolor_id'
        ]);
        $this->belongsTo('Wzors', [
            'foreignKey' => 'wzor_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->numeric('wartosc_float')
            ->allowEmpty('wartosc_float');

        $validator
            ->allowEmpty('wartosc_text');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['wariant_cecha_id'], 'WariantCecha'));
        $rules->add($rules->existsIn(['towar_id'], 'Towar'));
        $rules->add($rules->existsIn(['kolor_id'], 'Kolors'));
        $rules->add($rules->existsIn(['wzor_id'], 'Wzors'));

        return $rules;
    }
}
