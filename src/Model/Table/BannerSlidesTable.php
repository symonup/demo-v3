<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BannerSlides Model
 *
 * @property \App\Model\Table\BannerTable|\Cake\ORM\Association\BelongsTo $Banner
 *
 * @method \App\Model\Entity\BannerSlide get($primaryKey, $options = [])
 * @method \App\Model\Entity\BannerSlide newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\BannerSlide[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\BannerSlide|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BannerSlide patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\BannerSlide[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\BannerSlide findOrCreate($search, callable $callback = null, $options = [])
 */
class BannerSlidesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('banner_slides');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->belongsTo('Banner', [
            'foreignKey' => 'banner_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('BannerSlidesElements', [
            'foreignKey' => 'banner_slides_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('type');

        $validator
            ->allowEmpty('file');

        $validator
            ->boolean('is_linked')
            ->allowEmpty('is_linked');

        $validator
            ->allowEmpty('link');

        $validator
            ->allowEmpty('title');

        $validator
            ->allowEmpty('target');

        $validator
            ->integer('height')
            ->allowEmpty('height');

        $validator
            ->integer('width')
            ->allowEmpty('width');

        $validator
            ->allowEmpty('css');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['banner_id'], 'Banner'));

        return $rules;
    }
}
