<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Uzytkownik Model
 *
 * @property \App\Model\Table\WalutaTable|\Cake\ORM\Association\BelongsTo $Waluta
 * @property \App\Model\Table\AdministratorTable|\Cake\ORM\Association\BelongsTo $Administrator
 * @property \App\Model\Table\PlatnoscTable|\Cake\ORM\Association\HasMany $Platnosc
 * @property \App\Model\Table\TowarUzytkownikRabatTable|\Cake\ORM\Association\HasMany $TowarUzytkownikRabat
 * @property \App\Model\Table\UzytkownikAdresTable|\Cake\ORM\Association\HasMany $UzytkownikAdres
 * @property \App\Model\Table\ZamowienieTable|\Cake\ORM\Association\HasMany $Zamowienie
 *
 * @method \App\Model\Entity\Uzytkownik get($primaryKey, $options = [])
 * @method \App\Model\Entity\Uzytkownik newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Uzytkownik[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Uzytkownik|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Uzytkownik patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Uzytkownik[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Uzytkownik findOrCreate($search, callable $callback = null, $options = [])
 */
class UzytkownikTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('uzytkownik');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->hasMany('Platnosc', [
            'foreignKey' => 'uzytkownik_id'
        ]);
        $this->hasMany('UzytkownikAdres', [
            'foreignKey' => 'uzytkownik_id'
        ]);
        $this->hasMany('UzytkownikAdresWysylka',[
            'className'=>'UzytkownikAdres',
            'foreignKey' => 'uzytkownik_id',
            'conditions' => ['UzytkownikAdresWysylka.typ'=>1]
        ]);
        $this->hasOne('UzytkownikAdresFaktura',[
            'className'=>'UzytkownikAdres',
            'foreignKey' => 'uzytkownik_id',
            'conditions' => ['UzytkownikAdresFaktura.typ'=>2]
        ]);
        $this->hasMany('Zamowienie', [
            'foreignKey' => 'uzytkownik_id'
        ]);
        $this->hasMany('TowarCena',[
            'foreignKey' => 'uzytkownik_id'
            ]);
        $this->hasOne('UzytkownikWysylka',[
            'className'=>'UzytkownikAdres',
            'foreignKey' => 'uzytkownik_id',
            'conditions'=>['UzytkownikWysylka.typ'=>1,'UzytkownikWysylka.domyslny_wysylka'=>1]
        ]);
        $this->hasOne('UzytkownikFaktura',[
            'className'=>'UzytkownikAdres',
            'foreignKey' => 'uzytkownik_id',
            'conditions'=>['UzytkownikFaktura.typ'=>2]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');


        $validator
            ->boolean('newsletter')
            ->allowEmpty('newsletter');

        $validator
            ->allowEmpty('token');

        $validator
            ->dateTime('token_data')
            ->allowEmpty('token_data');

        $validator
            ->dateTime('data_rejestracji')
            ->allowEmpty('data_rejestracji');

        $validator
            ->boolean('aktywny')
            ->allowEmpty('aktywny');

        $validator
            ->allowEmpty('firma');

        $validator
            ->allowEmpty('nip');
        $validator
                ->allowEmpty('remember_token');
        $validator
                ->allowEmpty('bonusy_suma');
        $validator
                ->allowEmpty('wysylka_id');
        $validator
                ->allowEmpty('rodzaj_platnosci_id');
        $validator
                ->allowEmpty('paczkomat');
        $validator
                ->allowEmpty('paczkomat_info');

        return $validator;
    }
    public function validationRegulamin($validator) {
        $validator
            ->requirePresence('zgoda', 'create',t('validations.consentIsRequired'))
            ->notEmpty('zgoda',t('validations.consentIsRequired'));
        $validator
                ->requirePresence('regulamin', 'create',t('validations.regulationsIsRequired'))
            ->notEmpty('regulamin',t('validations.regulationsIsRequired'))
               ;
        return $validator;
    }
    public function validationPasswords($validator) {
        $validator
            ->requirePresence('password', 'create',t('validations.passwordIsRequired'))
            ->notEmpty('password',t('validations.passwordIsRequired'));
        $validator->notEmpty('repeat_password',t('validations.repeatPasswordIsRequired'))
                ->add('repeat_password', 'compare', [
            'rule' => ['compareWith', 'password'],
            'message' => t('validations.repeatPasswordDifrent'),
        ]);
        return $validator;
    }
    public function validationLogin($validator) {
        
        $validator
            ->email('email',false,t('validations.invalidEmail'))
            ->notEmpty('email',t('validations.emailIsRequired'));
        $validator
            ->requirePresence('password', 'create',t('validations.passwordIsRequired'))
            ->notEmpty('password',t('validations.passwordIsRequired'));
        return $validator;
    }
    public function validationRegistrationb2c($validator) {
        $this->validationDefault($validator);
        $this->validationPasswords($validator);
        
        $validator
            ->email('email',false,t('validations.invalidEmail'))
            ->requirePresence('email', 'create',t('validations.emailIsRequired'))
            ->notEmpty('email',t('validations.emailIsRequired'))
            ->add('email', 'unique', ['rule' => 'validateUnique', 'provider' => 'table','message'=>t('validations.emailUnique')]);

//        $validator
//            ->requirePresence('telefon', 'create',t('validations.phoneIsRequired'))
//            ->notEmpty('telefon',t('validations.phoneIsRequired'));

        $validator
            ->requirePresence('imie', 'create',t('validations.fieldIsRequired'))
            ->notEmpty('imie',t('validations.fieldIsRequired'));

        $validator
            ->requirePresence('nazwisko', 'create',t('validations.fieldIsRequired'))
            ->notEmpty('nazwisko',t('validations.fieldIsRequired'));
//        $this->validationRegulamin($validator);
        return $validator;
    }
    public function validationRegistrationb2b($validator) {
        $this->validationDefault($validator);
        $this->validationPasswords($validator);
        
        $validator
            ->email('email',false,t('validations.invalidEmail'))
            ->requirePresence('email', 'create',t('validations.emailIsRequired'))
            ->notEmpty('email',t('validations.emailIsRequired'))
            ->add('email', 'unique', ['rule' => 'validateUnique', 'provider' => 'table','message'=>t('validations.emailUnique')]);

        $validator
            ->requirePresence('telefon', 'create',t('validations.phoneIsRequired'))
            ->notEmpty('telefon',t('validations.phoneIsRequired'));

        $validator
            ->requirePresence('imie', 'create',t('validations.fieldIsRequired'))
            ->notEmpty('imie',t('validations.fieldIsRequired'));

        $validator
            ->requirePresence('nazwisko', 'create',t('validations.fieldIsRequired'))
            ->notEmpty('nazwisko',t('validations.fieldIsRequired'));
        
        $validator
            ->requirePresence('firma', 'create',t('validations.fieldIsRequired'))
            ->notEmpty('firma',t('validations.fieldIsRequired'));

        $validator
            ->requirePresence('nip', 'create',t('validations.fieldIsRequired'))
            ->notEmpty('nip',t('validations.fieldIsRequired'));
        
        $validator
            ->requirePresence('firma_ulica', 'create',t('validations.fieldIsRequired'))
            ->notEmpty('firma_ulica',t('validations.fieldIsRequired'));
        
        $validator
            ->requirePresence('firma_nr_domu', 'create',t('validations.fieldIsRequired'))
            ->notEmpty('firma_nr_domu',t('validations.fieldIsRequired'));
        
        $validator
            ->requirePresence('firma_kod', 'create',t('validations.fieldIsRequired'))
            ->notEmpty('firma_kod',t('validations.fieldIsRequired'));
        
        $validator
            ->requirePresence('firma_miasto', 'create',t('validations.fieldIsRequired'))
            ->notEmpty('firma_miasto',t('validations.fieldIsRequired'));
        
        return $validator;
    }
    
    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
}
