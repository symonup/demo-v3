<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * WysylkaKrajeKosztyFixture
 *
 */
class WysylkaKrajeKosztyFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'wysylka_kraje_koszty';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'wysylka_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'kraj' => ['type' => 'string', 'length' => 10, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'koszt' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'wysylka_id' => ['type' => 'unique', 'columns' => ['wysylka_id', 'kraj'], 'length' => []],
            'wysylka_kraje_koszty_ibfk_1' => ['type' => 'foreign', 'columns' => ['wysylka_id'], 'references' => ['wysylka', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'wysylka_id' => 1,
            'kraj' => 'Lorem ip',
            'koszt' => 1
        ],
    ];
}
