<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ZamowienieTowarFixture
 *
 */
class ZamowienieTowarFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'zamowienie_towar';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'zamowienie_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'ilosc' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'ilosc_w_kartonie' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '1', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'cena_za_sztuke_netto' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'cena_za_sztuke_brutto' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'cena_razem_netto' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'cena_razem_brutto' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'kod' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'nazwa' => ['type' => 'string', 'length' => 150, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'towar_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'wariant_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'jednostka_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'vat_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'vat_stawka' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        '_indexes' => [
            'zamowienie_id' => ['type' => 'index', 'columns' => ['zamowienie_id'], 'length' => []],
            'towar_id' => ['type' => 'index', 'columns' => ['towar_id'], 'length' => []],
            'wariant_id' => ['type' => 'index', 'columns' => ['wariant_id'], 'length' => []],
            'jednostka_id' => ['type' => 'index', 'columns' => ['jednostka_id'], 'length' => []],
            'vat_id' => ['type' => 'index', 'columns' => ['vat_id'], 'length' => []],
            'vat_id_2' => ['type' => 'index', 'columns' => ['vat_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'zamowienie_towar_ibfk_1' => ['type' => 'foreign', 'columns' => ['zamowienie_id'], 'references' => ['zamowienie', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'zamowienie_towar_ibfk_2' => ['type' => 'foreign', 'columns' => ['towar_id'], 'references' => ['towar', 'id'], 'update' => 'cascade', 'delete' => 'setNull', 'length' => []],
            'zamowienie_towar_ibfk_3' => ['type' => 'foreign', 'columns' => ['wariant_id'], 'references' => ['towar_atrybut', 'id'], 'update' => 'cascade', 'delete' => 'setNull', 'length' => []],
            'zamowienie_towar_ibfk_4' => ['type' => 'foreign', 'columns' => ['jednostka_id'], 'references' => ['jednostka', 'id'], 'update' => 'cascade', 'delete' => 'setNull', 'length' => []],
            'zamowienie_towar_ibfk_5' => ['type' => 'foreign', 'columns' => ['vat_id'], 'references' => ['vat', 'id'], 'update' => 'cascade', 'delete' => 'setNull', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'zamowienie_id' => 1,
            'ilosc' => 1,
            'ilosc_w_kartonie' => 1,
            'cena_za_sztuke_netto' => 1,
            'cena_za_sztuke_brutto' => 1,
            'cena_razem_netto' => 1,
            'cena_razem_brutto' => 1,
            'kod' => 'Lorem ipsum dolor sit amet',
            'nazwa' => 'Lorem ipsum dolor sit amet',
            'towar_id' => 1,
            'wariant_id' => 1,
            'jednostka_id' => 1,
            'vat_id' => 1,
            'vat_stawka' => 1
        ],
    ];
}
