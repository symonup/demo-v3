<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TowarPrzeznaczenieFixture
 *
 */
class TowarPrzeznaczenieFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'towar_przeznaczenie';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'towar_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'przeznaczenie_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'towar_id' => ['type' => 'index', 'columns' => ['towar_id'], 'length' => []],
            'przeznaczenie_id' => ['type' => 'index', 'columns' => ['przeznaczenie_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'towar_przeznaczenie_ibfk_1' => ['type' => 'foreign', 'columns' => ['towar_id'], 'references' => ['towar', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'towar_przeznaczenie_ibfk_2' => ['type' => 'foreign', 'columns' => ['przeznaczenie_id'], 'references' => ['przeznaczenie', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'towar_id' => 1,
            'przeznaczenie_id' => 1
        ],
    ];
}
