<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TowarWariantFixture
 *
 */
class TowarWariantFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'towar_wariant';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'towar_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'rozmiar_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'kolor_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'ilosc' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'ean' => ['type' => 'string', 'length' => 24, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'towar_id' => ['type' => 'index', 'columns' => ['towar_id'], 'length' => []],
            'rozmiar_id' => ['type' => 'index', 'columns' => ['rozmiar_id'], 'length' => []],
            'kolor_id' => ['type' => 'index', 'columns' => ['kolor_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'towar_wariant_ibfk_1' => ['type' => 'foreign', 'columns' => ['towar_id'], 'references' => ['towar', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'towar_wariant_ibfk_2' => ['type' => 'foreign', 'columns' => ['kolor_id'], 'references' => ['kolor', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'towar_wariant_ibfk_3' => ['type' => 'foreign', 'columns' => ['rozmiar_id'], 'references' => ['rozmiar', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'towar_id' => 1,
            'rozmiar_id' => 1,
            'kolor_id' => 1,
            'ilosc' => 1,
            'ean' => 'Lorem ipsum dolor sit '
        ],
    ];
}
