<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * KodyRabatoweKategoriaFixture
 *
 */
class KodyRabatoweKategoriaFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'kody_rabatowe_kategoria';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'kategoria_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'kody_rabatowe_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'kategoria_id' => ['type' => 'index', 'columns' => ['kategoria_id'], 'length' => []],
            'kody_rabatowe_id' => ['type' => 'index', 'columns' => ['kody_rabatowe_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'kody_rabatowe_kategoria_ibfk_1' => ['type' => 'foreign', 'columns' => ['kategoria_id'], 'references' => ['kategoria', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'kody_rabatowe_kategoria_ibfk_2' => ['type' => 'foreign', 'columns' => ['kody_rabatowe_id'], 'references' => ['kody_rabatowe', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'kategoria_id' => 1,
            'kody_rabatowe_id' => 1
        ],
    ];
}
