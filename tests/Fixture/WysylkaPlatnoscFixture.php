<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * WysylkaPlatnoscFixture
 *
 */
class WysylkaPlatnoscFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'wysylka_platnosc';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'wysylka_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'rodzaj_platnosci_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'wysylka_id' => ['type' => 'index', 'columns' => ['wysylka_id'], 'length' => []],
            'rodzaj_platnosci_id' => ['type' => 'index', 'columns' => ['rodzaj_platnosci_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'wysylka_platnosc_ibfk_1' => ['type' => 'foreign', 'columns' => ['wysylka_id'], 'references' => ['wysylka', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'wysylka_platnosc_ibfk_2' => ['type' => 'foreign', 'columns' => ['rodzaj_platnosci_id'], 'references' => ['rodzaj_platnosci', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'wysylka_id' => 1,
            'rodzaj_platnosci_id' => 1
        ],
    ];
}
