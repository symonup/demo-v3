<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * BanerKategoriaFixture
 *
 */
class BanerKategoriaFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'baner_kategoria';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'kategoria_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'banner_kategoria_plik_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'kategoria_id' => ['type' => 'index', 'columns' => ['kategoria_id'], 'length' => []],
            'banner_kategoria_plik_id' => ['type' => 'index', 'columns' => ['banner_kategoria_plik_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'id_UNIQUE' => ['type' => 'unique', 'columns' => ['id'], 'length' => []],
            'baner_kategoria_ibfk_1' => ['type' => 'foreign', 'columns' => ['banner_kategoria_plik_id'], 'references' => ['baner_kategoria_plik', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'baner_kategoria_ibfk_2' => ['type' => 'foreign', 'columns' => ['kategoria_id'], 'references' => ['kategoria', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_polish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'kategoria_id' => 1,
            'banner_kategoria_plik_id' => 1
        ],
    ];
}
