<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FakturaPlikiFixture
 *
 */
class FakturaPlikiFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'faktura_pliki';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'zamowienie_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'allegro_zamowienie_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'plik' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'data' => ['type' => 'timestamp', 'length' => null, 'null' => false, 'default' => 'CURRENT_TIMESTAMP', 'comment' => '', 'precision' => null],
        '_indexes' => [
            'zamowienie_id' => ['type' => 'index', 'columns' => ['zamowienie_id'], 'length' => []],
            'allegro_zamowienie_id' => ['type' => 'index', 'columns' => ['allegro_zamowienie_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'faktura_pliki_ibfk_1' => ['type' => 'foreign', 'columns' => ['zamowienie_id'], 'references' => ['zamowienie', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'faktura_pliki_ibfk_2' => ['type' => 'foreign', 'columns' => ['allegro_zamowienie_id'], 'references' => ['allegro_zamowienie', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'zamowienie_id' => 1,
            'allegro_zamowienie_id' => 1,
            'plik' => 'Lorem ipsum dolor sit amet',
            'data' => 1505154556
        ],
    ];
}
