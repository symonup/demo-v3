<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AtrybutKategoriaFixture
 *
 */
class AtrybutKategoriaFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'atrybut_kategoria';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'atrybut_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'kategoria_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'atrybut_id' => ['type' => 'index', 'columns' => ['atrybut_id'], 'length' => []],
            'kategoria_id' => ['type' => 'index', 'columns' => ['kategoria_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'atrybut_kategoria_ibfk_1' => ['type' => 'foreign', 'columns' => ['atrybut_id'], 'references' => ['atrybut', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'atrybut_kategoria_ibfk_2' => ['type' => 'foreign', 'columns' => ['kategoria_id'], 'references' => ['kategoria', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'atrybut_id' => 1,
            'kategoria_id' => 1
        ],
    ];
}
