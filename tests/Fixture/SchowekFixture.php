<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SchowekFixture
 *
 */
class SchowekFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'schowek';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'uzytkownik_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'towar_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'data' => ['type' => 'timestamp', 'length' => null, 'null' => false, 'default' => 'CURRENT_TIMESTAMP', 'comment' => '', 'precision' => null],
        '_indexes' => [
            'uzytkownik_id' => ['type' => 'index', 'columns' => ['uzytkownik_id'], 'length' => []],
            'towar_id' => ['type' => 'index', 'columns' => ['towar_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'uzytkownik_id_2' => ['type' => 'unique', 'columns' => ['uzytkownik_id', 'towar_id'], 'length' => []],
            'schowek_ibfk_1' => ['type' => 'foreign', 'columns' => ['uzytkownik_id'], 'references' => ['uzytkownik', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'schowek_ibfk_2' => ['type' => 'foreign', 'columns' => ['towar_id'], 'references' => ['towar', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'uzytkownik_id' => 1,
            'towar_id' => 1,
            'data' => 1522226560
        ],
    ];
}
