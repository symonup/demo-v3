<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UzytkownikFixture
 *
 */
class UzytkownikFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'uzytkownik';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'password' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'email' => ['type' => 'string', 'length' => 100, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'telefon' => ['type' => 'string', 'length' => 15, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'imie' => ['type' => 'string', 'length' => 50, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'nazwisko' => ['type' => 'string', 'length' => 70, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'newsletter' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
        'rabat' => ['type' => 'string', 'length' => 5, 'null' => false, 'default' => '0', 'collate' => 'utf8_general_ci', 'comment' => 'rabat ceny towaru dla zalogowanego użytkownika (w procentach)', 'precision' => null, 'fixed' => null],
        'token' => ['type' => 'string', 'fixed' => true, 'length' => 32, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null],
        'token_data' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'waluta_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'administrator_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'data_rejestracji' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'aktywny' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null],
        'firma' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'nip' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'waluta_id' => ['type' => 'index', 'columns' => ['waluta_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'email' => ['type' => 'unique', 'columns' => ['email'], 'length' => []],
            'administrator_id' => ['type' => 'unique', 'columns' => ['administrator_id'], 'length' => []],
            'uzytkownik_ibfk_1' => ['type' => 'foreign', 'columns' => ['waluta_id'], 'references' => ['waluta', 'id'], 'update' => 'cascade', 'delete' => 'restrict', 'length' => []],
            'uzytkownik_ibfk_2' => ['type' => 'foreign', 'columns' => ['administrator_id'], 'references' => ['administrator', 'id'], 'update' => 'cascade', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'password' => 'Lorem ipsum dolor sit amet',
            'email' => 'Lorem ipsum dolor sit amet',
            'telefon' => 'Lorem ipsum d',
            'imie' => 'Lorem ipsum dolor sit amet',
            'nazwisko' => 'Lorem ipsum dolor sit amet',
            'newsletter' => 1,
            'rabat' => 'Lor',
            'token' => 'Lorem ipsum dolor sit amet',
            'token_data' => '2017-09-21 11:25:46',
            'waluta_id' => 1,
            'administrator_id' => 1,
            'data_rejestracji' => '2017-09-21 11:25:46',
            'aktywny' => 1,
            'firma' => 'Lorem ipsum dolor sit amet',
            'nip' => 'Lorem ipsum dolor sit amet'
        ],
    ];
}
