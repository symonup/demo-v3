<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * WysylkaPunktyOdbioruFixture
 *
 */
class WysylkaPunktyOdbioruFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'wysylka_punkty_odbioru';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'wysylka_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'punkty_odbioru_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'wysylka_id' => ['type' => 'index', 'columns' => ['wysylka_id'], 'length' => []],
            'punkty_odbioru_id' => ['type' => 'index', 'columns' => ['punkty_odbioru_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'wysylka_punkty_odbioru_ibfk_1' => ['type' => 'foreign', 'columns' => ['punkty_odbioru_id'], 'references' => ['punkty_odbioru', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'wysylka_punkty_odbioru_ibfk_2' => ['type' => 'foreign', 'columns' => ['wysylka_id'], 'references' => ['wysylka', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'wysylka_id' => 1,
            'punkty_odbioru_id' => 1
        ],
    ];
}
