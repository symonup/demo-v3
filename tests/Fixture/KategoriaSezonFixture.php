<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * KategoriaSezonFixture
 *
 */
class KategoriaSezonFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'kategoria_sezon';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'sezon_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'kategoria_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'sezon_id' => ['type' => 'index', 'columns' => ['sezon_id'], 'length' => []],
            'kategoria_id' => ['type' => 'index', 'columns' => ['kategoria_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'kategoria_sezon_ibfk_1' => ['type' => 'foreign', 'columns' => ['sezon_id'], 'references' => ['sezon', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'kategoria_sezon_ibfk_2' => ['type' => 'foreign', 'columns' => ['kategoria_id'], 'references' => ['kategoria', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'sezon_id' => 1,
            'kategoria_id' => 1
        ],
    ];
}
