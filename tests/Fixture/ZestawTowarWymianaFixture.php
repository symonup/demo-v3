<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ZestawTowarWymianaFixture
 *
 */
class ZestawTowarWymianaFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'zestaw_towar_wymiana';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'zestaw_towar_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'towar_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'zestaw_towar_id' => ['type' => 'index', 'columns' => ['zestaw_towar_id'], 'length' => []],
            'towar_id' => ['type' => 'index', 'columns' => ['towar_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'zestaw_towar_wymiana_ibfk_1' => ['type' => 'foreign', 'columns' => ['zestaw_towar_id'], 'references' => ['zestaw_towar', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'zestaw_towar_wymiana_ibfk_2' => ['type' => 'foreign', 'columns' => ['towar_id'], 'references' => ['towar', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'zestaw_towar_id' => 1,
            'towar_id' => 1
        ],
    ];
}
