<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * KartaPodarunkowaKodFixture
 *
 */
class KartaPodarunkowaKodFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'karta_podarunkowa_kod';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'karta_podarunkowa_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'zamowienie_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'uzytkownik_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'kod' => ['type' => 'string', 'length' => 30, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'wartosc' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'data_dodania' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'data_uzycia' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'data_waznosci' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'aktywny' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null],
        'uzyty' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null],
        '_indexes' => [
            'karta_podarunkowa_id' => ['type' => 'index', 'columns' => ['karta_podarunkowa_id'], 'length' => []],
            'zamowienie_id' => ['type' => 'index', 'columns' => ['zamowienie_id'], 'length' => []],
            'uzytkownik_id' => ['type' => 'index', 'columns' => ['uzytkownik_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'kod' => ['type' => 'unique', 'columns' => ['kod'], 'length' => []],
            'karta_podarunkowa_kod_ibfk_1' => ['type' => 'foreign', 'columns' => ['karta_podarunkowa_id'], 'references' => ['karta_podarunkowa', 'id'], 'update' => 'cascade', 'delete' => 'restrict', 'length' => []],
            'karta_podarunkowa_kod_ibfk_2' => ['type' => 'foreign', 'columns' => ['uzytkownik_id'], 'references' => ['uzytkownik', 'id'], 'update' => 'cascade', 'delete' => 'noAction', 'length' => []],
            'karta_podarunkowa_kod_ibfk_3' => ['type' => 'foreign', 'columns' => ['zamowienie_id'], 'references' => ['zamowienie', 'id'], 'update' => 'cascade', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'karta_podarunkowa_id' => 1,
            'zamowienie_id' => 1,
            'uzytkownik_id' => 1,
            'kod' => 'Lorem ipsum dolor sit amet',
            'wartosc' => 1,
            'data_dodania' => '2019-01-11 21:59:40',
            'data_uzycia' => '2019-01-11 21:59:40',
            'data_waznosci' => '2019-01-11 21:59:40',
            'aktywny' => 1,
            'uzyty' => 1
        ],
    ];
}
