<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * KategoriaPromocjaFixture
 *
 */
class KategoriaPromocjaFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'kategoria_promocja';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'kategoria_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'towar_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'kategoria_id' => ['type' => 'index', 'columns' => ['kategoria_id'], 'length' => []],
            'towar_id' => ['type' => 'index', 'columns' => ['towar_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'kategoria_id_2' => ['type' => 'unique', 'columns' => ['kategoria_id', 'towar_id'], 'length' => []],
            'kategoria_promocja_ibfk_1' => ['type' => 'foreign', 'columns' => ['kategoria_id'], 'references' => ['kategoria', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'kategoria_promocja_ibfk_2' => ['type' => 'foreign', 'columns' => ['towar_id'], 'references' => ['towar', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'kategoria_id' => 1,
            'towar_id' => 1
        ],
    ];
}
