<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FakturaFixture
 *
 */
class FakturaFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'faktura';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'zamowienie_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'typ' => ['type' => 'string', 'length' => 30, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'status' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'status_fiskalny' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'id_dokumentu' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'numer_dostawcy' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'rozszerzenie_numeru' => ['type' => 'string', 'length' => 30, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'numer_dokumentu' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'numer_do_korekty' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'data_wystawienia_do_korekty' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'numer_zamowienia' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'magazyn_docelowy_mm' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'kod_kontrahenta' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'nazwa_skrocona' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'nazwa_pelna' => ['type' => 'string', 'length' => 250, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'miasto' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'kod_pocztowy' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'adres' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'nip' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'kategoria_dokumentu' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'podtytul_kategorii' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'miejsce_wystawienia' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'data_wystawienia' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'data_sprzedazy' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'data_otrzymania' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'ilosc_pozycji' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'wg_cen_netto' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null],
        'aktywna_cena' => ['type' => 'string', 'length' => 30, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'wartosc_netto' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'wartosc_vat' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'wartosc_brutto' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'koszt' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'rabat_nazwa' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'rabat_procent' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'forma_platnosci' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'termin_platnosci' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'kwota_zaplacona_przy_odbiorze' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'wartosc_do_zaplaty' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'zaokraglenie_do_zaplaty' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'zaokraglenie_vat' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'automatyczne_przeliczanie' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null],
        'statusy_specjalne' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'wystawiajacy' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'odbiorca' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'podstawa_wydania' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'wartosc_opakowan' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'wartosc_zwroconych_opakowan' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'waluta' => ['type' => 'string', 'length' => 10, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'kurs_waluty' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'uwagi' => ['type' => 'string', 'length' => 500, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'komentarz' => ['type' => 'string', 'length' => 500, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'podtytul' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'empty_data' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'import_dokumentu' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null],
        'eksportowy' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null],
        'rodzaj_transakcji' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'platnosc_karta_nazwa' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'platnosc_karta_kwota' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'platnosc_kredyt_nazwa' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'platnosc_kredyt_kwota' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'panstwo' => ['type' => 'string', 'length' => 30, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'prefix_ue' => ['type' => 'string', 'length' => 10, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'czy_ue' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null],
        'token' => ['type' => 'string', 'length' => 128, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'sklep_nazwa' => ['type' => 'string', 'length' => 150, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'sklep_miasto' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'sklep_kod_pocztowy' => ['type' => 'string', 'length' => 10, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'sklep_adres' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'sklep_nip' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'zamowienie_id' => ['type' => 'index', 'columns' => ['zamowienie_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'faktura_ibfk_1' => ['type' => 'foreign', 'columns' => ['zamowienie_id'], 'references' => ['zamowienie', 'id'], 'update' => 'cascade', 'delete' => 'setNull', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'zamowienie_id' => 1,
            'typ' => 'Lorem ipsum dolor sit amet',
            'status' => 1,
            'status_fiskalny' => 1,
            'id_dokumentu' => 'Lorem ipsum dolor sit amet',
            'numer_dostawcy' => 'Lorem ipsum dolor sit amet',
            'rozszerzenie_numeru' => 'Lorem ipsum dolor sit amet',
            'numer_dokumentu' => 'Lorem ipsum dolor sit amet',
            'numer_do_korekty' => 'Lorem ipsum dolor sit amet',
            'data_wystawienia_do_korekty' => '2020-04-02',
            'numer_zamowienia' => 'Lorem ipsum dolor sit amet',
            'magazyn_docelowy_mm' => 'Lorem ipsum dolor sit amet',
            'kod_kontrahenta' => 'Lorem ipsum dolor sit amet',
            'nazwa_skrocona' => 'Lorem ipsum dolor sit amet',
            'nazwa_pelna' => 'Lorem ipsum dolor sit amet',
            'miasto' => 'Lorem ipsum dolor sit amet',
            'kod_pocztowy' => 'Lorem ipsum dolor ',
            'adres' => 'Lorem ipsum dolor sit amet',
            'nip' => 'Lorem ipsum dolor ',
            'kategoria_dokumentu' => 'Lorem ipsum dolor sit amet',
            'podtytul_kategorii' => 'Lorem ipsum dolor sit amet',
            'miejsce_wystawienia' => 'Lorem ipsum dolor sit amet',
            'data_wystawienia' => '2020-04-02',
            'data_sprzedazy' => '2020-04-02',
            'data_otrzymania' => '2020-04-02',
            'ilosc_pozycji' => 1,
            'wg_cen_netto' => 1,
            'aktywna_cena' => 'Lorem ipsum dolor sit amet',
            'wartosc_netto' => 1,
            'wartosc_vat' => 1,
            'wartosc_brutto' => 1,
            'koszt' => 1,
            'rabat_nazwa' => 'Lorem ipsum dolor sit amet',
            'rabat_procent' => 1,
            'forma_platnosci' => 'Lorem ipsum dolor sit amet',
            'termin_platnosci' => '2020-04-02',
            'kwota_zaplacona_przy_odbiorze' => 1,
            'wartosc_do_zaplaty' => 1,
            'zaokraglenie_do_zaplaty' => 1,
            'zaokraglenie_vat' => 1,
            'automatyczne_przeliczanie' => 1,
            'statusy_specjalne' => 1,
            'wystawiajacy' => 'Lorem ipsum dolor sit amet',
            'odbiorca' => 'Lorem ipsum dolor sit amet',
            'podstawa_wydania' => 'Lorem ipsum dolor sit amet',
            'wartosc_opakowan' => 1,
            'wartosc_zwroconych_opakowan' => 1,
            'waluta' => 'Lorem ip',
            'kurs_waluty' => 1,
            'uwagi' => 'Lorem ipsum dolor sit amet',
            'komentarz' => 'Lorem ipsum dolor sit amet',
            'podtytul' => 'Lorem ipsum dolor sit amet',
            'empty_data' => 'Lorem ipsum dolor sit amet',
            'import_dokumentu' => 1,
            'eksportowy' => 1,
            'rodzaj_transakcji' => 1,
            'platnosc_karta_nazwa' => 'Lorem ipsum dolor sit amet',
            'platnosc_karta_kwota' => 1,
            'platnosc_kredyt_nazwa' => 'Lorem ipsum dolor sit amet',
            'platnosc_kredyt_kwota' => 1,
            'panstwo' => 'Lorem ipsum dolor sit amet',
            'prefix_ue' => 'Lorem ip',
            'czy_ue' => 1,
            'token' => 'Lorem ipsum dolor sit amet',
            'sklep_nazwa' => 'Lorem ipsum dolor sit amet',
            'sklep_miasto' => 'Lorem ipsum dolor sit amet',
            'sklep_kod_pocztowy' => 'Lorem ip',
            'sklep_adres' => 'Lorem ipsum dolor sit amet',
            'sklep_nip' => 'Lorem ipsum dolor '
        ],
    ];
}
