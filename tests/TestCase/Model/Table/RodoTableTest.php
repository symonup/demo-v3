<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RodoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RodoTable Test Case
 */
class RodoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RodoTable
     */
    public $Rodo;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.rodo'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Rodo') ? [] : ['className' => RodoTable::class];
        $this->Rodo = TableRegistry::get('Rodo', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Rodo);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
