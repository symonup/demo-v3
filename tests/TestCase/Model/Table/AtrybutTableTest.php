<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AtrybutTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AtrybutTable Test Case
 */
class AtrybutTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AtrybutTable
     */
    public $Atrybut;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.atrybut',
        'app.atrybut_typs',
        'app.kategorias',
        'app.atrybut_podrzedne',
        'app.i18n',
        'app.atrybut_i18n',
        'app.kategoria',
        'app.atrybut_kategoria',
        'app.towar',
        'app.towar_atrybut'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Atrybut') ? [] : ['className' => AtrybutTable::class];
        $this->Atrybut = TableRegistry::get('Atrybut', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Atrybut);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
