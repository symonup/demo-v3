<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PlatnoscTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PlatnoscTable Test Case
 */
class PlatnoscTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PlatnoscTable
     */
    public $Platnosc;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.platnosc',
        'app.uzytkownik',
        'app.zamowienie',
        'app.rodzaj_platnosci',
        'app.waluta',
        'app.wysylka',
        'app.wysylka_platnosc'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Platnosc') ? [] : ['className' => PlatnoscTable::class];
        $this->Platnosc = TableRegistry::get('Platnosc', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Platnosc);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
