<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TowarArtykulTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TowarArtykulTable Test Case
 */
class TowarArtykulTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TowarArtykulTable
     */
    public $TowarArtykul;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.towar_artykul',
        'app.towar',
        'app.producents',
        'app.kolors',
        'app.wzors',
        'app.kolekcjas',
        'app.allegro_aukcja',
        'app.items',
        'app.allegro_aukcja_przesylka',
        'app.allegro_aukcja_wariant',
        'app.allegro_pola_kategoria',
        'app.kategoria_promocja',
        'app.kategoria',
        'app.google_kategorie',
        'app.atrybut',
        'app.atrybut_typs',
        'app.kategorias',
        'app.atrybut_podrzedne',
        'app.towar_atrybut',
        'app.i18n',
        'app.atrybut_i18n',
        'app.atrybut_kategoria',
        'app.baner',
        'app.baner_tekst',
        'app.baner_tekst_i18n',
        'app.baner_kategoria',
        'app.baner_kategoria_plik',
        'app.kategoria_i18n',
        'app.sezon',
        'app.kategoria_sezon',
        'app.sezon_i18n',
        'app.towar_kategoria',
        'app.towar_akcesoria',
        'app.towar_cena',
        'app.towar_opinia',
        'app.towar_podobne',
        'app.towar_polecane',
        'app.towar_uzytkownik_rabat',
        'app.towar_wariant',
        'app.towar_zdjecie',
        'app.wariant_cecha',
        'app.wariant_cecha_wartosc',
        'app.zestaw',
        'app.zestaw_towar',
        'app.zestaw_towar_wymiana',
        'app.allegro_zamowienie',
        'app.transakcjas',
        'app.kupujacies',
        'app.rodzaj_dostawies',
        'app.zamowienie',
        'app.faktura_pliki',
        'app.allegro_zamowienie_towar',
        'app.ofertas',
        'app.zakups',
        'app.artykul',
        'app.artykul_i18n',
        'app.film',
        'app.film_i18n',
        'app.towar_film',
        'app.towar_i18n',
        'app.przeznaczenie',
        'app.ikona',
        'app.przeznaczenie_i18n',
        'app.towar_przeznaczenie',
        'app.zamowienie_towar'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TowarArtykul') ? [] : ['className' => TowarArtykulTable::class];
        $this->TowarArtykul = TableRegistry::get('TowarArtykul', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TowarArtykul);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
