<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MiastoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MiastoTable Test Case
 */
class MiastoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MiastoTable
     */
    public $Miasto;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.miasto'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Miasto') ? [] : ['className' => MiastoTable::class];
        $this->Miasto = TableRegistry::get('Miasto', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Miasto);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
