<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\KategoriaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\KategoriaTable Test Case
 */
class KategoriaTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\KategoriaTable
     */
    public $Kategoria;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.kategoria',
        'app.google_kategorie',
        'app.atrybut',
        'app.atrybut_typs',
        'app.kategorias',
        'app.atrybut_podrzedne',
        'app.towar_atrybut',
        'app.i18n',
        'app.atrybut_i18n',
        'app.atrybut_kategoria',
        'app.towar',
        'app.baner',
        'app.baner_tekst',
        'app.baner_tekst_i18n',
        'app.baner_kategoria',
        'app.baner_kategoria_plik',
        'app.kategoria_i18n',
        'app.sezon',
        'app.kategoria_sezon',
        'app.towar_kategoria'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Kategoria') ? [] : ['className' => KategoriaTable::class];
        $this->Kategoria = TableRegistry::get('Kategoria', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Kategoria);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
