<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WysylkaKosztTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WysylkaKosztTable Test Case
 */
class WysylkaKosztTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\WysylkaKosztTable
     */
    public $WysylkaKoszt;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.wysylka_koszt',
        'app.wysylka',
        'app.vats',
        'app.platnosc',
        'app.uzytkownik',
        'app.uzytkownik_adres',
        'app.zamowienie',
        'app.administrator',
        'app.roles',
        'app.waluta',
        'app.faktura_pliki',
        'app.allegro_zamowienie',
        'app.transakcjas',
        'app.kupujacies',
        'app.rodzaj_dostawies',
        'app.towar',
        'app.producent',
        'app.producent_gwarancja_translation',
        'app.producent_i18n',
        'app.kolor',
        'app.wariant_cecha_wartosc',
        'app.wariant_cecha',
        'app.kolors',
        'app.wzors',
        'app.kolor_nazwa_translation',
        'app.kolor_i18n',
        'app.wzor',
        'app.wzor_nazwa_translation',
        'app.wzor_i18n',
        'app.kolekcja',
        'app.kolekcja_nazwa_translation',
        'app.kolekcja_i18n',
        'app.allegro_aukcja',
        'app.items',
        'app.allegro_aukcja_przesylka',
        'app.allegro_aukcja_wariant',
        'app.allegro_pola_kategoria',
        'app.kategoria_promocja',
        'app.kategoria',
        'app.kategoria_nazwa_translation',
        'app.kategoria_naglowek_translation',
        'app.kategoria_seo_tytul_translation',
        'app.kategoria_seo_slowa_translation',
        'app.kategoria_seo_opis_translation',
        'app.kategoria_menu_tekst_naglowek_translation',
        'app.kategoria_menu_tekst_translation',
        'app.kategoria_tagi_translation',
        'app.kategoria_nazwa_producent_translation',
        'app.kategoria_i18n',
        'app.sezon',
        'app.kategoria_sezon',
        'app.sezon_nazwa_translation',
        'app.sezon_seo_tytul_translation',
        'app.sezon_seo_opis_translation',
        'app.sezon_seo_slowa_translation',
        'app.sezon_naglowek_translation',
        'app.sezon_menu_tekst_naglowek_translation',
        'app.sezon_menu_tekst_translation',
        'app.sezon_i18n',
        'app.towar_kategoria',
        'app.towar_akcesoria',
        'app.towar_cena',
        'app.jednostka',
        'app.jednostka_jednostka_translation',
        'app.jednostka_i18n',
        'app.vat',
        'app.towar_cena_user',
        'app.towar_cena_user_rabat',
        'app.towar_cena_user_all',
        'app.towar_cena_bazowa',
        'app.towar_opinia',
        'app.towar_podobne',
        'app.towar_polecane',
        'app.towar_uzytkownik_rabat',
        'app.towar_wariant',
        'app.towar_atrybut',
        'app.atrybut',
        'app.atrybut_typ',
        'app.atrybut_typ_parent',
        'app.atrybut_typ_parent_nazwa_translation',
        'app.atrybut_typ_parent_pod_tytul_translation',
        'app.atrybut_typ_parent_i18n',
        'app.atrybut_typ_nazwa_translation',
        'app.atrybut_typ_i18n',
        'app.atrybut_podrzedne',
        'app.atrybut_nazwa_translation',
        'app.atrybut_pod_tytul_translation',
        'app.atrybut_i18n',
        'app.towar_zdjecie',
        'app.zestaw',
        'app.zamowienie_towar',
        'app.zestaw_towar',
        'app.zestaw_towar_wymiana',
        'app.artykul',
        'app.towar_artykul',
        'app.artykul_tytul_translation',
        'app.artykul_pod_tytul_translation',
        'app.artykul_tresc_translation',
        'app.artykul_seo_tytul_translation',
        'app.artykul_seo_slowa_translation',
        'app.artykul_seo_opis_translation',
        'app.artykul_i18n',
        'app.akcesoria',
        'app.akcesoria_nazwa_translation',
        'app.akcesoria_opis_translation',
        'app.akcesoria_opis2_translation',
        'app.akcesoria_hint_cena_translation',
        'app.akcesoria_seo_tytul_translation',
        'app.akcesoria_seo_slowa_translation',
        'app.akcesoria_seo_opis_translation',
        'app.akcesoria_zestaw_nazwa_translation',
        'app.towar_i18n',
        'app.podobne',
        'app.podobne_nazwa_translation',
        'app.podobne_opis_translation',
        'app.podobne_opis2_translation',
        'app.podobne_hint_cena_translation',
        'app.podobne_seo_tytul_translation',
        'app.podobne_seo_slowa_translation',
        'app.podobne_seo_opis_translation',
        'app.podobne_zestaw_nazwa_translation',
        'app.polecane',
        'app.polecane_nazwa_translation',
        'app.polecane_opis_translation',
        'app.polecane_opis2_translation',
        'app.polecane_hint_cena_translation',
        'app.polecane_seo_tytul_translation',
        'app.polecane_seo_slowa_translation',
        'app.polecane_seo_opis_translation',
        'app.polecane_zestaw_nazwa_translation',
        'app.towar_film',
        'app.przeznaczenie',
        'app.ikona',
        'app.towar_przeznaczenie',
        'app.przeznaczenie_nazwa_translation',
        'app.przeznaczenie_i18n',
        'app.towar_nazwa_translation',
        'app.towar_opis_translation',
        'app.towar_opis2_translation',
        'app.towar_seo_tytul_translation',
        'app.towar_seo_slowa_translation',
        'app.towar_seo_opis_translation',
        'app.towar_zestaw_nazwa_translation',
        'app.allegro_zamowienie_towar',
        'app.ofertas',
        'app.zakups',
        'app.i18n',
        'app.uzytkownik_wysylka',
        'app.uzytkownik_faktura',
        'app.rodzaj_platnosci',
        'app.wysylka_platnosc'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('WysylkaKoszt') ? [] : ['className' => WysylkaKosztTable::class];
        $this->WysylkaKoszt = TableRegistry::get('WysylkaKoszt', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WysylkaKoszt);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
