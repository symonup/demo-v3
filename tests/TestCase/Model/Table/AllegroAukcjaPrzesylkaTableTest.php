<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AllegroAukcjaPrzesylkaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AllegroAukcjaPrzesylkaTable Test Case
 */
class AllegroAukcjaPrzesylkaTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AllegroAukcjaPrzesylkaTable
     */
    public $AllegroAukcjaPrzesylka;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.allegro_aukcja_przesylka',
        'app.allegro_aukcja',
        'app.towar',
        'app.items',
        'app.allegro_aukcja_wariant',
        'app.allegro_pola_kategoria'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('AllegroAukcjaPrzesylka') ? [] : ['className' => AllegroAukcjaPrzesylkaTable::class];
        $this->AllegroAukcjaPrzesylka = TableRegistry::get('AllegroAukcjaPrzesylka', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AllegroAukcjaPrzesylka);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
