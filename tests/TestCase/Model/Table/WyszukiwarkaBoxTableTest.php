<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WyszukiwarkaBoxTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WyszukiwarkaBoxTable Test Case
 */
class WyszukiwarkaBoxTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\WyszukiwarkaBoxTable
     */
    public $WyszukiwarkaBox;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.wyszukiwarka_box',
        'app.wyszukiwarka'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('WyszukiwarkaBox') ? [] : ['className' => WyszukiwarkaBoxTable::class];
        $this->WyszukiwarkaBox = TableRegistry::get('WyszukiwarkaBox', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WyszukiwarkaBox);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
