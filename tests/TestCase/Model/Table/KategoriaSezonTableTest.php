<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\KategoriaSezonTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\KategoriaSezonTable Test Case
 */
class KategoriaSezonTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\KategoriaSezonTable
     */
    public $KategoriaSezon;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.kategoria_sezon',
        'app.sezon',
        'app.kategoria',
        'app.google_kategorie',
        'app.atrybut',
        'app.atrybut_typs',
        'app.kategorias',
        'app.atrybut_podrzedne',
        'app.towar_atrybut',
        'app.i18n',
        'app.atrybut_i18n',
        'app.atrybut_kategoria',
        'app.towar',
        'app.baner',
        'app.baner_tekst',
        'app.baner_tekst_i18n',
        'app.baner_kategoria',
        'app.baner_kategoria_plik',
        'app.kategoria_i18n',
        'app.towar_kategoria'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('KategoriaSezon') ? [] : ['className' => KategoriaSezonTable::class];
        $this->KategoriaSezon = TableRegistry::get('KategoriaSezon', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->KategoriaSezon);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
