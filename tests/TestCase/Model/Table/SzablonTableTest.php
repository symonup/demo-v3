<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SzablonTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SzablonTable Test Case
 */
class SzablonTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SzablonTable
     */
    public $Szablon;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.szablon',
        'app.i18n',
        'app.szablon_i18n'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Szablon') ? [] : ['className' => SzablonTable::class];
        $this->Szablon = TableRegistry::get('Szablon', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Szablon);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
