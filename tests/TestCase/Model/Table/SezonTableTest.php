<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SezonTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SezonTable Test Case
 */
class SezonTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SezonTable
     */
    public $Sezon;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sezon',
        'app.kategoria',
        'app.google_kategorie',
        'app.atrybut',
        'app.atrybut_typs',
        'app.kategorias',
        'app.atrybut_podrzedne',
        'app.towar_atrybut',
        'app.i18n',
        'app.atrybut_i18n',
        'app.atrybut_kategoria',
        'app.towar',
        'app.baner',
        'app.baner_tekst',
        'app.baner_tekst_i18n',
        'app.baner_kategoria',
        'app.baner_kategoria_plik',
        'app.kategoria_i18n',
        'app.kategoria_sezon',
        'app.towar_kategoria',
        'app.sezon_i18n'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Sezon') ? [] : ['className' => SezonTable::class];
        $this->Sezon = TableRegistry::get('Sezon', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Sezon);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
