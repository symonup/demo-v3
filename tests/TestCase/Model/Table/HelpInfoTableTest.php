<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\HelpInfoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\HelpInfoTable Test Case
 */
class HelpInfoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\HelpInfoTable
     */
    public $HelpInfo;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.help_info'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('HelpInfo') ? [] : ['className' => HelpInfoTable::class];
        $this->HelpInfo = TableRegistry::get('HelpInfo', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->HelpInfo);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
