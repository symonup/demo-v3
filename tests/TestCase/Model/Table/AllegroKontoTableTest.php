<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AllegroKontoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AllegroKontoTable Test Case
 */
class AllegroKontoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AllegroKontoTable
     */
    public $AllegroKonto;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.allegro_konto'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('AllegroKonto') ? [] : ['className' => AllegroKontoTable::class];
        $this->AllegroKonto = TableRegistry::get('AllegroKonto', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AllegroKonto);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
