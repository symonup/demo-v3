<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PrzeznaczenieTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PrzeznaczenieTable Test Case
 */
class PrzeznaczenieTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PrzeznaczenieTable
     */
    public $Przeznaczenie;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.przeznaczenie',
        'app.ikona',
        'app.i18n',
        'app.przeznaczenie_i18n',
        'app.towar',
        'app.towar_przeznaczenie'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Przeznaczenie') ? [] : ['className' => PrzeznaczenieTable::class];
        $this->Przeznaczenie = TableRegistry::get('Przeznaczenie', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Przeznaczenie);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
