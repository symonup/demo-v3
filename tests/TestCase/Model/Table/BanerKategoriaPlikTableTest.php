<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BanerKategoriaPlikTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BanerKategoriaPlikTable Test Case
 */
class BanerKategoriaPlikTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BanerKategoriaPlikTable
     */
    public $BanerKategoriaPlik;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.baner_kategoria_plik'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('BanerKategoriaPlik') ? [] : ['className' => BanerKategoriaPlikTable::class];
        $this->BanerKategoriaPlik = TableRegistry::get('BanerKategoriaPlik', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BanerKategoriaPlik);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
