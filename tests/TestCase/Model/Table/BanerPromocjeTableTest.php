<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BanerPromocjeTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BanerPromocjeTable Test Case
 */
class BanerPromocjeTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BanerPromocjeTable
     */
    public $BanerPromocje;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.baner_promocje',
        'app.i18n',
        'app.baner_promocje_i18n'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('BanerPromocje') ? [] : ['className' => BanerPromocjeTable::class];
        $this->BanerPromocje = TableRegistry::get('BanerPromocje', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BanerPromocje);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
