<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SzablonFakturaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SzablonFakturaTable Test Case
 */
class SzablonFakturaTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SzablonFakturaTable
     */
    public $SzablonFaktura;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.szablon_faktura',
        'app.i18n',
        'app.szablon_faktura_i18n'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SzablonFaktura') ? [] : ['className' => SzablonFakturaTable::class];
        $this->SzablonFaktura = TableRegistry::get('SzablonFaktura', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SzablonFaktura);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
