<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BanerTekstTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BanerTekstTable Test Case
 */
class BanerTekstTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BanerTekstTable
     */
    public $BanerTekst;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.baner_tekst',
        'app.baner',
        'app.kategoria',
        'app.baner_kategoria',
        'app.baner_kategoria_plik',
        'app.i18n',
        'app.baner_tekst_i18n'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('BanerTekst') ? [] : ['className' => BanerTekstTable::class];
        $this->BanerTekst = TableRegistry::get('BanerTekst', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BanerTekst);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
