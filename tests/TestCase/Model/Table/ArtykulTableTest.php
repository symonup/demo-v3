<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ArtykulTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ArtykulTable Test Case
 */
class ArtykulTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ArtykulTable
     */
    public $Artykul;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.artykul',
        'app.i18n',
        'app.artykul_i18n',
        'app.towar',
        'app.towar_artykul'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Artykul') ? [] : ['className' => ArtykulTable::class];
        $this->Artykul = TableRegistry::get('Artykul', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Artykul);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
