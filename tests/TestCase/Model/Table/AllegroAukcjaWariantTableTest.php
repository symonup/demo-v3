<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AllegroAukcjaWariantTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AllegroAukcjaWariantTable Test Case
 */
class AllegroAukcjaWariantTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AllegroAukcjaWariantTable
     */
    public $AllegroAukcjaWariant;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.allegro_aukcja_wariant',
        'app.allegro_aukcja',
        'app.towar',
        'app.items',
        'app.allegro_aukcja_przesylka',
        'app.allegro_pola_kategoria'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('AllegroAukcjaWariant') ? [] : ['className' => AllegroAukcjaWariantTable::class];
        $this->AllegroAukcjaWariant = TableRegistry::get('AllegroAukcjaWariant', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AllegroAukcjaWariant);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
