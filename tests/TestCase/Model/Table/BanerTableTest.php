<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BanerTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BanerTable Test Case
 */
class BanerTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BanerTable
     */
    public $Baner;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.baner',
        'app.baner_tekst',
        'app.kategoria',
        'app.baner_kategoria'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Baner') ? [] : ['className' => BanerTable::class];
        $this->Baner = TableRegistry::get('Baner', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Baner);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
