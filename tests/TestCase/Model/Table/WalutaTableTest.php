<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WalutaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WalutaTable Test Case
 */
class WalutaTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\WalutaTable
     */
    public $Waluta;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.waluta'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Waluta') ? [] : ['className' => WalutaTable::class];
        $this->Waluta = TableRegistry::get('Waluta', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Waluta);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
