<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\KolekcjaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\KolekcjaTable Test Case
 */
class KolekcjaTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\KolekcjaTable
     */
    public $Kolekcja;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.kolekcja',
        'app.towar',
        'app.i18n',
        'app.kolekcja_i18n'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Kolekcja') ? [] : ['className' => KolekcjaTable::class];
        $this->Kolekcja = TableRegistry::get('Kolekcja', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Kolekcja);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
