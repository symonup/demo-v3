<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\KonfiguracjaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\KonfiguracjaTable Test Case
 */
class KonfiguracjaTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\KonfiguracjaTable
     */
    public $Konfiguracja;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.konfiguracja'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Konfiguracja') ? [] : ['className' => KonfiguracjaTable::class];
        $this->Konfiguracja = TableRegistry::get('Konfiguracja', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Konfiguracja);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
