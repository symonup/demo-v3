<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BanerKategoriaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BanerKategoriaTable Test Case
 */
class BanerKategoriaTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BanerKategoriaTable
     */
    public $BanerKategoria;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.baner_kategoria',
        'app.kategoria',
        'app.baner_kategoria_plik'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('BanerKategoria') ? [] : ['className' => BanerKategoriaTable::class];
        $this->BanerKategoria = TableRegistry::get('BanerKategoria', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BanerKategoria);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
