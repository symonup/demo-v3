<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AllegroSzablonTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AllegroSzablonTable Test Case
 */
class AllegroSzablonTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AllegroSzablonTable
     */
    public $AllegroSzablon;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.allegro_szablon'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('AllegroSzablon') ? [] : ['className' => AllegroSzablonTable::class];
        $this->AllegroSzablon = TableRegistry::get('AllegroSzablon', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AllegroSzablon);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
