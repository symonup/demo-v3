<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RoutingTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RoutingTable Test Case
 */
class RoutingTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RoutingTable
     */
    public $Routing;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.routing'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Routing') ? [] : ['className' => RoutingTable::class];
        $this->Routing = TableRegistry::get('Routing', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Routing);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
