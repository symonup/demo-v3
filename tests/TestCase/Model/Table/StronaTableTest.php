<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StronaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StronaTable Test Case
 */
class StronaTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StronaTable
     */
    public $Strona;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.strona',
        'app.i18n',
        'app.strona_i18n'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Strona') ? [] : ['className' => StronaTable::class];
        $this->Strona = TableRegistry::get('Strona', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Strona);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
