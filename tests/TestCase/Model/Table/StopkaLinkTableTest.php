<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StopkaLinkTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StopkaLinkTable Test Case
 */
class StopkaLinkTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StopkaLinkTable
     */
    public $StopkaLink;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.stopka_link',
        'app.stopka_grupas',
        'app.i18n',
        'app.stopka_link_i18n'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('StopkaLink') ? [] : ['className' => StopkaLinkTable::class];
        $this->StopkaLink = TableRegistry::get('StopkaLink', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->StopkaLink);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
