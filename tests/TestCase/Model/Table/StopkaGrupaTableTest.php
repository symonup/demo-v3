<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StopkaGrupaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StopkaGrupaTable Test Case
 */
class StopkaGrupaTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StopkaGrupaTable
     */
    public $StopkaGrupa;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.stopka_grupa',
        'app.stopka_link',
        'app.i18n',
        'app.stopka_grupa_i18n'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('StopkaGrupa') ? [] : ['className' => StopkaGrupaTable::class];
        $this->StopkaGrupa = TableRegistry::get('StopkaGrupa', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->StopkaGrupa);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
