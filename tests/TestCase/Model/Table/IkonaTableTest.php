<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\IkonaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\IkonaTable Test Case
 */
class IkonaTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\IkonaTable
     */
    public $Ikona;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ikona',
        'app.przeznaczenie'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Ikona') ? [] : ['className' => IkonaTable::class];
        $this->Ikona = TableRegistry::get('Ikona', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Ikona);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
