<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AtrybutPodrzedneTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AtrybutPodrzedneTable Test Case
 */
class AtrybutPodrzedneTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AtrybutPodrzedneTable
     */
    public $AtrybutPodrzedne;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.atrybut_podrzedne',
        'app.atrybut',
        'app.atrybut_typs',
        'app.kategorias',
        'app.i18n',
        'app.atrybut_i18n',
        'app.kategoria',
        'app.atrybut_kategoria',
        'app.towar',
        'app.towar_atrybut'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('AtrybutPodrzedne') ? [] : ['className' => AtrybutPodrzedneTable::class];
        $this->AtrybutPodrzedne = TableRegistry::get('AtrybutPodrzedne', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AtrybutPodrzedne);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
