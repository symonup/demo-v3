<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WysylkaKrajeKosztyTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WysylkaKrajeKosztyTable Test Case
 */
class WysylkaKrajeKosztyTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\WysylkaKrajeKosztyTable
     */
    public $WysylkaKrajeKoszty;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.wysylka_kraje_koszty',
        'app.wysylka',
        'app.vat',
        'app.towar_cena',
        'app.towar',
        'app.producent',
        'app.producent_gwarancja_translation',
        'app.producent_naglowek_translation',
        'app.producent_i18n',
        'app.platforma',
        'app.wersja',
        'app.wersja_nazwa_translation',
        'app.gatunek_i18n',
        'app.kolor',
        'app.wariant_cecha_wartosc',
        'app.wariant_cecha',
        'app.kolors',
        'app.wzors',
        'app.kolor_nazwa_translation',
        'app.kolor_i18n',
        'app.wzor',
        'app.wzor_nazwa_translation',
        'app.wzor_i18n',
        'app.bonus',
        'app.kolekcja',
        'app.kolekcja_nazwa_translation',
        'app.kolekcja_i18n',
        'app.allegro_aukcja',
        'app.items',
        'app.allegro_aukcja_przesylka',
        'app.allegro_aukcja_wariant',
        'app.allegro_pola_kategoria',
        'app.kategoria_promocja',
        'app.kategoria',
        'app.kategoria_nazwa_translation',
        'app.kategoria_naglowek_translation',
        'app.kategoria_opis_short_translation',
        'app.kategoria_seo_tekst_translation',
        'app.kategoria_seo_tytul_translation',
        'app.kategoria_seo_slowa_translation',
        'app.kategoria_seo_opis_translation',
        'app.kategoria_i18n',
        'app.towar_kategoria',
        'app.promocja',
        'app.towar_akcesoria',
        'app.towar_cena_default',
        'app.uzytkownik',
        'app.platnosc',
        'app.zamowienie',
        'app.administrator',
        'app.roles',
        'app.waluta',
        'app.faktura_pliki',
        'app.allegro_zamowienie',
        'app.transakcjas',
        'app.kupujacies',
        'app.rodzaj_dostawies',
        'app.allegro_zamowienie_towar',
        'app.ofertas',
        'app.zakups',
        'app.zamowienie_towar',
        'app.towar_atrybut',
        'app.atrybut',
        'app.atrybut_typ',
        'app.atrybut_typ_parent',
        'app.atrybut_typ_parent_nazwa_translation',
        'app.atrybut_typ_parent_pod_tytul_translation',
        'app.atrybut_typ_parent_i18n',
        'app.atrybut_typ_nazwa_translation',
        'app.atrybut_typ_i18n',
        'app.atrybut_podrzedne',
        'app.atrybut_nazwa_translation',
        'app.atrybut_pod_tytul_translation',
        'app.atrybut_i18n',
        'app.towar_zdjecie',
        'app.jednostka',
        'app.jednostka_jednostka_translation',
        'app.jednostka_i18n',
        'app.kody_rabatowe',
        'app.punkty_odbioru',
        'app.wysylka_punkty_odbioru',
        'app.punkty_odbioru_nazwa_translation',
        'app.punkty_odbioru_adres_translation',
        'app.punkty_odbioru_info_translation',
        'app.rodzaj_platnosci_i18n',
        'app.rodzaj_platnosci',
        'app.wysylka_platnosc',
        'app.rodzaj_platnosci_nazwa_translation',
        'app.karta_podarunkowa_kod',
        'app.karta_podarunkowa',
        'app.i18n',
        'app.uzytkownik_adres',
        'app.uzytkownik_adres_wysylka',
        'app.uzytkownik_adres_faktura',
        'app.uzytkownik_wysylka',
        'app.uzytkownik_faktura',
        'app.towar_opinia',
        'app.towar_podobne',
        'app.towar_polecane',
        'app.towar_gatunek',
        'app.gatunek',
        'app.gatunek_nazwa_translation',
        'app.gatunek_nazwa_2_translation',
        'app.towar_uzytkownik_rabat',
        'app.towar_wariant',
        'app.zestaw',
        'app.zestaw_towar',
        'app.zestaw_towar_wymiana',
        'app.akcesoria',
        'app.akcesoria_nazwa_translation',
        'app.akcesoria_opis_translation',
        'app.akcesoria_opis2_translation',
        'app.akcesoria_hint_cena_translation',
        'app.akcesoria_seo_tytul_translation',
        'app.akcesoria_seo_slowa_translation',
        'app.akcesoria_seo_opis_translation',
        'app.akcesoria_zestaw_nazwa_translation',
        'app.towar_i18n',
        'app.podobne',
        'app.podobne_nazwa_translation',
        'app.podobne_opis_translation',
        'app.podobne_opis2_translation',
        'app.podobne_hint_cena_translation',
        'app.podobne_seo_tytul_translation',
        'app.podobne_seo_slowa_translation',
        'app.podobne_seo_opis_translation',
        'app.podobne_zestaw_nazwa_translation',
        'app.polecane',
        'app.polecane_nazwa_translation',
        'app.polecane_opis_translation',
        'app.polecane_opis2_translation',
        'app.polecane_hint_cena_translation',
        'app.polecane_seo_tytul_translation',
        'app.polecane_seo_slowa_translation',
        'app.polecane_seo_opis_translation',
        'app.polecane_zestaw_nazwa_translation',
        'app.towar_film',
        'app.hot_deal',
        'app.hot_deal_nazwa_translation',
        'app.hot_deal_i18n',
        'app.hot_deal_edit',
        'app.hot_deal_edit_nazwa_translation',
        'app.promocja_nazwa_translation',
        'app.promocja_opis_translation',
        'app.promocja_opis2_translation',
        'app.promocja_seo_tytul_translation',
        'app.promocja_seo_slowa_translation',
        'app.promocja_seo_opis_translation',
        'app.promocja_zestaw_nazwa_translation',
        'app.towar_nazwa_translation',
        'app.towar_opis_translation',
        'app.towar_opis2_translation',
        'app.towar_seo_tytul_translation',
        'app.towar_seo_slowa_translation',
        'app.towar_seo_opis_translation',
        'app.towar_zestaw_nazwa_translation',
        'app.wysylka_koszt',
        'app.wysylka_nazwa_translation',
        'app.wysylka_opis_translation',
        'app.wysylka_komunikat_translation'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('WysylkaKrajeKoszty') ? [] : ['className' => WysylkaKrajeKosztyTable::class];
        $this->WysylkaKrajeKoszty = TableRegistry::get('WysylkaKrajeKoszty', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WysylkaKrajeKoszty);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
