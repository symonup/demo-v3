<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WyszukiwarkaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WyszukiwarkaTable Test Case
 */
class WyszukiwarkaTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\WyszukiwarkaTable
     */
    public $Wyszukiwarka;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.wyszukiwarka',
        'app.wyszukiwarka_box'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Wyszukiwarka') ? [] : ['className' => WyszukiwarkaTable::class];
        $this->Wyszukiwarka = TableRegistry::get('Wyszukiwarka', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Wyszukiwarka);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
