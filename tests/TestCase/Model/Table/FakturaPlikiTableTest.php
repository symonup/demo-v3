<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FakturaPlikiTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FakturaPlikiTable Test Case
 */
class FakturaPlikiTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FakturaPlikiTable
     */
    public $FakturaPliki;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.faktura_pliki',
        'app.zamowienie',
        'app.allegro_zamowienie',
        'app.transakcjas',
        'app.kupujacies',
        'app.rodzaj_dostawies',
        'app.towar',
        'app.allegro_zamowienie_towar',
        'app.ofertas',
        'app.zakups'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FakturaPliki') ? [] : ['className' => FakturaPlikiTable::class];
        $this->FakturaPliki = TableRegistry::get('FakturaPliki', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FakturaPliki);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
