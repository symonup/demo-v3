<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AtrybutTypTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AtrybutTypTable Test Case
 */
class AtrybutTypTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AtrybutTypTable
     */
    public $AtrybutTyp;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.atrybut_typ',
        'app.atrybut_typ_parent',
        'app.atrybut',
        'app.atrybut_typs',
        'app.kategorias',
        'app.atrybut_podrzedne',
        'app.towar_atrybut',
        'app.i18n',
        'app.atrybut_i18n',
        'app.kategoria',
        'app.atrybut_kategoria',
        'app.towar',
        'app.atrybut_typ_i18n'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('AtrybutTyp') ? [] : ['className' => AtrybutTypTable::class];
        $this->AtrybutTyp = TableRegistry::get('AtrybutTyp', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AtrybutTyp);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
