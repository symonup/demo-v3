<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UzytkownikRabatDistTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UzytkownikRabatDistTable Test Case
 */
class UzytkownikRabatDistTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UzytkownikRabatDistTable
     */
    public $UzytkownikRabatDist;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.uzytkownik_rabat_dist',
        'app.uzytkowniks'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UzytkownikRabatDist') ? [] : ['className' => UzytkownikRabatDistTable::class];
        $this->UzytkownikRabatDist = TableRegistry::get('UzytkownikRabatDist', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UzytkownikRabatDist);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
