<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\KodyRabatoweTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\KodyRabatoweTable Test Case
 */
class KodyRabatoweTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\KodyRabatoweTable
     */
    public $KodyRabatowe;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.kody_rabatowe'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('KodyRabatowe') ? [] : ['className' => KodyRabatoweTable::class];
        $this->KodyRabatowe = TableRegistry::get('KodyRabatowe', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->KodyRabatowe);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
