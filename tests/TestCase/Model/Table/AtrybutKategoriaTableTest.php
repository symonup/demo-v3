<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AtrybutKategoriaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AtrybutKategoriaTable Test Case
 */
class AtrybutKategoriaTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AtrybutKategoriaTable
     */
    public $AtrybutKategoria;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.atrybut_kategoria',
        'app.atrybut',
        'app.atrybut_typs',
        'app.kategorias',
        'app.atrybut_podrzedne',
        'app.i18n',
        'app.atrybut_i18n',
        'app.kategoria',
        'app.towar',
        'app.towar_atrybut'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('AtrybutKategoria') ? [] : ['className' => AtrybutKategoriaTable::class];
        $this->AtrybutKategoria = TableRegistry::get('AtrybutKategoria', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AtrybutKategoria);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
