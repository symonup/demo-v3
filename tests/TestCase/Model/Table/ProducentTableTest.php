<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProducentTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProducentTable Test Case
 */
class ProducentTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProducentTable
     */
    public $Producent;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.producent',
        'app.towar',
        'app.i18n',
        'app.producent_i18n'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Producent') ? [] : ['className' => ProducentTable::class];
        $this->Producent = TableRegistry::get('Producent', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Producent);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
