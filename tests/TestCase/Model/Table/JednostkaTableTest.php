<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\JednostkaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\JednostkaTable Test Case
 */
class JednostkaTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\JednostkaTable
     */
    public $Jednostka;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.jednostka',
        'app.towar_cena',
        'app.i18n',
        'app.jednostka_i18n'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Jednostka') ? [] : ['className' => JednostkaTable::class];
        $this->Jednostka = TableRegistry::get('Jednostka', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Jednostka);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
