<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\StronaController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\StronaController Test Case
 */
class StronaControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.strona',
        'app.strona_nazwa_translation',
        'app.strona_tresc_translation',
        'app.strona_seo_tytul_translation',
        'app.strona_seo_slowa_translation',
        'app.strona_seo_opis_translation',
        'app.strona_i18n'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test setField method
     *
     * @return void
     */
    public function testSetField()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test sort method
     *
     * @return void
     */
    public function testSort()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
