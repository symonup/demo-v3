<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\KonfiguracjaController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\KonfiguracjaController Test Case
 */
class KonfiguracjaControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.konfiguracja',
        'app.konfiguracja_wartosc_translation',
        'app.konfiguracja_i18n'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
