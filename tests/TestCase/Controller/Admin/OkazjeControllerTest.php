<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\OkazjeController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\OkazjeController Test Case
 */
class OkazjeControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.okazje',
        'app.towar',
        'app.producent',
        'app.producent_gwarancja_translation',
        'app.producent_naglowek_translation',
        'app.producent_i18n',
        'app.platforma',
        'app.wersja',
        'app.wersja_nazwa_translation',
        'app.gatunek_i18n',
        'app.kolor',
        'app.towar_wariant',
        'app.rozmiar',
        'app.wzor',
        'app.wariant_cecha_wartosc',
        'app.wariant_cecha',
        'app.kolors',
        'app.wzors',
        'app.wzor_nazwa_translation',
        'app.wzor_i18n',
        'app.bonus',
        'app.kolekcja',
        'app.kolekcja_nazwa_translation',
        'app.kolekcja_i18n',
        'app.allegro_aukcja',
        'app.allegro_konto',
        'app.kategoria_promocja',
        'app.kategoria',
        'app.kategoria_nazwa_translation',
        'app.kategoria_naglowek_translation',
        'app.kategoria_opis_short_translation',
        'app.kategoria_seo_tekst_translation',
        'app.kategoria_seo_tytul_translation',
        'app.kategoria_seo_slowa_translation',
        'app.kategoria_seo_opis_translation',
        'app.kategoria_i18n',
        'app.kategoria_map',
        'app.towar_kategoria',
        'app.promocja',
        'app.towar_akcesoria',
        'app.towar_cena',
        'app.uzytkownik',
        'app.platnosc',
        'app.zamowienie',
        'app.administrator',
        'app.roles',
        'app.waluta',
        'app.faktura_pliki',
        'app.allegro_zamowienie',
        'app.zamowienie_dhl',
        'app.allegro_zamowienie_towar',
        'app.zamowienie_towar',
        'app.towar_atrybut',
        'app.atrybut',
        'app.atrybut_typ',
        'app.atrybut_typ_parent',
        'app.atrybut_typ_parent_nazwa_translation',
        'app.atrybut_typ_parent_pod_tytul_translation',
        'app.atrybut_typ_parent_i18n',
        'app.atrybut_typ_nazwa_translation',
        'app.atrybut_typ_i18n',
        'app.atrybut_podrzedne',
        'app.atrybut_nazwa_translation',
        'app.atrybut_pod_tytul_translation',
        'app.atrybut_i18n',
        'app.towar_zdjecie',
        'app.jednostka',
        'app.jednostka_jednostka_translation',
        'app.jednostka_i18n',
        'app.vat',
        'app.wysylka',
        'app.wysylka_koszt',
        'app.wysylka_kraje_koszty',
        'app.wysylka_platnosc',
        'app.rodzaj_platnosci',
        'app.rodzaj_platnosci_nazwa_translation',
        'app.rodzaj_platnosci_i18n',
        'app.punkty_odbioru',
        'app.wysylka_punkty_odbioru',
        'app.punkty_odbioru_nazwa_translation',
        'app.punkty_odbioru_adres_translation',
        'app.punkty_odbioru_info_translation',
        'app.wysylka_nazwa_translation',
        'app.wysylka_opis_translation',
        'app.wysylka_komunikat_translation',
        'app.kody_rabatowe',
        'app.kody_rabatowe_kategoria',
        'app.kody_rabatowe_producent',
        'app.karta_podarunkowa_kod',
        'app.karta_podarunkowa',
        'app.uzytkownik_adres',
        'app.uzytkownik_adres_wysylka',
        'app.uzytkownik_adres_faktura',
        'app.uzytkownik_wysylka',
        'app.uzytkownik_faktura',
        'app.towar_cena_default',
        'app.towar_opinia',
        'app.towar_podobne',
        'app.towar_polecane',
        'app.towar_gatunek',
        'app.gatunek',
        'app.gatunek_nazwa_translation',
        'app.gatunek_nazwa_2_translation',
        'app.towar_uzytkownik_rabat',
        'app.zdjecie',
        'app.zestaw',
        'app.zestaw_towar',
        'app.zestaw_towar_wymiana',
        'app.akcesoria',
        'app.akcesoria_nazwa_translation',
        'app.akcesoria_opis_translation',
        'app.akcesoria_opis2_translation',
        'app.akcesoria_hint_cena_translation',
        'app.akcesoria_seo_tytul_translation',
        'app.akcesoria_seo_slowa_translation',
        'app.akcesoria_seo_opis_translation',
        'app.akcesoria_zestaw_nazwa_translation',
        'app.towar_i18n',
        'app.podobne',
        'app.podobne_nazwa_translation',
        'app.podobne_opis_translation',
        'app.podobne_opis2_translation',
        'app.podobne_hint_cena_translation',
        'app.podobne_seo_tytul_translation',
        'app.podobne_seo_slowa_translation',
        'app.podobne_seo_opis_translation',
        'app.podobne_zestaw_nazwa_translation',
        'app.polecane',
        'app.polecane_nazwa_translation',
        'app.polecane_opis_translation',
        'app.polecane_opis2_translation',
        'app.polecane_hint_cena_translation',
        'app.polecane_seo_tytul_translation',
        'app.polecane_seo_slowa_translation',
        'app.polecane_seo_opis_translation',
        'app.polecane_zestaw_nazwa_translation',
        'app.towar_film',
        'app.hot_deal',
        'app.hot_deal_nazwa_translation',
        'app.hot_deal_i18n',
        'app.hot_deal_edit',
        'app.hot_deal_edit_nazwa_translation',
        'app.certyfikat',
        'app.towar_certyfikat',
        'app.wiek',
        'app.opakowanie',
        'app.promocja_nazwa_translation',
        'app.promocja_opis_translation',
        'app.promocja_opis2_translation',
        'app.promocja_seo_tytul_translation',
        'app.promocja_seo_slowa_translation',
        'app.promocja_seo_opis_translation',
        'app.promocja_zestaw_nazwa_translation',
        'app.google_kategorie',
        'app.towar_nazwa_translation',
        'app.towar_opis_translation',
        'app.towar_opis2_translation',
        'app.towar_seo_tytul_translation',
        'app.towar_seo_slowa_translation',
        'app.towar_seo_opis_translation',
        'app.towar_zestaw_nazwa_translation',
        'app.okazje_towar'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test setField method
     *
     * @return void
     */
    public function testSetField()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test sort method
     *
     * @return void
     */
    public function testSort()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
