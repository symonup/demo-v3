<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\SzablonController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\SzablonController Test Case
 */
class SzablonControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.szablon',
        'app.szablon_temat_translation',
        'app.szablon_tresc_translation',
        'app.szablon_i18n'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test setField method
     *
     * @return void
     */
    public function testSetField()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test sort method
     *
     * @return void
     */
    public function testSort()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
