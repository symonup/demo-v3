<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\RodoController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\RodoController Test Case
 */
class RodoControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.rodo'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test setField method
     *
     * @return void
     */
    public function testSetField()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test sort method
     *
     * @return void
     */
    public function testSort()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
