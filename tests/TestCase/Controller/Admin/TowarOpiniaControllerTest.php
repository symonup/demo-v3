<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\TowarOpiniaController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\TowarOpiniaController Test Case
 */
class TowarOpiniaControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.towar_opinia',
        'app.towar',
        'app.producent',
        'app.producent_gwarancja_translation',
        'app.producent_i18n',
        'app.kolor',
        'app.wariant_cecha_wartosc',
        'app.wariant_cecha',
        'app.kolors',
        'app.wzors',
        'app.kolor_nazwa_translation',
        'app.kolor_i18n',
        'app.wzor',
        'app.wzor_nazwa_translation',
        'app.wzor_i18n',
        'app.kolekcja',
        'app.kolekcja_nazwa_translation',
        'app.kolekcja_i18n',
        'app.allegro_aukcja',
        'app.items',
        'app.allegro_aukcja_przesylka',
        'app.allegro_aukcja_wariant',
        'app.allegro_pola_kategoria',
        'app.kategoria_promocja',
        'app.kategoria',
        'app.kategoria_nazwa_translation',
        'app.kategoria_naglowek_translation',
        'app.kategoria_seo_tytul_translation',
        'app.kategoria_seo_slowa_translation',
        'app.kategoria_seo_opis_translation',
        'app.kategoria_menu_tekst_naglowek_translation',
        'app.kategoria_menu_tekst_translation',
        'app.kategoria_tagi_translation',
        'app.kategoria_nazwa_producent_translation',
        'app.kategoria_i18n',
        'app.google_kategorie',
        'app.sezon',
        'app.kategoria_sezon',
        'app.sezon_nazwa_translation',
        'app.sezon_seo_tytul_translation',
        'app.sezon_seo_opis_translation',
        'app.sezon_seo_slowa_translation',
        'app.sezon_naglowek_translation',
        'app.sezon_menu_tekst_naglowek_translation',
        'app.sezon_menu_tekst_translation',
        'app.sezon_i18n',
        'app.towar_kategoria',
        'app.towar_akcesoria',
        'app.towar_cena',
        'app.waluta',
        'app.jednostka',
        'app.jednostka_jednostka_translation',
        'app.jednostka_i18n',
        'app.vat',
        'app.wysylka',
        'app.vats',
        'app.wysylka_koszt',
        'app.zamowienie',
        'app.uzytkownik',
        'app.administrator',
        'app.roles',
        'app.platnosc',
        'app.rodzaj_platnosci',
        'app.wysylka_platnosc',
        'app.rodzaj_platnosci_nazwa_translation',
        'app.rodzaj_platnosci_i18n',
        'app.towar_uzytkownik_rabat',
        'app.uzytkownik_adres',
        'app.i18n',
        'app.faktura_pliki',
        'app.allegro_zamowienie',
        'app.transakcjas',
        'app.kupujacies',
        'app.rodzaj_dostawies',
        'app.allegro_zamowienie_towar',
        'app.ofertas',
        'app.zakups',
        'app.zamowienie_towar',
        'app.allegro_ofertas',
        'app.allegro_zakups',
        'app.zestaws',
        'app.wysylka_nazwa_translation',
        'app.wysylka_i18n',
        'app.towar_cena_user',
        'app.towar_podobne',
        'app.towar_polecane',
        'app.towar_wariant',
        'app.towar_atrybut',
        'app.atrybut',
        'app.atrybut_typ',
        'app.atrybut_typ_parent',
        'app.atrybut_typ_parent_nazwa_translation',
        'app.atrybut_typ_parent_pod_tytul_translation',
        'app.atrybut_typ_parent_i18n',
        'app.atrybut_typ_nazwa_translation',
        'app.atrybut_typ_i18n',
        'app.atrybut_podrzedne',
        'app.atrybut_nazwa_translation',
        'app.atrybut_pod_tytul_translation',
        'app.atrybut_i18n',
        'app.towar_zdjecie',
        'app.zestaw',
        'app.zestaw_towar',
        'app.kategorias',
        'app.zestaw_towar_wymiana',
        'app.artykul',
        'app.towar_artykul',
        'app.artykul_tytul_translation',
        'app.artykul_pod_tytul_translation',
        'app.artykul_tresc_translation',
        'app.artykul_seo_tytul_translation',
        'app.artykul_seo_slowa_translation',
        'app.artykul_seo_opis_translation',
        'app.artykul_i18n',
        'app.akcesoria',
        'app.akcesoria_nazwa_translation',
        'app.akcesoria_opis_translation',
        'app.akcesoria_opis2_translation',
        'app.akcesoria_hint_cena_translation',
        'app.akcesoria_seo_tytul_translation',
        'app.akcesoria_seo_slowa_translation',
        'app.akcesoria_seo_opis_translation',
        'app.akcesoria_zestaw_nazwa_translation',
        'app.towar_i18n',
        'app.podobne',
        'app.podobne_nazwa_translation',
        'app.podobne_opis_translation',
        'app.podobne_opis2_translation',
        'app.podobne_hint_cena_translation',
        'app.podobne_seo_tytul_translation',
        'app.podobne_seo_slowa_translation',
        'app.podobne_seo_opis_translation',
        'app.podobne_zestaw_nazwa_translation',
        'app.polecane',
        'app.polecane_nazwa_translation',
        'app.polecane_opis_translation',
        'app.polecane_opis2_translation',
        'app.polecane_hint_cena_translation',
        'app.polecane_seo_tytul_translation',
        'app.polecane_seo_slowa_translation',
        'app.polecane_seo_opis_translation',
        'app.polecane_zestaw_nazwa_translation',
        'app.towar_film',
        'app.przeznaczenie',
        'app.ikona',
        'app.towar_przeznaczenie',
        'app.przeznaczenie_nazwa_translation',
        'app.przeznaczenie_i18n',
        'app.towar_nazwa_translation',
        'app.towar_opis_translation',
        'app.towar_opis2_translation',
        'app.towar_hint_cena_translation',
        'app.towar_seo_tytul_translation',
        'app.towar_seo_slowa_translation',
        'app.towar_seo_opis_translation',
        'app.towar_zestaw_nazwa_translation'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test setField method
     *
     * @return void
     */
    public function testSetField()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test sort method
     *
     * @return void
     */
    public function testSort()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
